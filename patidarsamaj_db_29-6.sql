-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 29, 2021 at 03:57 AM
-- Server version: 5.7.23-23
-- PHP Version: 7.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `speni6bg_patidarsamaj_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_transcription`
--

CREATE TABLE `admin_transcription` (
  `id` int(11) NOT NULL,
  `txt_id` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `bill_id` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `uid` int(11) NOT NULL,
  `amount` double NOT NULL,
  `notes` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `by_uid` int(11) NOT NULL,
  `by_brand` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `by_model` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `bank_type` enum('bank','cash') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'cash',
  `type` enum('direct','admin') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'admin',
  `time` time NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `admin_transcription`
--

INSERT INTO `admin_transcription` (`id`, `txt_id`, `bill_id`, `uid`, `amount`, `notes`, `by_uid`, `by_brand`, `by_model`, `bank_type`, `type`, `time`, `status`, `date`) VALUES
(1, '16189817587558', 'R1', 214, 151000, 'Direct payment for bill no.: NKMJ37', 2, 'OPPO', 'CPH2001', 'bank', 'admin', '00:00:00', 1, '2021-04-21 01:09:18'),
(2, '16189818087917', 'R2', 253, 10000, 'Direct payment for bill no.: NKMJ38', 2, 'OPPO', 'CPH2001', 'bank', 'admin', '00:00:00', 1, '2021-04-21 01:10:08'),
(4, '16189853215580', 'R3', 8, 846024, 'રોકડ ટ્રાન્સફર', 2, 'OPPO', 'CPH2001', 'cash', 'admin', '00:00:00', 1, '2021-04-21 02:08:41'),
(5, '16189858257145', 'R4', 236, 11000, 'Direct payment for bill no.: NKMJ49', 2, 'OPPO', 'CPH2001', 'bank', 'admin', '00:00:00', 1, '2021-04-21 02:17:05'),
(6, '16191717105842', 'R5', 10, 2669, 'Direct payment for bill no.: NKMJ50', 2, 'OPPO', 'CPH2001', 'bank', 'admin', '00:00:00', 1, '2021-04-23 05:55:10'),
(7, '16191718537966', 'R6', 10, 117000, 'Direct payment for bill no.: NKMJ51', 2, 'OPPO', 'CPH2001', 'bank', 'admin', '00:00:00', 1, '2021-04-23 05:57:33'),
(8, '16192481052986', 'R7', 10, 326, 'Direct payment for bill no.: NKMJ52', 2, 'OPPO', 'CPH2001', 'bank', 'admin', '00:00:00', 1, '2021-04-24 03:08:25'),
(9, '16201224314405', 'R8', 14, 150000, 'Direct payment for bill no.: NKMJ53', 2, 'OPPO', 'CPH2001', 'cash', 'admin', '00:00:00', 1, '2021-05-04 06:00:31'),
(10, '16206275203755', 'R9', 213, 1000, 'Direct payment for bill no.: NKMJ54', 2, 'OPPO', 'CPH2001', 'bank', 'admin', '00:00:00', 1, '2021-05-10 02:18:40'),
(11, '16206471924570', 'R10', 14, 50000, 'Direct payment for bill no.: NKMJ55', 2, 'OPPO', 'CPH2001', 'cash', 'admin', '00:00:00', 1, '2021-05-10 07:46:32'),
(12, '16207438267674', 'R11', 10, 5000, 'Direct payment for bill no.: NKMJ56', 2, 'OPPO', 'CPH2001', 'cash', 'admin', '00:00:00', 1, '2021-05-11 10:37:06'),
(13, '16216870456334', 'R12', 14, 200000, 'Direct payment for bill no.: NKMJ57', 2, 'OPPO', 'CPH2001', 'cash', 'admin', '00:00:00', 1, '2021-05-22 08:37:25'),
(14, '16219434541959', 'R13', 328, 11111, 'Direct payment for bill no.: NKMJ58', 2, 'OPPO', 'CPH2001', 'bank', 'admin', '00:00:00', 1, '2021-05-25 07:50:54'),
(15, '16220136007140', 'R14', 214, 100000, 'Direct payment for bill no.: NKMJ59', 2, 'OPPO', 'CPH2001', 'bank', 'admin', '00:00:00', 1, '2021-05-26 03:20:00'),
(23, '16221177895167', 'R15', 8, 36400, '', 2, 'OPPO', 'CPH2001', 'cash', 'admin', '00:00:00', 1, '2021-05-27 08:16:29'),
(24, '16222631335382', 'R16', 10, 310, 'Direct payment for bill no.: NKMJ65', 2, 'OPPO', 'CPH2001', 'cash', 'admin', '00:00:00', 1, '2021-05-29 00:38:53'),
(25, '16227201943852', 'R17', 361, 600000, 'Direct payment for bill no.: NKMJ66', 2, 'OPPO', 'CPH2001', 'bank', 'admin', '00:00:00', 1, '2021-06-03 07:36:34'),
(26, '16233250209817', 'R18', 28, 3200, 'Direct payment for bill no.: NKMJ67', 2, 'OPPO', 'CPH2001', 'cash', 'admin', '00:00:00', 1, '2021-06-10 07:37:00'),
(27, '16235660509192', 'R19', 28, 100, 'Direct payment for bill no.: NKMJ69', 2, 'OPPO', 'CPH2001', 'cash', 'admin', '00:00:00', 1, '2021-06-13 02:34:10'),
(28, '16240805448420', 'R20', 14, 125000, 'Direct payment for bill no.: NKMJ70', 2, 'OPPO', 'CPH2001', 'cash', 'admin', '00:00:00', 1, '2021-06-19 01:29:04'),
(29, '16243693508268', 'R21', 389, 5000, 'Direct payment for bill no.: NKMJ74', 2, 'OPPO', 'CPH2001', 'bank', 'admin', '00:00:00', 1, '2021-06-22 09:42:30'),
(30, '16245289012001', 'R22', 14, 25000, 'Direct payment for bill no.: NKMJ75', 2, 'OPPO', 'CPH2001', 'cash', 'admin', '00:00:00', 1, '2021-06-24 06:01:41'),
(31, '16245296119687', 'R23', 394, 20000, 'Direct payment for bill no.: NKMJ76', 2, 'OPPO', 'CPH2001', 'bank', 'admin', '00:00:00', 1, '2021-06-24 06:13:31'),
(32, '16246842335671', 'R24', 396, 11000, 'Direct payment for bill no.: NKMJ77', 2, 'OPPO', 'CPH2001', 'bank', 'admin', '00:00:00', 1, '2021-06-26 01:10:33'),
(33, '16246845868103', 'R25', 397, 2500, 'Direct payment for bill no.: NKMJ78', 2, 'OPPO', 'CPH2001', 'bank', 'admin', '00:00:00', 1, '2021-06-26 01:16:26');

-- --------------------------------------------------------

--
-- Table structure for table `ayojan`
--

CREATE TABLE `ayojan` (
  `id` int(11) NOT NULL,
  `title` varchar(1000) NOT NULL,
  `short_notes` longtext NOT NULL,
  `notes` longtext NOT NULL,
  `uid` int(11) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ayojan`
--

INSERT INTO `ayojan` (`id`, `title`, `short_notes`, `notes`, `uid`, `status`, `date`) VALUES
(1, 'Test abcd', 'abcd1', 'bbbb2', 0, -1, '2021-03-16 11:05:59'),
(2, 'abcd ldsfjds ds fdskdsf ds dsfndsds fsnfdnfls f sdfndlfnds fdsf dsfdsnfds dsf dslfn', 'sfjhskfak', 'skfkdskfhdskfhdsfsd f dsfdsf fdsf\r\ndsfdsfsf\r\nsdf\r\nds\r\nfds\r\nf\r\nds\r\nfds\r\nf\r\ndsf\r\naDScnlkCdslkc\r\ncdsaf\r\ndsf\r\ndsf', 0, -1, '2021-03-16 08:34:50'),
(3, 'પાટીદાર શૈક્ષણિક ભવન 2', '11/4/21 ના રોજ આખા દિવસ દરમિયાન શ્રમ કર્યો નુ આયોજન ', '11/4/21 ના રોજ આખા દિવસ દરમિયાન શ્રમ કર્યો નુ આયોજન  કરવામા આવ્યુ છે. \r\n# કુવા માં મોટર ઉતારવા માટે ની વ્યવસ્થા કરવી.', 0, -1, '2021-04-10 15:30:35'),
(4, 'મિથીલીમ બ્લુ નું વિના મૂલ્યે વિતરણ', 'આજે તારીખ ૨૯. ૪. ૨૧ ના રોજ પાટીદાર શૈક્ષણિક ભવન, જસદણ ખાતે મીથિલીન બ્લુ ની બોટલ જરુરી યાત મંદ લોકો નેં વિના મૂલ્યે વિતરણ કરવામાં આવશે\r\nમીથીલીન બ્લુ નાં ઉપયોગ કરવાથી ઓક્સિજન લેવલને જાળવી રાખવા માટે ઉપયોગી નિવડે છે.', '', 0, -1, '2021-04-29 10:24:09'),
(5, '2 રાઉન્ડ Covid 19 વેક્સિન કેમ્પ', 'પાટીદાર શૈક્ષણિક ભવન જસદણ\r\nતારીખ 5/ 4 /2021 ના રોજ પાટીદાર શૈક્ષણિક ભવન જસદણ દ્વારા જેમણે  પ્રથમ ડોઝ (કોરોના વેકસીન) લીધેલો હોય તેવા તમામ વ્યક્તિઓ એ બીજો ડોઝ લેવા માટે તારીખ 1 /7/ 2021ના રોજ સવારે 9 થી સાંજના 5 દરમિયાન આધાર કાર્ડ ની ઝેરોક્ષ તેમજ વેક્સિન લીધાની પહોંચ સાથે લેતા આવવાની રહેશે.\r\nસ્થળ\r\n પાટીદાર શિક્ષણ ભવન જસદણ આટકોટ રોડ પાણીના ટાકા પાસે જસદણ,\r\nફોન, ‌9725138799', '', 0, 1, '2021-06-28 11:53:35'),
(6, 'પાટીદાર શૈક્ષણિક ભવન ખાતે સલેબ ભરવાની કામગીરી', 'પાટીદાર શૈક્ષણિક ભવન જસદણ ખાતે બાંધકામ સલેબ લેવલ પર ચાલી રહ્યું છે.\r\nહાલ લોખંડ બાંધવા ની કામગીરી ચાલુ છે, જેનો સલેબ 25/5/21 ને મંગળવારે ભરાઈ કરવાની હોય સમાજ શ્રેષ્ઠીઓ, વડીલો, યુવાનો હાજરી આપી યોગ્ય માર્ગદર્શન આપશો અને આ ભગીરથ કાર્યમાં સહભાગી થવા વિનંતી.', '', 0, -1, '2021-05-23 11:51:23'),
(7, 'વૃક્ષારોપણ કાર્યક્રમ,  3/6/21', 'વૃક્ષારોપણ કાર્યક્રમ\r\nપાટીદાર શૈક્ષણિક ભવન- જસદણ ખાતે સદભાવના વૃદ્ધાશ્રમ- રાજકોટ, અવતાર ચેરીટેબલ ટ્રસ્ટ -જસદણ, મોક્ષ ધામ સેવા સમિતિ -જસદણ તેમજ આદર્શ ગ્રુપ -જસદણ ના સંયુક્ત ઉપક્રમે વૃક્ષારોપણનો કાર્યક્રમ રાખેલ હોય તો સમાજના યુવાનો વડીલો આ કાર્યક્રમમાં ઉપસ્થિત રહેવા વિનંતી.\r\n\r\nવૃક્ષારોપણ તારીખ: 3/6/21 \r\nને ગુરુવારે\r\nસમય: સવારે,9 થી 12\r\nસ્થળ : પાટીદાર શૈક્ષણિક ભવન\r\nઆટકોટ રોડ,પાણીના ટાંકા પાસે, જસદણ\r\nમો. 9725138799', '\r\nપાટીદાર શૈક્ષણિક ભવન- જસદણ ખાતે જે વૃક્ષ નું વાવેતર કરવામાં આવે તે તમામ વૃક્ષ નું  જસદણના અવતાર ચેરીટેબલ ટ્રસ્ટ, મોક્ષ ધામ સેવા સમિતિ તેમજ આદર્શ ગ્રુપ ત્રણ વર્ષ માટે આ વૃક્ષનું જતન કરશે એમને પાણી પાવા ની વ્યવસ્થા કરશે અને વૃક્ષને ઉચ્છેર કરી મોટા કરીને સમાજને અર્પણ કરશે.\r\nવૃક્ષારોપણ તારીખ: 3/6/21 \r\nને ગુરુવારે\r\nસમય: સવારે,9 થી 12\r\nસ્થળ : પાટીદાર શૈક્ષણિક ભવન\r\nઆટકોટ રોડ,પાણીના ટાંકા પાસે, જસદણ\r\nમો. 9725138799', 0, -1, '2021-06-01 03:35:37'),
(8, 'આગામી તા.૨૫/૦૬/૨૦૨૧ થી ધોરણ ૧ માં ખાનગી શાળામાં મફત પ્રવેશ મેળવવા માટે સરકારશ્રીની RTE યોજના', 'આગામી તા.૨૫/૦૬/૨૦૨૧ થી ધોરણ ૧ માં ખાનગી શાળામાં મફત પ્રવેશ મેળવવા માટે સરકારશ્રીની RTE યોજના\r\n▪️જે બાળકની ઉમર તા.૩૧/૦૫/૨૦૨૧ સુધીમાં ૫ વર્ષ પૂરા થતા હોય તેઓ અરજી કરી શકશે. ', '\r\nજેમાં નીચે મુજબના ડોક્યુમેન્ટ તૈયાર કરી રાખવાના રહેશે.\r\n▪️બાળકનો જન્મનો ઓરીજનલ દાખલો\r\n▪️પિતાનો આવકનો ફોટાવાળો ટી.ડી.ઓ. નો દાખલો \r\n▪️પિતાનો જાતિનો દાખલો\r\n▪️બાળકનું આધારકાર્ડ. \r\n▪️માતા/પિતાનું આધારકાર્ડ  \r\n▪️માતા/પિતાની બેંક ખાતાની બુક \r\n▪️બાળકના બે પાસપોર્ટ ફોટા \r\nવગેરે ડોક્યુમેન્ટ વહેલી તકે તૈયાર કરી લેવા. \r\n▪️જે બાળકને આ યોજનામાં પ્રવેશ મળશે તેને ધોરણ ૧ થી ૮ સુધી ખાનગી શાળામાં ફી ભરવી પડશે નહિ. વધુ માહિતી માટે સંપર્ક કરો.', 0, 1, '2021-06-28 11:56:36');

-- --------------------------------------------------------

--
-- Table structure for table `ayojan_img`
--

CREATE TABLE `ayojan_img` (
  `id` int(11) NOT NULL,
  `ayojan_id` int(11) NOT NULL,
  `image` varchar(1000) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ayojan_img`
--

INSERT INTO `ayojan_img` (`id`, `ayojan_id`, `image`, `status`) VALUES
(2, 1, '65958bdbf1f43e2d15449401f20d91d1IMG20210213102909.jpg', 1),
(3, 1, '56853e8fc3729369fe0919f9684c0160IMG20210204103149.jpg', 1),
(4, 1, 'ebdd47400d8c7849892e4b14d4d1aad5IMG_20210314_191152.jpg', 1),
(5, 1, '777faec5fa1209bb99d68f95e4c2ccdfIMG_20210314_191228.jpg', 1),
(6, 3, '3785f81c03209ac643e568f4c17f2adb_પાટીદારસમાજ_નું_ભવન_20210226_230537.jpg', 1),
(7, 6, '066aba6cb2925ae9976f8c18bda5563fIMG20210523101103.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `business_master`
--

CREATE TABLE `business_master` (
  `id` int(11) NOT NULL,
  `name` varchar(500) NOT NULL,
  `image` varchar(1000) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `business_master`
--

INSERT INTO `business_master` (`id`, `name`, `image`, `status`, `date`) VALUES
(1, 'અન્ય', '8f0c7be6d53e149a1b4d4d2d56fefdd6question_mark_PNG134.png', 1, '2021-02-28 07:59:23'),
(2, 'આંગડિયા', '9627b2de4525d72b17791a546335e0aaIMG_20210222_232119.jpg', 1, '2021-02-28 07:59:09'),
(3, 'આઈસ્ક્રીમ & કોલ્ડ', 'f234102aab18a5a296e75d241b829d9446-460352_ice-cream-png-pluspng-ice-cream-images-hd.png', 1, '2021-01-30 23:23:36'),
(4, 'ઈલેક્ટ્રોનિક', 'c430a0eab056792a60b47bd0be82e634images.png', 1, '2021-01-30 23:28:06'),
(5, 'એગ્રો સેન્ટર', '26766e74e9cfbbe855ec673a0142768edfhdx.png', 1, '2021-01-30 23:38:29'),
(6, 'એન્જિનિયરિંગ ઈન્ડસ્ટ્રીઝ', '96c54a3e1288eaea0cbd0634bbac8b3a258-2583242_electronics-engineers-cartoon-png.png', 1, '2021-01-30 23:37:38'),
(7, 'ડાયમંડ ઇન્ડસ્ટ્રીઝ', '3bd29eee19432b9d9f8235271c27df91Diamond-Icon-PNG.png', 1, '2021-01-31 01:50:30'),
(8, 'ઓઈલ ઈન્ડસ્ટ્રીઝ', 'e2c239d2ae59f56324b73523b91bcebapng-transparent-petroleum-industry-gasoline-qatar-petroleum-drum-logo-barrel-oil.png', 1, '2021-01-30 23:41:41'),
(9, 'ઓટો પાટસૅ', '1df3fb83949615165527e08f16209fdd6504d02171d72304d880bbdf68697fcb.jpg', 1, '2021-01-31 01:50:13'),
(10, 'કટલેરી', '33ec66848f68463a019fd0b2100cd0a2253-2530236_make-up-png.png', 1, '2021-01-31 00:24:05'),
(11, 'સ્કૂલ/કોલેજ/ક્લાસિસ', '3ff8e4de01b9538760492a76f52bdb04unnamed.png', 1, '2021-01-30 23:17:17'),
(12, 'કલાકાર & ઓરકેસ્ટરા', '8fd9b3b794015bacf3cb16c9ec9750a7IMG_20210306_181737.jpg', 1, '2021-03-06 07:50:38'),
(13, 'Police department', '23acdfcae029bf9d51130a78bbb92ff7chikkaballapura-Police-Officer-icon.png', 1, '2021-01-30 23:21:53'),
(14, 'કાપડ ના વેપારી', 'f20ca186f52c838102cc1ca4391d7d12stack-clothing-icon-flat-white-background-vector-illustration-90976548.jpg', 1, '2021-01-31 00:40:58'),
(15, 'કરિયાણા & ટ્રેડિંગ', '002d340e2efe7aa91987ccbdd80443f9aaa.png', 1, '2021-02-05 23:57:28'),
(16, 'ગેરેજ', 'd29ba318310f646cfbf1c74b70e4a4ed59dfd8b96ae1d148d3057c8421589bed.jpg', 1, '2021-01-31 00:12:59'),
(17, 'જ્વેલરી', '5ce133ee5244a3979222587542845ca9img_83704.png', 1, '2021-01-31 00:17:03'),
(18, 'જીનિગ/સ્પિનીગ', 'f7df6b342ef8f1dc6040f0848447ae56IMG_20210226_203929.jpg', 1, '2021-02-28 07:58:46'),
(19, 'ટ્રાન્સપોર્ટ', 'a6bb32c37002a86baabf08b3c9cf84bffavpng_transport-vehicle-logo.png', 1, '2021-01-31 00:03:17'),
(20, 'ટુર્સ /ટ્રાવેલ્સ & ટુરિઝમ', '83ea5f19058e834105bfc406543caf9ea.png', 1, '2021-02-05 23:57:42'),
(21, 'પ્રિન્ટિંગ & સ્ટેશનરી', 'b6f4ae42337c85c7d7122ae65d57a2936655844.png', 1, '2021-01-31 01:46:56'),
(22, 'એન્જિનિયરિંગ & પ્લાનિંગ', 'eed5176b553aeaa39de6bc23199bfebcdownload.png', 1, '2021-01-30 23:34:56'),
(23, 'બિલ્ડીંગ કન્ટ્રકસ્ન & મટીરીયલ', 'e953d7a9351bb335b42fd38946157317123.png', 1, '2021-01-31 01:46:35'),
(24, 'બેન્ક & ફાઈનાન્સ', '63e6f96906c1a07fa8e763a72580aee8bank.png', 1, '2021-01-31 00:07:37'),
(25, 'મંડપ સર્વિસ & લાઈટ ડેકોરેશન', '776e5d4ca1f3f17e66afde542928114dThe-most-beautiful-gorgeous-mandap-decoration-ideas-we-came-across-1.png', 1, '2021-01-31 00:34:37'),
(26, 'રસોઈ & કેટરીન્ગ', '51b7e44140028d10a0c5ff557d8d3af1b457f066fc90c56a0192a695a4e42c1b.jpg', 1, '2021-02-05 23:50:47'),
(27, 'ગવર્મેન્ટ જોબ', 'b253da8878087b71bcb93bb3feb0366dicon.png', 1, '2021-01-31 01:45:28'),
(28, 'હીરા મજુરી કામ', '661a2602bbefe6598b5cdd2dc8229e32diamond.png', 1, '2021-01-31 00:26:51'),
(29, 'છુટક મજુરી કામ', '78e31ee29bfc516922ded4c00ec0d9fakisspng-carpenter-stock-photography-clip-art-woodworking-r-ampq-5d10a39f3c76b8.3306072515613715512477.png', 1, '2021-02-05 23:51:38'),
(30, 'ખેતી વાડી', '167462803537129c18a5de44d624afe8clipart1320600.png', 1, '2021-01-30 23:57:08'),
(31, 'મેડિકલ સ્ટોર્સ', '3d7d7d2bcf3be2fd3e537d105b51ecc7pngfind.com-medicine-png-images-6704562.png', 1, '2021-01-31 00:02:37'),
(32, 'વકીલ', 'ed65123014e200623762c90cca80b91claw-clipart-advocate-13.png', 1, '2021-01-31 00:06:36'),
(33, 'હોસ્પિટલ & લેબોરેટરી', 'ac528f1753db21309843460d2b1ec544hospitals-icon-13.jpg', 1, '2021-01-30 23:32:36'),
(34, 'હોટેલ & ફાસ્ટ ફૂડ', 'f9af8f7b5fa64ace94e8f0183f19c9e3kisspng-catering-food-computer-icons-logo-event-management-catering-5abf487cd18447.7546290415224853728582.png', 1, '2021-01-31 01:44:44'),
(35, 'સ્ટુડિયો & ફોટોગ્રાફી', 'c148d1b0be7aab52e2af947b166995ecpngtree-camera-photo-studio-blue-dotted-line-line-icon-png-image_1646612.jpg', 1, '2021-01-31 00:28:18'),
(36, 'હાર્ડવેર & પલાઈવુડ & કાચ', '7f6dda6e8996f9ff7ba3c93f62dd886cbundle-construction-tools-icons_24908-59860.jpg', 1, '2021-01-31 00:32:26'),
(37, 'વિમા એજન્ટ', 'f3df53cebdca32fbd9fc930389059c6615-159766_health-clip-art.png', 1, '2021-01-31 00:01:39'),
(38, 'રાજકીય આગેવાનો', '97ed089f0f5ead2151fa0f2b6144cac79796ac9ba625a8aa3a2bf6c1ebd9f1df.png', 1, '2021-01-31 00:39:25'),
(39, 'ફર્નિચર & કલર કામ', '952d373eae1700e42d3d9c420c901df9images.jpeg', 1, '2021-03-06 07:22:42'),
(40, 'ફળ / ફુલ / શાકભાજી ', '3f83b4189637253bc7e690eec712b84111766-NNM1YB_(1).png', 1, '2021-01-31 00:20:18'),
(41, 'ગિફ્ટ આર્ટીકલ', '1330a3d3096f5c8ee33e84b8d0fe56b1kisspng-line-gift-logo-clip-art-sinterklaas-5b33916a91b1a5.3178707815301062185968.png', 1, '2021-02-05 23:59:47'),
(42, 'પ્રેશ રીપોટૅર ', 'ab68da65db785d0cf1d92a80a2bfdec5images_(1).png', 1, '2021-01-31 00:10:51'),
(43, 'કોમ્પ્યુટર એન્જિનિયર', '266cd4d10079c9d63c81b6df2630f12e215-2158478_icon-computer-desktop-pc-png-image-my-computer-icon.png', 1, '2021-01-31 01:51:30'),
(44, 'CA/CS', '4ead83febd2431b39bd65424205e5b6clogo.png', 1, '2021-01-30 23:18:27'),
(45, 'એગ્રો ઈન્ડસ્ટ્રીઝ', 'add6fa22f25f906987894602adfd8142ol341u1ht27ph8cntfnthquq0l.png', 1, '2021-01-31 00:12:35'),
(46, 'કેમિકલ ઇન્ડસ્ટ્રીઝ', 'e6b9bba067d924de8460f40898b4df7e201607.png', 1, '2021-01-30 23:48:01'),
(47, 'સીરામીક & ટેક્ષ્ટાઈલ ઈન્ડસ્ટ્રીઝ', 'b0eccd8e20916b14594b6bd807e9c987tiles_floor_ceramic_interior-512.png', 1, '2021-01-31 00:29:48'),
(48, 'નેટવર્ક માર્કેટીંગ', 'c66e923f79163095f626e196aacd9bd9PngItem_5107618.png', 1, '2021-02-05 23:52:44'),
(49, 'IAS / IPS', '', -1, '2021-01-10 00:40:05'),
(50, 'વર્ગ 2 કમૅચારીઓ', '', -1, '2021-01-10 00:41:24'),
(51, 'ઈલેક્ટ્રીક એન્જિનિયર', '37404c4c3c2613af021f9b3f1e1eab5aIMG_20210226_203906.jpg', 1, '2021-02-28 07:57:24'),
(52, 'બ્યુટી પાર્લર', '2a2a14238c8debd5aac1d04cb7181935IMG_20210226_204015.jpg', 1, '2021-02-28 07:56:34'),
(53, 'પાન પાર્લર', '3bb7b1968355aa6b801fa11eccf43eedIMG-20210226-WA0300.jpg', 1, '2021-02-28 07:56:06'),
(54, 'હેન્ડીગ્રાફ', '41cc60158af2de3021fdd4bdd5c8ef72IMG-20200620-WA0112.jpg', 1, '2021-02-28 08:03:08'),
(55, 'માકૅટીગં યાર્ડ', 'a6d26e4799a525df35b6caf54217c16cIMG-20210227-WA0111.jpg', 1, '2021-02-28 07:53:49'),
(56, 'સામાજિક આગેવાન', 'd3bc19550744a49b93bfbc0030dbbcf3n1611314.png', 1, '2021-02-28 07:59:51'),
(57, 'નિવૃત્ત થઈ સામાજિક પ્રવૃત્તિઓ', 'eca5f830676bb28c4a269c56eb1ff0fdbusinessman-profile-cartoon_18591-58479.jpg', 1, '2021-02-28 08:00:33');

-- --------------------------------------------------------

--
-- Table structure for table `district_master`
--

CREATE TABLE `district_master` (
  `id` int(11) NOT NULL,
  `name` varchar(500) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `district_master`
--

INSERT INTO `district_master` (`id`, `name`, `status`, `date`) VALUES
(1, 'Amreli', -1, '2020-12-23 11:47:29'),
(2, 'Rajkot', 1, '2021-02-13 05:39:35'),
(3, 'Valsad', 1, '2021-01-22 00:39:06'),
(4, 'Tapi ', 1, '2021-01-07 07:34:06'),
(5, 'Navsari ', 1, '2021-01-07 07:34:35'),
(6, 'Narmada', 1, '2021-01-07 07:35:09'),
(7, 'Dang ', 1, '2021-01-07 07:35:39'),
(8, 'Bharuch ', 1, '2021-01-07 07:36:11'),
(9, 'Surat ', 1, '2021-01-07 07:36:43'),
(10, 'Kachchh ', 1, '2021-01-07 07:37:21'),
(11, 'Surendranagar', 1, '2021-01-07 07:37:47'),
(12, 'Porbandar ', 1, '2021-01-07 07:38:15'),
(13, 'Morbi ', 1, '2021-01-07 07:38:42'),
(14, 'Junagadh', 1, '2021-01-07 07:39:08'),
(15, 'Jamnagar ', 1, '2021-01-07 07:39:35'),
(16, 'Gir Somnath ', 1, '2021-01-07 07:40:03'),
(17, 'Dwarka', -1, '2021-01-07 07:40:32'),
(18, 'Devbhoomi Dwarka', 1, '2021-01-07 07:41:09'),
(19, 'Botad ', -1, '2021-01-07 07:46:41'),
(20, 'Bhavnagar ', 1, '2021-01-07 07:47:10'),
(21, 'Amreli', 1, '2021-01-07 07:47:40'),
(22, 'Sabarkantha ', 1, '2021-01-07 07:48:22'),
(23, 'Patan', 1, '2021-01-07 07:48:57'),
(24, 'Mehsana', 1, '2021-01-07 07:49:29'),
(25, 'Banaskantha', 1, '2021-01-07 07:49:58'),
(26, 'Aravalli', 1, '2021-01-07 07:50:29'),
(27, 'Gandhinagar ', 1, '2021-01-07 07:50:59'),
(28, 'Ahmedabad ', 1, '2021-01-07 07:51:24'),
(29, 'Panchmahal', 1, '2021-01-07 07:54:10'),
(30, 'Mahisagar ', 1, '2021-01-07 07:54:31'),
(31, 'Kheda ', 1, '2021-01-07 07:54:51'),
(32, 'Chhota Udaipur', 1, '2021-01-07 07:55:18'),
(33, 'Dahod', 1, '2021-01-07 07:55:42'),
(34, 'Anand', 1, '2021-01-07 07:56:01'),
(35, 'Vadodara', 1, '2021-01-07 07:59:33'),
(36, 'અન્ય', 1, '2021-01-08 01:20:16'),
(37, 'Botad', 1, '2021-01-08 02:35:57'),
(38, 'Rajasthan', 1, '2021-03-11 11:48:42'),
(39, 'Maharashtra', 1, '2021-03-11 11:49:05');

-- --------------------------------------------------------

--
-- Table structure for table `ebook`
--

CREATE TABLE `ebook` (
  `id` int(11) NOT NULL,
  `title` varchar(1000) NOT NULL,
  `document` varchar(1000) NOT NULL,
  `banner` varchar(1000) NOT NULL,
  `name` varchar(1000) NOT NULL,
  `uid` int(11) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ebook`
--

INSERT INTO `ebook` (`id`, `title`, `document`, `banner`, `name`, `uid`, `status`, `date`) VALUES
(1, 'સિંહ ગજૅના', '/b249e347efba9584827095c300a49638Sinhgarjna_book_by_mahesh_shiroya.pdf', '/3c0069394204dd0127bba901bf42282dIMG-20210312-WA0078.jpg', 'મહેશ સિરોયા', 0, 1, '2021-03-12 07:44:09'),
(2, 'કારકિર્દી માર્ગદર્શીકા', '/5146ab83a71806c66adc8386978a4175Karkirdi_Margardarshan_Visheshank-2021_JobOjas.com.pdf', '/9ea2b7702c8a071fe49a744ef12f055bIMG_20210622_134446.jpg', 'ગુજરાત સરકાર', 0, 1, '2021-06-22 04:27:34'),
(3, 'કારકિર્દી માર્ગદર્શીકા', '/4c3b571fd95438d84371594ac43d1937Karkirdi_Margardarshan_Visheshank-2021_JobOjas.com.pdf', '/9ea2b7702c8a071fe49a744ef12f055bIMG_20210622_134446.jpg', 'ગુજરાત સરકાર', 0, 1, '2021-06-22 04:28:20');

-- --------------------------------------------------------

--
-- Table structure for table `eventlog`
--

CREATE TABLE `eventlog` (
  `id` int(11) NOT NULL,
  `event_datetime` datetime NOT NULL,
  `uid` int(11) NOT NULL DEFAULT '-1',
  `event_type` varchar(50) NOT NULL DEFAULT 'ERROR',
  `event_title` varchar(200) DEFAULT '',
  `log_data` longtext NOT NULL,
  `ip` varchar(15) DEFAULT NULL,
  `user_agent` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `eventlog`
--

INSERT INTO `eventlog` (`id`, `event_datetime`, `uid`, `event_type`, `event_title`, `log_data`, `ip`, `user_agent`) VALUES
(1, '2021-02-13 05:20:14', 0, 'LOGIN_ATTEMPT', 'Login Failed', '{\"user\":\"9974010036\",\"password\":\"1q2w3e\"}', '49.34.209.157', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Safari/537.36'),
(2, '2021-02-13 05:20:19', 0, 'LOGIN_ATTEMPT', 'Login Failed', '{\"user\":\"9974010036\",\"password\":\"1q2w3e\"}', '49.34.209.157', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Safari/537.36'),
(3, '2021-02-13 05:21:28', 0, 'LOGIN_ATTEMPT', 'Login Failed', '{\"user\":\"9974010036\",\"password\":\"1q2w3e\"}', '49.34.209.157', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Safari/537.36'),
(4, '2021-02-13 05:21:28', 0, 'LOGIN_ATTEMPT', 'Login Failed', '{\"user\":\"9974010036\",\"password\":\"1q2w3e\"}', '49.34.209.157', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Safari/537.36'),
(5, '2021-02-13 05:21:31', 0, 'LOGIN_ATTEMPT', 'Login Failed', '{\"user\":\"9974010036\",\"password\":\"1q2w3e\"}', '49.34.209.157', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Safari/537.36'),
(6, '2021-02-13 05:21:31', 0, 'LOGIN_ATTEMPT', 'Login Failed', '{\"user\":\"9974010036\",\"password\":\"1q2w3e\"}', '49.34.209.157', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Safari/537.36'),
(7, '2021-02-13 05:21:32', 0, 'LOGIN_ATTEMPT', 'Login Failed', '{\"user\":\"9974010036\",\"password\":\"1q2w3e\"}', '49.34.209.157', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Safari/537.36'),
(8, '2021-02-13 05:21:34', 0, 'LOGIN_ATTEMPT', 'Login Failed', '{\"user\":\"9974010036\",\"password\":\"1q2w3e\"}', '49.34.209.157', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Safari/537.36'),
(9, '2021-03-06 08:51:20', 0, 'LOGIN_ATTEMPT', 'Login Failed', '{\"user\":\"8128500518\",\"password\":\"123\"}', '157.32.100.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.72 Safari/537.36 Edg/89.0.774.45'),
(10, '2021-03-06 08:51:24', 0, 'LOGIN_ATTEMPT', 'Login Failed', '{\"user\":\"8128500518\",\"password\":\"123\"}', '157.32.100.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.72 Safari/537.36 Edg/89.0.774.45'),
(11, '2021-03-12 12:03:29', 0, 'LOGIN_ATTEMPT', 'Login Failed', '{\"user\":\"8128500518\",\"password\":\"123\"}', '49.34.79.13', 'Mozilla/5.0 (Linux; Android 10; RMX1801) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.92 Mobile Safari/537.36'),
(12, '2021-03-24 09:52:37', 0, 'LOGIN_ATTEMPT', 'Login Failed', '{\"user\":\"8128500518\",\"password\":\"123\"}', '157.32.92.32', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36'),
(13, '2021-03-28 05:58:37', 0, 'LOGIN_ATTEMPT', 'Login Failed', '{\"user\":\"9974010036\",\"password\":\"12345\"}', '103.83.107.214', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1'),
(14, '2021-03-28 05:58:41', 0, 'LOGIN_ATTEMPT', 'Login Failed', '{\"user\":\"9974010036\",\"password\":\"12345\"}', '103.83.107.214', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1'),
(15, '2021-03-28 05:58:46', 0, 'LOGIN_ATTEMPT', 'Login Failed', '{\"user\":\"9974010036\",\"password\":\"12345\"}', '103.83.107.214', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1'),
(16, '2021-03-28 05:58:56', 0, 'LOGIN_ATTEMPT', 'Login Failed', '{\"user\":\"9974010036\",\"password\":\"12345\"}', '103.83.107.214', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1'),
(17, '2021-03-28 05:59:06', 0, 'LOGIN_ATTEMPT', 'Login Failed', '{\"user\":\"9974010036\",\"password\":\"12345\"}', '103.83.107.214', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1'),
(18, '2021-03-28 06:00:08', 0, 'LOGIN_ATTEMPT', 'Login Failed', '{\"user\":\"9974010036\",\"password\":\"12345\"}', '106.205.232.200', 'Mozilla/5.0 (Linux; Android 6.0.1; SM-A700FD) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.93 Mobile Safari/537.36'),
(19, '2021-03-28 07:57:29', 0, 'LOGIN_ATTEMPT', 'Login Failed', '{\"user\":\"9974010036\",\"password\":\"12345\"}', '103.83.107.214', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1'),
(20, '2021-03-28 08:19:23', 0, 'LOGIN_ATTEMPT', 'Login Failed', '{\"user\":\"9974010036\",\"password\":\"123\"}', '103.83.107.214', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1'),
(21, '2021-03-28 10:53:49', 0, 'LOGIN_ATTEMPT', 'Login Failed', '{\"user\":\"9974010036\",\"password\":\"12345\"}', '106.222.38.244', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1'),
(22, '2021-03-28 10:54:34', 0, 'LOGIN_ATTEMPT', 'Login Failed', '{\"user\":\"8128500518\",\"password\":\"12345\"}', '106.222.38.244', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1'),
(23, '2021-03-28 10:54:43', 0, 'LOGIN_ATTEMPT', 'Login Failed', '{\"user\":\"8128500518\",\"password\":\"123\"}', '106.222.38.244', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1'),
(24, '2021-03-28 10:54:57', 0, 'LOGIN_ATTEMPT', 'Login Failed', '{\"user\":\"8128500518\",\"password\":\"12345\"}', '106.222.38.244', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1'),
(25, '2021-03-28 10:55:05', 0, 'LOGIN_ATTEMPT', 'Login Failed', '{\"user\":\"8128500518\",\"password\":\"123456\"}', '106.222.38.244', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1'),
(26, '2021-03-29 01:21:52', 0, 'LOGIN_ATTEMPT', 'Login Failed', '{\"user\":\"9974010036\",\"password\":\"12345\"}', '106.222.38.244', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1'),
(27, '2021-03-29 01:21:53', 0, 'LOGIN_ATTEMPT', 'Login Failed', '{\"user\":\"9974010036\",\"password\":\"12345\"}', '106.222.38.244', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1'),
(28, '2021-03-30 02:28:11', 0, 'LOGIN_ATTEMPT', 'Login Failed', '{\"user\":\"+919974010036\",\"password\":\"123\"}', '157.32.88.120', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36 Edg/89.0.774.63'),
(29, '2021-03-30 02:28:16', 0, 'LOGIN_ATTEMPT', 'Login Failed', '{\"user\":\"8128500518\",\"password\":\"12345\"}', '157.32.88.120', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36 Edg/89.0.774.63'),
(30, '2021-05-03 05:30:47', 0, 'LOGIN_ATTEMPT', 'Login Failed', '{\"user\":\"9974010036\",\"password\":\"123456\"}', '103.83.105.222', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/77.0.3865.116 Mobile Safari/537.36 HeyTapBrowser/45.7.7.1'),
(31, '2021-05-03 05:30:48', 0, 'LOGIN_ATTEMPT', 'Login Failed', '{\"user\":\"9974010036\",\"password\":\"123456\"}', '103.83.105.222', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/77.0.3865.116 Mobile Safari/537.36 HeyTapBrowser/45.7.7.1'),
(32, '2021-05-07 09:03:53', 0, 'LOGIN_ATTEMPT', 'Login Failed', '{\"user\":\"\\u0aef\\u0aef\\u0aed\\u0aea\\u0ae6\\u0ae7\\u0ae6\\u0ae6\\u0ae9\\u0aec\",\"password\":\"12345\"}', '106.222.38.249', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/77.0.3865.116 Mobile Safari/537.36 HeyTapBrowser/45.7.7.1'),
(33, '2021-05-28 00:25:32', 0, 'LOGIN_ATTEMPT', 'Login Failed', '{\"user\":\"\",\"password\":\"\"}', '49.34.129.113', 'Mozilla/5.0 (Linux; Android 10; RMX1801) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.210 Mobile Safari/537.36'),
(34, '2021-06-04 01:08:39', 0, 'LOGIN_ATTEMPT', 'Login Failed', '{\"user\":\"8128500518\",\"password\":\"123\"}', '157.32.99.249', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36'),
(35, '2021-06-04 06:08:24', 0, 'LOGIN_ATTEMPT', 'Login Failed', '{\"user\":\"8128500518\",\"password\":\"123\"}', '157.32.126.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36'),
(36, '2021-06-10 06:34:17', 0, 'LOGIN_ATTEMPT', 'Login Failed', '{\"user\":\"8128500518\",\"password\":\"123\"}', '49.34.143.83', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.77 Safari/537.36'),
(37, '2021-06-10 06:51:53', 0, 'LOGIN_ATTEMPT', 'Login Failed', '{\"user\":\"8128500518\",\"password\":\"123\"}', '49.34.143.83', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.77 Safari/537.36'),
(38, '2021-06-19 02:07:19', 0, 'LOGIN_ATTEMPT', 'Login Failed', '{\"user\":\"8128500518\",\"password\":\"123\"}', '49.34.181.164', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.106 Safari/537.36'),
(39, '2021-06-20 07:22:29', 0, 'LOGIN_ATTEMPT', 'Login Failed', '{\"user\":\"8128500518\",\"password\":\"123\"}', '49.34.190.92', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.106 Safari/537.36'),
(40, '2021-06-20 07:22:31', 0, 'LOGIN_ATTEMPT', 'Login Failed', '{\"user\":\"8128500518\",\"password\":\"123\"}', '49.34.190.92', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.106 Safari/537.36'),
(41, '2021-06-22 00:53:06', 0, 'LOGIN_ATTEMPT', 'Login Failed', '{\"user\":\"8128500518\",\"password\":\"123\"}', '49.34.99.236', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.106 Safari/537.36'),
(42, '2021-06-22 01:47:12', 0, 'LOGIN_ATTEMPT', 'Login Failed', '{\"user\":\"8128500518\",\"password\":\"123\"}', '49.34.99.236', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.106 Safari/537.36'),
(43, '2021-06-22 06:38:46', 0, 'LOGIN_ATTEMPT', 'Login Failed', '{\"user\":\"8128500518\",\"password\":\"123\"}', '49.34.104.179', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.106 Safari/537.36'),
(44, '2021-06-24 01:01:40', 0, 'LOGIN_ATTEMPT', 'Login Failed', '{\"user\":\"9972010036\",\"password\":\"12345\"}', '27.61.205.122', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/77.0.3865.116 Mobile Safari/537.36 HeyTapBrowser/45.7.7.1'),
(45, '2021-06-25 07:52:44', 0, 'LOGIN_ATTEMPT', 'Login Failed', '{\"user\":\"8128500518\",\"password\":\"123\"}', '157.32.82.130', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36'),
(46, '2021-06-28 08:47:36', 0, 'LOGIN_ATTEMPT', 'Login Failed', '{\"user\":\"9973010036\",\"password\":\"12345\"}', '106.213.243.120', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/77.0.3865.116 Mobile Safari/537.36 HeyTapBrowser/45.7.7.1'),
(47, '2021-06-28 08:47:44', 0, 'LOGIN_ATTEMPT', 'Login Failed', '{\"user\":\"9973010036\",\"password\":\"9415\"}', '106.213.243.120', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/77.0.3865.116 Mobile Safari/537.36 HeyTapBrowser/45.7.7.1'),
(48, '2021-06-28 08:48:10', 0, 'LOGIN_ATTEMPT', 'Login Failed', '{\"user\":\"9974010036\",\"password\":\"9415\"}', '106.213.243.120', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/77.0.3865.116 Mobile Safari/537.36 HeyTapBrowser/45.7.7.1');

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE `feedback` (
  `id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `title` longtext NOT NULL,
  `notes` longtext NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `feedback`
--

INSERT INTO `feedback` (`id`, `uid`, `title`, `notes`, `status`, `date`) VALUES
(1, 1, 'Test', 'abcd', -1, '2021-03-17 10:16:05'),
(2, 2, 'Hamckfjeixxz', 'Hdfcxxશનધધઠઠઞઝદધનઞઞૉ', -1, '2021-03-24 02:00:02');

-- --------------------------------------------------------

--
-- Table structure for table `fund_type`
--

CREATE TABLE `fund_type` (
  `id` int(11) NOT NULL,
  `name` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `fund_type`
--

INSERT INTO `fund_type` (`id`, `name`, `status`, `date`) VALUES
(1, 'બાંધકામ પેટે', 1, '2021-03-11 09:59:32'),
(2, 'ભોજન સહાય', 1, '2021-03-11 09:59:09'),
(3, 'સમુહલગ્ન મહોત્સવ', 1, '2021-03-11 09:02:05'),
(4, 'એજ્યુકેશન સહાય', 1, '2021-03-11 10:00:04'),
(6, 'સેવા સહયોગ', 1, '2021-03-12 07:54:26'),
(10, 'બેંક ખાતા માં', 1, '2021-06-28 22:46:35');

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE `gallery` (
  `id` int(11) NOT NULL,
  `name` varchar(1000) NOT NULL,
  `banner` varchar(1000) NOT NULL,
  `uid` int(11) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `sender_type` enum('temple','dada','all') NOT NULL DEFAULT 'all',
  `status` int(1) NOT NULL DEFAULT '1',
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `gallery`
--

INSERT INTO `gallery` (`id`, `name`, `banner`, `uid`, `sender_id`, `sender_type`, `status`, `date`) VALUES
(1, 'પાટીદાર શૌક્ષણિક ભવન નું ખાત મુહૂર્ત, જસદણ', '365d8a18a0841d86bdc48c73c3aa5c70IMG20210204103425.jpg', 2, 2, 'all', 1, '2021-03-14 09:46:03'),
(2, 'Test', 'b7869d798de989e89ca647f14aad9903Screenshot_at_Feb_28_16-59-20.png', 1, 1, 'all', -1, '2021-03-09 09:04:03'),
(3, 'પાટીદાર શૌક્ષણિક ભવન-1', 'c9b02b59614078eac21288f76896fc51IMG_20210314_191356.jpg', 2, 2, 'all', 1, '2021-03-14 09:46:44'),
(4, 'સમાજ નાં આગેવાનો', '44fe3e3c2e2818cfe198c09d10072d52IMG-20210312-WA0043.jpg', 2, 2, 'all', 1, '2021-03-14 09:54:09'),
(5, 'સમાજ ભવન ની મુલાકાત', '460d5c907baaa792b0587b55ea695859IMG-20210311-WA0139.jpg', 2, 2, 'all', 1, '2021-03-14 09:59:37'),
(6, 'વિવિધ એસોસિયેશન જસદણ ', 'f1d931725e4599bb1e09f3f68833c57bIMG-20210323-WA0124.jpg', 2, 2, 'all', 1, '2021-03-27 12:25:23'),
(7, 'ભંડારીયા ગામ મીટીંગ', 'dad762df6a5c35e388bf05e50d1416fcIMG-20210323-WA0299.jpg', 2, 2, 'all', 1, '2021-03-23 13:56:33'),
(8, 'મિથીલીમ બ્લુ વિતરણ', 'af960d5a137da5559de9083f24a95a04IMG20210429094746.jpg', 2, 2, 'all', 1, '2021-04-29 10:29:02'),
(9, 'શીવરાજપુર મીટીંગ', 'c71bd17369b5bd503749676a7a3481fbIMG-20210429-WA0251.jpg', 2, 2, 'all', 1, '2021-04-29 11:53:58'),
(10, 'કોરોના વેકસીન કેમ્પ', '6a268df8f6ef0eb1ae0269121238ffc5IMG20210413114256.jpg', 2, 2, 'all', 1, '2021-04-30 02:25:08'),
(11, 'શ્રમ કાયૅ', 'd64b6ea93d8d5bd0f8a8e1a7352bf6dbIMG-20210511-WA0125.jpg', 2, 2, 'all', 1, '2021-05-11 09:24:19');

-- --------------------------------------------------------

--
-- Table structure for table `gallery_details`
--

CREATE TABLE `gallery_details` (
  `id` int(11) NOT NULL,
  `g_id` int(11) NOT NULL,
  `image` varchar(1000) NOT NULL,
  `type` enum('image','video') NOT NULL DEFAULT 'image',
  `status` int(1) NOT NULL DEFAULT '1',
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `gallery_details`
--

INSERT INTO `gallery_details` (`id`, `g_id`, `image`, `type`, `status`, `date`) VALUES
(2, 1, '4306a8e32d3f1a20d69a926af265fff6IMG20210213102909.jpg', 'image', -1, '2021-02-13 05:35:44'),
(3, 1, '403a98de643e016555120f582596f16aIMG-20210205-WA0044.jpg', 'image', -1, '2021-02-13 05:36:19'),
(4, 1, '403a98de643e016555120f582596f16aIMG-20210205-WA0044.jpg', 'image', -1, '2021-02-13 05:36:51'),
(5, 1, '65475522a7bb00919a8ba995fb8bbe74IMG-20210205-WA0051.jpg', 'image', -1, '2021-02-13 05:37:38'),
(6, 1, '1370ed6e7edea6b6749e8d2c3f7c3deeIMG20210204103425.jpg', 'image', -1, '2021-02-13 05:38:14'),
(7, 1, '2362b16c71eaed32fb67b97f1f9c2489WhatsApp_Image_2021-02-13_at_22.47.02.jpeg', 'image', -1, '2021-02-13 12:23:10'),
(8, 1, '', 'image', -1, '2021-03-12 02:34:44'),
(9, 1, 'f2a9e1de023fce66627d61a2a51e2f85IMG-20210312-WA0080.jpg', 'image', -1, '2021-03-12 02:35:18'),
(10, 1, '65958bdbf1f43e2d15449401f20d91d1IMG20210213102909.jpg', 'image', 1, '2021-03-12 02:35:46'),
(11, 1, '56853e8fc3729369fe0919f9684c0160IMG20210204103149.jpg', 'image', 1, '2021-03-12 02:36:09'),
(12, 1, 'ff33c9acaeaaa0f353293ccab1107daaIMG20210204101015.jpg', 'image', 1, '2021-03-12 02:36:33'),
(13, 1, '3eb2c42e17f1b1ab69f0076dd2b80b7bIMG-20210312-WA0037.jpg', 'image', -1, '2021-03-12 06:02:32'),
(14, 1, 'ea98da6b862e4186d6f5944ff4b0700cIMG-20210312-WA0027.jpg', 'image', -1, '2021-03-12 06:02:57'),
(15, 1, 'cdce1d3cd5b70fbd9d591100e3a701a0IMG-20210312-WA0046.jpg', 'image', -1, '2021-03-12 07:58:23'),
(16, 3, 'ebdd47400d8c7849892e4b14d4d1aad5IMG_20210314_191152.jpg', 'image', 1, '2021-03-14 09:47:03'),
(17, 3, 'fcc7b083c472465c6b7eac990c4cf9a0IMG_20210314_191213.jpg', 'image', 1, '2021-03-14 09:47:24'),
(18, 3, '777faec5fa1209bb99d68f95e4c2ccdfIMG_20210314_191228.jpg', 'image', 1, '2021-03-14 09:47:37'),
(19, 3, '791dc89cf933fa6c37e8264b171dd205IMG_20210314_191300.jpg', 'image', 1, '2021-03-14 09:47:52'),
(20, 3, '097d9cb603ae48b8c4e596602f7258cbIMG_20210314_191315.jpg', 'image', 1, '2021-03-14 09:48:05'),
(21, 3, '097d9cb603ae48b8c4e596602f7258cbIMG_20210314_191315.jpg', 'image', -1, '2021-03-14 09:48:16'),
(22, 3, 'f82f00d7676a87dca05de860061aaf90IMG_20210314_191356.jpg', 'image', 1, '2021-03-14 09:48:51'),
(23, 3, '3b33674d424659dddf9f290635d0283cIMG_20210314_191338.jpg', 'image', 1, '2021-03-14 09:49:08'),
(24, 1, '', 'image', -1, '2021-03-14 09:50:22'),
(25, 1, 'c0bf8dfb759760063ad7b139128a775bIMG20210204104126.jpg', 'image', 1, '2021-03-14 09:50:55'),
(26, 1, 'b5c30207c07c9cc9254638c397557c3dIMG20210204103149.jpg', 'image', 1, '2021-03-14 09:51:27'),
(27, 1, 'dca62987a8aebc760bb64e7d9175cacaIMG20210204104113.jpg', 'image', 1, '2021-03-14 09:52:05'),
(28, 4, 'a67d53c4524a5f380bb6719eb0c10aafIMG-20210312-WA0046.jpg', 'image', 1, '2021-03-14 09:54:56'),
(29, 4, 'c45fa395149419effd45b49aaa3b31fcIMG-20210312-WA0045.jpg', 'image', 1, '2021-03-14 09:56:03'),
(30, 4, '987257e51bc8479cff673ca7a153faecIMG-20210312-WA0038.jpg', 'image', 1, '2021-03-14 09:56:20'),
(31, 4, '6d7e86c8a2aa05019ec7cd188ea298c3IMG-20210312-WA0027.jpg', 'image', 1, '2021-03-14 09:58:03'),
(32, 5, 'a03d399286a7bddd350d8693e74d6999IMG-20210308-WA0053.jpg', 'image', 1, '2021-03-14 10:02:44'),
(33, 5, '8940a194239de3498fb97383b050f696IMG-20210311-WA0138.jpg', 'image', 1, '2021-03-14 10:03:44'),
(34, 5, 'e474c676b4d5b526660a7d54d6739774IMG-20210214-WA0065.jpg', 'image', -1, '2021-03-14 10:07:18'),
(35, 5, 'e474c676b4d5b526660a7d54d6739774IMG-20210214-WA0065.jpg', 'image', 1, '2021-03-14 10:07:30'),
(36, 5, 'c6b58bec910c26e3e81314cab0faccb6IMG20210213102911.jpg', 'image', 1, '2021-03-14 10:08:02'),
(37, 5, 'c3f1447ca75282272fe484ddf997fc6eIMG_20210315_183552.jpg', 'image', 1, '2021-03-15 09:07:12'),
(38, 5, '2bca3caa67b7ba12b0c7c4459688b43aIMG20210315195344.jpg', 'image', 1, '2021-03-15 10:28:19'),
(39, 5, '4a33a4d0561a480c38b0ed2e3f2568b9IMG-20210316-WA0062.jpg', 'image', 1, '2021-03-16 02:45:53'),
(40, 4, 'b045fd93d3b5eec38b97ed64f1f1b912IMG-20210316-WA0069.jpg', 'image', 1, '2021-03-16 03:24:15'),
(41, 5, '', 'image', -1, '2021-03-16 08:10:30'),
(42, 6, 'b36225c4862325da90c3c17df12f4929IMG-20210323-WA0124.jpg', 'image', 1, '2021-03-23 13:55:02'),
(43, 6, 'e54e8d2c8645339aa6637eda88a56b4dIMG-20210323-WA0120.jpg', 'image', 1, '2021-03-23 13:55:26'),
(44, 6, '45e19aaf1b8a983f7a06adcb738e41d6IMG-20210323-WA0115.jpg', 'image', 1, '2021-03-23 13:55:49'),
(45, 7, '3cb30f5fcd3534e7733a9285fb0cdbe2IMG-20210323-WA0299.jpg', 'image', 1, '2021-03-23 13:57:01'),
(46, 7, '55f19430c2eade5b419d7889897e1098IMG-20210323-WA0299.jpg', 'image', -1, '2021-03-23 13:57:22'),
(47, 7, '55f19430c2eade5b419d7889897e1098IMG-20210323-WA0299.jpg', 'image', -1, '2021-03-23 13:57:35'),
(48, 5, '2cd5cffa9d3d6227880e8bb366f6fae4IMG-20210325-WA0174.jpg', 'image', 1, '2021-03-25 03:49:15'),
(49, 4, '74e7f1a83c242c12b34f72b1c88750d0IMG-20210325-WA0086.jpg', 'image', 1, '2021-03-25 10:35:41'),
(50, 5, '7ab672134c082cccb5a1f7f782e65036IMG-20210325-WA0096.jpg', 'image', 1, '2021-03-25 10:36:08'),
(51, 5, '175bddeec797629b897bb644be1af602IMG-20210325-WA0084.jpg', 'image', 1, '2021-03-25 10:36:37'),
(52, 5, 'eaa304728ce4217f167db86ac7f4ee89IMG-20210325-WA0077.jpg', 'image', 1, '2021-03-25 10:36:50'),
(53, 7, '79eb2f231998f12cc57d0eac2da4b19aIMG-20210323-WA0290.jpg', 'image', 1, '2021-03-25 10:47:13'),
(54, 6, '03595bb267cad29e57715a09bc014f57IMG20210327201217.jpg', 'image', 1, '2021-03-27 12:26:00'),
(55, 6, '4eb13d568c22d2b7083315cd5402e331IMG20210327201220.jpg', 'image', 1, '2021-03-27 12:26:20'),
(56, 6, '2cd06e61cda862b76553bbbdf6985283IMG-20210327-WA0059.jpg', 'image', 1, '2021-03-27 12:27:50'),
(57, 6, '4ffe9839e61aec65c9c8b9864d64ade6IMG-20210327-WA0063.jpg', 'image', 1, '2021-03-27 12:28:16'),
(58, 8, '67817e3d11d3737d503adfaa6cc18901IMG20210429094817.jpg', 'image', 1, '2021-04-29 10:25:48'),
(59, 8, '64106e61da61199ad9c2a3d056da874dIMG20210429094810.jpg', 'image', 1, '2021-04-29 10:26:14'),
(60, 8, '8bfc44baf0febd08e3beb23c1a8a56beIMG20210429094803.jpg', 'image', 1, '2021-04-29 10:26:39'),
(61, 8, 'a7a4036d13bc5bd4fb4c65ca96ea9afcIMG20210429094738.jpg', 'image', 1, '2021-04-29 10:27:06'),
(62, 8, '7e2b356a487cd74be1f987669131b746IMG20210429090145.jpg', 'image', 1, '2021-04-29 10:27:40'),
(63, 8, 'f3d4593d4aa7b790163eceadfc0fed6cIMG20210429094746.jpg', 'image', 1, '2021-04-29 10:28:03'),
(64, 9, '981d5a025b970200f6dbd8ef1e9cdc22IMG-20210429-WA0251.jpg', 'image', 1, '2021-04-29 11:54:20'),
(65, 9, 'e407cbeaaa772155d94e8ff92e2517bbIMG-20210429-WA0250.jpg', 'image', 1, '2021-04-29 11:54:34'),
(66, 9, '640af452376d91991606b3e2984bf639IMG-20210429-WA0248.jpg', 'image', 1, '2021-04-29 11:54:45'),
(67, 10, 'e6bbd3ae348700e941a7fa9be08a7966IMG-20210407-WA0152.jpg', 'image', 1, '2021-04-30 03:30:46'),
(68, 10, 'a5bb8c6ba0113fa218ddfa255258d49dIMG-20210430-WA0129.jpg', 'image', 1, '2021-04-30 03:30:57'),
(69, 10, 'f201e45ddab129f8779c313fdcfc5bc1IMG-20210430-WA0130.jpg', 'image', 1, '2021-04-30 03:31:11'),
(70, 11, '227dbea66c753a927e9180129d6c18c9IMG-20210511-WA0301.jpg', 'image', -1, '2021-05-11 09:24:42'),
(71, 11, '8701b935b2a2bcb9fa179148f09dd863IMG-20210511-WA0304.jpg', 'image', -1, '2021-05-11 09:24:57'),
(72, 11, '1ad750cad79bfa8fc3bf24e2c3226c24IMG-20210511-WA0125.jpg', 'image', -1, '2021-05-11 09:25:10'),
(73, 11, '21240bff277d786762ad6926e25c7257IMG-20210510-WA0314.jpg', 'image', -1, '2021-05-11 09:25:55'),
(74, 11, '35669dc1a840dc0e1046a3f3cadf9d0aIMG-20210510-WA0322.jpg', 'image', -1, '2021-05-11 09:26:19'),
(75, 11, 'b10eafe3dee94094c90fe6aa1b055b18IMG-20210510-WA0324.jpg', 'image', -1, '2021-05-11 09:26:42'),
(76, 5, '6c89491adcc8656eea65603bb3348834IMG20210511131053.jpg', 'image', 1, '2021-05-11 09:27:18'),
(77, 2, 'https://youtu.be/vAtCAEg-gKc', 'video', 1, '2021-06-04 01:04:32'),
(78, 11, '8824d39074bb4a8be21f2d1e93504755IMG20210603104020.jpg', 'image', 1, '2021-06-04 01:45:08'),
(79, 11, 'cc3ee1d620f3d7e7143ec8790f76dc08IMG20210603193104.jpg', 'image', 1, '2021-06-04 01:46:16'),
(80, 11, '9dbab43611fe14917521539420df87ddIMG20210503172712.jpg', 'image', 1, '2021-06-04 01:47:30'),
(81, 5, '', 'image', 1, '2021-06-10 05:29:04');

-- --------------------------------------------------------

--
-- Table structure for table `gov_document`
--

CREATE TABLE `gov_document` (
  `id` int(11) NOT NULL,
  `title` varchar(1000) NOT NULL,
  `name` varchar(1000) NOT NULL,
  `notes` longtext NOT NULL,
  `document` varchar(1000) NOT NULL,
  `office` varchar(1000) NOT NULL,
  `link` varchar(1000) NOT NULL,
  `type` enum('sahay','gov') NOT NULL DEFAULT 'sahay',
  `uid` int(11) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `gov_document`
--

INSERT INTO `gov_document` (`id`, `title`, `name`, `notes`, `document`, `office`, `link`, `type`, `uid`, `status`, `date`) VALUES
(1, 'ભોજનબીલ સહાય,', '', '૧. અરજીબીલ,\r\n૨. આધારકાર્ડ,\r\n૩.  બિન અનામત વર્ગનું પ્રમાણપત્ર,\r\n૪. આવકનું પ્રમાણપત્ર (દાખલો),\r\n૫. રહેઠાણનો પુરાવો,\r\n૬. જન્મતારીખ નો દાખલો અથવા લિવિંગસર્ટિ,\r\n૭. ધોરણ ૧૨ અથવા છેલ્લા વર્ષની માર્કશીટ,\r\n૮. બેંકપાસબૂક ની નકલ\r\n૯. શાળા/કોલેજ નો એડમિશન લેટર,\r\n૧૦.હોસ્ટેલ/સમાજ/ટ્રષ્ટ/સંસ્થા સંચાલિત છે તો તેનો પુરાવો,', '/67d0d5939d605d882edc5448d8db78cfBhojanbill.pdf', '', 'https://gueedc.gujarat.gov.in/food-bill-scheme.html', 'sahay', 0, 1, '2021-01-31 23:19:28'),
(2, 'બિનઅનામત વર્ગ નું પ્રમાણપત્ર અને EWS 10% અનામતનાં પ્રમાણપત્ર,', '', '૧. અરજદારનું શાળા છોડીયાનું પ્રમાણપત્ર\r\n    સક્ષમ અધિકારીને જણાયતો દાદા/પીતા/કાકા/ફોઇ પૈકી કોઈ એક નો\r\n    જાતિ પુરવાર થાય તેવો દસ્તાવેજ માગીશકાય:,\r\n૨.  રહેઠાણ ના પુરાવા માટે નીચે પૈકી કોઈ એક :\r\n     રેશનકાર્ડ/આધારકાર્ડ/લાઈટબીલ/ચૂંટણીકાર્ડ/પાનકાર્ડ/ડ્રાવિંગલાઇસન્સ/\r\n૩.  અરજદાર પુખ્તવયનો હોયતો તેમનું સ્વયંનું નિયત નમૂનાનું સોગંદનામું\r\n૪. અરજદાર સગીર હોય તો પિતાનું અને પિતા ના હોય તો માતાનું\r\n     નિયત નમૂનાનું સોગંદનામું,\r\n૫. પાસપોર્ટ સાઈજના ફોટા ૨,\r\n૬. આવકનો દાખલો :\r\n    આરજદારના કુટુંબ તમામ સ્ત્રોત મળીને કુલ આવક રૂપીયા ૮ લાખ કરતાં ઓછી\r\n    હોવી જોઈએ (આરજદારના કુટુંબના તમામ સભ્યોની વાર્ષિક આવકના પુરાવા),\r\n૭. આરજદાર સ્વયં ખેતી ધારણ કરતો હોય તો ૭/૧૨ તથા કુટુંબના અન્ય સભ્યો\r\n    ખેતી ધારણ કરતો હોયતો રજુ કરવી તથા ખેતીની આવક દર્શાવતા પુરાવા\r\n    રજુ કરવા,\r\n૮. સક્ષમ અધિકારઓ/ અપીલ અધિકારીઓ જરૂર જણાયતો વધારાના અન્ય \r\n     આધાર પુરાવા પણ માગી શકે  \r\n     \r\n', '/b9f8b198bfa1dc4fc8d30833f6ffc514Bin_Anamat_certificate.pdf', '', 'https://sje.gujarat.gov.in', 'sahay', 0, 1, '2021-02-02 03:23:11'),
(3, 'આધારકાર્ડ મેળવવા માટે .', '', '૧. આઈ. ડી. પ્રુફ નીચે પૈકી એક,\r\n૨. પાન કાર્ડ,\r\n૩. ચૂંટણી કાર્ડ, \r\n૪.  ડ્રાયવીંગ લાઇસન્સ, \r\n૫. પાસપોર્ટ,\r\n૬. એડ્રેસ પ્રુફ નીચે પૈકી એક,\r\n૭. રેશનકાર્ડ/ લાઇટબિલ/ગેસની બૂક,\r\n૮. એડ્રેસ પ્રુફ નીચે પૈકી એક\r\n     રેશન કાર્ડ/ લાઈટ બીલ/ ગેસ ની બુક  \r\n', '/abefa0e3c00efb00b8442374a51e8080Adhar_Card.pdf', '', 'https://udai.gov.in/', 'gov', 0, 1, '2021-02-04 04:21:40'),
(4, 'નોન કીમીલીયર સર્ટી મેળવવા માટે,', '', '૧. ફોર્મ અને ફોટો,\r\n૨. રેશનકાર્ડ ની નકલ,\r\n૩. જાતિનો દાખલો,\r\n૪. આવકનો દાખલો,\r\n૫. સ્કૂલ લીવીંગ સર્ટી,\r\n૬. વાલીનું આવકનું સોગંધનામું,\r\n૭. છેલુ લાઇટબિલ,', '/11fc144781303f61a248dd4417d2061eNON_CRIMELIYER_SARTI_.pdf', '', 'https://orbitcareers.com/character-certificate/', 'gov', 0, 1, '2021-02-04 00:27:43'),
(5, 'જાતિનો દાખલા મેળવવા માટે,', '', '૧. ફ્રોમ અને ફોટા,\r\n૨. રેશનકાર્ડની નકલ,\r\n૩. સ્કૂલ લિવિંગસર્ટી,\r\n૪. પિતા/ભાઈ/બહેનનું લિવિંગસર્ટી,\r\n૫. છેલું લાઇટબિલ,\r\n૬. તલાટીનો દાખલો,', '/14d305cd3735ab38444b1f303824bc5eJATI_NU_PRAMANPTRA.pdf', '', 'https://www.digitalgujarat.gov.in', 'gov', 0, 1, '2021-02-04 22:14:38'),
(6, 'ડોમિસાઇલ સર્ટી મેળવવા માટે,', '', '૧. ફોર્મ અને ફોટા,\r\n૨. રેશનકાર્ડની નકલ,\r\n૩. સ્કૂલ લિવિંગસર્ટી,\r\n૪. તલાટીનો ૧૦ વર્ષનો રહેઠાણનો દાખલો ,\r\n૫. રહેઠાણ અંગેનું સોગંદનામું,\r\n૬. છેલું લાઇટબિલ,\r\n૭. જન્મનો દાખલો,\r\n૮. પોલીસ સ્ટેશનનો દાખલો,', '/0c3f0b45fb7f01100a00f641a272d4e3DOMISAIEL_SARTI_1.pdf', '', 'https://www.digitalgujarat.gov.in/Citizen/CitizenService.aspx', 'gov', 0, 1, '2021-02-04 05:12:00'),
(7, 'સીનીયર સીટીજન સર્ટી,', '', '૧. ફોર્મ અને ફોટા,\r\n૨. રેશનકાર્ડની નકલ,\r\n૩. સ્કૂલ લિવિંગસર્ટી,\r\n૪. જન્મનો દાખલો,\r\n૫. ચૂંટણીકાર્ડ ની નકલ,\r\n૬. છેલું લાઇટબિલ,\r\n', '/64016541b8e88bdf0377fb3b4f4cf46eSenior_Citizen_Certifiate.pdf', '', 'https://www.digitalgujarat.gov.in/Citizen/CitizenService.aspx', 'gov', 0, 1, '2021-02-04 05:12:35'),
(8, 'આવકનો દાખલો મેળવવા માટે,', '', '૧. ફોર્મ અને ફોટા,\r\n૨. રેશનકાર્ડની નકલ,\r\n૩. ચુંટણીકાર્ડની નકલ,\r\n૪. તલાટીનો આવકનો દાખલો,\r\n૫. છેલું લાઇટબિલ,', '/9e342df31379a486d0748b0134a5bb1dAavak_No_Dakhlo_2.pdf', '', 'https://kachchh.nic.in/gu/form', 'gov', 0, 1, '2021-01-31 22:32:49'),
(9, 'ધાર્મિક લઘુમતીનો દાખલો,', '', '૧. ફોર્મ અને ફોટો,\r\n૨. રેશનકાર્ડની નકલ,\r\n૩. સ્કૂલ લિવિંગસર્ટી,\r\n૪. તલાટીનો આવકનો દાખલો,\r\n૫. છેલું લાઇટબિલ,\r\n૬. સ્કૂલનાં આચાર્યનો લેટરપેડ પર દાખલો,', '/172cd90bb2be976e60a3047770761bf5DHARMIK_LAGHUMATI.pdf', '', 'digitalguarat.gov.in/', 'gov', 0, 1, '2021-02-04 06:03:55'),
(10, 'ચારિત્ર્યનો દાખલો,', '', '૧. ફોર્મ અને ફોટો,\r\n૨. રેશનકાર્ડની નકલ,\r\n૩. સ્કૂલ લિવિંગસર્ટી,\r\n૪. તલાટીનો ચારીત્રયનો દાખલો,\r\n૫. છેલું લાઇટબિલ,\r\n૬. પોલીસ સ્ટેશનનો દાખલો,\r\n૭. ચુંટણીકાર્ડની નકલ,', '/866ac18c875f73dcdbc48413978b853bFinal_Certificate_Format_for_MTRA.pdf', '', '', 'gov', 0, -1, '2021-01-29 05:29:27'),
(11, 'વિધવા સહાય મેળવવા માટે,', '', '૧. ફોર્મ અને ફોટો,\r\n૨. રેશનકાર્ડની નકલ,\r\n૩. સ્કૂલ લિવિંગસર્ટી,\r\n૪. પતિનાં મૃત્યુનો દાખલો,\r\n૫. ચુંટણીકાર્ડની નકલ,\r\n૬. છેલું લાઇટબિલ,\r\n૭. સંતાનો નાં જન્મ તારીખનાં દાખલા,', '/b43a8d354caeb6d601f9f476f82d50b0vidhava_sahay_app_form_2.pdf', '', 'https://indiascheme.com/vidhva-sahay-yojana/', 'gov', 0, 1, '2021-02-03 23:27:17'),
(12, 'મા અમૃતમ કાર્ડ- મા વાત્સલયકાર્ડ,', '', '૧. ચુંટણીકાર્ડની નકલ,\r\n૨. રેશનકાર્ડની નકલ,\r\n૩. આધારકાર્ડ,\r\n૪. આવકનો દાખલો,', '/6ce866b7cc6ae797c68eb86e5c3116cbVATSLY_CARD.pdf', '', 'http://www.magujarat.com/', 'gov', 0, 1, '2021-02-04 05:55:19'),
(13, 'વયવંદના યોજના,', '', '૧. ચુંટણીકાર્ડની નકલ,\r\n૨. રેશનકાર્ડની નકલ,\r\n૩. આધારકાર્ડ,\r\n૪. સ્કૂલ લિવિંગસર્ટી,\r\n૫. ૦ થી ૧૬ નો તલાટીનો બીપીએલ નો દાખલો,\r\n૬. બેંકપાસબૂકની નકલ,', '/d83ffdbf8112c839c1409a23de00add2indira-gandhi-pension-02012020.pdf', '', 'https://sje.gujarat.gov.in/dsd/form', 'gov', 0, 1, '2021-01-30 04:53:34'),
(14, 'નિરાધાર વૃદ્ધસહાય યોજના,', '', '૧. ચુંટણીકાર્ડની નકલ,\r\n૨. રેશનકાર્ડની નકલ,\r\n૩. આધારકાર્ડ,\r\n૪. સ્કૂલ લિવિંગ સર્ટી,\r\n૫. બેંક પાસબુકની નકલ,', '/ada23ad9acb6479f49e3397a31c45832VRUDH_PENTION_YOJNA_2.pdf', '', 'https://sje.gujarat.gov.in/dsd/form', 'gov', 0, 1, '2021-02-01 02:00:37'),
(15, 'અલગ રેશનકાર્ડ કારવવા માટે', '', '૧. ફોર્મ અને ફોટો,\r\n૨. ચુંટણીકાર્ડની નકલ,\r\n૩. જુના રેશનકાર્ડની નકલ,\r\n૪. આધારકાર્ડ,\r\n૫. આવકનો દાખલો,\r\n૬. બેંક પાસબુકની નકલ,\r\n૭. તલાટીનો દાખલો,\r\n૮. પિતાનું સમંતી સોંગદનામું,\r\n૯. લાઇટબિલ,', '/866ac18c875f73dcdbc48413978b853bFinal_Certificate_Format_for_MTRA.pdf', '', 'https://mysy.guj.nic.in/', 'gov', 0, -1, '2021-01-29 05:16:08'),
(16, 'નવા રેશનકાર્ડ માટે,', '', '૧. ફોર્મ અને ફોટા,\r\n૨. ચૂંટણીકાર્ડની નકલ,\r\n૩. નામ કમીનો  દાખલો,\r\n૪. આધારકાર્ડ,\r\n૫. બેંક પાસબૂકની નકલ,\r\n૬. તલાટીનો દાખલો,\r\n૭. લાઇટબિલ,', '/1d37cb59baa3c6ecca99f70e51c34ab2Namuna1.pdf', '', '', 'gov', 0, -1, '2021-01-29 05:20:25'),
(17, 'રેશનકાર્ડમાં નામ દાખલ કરાવવામાટે,', '', 'કમી૧. ફોર્મ અને ફોટા,\r\n૨. ચૂંટણીકાર્ડની નકલ,\r\n૩. નામ કમીનો  દાખલો,\r\n૪. આધારકાર્ડ,\r\n૫. જન્મનો દાખલો,\r\n૬. પત્નીનું નામ દાખલ કરવાનું\r\n     હોયતો પિયરપક્ષ માંથી નામ નો દાખલો,', '/1d37cb59baa3c6ecca99f70e51c34ab2Namuna1.pdf', '', 'https://dcs-dof.gujarat.gov.in/ration-card.htm', 'gov', 0, -1, '2021-01-30 01:16:54'),
(18, 'માં અન્નપુર્ણા યોજના,', '', '૧. ફોર્મ અને ફોટા,\r\n૨. ચૂંટણીકાર્ડની નકલ,\r\n૩. રેશનકાર્ડની નકલ,\r\n૪. આધારકાર્ડ,\r\n૫. આવકનો દાખલો, \r\n૬. બેંક પાસબૂકની નકલ, ', '/1d37cb59baa3c6ecca99f70e51c34ab2Namuna1.pdf', '', '', 'gov', 0, -1, '2021-01-29 05:26:44'),
(19, 'માં અન્નપુર્ણા યોજના,', '', '૧. ફોર્મ અને ફોટા,\r\n૨. ચૂંટણીકાર્ડની નકલ,\r\n૩. રેશનકાર્ડની નકલ,\r\n૪. આધારકાર્ડ,\r\n૫. આવકનો દાખલો, \r\n૬. બેંક પાસબૂકની નકલ, ', '/9a926a5de972d9902f80ae7b8fdec655MA_ANPURNA_PDF_FORM.pdf', '', '', 'gov', 0, 1, '2021-02-03 23:13:32'),
(20, 'રેશનકાર્ડ નામ દાખલ કરાવવા,', '', '૧. ફોર્મ અને ફોટા,\r\n૨. ચૂંટણીકાર્ડની નકલ,\r\n૩. નામ કમીનો  દાખલો,\r\n૪. આધારકાર્ડ,\r\n૫. જન્મનો દાખલો,\r\n૬. પત્નીનું નામ દાખલ કરવાનું\r\n     હોયતો પિયરપક્ષ માંથી નામ નો દાખલો,', '/d2d2029d6fbdc66518ff49e91f1b15aaRetion_Card.pdf', '', 'https://dcs-dof.gujarat.gov.in/ration-card.htm', 'gov', 0, 1, '2021-02-05 23:42:25'),
(21, 'નવા રાશનકાર્ડ માટે,', '', '૧. ફોર્મ અને ફોટા,\r\n૨. ચૂંટણીકાર્ડની નકલ,\r\n૩. નામ કમીનો  દાખલો,\r\n૪. આધારકાર્ડ,\r\n૫. બેંક પાસબૂકની નકલ,\r\n૬. તલાટીનો દાખલો,\r\n૭. લાઇટબિલ,', '/f94a4f71a252733d2209f190fd71ebe8Application_For_New_Ration_Card.pdf', '', 'https://dcs-dof.gujarat.gov.in/ration-card.htm', 'gov', 0, 1, '2021-02-05 23:04:34'),
(22, 'અલગ રાશનકાર્ડ કરાવવા માટે,', '', '૧. ફોર્મ અને ફોટો,\r\n૨. ચુંટણીકાર્ડની નકલ,\r\n૩. જુના રેશનકાર્ડની નકલ,\r\n૪. આધારકાર્ડ,\r\n૫. આવકનો દાખલો,\r\n૬. બેંક પાસબુકની નકલ,\r\n૭. તલાટીનો દાખલો,\r\n૮. પિતાનું સમંતી સોંગદનામું,\r\n૯. લાઇટબિલ,', '/62cc1eb7a55f79f2d11f649dd6323ed7Application_For_Separate_Ration_Card.pdf', '', 'https://dcs-dof.gujarat.gov.in/ration-card.htm', 'gov', 0, 1, '2021-02-04 05:07:35'),
(23, 'ચારીત્રયનો દાખલો મેળવવા,', '', '૧. ફોર્મ અને ફોટો,\r\n૨. રેશનકાર્ડની નકલ,\r\n૩. સ્કૂલ લિવિંગસર્ટી,\r\n૪. તલાટીનો ચારીત્રયનો દાખલો,\r\n૫. છેલું લાઇટબિલ,\r\n૬. પોલીસ સ્ટેશનનો દાખલો,\r\n૭. ચુંટણીકાર્ડની નકલ,', '/3ed0169549445fac2954a36c98e111a5Character_Certificate_1.pdf', '', 'https://digitalgujarat.gov.in/', 'gov', 0, 1, '2021-02-05 23:40:46'),
(24, 'ધોરણ 10 અને 12 ની માર્કશીટ અથવા કોઈ પ્રમાણપત્ર ખોવાઇ ગયેલ હોય.', '', 'ધોરણ 10 અને 12 ની માર્કશીટ અથવા કોઈ પ્રમાણપત્ર ખોવાઇ ગયેલ હોય તો હવેથી ઓનલાઈન મેળવી શકાશે. વર્ષ 1952 થી અત્યાર સુધીના બધા જ પ્રમાણપત્ર ઓનલાઇન મુકી દેવામાં આવ્યા છે.\r\n\r\nતમારું કોઈ પણ પ્રમાણપત્ર ખોવાયેલા હોય અથવા તો તૂટી ગયેલા હોય તો ડાઉનલોડ કરી શકો છો.\r\n\r\n', '', '', 'https://www.gsebeservice.com/ ', 'sahay', 0, 1, '2021-01-30 06:55:56'),
(25, 'કોચીંગ સહાય,		', '', 'ધોરણ ૧૦ માં ૭૫% મેળવેલ હોય અને કોચીંગકલાસ ભણતા ધોરણ ૧૧ અને ધોરણ ૧૨ ના વિજ્ઞાન પ્રવાહમાં અભ્યાસ કરતાં વિધાર્થીઓ માટે,\r\n૧. અરજી પત્રક,\r\n૨. આધારકાર્ડ,\r\n૩. બિન અનામતનું પ્રમાણપત્ર,\r\n૪. આવકનું પ્રમાણપત્ર (આવક મર્યાદા કુટુંબની ૪.૫૦લાખ),\r\n૫. જન્મનું પ્રમાણપત્ર/લિવિંગસર્ટિફિકેટ,\r\n૬. પ્રિન્સિપાલનું વિધાર્થીનું ચાલુ અભ્યાસ અંગેનું સર્ટિફિકેટ,\r\n૭. બેંક પાસબૂકની નકલ,\r\n૮. ધોરણ ૧૦ ની માર્કશીટ,\r\n૯. ટયુસન ક્લાસની વિગત(ભરેલ અથવા ભરવાપાત્ર ફી સાથે).', '/0dac12ac71bf574468be01cd4264b3d4Koching_Class_.pdf', '', 'https://gueedc.apphost.in/', 'sahay', 0, 1, '2021-02-06 04:35:53'),
(26, 'JEE, GUJCET, NEET  પરીક્ષા માટે કોચીંગ સહાય,', '', '૧. અરજી પત્રક,\r\n૨. આધારકાર્ડની નકલ,\r\n૩. બિન અનામતનું પ્રમાણપત્ર,\r\n૪. આવકનું પ્રમાણપત્ર (આવક મર્યાદા કુટુંબની ૪.૫૦લાખ),\r\n૫. જન્મનું પ્રમાણપત્ર/લિવિંગસર્ટિફિકેટ,\r\n૬. રહેઠાણનો પુરાવો,\r\n૭. બેંક પાસબૂકની નકલ,\r\n૮. ધોરણ ૧૦ ની માર્કશીટની નકલ,\r\n૯  ધોરણ ૧૧ ની માર્કશીટની નકલ,\r\n૧૦. કોચિંગક્લાસ સમાજ/ટ્રસ્ટ/સંસ્થા સંચાલિતછે તો તેનો રજીસ્ટ્રેશન નંબર\r\n       તેમજ સંસ્થાનાં ૩ વર્ષનાં અનુભવનો પુરાવો,\r\n૧૧. ભરેલ/ ભરવાપાત્ર કોચિંગ ફી નો પુરાવો,', '/71ab56d49960cc08439e525b04a386f3JEE,_GUJCET,_NEET_2.pdf', '', 'https://gueedc.gujarat.gov.in/Coaching-Help-Scheme-for-Jee-GUJCET-NEET-Exams.html', 'sahay', 0, 1, '2021-02-06 04:23:09'),
(27, 'સ્પર્ધાત્મક પરીક્ષા માટે તાલીમ સહાય', '', '૧. અરજી પત્રક,\r\n૨. આધારકાર્ડની નકલ,\r\n૩. બિન અનામતનું પ્રમાણપત્ર,\r\n૪. આવકનું પ્રમાણપત્ર (આવક મર્યાદા કુટુંબની ૪.૫૦લાખ),,\r\n૫. રહેઠાણનો પુરાવો,\r\n૬. બેંક પાસબૂકની નકલ,\r\n૭. ધોરણ ૧૦ ની માર્કશીટની નકલ,\r\n૮.  ધોરણ ૧૨ ની માર્કશીટની નકલ,\r\n૯. સ્પર્ધાત્મક પરિક્ષાતાલીમ ક્લાસ સમાજ/ટ્રસ્ટ/સંસ્થા સંચાલિતછે તો તેનો \r\n       રજીસ્ટ્રેશન નંબર/GST નંબરનો પુરાવો, \r\n૧૦. તાલીમ માટે થતી /ભરેલ ફી નો પુરાવો,\r\n૧૧. સંસ્થાનો એડમિશન લેટર(સ્પર્ધાત્મક પરિક્ષાના નામ તેમજ ફી ની વિગતો \r\n       સહિત), ', '/7a1e7f81134efcf2c195ae830c3bd0feSPRDHATMAK_PARIXA_MATE_TALIM_SAHAY.pdf', '', 'https:/gueedc.gujarat.gov.in', 'sahay', 0, 1, '2021-02-06 04:29:59'),
(28, 'સ્નાતક તબીબ,વકીલ,ટેકનિકલ સ્નાતક  માટે લીધેલ લોન પર વ્યાજ સહાય ૫ % વ્યાજ સહાય,', '', 'તબીબ, વકીલ, ટેકનિકલ સ્નાતક થયેલ બિનઅનામત વર્ગના લાભાર્થીઓ પોતાનું\r\nકલીનીક,લેબોરેટરી,રેડિયોલોજી કલીનીક કે ઓફિસ ખોલવા ઇચ્છે તો બેન્ક પાસેથી\r\nલીધેલા રૂપિયા ૧૦ લાખ સુધીની લોન પર ૫% વ્યાજ સહાય મળવા પાત્ર થશે, ', '/c8703cc6e76b1c52ac5af1d5b713b1fdSNATAK_TABIB_VAKIL_VYAJ_SAHAY.pdf', '', '', 'sahay', 0, 1, '2021-01-31 07:51:13'),
(29, 'શૈક્ષણિક અભ્યાસ યોજના (લોન) - રૂપિયા ૧૦ લાખની મર્યાદામાં', '', '૧. અરજી પત્રક,\r\n૨. આધારકાર્ડની નકલ,\r\n૩. બિન અનામતનું પ્રમાણપત્ર,\r\n૪. આવકનું પ્રમાણપત્ર (આવક મર્યાદા કુટુંબની વાર્ષિક ૬ લાખ),\r\n૫. રહેઠાણનો પુરાવો,\r\n૬. બેંક પાસબૂકની નકલ,\r\n૭. શાળા છોડીયાનો દાખલો તેમજ ધોરણ ૧૦/૧૨ ની માર્કશીટની નકલ,\r\n૮. ધોરણ ૧૨ અને તે પછીની માર્કશીટની નકલ,  \r\n૯.  પ્રતિવર્ષ ભરવાની થતી /ભરેલ ફી નો પુરાવો,\r\n૧૦. ગીરોખત/બોજાનોંધ અને ૫ ચેક,\r\n૧૧. પિતા/વાલીની મિલકત મોર્ગેજ કરવાની સંમતિપત્ર,\r\n૧૨. એડમિશન લેટર,\r\n૧૩. પિતા/વાલીની  મિલકત વેલ્યુએશન સર્ટી અને મિલકતના આધારો,\r\n૧૪. જમીનદારનીસહમતી અને મિલકત વેલ્યુએશન સર્ટી અને મિલકતના આધારો ', '/720c380c2f1c63980ed54d980a25a277SHAXNIK_ABHYAS_YOJNA.pdf', '', 'https://sje.gujarat.gov.in/ddcw/Educational', 'sahay', 0, 1, '2021-02-06 04:12:42'),
(30, 'વિદેશ અભ્યાસ લોન,(15 લાખનીમર્યાદામાં)', '', '૧. અરજી પત્રક,\r\n૨. આધારકાર્ડની નકલ,\r\n૩. બિન અનામતનું પ્રમાણપત્ર,\r\n૪. આવકનું પ્રમાણપત્ર (આવક મર્યાદા કુટુંબની વાર્ષિક ૬ લાખ),\r\n૫. રહેઠાણનો પુરાવો,\r\n૬. બેંક પાસબૂકની નકલ,\r\n૭. શાળા છોડીયાનો દાખલો તેમજ ધોરણ ૧૦/૧૨ ની માર્કશીટની નકલ,\r\n૮. ધોરણ ૧૨ અને તે પછીની માર્કશીટની નકલ,  \r\n૯.  પ્રતિવર્ષ ભરવાની થતી /ભરેલ ફી નો પુરાવો,\r\n૧૦. ગીરોખત/બોજાનોંધ અને ૫ ચેક,\r\n૧૧. પિતા/વાલીની મિલકત મોર્ગેજ કરવાની સંમતિપત્ર,\r\n૧૨. એડમિશન લેટર,\r\n૧૩. પિતા/વાલીની  મિલકત વેલ્યુએશન સર્ટી અને મિલકતના આધારો,\r\n૧૪. પાસપોર્ટની નકલ વિજા ની નકલ અને એર ટિકીટની નકલ', '/9401de64c8857d1a20b6430663505b9aVIDESH_ABHYAS_LON.pdf', '', 'https://esamajkalyan.gujarat.gov.in/', 'sahay', 0, 1, '2021-02-06 04:08:22'),
(31, 'સ્વ-રોજગારલક્ષી યોજના,', '', '૧. અરજી પત્રક,\r\n૨. આધારકાર્ડની નકલ,\r\n૩. બિન અનામત વર્ગનું પ્રમાણપત્ર,\r\n૪. આવકનું પ્રમાણપત્ર (આવક મર્યાદા કુટુંબની વાર્ષિક ૬ લાખ),\r\n૫. બેંક પાસબૂકની નકલ,\r\n૬. રેશનકાર્ડની નકલ,\r\n૭. ઉમરનો પુરાવો,', '/e6a95c316bcf96a99da16639ced2f2a6SAVA_ROJGARLASHI_YOJNA.pdf', '', 'https://gu.vikaspedia.in', 'sahay', 0, 1, '2021-02-06 03:59:47'),
(32, 'કોમર્શિયલ પાયલોટની તાલીમ માટેની લોન,', '', '૧. અરજી પત્રક,\r\n૨. આધારકાર્ડની નકલ,\r\n૩. બિન અનામતનું પ્રમાણપત્ર,\r\n૪. આવકનું પ્રમાણપત્ર (આવક મર્યાદા કુટુંબની વાર્ષિક ૬ લાખ),\r\n૫. રહેઠાણનો પુરાવો,\r\n૬. બેંક પાસબૂકની નકલ,\r\n૭. શાળા છોડીયાનો દાખલો તેમજ ધોરણ ૧૦/૧૨ ની માર્કશીટની નકલ,\r\n૮. ધોરણ ૧૨ અને તે પછીના સ્નાતક અભ્યાસની માર્કશીટની નકલ,  \r\n૯.  પ્રતિવર્ષ ભરવાની થતી /ભરેલ ફી નો પુરાવો,\r\n૧૦. જે સંસ્થામાપ્રવેશમેળવેલ હોય તેનો અડમિશન લેટર,\r\n૧૧.  લાભાર્થીની મિલકત મોર્ગેજ કરવાની સંમતિપત્ર,\r\n૧૨.  સ્ટુડન્ટ પાયલોટ લાઇસન્સ,\r\n૧૩. પિતા/વાલીની  મિલકત વેલ્યુએશન સર્ટી અને મિલકતના આધારો,\r\n૧૪. બે જમીનદારની સહમતી અને મિલકત વેલ્યુએશન સર્ટી અને મિલકતના \r\n        આધારો,\r\n૧૫. વિજા ની નકલ જો સંસ્થા વિદેશી હોય તો, \r\n૧૬. પાસપોર્ટ ની નકલ જો સંસ્થા વિદેશી હોય તો,\r\n૧૭. શૈક્ષનીક લાયકાત', '/667144e647bd68fc41b9eeba57343ed9COMARSIYEL_PAILOT_TALIM.pdf', '', '', 'sahay', 0, 1, '2021-01-31 10:30:41'),
(33, 'મુખ્યમંત્રી યુવા સ્વાવલંબન યોજના (MYSY)', '', '', '/36903595060926b16298ece587d626c6MYSY_2.pdf', '', 'https://mysy.guj.nic.in/', 'sahay', 0, 1, '2021-01-31 21:57:04'),
(34, 'મફત અને ફરજિયાત શિક્ષણનો અધિકાર (RTE)', '', '૧. અરજીફોર્મ અને ફોટો,\r\n૨. આધારકાર્ડ ની નકલ (માતા/પિતા અને બાળક એમ ત્રણેય ની નકલ)\r\n૩. રેશનકાર્ડની નકલ,\r\n૪. લાઈટબિલ અને વેરાબિલ ની નકલ,\r\n૫. ભાડાકરાર,\r\n૬. બેંકપાસબુકની નકલ,\r\n૭. આવકનો દાખલો,\r\n૮. સ્કૂલ બોનોફાઈડ સર્ટી,', '/84fd58ac01b8508be291bb1944ab697fRTE_3.pdf', '', 'https://rte.orpgujarat.com', 'sahay', 0, 1, '2021-02-02 03:17:30'),
(35, 'શિષ્યવ્રુતિ- શિક્ષણ વિભાગ', '', '૧. અરજી ફોર્મ અને ફોટા,\r\n૨. ધોરણ ૭ નું શાળા છોડિયાનું પ્રમાણપત્ર,\r\n૩. માર્કશીટની નકલ,\r\n૪.  ફી નો ડીડી અથવા બેંક ચેક ', '/29f45ff74edb9af9fbf37a3f202ce3fdShishyvruti_shikshan_vibhag.pdf', '', 'https://gujarat-education.gov.in/education/', 'sahay', 0, 1, '2021-02-06 03:24:22'),
(36, 'વિકલાંગ વિધાર્થીઓ માટે વિકલાંગ શિષ્યવૃતિ યોજના,', '', 'લાભ કોને મળે ?\r\nધોરણ ૧  થી ૭ માં ભણતા વિકલાંગ વિધાર્થીઓ (રૂપિયા ૧૦૦૦ વાર્ષિક)અને \r\nધોરણ ૮ કે તેથી વધુ અભ્યાસ કરતાં વિકલાંગ વિધાર્થીઓ (રૂપિયા ૧૫૦૦ કે તેથી વધુ\r\nરૂપિયા ૫૦૦૦ સુધી વાર્ષિક),\r\n૧. શાળામાં ભણતા હોય તેનો પુરાવો,\r\n૨. વિકલાંગતાનું પ્રમાણપત્ર અને ઓળખપત્ર, ', '/d1dea52e768dca6aca0d5cc9412367e1VIKLANG_SHISHYVRUTI.pdf', '', 'https://sje.gujarat.gov.in', 'sahay', 0, 1, '2021-02-01 23:46:05'),
(37, 'મુખ્યમંત્રી અમૃતમ \'\'માં\'\' યોજના,', '', '૧. ફોર્મ અને ફોટો,\r\n૨. ચુંટણીકાર્ડ ની નકલ,\r\n૩. રેશનકાર્ડની નકલ,\r\n૪. આધારકાર્ડની નકલ,\r\n૫. આવકનો દાખલો,', '/9d0a92b3f77e733d1c6415e6ac754ce9Maa_AMrutam.pdf', '', 'https://magujarat.com', 'sahay', 0, 1, '2021-02-06 02:18:17'),
(38, 'ચિરંજીવી યોજના,', '', '૧. ફોર્મ અને ફોટો,\r\n૨. આવકનો દાખલો અથવા બી.પી.એલ. કાર્ડ\r\n૩. આધારકાર્ડ, ચુંટણીકાર્ડ ની નકલ,\r\n૪. રેશનકાર્ડની નકલ,\r\n૫. બેંકપાસબુકની નકલ,', '/90334b16a376062d0fa0d626c6303cf5CHIRANJIVI_YOJNA.pdf', '', 'https://www.nhp.gov.in/', 'sahay', 0, 1, '2021-02-01 23:44:15'),
(39, 'મુખ્યમંત્રી રાહત ફંડ,', '', '૧. અરજીફોર્મ  (મુખ્યમંત્રીશ્રી ને),\r\n૨. કેશપેપર(હોસ્પિટલ- ઓ.પી.ડી.કેશપેપર,લેટરપેડ,વૈયદિક નોંધ- સંપૂર્ણ ફાઈલ)\r\n૩. ખર્ચનો અંદાજ,\r\n૪.ડોક્ટરનું પ્રમાણપત્ર (ઓપરેશન બાકી છે તે મુજબનું સંબંધિત ડોક્ટરનું અસલ \r\n     પ્રમાણપત્ર),\r\n૫. રેશનકાર્ડની નકલ,\r\n૬. આવકનો દાખલો,\r\n૭. સોંગદનામું,.\r\n૮. ભલામણપત્ર (સંસદસભ્ય અથવા ધારાસભ્ય), ', '/fee05f0f4a58b56bab78a346edc75c4fRAHAT_FUND__SAHAY.pdf', '', 'https://revenuedepartment.gujarat.gov.in/cm-relief-fund', 'sahay', 0, 1, '2021-02-06 01:58:22'),
(40, 'અનાથ બાળકોને માસિક રૂ.૩૦૦૦ ની સહાય યોજના,', '', '૧. અરજીપત્ર (પાલક માતા/પિતાએ અરજી કરવાની રહેશે)\r\n૨. આવકનો દાખલો(આવકનો દાખલો પાલક માતા/પિતાએ તાલુકા મામલદાર શ્રી \r\n      પાસે થી મેળળવાનો રહેશે. આવક મર્યાદા ૨૭૦૦૦/-),\r\n૩. બાળક નાં માતાપિતા હયાત નથી (અવસાન પામેલ) તેવો મૃત્યુનું પ્રમાણપત્ર,\r\n૪. બાળક ની ઉંમરનું પ્રમાણપત્ર,\r\n૫. ફોટા (વાલી થનારનો બાળક સાથેનો પાસપોર્ટ સાઈઝ નો ફોટો,\r\n૬. બેંકપાસબુક(બાળક તેમજ વાલીનું સયુંકત બેંકખાતું) ની નકલ,', '/3183e42952f77d67481d5689ca30ad85Anath_Balak_Sahay.pdf', '', 'https://sje.gujarat.gov.in', 'sahay', 0, 1, '2021-02-06 01:52:25'),
(41, 'કુદરતી આપતિઓને કારણે થતા નુકશાન માટે નાણાંકીય સહાય,', '', '૧. અરજીપત્રક અને ફોટો,\r\n૨. આધારકાર્ડની નકલ,\r\n૪. રેશનકાર્ડની નકલ\r\n૫. ચુંટણીકાર્ડની નકલ,\r\n૬. મૃત્યુ કુદરતી આપતીનાં કારણે થયું છે તેવો પોસ્ટમોટામ રીપોર્ટ,\r\n૭. મૃત્યુ નું પ્રમાણપત્ર(દાખલો),', '/93f1d60a22219ff1a6a355e68c238b30Kudrati_Aapatio_Na_Nukshan_Sahay.pdf', '', 'https://gu.vikaspedia.in/agriculture/', 'sahay', 0, 1, '2021-02-06 01:47:42'),
(42, 'વહાલીદીકરી યોજના,', '', '૧. અરજીફોર્મ અને ફોટા\r\n૨. દીકરીનું જન્મનું પ્રમાણપત્રની નકલ.\r\n૩. માતા તથા પિતા નાં આધારકાર્ડની નકલ,\r\n૪. બેંક પાસબુકની નકલ,\r\n૫. માતાનાં જન્મનું પ્રમાણપત્રની નકલ,\r\n૬. માતા-પિતા ની વાર્ષિક આવકનું પ્રમાણપત્ર,\r\n૭. સંતતિ નીયમનનું પ્રમાણપત્ર ( બીજું સંતાન હોય ત્યારે),\r\n૮. દંપતીનું સોગંધનામું,\r\n', '/d6bac496619c248e2e42301bfd70645cVAHALI_DIAKRI__YOJNA_2.pdf', '', 'https://sje.gujarat.gov.in/dsd', 'sahay', 0, 1, '2021-02-02 01:28:00'),
(43, 'પાલક માતા-પિતા યોજના,', '', '૧. અરજીપત્ર (ફોર્મ) અને ફોટો,\r\n૨. બાળકનાં માતા/પિતાનું મૃત્યુનું પ્રમાણપત્રની નકલ,\r\n૩. બાળકની માતાએ પુનઃલગ્ન કરેલા હોય તો તે બાબતનું લગ્નનું પ્રમાણપત્ર,\r\n૪.  પાલક માતા/પિતા નો આવકનો દાખલો,\r\n૫. બેંકપાસબુક ( પાલક માતા/પિતા સાથે બેંક નું સયુંકત બેંકખાતું) ની નકલ,', '/1e7ea1d9233d28fce13fc869c476c5bdPalak_Mata_Pita_Yojna.pdf', '', 'https://sje.gujarat.gov.in/dsd', 'sahay', 0, 1, '2021-02-02 01:24:28'),
(44, 'કમિશ્નર, કુટીર અને ગ્રામોદ્યોગ (ગુજરાત સરકાર)\r\nકૌશલ્ય વધારીને કુશળ બનાવતી અને  નાણાકીય સહાય સાથે સહકાર આપતી અનોખી યોજના,', '', '૧ અરજીફોર્મ તથા ફોટો,\r\n૨. ઉંમરનો પુરાવો  (જન્મનો દાખલો),\r\n૩. ધોરણ ૧૦ નું લિવિંગસર્ટી,\r\n૪. નિયત ધંધા નો ૧ વર્ષનાં અનુભવનું પ્રમાણપત્ર,\r\n૫. રેશનકાર્ડની નકલ,\r\n૬. આધારકાર્ડની નકલ,\r\n૭. ચુંટણીકાર્ડ ની નકલ.\r\n૮. પાનકાર્ડની નકલ,\r\n૯. બેન્કપાસબુકની નકલ,\r\n', '/710b4a505a12d7acaaaa43cd202ab8a5KUTIR_ANE_GRAM_UDHYOG_1.pdf', '', 'http://www.cottage.gujarat.gov.in/Eng/HomeGuj', 'sahay', 0, 1, '2021-02-04 23:03:57'),
(45, 'પ્રધાનમંત્રી ફસલ બીમા યોજના,', '', '૧. અરજીફોર્મ અને ફોટા,\r\n૨. બેંકપાસબુકની નકલ,\r\n૩. આધારકાર્ડની નકલ,\r\n૪. ચુંટણીકાર્ડની નકલ,\r\n૫. ૭/૧૨ અને ૮-અ (તાજેતરનાં)\r\n૬. મોબાઈલ નંબર', '/08ad55ca24cb0fb01bc6534b25fe0c81PMFBY_3.pdf', '', 'https://pmfby.gov.in', 'sahay', 0, 1, '2021-02-02 01:53:41'),
(46, 'પ્રધાનમંત્રી સુરક્ષા વીમા યોજના,', '', 'અરજદારની ઉંમર વર્ષ ૧૮ થી ૭૦ \r\n૧. ફોર્મ અને ફોટો,\r\n૨. બેંક એકાઉન્ટ પાસબુકની નકલ,\r\n૩. આધારકાર્ડની નકલ\r\n', '/0773e80ee543d26f23d7f10434933559Surksha_Vima_Yojna_2.pdf', '', 'https://ikhedut.gujarat.gov.in/nais', 'sahay', 0, 1, '2021-02-04 22:36:27'),
(47, 'ખેતઓજારની સહાય- ખેડૂતોને ખેતી માટેનાં ઓજારો માટે નાણાંકીય સહાય આપતી યોજના,', '', '૧. અરજીફોર્મ  અને ફોટો,\r\n૨. ૭/૧૨, ૮-અ ,\r\n૩. આધારકાર્ડની નકલ,\r\n૪. બેંકની પાસબુકની નકલ,\r\n૫. મોબાઈલ નંબર', '/9077aa0742edc89ae03df14db886509aKhet_Ojar_Sahay_4.pdf', '', 'https://ikhedut.gujarat.gov.in/Public/frm_Public_SchemeDetails.aspx', 'sahay', 0, 1, '2021-02-04 23:15:25'),
(48, 'ખેડૂત અકસ્માત વીમા યોજના,', '', '૧. અરજીપત્રક અને ફોટો,\r\n૨. આધારકાર્ડની નકલ,\r\n૪. રેશનકાર્ડની નકલ\r\n૫. ચુંટણીકાર્ડની નકલ,\r\n૬. મૃત્યુ કુદરતી આપતીનાં કારણે થયું છે તેવો પોસ્ટમોટામ રીપોર્ટ,\r\n૭. મૃત્યુ નું પ્રમાણપત્ર(દાખલો),', '/10244e40f9077be8652089784ec9e76eKHEDUT_AKSMAT_VIMA_YOJNA.pdf', '', 'https://dag.gujarat.gov.in', 'sahay', 0, 1, '2021-02-04 23:11:33'),
(49, 'પ્રધાનમંત્રી કિસાન સમ્માન નિધિ યોજના,', '', '૧. અરજીફોર્મ અને ફોટો,\r\n૨ ૭/૧૨ અને ૮-અ,\r\n૩. આધારકાર્ડ ની નકલ,\r\n૪. ચુંટણીકાર્ડની નકલ,\r\n૫. બેંકપાસબુકની નકલ,', '/29d5f1e285dac8acce774079a60df620KISHAN_SANMAN_NIDHI_YOJNA.pdf', '', 'https://pmkisan.gov.in/', 'sahay', 0, 1, '2021-02-06 01:09:45');

-- --------------------------------------------------------

--
-- Table structure for table `jiveensathi`
--

CREATE TABLE `jiveensathi` (
  `id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `name` varchar(1000) NOT NULL,
  `address` varchar(1000) NOT NULL,
  `mobile` varchar(15) NOT NULL,
  `home_mobile` varchar(15) NOT NULL,
  `email` varchar(100) NOT NULL,
  `dob` date NOT NULL,
  `height` varchar(100) NOT NULL,
  `weight` varbinary(100) NOT NULL,
  `qualification` varchar(1000) NOT NULL,
  `mulvatan` varchar(1100) NOT NULL,
  `mosal` varchar(1000) NOT NULL,
  `pasndi` varchar(1000) NOT NULL,
  `business` varchar(100) NOT NULL,
  `profile` varchar(1000) NOT NULL,
  `gender` int(1) NOT NULL DEFAULT '1',
  `status` int(1) NOT NULL DEFAULT '0',
  `sate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `job`
--

CREATE TABLE `job` (
  `id` int(11) NOT NULL,
  `title` varchar(1000) NOT NULL,
  `company_name` varchar(1000) NOT NULL,
  `name` varchar(100) NOT NULL,
  `mobile` varchar(15) NOT NULL,
  `qualification` varchar(1000) NOT NULL,
  `vacancies` varchar(100) NOT NULL,
  `description` longtext NOT NULL,
  `location` varchar(1000) NOT NULL,
  `website` varchar(1000) NOT NULL,
  `salary` varchar(1000) NOT NULL,
  `document` varchar(1000) NOT NULL,
  `type` enum('employee','company') NOT NULL DEFAULT 'company',
  `status` int(1) NOT NULL DEFAULT '1',
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `job`
--

INSERT INTO `job` (`id`, `title`, `company_name`, `name`, `mobile`, `qualification`, `vacancies`, `description`, `location`, `website`, `salary`, `document`, `type`, `status`, `date`) VALUES
(1, ' Press Operator, Helper', ' Cosmos Manpower Pvt Ltd Gujarat ', '', '', '10/12 pass', '100', '*Urgent 100 Male Required*\r\n( Cosmos Manpower Pvt Ltd Gujarat )\r\nAddress - Bechraji, District - Ahamadabad, Gujarat \r\n\r\nResponsibilities - Manufacturing of Steel Metal Components,Diesel tools ,Pancel Checkers and their Toppings \r\n\r\nPost - Press Operator, Helper \r\n \r\nAge - 18 TO 26\r\n\r\nQualifications: Minimum 10th pass, ITI  \r\n\r\nSalary - \r\n(1)10th,12th -13012/-CTC* Inhand  - 11288/-\r\n(2)Fitter - 14085/-CTC\r\nIn hand -12361/-\r\n\r\nOne Time food, Transportation subsidies Facility\r\n \r\nAccomodaction -1000/-\r\n\r\nWorking hours - 12 hour \r\n\r\nOver Time - /-Hours\r\n\r\nRequired Documents -\r\n1. 10th Marksheet Original & Xerox\r\n2. Bank Passbook Xerox\r\n3. Pan Card  & Aadhar Card \r\n4. 6 Photo Passport Size.\r\n\r\nDirect Joining\r\n\r\nJoining date -25 March 2021\r\n\r\nCall nhi mile to WhatsApp par Registration kre\r\nMr.Vinod  - 7597886079\r\nWhat\'sapp - 7425095384\r\n\r\nPlease share this msg with All friends & Groups for needy Candidates', 'Address - Bechraji, District - Ahamadabad, Gujarat', '', 'Salary -  (1)10th,12th -13012/-CTC* Inhand  - 11288/- (2)Fitter - 14085/-CTC', '', 'company', 1, '2021-03-06 08:41:07'),
(2, ' પોલીસ માં ભરતી', 'ગુજરાત પોલીસ વિભાગમાં ભરતી', '', '', '', '1,382', 'ગુજરાતના પોલીસ વિભાગમાં ભરતીની જાહેરાત\r\n1,382 ઉમેદવારોની પોલીસ વિભાગમાં કરાશે ભરતી\r\n16 માર્ચથી ઓનલાઈન ફોર્મ ભરાવવાની થશે શરૂઆત\r\nબિન હથિયારી પોલીસ સબ ઈન્સ્પેક્ટરની 202 જગ્યાઓ\r\nબિન હથિયારી PSI મહિલાઓ માટે 98 જગ્યાઓ\r\nહથિયારી PSI પુરુષો માટે 72 જગ્યાઓ\r\nઈન્ટેલિજન્સ ઓફિસર પુરુષો માટે 18 જગ્યાઓ\r\nઈન્ટેલિજન્સ ઓફિસર મહિલાઓ માટે 9 જગ્યાઓ\r\nબિન હથિયારી ASI પુરુષ માટે 659 જગ્યાઓ\r\nબિન હથિયારી ASI મહિલાઓ માટે 324 જગ્યાઓ', 'ગુજરાત', '', '', '/b8bfb71d161cfa70800927cdfc870a564_5942986250800596913.pdf', 'company', 1, '2021-03-15 05:30:35'),
(3, 'પોલીસ સબ ઇન્સ્પેક્ટર ની ભરતી', 'ગુજરાત સરકાર', '', '', '', '1382', '', 'સરદાર ગામ,વૈશ્રનોદેવી સર્કલ, અમદાવાદ', '', '', '/56f2890cdec80a1ae6e62cefc2191b63IMG-20210317-WA0155.jpg', 'company', -1, '2021-03-17 06:48:33'),
(4, 'vacancy in computor work \r\nTALLY & EXCEL WORK\r\n', 'M M YARNS PVT LTD AT, Kharchiya', '', '9426719610', 'B.com', '5', 'Account', 'Atkot,kharchiya', '', '8000/10000', '/334f057a1c56042d2ba96c69f7a56d6fIMG-20210407-WA0181.jpg', 'company', 1, '2021-04-07 07:53:58');

-- --------------------------------------------------------

--
-- Table structure for table `karkidi_master`
--

CREATE TABLE `karkidi_master` (
  `id` int(11) NOT NULL,
  `name` varchar(1000) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `karkidi_master`
--

INSERT INTO `karkidi_master` (`id`, `name`, `status`, `date`) VALUES
(1, 'Test', -1, '2021-02-08 19:41:27'),
(2, 'શિક્ષણ ક્ષેત્ર', 1, '2021-02-13 12:15:13'),
(3, 'આરોગ્ય ક્ષેત્રે', 1, '2021-02-13 12:19:11'),
(4, 'ઓર્ગેનિક ખેતી વાડી', 1, '2021-02-13 12:19:34'),
(5, 'પોલીસ ડીપાર્ટમેન્ટ', 1, '2021-02-13 12:19:55'),
(6, 'વહીવટી ક્ષેત્રે', 1, '2021-02-13 12:20:16'),
(7, 'ઔધ્યોગિક ક્ષેત્રે', 1, '2021-02-13 12:20:33'),
(8, 'કમ્પ્યુટર ક્ષેત્રમાં', 1, '2021-02-13 12:20:53'),
(9, 'સોશ્યલ એકટી વિટી', 1, '2021-02-13 12:21:12'),
(10, 'સામાજિક ક્ષેત્રે', 1, '2021-02-13 12:21:31'),
(11, 'રાજકીય ક્ષેત્રે', 1, '2021-02-13 12:21:48');

-- --------------------------------------------------------

--
-- Table structure for table `kharsh_transcription`
--

CREATE TABLE `kharsh_transcription` (
  `id` int(11) NOT NULL,
  `txt_id` varchar(100) NOT NULL,
  `bill_id` varchar(100) NOT NULL,
  `uid` int(11) NOT NULL,
  `amount` double NOT NULL,
  `notes` longtext NOT NULL,
  `by_uid` int(11) NOT NULL,
  `by_brand` varchar(500) NOT NULL,
  `by_model` varchar(500) NOT NULL,
  `bank_type` enum('bank','cash') NOT NULL,
  `time` time NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kharsh_transcription`
--

INSERT INTO `kharsh_transcription` (`id`, `txt_id`, `bill_id`, `uid`, `amount`, `notes`, `by_uid`, `by_brand`, `by_model`, `bank_type`, `time`, `status`, `date`) VALUES
(1, '16190097737711', 'M1', 0, 3501, 'ખાતમુહૂર્ત ખર્ચ', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-04-21 08:56:13'),
(2, '16190098598314', 'M2', 0, 1100, 'નટુભાઇ બોધરા', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-04-21 08:57:39'),
(3, '16190099088235', 'M3', 0, 50000, 'વૈભવ સ્ટીલ 12/2/21', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-04-21 08:58:28'),
(4, '16190099372421', 'M4', 0, 98870, 'વૈભવ સ્ટીલ 16/2/21', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-04-21 08:58:57'),
(5, '16190099838113', 'M5', 0, 9630, 'ખોડીયાર પાઈપ 19/2/21', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-04-21 08:59:43'),
(6, '16190100299435', 'M6', 0, 9500, 'લોખંડ ના પોલ 22/2/21', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-04-21 09:00:29'),
(7, '16190100581156', 'M7', 0, 20000, 'નટુભાઇ બોધરા 24/2/21', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-04-21 09:00:58'),
(8, '16190100772239', 'M8', 0, 20000, 'નટુભાઇ બોધરા 25/2/21', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-04-21 09:01:17'),
(9, '16190101176981', 'M9', 0, 12370, 'કાંટો તાર ભવન 2 માટે 26/2/21', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-04-21 09:01:57'),
(10, '16190101456194', 'M10', 0, 30000, 'ધાનાણી સ્ટીલ 3/3/21', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-04-21 09:02:25'),
(11, '16190101806310', 'M11', 0, 51700, 'ધાનાણી સ્ટીલ 5/3/21', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-04-21 09:03:00'),
(12, '16190102016424', 'M12', 0, 40000, 'નટુભાઇ બોધરા', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-04-21 09:03:21'),
(13, '16190102824839', 'M13', 0, 23653, 'પરચુરણ ખર્ચ 4/2 થી 10/3/21 સુધી', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-04-21 09:04:42'),
(14, '16190103097642', 'M14', 0, 50000, 'વૈભવ સ્ટીલ 17/3/21', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-04-21 09:05:09'),
(15, '16190103876902', 'M15', 0, 19750, '  3 /ઈટુ /ભાડું/ ખાડા 19/3/21', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-04-21 09:06:27'),
(16, '16190104302282', 'M16', 0, 5100, 'મીટીંગ ભોજન ખર્ચ 21/3/21', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-04-21 09:07:10'),
(17, '16190104494750', 'M17', 0, 50000, 'વૈભવ સ્ટીલ 22/3/21', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-04-21 09:07:29'),
(18, '16190105124050', 'M18', 0, 100000, 'નટુભાઇ બોધરા\nચેક નં 003    27/3/21', 2, 'OPPO', 'CPH2001', 'bank', '00:00:00', 1, '2021-04-21 09:08:32'),
(19, '16190105514975', 'M19', 0, 83500, 'રેતી હરેશભાઈ 27/3/21', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-04-21 09:09:11'),
(20, '16190106621884', 'M20', 0, 1400, 'નળ ફીટીગ 20/3/21', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-04-21 09:11:02'),
(21, '16190106871644', 'M21', 0, 12000, '2 ઈંટ ભુપતભાઇ', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-04-21 09:11:27'),
(22, '16190107362011', 'M22', 0, 3520, 'કોદાળી/ પાવડો/30/3/21', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-04-21 09:12:16'),
(23, '16190107867007', 'M23', 0, 3000, '30 ફેરા ટેકર ભાડું સેવા નટુભાઇ', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-04-21 09:13:06'),
(24, '16190108325692', 'M24', 0, 3400, ' ગાડી ભાડું સીમેન્ટ મંગાવતા 2/4/21', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-04-21 09:13:52'),
(25, '16190108582785', 'M25', 0, 54000, '9 ઈંટ ભુપતભાઇ 7/4/21', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-04-21 09:14:18'),
(26, '16190108778233', 'M26', 0, 50000, 'વૈભવ સ્ટીલ 9/4/21', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-04-21 09:14:37'),
(27, '16190109116605', 'M27', 0, 40000, 'ઘાનાણી સ્ટીલ 9/4/21', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-04-21 09:15:11'),
(28, '16190109536898', 'M28', 0, 2750, '11 ટ્રેક્ટર ભાડે ઈંટ 10/4/21', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-04-21 09:15:53'),
(29, '16190109745636', 'M29', 0, 50000, 'વૈભવ સ્ટીલ 12/4/21', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-04-21 09:16:14'),
(30, '16190110655760', 'M30', 0, 300, 'રીક્ષા ભાડું 16/4/21', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-04-21 09:17:45'),
(31, '16190111204683', 'M31', 0, 24000, 'નટુભાઇ બોધરા 16/4/21', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-04-21 09:18:40'),
(32, '16190111422802', 'M32', 0, 18000, '3 ઈટુ ભુપતભાઇ', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-04-21 09:19:02'),
(33, '16191601955456', 'M33', 0, 16924, 'G e b rajistri 7/4/21', 2, 'OPPO', 'CPH2001', 'bank', '00:00:00', 1, '2021-04-23 02:43:15'),
(34, '16191602381537', 'M34', 0, 16924, 'G e b rajistri 2. 7/4/21', 2, 'OPPO', 'CPH2001', 'bank', '00:00:00', 1, '2021-04-23 02:43:58'),
(35, '16191603787738', 'M35', 0, 7000, 'Office Patra majuri 23/4/21', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-04-23 02:46:18'),
(36, '16193281802032', 'M36', 0, 5500, 'મધુરમ મારબલ\nઓફિસ લાદી 40 પેટી', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-04-25 01:23:00'),
(37, '16201038876669', 'M37', 0, 100, 'Riksha bhadu\nPaip mangavta ,vipul', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-05-04 00:51:27'),
(38, '16201039573538', 'M38', 0, 490, 'Motar sa.riprer H.vipul', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-05-04 00:52:37'),
(39, '16203634736331', 'M39', 0, 100000, 'Vaybhav still\n', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-05-07 00:57:53'),
(40, '16203704386734', 'M40', 0, 2750, '11 fera itu tektar bhadu', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-05-07 02:53:58'),
(41, '16203704975939', 'M41', 0, 24000, '4 fera itu 6000*4', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-05-07 02:54:57'),
(42, '16207046903760', 'M42', 0, 24000, '4 fera itu 6000*4', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-05-10 23:44:50'),
(43, '16207047263567', 'M43', 0, 2000, 'Office calar majuri Maraj ne', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-05-10 23:45:26'),
(44, '16208867315469', 'M44', 0, 10000, 'Pop aedvansh pement\n22500-10000#12500', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-05-13 02:18:51'),
(45, '16208886125132', 'M45', 0, 50000, 'Reti 2 fera aedvansh apel', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-05-13 02:50:12'),
(46, '16208892959929', 'M46', 0, 10000, 'Shivshkti bakda 5 no', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-05-13 03:01:35'),
(47, '16214228066776', 'M47', 0, 980, 'Vipul bhai haste kharch ni yadi', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-05-19 07:13:26'),
(48, '16214228708384', 'M48', 0, 1100, 'Haveli jasdan\nDan bhet', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-05-19 07:14:30'),
(49, '16214228892326', 'M49', 0, 2100, 'Amrshi Bhai ne dan bhet', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-05-19 07:14:49'),
(50, '16215949247978', 'M50', 0, 800, '500+300\nડીઝલ+ ડાયવર ને રોજ ટેન્કર જીવાપર મોકલતા', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-05-21 07:02:04'),
(51, '16216716944671', 'M51', 0, 200000, 'Vaybhav still Atkot', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-05-22 04:21:34'),
(52, '16220343364334', 'M52', 0, 490, ' પરચુરણ ખર્ચ ચા અને કોલ્ડ્રીંક', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-05-26 09:05:36'),
(53, '16220343685110', 'M53', 0, 28050, 'મીની પ્લાન્ટ સિમેન્ટ 165 જેટલી ચૂકવતા', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-05-26 09:06:08'),
(54, '16220354245912', 'M54', 0, 845, ' પરચુરણ ખર્ચ સલેબ ભરતી વખતે ખર્ચ', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-05-26 09:23:44'),
(55, '16220354868858', 'M55', 0, 7158, 'ખુડસી તેમજ સરબત ખર્ચ સલેબ ભરતી વખતે', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-05-26 09:24:46'),
(56, '16220355512878', 'M56', 0, 9250, 'નટુભાઇ બોધરા ને ઉપાડ\nપ્રકાશ હસ્તે', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-05-26 09:25:51'),
(57, '16221848298537', 'M57', 0, 100000, 'Parkas', 2, 'OPPO', 'CPH2001', 'bank', '00:00:00', 1, '2021-05-28 02:53:49'),
(58, '16221895585743', 'M58', 0, 18500, 'Dhanani still', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-05-28 04:12:38'),
(59, '16224821847062', 'M59', 0, 2100, 'સદભાવના વૃદ્ધાશ્રમ દ્વારા વૃક્ષ મંગાવવામાં આવેલ હતા જેનું ભાડું આપતા', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-05-31 13:29:44'),
(60, '16226128388983', 'M60', 0, 17700, 'Gel Krupp electric bil chukvta', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-06-02 01:47:18'),
(61, '16227083439348', 'M61', 0, 125000, 'Vaybhav still Atkot', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-06-03 04:19:03'),
(62, '16227083802832', 'M62', 0, 10000, 'Vipul Bhai ne pagar\nMay masno', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-06-03 04:19:40'),
(63, '16227084729115', 'M63', 0, 24000, 'Dhanani still jasdan', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-06-03 04:21:12'),
(64, '16227088934971', 'M64', 0, 80000, 'Parkas boghra', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-06-03 04:28:13'),
(65, '16228733493783', 'M65', 0, 1400, 'પારકીગ માં માટી પાથરવા ની મજુરી', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-06-05 02:09:09'),
(66, '16230481509442', 'M66', 0, 4500, 'નોન હુવન બેગ 1000 બનાવતા', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-06-07 02:42:30'),
(67, '16230544256773', 'M67', 0, 20000, 'Parkas boghra upad pete', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-06-07 04:27:05'),
(68, '16232341066331', 'M68', 0, 10800, 'ઈન્ડિયન પ્રીનટીગ 100=ફ્રેમ તેમજ1000= પ્રમાણ પત્ર ', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-06-09 06:21:46'),
(69, '16232341915705', 'M69', 0, 3800, 'રાજ ગ્લો સાઇન ઓફિસ પર ના બેનર તેમજ ફ્રેમ', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-06-09 06:23:11'),
(70, '16232459842396', 'M70', 0, 40000, 'રેતી ના બિલ મુજબ ઉપાડ આપતા\nહ. વિપુલ ટીલાળા', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-06-09 09:39:44'),
(71, '16233250996145', 'M71', 0, 3200, 'ટ્રેકટર ભાડું યાદી મુજબ\nબાંકડા ભરવા\nમાટી ફેરવતા\nજીવાપર ટેન્કર મુકવા જતા', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-06-10 07:38:19'),
(72, '16234713384529', 'M72', 0, 500, 'ખાટલા ની પાટી મંગાવતા', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-06-12 00:15:38'),
(73, '16236593736684', 'M73', 0, 100, 'ઝાડ ફરતે ખામણા કરવા ની મજુરી', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-06-14 04:29:33'),
(74, '16236695843360', 'M74', 0, 17500, 'JCB તેમજ જુનિ દીવાલ પાડવા માટે મજુરી', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-06-14 07:19:44'),
(75, '16236785949342', 'M75', 0, 18000, '3 fera itu nu bil chukvta\nBhadu Baki', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-06-14 09:49:54'),
(76, '16236837719909', 'M76', 0, 36000, 'વૈભવ સ્ટીલ આટકોટ ભવન 2 ના હીસાબ પેટ જમા કરવાતા', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-06-14 11:16:11'),
(77, '16236838073185', 'M77', 0, 25000, 'પાઈપ નું બિલ ચુકવતા', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-06-14 11:16:47'),
(78, '16237426174342', 'M78', 0, 28000, 'Patra', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-06-15 03:36:57'),
(79, '16237427122645', 'M79', 0, 34850, 'Matel ciment', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-06-15 03:38:32'),
(80, '16237603784139', 'M80', 0, 22500, '3 fera itu+ bhadu\n4850 bhav', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-06-15 08:32:58'),
(81, '16240038173404', 'M81', 0, 100000, 'Vaybhav still Atkot jama\nKeval', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-06-18 04:10:17'),
(82, '16240038458566', 'M82', 0, 12000, 'Anil pop kam kata chukvel', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-06-18 04:10:45'),
(83, '16240038786329', 'M83', 0, 25000, 'Bhavan 2 na kam Pete upad', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-06-18 04:11:18'),
(84, '16241600699963', 'M84', 0, 22650, 'Itu fera 3 + bhadu\n4850', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-06-19 23:34:29'),
(85, '16243372442160', 'M85', 0, 2000, 'AC fiting majuri', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-06-22 00:47:24'),
(86, '16243372741980', 'M86', 0, 25500, 'AC kharidi karta Check...', 2, 'OPPO', 'CPH2001', 'bank', '00:00:00', 1, '2021-06-22 00:47:54'),
(87, '16245287969827', 'M87', 0, 60000, 'Self\nVaybhav still p m Angadiya', 2, 'OPPO', 'CPH2001', 'bank', '00:00:00', 1, '2021-06-24 05:59:56'),
(88, '16245288308556', 'M88', 0, 25000, 'Natubhai upad bhavan 1\nDinesh Bhai hast', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-06-24 06:00:30'),
(89, '16246062409519', 'M89', 0, 20000, '6 fera 4500*9000=40500 mathi 20000upadpete apta', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-06-25 03:30:40'),
(90, '16247075159150', 'M90', 0, 7500, 'ગાર્ડન સ્ટુડિયો જસદણ હ.જેસાણી સર', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-06-26 07:38:35'),
(91, '16247075603753', 'M91', 0, 9999, 'ઓમકાર સ્ટીકર.હ J', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-06-26 07:39:20'),
(92, '16247076127253', 'M92', 0, 300, 'બેનર લગાવવાની મજુરી. J', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-06-26 07:40:12'),
(93, '16247077148798', 'M93', 0, 480, 'ભગવતી સ્ટેશનરી, ગીતાજી, રીક્ષા ભાડું. J', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-06-26 07:41:54'),
(94, '16247077707371', 'M94', 0, 4196, 'જંગવડ વિધાર્થીઓ ને પુસ્તક.J', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-06-26 07:42:50'),
(95, '16247078088656', 'M95', 0, 2960, 'ઓફિસ બેનર J', 2, 'OPPO', 'CPH2001', 'cash', '00:00:00', 1, '2021-06-26 07:43:28');

-- --------------------------------------------------------

--
-- Table structure for table `labharthi_master`
--

CREATE TABLE `labharthi_master` (
  `id` int(11) NOT NULL,
  `name` varchar(1000) NOT NULL,
  `mobile` varchar(15) NOT NULL,
  `village` varchar(100) NOT NULL,
  `taluko` varchar(100) NOT NULL,
  `dist` varchar(100) NOT NULL,
  `address` varchar(1000) NOT NULL,
  `yojna_name` varchar(500) NOT NULL,
  `amount` double NOT NULL,
  `qualification` varchar(100) NOT NULL,
  `approv_amt` double NOT NULL,
  `join_date` date NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `labharthi_master`
--

INSERT INTO `labharthi_master` (`id`, `name`, `mobile`, `village`, `taluko`, `dist`, `address`, `yojna_name`, `amount`, `qualification`, `approv_amt`, `join_date`, `status`, `date`) VALUES
(2, 'PANSURIYA JITENDRABHAI NATHABHAI', '9409366000', 'JASDAN', 'JASDAN', 'Rajkot', 'AT JASDAN SARDAR PATEL NAGAR B/H. GANGA BHUVAN TA JASDAN DIST RAJKOT 360050', '૫શુ આહાર', 500000, 'ધોરણ - ૭', 500000, '2020-06-11', 1, '2021-06-20 17:28:07'),
(3, 'RAMANI JITESHBHAI PRAVIBHAI', '9558619174', 'LILAPUR', 'JASDAN', 'Rajkot', 'AT LILAPUR AHMEABAD HIGHWAY TA JASDAN DIST RAJKOT 360050', '૫શુ આહાર', 500000, 'ધોરણ - ૯', 500000, '2020-06-11', 1, '2021-06-20 17:28:07'),
(4, 'PANSURIYA KAMLESHBHAI GOVINDBHAI', '9998100071', 'JASDAN', 'JASDAN', 'Rajkot', 'AT JASDAN SARDAR PATEL NAGAR GANGA BHUVAN TA JASDAN DIST RAJKOT 360050', '૫શુ આહાર', 500000, 'પોસ્ટ ગ્રેજ્યુએટ', 500000, '2020-06-11', 1, '2021-06-20 17:28:07'),
(5, 'VORA HITESHBHAI HIRABHAI', '9586511324', 'SANATHALI', 'JASDAN', 'Rajkot', 'MAIN BAZAR SANATHALI TA JASDAN DIS RAJKOT', '૫શુ આહાર', 500000, 'ધોરણ - ૧૦', 500000, '2020-06-11', 1, '2021-06-20 17:28:07'),
(6, 'VORA JAGDISH SHAMBHUBHAI', '9825980588', 'SANTHALI', 'JASDAN', 'Rajkot', 'TO- SANTHALI, TA- JASDAN, DIST- RAJKOT', '૫શુ આહાર', 500000, 'ગ્રેજ્યુએટ', 500000, '2020-06-11', 1, '2021-06-20 17:28:07'),
(7, 'RAMANI BHAVESH VALLABHBHAI', '9974403026', 'LILAPUR', 'JASDAN', 'Rajkot', 'AT LILAPUR TA JASDAN DIST RAJKOT 360055', '૫શુ આહાર', 500000, 'ધોરણ - ૮', 500000, '2020-06-11', 1, '2021-06-20 17:28:07'),
(8, 'TADHANI DALSUKHBHAI BHAVANBHAI', '9737149787', 'PANCHAVADA', 'JASDAN', 'Rajkot', 'AT PANCHAVADA TA JASDAN DIST RAJKOT', '૫શુ આહાર', 500000, 'ગ્રેજ્યુએટ', 500000, '2020-06-11', 1, '2021-06-20 17:28:07'),
(9, 'SAKHIYA NILESHBHAI DEVJIBHAI', '9725461555', 'JASDAN', 'JASDAN', 'Rajkot', 'AT JASDAN GANGA BHUVAN SARDAR PATEL NAGAR NO.2 TA JASDAN DIST RAJKOT', '૫શુ આહાર', 500000, 'ધોરણ - ૮', 500000, '2020-06-11', 1, '2021-06-20 17:28:07'),
(10, 'KAKADIYA HARSHAD KALYANBHAI', '9723668907', 'LILAPUR', 'JASDAN', 'Rajkot', 'AT.LILAPUR TA.JASDAN DIS.RAJKOT OPP.BUS STAND', '૫શુ આહાર', 500000, 'ધોરણ - ૮', 500000, '2020-06-11', 1, '2021-06-20 17:28:07'),
(11, 'VAGHASIYA ARUNBHAI JENTIBHAI', '9925940020', 'JASDAN', 'JASDAN', 'Rajkot', 'KILASH NAGAR2 JASDAN,RAJKOT', '૫શુ આહાર', 500000, 'ધોરણ - ૧૦', 500000, '2020-06-11', 1, '2021-06-20 17:28:07'),
(12, 'VADODARIYA DILIP PANKAJ BHAI', '9978680980', 'SANTHALI', 'JASDAN', 'Rajkot', 'SANTHALI', '૫શુ આહાર', 500000, 'ધોરણ - ૧૨', 500000, '2020-06-11', 1, '2021-06-20 17:28:07'),
(13, 'DOBARIYA BHAVESHBHAI NANJIBHAI', '9106231112', 'LILAPUR', 'JASDAN', 'Rajkot', 'AT LILAPUR BAJRANG PLOT TA JASDAN DIST RAJKOT', '૫શુ આહાર', 500000, 'ધોરણ - ૧૦', 500000, '2020-06-11', 1, '2021-06-20 17:28:07'),
(14, 'YAGNIK GHANSHYAMBHAI RADADIYA', '7600558964', 'GOKHALANA', 'JASDAN', 'Rajkot', 'AT GOKHALANA TA JASDAN DIST RAJKOT 360050', '૫શુ આહાર', 500000, 'ધોરણ - ૧૨', 500000, '2020-06-11', 1, '2021-06-20 17:28:07'),
(15, 'DOBARIYA MAYUR PRAVINBHAI', '9898969463', 'LILAPUR', 'JASDAN', 'Rajkot', 'AT.LILAPUR TA.JASDAN DIS.RAJKOT', '૫શુ આહાર', 500000, 'ધોરણ - ૧૨', 500000, '2020-06-11', 1, '2021-06-20 17:28:07'),
(16, 'RAMANI ASHVINBHAI BABUBHAI', '9898257338', 'JASDAN', 'JASDAN', 'Rajkot', 'AT JASDAN CHITALIYA ROAD HIRPARA NAGAR 1 TA JASDAN', 'ઈલેક્ટ્રોનીક્સ ગુડસ સેલ્સ એન્ડ સર્વિસ', 500000, 'ધોરણ - ૯', 500000, '2020-06-11', 1, '2021-06-20 17:28:07'),
(17, 'RAMOLIYA ASWINBHAI PARBATBHAI', '9904193273', 'SANATHALI', 'JASDAN', 'Rajkot', 'PLOT VISTAR AT.SANATHALI TA. JASDAN', 'ઈલેક્ટ્રોનીક્સ ગુડસ સેલ્સ એન્ડ સર્વિસ', 500000, 'પોસ્ટ ગ્રેજ્યુએટ', 500000, '2020-06-11', 1, '2021-06-20 17:28:07'),
(18, 'RAMANI DILIPBHAI BABUBHAI', '9276815282', 'JASDAN', 'JASDAN', 'Rajkot', 'AT JASDAN HIRPARA NAGAR STREET NO 1 CHITALIYA ROAD JASDAN TA JASDAN DIST RAJKOT 360050', 'ઈલેક્ટ્રોનીક્સ ગુડસ સેલ્સ એન્ડ સર્વિસ', 500000, 'ધોરણ - ૯', 500000, '2020-06-11', 1, '2021-06-20 17:28:07'),
(19, 'KAMANI SAVAN MANSUKHBHAI', '9558659493', 'LILAPUR', 'JASDAN', 'Rajkot', 'AT.LILAPUR TA.JASDAN DIS.RAJKOT', 'ઈલેક્ટ્રોનીક્સ ગુડસ સેલ્સ એન્ડ સર્વિસ', 500000, 'ધોરણ - ૧૦', 500000, '2020-06-11', 1, '2021-06-20 17:28:07'),
(20, 'VEKARIYA JAYRAJBHAI JAGABHAI', '9909204432', 'VIRNAGAR', 'JASDAN', 'Rajkot', 'AT VIRNAGAR TANKAVALI STREET TA JASDAN DIST RAJKOT 360060', 'એગ્રોસર્વિસ સ્ટેશન(જંતુનાશક દવાઓ તેમજ બિયારણની દુકાન)', 750000, 'ધોરણ - ૯', 750000, '2020-06-11', 1, '2021-06-20 17:28:07'),
(21, 'SHIROYA KIRITBHAI KHODABHAI', '7359929581', 'SANATHALI', 'JASDAN', 'Rajkot', 'SANATHALI', 'એગ્રોસર્વિસ સ્ટેશન(જંતુનાશક દવાઓ તેમજ બિયારણની દુકાન)', 750000, 'ધોરણ - ૮', 750000, '2020-06-11', 1, '2021-06-20 17:28:07'),
(22, 'MOVALIYA KALPESHKUMAR BABUBHAI', '9824425371', 'JASDAN', 'JASDAN', 'Rajkot', 'AT JASDAN GITA NAGAR AND AMBIKA NAGAR TA JASDAN DIST RAJKOT 360050', 'ઓટોમોબાઈલ્સ સર્વિસ & સ્પેરપાટર્સની દુકાન', 500000, 'ધોરણ - ૧૦', 500000, '2020-06-11', 1, '2021-06-20 17:28:07'),
(23, 'KAKADIYA PARESH KALYANBHAI', '9724608168', 'LILAPUR', 'JASDAN', 'Rajkot', 'AT.LILAPUR TA.JASDAN DIS.RAJKOT OPP.BUS STAND', 'કરીયાણા દુકાન /પ્રોવિઝન સ્ટોર/કટલરી સ્ટોર', 500000, 'ગ્રેજ્યુએટ', 500000, '2020-06-11', 1, '2021-06-20 17:28:07'),
(24, 'DOBARIYA ROHITBHAI GANDUBHAI', '9974579033', 'LILAPUR', 'JASDAN', 'Rajkot', 'AT LILAPUR PLOT VISTAR TA JASDAN DIST RAJKOT 360050', 'કરીયાણા દુકાન /પ્રોવિઝન સ્ટોર/કટલરી સ્ટોર', 500000, 'ધોરણ - ૧૨', 500000, '2020-06-11', 1, '2021-06-20 17:28:07'),
(25, 'VAGHASIYA MAHESH JENTIBHAI', '9979888876', 'JASDAN', 'JASDAN', 'Rajkot', 'KAILASHNAGAR 2, OPP ST BUS STATION,AT TA -JASDAN DIS- RAJKOT', 'કરીયાણા દુકાન /પ્રોવિઝન સ્ટોર/કટલરી સ્ટોર', 500000, 'ધોરણ - ૧૦', 500000, '2020-06-11', 1, '2021-06-20 17:28:07'),
(26, '.RAMANI KIRITBHAI VALLABHABHAI', '9664659511', 'LILAPUR', 'JASDAN', 'Rajkot', 'AT LILAPUR TA JASDAN DI RAJKOT', 'કરીયાણા દુકાન /પ્રોવિઝન સ્ટોર/કટલરી સ્ટોર', 500000, 'ધોરણ - ૧૦', 750000, '2020-06-11', 1, '2021-06-20 17:28:07'),
(27, 'VASANI SATISH BHUPATBHAI', '9909281781', 'SANATHALI', 'JASDAN', 'Rajkot', 'AT SANATHALI TA JASDAN DI RAJKOT', 'કરીયાણા દુકાન /પ્રોવિઝન સ્ટોર/કટલરી સ્ટોર', 500000, 'પોસ્ટ ગ્રેજ્યુએટ', 500000, '2020-06-11', 1, '2021-06-20 17:28:07'),
(28, 'SHIROYA RAJESHBHAI ARVINDBHAI', '9998196571', 'SANATHALI', 'JASDAN', 'Rajkot', 'AT SANATHALI TA JASDAN DIST RAJKOT', 'કરીયાણા દુકાન /પ્રોવિઝન સ્ટોર/કટલરી સ્ટોર', 500000, 'ગ્રેજ્યુએટ', 500000, '2020-06-11', 1, '2021-06-20 17:28:07'),
(29, 'MAYUR VINODBHAI HIRANI', '9824818127', 'KHARACHIYA JAM', 'JASDAN', 'Rajkot', 'AT KHARACHIYA JAM TA JASDAN DIST RAJKOT', 'કરીયાણા દુકાન /પ્રોવિઝન સ્ટોર/કટલરી સ્ટોર', 500000, 'ધોરણ - ૧૦', 500000, '2020-06-11', 1, '2021-06-20 17:28:08'),
(30, 'VASANI ANJALI HASMUKHBHAI', '9974416916', 'SANATHALI', 'JASDAN', 'Rajkot', 'TO-SANATHALI TA-JASDAN DIS-RAJKOT', 'કરીયાણા દુકાન /પ્રોવિઝન સ્ટોર/કટલરી સ્ટોર', 500000, 'પોસ્ટ ગ્રેજ્યુએટ', 500000, '2020-06-11', 1, '2021-06-20 17:28:08'),
(31, 'HIRPARA KAMLESHBHAI MADHABHAI', '9427238761', 'JASDAN', 'JASDAN', 'Rajkot', 'AT JASDAN JUNO KOTHI ROAD RAM NAGAR TA JASDAN DIST RAJKOT', 'કરીયાણા દુકાન /પ્રોવિઝન સ્ટોર/કટલરી સ્ટોર', 500000, 'ધોરણ - ૨', 500000, '2020-06-11', 1, '2021-06-20 17:28:08'),
(32, 'RADADIYA GHANSHYAMBHAI BHIMJIBHAI', '9724428244', 'GOKHALANA', 'JASDAN', 'Rajkot', 'AT GOKHLANA TA JASDAN DIST RAJKOT 360050', 'કરીયાણા દુકાન /પ્રોવિઝન સ્ટોર/કટલરી સ્ટોર', 500000, 'ધોરણ - ૧૨', 500000, '2020-06-11', 1, '2021-06-20 17:28:08'),
(33, 'VAGHASIYA SAGAR JENTIBHAI', '9913291490', 'JASDAN', 'JASDAN', 'Rajkot', 'KILASHNAGER-2 JAASDAN,RAJKOT', 'કેટરીંગ', 500000, 'ધોરણ - ૧૦', 500000, '2020-06-11', 1, '2021-06-20 17:28:08'),
(34, 'KHANPARA DENISH DHANJIBHAI', '9723521221', 'JASDAN', 'JASDAN', 'Rajkot', 'TO JASDAN AMBIKA NAGAR BH NAYAY MANDIR ATKOT ROAD TA JASDAN DIST RAJKOT', 'કોચીંગ ક્લાસ', 500000, 'પોસ્ટ ગ્રેજ્યુએટ', 500000, '2020-06-11', 1, '2021-06-20 17:28:08'),
(35, 'MALAVIYA BHAVESHKUMAR NANJIBHAI', '9428259644', 'ISHWARIYA', 'JASDAN', 'Rajkot', 'AT- ISHWARIYA', 'કોચીંગ ક્લાસ', 500000, 'ગ્રેજ્યુએટ', 500000, '2020-06-11', 1, '2021-06-20 17:28:08'),
(36, 'TADHANI SAGAR VITTHALBHAI', '9574422099', 'PANCHAVADA', 'JASDAN', 'Rajkot', 'PANCHAVADA, ATKOT, TA. JASDAN', 'કોમ્પ્યુટર (સાયબર કાફે) /એકાઉન્ટીંગ GST, Tally Software Office', 275000, 'ગ્રેજ્યુએટ', 275000, '2020-06-11', 1, '2021-06-20 17:28:08'),
(37, 'RAMANI SANKET PRAVINBHAI', '9879173956', 'ATKOT', 'JASDAN', 'Rajkot', 'RADHANAGAR, AT ATKOT', 'કોમ્પ્યુટર (સાયબર કાફે) /એકાઉન્ટીંગ GST, Tally Software Office', 300000, 'ગ્રેજ્યુએટ', 300000, '2020-06-11', 1, '2021-06-20 17:28:08'),
(38, 'DHOLARIYA DIPESH MANSUKHBHAI', '9099968908', 'JASDAN', 'JASDAN', 'Rajkot', 'AT JASDAN GOKHLANA ROAD RAMESHWAR NAGAR TA JASDAN DIST RAJKOT 360050', 'ગીફટ આર્ટીકલ / ડેકોરેટીવગુડસ', 500000, 'ગ્રેજ્યુએટ', 500000, '2020-06-11', 1, '2021-06-20 17:28:08'),
(39, 'SIROYA NITESHBHAI BHIKHUBHAI', '9879597474', 'SANATHALI', 'JASDAN', 'Rajkot', 'PLOT VISTAR,', 'ઝેરોક્ષ / લેમીનેશન/સ્પાઇરલ બાઇડીંગની દુકાન', 250000, 'ધોરણ - ૧૦', 250000, '2020-06-11', 1, '2021-06-20 17:28:08'),
(40, 'KHANPARA BALRAM DHANJIBHAI', '9033313561', 'JASDAN', 'JASDAN', 'Rajkot', 'TO JASDAN AMBIKA NAGAR BH NAYAY MANDIR ATKOT ROAD JASDAN TA JASDAN DIST RAJKOT', 'ફોટો સ્ટુડિયો / કેમેરા/ વિડિયોગ્રાફી /CCTV કેમેરા સેલ્સ અને સર્વિસ', 500000, 'પોસ્ટ ગ્રેજ્યુએટ', 500000, '2020-06-11', 1, '2021-06-20 17:28:08'),
(41, 'KAPADIYA DILIP BABUBHAI', '9662969430', 'VIRNAGAR', 'JASDAN', 'Rajkot', 'TANAKA VALI SERI,AT VIRNAGAR TA JASDAN DIS RAJKOT', 'ફોટો સ્ટુડિયો / કેમેરા/ વિડિયોગ્રાફી /CCTV કેમેરા સેલ્સ અને સર્વિસ', 500000, 'ગ્રેજ્યુએટ', 500000, '2020-06-11', 1, '2021-06-20 17:28:08'),
(42, 'KAKADIYA RASIK BHIKHABHAI', '9909123830', 'LILAPUR', 'JASDAN', 'Rajkot', 'PARA BAJAR AT LILAPUR TA JASDAN DIS RAJKOR', 'ફોટો સ્ટુડિયો / કેમેરા/ વિડિયોગ્રાફી /CCTV કેમેરા સેલ્સ અને સર્વિસ', 500000, 'ધોરણ - ૮', 500000, '2020-06-11', 1, '2021-06-20 17:28:08'),
(43, 'NABHOYA ASHOKKUMAR RAMESHBHAI', '9427268697', 'JANGVAD', 'JASDAN', 'Rajkot', 'AT JANGVAD TA JASDAN DIST RAJKOT', 'ફોટો સ્ટુડિયો / કેમેરા/ વિડિયોગ્રાફી /CCTV કેમેરા સેલ્સ અને સર્વિસ', 500000, 'ગ્રેજ્યુએટ', 500000, '2020-06-11', 1, '2021-06-20 17:28:08'),
(44, 'SUVAGIYA PARSOTAMBHAI DHANJIBHAI', '9909067916', 'BHADLA', 'JASDAN', 'Rajkot', 'AT BHADLA TA JASDAN DIST RAJKOT', 'ફોટો સ્ટુડિયો / કેમેરા/ વિડિયોગ્રાફી /CCTV કેમેરા સેલ્સ અને સર્વિસ', 500000, 'ધોરણ - ૧૨', 500000, '2020-06-11', 1, '2021-06-20 17:28:08'),
(45, 'SUVAGIYA CHANDRAKANT GOVINDBHAI', '9099438569', 'BHADLA', 'JASDAN', 'Rajkot', 'AT BHADLA TA JASDAN DIST RAJKOT', 'ફોટો સ્ટુડિયો / કેમેરા/ વિડિયોગ્રાફી /CCTV કેમેરા સેલ્સ અને સર્વિસ', 500000, 'ધોરણ - ૧૦', 500000, '2020-06-11', 1, '2021-06-20 17:28:08'),
(46, 'RUPARELIYA MAYURBHAI LALAJIBHAI', '9016474612', 'SANATHALI', 'JASDAN', 'Rajkot', 'VASAVAD ROAD , RUPARELIYA SOSAYATI', 'ફોટો સ્ટુડિયો / કેમેરા/ વિડિયોગ્રાફી /CCTV કેમેરા સેલ્સ અને સર્વિસ', 500000, 'ધોરણ - ૧૨', 500000, '2020-06-11', 1, '2021-06-20 17:28:08'),
(47, 'VORA HARESH UKABHAI', '9825338978', 'SANATHALI', 'JASDAN', 'Rajkot', 'SWAMINARAN MANDIR , CHORAVALI SHERI', 'ફોટો સ્ટુડિયો / કેમેરા/ વિડિયોગ્રાફી /CCTV કેમેરા સેલ્સ અને સર્વિસ', 500000, 'ધોરણ - ૧૨', 500000, '2020-06-11', 1, '2021-06-20 17:28:08'),
(48, 'PANSURIYA DHARMISTHABEN RAMESHBHAI', '9924000340', 'JASDAN', 'JASDAN', 'Rajkot', 'AT JASDAN SARDAR PATEL NAGAR 3 B/H GANGA BHUVAN TA JASDAN DIST RAJKOT 360050', 'બ્યુટી પાર્લર/ સલૂન', 250000, 'ગ્રેજ્યુએટ', 250000, '2020-06-11', 1, '2021-06-20 17:28:08'),
(49, 'CHOTHANI BHAVESH MANJIBHAI', '9687425007', 'NAVA JASAPAR', 'JASDAN', 'Rajkot', 'NAVA JASAPAR, TA. JASDAN', 'બ્રાસ ના સ્પેરપાર્ટસ અને હાર્ડવેર', 325000, 'પોસ્ટ ગ્રેજ્યુએટ', 325000, '2020-06-11', 1, '2021-06-20 17:28:08'),
(50, 'DOBARIYA AMIT NARSINHBHAI', '9725605504', 'LILAPUR', 'JASDAN', 'Rajkot', 'AT LILAPUR BAJRANG PLOT TA JASDAN DIST RAJKOT 360050', 'બ્રાસ ના સ્પેરપાર્ટસ અને હાર્ડવેર', 500000, 'ગ્રેજ્યુએટ', 500000, '2020-06-11', 1, '2021-06-20 17:28:08'),
(51, 'TADHANI ASHVIN BHAVANBHAI', '9824478604', 'PANCHAVADA', 'JASDAN', 'Rajkot', 'AT PANCHAVADA TA JASDAN DIST RAJKOT', 'બ્રાસ ના સ્પેરપાર્ટસ અને હાર્ડવેર', 500000, 'ગ્રેજ્યુએટ', 500000, '2020-06-11', 1, '2021-06-20 17:28:08'),
(52, 'RAMANI ANIL KALYANBHAI', '9724778196', 'LILAPUR', 'JASDAN', 'Rajkot', 'AT LILAPUR TA JASDAN DIST RAJKOT', 'મંડપ ડેકોરેશન', 500000, 'ગ્રેજ્યુએટ', 500000, '2020-06-11', 1, '2021-06-20 17:28:08'),
(53, 'TADHANI RAJAN PRAVINBHAI', '9974464009', 'JASDAN', 'JASDAN', 'Rajkot', 'AT JASDAN ADAMJI ROAD STREET NO 4 TA JASDAN DIST RAJKOT', 'મંડપ ડેકોરેશન', 500000, 'ધોરણ - ૧૨', 500000, '2020-06-11', 1, '2021-06-20 17:28:08'),
(54, 'HARKHANI HARDIKBHAI RAVJIBHAI', '7990663176', 'JASDAN', 'JASDAN', 'Rajkot', 'AT JASDAN GITA NAGAR NEAR DENA BANKK TA JASDAN DIST RAJKOT', 'મેડીકલ સ્ટોર્સ', 750000, 'ગ્રેજ્યુએટ', 750000, '2020-06-11', 1, '2021-06-20 17:28:08'),
(55, 'BOGHARA MUKESH VALLABHBHAI', '9601920555', 'KAMLAPUR', 'JASDAN', 'Rajkot', 'PLOT AREA AT KAMLAPUR TA JASDAN DIST RAJKOT', 'મેડીકલ સ્ટોર્સ', 1000000, 'ગ્રેજ્યુએટ', 1000000, '2020-06-11', 1, '2021-06-20 17:28:08'),
(56, 'SAVALIYA JAGDISH DHIRUBHAI', '8000019261', 'KHARACHIYA JAM', 'JASDAN', 'Rajkot', 'AT KHARACHIYA JAM TA JASDAN DIST RAJKOT', 'મોબાઈલ શોપ', 500000, 'ધોરણ - ૭', 500000, '2020-06-11', 1, '2021-06-20 17:28:08'),
(57, 'PADMANI RAJESH BHOLABHAI', '7878868787', 'JASDAN', 'JASDAN', 'Rajkot', 'TO JASDAN SARDAR PATEL NAGAR GANGABHUVAN TA JASDAN', 'મોબાઈલ શોપ', 500000, 'ધોરણ - ૧૨', 500000, '2020-06-11', 1, '2021-06-20 17:28:08'),
(58, 'RAMANI JITENDRA ARVINDBHAI', '9974026414', 'JASDAN', 'JASDAN', 'Rajkot', 'AT JASDAN CHITALIYA ROAD HIRPARA NAGAR 2 TA JASDAN DIST RAJKOT 360050', 'મોબાઈલ શોપ', 500000, 'ધોરણ - ૧૦', 500000, '2020-06-11', 1, '2021-06-20 17:28:08'),
(59, 'UNDHAD DRISHANT MUKESHBHAI', '6355776355', 'JASDAN', 'JASDAN', 'Rajkot', 'AT JASDAN TA JASDAN DIST RAJKOT 360050', 'મોબાઈલ શોપ', 500000, 'ધોરણ - ૧૧', 500000, '2020-06-11', 1, '2021-06-20 17:28:09'),
(60, 'RAMANI JATIN PRAKASHBHAI', '8780118480', 'JASDAN', 'JASDAN', 'Rajkot', 'TO JASDAN SARDAR CHOWK OPP SARDAR SCHOOL TA JASDAN', 'મોબાઈલ શોપ', 500000, 'ધોરણ - ૧૨', 500000, '2020-06-11', 1, '2021-06-20 17:28:09'),
(61, 'JODHANI UTSAVKUMAR RAJESHBHAI', '9726880605', 'VINCHHIYA', 'JASDAN', 'Rajkot', 'SATYAJIT SOCIETY TO VINCHHIYA TA JASDAN DI RAJKOT', 'મોબાઈલ શોપ', 500000, 'ગ્રેજ્યુએટ', 500000, '2020-06-11', 1, '2021-06-20 17:28:09'),
(62, 'CHOVATIYA SAGAR JAYNTIBHAI', '8141231672', 'KHARACHIYA JAM', 'JASDAN', 'Rajkot', 'KHARACHIYA JAM, JASDAN,', 'મોબાઈલ શોપ', 500000, 'ગ્રેજ્યુએટ', 500000, '2020-06-11', 1, '2021-06-20 17:28:09'),
(63, 'LIMBASIYA ROHAN SANJAYBHAI', '7779000194', 'JASDAN', 'JASDAN', 'Rajkot', 'KHANPER ROAD KILAS NAGAR ,JASDAN 360050', 'મોબાઈલ શોપ', 500000, 'ધોરણ - ૧૦', 500000, '2020-06-11', 1, '2021-06-20 17:28:09'),
(64, 'HIRPARA BIPINBHAI JIVRAJBHAI', '9824221402', 'JASDAN', 'JASDAN', 'Rajkot', 'GANGABHUVAN HARIKRUSHN NAGAR 2 AT-JASDAN', 'રેડીમેઈડ ગારમેન્ટસ સ્ટોર્સ/ સાડી સ્ટોર્સ/શુટિંગ-શટીગ', 500000, 'ધોરણ - ૧૦', 500000, '2020-06-11', 1, '2021-06-20 17:28:09'),
(65, 'KAVATHIYA DHARMESH RANCHHODBHAI', '7600137701', 'JASDAN', 'JASDAN', 'Rajkot', 'AT JASDAN DHOLARIYA STREET JASDAN - 360050', 'રેડીમેઈડ ગારમેન્ટસ સ્ટોર્સ/ સાડી સ્ટોર્સ/શુટિંગ-શટીગ', 500000, 'ધોરણ - ૧૨', 500000, '2020-06-11', 1, '2021-06-20 17:28:09'),
(66, 'RAMANI MUKESH CHHAGANBHAI', '8511660700', 'KAMLAPUR', 'JASDAN', 'Rajkot', '', 'રેડીમેઈડ ગારમેન્ટસ સ્ટોર્સ/ સાડી સ્ટોર્સ/શુટિંગ-શટીગ', 500000, 'ગ્રેજ્યુએટ', 500000, '2020-06-11', 1, '2021-06-20 17:28:09'),
(67, 'RAMANI RAMESHBHAI POPATBHAI', '8401120256', 'LILAPUR', 'JASDAN', 'Rajkot', '', 'રેડીમેઈડ ગારમેન્ટસ સ્ટોર્સ/ સાડી સ્ટોર્સ/શુટિંગ-શટીગ', 500000, 'ધોરણ - ૧૦', 500000, '2020-06-11', 1, '2021-06-20 17:28:09'),
(68, 'MALAVIYA NEETABEN BHAVESHKUMAR', '9429249250', 'ISHWARIYA', 'JASDAN', 'Rajkot', '', 'રેડીમેઈડ ગારમેન્ટસ સ્ટોર્સ/ સાડી સ્ટોર્સ/શુટિંગ-શટીગ', 500000, 'ગ્રેજ્યુએટ', 500000, '2020-06-11', 1, '2021-06-20 17:28:09'),
(69, 'KAKADIYA ASWINBHAI DAMJIBHAI', '9974401714', 'LILAPUR', 'JASDAN', 'Rajkot', '', 'રેડીમેઈડ ગારમેન્ટસ સ્ટોર્સ/ સાડી સ્ટોર્સ/શુટિંગ-શટીગ', 500000, 'ધોરણ - ૧૦', 500000, '2020-06-11', 1, '2021-06-20 17:28:09'),
(70, 'SIDHAPARA PRAKASH CHHAGANBHAI', '9265155995', 'JANGVAD', 'JASDAN', 'Rajkot', '', 'લુહારી કામ/ફેબ્રીકેશન / વેલ્ડીંગ વર્કસની દુકાન', 250000, 'ધોરણ - ૮', 250000, '2020-06-11', 1, '2021-06-20 17:28:09'),
(71, 'PANSURIYA NILESHBHAI JAYANTIBHAI', '8780904030', 'PANCHAVADA', 'JASDAN', 'Rajkot', '', 'સિરામિક્સ/બોક્સ/બિલ્ડીંગ મટીરીયલ્સ નો ધંધો', 500000, 'ધોરણ - ૧૦', 500000, '2020-06-11', 1, '2021-06-20 17:28:09'),
(72, 'RAMANI BHAVESHBHAI RAGHAVBHAI', '9106589592', 'LILAPUR', 'JASDAN', 'Rajkot', 'VICHHIYA ROAD', 'સિરામિક્સ/બોક્સ/બિલ્ડીંગ મટીરીયલ્સ નો ધંધો', 500000, 'ધોરણ - ૯', 500000, '2020-06-11', 1, '2021-06-20 17:28:09'),
(73, 'RAMANI KETAN MANJIBHAI', '9898910592', 'LILAPUR', 'JASDAN', 'Rajkot', '', 'સિરામિક્સ/બોક્સ/બિલ્ડીંગ મટીરીયલ્સ નો ધંધો', 500000, 'ધોરણ - ૧૨', 500000, '2020-06-11', 1, '2021-06-20 17:28:09'),
(74, 'MALAVIYA HARSHAD VINUBHAI', '8200223823', 'ISHWARIA', 'JASDAN', 'Rajkot', '', 'સેન્ટીંગ કામના સાધનો (મીક્ષર મશીન સાથે)', 450000, 'ગ્રેજ્યુએટ', 450000, '2020-06-11', 1, '2021-06-20 17:28:09'),
(75, 'TADHANI HASMUKHBHAI DEVSHIBHAI', '9978997258', 'PANCHAVDA', 'JASDAN', 'Rajkot', '', 'સેન્ટીંગ કામના સાધનો (મીક્ષર મશીન સાથે)', 500000, 'ધોરણ - ૯', 500000, '2020-06-11', 1, '2021-06-20 17:28:09'),
(76, 'MARAKANA LALITBHAI UKABHAI', '9978917734', 'ISHWARIYA', 'JASDAN', 'Rajkot', 'AT-ISHWARIYA,207-KHODIYAR KRUPA, NEAR PRIMARY SCHOOL - GUJARAT-360040.', 'સેન્ટીંગ કામના સાધનો (મીક્ષર મશીન સાથે)', 500000, 'ગ્રેજ્યુએટ', 500000, '2020-06-11', 1, '2021-06-20 17:28:09'),
(77, 'RAMANI DHARMESHBHAI ARVINDBHAI', '9974133669', 'JASDAN', 'JASDAN', 'Rajkot', 'AT JASDAN CHITALIYA ROAD HIRPARA NAGAR 2 - 360050', 'સોના-ચાંદીનો ઘંઘો', 750000, 'ધોરણ - ૧૨', 750000, '2020-06-11', 1, '2021-06-20 17:28:09'),
(78, 'CHHAYANI PIYUSH RAMESHBHAI', '9998443778', 'JASDAN', 'JASDAN', 'Rajkot', 'AT JASDAN ATKOTROAD VADLA VADI OPP TAXSHILA SCHOOL- 360050', 'સોના-ચાંદીનો ઘંઘો', 750000, 'ગ્રેજ્યુએટ', 750000, '2020-06-11', 1, '2021-06-20 17:28:09'),
(79, 'PIDHADIYA DIVYESHKUMAR MANSUKHBHAI', '7874746915', 'JIVAPAR', 'JASDAN', 'Rajkot', '', 'સોના-ચાંદીનો ઘંઘો', 750000, 'ધોરણ - ૧૨', 750000, '2020-06-11', 1, '2021-06-20 17:28:09'),
(80, 'RAMANI HARSH BHAVESHBHAI', '7046456859', 'LILAPUR', 'JASDAN', 'Rajkot', 'LILAPUR VINCHHIYA ROAD -360050', 'સોના-ચાંદીનો ઘંઘો', 750000, 'ધોરણ - ૧૨', 750000, '2020-06-11', 1, '2021-06-20 17:28:09'),
(81, 'RAMANI CHANKIT RAMESHBHAI', '9978544454', 'LILAPUR', 'JASDAN', 'Rajkot', 'LILAPUR VINCHHIYA ROAD -360050', 'સોના-ચાંદીનો ઘંઘો', 750000, 'ગ્રેજ્યુએટ', 750000, '2020-06-11', 1, '2021-06-20 17:28:09'),
(82, 'Khokhriya Rahul jitendra bhai', '9662836615', '', '', '', '454,lalita park sosayti katargam, Surat.', 'Vides abhyash Lon ', 1500000, '', 0, '2021-02-22', 1, '2021-06-20 17:31:15');

-- --------------------------------------------------------

--
-- Table structure for table `loan_transcription`
--

CREATE TABLE `loan_transcription` (
  `id` int(11) NOT NULL,
  `txt_id` varchar(100) NOT NULL,
  `bill_id` varchar(100) NOT NULL,
  `uid` int(11) NOT NULL,
  `amount` double NOT NULL,
  `notes` longtext NOT NULL,
  `by_uid` int(11) NOT NULL,
  `by_brand` varchar(100) NOT NULL,
  `by_model` varchar(100) NOT NULL,
  `type` enum('cr','dr') NOT NULL DEFAULT 'cr',
  `bank_type` enum('cash','bank') NOT NULL DEFAULT 'cash',
  `status` int(1) NOT NULL DEFAULT '1',
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `loan_transcription`
--

INSERT INTO `loan_transcription` (`id`, `txt_id`, `bill_id`, `uid`, `amount`, `notes`, `by_uid`, `by_brand`, `by_model`, `type`, `bank_type`, `status`, `date`) VALUES
(1, '16189821247717', 'L1', 213, 25000, 'ચેક નં 600001', 2, 'OPPO', 'CPH2001', 'dr', 'bank', 1, '2021-04-21 01:15:24'),
(4, '16190079157431', 'L2', 301, 9950, 'ચેક.નં 00002\n10/3/21', 2, 'OPPO', 'CPH2001', 'dr', 'bank', 1, '2021-04-21 08:25:15'),
(5, '16190115865600', 'L3', 213, 2000, 'રોકડ ટ્રાન્સફર બેન્ક ખાતાં માં ટ્રાન્સફર', 2, 'OPPO', 'CPH2001', 'cr', 'bank', 1, '2021-04-21 09:26:26'),
(6, '16208896909972', 'L4', 213, 1000, '10/4/21  હપ્તો ૩', 2, 'OPPO', 'CPH2001', 'cr', 'bank', 1, '2021-05-13 03:08:10'),
(7, '16227186056550', 'L5', 14, 175000, 'ડો.25000+150000 ગીરધરભાઇ', 2, 'OPPO', 'CPH2001', 'dr', 'cash', 1, '2021-06-03 07:10:05'),
(8, '16227186679579', 'L6', 14, 175000, 'રમેશ કડી હસ્તે 3/6/21', 2, 'OPPO', 'CPH2001', 'cr', 'cash', 1, '2021-06-03 07:11:07'),
(9, '16227187882613', 'L7', 14, 175000, 'રમેશ કડી હસ્તે જમા આવેલ છે.', 2, 'OPPO', 'CPH2001', 'cr', 'cash', 1, '2021-06-03 07:13:08'),
(10, '16232197102205', 'L8', 14, 350000, 'રમેશ કડી હસ્તે આવેલ રકમ પરત ચુકવણી કરી.', 2, 'OPPO', 'CPH2001', 'dr', 'cash', 1, '2021-06-09 02:21:50'),
(11, '16232392087841', 'L9', 14, 50000, 'રેતી ના ચુકવવા આપેલ 175-50=125 બાકી', 2, 'OPPO', 'CPH2001', 'cr', 'cash', 1, '2021-06-09 07:46:48'),
(12, '16233124469835', 'L10', 213, 1000, 'જમા કરવામાં આવે છે', 2, 'OPPO', 'CPH2001', 'cr', 'bank', 1, '2021-06-10 04:07:26'),
(13, '16236805885694', 'L11', 14, 50000, 'SBI self\n250-200', 2, 'OPPO', 'CPH2001', 'cr', 'cash', 1, '2021-06-14 10:23:08'),
(14, '16240803823892', 'L12', 14, 75000, 'Kadi pasethi Abel\nKeval 200000-125000 Dinesh Bhai na khate jama karavya', 2, 'OPPO', 'CPH2001', 'cr', 'cash', 1, '2021-06-19 01:26:22');

-- --------------------------------------------------------

--
-- Table structure for table `mainAdmin_transcription`
--

CREATE TABLE `mainAdmin_transcription` (
  `id` int(11) NOT NULL,
  `txt_id` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `bill_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `uid` int(11) NOT NULL,
  `amount` double NOT NULL,
  `notes` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `by_uid` int(11) NOT NULL,
  `by_brand` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `by_model` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `bank_type` enum('bank','cash') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'cash',
  `type` enum('cr','dr') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'cr',
  `time` time NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mainAdmin_transcription`
--

INSERT INTO `mainAdmin_transcription` (`id`, `txt_id`, `bill_id`, `uid`, `amount`, `notes`, `by_uid`, `by_brand`, `by_model`, `bank_type`, `type`, `time`, `status`, `date`) VALUES
(1, '16189817587452', 'M1', 214, 151000, 'Direct payment for member id: PSB195', 2, 'OPPO', 'CPH2001', 'bank', 'cr', '00:00:00', 1, '2021-04-21 01:09:18'),
(2, '16189818088818', 'M2', 253, 10000, 'Direct payment for member id: PSB234', 2, 'OPPO', 'CPH2001', 'bank', 'cr', '00:00:00', 1, '2021-04-21 01:10:08'),
(3, '16189853215580', 'M3', 8, 846024, 'રોકડ ટ્રાન્સફર', 2, 'OPPO', 'CPH2001', 'cash', 'cr', '00:00:00', 1, '2021-04-21 06:13:12'),
(4, '16189858258165', 'M4', 236, 11000, 'Direct payment for member id: PSB217', 2, 'OPPO', 'CPH2001', 'bank', 'cr', '00:00:00', 1, '2021-04-21 02:17:05'),
(5, '16190121994131', 'M5', 2, 72070, 'રોકડ ટ્રાન્સફર બેન્ક ખાતાં માં જમા', 2, 'OPPO', 'CPH2001', 'cash', 'dr', '00:00:00', 1, '2021-04-21 09:36:39'),
(6, '16190121994131', 'M6', 2, 72070, 'રોકડ ટ્રાન્સફર બેન્ક ખાતાં માં જમા', 2, 'OPPO', 'CPH2001', 'bank', 'cr', '00:00:00', 1, '2021-04-21 09:36:39'),
(11, '16191717109378', 'M7', 10, 2669, 'Direct payment for member id: PSB5', 2, 'OPPO', 'CPH2001', 'bank', 'cr', '00:00:00', 1, '2021-04-23 05:55:10'),
(12, '16191718531633', 'M8', 10, 117000, 'Direct payment for member id: PSB5', 2, 'OPPO', 'CPH2001', 'bank', 'cr', '00:00:00', 1, '2021-04-23 05:57:33'),
(13, '16192481051896', 'M9', 10, 326, 'Direct payment for member id: PSB5', 2, 'OPPO', 'CPH2001', 'bank', 'cr', '00:00:00', 1, '2021-04-24 03:08:25'),
(14, '16201224312932', 'M10', 14, 150000, 'Direct payment for member id: PSB7', 2, 'OPPO', 'CPH2001', 'cash', 'cr', '00:00:00', 1, '2021-05-04 06:00:31'),
(15, '16206275208139', 'M11', 213, 1000, 'Direct payment for member id: PSB194', 2, 'OPPO', 'CPH2001', 'bank', 'cr', '00:00:00', 1, '2021-05-10 02:18:40'),
(16, '16206471924349', 'M12', 14, 50000, 'Direct payment for member id: PSB7', 2, 'OPPO', 'CPH2001', 'cash', 'cr', '00:00:00', 1, '2021-05-10 07:46:32'),
(17, '16207438268401', 'M13', 10, 5000, 'Direct payment for member id: PSB5', 2, 'OPPO', 'CPH2001', 'cash', 'cr', '00:00:00', 1, '2021-05-11 10:37:06'),
(18, '16216870452745', 'M14', 14, 200000, 'Direct payment for member id: PSB7', 2, 'OPPO', 'CPH2001', 'cash', 'cr', '00:00:00', 1, '2021-05-22 08:37:25'),
(19, '16219434548736', 'M15', 328, 11111, 'Direct payment for member id: PSB308', 2, 'OPPO', 'CPH2001', 'bank', 'cr', '00:00:00', 1, '2021-05-25 07:50:54'),
(20, '16220136002091', 'M16', 214, 100000, 'Direct payment for member id: PSB195', 2, 'OPPO', 'CPH2001', 'bank', 'cr', '00:00:00', 1, '2021-05-26 03:20:00'),
(21, '16221132937925', 'M17', 2, 55000, 'Self chak ', 2, 'OPPO', 'CPH2001', 'bank', 'dr', '00:00:00', 1, '2021-05-27 07:01:33'),
(22, '16221132937925', 'M18', 2, 55000, 'Self chak ', 2, 'OPPO', 'CPH2001', 'cash', 'cr', '00:00:00', 1, '2021-05-27 07:01:33'),
(24, '16221177899198', 'M19', 8, 36400, '', 2, 'OPPO', 'CPH2001', 'cash', 'cr', '00:00:00', 1, '2021-05-27 08:16:29'),
(25, '16221407339018', 'M20', 2, 150000, 'Gird...chay..... check no.....', 2, 'OPPO', 'CPH2001', 'bank', 'dr', '00:00:00', 1, '2021-05-27 14:38:53'),
(26, '16221407339018', 'M21', 2, 150000, 'Gird...chay..... check no.....', 2, 'OPPO', 'CPH2001', 'cash', 'cr', '00:00:00', 1, '2021-05-27 14:38:53'),
(27, '16222631336472', 'M22', 10, 310, 'Direct payment for member id: PSB5', 2, 'OPPO', 'CPH2001', 'cash', 'cr', '00:00:00', 1, '2021-05-29 00:38:53'),
(30, '16227201944209', 'M23', 361, 600000, 'Direct payment for member id: PSB341', 2, 'OPPO', 'CPH2001', 'bank', 'cr', '00:00:00', 1, '2021-06-03 07:36:34'),
(31, '16232196042613', 'M24', 2, 400000, 'Self chak\n7/6/21', 2, 'OPPO', 'CPH2001', 'bank', 'dr', '00:00:00', 1, '2021-06-09 02:20:04'),
(32, '16232196042613', 'M25', 2, 400000, 'Self chak\n7/6/21', 2, 'OPPO', 'CPH2001', 'cash', 'cr', '00:00:00', 1, '2021-06-09 02:20:04'),
(33, '16233250201360', 'M26', 28, 3200, 'Direct payment for member id: PSB21', 2, 'OPPO', 'CPH2001', 'cash', 'cr', '00:00:00', 1, '2021-06-10 07:37:00'),
(34, '16235660501131', 'M27', 28, 100, 'Direct payment for member id: PSB21', 2, 'OPPO', 'CPH2001', 'cash', 'cr', '00:00:00', 1, '2021-06-13 02:34:10'),
(35, '16236838783904', 'M28', 2, 150000, 'સેલફ', 2, 'OPPO', 'CPH2001', 'bank', 'dr', '00:00:00', 1, '2021-06-14 11:17:58'),
(36, '16236838783904', 'M29', 2, 150000, 'સેલફ', 2, 'OPPO', 'CPH2001', 'cash', 'cr', '00:00:00', 1, '2021-06-14 11:17:58'),
(37, '16240805447303', 'M30', 14, 125000, 'Direct payment for member id: PSB7', 2, 'OPPO', 'CPH2001', 'cash', 'cr', '00:00:00', 1, '2021-06-19 01:29:04'),
(38, '16243693503595', 'M31', 389, 5000, 'Direct payment for member id: PSB369', 2, 'OPPO', 'CPH2001', 'bank', 'cr', '00:00:00', 1, '2021-06-22 09:42:30'),
(39, '16245289014845', 'M32', 14, 25000, 'Direct payment for member id: PSB7', 2, 'OPPO', 'CPH2001', 'cash', 'cr', '00:00:00', 1, '2021-06-24 06:01:41'),
(40, '16245296117453', 'M33', 394, 20000, 'Direct payment for member id: PSB374', 2, 'OPPO', 'CPH2001', 'bank', 'cr', '00:00:00', 1, '2021-06-24 06:13:31'),
(41, '16246842336252', 'M34', 396, 11000, 'Direct payment for member id: PSB376', 2, 'OPPO', 'CPH2001', 'bank', 'cr', '00:00:00', 1, '2021-06-26 01:10:33'),
(42, '16246845864947', 'M35', 397, 2500, 'Direct payment for member id: PSB377', 2, 'OPPO', 'CPH2001', 'bank', 'cr', '00:00:00', 1, '2021-06-26 01:16:26');

-- --------------------------------------------------------

--
-- Table structure for table `notice`
--

CREATE TABLE `notice` (
  `id` int(11) NOT NULL,
  `title` longtext NOT NULL,
  `notes` longtext NOT NULL,
  `image` varchar(1000) NOT NULL,
  `uid` int(11) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `sender_type` enum('temple','dada','all') NOT NULL DEFAULT 'all',
  `status` int(1) NOT NULL DEFAULT '1',
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `notice`
--

INSERT INTO `notice` (`id`, `title`, `notes`, `image`, `uid`, `sender_id`, `sender_type`, `status`, `date`) VALUES
(1, 'ભવન નિર્માણ કાર્ય માટે ઉદાર હાથે દાન આપવા નમ્ર વિનંતી.', 'પાટીદાર શૈક્ષણિક ભવનનું નિર્માણ થઇ રહ્યું છે, એના માટે તમામ પાટીદાર ભાઈઓને નમ્ર વિનંતી ફુલ નહિ તો ફુલની પાંખડી યોગદાન આપીને સમાજ નિર્માણ કાર્ય ના સહ ભાગીદારી બનવા અપીલ.', 'a4499e212b705361c9b1f837fcd9989103.jpg', 2, 2, 'all', -1, '2021-03-06 12:55:01'),
(2, 'વર્ગ-3 ની ભરતી ની તૈયારી કરતા વિધાર્થીઓ માટે.', 'ટૂંક સમયમાં જાહેર થનાર PSI/ASI/Constable તથા વર્ગ-3 ની ભરતી માટે શ્રી ખોડલધામ વિદ્યાર્થી સમિતિ(KDVS) દ્વારા વર્ગો શરૂ થઈ ચૂક્યા છે તથા પોલીસ ભરતી માટે દોડ પણ શરૂ કરાવવામાં આવેલ છે.\r\nસંપર્ક નંબર :- 7405469239', 'e35fb945fca58c3727d577272eb8ffc1IMG-20210308-WA0118.jpg', 2, 2, 'all', 1, '2021-03-08 00:33:33'),
(3, 'પોલીસ સબ ઇન્સ્પેક્ટર ની ભરતી', 'સરદાર ઘામ, વૈશ્રનોદેવી સર્કલ પાસે, અમદાવાદ', '0d8b7290a469c5e84c55cc592f8fb25bIMG-20210317-WA0155.jpg', 2, 2, 'all', 1, '2021-03-17 06:50:31'),
(4, 'પાટીદાર સમાજ ભંડારીયા માટેની  \r\n   અગત્યની મીટીંગ', 'પાટીદાર સમાજ ભંડારીયા\r\n             માટેની  \r\n   અગત્યની મીટીંગ\r\nલેઉવા પટેલ નૂતન કેળવણી મંડળ- જસદણ સંચાલિત પાટીદાર શૈક્ષણિક ભવન નાં નિર્માણ અથૅ તેમજ શિક્ષણ માટે મળતી વિવિધ સહાય તેમજ સરકારી યોજનાઓ ની માહિતી  સમાજ ના દરેક વ્યક્તિઓને મળતી રહે એ માટે  પધારશો, એવી અપેક્ષા.\r\n મીટીંગ સ્થળ.\r\nસગપરીયા પરીવાર નું મંદિર, ભંડારીયા.\r\n તારીખ, 23/3/21\r\nસમય, સાંજ ના 8.00 \r\nમો.9898348148( નિખીલ ભાઈ દેસાઈ ભંડારીયા )\r\n9104424460 ( કિસોર ભાઈ સગપરીયા )\r\n9978442227 ,( નવનિત ભાઈ સરઘારા )\r\nનિમંત્રણ, પાટીદાર શૌક્ષણિક ભવન,\r\nમો. 9725138799', '', 2, 2, 'all', -1, '2021-03-22 12:00:24'),
(5, 'પાટીદાર સમાજ ના હેન્ડીક્રાફ્ટ ઉદ્યોગ કારો  માટેની  \r\n   અગત્યની મીટીંગ\r\n', 'પાટીદાર સમાજ ના હેન્ડીક્રાફ્ટ ઉદ્યોગ કારો   માટેની  \r\n   અગત્યની મીટીંગ\r\nલેઉવા પટેલ નૂતન કેળવણી મંડળ- જસદણ સંચાલિત પાટીદાર શૈક્ષણિક ભવન નાં નિર્માણ અથૅ તેમજ ઉદ્યોગ ને મળતી વિવિધ સહાય તેમજ સરકારી યોજનાઓ ની માહિતી  સમાજ ના દરેક વ્યક્તિઓને મળતી રહે એ માટે  પધારશો, એવી અપેક્ષા.\r\n મીટીંગ સ્થળ,\r\n પાટીદાર શૌક્ષણિક ભવન- જસદણ\r\nપાણી નાં ટાંકા પાસે આટકોટ રોડ, જસદણ.\r\n તારીખ, 23/3/21\r\nસમય, સાંજ ના 5.30 \r\nમીટીંગ પુણ થયે અલ્પાહાર સાથે લેસુ.\r\nમો.9725138799\r\nનિમંત્રણ, પાટીદાર શૌક્ષણિક ભવન,', '', 2, 2, 'all', -1, '2021-03-22 11:53:53'),
(6, 'રક્તદાન એજ મહાદાન', 'રક્તદાન એજ મહાદાન\r\n\r\nસ્વ. વિપુલભાઈ વેલજીભાઇ હીરપરા (લેમ્પ) તથા સ્વ.અંકુરભાઈ લક્ષમણ ભાઈ વઘાસિયા નુ જય સરદાર ગ્રુપ જસદણ તથા દિવ્યધામ વલારડી પાતા દાદા ગ્રુપ જસદણ વતી એક  રકતદાન કેમ્પ નું આયોજન કરેલ છે.\r\nતો રક્તદાન કરવા માંગતા  સર્વ સમાજ,ગ્રુપ વડીલો મિત્રો ને રક્તદાન કરવા ભાવ ભર્યું આમંત્રણ છે.\r\n\r\nતા. ૨૫/૦૩/૨૦૨૧\r\nવાર. ગુરુવાર\r\nસમય : ૮:૦૦ થી ૩:૦૦\r\nસ્થળ. હીરપરા વાડી\r\n\r\nગ્રુપ ના તમામ સભ્યો ને ફરજીયાત spg ગ્રુપ નું ટિશર્ટ પહેરીને ફરજીયાત હાજરી આપવા વિનંતી.', '', 2, 2, 'all', -1, '2021-03-25 03:39:52'),
(7, 'પાટીદાર સમાજ ના ઈલેક્ટ્રીક નાં વ્યવસાય તેમજ મોટર રીવાઇન્ડ ના વ્યવસાય સાથે જોડાયેલા સમાજના વ્યવસાય કારો*  માટેની  \r\n   *અગત્યની મીટીંગ*', '*પાટીદાર સમાજ ના ઈલેક્ટ્રીક નાં વ્યવસાય તેમજ મોટર રીવાઇન્ડ ના વ્યવસાય સાથે જોડાયેલા સમાજના વ્યવસાય કારો*  માટેની  \r\n   *અગત્યની મીટીંગ*\r\nલેઉવા પટેલ નૂતન કેળવણી મંડળ- જસદણ સંચાલિત પાટીદાર શૈક્ષણિક ભવન નાં નિર્માણ અથૅ તેમજ ઉદ્યોગ ને મળતી વિવિધ સહાય તેમજ સરકારી યોજનાઓ ની માહિતી  સમાજ ના દરેક વ્યક્તિઓને મળતી રહે, તેમજ જસદણ ખાતે તૈયાર થઈ રહેલ પાટીદાર શૈક્ષણિક ભવન ની માહિતી આપણને મળતી રહે તે માટે આ મિટિંગમાં  પધારશો, એવી અમારી અપેક્ષા.\r\n *મીટીંગ સ્થળ*, પાટીદાર શૌક્ષણિક ભવન- જસદણ\r\nપાણી નાં ટાંકા પાસે આટકોટ રોડ, જસદણ.\r\n *તારીખ,* 27/3/21 શનિવારે\r\n*સમય,* સાંજ ના 5.30 \r\n*મીટીંગ પુણ થયે અલ્પાહાર સાથે લેસુ.*\r\nમો.9725138799\r\n*નિમંત્રણ*, પાટીદાર શૌક્ષણિક ભવન,', '', 2, 2, 'all', -1, '2021-03-27 00:37:44'),
(8, '*પાટીદાર સમાજ ના એન્જિનિયરિંગ ઉદ્યોગ,ફેબ્રિકેશન તેમજ એલ્યુમિનિયમ સેકસન ના વ્યવસાય સાથે જોડાયેલા સમાજના વ્યવસાય કારો*  માટેની  \r\n   *અગત્યની મીટીંગ*', '*પાટીદાર સમાજ ના એન્જિનિયરિંગ ઉદ્યોગ,ફેબ્રિકેશન તેમજ એલ્યુમિનિયમ સેકસન ના વ્યવસાય સાથે જોડાયેલા સમાજના વ્યવસાય કારો*  માટેની  \r\n   *અગત્યની મીટીંગ*\r\nલેઉવા પટેલ નૂતન કેળવણી મંડળ- જસદણ સંચાલિત પાટીદાર શૈક્ષણિક ભવન નાં નિર્માણ અથૅ તેમજ ઉદ્યોગ ને મળતી વિવિધ સહાય તેમજ સરકારી યોજનાઓ ની માહિતી  સમાજ ના દરેક વ્યક્તિઓને મળતી રહે, તેમજ જસદણ ખાતે તૈયાર થઈ રહેલ પાટીદાર શૈક્ષણિક ભવન ની માહિતી આપણને મળતી રહે તે માટે આ મિટિંગમાં  પધારશો, એવી અમારી અપેક્ષા.\r\n *મીટીંગ સ્થળ*, પાટીદાર શૌક્ષણિક ભવન- જસદણ\r\nપાણી નાં ટાંકા પાસે આટકોટ રોડ, જસદણ.\r\n *તારીખ,* 27/3/21 શનિવારે\r\n*સમય,* સાંજ ના 5.30 \r\n*મીટીંગ પુણ થયે અલ્પાહાર સાથે લેસુ.*\r\nમો.9725138799\r\n*નિમંત્રણ*, પાટીદાર શૌક્ષણિક ભવન,', '', 2, 2, 'all', -1, '2021-03-27 00:38:29'),
(9, 'Covid 19  વેક્સિન માટે રજીસ્ટ્રેશન', 'પાટીદાર શૈક્ષણિક ભવન જસદણ ખાતે\r\nતારીખ 5/ 4 /2021 ના રોજ પાટીદાર શૈક્ષણિક ભવન જસદણ દ્વારા જેમણે  પ્રથમ ડોઝ (કોરોના વેકસીન) લીધેલો હોય તેવા તમામ વ્યક્તિઓ એ બીજો ડોઝ લેવા માટે આવતીકાલે તારીખ 13 /5/ 2021ના રોજ સવારે 9 થી સાંજના 5 દરમિયાન આધાર કાર્ડ ની ઝેરોક્ષ તેમજ વેક્સિન લીધાની પહોંચ સાથે નોંધણી કરાવી દેવા વિનંતી,\r\n જેમની નોંધણી થઇ હશે એમને ફોન કરી અને વેક્સિન માટે બોલાવવામાં આવશે,\r\nલેઉવા પટેલ નૂતન કેળવણી મંડળ સંચાલિત\r\n પાટીદાર શિક્ષણ ભવન જસદણ આટકોટ રોડ પાણીના ટાકા પાસે જસદણ,\r\nફોન, ‌9725138799', '', 2, 2, 'all', -1, '2021-05-13 06:54:22'),
(10, 'આગામી તા.૨૫/૦૬/૨૦૨૧ થી ધોરણ ૧ માં ખાનગી શાળામાં મફત પ્રવેશ મેળવવા માટે સરકારશ્રીની RTE યોજના', '\r\n▪️જે બાળકની ઉમર તા.૩૧/૦૫/૨૦૨૧ સુધીમાં ૫ વર્ષ પૂરા થતા હોય તેઓ અરજી કરી શકશે. \r\nજેમાં નીચે મુજબના ડોક્યુમેન્ટ તૈયાર કરી રાખવાના રહેશે.\r\n▪️બાળકનો જન્મનો ઓરીજનલ દાખલો\r\n▪️પિતાનો આવકનો ફોટાવાળો ટી.ડી.ઓ. નો દાખલો \r\n▪️પિતાનો જાતિનો દાખલો\r\n▪️બાળકનું આધારકાર્ડ. \r\n▪️માતા/પિતાનું આધારકાર્ડ  \r\n▪️માતા/પિતાની બેંક ખાતાની બુક \r\n▪️બાળકના બે પાસપોર્ટ ફોટા \r\nવગેરે ડોક્યુમેન્ટ વહેલી તકે તૈયાર કરી લેવા. \r\n▪️જે બાળકને આ યોજનામાં પ્રવેશ મળશે તેને ધોરણ ૧ થી ૮ સુધી ખાનગી શાળામાં ફી ભરવી પડશે નહિ. વધુ માહિતી માટે સંપર્ક કરો.', '', 2, 2, 'all', 1, '2021-06-28 11:59:03');

-- --------------------------------------------------------

--
-- Table structure for table `qualification_master`
--

CREATE TABLE `qualification_master` (
  `id` int(11) NOT NULL,
  `name` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `qualification_master`
--

INSERT INTO `qualification_master` (`id`, `name`, `status`, `date`) VALUES
(5, 'diploma engineering', 1, '2021-01-08 23:08:33'),
(6, 'Mca / Mcom /Msc /MA', 1, '2021-01-08 00:14:55'),
(7, 'B.Com / BCA / BA/ Bsc ', 1, '2021-01-08 00:12:35'),
(8, 'std 10 to 12', 1, '2021-01-08 00:10:43'),
(9, 'std 1 to 9', 1, '2021-01-08 00:10:20'),
(10, 'અન્ય', 1, '2021-01-08 01:19:59'),
(11, 'Medical sciences', 1, '2021-01-08 23:29:28'),
(12, 'Business', -1, '2021-01-08 23:04:41'),
(13, 'Degree engineering', 1, '2021-01-08 23:08:55'),
(14, 'B.ed/m.ed/LLB', 1, '2021-01-08 23:11:16'),
(15, 'Government job', -1, '2021-01-08 23:10:42'),
(16, 'M.phill/PHD', 1, '2021-01-08 23:28:31'),
(17, 'I T I', 1, '2021-01-08 23:29:57'),
(18, 'Under graduation', 1, '2021-01-08 23:30:19'),
(19, 'Post graduation', 1, '2021-01-08 23:30:34'),
(20, 'GPSC/UPSC', 1, '2021-01-08 23:31:23'),
(21, 'વિદેશી માં અભ્યાસ ચાલુ છે.', 1, '2021-01-18 08:32:37'),
(22, 'અભ્યાસ ચાલુ.', 1, '2021-01-18 08:32:07');

-- --------------------------------------------------------

--
-- Table structure for table `role_master`
--

CREATE TABLE `role_master` (
  `id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `assign_id` int(11) NOT NULL,
  `type` enum('managment','dist','taluka','village') NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `role_master`
--

INSERT INTO `role_master` (`id`, `city_id`, `uid`, `assign_id`, `type`, `status`, `date`) VALUES
(1, 28, 2, 1, 'dist', -1, '2021-03-18 00:00:00'),
(2, 28, 2, 1, 'dist', -1, '2021-03-19 00:00:00'),
(4, 2, 1, 1, 'taluka', -1, '2021-03-19 00:00:00'),
(5, 1, 2, 1, 'village', -1, '2021-03-19 00:00:00'),
(6, 2, 8, 1, 'dist', -1, '2021-03-20 05:54:45'),
(7, 2, 9, 1, 'taluka', -1, '2021-03-20 05:55:50'),
(8, 3, 10, 1, 'village', 1, '2021-03-20 05:57:11'),
(9, 21, 11, 1, 'dist', -1, '2021-03-24 09:54:19'),
(10, 14, 16, 1, 'dist', -1, '2021-03-24 09:55:09'),
(11, 0, 2, 0, 'managment', -1, '2021-03-27 06:29:54'),
(12, 0, 14, 0, 'managment', -1, '2021-03-28 00:35:47'),
(13, 0, 46, 0, 'managment', -1, '2021-03-28 00:37:23'),
(14, 0, 120, 0, 'managment', -1, '2021-03-28 00:57:10'),
(15, 0, 11, 0, 'managment', -1, '2021-03-28 01:02:04'),
(16, 0, 263, 0, 'managment', -1, '2021-03-28 01:07:59'),
(17, 2, 8, 2, 'dist', 1, '2021-03-30 02:41:45'),
(18, 2, 264, 1, 'dist', 1, '2021-06-10 06:44:35'),
(19, 2, 46, 1, 'dist', 1, '2021-06-10 06:45:11');

-- --------------------------------------------------------

--
-- Table structure for table `session`
--

CREATE TABLE `session` (
  `id` int(11) NOT NULL,
  `session_id` varchar(50) DEFAULT NULL,
  `user_name` varchar(100) DEFAULT NULL,
  `user_pwd` varchar(100) DEFAULT '',
  `uid` int(11) NOT NULL,
  `ip` varchar(20) NOT NULL,
  `user_agent` varchar(500) NOT NULL,
  `status` enum('active','inactive') NOT NULL,
  `logout_at` datetime DEFAULT NULL,
  `last_seen` datetime DEFAULT NULL,
  `session_type` enum('front','back') NOT NULL,
  `stamp` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `session`
--

INSERT INTO `session` (`id`, `session_id`, `user_name`, `user_pwd`, `uid`, `ip`, `user_agent`, `status`, `logout_at`, `last_seen`, `session_type`, `stamp`) VALUES
(1, '3g6kj43cjvpmh9lk391f9lv97g', '8128500518', '12345', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.146 Safari/537.36', 'inactive', '2021-02-08 17:35:03', '2021-02-08 17:30:12', 'back', '2021-02-08 17:30:12'),
(2, '3g6kj43cjvpmh9lk391f9lv97g', '8128500518', '12345', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.146 Safari/537.36', 'active', NULL, '2021-02-08 17:35:05', 'back', '2021-02-08 17:35:05'),
(3, 'bggmftljq42miumhlq8me0ke90', '8128500518', '12345', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Safari/537.36', 'active', NULL, '2021-02-12 10:04:33', 'back', '2021-02-12 10:04:33'),
(4, '51fd4433ff8065cf2fc4570cd13af811', '8128500518', '12345', 1, '49.34.209.157', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Safari/537.36 Edg/88.0.705.63', 'active', NULL, '2021-02-13 04:57:11', 'back', '2021-02-13 04:57:11'),
(5, '5f4b142b2c59b5542c5b1f3192d62822', '9974010036', '12345', 2, '49.34.209.157', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Safari/537.36', 'active', NULL, '2021-02-13 05:21:40', 'back', '2021-02-13 05:21:40'),
(6, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '157.32.105.72', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.5.1', 'inactive', '2021-03-26 06:43:28', '2021-02-13 05:23:33', 'back', '2021-02-13 05:23:33'),
(7, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '157.32.105.72', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.5.1', 'inactive', '2021-03-26 06:43:28', '2021-02-13 05:58:33', 'back', '2021-02-13 05:58:33'),
(8, '655ce7fc06be7774fc4e35d3321fad63', '9974010036', '12345', 2, '103.83.107.214', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Safari/537.36', 'inactive', '2021-02-13 12:24:49', '2021-02-13 10:58:35', 'back', '2021-02-13 10:58:35'),
(9, '655ce7fc06be7774fc4e35d3321fad63', '9974010036', '12345', 2, '103.83.107.214', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Safari/537.36', 'inactive', '2021-02-13 12:24:49', '2021-02-13 12:24:39', 'back', '2021-02-13 12:24:39'),
(10, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '106.222.38.201', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.5.1', 'inactive', '2021-03-26 06:43:28', '2021-02-27 15:37:19', 'back', '2021-02-27 15:37:19'),
(11, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '106.222.38.221', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.5.1', 'inactive', '2021-03-26 06:43:28', '2021-02-28 07:52:48', 'back', '2021-02-28 07:52:48'),
(12, 'a955c6d23c778d1547ef9b7fa720fea2', '9974010036', '12345', 2, '223.184.215.211', 'Mozilla/5.0 (Linux; Android 6.0.1; SM-A700FD) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.93 Mobile Safari/537.36', 'inactive', '2021-03-06 07:27:10', '2021-03-06 07:21:43', 'back', '2021-03-06 07:21:43'),
(13, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '27.61.230.224', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.5.1', 'inactive', '2021-03-26 06:43:28', '2021-03-06 07:49:52', 'back', '2021-03-06 07:49:52'),
(14, '6c827678227f54009d1813b27ada8b5a', '8128500518', '12345', 1, '157.32.100.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.72 Safari/537.36 Edg/89.0.774.45', 'active', NULL, '2021-03-06 08:51:26', 'back', '2021-03-06 08:51:26'),
(15, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '103.83.107.214', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.5.1', 'inactive', '2021-03-26 06:43:28', '2021-03-06 11:02:02', 'back', '2021-03-06 11:02:02'),
(16, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '27.61.230.178', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.5.1', 'inactive', '2021-03-26 06:43:28', '2021-03-06 11:57:29', 'back', '2021-03-06 11:57:29'),
(17, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '27.61.230.178', 'Mozilla/5.0 (Linux; Android 10; CPH2001 Build/QP1A.190711.020; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36', 'inactive', '2021-03-26 06:43:28', '2021-03-06 11:57:30', 'back', '2021-03-06 11:57:30'),
(18, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '103.83.107.214', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.5.1', 'inactive', '2021-03-26 06:43:28', '2021-03-06 12:47:07', 'back', '2021-03-06 12:47:07'),
(19, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '103.83.107.214', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.5.1', 'inactive', '2021-03-26 06:43:28', '2021-03-06 13:05:17', 'back', '2021-03-06 13:05:17'),
(20, '27e1bd2b1117859f6ce6dff160f0b82f', '8128500518', '12345', 1, '49.34.184.228', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.72 Safari/537.36 Edg/89.0.774.45', 'active', NULL, '2021-03-06 21:50:22', 'back', '2021-03-06 21:50:22'),
(21, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '106.222.38.221', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.5.1', 'inactive', '2021-03-26 06:43:28', '2021-03-07 03:11:06', 'back', '2021-03-07 03:11:06'),
(22, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '106.222.38.221', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.5.1', 'inactive', '2021-03-26 06:43:28', '2021-03-07 06:02:23', 'back', '2021-03-07 06:02:23'),
(23, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '103.83.106.17', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.5.1', 'inactive', '2021-03-26 06:43:28', '2021-03-08 00:30:38', 'back', '2021-03-08 00:30:38'),
(24, '5b4eb5757bf162e7d0bc58b9400a6018', '8128500518', '12345', 1, '49.34.123.167', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.82 Safari/537.36 Edg/89.0.774.48', 'active', NULL, '2021-03-09 08:57:54', 'back', '2021-03-09 08:57:54'),
(25, '5b4eb5757bf162e7d0bc58b9400a6018', '8128500518', '12345', 1, '49.34.123.167', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.82 Safari/537.36 Edg/89.0.774.48', 'active', NULL, '2021-03-09 08:57:55', 'back', '2021-03-09 08:57:55'),
(26, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '103.83.107.214', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.5.1', 'inactive', '2021-03-26 06:43:28', '2021-03-09 11:52:35', 'back', '2021-03-09 11:52:35'),
(27, '3fe3ca4594cf1b66189d1e63daf7ab78', '9974010036', '12345', 2, '106.213.220.24', 'Mozilla/5.0 (Linux; Android 6.0.1; SM-A700FD) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.93 Mobile Safari/537.36', 'inactive', '2021-03-09 12:13:51', '2021-03-09 12:13:06', 'back', '2021-03-09 12:13:06'),
(28, '3fe3ca4594cf1b66189d1e63daf7ab78', '9974010036', '12345', 2, '106.213.220.24', 'Mozilla/5.0 (Linux; Android 6.0.1; SM-A700FD) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.93 Mobile Safari/537.36', 'active', NULL, '2021-03-09 12:18:07', 'back', '2021-03-09 12:18:07'),
(29, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '103.83.107.214', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.5.1', 'inactive', '2021-03-26 06:43:28', '2021-03-10 02:33:44', 'back', '2021-03-10 02:33:44'),
(30, '297d741b79872ae2564237b482c66834', '8128500518', '12345', 1, '157.32.97.141', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36', 'active', NULL, '2021-03-10 04:57:43', 'back', '2021-03-10 04:57:43'),
(31, '297d741b79872ae2564237b482c66834', '8128500518', '12345', 1, '157.32.97.141', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36', 'active', NULL, '2021-03-10 06:00:04', 'back', '2021-03-10 06:00:04'),
(32, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '27.61.139.236', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.5.1', 'inactive', '2021-03-26 06:43:28', '2021-03-11 09:01:29', 'back', '2021-03-11 09:01:29'),
(33, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '27.61.139.236', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.5.1', 'inactive', '2021-03-26 06:43:28', '2021-03-11 09:58:14', 'back', '2021-03-11 09:58:14'),
(34, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '27.61.139.236', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.5.1', 'inactive', '2021-03-26 06:43:28', '2021-03-11 11:37:03', 'back', '2021-03-11 11:37:03'),
(35, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '27.61.139.236', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.5.1', 'inactive', '2021-03-26 06:43:28', '2021-03-11 13:04:11', 'back', '2021-03-11 13:04:11'),
(36, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '27.61.139.236', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.5.1', 'inactive', '2021-03-26 06:43:28', '2021-03-11 22:36:07', 'back', '2021-03-11 22:36:07'),
(37, '5e3b4adacbb47754c5de3461eb9e53e9', '8128500518', '12345', 1, '49.34.82.181', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.82 Safari/537.36 Edg/89.0.774.50', 'active', NULL, '2021-03-11 23:18:11', 'back', '2021-03-11 23:18:11'),
(38, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '223.184.250.73', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.5.1', 'inactive', '2021-03-26 06:43:28', '2021-03-12 01:47:47', 'back', '2021-03-12 01:47:47'),
(39, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '223.184.251.193', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.5.1', 'inactive', '2021-03-26 06:43:28', '2021-03-12 05:59:47', 'back', '2021-03-12 05:59:47'),
(40, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '223.184.251.25', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.5.1', 'inactive', '2021-03-26 06:43:28', '2021-03-12 07:26:59', 'back', '2021-03-12 07:26:59'),
(41, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '223.184.251.25', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.5.1', 'inactive', '2021-03-26 06:43:28', '2021-03-12 07:32:17', 'back', '2021-03-12 07:32:17'),
(42, 'b25653c5d7061287eca721e31360ee36', '8128500518', '12345', 1, '157.32.88.172', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.82 Safari/537.36', 'active', NULL, '2021-03-12 08:14:01', 'back', '2021-03-12 08:14:01'),
(43, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '103.83.107.214', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1', 'inactive', '2021-03-26 06:43:28', '2021-03-12 09:21:28', 'back', '2021-03-12 09:21:28'),
(44, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '223.184.250.97', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1', 'inactive', '2021-03-26 06:43:28', '2021-03-12 10:51:03', 'back', '2021-03-12 10:51:03'),
(45, '037b2f49a8414fb6d7771f4f94ac5e87', '9974010036', '12345', 2, '103.83.107.214', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.82 Safari/537.36', 'inactive', '2021-03-12 13:04:50', '2021-03-12 11:50:34', 'back', '2021-03-12 11:50:34'),
(46, '7c9d3614f88b9b44746b34534b0f1ec2', '8128500518', '12345', 1, '49.34.79.13', 'Mozilla/5.0 (Linux; Android 10; RMX1801) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.92 Mobile Safari/537.36', 'active', NULL, '2021-03-12 12:03:39', 'back', '2021-03-12 12:03:39'),
(47, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '223.184.252.217', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1', 'inactive', '2021-03-26 06:43:28', '2021-03-12 12:47:09', 'back', '2021-03-12 12:47:09'),
(48, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '223.184.252.217', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1', 'inactive', '2021-03-26 06:43:28', '2021-03-12 23:45:48', 'back', '2021-03-12 23:45:48'),
(49, '7be9b08357d6b5985a7e75a5234c00b4', '9974010036', '12345', 2, '157.32.94.25', 'Mozilla/5.0 (Linux; Android 10; RMX1801) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.181 Mobile Safari/537.36', 'active', NULL, '2021-03-13 00:09:38', 'back', '2021-03-13 00:09:38'),
(50, 'cd54afafbe88aa12154bd7e44fd5be94', '9974010036', '12345', 2, '157.32.94.25', 'Mozilla/5.0 (Linux; Android 10; RMX1801) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.181 Mobile Safari/537.36', 'inactive', '2021-03-13 00:34:48', '2021-03-13 00:33:20', 'back', '2021-03-13 00:33:20'),
(51, '15f27137f8244014aebf7d7fb51ece18', '8128500518', '12345', 1, '49.34.69.97', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.82 Safari/537.36 Edg/89.0.774.50', 'active', NULL, '2021-03-13 05:05:32', 'back', '2021-03-13 05:05:32'),
(52, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '103.83.106.17', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1', 'inactive', '2021-03-26 06:43:28', '2021-03-13 05:40:49', 'back', '2021-03-13 05:40:49'),
(53, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '103.83.106.17', 'Mozilla/5.0 (Linux; Android 10; CPH2001 Build/QP1A.190711.020; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36', 'inactive', '2021-03-26 06:43:28', '2021-03-13 05:40:50', 'back', '2021-03-13 05:40:50'),
(54, '0a0d6bbf1cc0ab29f1b5c4347eb643c5', '8128500518', '12345', 1, '157.32.14.44', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.82 Safari/537.36', 'active', NULL, '2021-03-13 06:55:05', 'back', '2021-03-13 06:55:05'),
(55, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '103.83.106.17', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1', 'inactive', '2021-03-26 06:43:28', '2021-03-13 07:25:15', 'back', '2021-03-13 07:25:15'),
(56, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '106.205.243.99', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1', 'inactive', '2021-03-26 06:43:28', '2021-03-13 08:17:48', 'back', '2021-03-13 08:17:48'),
(57, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '106.205.243.99', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1', 'inactive', '2021-03-26 06:43:28', '2021-03-13 12:33:00', 'back', '2021-03-13 12:33:00'),
(58, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '223.184.143.67', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1', 'inactive', '2021-03-26 06:43:28', '2021-03-13 23:26:46', 'back', '2021-03-13 23:26:46'),
(59, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '223.184.143.67', 'Mozilla/5.0 (Linux; Android 10; CPH2001 Build/QP1A.190711.020; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36', 'inactive', '2021-03-26 06:43:28', '2021-03-13 23:26:47', 'back', '2021-03-13 23:26:47'),
(60, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '223.184.143.67', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1', 'inactive', '2021-03-26 06:43:28', '2021-03-14 01:48:28', 'back', '2021-03-14 01:48:28'),
(61, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '223.238.227.122', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1', 'inactive', '2021-03-26 06:43:28', '2021-03-14 08:41:44', 'back', '2021-03-14 08:41:44'),
(62, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '223.238.227.122', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1', 'inactive', '2021-03-26 06:43:28', '2021-03-14 09:44:25', 'back', '2021-03-14 09:44:25'),
(63, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '223.238.227.122', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1', 'inactive', '2021-03-26 06:43:28', '2021-03-14 13:17:02', 'back', '2021-03-14 13:17:02'),
(64, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '223.238.227.122', 'Mozilla/5.0 (Linux; Android 10; CPH2001 Build/QP1A.190711.020; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36', 'inactive', '2021-03-26 06:43:28', '2021-03-14 13:17:03', 'back', '2021-03-14 13:17:03'),
(65, 'd7bbc9c08f342185fb8f0f4a42d32900', '9974010036', '12345', 2, '49.34.216.150', 'Mozilla/5.0 (Linux; Android 10; RMX1801) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.86 Mobile Safari/537.36', 'active', NULL, '2021-03-15 00:52:56', 'back', '2021-03-15 00:52:56'),
(66, '7c3a22086383c1fb379afc57dfa9c3b8', '9974010036', '12345', 2, '103.83.106.17', 'Mozilla/5.0 (Linux; Android 10; RMX1801) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.86 Mobile Safari/537.36', 'active', NULL, '2021-03-15 01:43:56', 'back', '2021-03-15 01:43:56'),
(67, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '223.238.227.122', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1', 'inactive', '2021-03-26 06:43:28', '2021-03-15 02:21:38', 'back', '2021-03-15 02:21:38'),
(68, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '103.83.107.214', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1', 'inactive', '2021-03-26 06:43:28', '2021-03-15 04:46:35', 'back', '2021-03-15 04:46:35'),
(69, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '106.213.130.113', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1', 'inactive', '2021-03-26 06:43:28', '2021-03-15 05:24:53', 'back', '2021-03-15 05:24:53'),
(70, '8f77992b33b2a35d0474b463b382295b', '9974010036', '12345', 2, '27.61.220.137', 'Mozilla/5.0 (Linux; Android 6.0.1; SM-A700FD) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.93 Mobile Safari/537.36', 'active', NULL, '2021-03-15 08:26:24', 'back', '2021-03-15 08:26:24'),
(71, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '106.213.130.113', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1', 'inactive', '2021-03-26 06:43:28', '2021-03-15 08:33:25', 'back', '2021-03-15 08:33:25'),
(72, '98b2cebe3550c351bb7c5eedc43a3d43', '8128500518', '12345', 1, '49.34.85.47', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.82 Safari/537.36', 'active', NULL, '2021-03-15 09:35:52', 'back', '2021-03-15 09:35:52'),
(73, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '106.213.130.113', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1', 'inactive', '2021-03-26 06:43:28', '2021-03-15 10:27:37', 'back', '2021-03-15 10:27:37'),
(74, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '106.222.38.247', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1', 'inactive', '2021-03-26 06:43:28', '2021-03-16 02:28:32', 'back', '2021-03-16 02:28:32'),
(75, '26e7117d99cdf67491de888aa02371de', '8128500518', '12345', 1, '49.34.5.228', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.82 Safari/537.36', 'active', NULL, '2021-03-16 06:00:48', 'back', '2021-03-16 06:00:48'),
(76, '8f77992b33b2a35d0474b463b382295b', '9974010036', '12345', 2, '27.61.220.137', 'Mozilla/5.0 (Linux; Android 6.0.1; SM-A700FD) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.93 Mobile Safari/537.36', 'active', NULL, '2021-03-16 06:08:41', 'back', '2021-03-16 06:08:41'),
(77, '26e7117d99cdf67491de888aa02371de', '8128500518', '12345', 1, '49.34.5.228', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.82 Safari/537.36', 'active', NULL, '2021-03-16 07:20:32', 'back', '2021-03-16 07:20:32'),
(78, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '106.222.38.247', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1', 'inactive', '2021-03-26 06:43:28', '2021-03-16 08:10:07', 'back', '2021-03-16 08:10:07'),
(79, '26e7117d99cdf67491de888aa02371de', '8128500518', '12345', 1, '49.34.5.228', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.82 Safari/537.36', 'active', NULL, '2021-03-16 08:20:48', 'back', '2021-03-16 08:20:48'),
(80, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '106.222.70.240', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1', 'inactive', '2021-03-26 06:43:28', '2021-03-16 12:26:00', 'back', '2021-03-16 12:26:00'),
(81, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '106.205.227.97', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1', 'inactive', '2021-03-26 06:43:28', '2021-03-17 06:46:28', 'back', '2021-03-17 06:46:28'),
(82, '2e867c68a1da8563c2c1a93b1b64a50a', '8128500518', '12345', 1, '49.34.31.31', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.82 Safari/537.36', 'active', NULL, '2021-03-17 08:45:56', 'back', '2021-03-17 08:45:56'),
(83, '2e867c68a1da8563c2c1a93b1b64a50a', '8128500518', '12345', 1, '49.34.31.31', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.82 Safari/537.36', 'active', NULL, '2021-03-17 10:16:19', 'back', '2021-03-17 10:16:19'),
(84, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '106.205.243.100', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1', 'inactive', '2021-03-26 06:43:28', '2021-03-17 11:41:28', 'back', '2021-03-17 11:41:28'),
(85, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '103.83.106.17', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1', 'inactive', '2021-03-26 06:43:28', '2021-03-18 03:51:18', 'back', '2021-03-18 03:51:18'),
(86, 'c77328b01f2173c2ce36b0bb8872f199', '9974010036', '12345', 2, '49.34.79.0', 'Mozilla/5.0 (Linux; Android 10; RMX1801) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.86 Mobile Safari/537.36', 'active', NULL, '2021-03-18 04:43:41', 'back', '2021-03-18 04:43:41'),
(87, '3c6e6eb0a74ad34fcab22abe6ce3ee75', '9974010036', '12345', 2, '49.34.79.0', 'Mozilla/5.0 (Linux; Android 10; RMX1801) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.86 Mobile Safari/537.36', 'active', NULL, '2021-03-18 04:48:02', 'back', '2021-03-18 04:48:02'),
(88, '183793f194231c02819de4e119a2fb94', '9974010036', '12345', 2, '49.34.79.0', 'Mozilla/5.0 (Linux; Android 10; RMX1801) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.86 Mobile Safari/537.36', 'active', NULL, '2021-03-18 04:52:26', 'back', '2021-03-18 04:52:26'),
(89, '719b080302ed2e5b8a852b2cf2759cd2', '9974010036', '12345', 2, '49.34.79.0', 'Mozilla/5.0 (Linux; Android 10; RMX1801) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.86 Mobile Safari/537.36', 'active', NULL, '2021-03-18 04:56:57', 'back', '2021-03-18 04:56:57'),
(90, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '106.205.243.100', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1', 'inactive', '2021-03-26 06:43:28', '2021-03-18 05:57:50', 'back', '2021-03-18 05:57:50'),
(91, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '106.205.243.100', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1', 'inactive', '2021-03-26 06:43:28', '2021-03-18 07:03:21', 'back', '2021-03-18 07:03:21'),
(92, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '106.205.243.100', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1', 'inactive', '2021-03-26 06:43:28', '2021-03-18 09:45:33', 'back', '2021-03-18 09:45:33'),
(93, 'fd3de9d111db33852c45191370cfdf43', '9974010036', '12345', 2, '49.34.68.37', 'Mozilla/5.0 (Linux; Android 10; RMX1801) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.86 Mobile Safari/537.36', 'active', NULL, '2021-03-19 08:43:07', 'back', '2021-03-19 08:43:07'),
(94, 'a33f2510d9a771be50dc526e8ed3bcf8', '9974010036', '12345', 2, '49.34.68.37', 'Mozilla/5.0 (Linux; Android 10; RMX1801) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.86 Mobile Safari/537.36', 'active', NULL, '2021-03-19 08:50:38', 'back', '2021-03-19 08:50:38'),
(95, '0b8b7f16bce714d2f441a2b41f1e5479', '8128500518', '12345', 1, '157.32.103.19', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36', 'active', NULL, '2021-03-19 10:20:04', 'back', '2021-03-19 10:20:04'),
(96, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '106.222.6.238', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1', 'inactive', '2021-03-26 06:43:28', '2021-03-19 10:25:57', 'back', '2021-03-19 10:25:57'),
(97, '6b13460f4df43120114540831cbaa9d4', '8128500518', '12345', 1, '49.34.105.237', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36', 'active', NULL, '2021-03-20 00:46:29', 'back', '2021-03-20 00:46:29'),
(98, '58ab2fe45643e74ec76f5959ff5b7651', '8128500518', '12345', 1, '49.34.105.237', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36 Edg/89.0.774.57', 'active', NULL, '2021-03-20 03:44:00', 'back', '2021-03-20 03:44:00'),
(99, '58ab2fe45643e74ec76f5959ff5b7651', '8128500518', '12345', 1, '49.34.111.235', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36 Edg/89.0.774.57', 'active', NULL, '2021-03-20 05:41:04', 'back', '2021-03-20 05:41:04'),
(100, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '103.83.106.17', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1', 'inactive', '2021-03-26 06:43:28', '2021-03-20 05:42:52', 'back', '2021-03-20 05:42:52'),
(101, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '223.184.234.235', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1', 'inactive', '2021-03-26 06:43:28', '2021-03-21 12:51:00', 'back', '2021-03-21 12:51:00'),
(102, '8d4b47a542219fc57e124a5a12118869', '8128500518', '12345', 1, '49.34.102.136', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36', 'active', NULL, '2021-03-22 06:07:46', 'back', '2021-03-22 06:07:46'),
(103, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '27.61.130.145', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1', 'inactive', '2021-03-26 06:43:28', '2021-03-22 11:48:07', 'back', '2021-03-22 11:48:07'),
(104, '85acfc9a421292e22a7e04b23cecd486', '9974010036', '12345', 2, '27.61.253.9', 'Mozilla/5.0 (Linux; Android 6.0.1; SM-A700FD) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.93 Mobile Safari/537.36', 'active', NULL, '2021-03-23 09:33:58', 'back', '2021-03-23 09:33:58'),
(105, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '27.61.130.145', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1', 'inactive', '2021-03-26 06:43:28', '2021-03-23 12:32:52', 'back', '2021-03-23 12:32:52'),
(106, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '27.61.130.145', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1', 'inactive', '2021-03-26 06:43:28', '2021-03-23 13:52:12', 'back', '2021-03-23 13:52:12'),
(107, '8c5fb5b0ba84dbd1854dc5322877585e', '8128500518', '12345', 1, '157.32.92.32', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36', 'active', NULL, '2021-03-24 09:52:48', 'back', '2021-03-24 09:52:48'),
(108, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '27.61.130.145', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1', 'inactive', '2021-03-26 06:43:28', '2021-03-25 02:52:21', 'back', '2021-03-25 02:52:21'),
(109, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '103.83.107.214', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1', 'inactive', '2021-03-26 06:43:28', '2021-03-25 04:57:59', 'back', '2021-03-25 04:57:59'),
(110, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '223.184.193.197', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1', 'inactive', '2021-03-26 06:43:28', '2021-03-25 07:19:37', 'back', '2021-03-25 07:19:37'),
(111, '85acfc9a421292e22a7e04b23cecd486', '9974010036', '12345', 2, '106.205.232.200', 'Mozilla/5.0 (Linux; Android 6.0.1; SM-A700FD) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.93 Mobile Safari/537.36', 'active', NULL, '2021-03-25 09:09:44', 'back', '2021-03-25 09:09:44'),
(112, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '103.83.107.214', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1', 'inactive', '2021-03-26 06:43:28', '2021-03-25 10:35:04', 'back', '2021-03-25 10:35:04'),
(113, '85acfc9a421292e22a7e04b23cecd486', '9974010036', '12345', 2, '106.205.232.200', 'Mozilla/5.0 (Linux; Android 6.0.1; SM-A700FD) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.93 Mobile Safari/537.36', 'active', NULL, '2021-03-25 10:39:08', 'back', '2021-03-25 10:39:08'),
(114, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '103.83.106.17', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1', 'inactive', '2021-03-26 06:43:28', '2021-03-26 01:41:03', 'back', '2021-03-26 01:41:03'),
(115, '1408841c08becd438ab74b33c5445ddb', '9974010036', '12345', 2, '103.83.106.17', 'Mozilla/5.0 (Linux; Android 10; RMX1801) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.86 Mobile Safari/537.36', 'active', NULL, '2021-03-26 01:48:06', 'back', '2021-03-26 01:48:06'),
(116, '7642df9fd6776a3cee9f227709807c42', '8128500518', '12345', 1, '157.32.79.4', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36', 'active', NULL, '2021-03-26 05:49:01', 'back', '2021-03-26 05:49:01'),
(117, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '106.222.70.242', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1', 'inactive', '2021-03-26 06:43:28', '2021-03-26 06:41:45', 'back', '2021-03-26 06:41:45'),
(118, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '106.213.233.188', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1', 'active', NULL, '2021-03-27 00:32:52', 'back', '2021-03-27 00:32:52'),
(119, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '106.213.233.188', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1', 'active', NULL, '2021-03-27 04:30:18', 'back', '2021-03-27 04:30:18'),
(120, 'e3a53649a2ce09cfe6ac0849d7bbeee0', '8128500518', '12345', 1, '49.34.1.240', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36', 'active', NULL, '2021-03-27 06:29:29', 'back', '2021-03-27 06:29:29'),
(121, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '106.213.233.188', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1', 'active', NULL, '2021-03-27 08:24:30', 'back', '2021-03-27 08:24:30'),
(122, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '106.213.233.188', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1', 'active', NULL, '2021-03-27 12:24:15', 'back', '2021-03-27 12:24:15'),
(123, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '106.213.233.188', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1', 'active', NULL, '2021-03-28 00:31:12', 'back', '2021-03-28 00:31:12'),
(124, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '106.213.233.188', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1', 'active', NULL, '2021-03-28 01:56:08', 'back', '2021-03-28 01:56:08'),
(125, '350cb8f04b20570f4951429186f4ae12', '8128500518', '12345', 1, '157.32.88.120', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36 Edg/89.0.774.63', 'active', NULL, '2021-03-30 02:29:42', 'back', '2021-03-30 02:29:42'),
(126, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '106.222.38.244', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1', 'active', NULL, '2021-03-30 02:38:59', 'back', '2021-03-30 02:38:59'),
(127, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '106.222.70.253', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1', 'active', NULL, '2021-03-31 04:43:30', 'back', '2021-03-31 04:43:30'),
(128, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '106.222.70.253', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1', 'active', NULL, '2021-03-31 07:27:07', 'back', '2021-03-31 07:27:07'),
(129, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '106.205.211.103', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1', 'active', NULL, '2021-04-05 10:21:42', 'back', '2021-04-05 10:21:42'),
(130, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '27.61.145.151', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1', 'active', NULL, '2021-04-07 04:43:53', 'back', '2021-04-07 04:43:53'),
(131, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '27.61.145.151', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1', 'active', NULL, '2021-04-07 07:33:37', 'back', '2021-04-07 07:33:37'),
(132, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '223.238.227.108', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1', 'active', NULL, '2021-04-09 01:43:46', 'back', '2021-04-09 01:43:46'),
(133, 'b61f2371001133316ade67c1d4ef4ce3', '8128500518', '12345', 1, '49.34.197.60', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36', 'active', NULL, '2021-04-09 01:47:18', 'back', '2021-04-09 01:47:18'),
(134, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '223.238.227.108', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1', 'active', NULL, '2021-04-09 03:14:19', 'back', '2021-04-09 03:14:19'),
(135, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '223.184.211.130', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1', 'active', NULL, '2021-04-10 04:26:47', 'back', '2021-04-10 04:26:47'),
(136, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '223.184.208.130', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1', 'active', NULL, '2021-04-10 15:24:23', 'back', '2021-04-10 15:24:23'),
(137, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '103.83.105.222', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1', 'active', NULL, '2021-04-11 13:02:25', 'back', '2021-04-11 13:02:25'),
(138, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '103.83.105.222', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1', 'active', NULL, '2021-04-12 04:19:28', 'back', '2021-04-12 04:19:28'),
(139, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '27.61.248.155', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1', 'active', NULL, '2021-04-12 07:18:05', 'back', '2021-04-12 07:18:05'),
(140, '29aec7a20cde35c9537b3011d898348b', '9974010036', '12345', 2, '106.213.215.88', 'Mozilla/5.0 (Linux; Android 6.0.1; SM-A700FD) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.93 Mobile Safari/537.36', 'active', NULL, '2021-04-13 07:26:48', 'back', '2021-04-13 07:26:48'),
(141, 'e123c35c759cf7f90dfd77cf963ada8a', '8128500518', '12345', 1, '49.34.51.187', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.72 Safari/537.36 Edg/90.0.818.39', 'active', NULL, '2021-04-16 23:51:43', 'back', '2021-04-16 23:51:43'),
(142, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '103.83.105.222', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1', 'active', NULL, '2021-04-17 02:21:27', 'back', '2021-04-17 02:21:27'),
(143, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '27.61.159.34', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1', 'active', NULL, '2021-04-17 12:29:22', 'back', '2021-04-17 12:29:22'),
(144, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '27.61.215.77', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1', 'active', NULL, '2021-04-20 00:05:11', 'back', '2021-04-20 00:05:11'),
(145, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '103.83.105.222', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1', 'active', NULL, '2021-04-20 23:48:03', 'back', '2021-04-20 23:48:03'),
(146, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '106.222.6.206', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1', 'active', NULL, '2021-04-21 00:51:02', 'back', '2021-04-21 00:51:02'),
(147, 'a28ed537ec91780e28e80c63197ee00b', '9974010036', '12345', 2, '103.83.106.17', 'Mozilla/5.0 (Linux; Android 10; RMX1801) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.105 Mobile Safari/537.36', 'active', NULL, '2021-04-21 04:24:01', 'back', '2021-04-21 04:24:01'),
(148, 'a28ed537ec91780e28e80c63197ee00b', '9974010036', '12345', 2, '103.83.106.17', 'Mozilla/5.0 (Linux; Android 10; RMX1801) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.105 Mobile Safari/537.36', 'active', NULL, '2021-04-21 04:24:02', 'back', '2021-04-21 04:24:02'),
(149, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '103.83.105.222', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1', 'active', NULL, '2021-04-23 04:01:29', 'back', '2021-04-23 04:01:29'),
(150, '6caabd16caec59adb4476a25b96a8490', '9974010036', '12345', 2, '103.83.105.222', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.85 Safari/537.36', 'active', NULL, '2021-04-24 02:18:18', 'back', '2021-04-24 02:18:18'),
(151, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '103.83.105.222', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1', 'active', NULL, '2021-04-24 03:18:59', 'back', '2021-04-24 03:18:59'),
(152, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '103.83.105.222', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1', 'active', NULL, '2021-04-29 06:48:25', 'back', '2021-04-29 06:48:25');
INSERT INTO `session` (`id`, `session_id`, `user_name`, `user_pwd`, `uid`, `ip`, `user_agent`, `status`, `logout_at`, `last_seen`, `session_type`, `stamp`) VALUES
(153, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '106.205.227.97', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1', 'active', NULL, '2021-04-29 10:21:11', 'back', '2021-04-29 10:21:11'),
(154, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '106.205.227.97', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1', 'active', NULL, '2021-04-29 11:52:07', 'back', '2021-04-29 11:52:07'),
(155, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '103.83.105.222', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1', 'active', NULL, '2021-04-30 02:19:24', 'back', '2021-04-30 02:19:24'),
(156, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '106.205.227.107', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1', 'active', NULL, '2021-04-30 03:30:14', 'back', '2021-04-30 03:30:14'),
(157, 'dcc2ee0a28ef2af866a6533a3688b06c', '9974010036', '12345', 2, '103.83.105.222', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36', 'active', NULL, '2021-04-30 03:41:50', 'back', '2021-04-30 03:41:50'),
(158, '24df077a385acb9c3eba6b858ce23ccd', '9974010036', '12345', 2, '106.205.227.107', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/45.7.6.1', 'active', NULL, '2021-04-30 09:53:28', 'back', '2021-04-30 09:53:28'),
(159, 'e0f08cc75505040ea83f90ab286468e4', '9974010036', '12345', 2, '103.83.105.222', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36', 'inactive', '2021-05-01 08:44:03', '2021-05-01 01:40:54', 'back', '2021-05-01 01:40:54'),
(160, 'e0f08cc75505040ea83f90ab286468e4', '9974010036', '12345', 2, '103.83.105.222', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36', 'inactive', '2021-05-01 08:44:03', '2021-05-01 04:40:11', 'back', '2021-05-01 04:40:11'),
(161, 'e0f08cc75505040ea83f90ab286468e4', '9974010036', '12345', 2, '103.83.105.222', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36', 'inactive', '2021-05-01 08:44:03', '2021-05-01 08:31:33', 'back', '2021-05-01 08:31:33'),
(162, 'aed0f7ce618eaff772e2230c00290f70', '9974010036', '12345', 2, '103.83.105.222', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/77.0.3865.116 Mobile Safari/537.36 HeyTapBrowser/45.7.7.1', 'active', NULL, '2021-05-03 05:30:52', 'back', '2021-05-03 05:30:52'),
(163, 'f8d16c68d99511a0e2a32db3d5b8c753', '9974010036', '12345', 2, '103.83.105.222', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36', 'active', NULL, '2021-05-04 06:14:35', 'back', '2021-05-04 06:14:35'),
(164, 'aed0f7ce618eaff772e2230c00290f70', '9974010036', '12345', 2, '103.83.105.222', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/77.0.3865.116 Mobile Safari/537.36 HeyTapBrowser/45.7.7.1', 'active', NULL, '2021-05-06 05:17:52', 'back', '2021-05-06 05:17:52'),
(165, 'aed0f7ce618eaff772e2230c00290f70', '9974010036', '12345', 2, '103.83.105.222', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/77.0.3865.116 Mobile Safari/537.36 HeyTapBrowser/45.7.7.1', 'active', NULL, '2021-05-07 06:48:22', 'back', '2021-05-07 06:48:22'),
(166, 'aed0f7ce618eaff772e2230c00290f70', '9974010036', '12345', 2, '106.222.38.249', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/77.0.3865.116 Mobile Safari/537.36 HeyTapBrowser/45.7.7.1', 'active', NULL, '2021-05-07 09:04:05', 'back', '2021-05-07 09:04:05'),
(167, 'aed0f7ce618eaff772e2230c00290f70', '9974010036', '12345', 2, '106.222.38.249', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/77.0.3865.116 Mobile Safari/537.36 HeyTapBrowser/45.7.7.1', 'active', NULL, '2021-05-07 09:50:20', 'back', '2021-05-07 09:50:20'),
(168, 'c4d89b54fd829adf5aecb1610e4d5151', '8128500518', '12345', 1, '157.32.106.95', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36', 'active', NULL, '2021-05-07 10:22:23', 'back', '2021-05-07 10:22:23'),
(169, '9e5a49871d13df9581b6d8937131f38f', '9974010036', '12345', 2, '103.83.105.222', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36', 'active', NULL, '2021-05-09 07:01:05', 'back', '2021-05-09 07:01:05'),
(170, 'aed0f7ce618eaff772e2230c00290f70', '9974010036', '12345', 2, '223.238.227.96', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/77.0.3865.116 Mobile Safari/537.36 HeyTapBrowser/45.7.7.1', 'active', NULL, '2021-05-11 09:23:03', 'back', '2021-05-11 09:23:03'),
(171, 'c82288b3e6ec69fe4904b390cc5e1a6a', '9974010036', '12345', 2, '103.83.105.222', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36', 'active', NULL, '2021-05-12 04:28:06', 'back', '2021-05-12 04:28:06'),
(172, 'aed0f7ce618eaff772e2230c00290f70', '9974010036', '12345', 2, '27.61.245.168', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/77.0.3865.116 Mobile Safari/537.36 HeyTapBrowser/45.7.7.1', 'active', NULL, '2021-05-13 06:51:24', 'back', '2021-05-13 06:51:24'),
(173, 'aed0f7ce618eaff772e2230c00290f70', '9974010036', '12345', 2, '106.213.203.4', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/77.0.3865.116 Mobile Safari/537.36 HeyTapBrowser/45.7.7.1', 'active', NULL, '2021-05-15 07:40:16', 'back', '2021-05-15 07:40:16'),
(174, 'aed0f7ce618eaff772e2230c00290f70', '9974010036', '12345', 2, '106.222.38.228', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/77.0.3865.116 Mobile Safari/537.36 HeyTapBrowser/45.7.7.1', 'active', NULL, '2021-05-22 13:33:13', 'back', '2021-05-22 13:33:13'),
(175, 'aed0f7ce618eaff772e2230c00290f70', '9974010036', '12345', 2, '103.83.105.222', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/77.0.3865.116 Mobile Safari/537.36 HeyTapBrowser/45.7.7.1', 'active', NULL, '2021-05-23 11:49:45', 'back', '2021-05-23 11:49:45'),
(176, '6375534d164eb3280add3ded05d18828', '8128500518', '12345', 1, '49.34.86.201', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36', 'active', NULL, '2021-05-24 00:59:15', 'back', '2021-05-24 00:59:15'),
(177, 'aed0f7ce618eaff772e2230c00290f70', '9974010036', '12345', 2, '106.222.38.228', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/77.0.3865.116 Mobile Safari/537.36 HeyTapBrowser/45.7.7.1', 'active', NULL, '2021-05-24 07:36:22', 'back', '2021-05-24 07:36:22'),
(178, 'aed0f7ce618eaff772e2230c00290f70', '9974010036', '12345', 2, '106.222.38.228', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/77.0.3865.116 Mobile Safari/537.36 HeyTapBrowser/45.7.7.1', 'active', NULL, '2021-05-24 07:36:23', 'back', '2021-05-24 07:36:23'),
(179, '6375534d164eb3280add3ded05d18828', '8128500518', '12345', 1, '49.34.84.245', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36', 'active', NULL, '2021-05-24 08:40:06', 'back', '2021-05-24 08:40:06'),
(180, 'aed0f7ce618eaff772e2230c00290f70', '9974010036', '12345', 2, '106.213.167.241', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/77.0.3865.116 Mobile Safari/537.36 HeyTapBrowser/45.7.7.1', 'active', NULL, '2021-05-25 07:41:15', 'back', '2021-05-25 07:41:15'),
(181, 'aeb35eeefc25333f3163e2128f8818e6', '9974010036', '12345', 2, '103.83.105.222', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36', 'active', NULL, '2021-05-26 05:19:14', 'back', '2021-05-26 05:19:14'),
(182, 'aed0f7ce618eaff772e2230c00290f70', '9974010036', '12345', 2, '106.213.167.241', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/77.0.3865.116 Mobile Safari/537.36 HeyTapBrowser/45.7.7.1', 'active', NULL, '2021-05-27 03:16:53', 'back', '2021-05-27 03:16:53'),
(183, '38422ec9cec5a69e7c94226c2e8e1c94', '9974010036', '12345', 2, '49.34.159.15', 'Mozilla/5.0 (Linux; Android 10; RMX1801) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.210 Mobile Safari/537.36', 'active', NULL, '2021-05-27 06:58:37', 'back', '2021-05-27 06:58:37'),
(184, '0b472046a0de074ff00116f9636a4fe9', '9974010036', '12345', 2, '49.34.129.113', 'Mozilla/5.0 (Linux; Android 10; RMX1801) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.210 Mobile Safari/537.36', 'active', NULL, '2021-05-28 00:25:38', 'back', '2021-05-28 00:25:38'),
(185, '3c69a1c75c0444f59398a8361e41ce5b', '9974010036', '12345', 2, '103.83.105.222', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36', 'active', NULL, '2021-05-31 04:50:15', 'back', '2021-05-31 04:50:15'),
(186, '9d0284440a8c1a7d3ea32e2ef9c2cae8', '9974010036', '12345', 2, '49.34.141.202', 'Mozilla/5.0 (Linux; Android 10; RMX1801) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.210 Mobile Safari/537.36', 'active', NULL, '2021-06-01 01:23:28', 'back', '2021-06-01 01:23:28'),
(187, 'aed0f7ce618eaff772e2230c00290f70', '9974010036', '12345', 2, '103.83.105.222', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/77.0.3865.116 Mobile Safari/537.36 HeyTapBrowser/45.7.7.1', 'active', NULL, '2021-06-01 03:28:13', 'back', '2021-06-01 03:28:13'),
(188, 'aed0f7ce618eaff772e2230c00290f70', '9974010036', '12345', 2, '27.61.153.199', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/77.0.3865.116 Mobile Safari/537.36 HeyTapBrowser/45.7.7.1', 'active', NULL, '2021-06-02 07:09:48', 'back', '2021-06-02 07:09:48'),
(189, 'aed0f7ce618eaff772e2230c00290f70', '9974010036', '12345', 2, '106.205.227.101', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/77.0.3865.116 Mobile Safari/537.36 HeyTapBrowser/45.7.7.1', 'active', NULL, '2021-06-03 07:30:42', 'back', '2021-06-03 07:30:42'),
(190, 'aed0f7ce618eaff772e2230c00290f70', '9974010036', '12345', 2, '106.205.227.101', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/77.0.3865.116 Mobile Safari/537.36 HeyTapBrowser/45.7.7.1', 'active', NULL, '2021-06-03 07:30:42', 'back', '2021-06-03 07:30:42'),
(191, 'aed0f7ce618eaff772e2230c00290f70', '9974010036', '12345', 2, '106.222.70.203', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/77.0.3865.116 Mobile Safari/537.36 HeyTapBrowser/45.7.7.1', 'active', NULL, '2021-06-04 01:02:10', 'back', '2021-06-04 01:02:10'),
(192, 'f7a58472cbd645b89830e8c86ec8b2c5', '8128500518', '12345', 1, '157.32.99.249', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36', 'active', NULL, '2021-06-04 01:08:44', 'back', '2021-06-04 01:08:44'),
(193, 'aed0f7ce618eaff772e2230c00290f70', '9974010036', '12345', 2, '106.222.70.203', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/77.0.3865.116 Mobile Safari/537.36 HeyTapBrowser/45.7.7.1', 'active', NULL, '2021-06-04 01:43:27', 'back', '2021-06-04 01:43:27'),
(194, 'f7a58472cbd645b89830e8c86ec8b2c5', '8128500518', '12345', 1, '157.32.126.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36', 'active', NULL, '2021-06-04 06:08:28', 'back', '2021-06-04 06:08:28'),
(195, 'f7a58472cbd645b89830e8c86ec8b2c5', '8128500518', '12345', 1, '157.32.126.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36', 'active', NULL, '2021-06-04 07:44:49', 'back', '2021-06-04 07:44:49'),
(196, 'fe06cc0372b2f1d5f5d26be686c34a2c', '8128500518', '12345', 1, '157.32.116.255', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.77 Safari/537.36', 'active', NULL, '2021-06-05 00:22:51', 'back', '2021-06-05 00:22:51'),
(197, '8ce9158acfce7193aeba5f251a90df6e', '9974010036', '12345', 2, '103.83.105.222', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.77 Safari/537.36', 'active', NULL, '2021-06-07 04:51:15', 'back', '2021-06-07 04:51:15'),
(198, 'aed0f7ce618eaff772e2230c00290f70', '9974010036', '12345', 2, '103.83.105.222', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/77.0.3865.116 Mobile Safari/537.36 HeyTapBrowser/45.7.7.1', 'active', NULL, '2021-06-10 05:27:51', 'back', '2021-06-10 05:27:51'),
(199, '5bd48cdcae1b03a3b5f2b8cb65e54485', '8128500518', '12345', 1, '49.34.143.83', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.77 Safari/537.36 Edg/91.0.864.41', 'active', NULL, '2021-06-10 06:40:04', 'back', '2021-06-10 06:40:04'),
(200, '5bd48cdcae1b03a3b5f2b8cb65e54485', '8128500518', '12345', 1, '49.34.143.83', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.77 Safari/537.36 Edg/91.0.864.41', 'active', NULL, '2021-06-10 06:40:04', 'back', '2021-06-10 06:40:04'),
(201, 'aed0f7ce618eaff772e2230c00290f70', '9974010036', '12345', 2, '106.213.243.119', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/77.0.3865.116 Mobile Safari/537.36 HeyTapBrowser/45.7.7.1', 'active', NULL, '2021-06-12 10:15:46', 'back', '2021-06-12 10:15:46'),
(202, 'aed0f7ce618eaff772e2230c00290f70', '9974010036', '12345', 2, '106.205.243.108', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/77.0.3865.116 Mobile Safari/537.36 HeyTapBrowser/45.7.7.1', 'active', NULL, '2021-06-13 02:39:45', 'back', '2021-06-13 02:39:45'),
(203, 'c0f6274397206b8d98936f86492412ec', '9974010036', '12345', 2, '103.83.105.222', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.77 Safari/537.36', 'active', NULL, '2021-06-13 05:56:59', 'back', '2021-06-13 05:56:59'),
(204, 'a330abf328a412e2ecf19aa0a74e6abe', '8128500518', '12345', 1, '49.34.13.93', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.101 Safari/537.36 Edg/91.0.864.48', 'active', NULL, '2021-06-14 00:30:48', 'back', '2021-06-14 00:30:48'),
(205, '6b6d2c4cbfe00b7f81df3e894da5e19f', '8128500518', '12345', 1, '49.34.181.164', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.106 Safari/537.36', 'active', NULL, '2021-06-19 02:07:25', 'back', '2021-06-19 02:07:25'),
(206, 'aed0f7ce618eaff772e2230c00290f70', '9974010036', '12345', 2, '106.222.6.231', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/77.0.3865.116 Mobile Safari/537.36 HeyTapBrowser/45.7.7.1', 'active', NULL, '2021-06-19 02:11:33', 'back', '2021-06-19 02:11:33'),
(207, 'aed0f7ce618eaff772e2230c00290f70', '9974010036', '12345', 2, '106.222.6.231', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/77.0.3865.116 Mobile Safari/537.36 HeyTapBrowser/45.7.7.1', 'active', NULL, '2021-06-19 02:11:34', 'back', '2021-06-19 02:11:34'),
(208, 'aed0f7ce618eaff772e2230c00290f70', '9974010036', '12345', 2, '27.61.205.128', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/77.0.3865.116 Mobile Safari/537.36 HeyTapBrowser/45.7.7.1', 'active', NULL, '2021-06-20 04:10:30', 'back', '2021-06-20 04:10:30'),
(209, 'aed0f7ce618eaff772e2230c00290f70', '9974010036', '12345', 2, '27.61.205.128', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/77.0.3865.116 Mobile Safari/537.36 HeyTapBrowser/45.7.7.1', 'active', NULL, '2021-06-20 04:10:31', 'back', '2021-06-20 04:10:31'),
(210, '741901b7da7f4d7258efc9a627aa95b6', '8128500518', '12345', 1, '49.34.190.92', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36 Edg/91.0.864.54', 'active', NULL, '2021-06-20 06:38:51', 'back', '2021-06-20 06:38:51'),
(211, '0c53fc40d4ded349e797f80fba6b1476', '8128500518', '12345', 1, '49.34.190.92', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.106 Safari/537.36', 'active', NULL, '2021-06-20 07:22:33', 'back', '2021-06-20 07:22:33'),
(212, 'aed0f7ce618eaff772e2230c00290f70', '9974010036', '12345', 2, '27.61.205.128', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/77.0.3865.116 Mobile Safari/537.36 HeyTapBrowser/45.7.7.1', 'active', NULL, '2021-06-20 08:13:37', 'back', '2021-06-20 08:13:37'),
(213, 'd43178f8f5f0acb3d8b4f0c55dfc9d78', '8128500518', '12345', 1, '49.34.99.236', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.106 Safari/537.36', 'active', NULL, '2021-06-22 00:53:11', 'back', '2021-06-22 00:53:11'),
(214, 'd43178f8f5f0acb3d8b4f0c55dfc9d78', '8128500518', '12345', 1, '49.34.99.236', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.106 Safari/537.36', 'active', NULL, '2021-06-22 01:47:15', 'back', '2021-06-22 01:47:15'),
(215, 'aed0f7ce618eaff772e2230c00290f70', '9974010036', '12345', 2, '103.83.105.222', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/77.0.3865.116 Mobile Safari/537.36 HeyTapBrowser/45.7.7.1', 'active', NULL, '2021-06-22 04:26:21', 'back', '2021-06-22 04:26:21'),
(216, 'd43178f8f5f0acb3d8b4f0c55dfc9d78', '8128500518', '12345', 1, '49.34.104.179', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.106 Safari/537.36', 'active', NULL, '2021-06-22 06:38:55', 'back', '2021-06-22 06:38:55'),
(217, 'aed0f7ce618eaff772e2230c00290f70', '9974010036', '12345', 2, '27.61.205.180', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/77.0.3865.116 Mobile Safari/537.36 HeyTapBrowser/45.7.7.1', 'active', NULL, '2021-06-22 08:56:51', 'back', '2021-06-22 08:56:51'),
(218, 'aed0f7ce618eaff772e2230c00290f70', '9974010036', '12345', 2, '27.61.205.122', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/77.0.3865.116 Mobile Safari/537.36 HeyTapBrowser/45.7.7.1', 'active', NULL, '2021-06-24 01:01:53', 'back', '2021-06-24 01:01:53'),
(219, 'aed0f7ce618eaff772e2230c00290f70', '9974010036', '12345', 2, '27.61.205.122', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/77.0.3865.116 Mobile Safari/537.36 HeyTapBrowser/45.7.7.1', 'active', NULL, '2021-06-24 02:45:25', 'back', '2021-06-24 02:45:25'),
(220, 'aed0f7ce618eaff772e2230c00290f70', '9974010036', '12345', 2, '103.83.105.222', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/77.0.3865.116 Mobile Safari/537.36 HeyTapBrowser/45.7.7.1', 'active', NULL, '2021-06-24 06:10:04', 'back', '2021-06-24 06:10:04'),
(221, 'aed0f7ce618eaff772e2230c00290f70', '9974010036', '12345', 2, '103.83.105.222', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/77.0.3865.116 Mobile Safari/537.36 HeyTapBrowser/45.7.7.1', 'active', NULL, '2021-06-25 01:35:34', 'back', '2021-06-25 01:35:34'),
(222, '6435a1de4a1f1894189056357a3963c7', '8128500518', '12345', 1, '157.32.82.130', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36', 'active', NULL, '2021-06-25 07:52:48', 'back', '2021-06-25 07:52:48'),
(223, 'aed0f7ce618eaff772e2230c00290f70', '9974010036', '12345', 2, '27.61.205.144', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/77.0.3865.116 Mobile Safari/537.36 HeyTapBrowser/45.7.7.1', 'active', NULL, '2021-06-26 01:07:44', 'back', '2021-06-26 01:07:44'),
(224, 'd1eadfd13e3f5fa71bdab91b7165f449', '9974010036', '12345', 2, '103.83.105.222', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36', 'active', NULL, '2021-06-27 02:56:58', 'back', '2021-06-27 02:56:58'),
(225, 'aed0f7ce618eaff772e2230c00290f70', '9974010036', '12345', 2, '106.213.243.120', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/77.0.3865.116 Mobile Safari/537.36 HeyTapBrowser/45.7.7.1', 'active', NULL, '2021-06-27 06:27:20', 'back', '2021-06-27 06:27:20'),
(226, 'aed0f7ce618eaff772e2230c00290f70', '9974010036', '12345', 2, '106.213.243.120', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/77.0.3865.116 Mobile Safari/537.36 HeyTapBrowser/45.7.7.1', 'active', NULL, '2021-06-28 08:48:17', 'back', '2021-06-28 08:48:17'),
(227, 'b6db7ab306966cd5624fbcbac9c49104', '8128500518', '12345', 1, '157.32.70.33', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36', 'active', NULL, '2021-06-28 09:33:23', 'back', '2021-06-28 09:33:23'),
(228, 'b6db7ab306966cd5624fbcbac9c49104', '8128500518', '12345', 1, '157.32.70.33', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36', 'active', NULL, '2021-06-28 09:33:24', 'back', '2021-06-28 09:33:24'),
(229, 'aed0f7ce618eaff772e2230c00290f70', '9974010036', '12345', 2, '103.83.105.222', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/77.0.3865.116 Mobile Safari/537.36 HeyTapBrowser/45.7.7.1', 'active', NULL, '2021-06-28 11:49:50', 'back', '2021-06-28 11:49:50'),
(230, 'aed0f7ce618eaff772e2230c00290f70', '9974010036', '12345', 2, '103.83.105.222', 'Mozilla/5.0 (Linux; U; Android 10; en-gb; CPH2001 Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/77.0.3865.116 Mobile Safari/537.36 HeyTapBrowser/45.7.7.1', 'active', NULL, '2021-06-28 14:43:59', 'back', '2021-06-28 14:43:59'),
(231, 'c8ae4fa942202ba8c4d3606c79f92673', '8128500518', '12345', 1, '157.32.78.190', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36 Edg/91.0.864.59', 'active', NULL, '2021-06-28 22:41:54', 'back', '2021-06-28 22:41:54'),
(232, 'e5cd909572f47b7a6f87c4c78625b4e2', '8128500518', '12345', 1, '157.32.78.190', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36', 'active', NULL, '2021-06-28 23:53:32', 'back', '2021-06-28 23:53:32');

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE `setting` (
  `id` int(11) NOT NULL,
  `key` varchar(100) NOT NULL,
  `val` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`id`, `key`, `val`) VALUES
(1, 'upload_path', 'images/uploads/'),
(2, 'img_height', '400'),
(3, 'img_width', '400'),
(4, 'app_version', '1.0.12'),
(5, 'adv_mobile', '9725138799'),
(6, 'bank_name', 'Shri Rajkot district co-operative Bank ltd.'),
(7, 'account_name', 'Leva Patel nutan kelv Mandal . jasdan'),
(8, 'account_no', '614003019342'),
(9, 'ifsc_code', 'GSCB0RJT001'),
(10, 'branch_name', 'Jasdan'),
(11, 'phonepay_mobile', ''),
(12, 'phonepay_name', 'શ્રી લેઉવા પટેલ નૂતન કેળવણી મંડળ'),
(13, 'gpay_mobile', ''),
(14, 'gpay_name', 'શ્રી લેઉવા પટેલ નૂતન કેળવણી મંડળ');

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE `slider` (
  `id` int(11) NOT NULL,
  `image` varchar(1000) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`id`, `image`, `status`, `date`) VALUES
(1, '5a058330fb942cb108d1f7d797c4d329FAMILY_FOTO_1.png', -1, '2021-01-09 02:05:40'),
(2, '2ae6e97798a0363b9fe89d435ad6c6a3FAMILY_FOTO_2.png', -1, '2021-01-09 02:06:08'),
(3, '42b951d2b9c333464805031ae07ef07cFAMILY_FOTO5.png', -1, '2021-01-09 02:06:32'),
(4, 'b42a585960d3115d33d472ea31c382eeFAMILY_FOTO_3.png', -1, '2021-01-09 02:07:01'),
(5, '133ee5663ca1e0cdd5d8d77b0011cdddFAMILY_FOTO_4.png', -1, '2021-01-09 02:07:27'),
(6, '5f446619a9f63115179023315f0304a2WhatsApp_Image_2021-02-05_at_09.40.58_(1).jpeg', -1, '2021-02-13 11:35:46'),
(7, 'f28a34054e8d4b0461c1df4e9333ba44WhatsApp_Image_2021-02-05_at_09.40.58.jpeg', -1, '2021-02-13 11:36:08'),
(8, 'f28a34054e8d4b0461c1df4e9333ba44WhatsApp_Image_2021-02-05_at_09.40.58.jpeg', -1, '2021-02-13 11:36:24'),
(9, 'a96d0565d6c567e08270d468817f5426WhatsApp_Image_2021-02-05_at_09.40.59.jpeg', -1, '2021-02-13 11:36:41'),
(10, 'ad0131f48e37893ac151402855978b81WhatsApp_Image_2021-02-05_at_09.41.00_(1).jpeg', -1, '2021-02-13 11:38:26'),
(11, 'fd16a2049dad28af56992bc7eb5deb99WhatsApp_Image_2021-02-03_at_21.10.32.jpeg', -1, '2021-02-13 11:40:52'),
(12, '17f66e5c649cb8aa4cd990cae210c2ebWhatsApp_Image_2021-02-05_at_09.41.00.jpeg', -1, '2021-02-13 11:41:15'),
(13, '11debb377f03d4d04402e8c18e87376cWhatsApp_Image_2021-02-05_at_09.41.02_(1).jpeg', -1, '2021-02-13 11:41:34'),
(14, 'a0180e2afa15c73432e714754955629dWhatsApp_Image_2021-02-05_at_09.41.02.jpeg', -1, '2021-02-13 11:41:53'),
(15, '1bfc56715da364fe9f845e6bac031ddeWhatsApp_Image_2021-02-13_at_22.47.02.jpeg', -1, '2021-02-13 12:23:35'),
(16, '3deff4b9e87774cdaf10e0d4333a5bfbIMG-20210307-WA0078.jpg', -1, '2021-03-07 03:11:51'),
(17, '3deff4b9e87774cdaf10e0d4333a5bfbIMG-20210307-WA0078.jpg', -1, '2021-03-07 03:12:02'),
(18, '', -1, '2021-03-09 11:54:01'),
(19, '2619f93a1a32abb00f5e0c28b9921eedIMG20210307154022.jpg', 1, '2021-03-09 11:54:32'),
(20, 'b0061818aa4100d683283db9107a083fIMG20210307154022.jpg', -1, '2021-03-09 11:55:01'),
(21, '5caf5c864161c0a920cfa05f318a1b07IMG20210213102909.jpg', 1, '2021-03-09 11:56:09'),
(22, '9fb3cab5d7df5648f698c3ec741ce8ffIMG-20210308-WA0170.jpg', 1, '2021-03-09 11:56:51'),
(23, '000c9f0372385d47a2b1d18f6abb016cIMG-20210205-WA0058.jpg', 1, '2021-03-09 11:59:40'),
(24, '000c9f0372385d47a2b1d18f6abb016cIMG-20210205-WA0058.jpg', -1, '2021-03-09 12:01:06'),
(25, '0b4ad2443a5b7740a6ee2c18feff8b04IMG-20210205-WA0047.jpg', 1, '2021-03-09 12:01:44'),
(26, '1cc04d1753f325c353fdf6d037907b9fIMG-20210205-WA0051.jpg', 1, '2021-03-09 12:06:01'),
(27, 'd78845c527ba92f4d0c1748481375faaIMG-20210227-WA0108.jpg', 1, '2021-03-10 02:35:40'),
(28, 'd1dd21a754e182bbd65a14e6706b1d58IMG-20210307-WA0075.jpg', -1, '2021-03-10 02:36:11'),
(29, '25e2358c2143b1128ead02a418d40b1cIMG-20210214-WA0065.jpg', 1, '2021-03-10 02:39:24'),
(30, '4c2e13abf3163cce0ad3a86a7e0b0393IMG-20210312-WA0080.jpg', -1, '2021-03-12 02:32:35'),
(31, '097220a085f849f634ab9aa17cd73b17IMG-20210312-WA0080.jpg', 1, '2021-03-12 07:29:59'),
(32, 'c388f5ebe609b1255041407d5f300964IMG-20210314-WA0032.jpg', 1, '2021-03-14 01:54:47'),
(33, '434d3a497c3ac962c30bcdf4a24ca082IMG-20210326-WA0124.jpg', 1, '2021-03-26 06:42:28');

-- --------------------------------------------------------

--
-- Table structure for table `std_master`
--

CREATE TABLE `std_master` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `std_master`
--

INSERT INTO `std_master` (`id`, `name`, `status`, `date`) VALUES
(1, '1', -1, '2021-05-05 08:42:38'),
(2, '2', -1, '2021-05-05 08:42:44'),
(3, '3', -1, '2021-05-05 08:42:58'),
(4, '4', -1, '2021-05-05 08:43:06'),
(5, '5', -1, '2021-05-05 08:43:13'),
(6, '6', -1, '2021-05-05 08:43:20'),
(7, '7', -1, '2021-05-11 18:23:57'),
(8, '10', 1, '2021-05-27 17:59:02'),
(9, '11', 1, '2021-06-18 15:28:12'),
(10, '9', 1, '2021-06-20 04:12:04'),
(11, '12', 1, '2021-06-20 04:12:16'),
(12, 'બિન સચિવાલય', 1, '2021-06-20 04:14:58'),
(13, 'તલાટી', 1, '2021-06-20 04:13:00'),
(14, 'પોલીસ', 1, '2021-06-20 04:13:09'),
(15, 'GPSC', 1, '2021-06-20 04:13:21'),
(16, 'UPSC', 1, '2021-06-20 04:13:35');

-- --------------------------------------------------------

--
-- Table structure for table `student_scholarship`
--

CREATE TABLE `student_scholarship` (
  `id` int(11) NOT NULL,
  `student_name` varchar(500) NOT NULL,
  `mobile` varchar(15) NOT NULL,
  `parents_name` varchar(500) NOT NULL,
  `parents_mobile` varchar(15) NOT NULL,
  `address` varchar(1000) NOT NULL,
  `city` varchar(100) NOT NULL,
  `join_date` date NOT NULL,
  `reference` varchar(200) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `student_scholarship`
--

INSERT INTO `student_scholarship` (`id`, `student_name`, `mobile`, `parents_name`, `parents_mobile`, `address`, `city`, `join_date`, `reference`, `status`, `date`) VALUES
(1, 'Jayanit Satani', '8128500518', 'Vithalbhai satani', '9687581020', 'B/H Gampanchayat off., Dharai', 'Dharai', '2021-06-22', 'Kamlesh Hirpara', -1, '2021-06-22 06:39:55'),
(2, 'Keval', '9879879876', 'Kaak', '', 'Jasdan', 'Jasd', '2021-06-01', 'Kamlesh', 1, '2021-06-24 02:47:39');

-- --------------------------------------------------------

--
-- Table structure for table `student_scholarship_info`
--

CREATE TABLE `student_scholarship_info` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `std_id` int(11) NOT NULL,
  `school_name` varchar(1000) NOT NULL,
  `school_address` varchar(1000) NOT NULL,
  `school_mobile` varchar(15) NOT NULL,
  `donation_id` int(11) NOT NULL,
  `total_fees` double NOT NULL,
  `scholarship_fees` double NOT NULL,
  `donation_fees` double NOT NULL,
  `fees_date` int(1) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `student_scholarship_info`
--

INSERT INTO `student_scholarship_info` (`id`, `student_id`, `std_id`, `school_name`, `school_address`, `school_mobile`, `donation_id`, `total_fees`, `scholarship_fees`, `donation_fees`, `fees_date`, `status`, `date`) VALUES
(5, 0, 13, 'Abcd', 'Jasdan', '123456789', 0, 5000, 3000, 0, 0, -1, '2021-06-20 04:15:34'),
(6, 0, 12, 'Patel School', 'Atkot', '123564789', 2, 5000, 2000, 0, 0, -1, '2021-06-20 04:14:30'),
(7, 1, 13, '', '', '', 0, 0, 0, 0, 0, -1, '2021-06-20 04:15:51'),
(8, 1, 12, 'Shantiniketan school', 'Jasdan', '1236547890', 2, 5000, 3000, 2000, 0, 1, '2021-06-22 01:53:11');

-- --------------------------------------------------------

--
-- Table structure for table `surname`
--

CREATE TABLE `surname` (
  `id` int(11) NOT NULL,
  `samaj_id` int(11) NOT NULL COMMENT '1 - leva 2- kadva 3- other',
  `name` varchar(1000) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `surname`
--

INSERT INTO `surname` (`id`, `samaj_id`, `name`, `status`, `date`) VALUES
(1, 1, 'KATBA', 1, '2021-02-08 16:56:33'),
(2, 1, 'KACHCHHI', 1, '2021-02-08 16:56:33'),
(3, 1, 'KALSARIYA', 1, '2021-02-08 16:56:33'),
(4, 1, 'KAPUPARA', 1, '2021-02-08 16:56:33'),
(5, 1, 'KAMANI', 1, '2021-02-08 16:56:33'),
(6, 1, 'KAPURIYA', 1, '2021-02-08 16:56:33'),
(7, 1, 'KALKANI', 1, '2021-02-08 16:56:33'),
(8, 1, 'KANKOTIYA', 1, '2021-02-08 16:56:33'),
(9, 1, 'KARAD', 1, '2021-02-08 16:56:33'),
(10, 1, 'KARKAR', 1, '2021-02-08 16:56:33'),
(11, 1, 'KASVALA', 1, '2021-02-08 16:56:33'),
(12, 1, 'KATHIRIYTA', 1, '2021-02-08 16:56:33'),
(13, 1, 'KALTHIYA', 1, '2021-02-08 16:56:33'),
(14, 1, 'KYADA', 1, '2021-02-08 16:56:33'),
(15, 1, 'KANBODI', 1, '2021-02-08 16:56:33'),
(16, 1, 'KABARIYA', 1, '2021-02-08 16:56:34'),
(17, 1, 'KABRA', 1, '2021-02-08 16:56:34'),
(18, 1, 'KACHHADIYA', 1, '2021-02-08 16:56:34'),
(19, 1, 'KAKDIYA', 1, '2021-02-08 16:56:34'),
(20, 1, 'JARGALIYA', 1, '2021-02-08 16:56:34'),
(21, 1, 'JASANI', 1, '2021-02-08 16:56:34'),
(22, 1, 'JAGANI', 1, '2021-02-08 16:56:34'),
(23, 1, 'JADVANI', 1, '2021-02-08 16:56:34'),
(24, 1, 'JAJDIYA', 1, '2021-02-08 16:56:34'),
(25, 1, 'JASOLIYA', 1, '2021-02-08 16:56:34'),
(26, 1, 'JIYANI', 1, '2021-02-08 16:56:34'),
(27, 1, 'JETANI', 1, '2021-02-08 16:56:34'),
(28, 1, 'JESANI', 1, '2021-02-08 16:56:34'),
(29, 1, 'JOGHANI', 1, '2021-02-08 16:56:34'),
(30, 1, 'JOGANI', 1, '2021-02-08 16:56:34'),
(31, 1, 'JAGVADIYA', 1, '2021-02-08 16:56:34'),
(32, 1, 'ZADAFIYA', 1, '2021-02-08 16:56:34'),
(33, 1, 'ZALAVADIYA', 1, '2021-02-08 16:56:34'),
(34, 1, 'TADHA', 1, '2021-02-08 16:56:34'),
(35, 1, 'TADHANI', 1, '2021-02-08 16:56:34'),
(36, 1, 'TILALA', 1, '2021-02-08 16:56:34'),
(37, 1, 'TIKADIYA', 1, '2021-02-08 16:56:34'),
(38, 1, 'TIMBADIYA', 1, '2021-02-08 16:56:35'),
(39, 1, 'TOPIYA', 1, '2021-02-08 16:56:35'),
(40, 1, 'THUMAR', 1, '2021-02-08 16:56:35'),
(41, 1, 'THUMBAR', 1, '2021-02-08 16:56:35'),
(42, 1, 'THESIYA', 1, '2021-02-08 16:56:35'),
(43, 1, 'DABHOYA', 1, '2021-02-08 16:56:35'),
(44, 1, 'DABSHIYA', 1, '2021-02-08 16:56:35'),
(45, 1, 'DAMASHIYA', 1, '2021-02-08 16:56:35'),
(46, 1, 'DAVRA', 1, '2021-02-08 16:56:35'),
(47, 1, 'DAKA', 1, '2021-02-08 16:56:35'),
(48, 1, 'DANKHRA', 1, '2021-02-08 16:56:35'),
(49, 1, 'DANGARIYA', 1, '2021-02-08 16:56:35'),
(50, 1, 'DUNGRANI', 1, '2021-02-08 16:56:35'),
(51, 1, 'DODA', 1, '2021-02-08 16:56:35'),
(52, 1, 'RABDIYA', 1, '2021-02-08 16:56:35'),
(53, 1, 'RAJANI', 1, '2021-02-08 16:56:35'),
(54, 1, 'RAMOLIYA', 1, '2021-02-08 16:56:35'),
(55, 1, 'RAKHOLIYA', 1, '2021-02-08 16:56:35'),
(56, 1, 'RANOLIYA', 1, '2021-02-08 16:56:35'),
(57, 1, 'RAMANI', 1, '2021-02-08 16:56:35'),
(58, 1, 'RANK', 1, '2021-02-08 16:56:35'),
(59, 1, 'RANPARIYA', 1, '2021-02-08 16:56:35'),
(60, 1, 'RADADIYA', 1, '2021-02-08 16:56:35'),
(61, 1, 'RAJAPARA', 1, '2021-02-08 16:56:35'),
(62, 1, 'RIBADIYA', 1, '2021-02-08 16:56:35'),
(63, 1, 'RUDANI', 1, '2021-02-08 16:56:36'),
(64, 1, 'RUPARELIYA', 1, '2021-02-08 16:56:36'),
(65, 1, 'RUPAPARA', 1, '2021-02-08 16:56:36'),
(66, 1, 'RUPANI', 1, '2021-02-08 16:56:36'),
(67, 1, 'REYANI', 1, '2021-02-08 16:56:36'),
(68, 1, 'RANGANI', 1, '2021-02-08 16:56:36'),
(69, 1, 'RANGPARIYA', 1, '2021-02-08 16:56:36'),
(70, 1, 'ROKAD', 1, '2021-02-08 16:56:36'),
(71, 1, 'ROY', 1, '2021-02-08 16:56:36'),
(72, 1, 'ROLA', 1, '2021-02-08 16:56:36'),
(73, 1, 'LAHERI', 1, '2021-02-08 16:56:36'),
(74, 1, 'LAKKAD', 1, '2021-02-08 16:56:36'),
(75, 1, 'LAKANI', 1, '2021-02-08 16:56:36'),
(76, 1, 'LASHKARI', 1, '2021-02-08 16:56:36'),
(77, 1, 'LAKHANI', 1, '2021-02-08 16:56:36'),
(78, 1, 'LALANI', 1, '2021-02-08 16:56:36'),
(79, 1, 'LILA', 1, '2021-02-08 16:56:36'),
(80, 1, 'LIMBANI', 1, '2021-02-08 16:56:36'),
(81, 1, 'LIBSHIYA', 1, '2021-02-08 16:56:36'),
(82, 1, 'LUKHI', 1, '2021-02-08 16:56:36'),
(83, 1, 'LUNAGARIYA', 1, '2021-02-08 16:56:36'),
(84, 1, 'KANANI', 1, '2021-02-08 16:56:36'),
(85, 1, 'KANGAD', 1, '2021-02-08 16:56:36'),
(86, 1, 'KAPDIYA', 1, '2021-02-08 16:56:36'),
(87, 1, 'KAJAVADARA', 1, '2021-02-08 16:56:37'),
(88, 1, 'KATHROTIYA', 1, '2021-02-08 16:56:37'),
(89, 1, 'KASADARIYA', 1, '2021-02-08 16:56:37'),
(90, 1, 'KAVANI', 1, '2021-02-08 16:56:37'),
(91, 1, 'KANPARIYA', 1, '2021-02-08 16:56:37'),
(92, 1, 'KATRODIYA', 1, '2021-02-08 16:56:37'),
(93, 1, 'KIKANI', 1, '2021-02-08 16:56:37'),
(94, 1, 'KUNJADIYA', 1, '2021-02-08 16:56:37'),
(95, 1, 'KUKADIYA', 1, '2021-02-08 16:56:37'),
(96, 1, 'KUNADIYA', 1, '2021-02-08 16:56:37'),
(97, 1, 'KUNBHANI', 1, '2021-02-08 16:56:37'),
(98, 1, 'KERAI', 1, '2021-02-08 16:56:37'),
(99, 1, 'KERALIYA', 1, '2021-02-08 16:56:37'),
(100, 1, 'KESARI', 1, '2021-02-08 16:56:37'),
(101, 1, 'KEVADIYA', 1, '2021-02-08 16:56:37'),
(102, 1, 'KOLADIYA', 1, '2021-02-08 16:56:37'),
(103, 1, 'DOBARIYA', 1, '2021-02-08 16:56:37'),
(104, 1, 'DHANKECHA', 1, '2021-02-08 16:56:37'),
(105, 1, 'DHEDHI', 1, '2021-02-08 16:56:38'),
(106, 1, 'DHEBARIYA', 1, '2021-02-08 16:56:38'),
(107, 1, 'DHOLA', 1, '2021-02-08 16:56:38'),
(108, 1, 'DHOLARIYA', 1, '2021-02-08 16:56:38'),
(109, 1, 'TALPADA', 1, '2021-02-08 16:56:38'),
(110, 1, 'TALAVIYA', 1, '2021-02-08 16:56:38'),
(111, 1, 'TARPARA', 1, '2021-02-08 16:56:38'),
(112, 1, 'TADA', 1, '2021-02-08 16:56:38'),
(113, 1, 'TEJANI', 1, '2021-02-08 16:56:38'),
(114, 1, 'TOGADIYA', 1, '2021-02-08 16:56:38'),
(115, 1, 'TANTI', 1, '2021-02-08 16:56:38'),
(116, 1, 'TRAPASIYA', 1, '2021-02-08 16:56:38'),
(117, 1, 'DUDHAGARA', 1, '2021-02-08 16:56:38'),
(118, 1, 'DUDHAT', 1, '2021-02-08 16:56:38'),
(119, 1, 'DUDHATRA', 1, '2021-02-08 16:56:38'),
(120, 1, 'DIYORA', 1, '2021-02-08 16:56:38'),
(121, 1, 'DESAI', 1, '2021-02-08 16:56:38'),
(122, 1, 'DEVANI', 1, '2021-02-08 16:56:38'),
(123, 1, 'DEVEIYA', 1, '2021-02-08 16:56:38'),
(124, 1, 'DOMADIYA', 1, '2021-02-08 16:56:38'),
(125, 1, 'DONGA', 1, '2021-02-08 16:56:38'),
(126, 1, 'DHADUK', 1, '2021-02-08 16:56:38'),
(127, 1, 'DHAMELIYA', 1, '2021-02-08 16:56:38'),
(128, 1, 'DHAMI', 1, '2021-02-08 16:56:38'),
(129, 1, 'DHANANI', 1, '2021-02-08 16:56:39'),
(130, 1, 'DHUDESHIYA', 1, '2021-02-08 16:56:39'),
(131, 1, 'DHORAJIYA', 1, '2021-02-08 16:56:39'),
(132, 1, 'DHOLIYA', 1, '2021-02-08 16:56:39'),
(133, 1, 'DHILKIYA', 1, '2021-02-08 16:56:39'),
(134, 1, 'NADIYADHRA', 1, '2021-02-08 16:56:39'),
(135, 1, 'LODVIYA', 1, '2021-02-08 16:56:39'),
(136, 1, 'VASOYA', 1, '2021-02-08 16:56:39'),
(137, 1, 'VADODARIYA', 1, '2021-02-08 16:56:39'),
(138, 1, 'VADODARIYA', 1, '2021-02-08 16:56:39'),
(139, 1, 'VANPARIYA', 1, '2021-02-08 16:56:39'),
(140, 1, 'VARSANI', 1, '2021-02-08 16:56:39'),
(141, 1, 'VAGHASIYA', 1, '2021-02-08 16:56:39'),
(142, 1, 'VAGHJIYANI', 1, '2021-02-08 16:56:39'),
(143, 1, 'VAGADIYA', 1, '2021-02-08 16:56:39'),
(144, 1, 'VAGHANI', 1, '2021-02-08 16:56:39'),
(145, 1, 'VASANI', 1, '2021-02-08 16:56:39'),
(146, 1, 'VADI', 1, '2021-02-08 16:56:39'),
(147, 1, 'VANANI', 1, '2021-02-08 16:56:39'),
(148, 1, 'VANECHA', 1, '2021-02-08 16:56:39'),
(149, 1, 'VIRADIYA', 1, '2021-02-08 16:56:39'),
(150, 1, 'VIRANI', 1, '2021-02-08 16:56:39'),
(151, 1, 'VISAVELIYA', 1, '2021-02-08 16:56:39'),
(152, 1, 'VIROLIYA', 1, '2021-02-08 16:56:39'),
(153, 1, 'VEKARIYA', 1, '2021-02-08 16:56:39'),
(154, 1, 'VELANI', 1, '2021-02-08 16:56:39'),
(155, 1, 'VEJPARA', 1, '2021-02-08 16:56:39'),
(156, 1, 'VESHNAV', 1, '2021-02-08 16:56:39'),
(157, 1, 'VORA', 1, '2021-02-08 16:56:40'),
(158, 1, 'SHANI', 1, '2021-02-08 16:56:40'),
(159, 1, 'SHIHORA', 1, '2021-02-08 16:56:40'),
(160, 1, 'SHINGALA', 1, '2021-02-08 16:56:40'),
(161, 1, 'SHIROYA', 1, '2021-02-08 16:56:40'),
(162, 1, 'SHIYANI', 1, '2021-02-08 16:56:40'),
(163, 1, 'SHEKHDA', 1, '2021-02-08 16:56:40'),
(164, 1, 'SHEKHALIYA', 1, '2021-02-08 16:56:40'),
(165, 1, 'SHETA', 1, '2021-02-08 16:56:40'),
(166, 1, 'SHAKHAVARA', 1, '2021-02-08 16:56:40'),
(167, 1, 'KODINARIYA', 1, '2021-02-08 16:56:40'),
(168, 1, 'KORAT', 1, '2021-02-08 16:56:40'),
(169, 1, 'KOTADIYA', 1, '2021-02-08 16:56:40'),
(170, 1, 'KOTHARI', 1, '2021-02-08 16:56:40'),
(171, 1, 'KORADIYA', 1, '2021-02-08 16:56:40'),
(172, 1, 'KOTHIYA', 1, '2021-02-08 16:56:40'),
(173, 1, 'KOYANI', 1, '2021-02-08 16:56:40'),
(174, 1, 'KOSHIYA', 1, '2021-02-08 16:56:40'),
(175, 1, 'KHAKHARIYA', 1, '2021-02-08 16:56:40'),
(176, 1, 'KHATARA', 1, '2021-02-08 16:56:40'),
(177, 1, 'KHACHARIYA', 1, '2021-02-08 16:56:40'),
(178, 1, 'KHICHADIYA', 1, '2021-02-08 16:56:40'),
(179, 1, 'KHIMANI', 1, '2021-02-08 16:56:40'),
(180, 1, 'KHETANI', 1, '2021-02-08 16:56:40'),
(181, 1, 'KHER', 1, '2021-02-08 16:56:40'),
(182, 1, 'KHENI', 1, '2021-02-08 16:56:40'),
(183, 1, 'KHUNT', 1, '2021-02-08 16:56:40'),
(184, 1, 'KHOYANI', 1, '2021-02-08 16:56:40'),
(185, 1, 'GADHESARIYA', 1, '2021-02-08 16:56:40'),
(186, 1, 'NAVAPARA', 1, '2021-02-08 16:56:41'),
(187, 1, 'NASIT', 1, '2021-02-08 16:56:41'),
(188, 1, 'NAMERA', 1, '2021-02-08 16:56:41'),
(189, 1, 'NAGANI', 1, '2021-02-08 16:56:41'),
(190, 1, 'NAKRANI', 1, '2021-02-08 16:56:41'),
(191, 1, 'NARIYA', 1, '2021-02-08 16:56:41'),
(192, 1, 'NADA', 1, '2021-02-08 16:56:41'),
(193, 1, 'NAROLA', 1, '2021-02-08 16:56:41'),
(194, 1, 'NADODA', 1, '2021-02-08 16:56:41'),
(195, 1, 'NAYANI', 1, '2021-02-08 16:56:41'),
(196, 1, 'NATHANI', 1, '2021-02-08 16:56:41'),
(197, 1, 'NOGHANVADRA', 1, '2021-02-08 16:56:41'),
(198, 1, 'NANDANIYA', 1, '2021-02-08 16:56:41'),
(199, 1, 'PADARIYA', 1, '2021-02-08 16:56:41'),
(200, 1, 'PADMANI', 1, '2021-02-08 16:56:41'),
(201, 1, 'PADASHALA', 1, '2021-02-08 16:56:41'),
(202, 1, 'PARSANA', 1, '2021-02-08 16:56:41'),
(203, 1, 'PATOLIYA', 1, '2021-02-08 16:56:41'),
(204, 1, 'PADARIYA', 1, '2021-02-08 16:56:41'),
(205, 1, 'PAGHDAL', 1, '2021-02-08 16:56:41'),
(206, 1, 'PALADIYA', 1, '2021-02-08 16:56:41'),
(207, 1, 'PADALIYA', 1, '2021-02-08 16:56:41'),
(208, 1, 'PATADIYA', 1, '2021-02-08 16:56:41'),
(209, 1, 'PAMBHAR', 1, '2021-02-08 16:56:41'),
(210, 1, 'PANCHANI', 1, '2021-02-08 16:56:41'),
(211, 1, 'PANELIYA', 1, '2021-02-08 16:56:41'),
(212, 1, 'PANSARA', 1, '2021-02-08 16:56:41'),
(213, 1, 'PANSERIYA', 1, '2021-02-08 16:56:41'),
(214, 1, 'PANSURIYA', 1, '2021-02-08 16:56:41'),
(215, 1, 'PARKHIYA', 1, '2021-02-08 16:56:41'),
(216, 1, 'PIPALIYA', 1, '2021-02-08 16:56:41'),
(217, 1, 'PIPALVA', 1, '2021-02-08 16:56:41'),
(218, 1, 'RAJODIYA', 1, '2021-02-08 16:56:41'),
(219, 1, 'SATASHIYA', 1, '2021-02-08 16:56:41'),
(220, 1, 'SABHAYA', 1, '2021-02-08 16:56:41'),
(221, 1, 'SAGPARIYA', 1, '2021-02-08 16:56:42'),
(222, 1, 'SAKHIYA', 1, '2021-02-08 16:56:42'),
(223, 1, 'SAKHRELIYA', 1, '2021-02-08 16:56:42'),
(224, 1, 'SAKHVADA', 1, '2021-02-08 16:56:42'),
(225, 1, 'SALIYA', 1, '2021-02-08 16:56:42'),
(226, 1, 'SARVALIYA', 1, '2021-02-08 16:56:42'),
(227, 1, 'SABHANI', 1, '2021-02-08 16:56:42'),
(228, 1, 'SAVSAVIYA', 1, '2021-02-08 16:56:42'),
(229, 1, 'SARDHARA', 1, '2021-02-08 16:56:42'),
(230, 1, 'SARKHELIYA', 1, '2021-02-08 16:56:42'),
(231, 1, 'SARKHEDIYA', 1, '2021-02-08 16:56:42'),
(232, 1, 'SATODIYA', 1, '2021-02-08 16:56:42'),
(233, 1, 'SAVAKIYA', 1, '2021-02-08 16:56:42'),
(234, 1, 'SAVALIYA', 1, '2021-02-08 16:56:42'),
(235, 1, 'SABALPARA', 1, '2021-02-08 16:56:42'),
(236, 1, 'SAKARIYA', 1, '2021-02-08 16:56:42'),
(237, 1, 'SAVAJ', 1, '2021-02-08 16:56:42'),
(238, 1, 'SAPARIYA', 1, '2021-02-08 16:56:42'),
(239, 1, 'SANGANI', 1, '2021-02-08 16:56:42'),
(240, 1, 'SACHPARA', 1, '2021-02-08 16:56:42'),
(241, 1, 'SIDHDHPURA', 1, '2021-02-08 16:56:42'),
(242, 1, 'SADPARA', 1, '2021-02-08 16:56:42'),
(243, 1, 'SHELADIYA', 1, '2021-02-08 16:56:42'),
(244, 1, 'SENJALIYA', 1, '2021-02-08 16:56:42'),
(245, 1, 'SELARKA', 1, '2021-02-08 16:56:42'),
(246, 1, 'SURANI', 1, '2021-02-08 16:56:42'),
(247, 1, 'SUTARSADHIYA', 1, '2021-02-08 16:56:42'),
(248, 1, 'SUDANI', 1, '2021-02-08 16:56:42'),
(249, 1, 'SUTARIYA', 1, '2021-02-08 16:56:42'),
(250, 1, 'GADHIYA', 1, '2021-02-08 16:56:42'),
(251, 1, 'GARANBHA', 1, '2021-02-08 16:56:43'),
(252, 1, 'GAJERA', 1, '2021-02-08 16:56:43'),
(253, 1, 'GARSONDIYA', 1, '2021-02-08 16:56:43'),
(254, 1, 'GAMDHA', 1, '2021-02-08 16:56:43'),
(255, 1, 'GALANI', 1, '2021-02-08 16:56:43'),
(256, 1, 'GAJIPARA', 1, '2021-02-08 16:56:43'),
(257, 1, 'GABANI', 1, '2021-02-08 16:56:43'),
(258, 1, 'GAMI', 1, '2021-02-08 16:56:43'),
(259, 1, 'GANGANI', 1, '2021-02-08 16:56:43'),
(260, 1, 'GINOYA', 1, '2021-02-08 16:56:43'),
(261, 1, 'GUNA', 1, '2021-02-08 16:56:43'),
(262, 1, 'GUNDARANIYA', 1, '2021-02-08 16:56:43'),
(263, 1, 'GUJARATI', 1, '2021-02-08 16:56:43'),
(264, 1, 'GELANI', 1, '2021-02-08 16:56:43'),
(265, 1, 'GEVARIYA', 1, '2021-02-08 16:56:43'),
(266, 1, 'GEDIYA', 1, '2021-02-08 16:56:43'),
(267, 1, 'GOTHADIYA', 1, '2021-02-08 16:56:43'),
(268, 1, 'GOTHDIYA', 1, '2021-02-08 16:56:43'),
(269, 1, 'PIROJIYA', 1, '2021-02-08 16:56:43'),
(270, 1, 'PINDORIYA', 1, '2021-02-08 16:56:43'),
(271, 1, 'PITHADIYA', 1, '2021-02-08 16:56:43'),
(272, 1, 'PEDHADIYA', 1, '2021-02-08 16:56:43'),
(273, 1, 'POKAR', 1, '2021-02-08 16:56:43'),
(274, 1, 'POLARA', 1, '2021-02-08 16:56:43'),
(275, 1, 'POKIYA', 1, '2021-02-08 16:56:43'),
(276, 1, 'POSHIYA', 1, '2021-02-08 16:56:43'),
(277, 1, 'FALDU', 1, '2021-02-08 16:56:43'),
(278, 1, 'FINVIYA', 1, '2021-02-08 16:56:43'),
(279, 1, 'FIDOLIYA', 1, '2021-02-08 16:56:44'),
(280, 1, 'BARVALIYA', 1, '2021-02-08 16:56:44'),
(281, 1, 'BALLAR', 1, '2021-02-08 16:56:44'),
(282, 1, 'BAGSARIYA', 1, '2021-02-08 16:56:44'),
(283, 1, 'BHADRESHIYA', 1, '2021-02-08 16:56:44'),
(284, 1, 'BABARIYA', 1, '2021-02-08 16:56:44'),
(285, 1, 'BALDHA', 1, '2021-02-08 16:56:44'),
(286, 1, 'BAMBHROLIYA', 1, '2021-02-08 16:56:44'),
(287, 1, 'BARASIYA', 1, '2021-02-08 16:56:44'),
(288, 1, 'BAREJIYA', 1, '2021-02-08 16:56:44'),
(289, 1, 'BAREVADIYA', 1, '2021-02-08 16:56:44'),
(290, 1, 'BAPODARIYA', 1, '2021-02-08 16:56:44'),
(291, 1, 'BAVISHA', 1, '2021-02-08 16:56:44'),
(292, 1, 'BASIDA', 1, '2021-02-08 16:56:44'),
(293, 1, 'BANDHNIYA', 1, '2021-02-08 16:56:44'),
(294, 1, 'BBAMBHNIYA', 1, '2021-02-08 16:56:44'),
(295, 1, 'BAJARIYA', 1, '2021-02-08 16:56:44'),
(296, 1, 'BIVISHI', 1, '2021-02-08 16:56:44'),
(297, 1, 'BAVALIYA', 1, '2021-02-08 16:56:44'),
(298, 1, 'BUTANI', 1, '2021-02-08 16:56:44'),
(299, 1, 'BUHA', 1, '2021-02-08 16:56:44'),
(300, 1, 'BELADIYA', 1, '2021-02-08 16:56:44'),
(301, 1, 'SUKHDIYA', 1, '2021-02-08 16:56:44'),
(302, 1, 'SUVAGIYA', 1, '2021-02-08 16:56:44'),
(303, 1, 'SOJITRA', 1, '2021-02-08 16:56:44'),
(304, 1, 'SONANI', 1, '2021-02-08 16:56:44'),
(305, 1, 'SORTHIYA', 1, '2021-02-08 16:56:44'),
(306, 1, 'SANGHANI', 1, '2021-02-08 16:56:45'),
(307, 1, 'HARSODA', 1, '2021-02-08 16:56:45'),
(308, 1, 'HAPANI', 1, '2021-02-08 16:56:45'),
(309, 1, 'HARKHANI', 1, '2021-02-08 16:56:45'),
(310, 1, 'HALAI', 1, '2021-02-08 16:56:45'),
(311, 1, 'HALA', 1, '2021-02-08 16:56:45'),
(312, 1, 'HAPALIYA', 1, '2021-02-08 16:56:45'),
(313, 1, 'HANSLIYA', 1, '2021-02-08 16:56:45'),
(314, 1, 'HINGLADIYA', 1, '2021-02-08 16:56:45'),
(315, 1, 'HIDAD', 1, '2021-02-08 16:56:45'),
(316, 1, 'HIRANI', 1, '2021-02-08 16:56:45'),
(317, 1, 'HIRPARA', 1, '2021-02-08 16:56:45'),
(318, 1, 'HIHORIYA', 1, '2021-02-08 16:56:45'),
(319, 1, 'AJANI', 1, '2021-02-08 16:56:45'),
(320, 1, 'AJUDIYA', 1, '2021-02-08 16:56:45'),
(321, 1, 'AKBARI', 1, '2021-02-08 16:56:45'),
(322, 1, 'AKVALIYA', 1, '2021-02-08 16:56:45'),
(323, 1, 'AMIN', 1, '2021-02-08 16:56:45'),
(324, 1, 'AMIPARA', 1, '2021-02-08 16:56:45'),
(325, 1, 'AMRELIYA', 1, '2021-02-08 16:56:45'),
(326, 1, 'ADALJA', 1, '2021-02-08 16:56:45'),
(327, 1, 'ANDANI', 1, '2021-02-08 16:56:45'),
(328, 1, 'ANDARIYA', 1, '2021-02-08 16:56:45'),
(329, 1, 'ABHAGI', 1, '2021-02-08 16:56:45'),
(330, 1, 'ANGHAD', 1, '2021-02-08 16:56:45'),
(331, 1, 'AMBALIYA', 1, '2021-02-08 16:56:45'),
(332, 1, 'AKOLIYA', 1, '2021-02-08 16:56:45'),
(333, 1, 'GODHANI', 1, '2021-02-08 16:56:45'),
(334, 1, 'GOLANI', 1, '2021-02-08 16:56:45'),
(335, 1, 'GONDALIYA', 1, '2021-02-08 16:56:45'),
(336, 1, 'GORASIYA', 1, '2021-02-08 16:56:45'),
(337, 1, 'GOYANI', 1, '2021-02-08 16:56:45'),
(338, 1, 'GOPANI', 1, '2021-02-08 16:56:45'),
(339, 1, 'GOTI', 1, '2021-02-08 16:56:45'),
(340, 1, 'GONDHAT', 1, '2021-02-08 16:56:45'),
(341, 1, 'GODANI', 1, '2021-02-08 16:56:45'),
(342, 1, 'GOLAKIYA', 1, '2021-02-08 16:56:45'),
(343, 1, 'GANGAJALIYA', 1, '2021-02-08 16:56:46'),
(344, 1, 'GHARDUSIYA', 1, '2021-02-08 16:56:46'),
(345, 1, 'GHADIYA', 1, '2021-02-08 16:56:46'),
(346, 1, 'DHIMELIYA', 1, '2021-02-08 16:56:46'),
(347, 1, 'GHELANI', 1, '2021-02-08 16:56:46'),
(348, 1, 'GHEVARIYA', 1, '2021-02-08 16:56:46'),
(349, 1, 'GHONIYA', 1, '2021-02-08 16:56:46'),
(350, 1, 'GHOGHARI', 1, '2021-02-08 16:56:46'),
(351, 1, 'GHORI', 1, '2021-02-08 16:56:46'),
(352, 1, 'BOGHANI', 1, '2021-02-08 16:56:46'),
(353, 1, 'BORSADIYA', 1, '2021-02-08 16:56:46'),
(354, 1, 'BOGHARA', 1, '2021-02-08 16:56:46'),
(355, 1, 'BORAD', 1, '2021-02-08 16:56:46'),
(356, 1, 'BORDA', 1, '2021-02-08 16:56:46'),
(357, 1, 'BODAR', 1, '2021-02-08 16:56:46'),
(358, 1, 'BHARODIYA', 1, '2021-02-08 16:56:46'),
(359, 1, 'BHANDERI', 1, '2021-02-08 16:56:46'),
(360, 1, 'BHAGAT', 1, '2021-02-08 16:56:46'),
(361, 1, 'BHADANI', 1, '2021-02-08 16:56:46'),
(362, 1, 'BHALARA', 1, '2021-02-08 16:56:47'),
(363, 1, 'BHAYANI', 1, '2021-02-08 16:56:47'),
(364, 1, 'BHALIYA', 1, '2021-02-08 16:56:47'),
(365, 1, 'BHAGIYA', 1, '2021-02-08 16:56:47'),
(366, 1, 'BHAKHAR', 1, '2021-02-08 16:56:47'),
(367, 1, 'BHIKADIYA', 1, '2021-02-08 16:56:47'),
(368, 1, 'BHIMANI', 1, '2021-02-08 16:56:47'),
(369, 1, 'BHINGRALIYA', 1, '2021-02-08 16:56:47'),
(370, 1, 'BHUT', 1, '2021-02-08 16:56:47'),
(371, 1, 'BHUVA', 1, '2021-02-08 16:56:47'),
(372, 1, 'BHUDIYA', 1, '2021-02-08 16:56:47'),
(373, 1, 'BHRAKOYA', 1, '2021-02-08 16:56:47'),
(374, 1, 'BHUNGLIYA', 1, '2021-02-08 16:56:47'),
(375, 1, 'BHESANIYA', 1, '2021-02-08 16:56:47'),
(376, 1, 'MADAT', 1, '2021-02-08 16:56:47'),
(377, 1, 'MADANI', 1, '2021-02-08 16:56:47'),
(378, 1, 'MAYANI', 1, '2021-02-08 16:56:47'),
(379, 1, 'MADARIYA', 1, '2021-02-08 16:56:47'),
(380, 1, 'MAKANI', 1, '2021-02-08 16:56:47'),
(381, 1, 'MADHANI', 1, '2021-02-08 16:56:47'),
(382, 1, 'MAHAPARIYA', 1, '2021-02-08 16:56:47'),
(383, 1, 'MALAVIYA', 1, '2021-02-08 16:56:47'),
(384, 1, 'AALGIYA', 1, '2021-02-08 16:56:47'),
(385, 1, 'AMBALIYA', 1, '2021-02-08 16:56:47'),
(386, 1, 'ATKOTIYA', 1, '2021-02-08 16:56:47'),
(387, 1, 'ASODARIYA', 1, '2021-02-08 16:56:47'),
(388, 1, 'ARDESHNA', 1, '2021-02-08 16:56:47'),
(389, 1, 'ANTALA', 1, '2021-02-08 16:56:47'),
(390, 1, 'ITALIYA', 1, '2021-02-08 16:56:47'),
(391, 1, 'UMRETIYA', 1, '2021-02-08 16:56:47'),
(392, 1, 'UNDHAD', 1, '2021-02-08 16:56:47'),
(393, 1, 'USDAL', 1, '2021-02-08 16:56:47'),
(394, 1, 'USDADIYA', 1, '2021-02-08 16:56:47'),
(395, 1, 'UJARIYA', 1, '2021-02-08 16:56:47'),
(396, 1, 'UKANI', 1, '2021-02-08 16:56:48'),
(397, 1, 'GHAVA', 1, '2021-02-08 16:56:48'),
(398, 1, 'CHAKLASIYA', 1, '2021-02-08 16:56:48'),
(399, 1, 'CHABHADIYA', 1, '2021-02-08 16:56:48'),
(400, 1, 'CHABHANI', 1, '2021-02-08 16:56:48'),
(401, 1, 'CHAVADIYA', 1, '2021-02-08 16:56:48'),
(402, 1, 'CHANGANI', 1, '2021-02-08 16:56:48'),
(403, 1, 'CHANCHAD', 1, '2021-02-08 16:56:48'),
(404, 1, 'CHANDPARA', 1, '2021-02-08 16:56:48'),
(405, 1, 'CHIKHALIYA', 1, '2021-02-08 16:56:48'),
(406, 1, 'CHITALIYA', 1, '2021-02-08 16:56:48'),
(407, 1, 'CHINDEDIYA', 1, '2021-02-08 16:56:48'),
(408, 1, 'CHUVALIYA', 1, '2021-02-08 16:56:48'),
(409, 1, 'CHOHALIYA', 1, '2021-02-08 16:56:48'),
(410, 1, 'CHOTHANI', 1, '2021-02-08 16:56:48'),
(411, 1, 'CHOVATIYA', 1, '2021-02-08 16:56:48'),
(412, 1, 'CHODVADIYA', 1, '2021-02-08 16:56:48'),
(413, 1, 'CHOPDA', 1, '2021-02-08 16:56:48'),
(414, 1, 'CHHAYANI', 1, '2021-02-08 16:56:48'),
(415, 1, 'CHHODVADIYA', 1, '2021-02-08 16:56:48'),
(416, 1, 'MARVIYA', 1, '2021-02-08 16:56:48'),
(417, 1, 'MATARIYA', 1, '2021-02-08 16:56:48'),
(418, 1, 'MANSARA', 1, '2021-02-08 16:56:48'),
(419, 1, 'MARKANA', 1, '2021-02-08 16:56:48'),
(420, 1, 'MATHUKIYA', 1, '2021-02-08 16:56:48'),
(421, 1, 'MAVANI', 1, '2021-02-08 16:56:48'),
(422, 1, 'MANDNAKA', 1, '2021-02-08 16:56:48'),
(423, 1, 'MANGROLIYA', 1, '2021-02-08 16:56:48'),
(424, 1, 'MANGROLA', 1, '2021-02-08 16:56:48'),
(425, 1, 'MANGUKIYA', 1, '2021-02-08 16:56:48'),
(426, 1, 'MIYANI', 1, '2021-02-08 16:56:48'),
(427, 1, 'MULANI', 1, '2021-02-08 16:56:49'),
(428, 1, 'MUNGRA', 1, '2021-02-08 16:56:49'),
(429, 1, 'MUNGLA', 1, '2021-02-08 16:56:49'),
(430, 1, 'MUNGALPARA', 1, '2021-02-08 16:56:49'),
(431, 1, 'MUKHI', 1, '2021-02-08 16:56:49'),
(432, 1, 'MUNJPARA', 1, '2021-02-08 16:56:49'),
(433, 1, 'MUNGPARA', 1, '2021-02-08 16:56:49'),
(434, 1, 'MENDPARA', 1, '2021-02-08 16:56:49'),
(435, 1, 'MEPANI', 1, '2021-02-08 16:56:49'),
(436, 1, 'MEGHANI', 1, '2021-02-08 16:56:49'),
(437, 1, 'MORJA', 1, '2021-02-08 16:56:49'),
(438, 1, 'MOLIYA', 1, '2021-02-08 16:56:49'),
(439, 1, 'MONPARA', 1, '2021-02-08 16:56:49'),
(440, 1, 'MONPARIYA', 1, '2021-02-08 16:56:49'),
(441, 1, 'MORAD', 1, '2021-02-08 16:56:49'),
(442, 1, 'MORDIYA', 1, '2021-02-08 16:56:49'),
(443, 1, 'MOVALIYA', 1, '2021-02-08 16:56:49'),
(444, 1, 'MONGHARIYA', 1, '2021-02-08 16:56:49'),
(445, 1, 'RAFALIYA', 1, '2021-02-08 16:56:49'),
(446, 1, 'RATANPARIYA', 1, '2021-02-08 16:56:49'),
(447, 1, 'SATANI', 1, '2021-02-08 16:56:49'),
(448, 2, 'AADROJA', 1, '2021-02-08 16:57:31'),
(449, 2, 'AAJOLIYA', 1, '2021-02-08 16:57:31'),
(450, 2, 'AAKHAJA', 1, '2021-02-08 16:57:31'),
(451, 2, 'AAKOLA', 1, '2021-02-08 16:57:31'),
(452, 2, 'AALODARIA', 1, '2021-02-08 16:57:31'),
(453, 2, 'AALODRA', 1, '2021-02-08 16:57:31'),
(454, 2, 'AALONDRA', 1, '2021-02-08 16:57:31'),
(455, 2, 'AAMBALIYA', 1, '2021-02-08 16:57:31'),
(456, 2, 'AANJA', 1, '2021-02-08 16:57:31'),
(457, 2, 'AANJOLIYA', 1, '2021-02-08 16:57:31'),
(458, 2, 'AANKELA', 1, '2021-02-08 16:57:31'),
(459, 2, 'AANKOLA', 1, '2021-02-08 16:57:31'),
(460, 2, 'AASARAVA', 1, '2021-02-08 16:57:31'),
(461, 2, 'ACHADIYA', 1, '2021-02-08 16:57:31'),
(462, 2, 'ADHADUK', 1, '2021-02-08 16:57:31'),
(463, 2, 'ADHERA', 1, '2021-02-08 16:57:31'),
(464, 2, 'ADODARIYA', 1, '2021-02-08 16:57:31'),
(465, 2, 'ADROJA', 1, '2021-02-08 16:57:31'),
(466, 2, 'AEDAPARA', 1, '2021-02-08 16:57:31'),
(467, 2, 'AEVIYA', 1, '2021-02-08 16:57:31'),
(468, 2, 'AGHARA', 1, '2021-02-08 16:57:31'),
(469, 2, 'AGHERA', 1, '2021-02-08 16:57:31'),
(470, 2, 'AGOLA', 1, '2021-02-08 16:57:31'),
(471, 2, 'AKAVLIYA', 1, '2021-02-08 16:57:31'),
(472, 2, 'AMBANI', 1, '2021-02-08 16:57:31'),
(473, 2, 'AMRUTIYA', 1, '2021-02-08 16:57:31'),
(474, 2, 'ANDARPA', 1, '2021-02-08 16:57:31'),
(475, 2, 'ANDIPARA', 1, '2021-02-08 16:57:31'),
(476, 2, 'AODHAVIUYA', 1, '2021-02-08 16:57:31'),
(477, 2, 'AODIYA', 1, '2021-02-08 16:57:31'),
(478, 2, 'ARAVADIYA', 1, '2021-02-08 16:57:32'),
(479, 2, 'ARDESHANA', 1, '2021-02-08 16:57:32'),
(480, 2, 'ARNIYA', 1, '2021-02-08 16:57:32'),
(481, 2, 'DABARIYA', 1, '2021-02-08 16:57:32'),
(482, 2, 'DADHANIA', 1, '2021-02-08 16:57:32'),
(483, 2, 'DADHASANA', 1, '2021-02-08 16:57:32'),
(484, 2, 'DALSANIYA', 1, '2021-02-08 16:57:32'),
(485, 2, 'DANGROSIYA', 1, '2021-02-08 16:57:32'),
(486, 2, 'DARA', 1, '2021-02-08 16:57:32'),
(487, 2, 'DARANIYA', 1, '2021-02-08 16:57:32'),
(488, 2, 'DASADIYA', 1, '2021-02-08 16:57:32'),
(489, 2, 'DAVADA', 1, '2021-02-08 16:57:32'),
(490, 2, 'DEDAKIYA', 1, '2021-02-08 16:57:32'),
(491, 2, 'DEDANIYA', 1, '2021-02-08 16:57:32'),
(492, 2, 'DEKIVADIYA', 1, '2021-02-08 16:57:32'),
(493, 2, 'DELVADIYA', 1, '2021-02-08 16:57:32'),
(494, 2, 'DEPANI', 1, '2021-02-08 16:57:32'),
(495, 2, 'DESAI', 1, '2021-02-08 16:57:32'),
(496, 2, 'DETHRIYA', 1, '2021-02-08 16:57:32'),
(497, 2, 'DETROJA', 1, '2021-02-08 16:57:32'),
(498, 2, 'DHADHANIYA', 1, '2021-02-08 16:57:32'),
(499, 2, 'DHAMAT', 1, '2021-02-08 16:57:32'),
(500, 2, 'DHANEJA', 1, '2021-02-08 16:57:32'),
(501, 2, 'DHARSANDIA', 1, '2021-02-08 16:57:32'),
(502, 2, 'DHINGANI', 1, '2021-02-08 16:57:32'),
(503, 2, 'DHOL', 1, '2021-02-08 16:57:32'),
(504, 2, 'DHORIYA', 1, '2021-02-08 16:57:32'),
(505, 2, 'DHORIYANI', 1, '2021-02-08 16:57:33'),
(506, 2, 'DHRASOTIYA', 1, '2021-02-08 16:57:33'),
(507, 2, 'DHUDASIYA', 1, '2021-02-08 16:57:33'),
(508, 2, 'DHUDIYA', 1, '2021-02-08 16:57:33'),
(509, 2, 'DHUJARIYA', 1, '2021-02-08 16:57:33'),
(510, 2, 'DHULASIYA', 1, '2021-02-08 16:57:33'),
(511, 2, 'DHULIA', 1, '2021-02-08 16:57:33'),
(512, 2, 'DHUMALIYA', 1, '2021-02-08 16:57:33'),
(513, 2, 'DHUSARIYA', 1, '2021-02-08 16:57:33'),
(514, 2, 'DINAN', 1, '2021-02-08 16:57:33'),
(515, 2, 'DOLIYA', 1, '2021-02-08 16:57:33'),
(516, 2, 'DUDANI', 1, '2021-02-08 16:57:33'),
(517, 2, 'DUDASIYA', 1, '2021-02-08 16:57:33'),
(518, 2, 'DUDIYA', 1, '2021-02-08 16:57:33'),
(519, 2, 'DULIYA', 1, '2021-02-08 16:57:33'),
(520, 2, 'BADODARIYA', 1, '2021-02-08 16:57:33'),
(521, 2, 'BAFLIPARA', 1, '2021-02-08 16:57:33'),
(522, 2, 'BAJADIYA', 1, '2021-02-08 16:57:33'),
(523, 2, 'BAKORI', 1, '2021-02-08 16:57:33'),
(524, 2, 'BAKORIYA', 1, '2021-02-08 16:57:33'),
(525, 2, 'BANGARIYA', 1, '2021-02-08 16:57:33'),
(526, 2, 'BANUGARIYA', 1, '2021-02-08 16:57:33'),
(527, 2, 'BAPODARIYA', 1, '2021-02-08 16:57:33'),
(528, 2, 'BARAIYA', 1, '2021-02-08 16:57:33'),
(529, 2, 'BARASARA', 1, '2021-02-08 16:57:33'),
(530, 2, 'BARASHIYA', 1, '2021-02-08 16:57:33'),
(531, 2, 'BARIYA', 1, '2021-02-08 16:57:34'),
(532, 2, 'BAROCHIYA', 1, '2021-02-08 16:57:34'),
(533, 2, 'BATHANI', 1, '2021-02-08 16:57:34'),
(534, 2, 'BAVARIA', 1, '2021-02-08 16:57:34'),
(535, 2, 'BAVARIYA', 1, '2021-02-08 16:57:34'),
(536, 2, 'BAVARVA', 1, '2021-02-08 16:57:34'),
(537, 2, 'BECHARA', 1, '2021-02-08 16:57:34'),
(538, 2, 'BECHRA', 1, '2021-02-08 16:57:34'),
(539, 2, 'BEDIYA', 1, '2021-02-08 16:57:34'),
(540, 2, 'BERA', 1, '2021-02-08 16:57:34'),
(541, 2, 'BHADANIYA', 1, '2021-02-08 16:57:34'),
(542, 2, 'BHADJA', 1, '2021-02-08 16:57:34'),
(543, 2, 'BHAGAI', 1, '2021-02-08 16:57:34'),
(544, 2, 'BHALANI', 1, '2021-02-08 16:57:34'),
(545, 2, 'BHALODI', 1, '2021-02-08 16:57:34'),
(546, 2, 'BHALODIYA', 1, '2021-02-08 16:57:34'),
(547, 2, 'BHANAPARA', 1, '2021-02-08 16:57:34'),
(548, 2, 'BHANAVADIYA', 1, '2021-02-08 16:57:34'),
(549, 2, 'BHANGANECHA', 1, '2021-02-08 16:57:34'),
(550, 2, 'BHANGNECHA', 1, '2021-02-08 16:57:34'),
(551, 2, 'BHANVADIYA', 1, '2021-02-08 16:57:34'),
(552, 2, 'BHARANI', 1, '2021-02-08 16:57:34'),
(553, 2, 'BHATASANA', 1, '2021-02-08 16:57:34'),
(554, 2, 'BHATAVADIYA', 1, '2021-02-08 16:57:34'),
(555, 2, 'BHATIYA', 1, '2021-02-08 16:57:34'),
(556, 2, 'BHATVADIA', 1, '2021-02-08 16:57:34'),
(557, 2, 'BHAVANAGARI', 1, '2021-02-08 16:57:35'),
(558, 2, 'BHAYANI', 1, '2021-02-08 16:57:35'),
(559, 2, 'BHDANIYA', 1, '2021-02-08 16:57:35'),
(560, 2, 'BHDASANA', 1, '2021-02-08 16:57:35'),
(561, 2, 'BHESDADIYA', 1, '2021-02-08 16:57:35'),
(562, 2, 'BHILA', 1, '2021-02-08 16:57:35'),
(563, 2, 'BHIMANI', 1, '2021-02-08 16:57:35'),
(564, 2, 'BHOJANI', 1, '2021-02-08 16:57:35'),
(565, 2, 'BHORANIYA', 1, '2021-02-08 16:57:35'),
(566, 2, 'BHUGANI', 1, '2021-02-08 16:57:35'),
(567, 2, 'BHUT', 1, '2021-02-08 16:57:35'),
(568, 2, 'BHUVA', 1, '2021-02-08 16:57:35'),
(569, 2, 'BHVANI', 1, '2021-02-08 16:57:35'),
(570, 2, 'BODA', 1, '2021-02-08 16:57:35'),
(571, 2, 'BODAR', 1, '2021-02-08 16:57:35'),
(572, 2, 'BOKARAVADIYA', 1, '2021-02-08 16:57:35'),
(573, 2, 'BOPALIYA', 1, '2021-02-08 16:57:35'),
(574, 2, 'BORASANIYA', 1, '2021-02-08 16:57:35'),
(575, 2, 'BORIGADHA', 1, '2021-02-08 16:57:35'),
(576, 2, 'BORSANIYA', 1, '2021-02-08 16:57:35'),
(577, 2, 'BUD', 1, '2021-02-08 16:57:35'),
(578, 2, 'BUDASANA', 1, '2021-02-08 16:57:35'),
(579, 2, 'BUTANI', 1, '2021-02-08 16:57:35'),
(580, 2, 'JAGANI', 1, '2021-02-08 16:57:35'),
(581, 2, 'JAGODARA', 1, '2021-02-08 16:57:36'),
(582, 2, 'JAKASANIYA', 1, '2021-02-08 16:57:36'),
(583, 2, 'JAKHNIYA', 1, '2021-02-08 16:57:36'),
(584, 2, 'JALARIYA', 1, '2021-02-08 16:57:36'),
(585, 2, 'JANJRUKIYA', 1, '2021-02-08 16:57:36'),
(586, 2, 'JARSANIYA', 1, '2021-02-08 16:57:36'),
(587, 2, 'JASANI', 1, '2021-02-08 16:57:36'),
(588, 2, 'JASAPARA', 1, '2021-02-08 16:57:36'),
(589, 2, 'JATAKIYA', 1, '2021-02-08 16:57:36'),
(590, 2, 'JATISANIYA', 1, '2021-02-08 16:57:36'),
(591, 2, 'JAVIA', 1, '2021-02-08 16:57:36'),
(592, 2, 'JAVIYA', 1, '2021-02-08 16:57:36'),
(593, 2, 'JEKIVANIYA', 1, '2021-02-08 16:57:36'),
(594, 2, 'JETANI', 1, '2021-02-08 16:57:36'),
(595, 2, 'JETHALOJA', 1, '2021-02-08 16:57:36'),
(596, 2, 'JETPARIYA', 1, '2021-02-08 16:57:36'),
(597, 2, 'JINJA', 1, '2021-02-08 16:57:36'),
(598, 2, 'JITHRA', 1, '2021-02-08 16:57:36'),
(599, 2, 'JIVANI', 1, '2021-02-08 16:57:36'),
(600, 2, 'JOTASANA', 1, '2021-02-08 16:57:36'),
(601, 2, 'JULASANA', 1, '2021-02-08 16:57:36'),
(602, 2, 'CHADANIYA', 1, '2021-02-08 16:57:36'),
(603, 2, 'CHADASANIYA', 1, '2021-02-08 16:57:36'),
(604, 2, 'CHADSANIYA', 1, '2021-02-08 16:57:36'),
(605, 2, 'CHANCHADIYA', 1, '2021-02-08 16:57:36'),
(606, 2, 'CHANDRAKALA', 1, '2021-02-08 16:57:36'),
(607, 2, 'CHANDRALA', 1, '2021-02-08 16:57:37'),
(608, 2, 'CHANDROLA', 1, '2021-02-08 16:57:37'),
(609, 2, 'CHANEPARA', 1, '2021-02-08 16:57:37'),
(610, 2, 'CHANGELA', 1, '2021-02-08 16:57:37'),
(611, 2, 'CHANIYARA', 1, '2021-02-08 16:57:37'),
(612, 2, 'CHANTASANIYA', 1, '2021-02-08 16:57:37'),
(613, 2, 'CHAPADIYA', 1, '2021-02-08 16:57:37'),
(614, 2, 'CHAPANI', 1, '2021-02-08 16:57:37'),
(615, 2, 'CHAPDIYA', 1, '2021-02-08 16:57:37'),
(616, 2, 'CHAPLA', 1, '2021-02-08 16:57:37'),
(617, 2, 'CHAROLA', 1, '2021-02-08 16:57:37'),
(618, 2, 'CHAROTRA', 1, '2021-02-08 16:57:37'),
(619, 2, 'CHHATROLA', 1, '2021-02-08 16:57:37'),
(620, 2, 'CHHTRALA', 1, '2021-02-08 16:57:37'),
(621, 2, 'CHIDARODA', 1, '2021-02-08 16:57:37'),
(622, 2, 'CHIKANI', 1, '2021-02-08 16:57:37'),
(623, 2, 'CHIKHALIYA', 1, '2021-02-08 16:57:37'),
(624, 2, 'CHODHARI', 1, '2021-02-08 16:57:37'),
(625, 2, 'CHOKHLIYA', 1, '2021-02-08 16:57:37'),
(626, 2, 'CHOPADA', 1, '2021-02-08 16:57:37'),
(627, 2, 'CHOVATIYA', 1, '2021-02-08 16:57:37'),
(628, 2, 'FADADU', 1, '2021-02-08 16:57:37'),
(629, 2, 'FALADADHIYA', 1, '2021-02-08 16:57:37'),
(630, 2, 'FALDU', 1, '2021-02-08 16:57:37'),
(631, 2, 'FEFAR', 1, '2021-02-08 16:57:37'),
(632, 2, 'FEKAR', 1, '2021-02-08 16:57:38'),
(633, 2, 'FINAVA', 1, '2021-02-08 16:57:38'),
(634, 2, 'FINAVICHA', 1, '2021-02-08 16:57:38'),
(635, 2, 'FULETRA', 1, '2021-02-08 16:57:38'),
(636, 2, 'FULTARIYA', 1, '2021-02-08 16:57:38'),
(637, 2, 'ESOTIYA', 1, '2021-02-08 16:57:38'),
(638, 2, 'ISHATIYA', 1, '2021-02-08 16:57:38'),
(639, 2, 'PABANI', 1, '2021-02-08 16:57:38'),
(640, 2, 'PADALIYA', 1, '2021-02-08 16:57:38'),
(641, 2, 'PADODARA', 1, '2021-02-08 16:57:38'),
(642, 2, 'PADSHUMBIYA', 1, '2021-02-08 16:57:38'),
(643, 2, 'PAGHADAR', 1, '2021-02-08 16:57:38'),
(644, 2, 'PAIJA', 1, '2021-02-08 16:57:38'),
(645, 2, 'PALANI', 1, '2021-02-08 16:57:38'),
(646, 2, 'PAN', 1, '2021-02-08 16:57:38'),
(647, 2, 'PANARA', 1, '2021-02-08 16:57:38'),
(648, 2, 'PANCHANI', 1, '2021-02-08 16:57:38'),
(649, 2, 'PANCHARA', 1, '2021-02-08 16:57:38'),
(650, 2, 'PANSARA', 1, '2021-02-08 16:57:38'),
(651, 2, 'PANTOTIYA', 1, '2021-02-08 16:57:38'),
(652, 2, 'PAR', 1, '2021-02-08 16:57:38'),
(653, 2, 'PARAJIYA', 1, '2021-02-08 16:57:38'),
(654, 2, 'PARECHHA', 1, '2021-02-08 16:57:38'),
(655, 2, 'PARESA', 1, '2021-02-08 16:57:38'),
(656, 2, 'PAREYA', 1, '2021-02-08 16:57:38'),
(657, 2, 'PARIKH', 1, '2021-02-08 16:57:38'),
(658, 2, 'PARIYA', 1, '2021-02-08 16:57:38'),
(659, 2, 'PARSANIYA', 1, '2021-02-08 16:57:38'),
(660, 2, 'PARVADIA', 1, '2021-02-08 16:57:38'),
(661, 2, 'PASAMIYA', 1, '2021-02-08 16:57:38'),
(662, 2, 'PATEL', 1, '2021-02-08 16:57:38'),
(663, 2, 'PATHRA', 1, '2021-02-08 16:57:38'),
(664, 2, 'PESIVADIYA', 1, '2021-02-08 16:57:38'),
(665, 2, 'PETHANI', 1, '2021-02-08 16:57:38'),
(666, 2, 'PETHAPARA', 1, '2021-02-08 16:57:38'),
(667, 2, 'POKAR', 1, '2021-02-08 16:57:38'),
(668, 2, 'PURANI', 1, '2021-02-08 16:57:39'),
(669, 2, 'VACHHANI', 1, '2021-02-08 16:57:39'),
(670, 2, 'VADAKIYA', 1, '2021-02-08 16:57:39'),
(671, 2, 'VADALIA', 1, '2021-02-08 16:57:39'),
(672, 2, 'VADARIYA', 1, '2021-02-08 16:57:39'),
(673, 2, 'VADAVIYA', 1, '2021-02-08 16:57:39'),
(674, 2, 'VADGASIYA', 1, '2021-02-08 16:57:39'),
(675, 2, 'VADHADIYA', 1, '2021-02-08 16:57:39'),
(676, 2, 'VADHAROKIYA', 1, '2021-02-08 16:57:39'),
(677, 2, 'VADOLIYA', 1, '2021-02-08 16:57:39'),
(678, 2, 'VADSOLA', 1, '2021-02-08 16:57:39'),
(679, 2, 'VADUKIYA', 1, '2021-02-08 16:57:39'),
(680, 2, 'VAGADIYA', 1, '2021-02-08 16:57:39'),
(681, 2, 'VAGHADIYA', 1, '2021-02-08 16:57:39'),
(682, 2, 'VAGHRIYA', 1, '2021-02-08 16:57:39'),
(683, 2, 'VAISHANI', 1, '2021-02-08 16:57:39'),
(684, 2, 'VAISHNANI', 1, '2021-02-08 16:57:39'),
(685, 2, 'VAKANERIYA', 1, '2021-02-08 16:57:39'),
(686, 2, 'VAMJA', 1, '2021-02-08 16:57:39'),
(687, 2, 'VANANI', 1, '2021-02-08 16:57:39'),
(688, 2, 'VANSAJALIYA', 1, '2021-02-08 16:57:39'),
(689, 2, 'VANSDADIYA', 1, '2021-02-08 16:57:39'),
(690, 2, 'VARASADA', 1, '2021-02-08 16:57:39'),
(691, 2, 'VARDIYA', 1, '2021-02-08 16:57:39'),
(692, 2, 'VARIYA', 1, '2021-02-08 16:57:39'),
(693, 2, 'VARMORA', 1, '2021-02-08 16:57:39'),
(694, 2, 'VARSADIYA', 1, '2021-02-08 16:57:39'),
(695, 2, 'VARSANIYA', 1, '2021-02-08 16:57:39'),
(696, 2, 'VASAMDA', 1, '2021-02-08 16:57:40'),
(697, 2, 'VASANIA', 1, '2021-02-08 16:57:40'),
(698, 2, 'VASDADIYA', 1, '2021-02-08 16:57:40'),
(699, 2, 'VASJANIYA', 1, '2021-02-08 16:57:40'),
(700, 2, 'VAVAIYA', 1, '2021-02-08 16:57:40'),
(701, 2, 'VEGAD', 1, '2021-02-08 16:57:40'),
(702, 2, 'VEGADA', 1, '2021-02-08 16:57:40'),
(703, 2, 'VEKARIYA', 1, '2021-02-08 16:57:40'),
(704, 2, 'VELANI', 1, '2021-02-08 16:57:40'),
(705, 2, 'VIDAJA', 1, '2021-02-08 16:57:40'),
(706, 2, 'VIDJA', 1, '2021-02-08 16:57:40'),
(707, 2, 'VIDLOJA', 1, '2021-02-08 16:57:40'),
(708, 2, 'VIJAYAT', 1, '2021-02-08 16:57:40'),
(709, 2, 'VIKANI', 1, '2021-02-08 16:57:40'),
(710, 2, 'VIRMGAMA', 1, '2021-02-08 16:57:40'),
(711, 2, 'VIROJA', 1, '2021-02-08 16:57:40'),
(712, 2, 'VIRPARA', 1, '2021-02-08 16:57:40'),
(713, 2, 'VIRPARIYA', 1, '2021-02-08 16:57:40'),
(714, 2, 'VIRPURA', 1, '2021-02-08 16:57:40'),
(715, 2, 'VISAPARA', 1, '2021-02-08 16:57:40'),
(716, 2, 'VISHRORIYA', 1, '2021-02-08 16:57:40'),
(717, 2, 'VITTHAPARA', 1, '2021-02-08 16:57:40'),
(718, 2, 'SABAPARA', 1, '2021-02-08 16:57:40'),
(719, 2, 'SADANIYA', 1, '2021-02-08 16:57:40'),
(720, 2, 'SADARIYA', 1, '2021-02-08 16:57:40'),
(721, 2, 'SADATIYA', 1, '2021-02-08 16:57:40'),
(722, 2, 'SADAVIYA', 1, '2021-02-08 16:57:40'),
(723, 2, 'SADHARKIYA', 1, '2021-02-08 16:57:40'),
(724, 2, 'SADRANI', 1, '2021-02-08 16:57:40'),
(725, 2, 'SAGAPARIYA', 1, '2021-02-08 16:57:40'),
(726, 2, 'SAGARVADIYA', 1, '2021-02-08 16:57:40'),
(727, 2, 'SAIJA', 1, '2021-02-08 16:57:41'),
(728, 2, 'SAKARIYA', 1, '2021-02-08 16:57:41'),
(729, 2, 'SAKARVADIYA', 1, '2021-02-08 16:57:41'),
(730, 2, 'SALAPARA', 1, '2021-02-08 16:57:41'),
(731, 2, 'SAMANI', 1, '2021-02-08 16:57:41'),
(732, 2, 'SANANDIYA', 1, '2021-02-08 16:57:41'),
(733, 2, 'SANARIYA', 1, '2021-02-08 16:57:41'),
(734, 2, 'SANAVAD', 1, '2021-02-08 16:57:41'),
(735, 2, 'SANDIYA', 1, '2021-02-08 16:57:41'),
(736, 2, 'SANEPARA', 1, '2021-02-08 16:57:41'),
(737, 2, 'SANGHANI', 1, '2021-02-08 16:57:41'),
(738, 2, 'SANIJA', 1, '2021-02-08 16:57:41'),
(739, 2, 'SANJA', 1, '2021-02-08 16:57:41'),
(740, 2, 'SANTOKI', 1, '2021-02-08 16:57:41'),
(741, 2, 'SAPOVADIYA', 1, '2021-02-08 16:57:41'),
(742, 2, 'SAPRIYA', 1, '2021-02-08 16:57:41'),
(743, 2, 'SARADVA', 1, '2021-02-08 16:57:41'),
(744, 2, 'SARANIYA', 1, '2021-02-08 16:57:41'),
(745, 2, 'SARKHEDI', 1, '2021-02-08 16:57:42'),
(746, 2, 'SARODIYA', 1, '2021-02-08 16:57:42'),
(747, 2, 'SARSAVADIYA', 1, '2021-02-08 16:57:42'),
(748, 2, 'SAVALIYA', 1, '2021-02-08 16:57:42'),
(749, 2, 'SAVANI', 1, '2021-02-08 16:57:42'),
(750, 2, 'SAVERA', 1, '2021-02-08 16:57:42'),
(751, 2, 'SAVRIYA', 1, '2021-02-08 16:57:42'),
(752, 2, 'SAVSANI', 1, '2021-02-08 16:57:42'),
(753, 2, 'SAYANI', 1, '2021-02-08 16:57:42'),
(754, 2, 'SEJANI', 1, '2021-02-08 16:57:42'),
(755, 2, 'SEKHALIYA', 1, '2021-02-08 16:57:42'),
(756, 2, 'SEKHAT', 1, '2021-02-08 16:57:42'),
(757, 2, 'SHERASIYA', 1, '2021-02-08 16:57:42'),
(758, 2, 'SHERATHIYA', 1, '2021-02-08 16:57:42'),
(759, 2, 'SHIHORA', 1, '2021-02-08 16:57:42'),
(760, 2, 'SHIJORA', 1, '2021-02-08 16:57:42'),
(761, 2, 'SHILOJA', 1, '2021-02-08 16:57:42'),
(762, 2, 'SHIRA', 1, '2021-02-08 16:57:42'),
(763, 2, 'SHIYAT', 1, '2021-02-08 16:57:42'),
(764, 2, 'SHOBHANA', 1, '2021-02-08 16:57:42'),
(765, 2, 'SHOBHASANA', 1, '2021-02-08 16:57:42'),
(766, 2, 'SHOLADHRA', 1, '2021-02-08 16:57:42'),
(767, 2, 'SHUREJIA', 1, '2021-02-08 16:57:42'),
(768, 2, 'SIDHDHIVALA', 1, '2021-02-08 16:57:42'),
(769, 2, 'SIHORA', 1, '2021-02-08 16:57:42'),
(770, 2, 'SINOJIA', 1, '2021-02-08 16:57:42'),
(771, 2, 'SIRAVI', 1, '2021-02-08 16:57:42'),
(772, 2, 'SISANGIYA', 1, '2021-02-08 16:57:42'),
(773, 2, 'SISOTIYA', 1, '2021-02-08 16:57:42'),
(774, 2, 'SITAPARA', 1, '2021-02-08 16:57:42'),
(775, 2, 'SOBHANI', 1, '2021-02-08 16:57:42'),
(776, 2, 'SODIYA', 1, '2021-02-08 16:57:42'),
(777, 2, 'SOLADHARA', 1, '2021-02-08 16:57:42'),
(778, 2, 'SOLIYA', 1, '2021-02-08 16:57:43'),
(779, 2, 'SUKHATIYA', 1, '2021-02-08 16:57:43'),
(780, 2, 'SURANI', 1, '2021-02-08 16:57:43'),
(781, 2, 'SUREJA', 1, '2021-02-08 16:57:43'),
(782, 2, 'SUTARIYA', 1, '2021-02-08 16:57:43'),
(783, 2, 'SUVARIYA', 1, '2021-02-08 16:57:43'),
(784, 2, 'TAJAPARA', 1, '2021-02-08 16:57:43'),
(785, 2, 'TALAVIYA', 1, '2021-02-08 16:57:43'),
(786, 2, 'TALVANIYA', 1, '2021-02-08 16:57:43'),
(787, 2, 'TENTIYA', 1, '2021-02-08 16:57:43'),
(788, 2, 'THORIYA', 1, '2021-02-08 16:57:43'),
(789, 2, 'TIKARIYA', 1, '2021-02-08 16:57:43'),
(790, 2, 'TIKIRIYA', 1, '2021-02-08 16:57:43'),
(791, 2, 'TILALA', 1, '2021-02-08 16:57:43'),
(792, 2, 'TILVA', 1, '2021-02-08 16:57:43'),
(793, 2, 'TITODIYA', 1, '2021-02-08 16:57:43'),
(794, 2, 'TOGADIYA', 1, '2021-02-08 16:57:43'),
(795, 2, 'TRAMADIYA', 1, '2021-02-08 16:57:43'),
(796, 2, 'TRAMBADIYA', 1, '2021-02-08 16:57:43'),
(797, 2, 'TRAPADIYA', 1, '2021-02-08 16:57:43'),
(798, 2, 'UBHADA', 1, '2021-02-08 16:57:43'),
(799, 2, 'UBHADIYA', 1, '2021-02-08 16:57:43'),
(800, 2, 'UGHREJA', 1, '2021-02-08 16:57:43'),
(801, 2, 'UKADIYA', 1, '2021-02-08 16:57:43'),
(802, 2, 'UKANI', 1, '2021-02-08 16:57:43'),
(803, 2, 'UNJIYA', 1, '2021-02-08 16:57:43'),
(804, 2, 'UTESHIYA', 1, '2021-02-08 16:57:43'),
(805, 2, 'ZALARIYA', 1, '2021-02-08 16:57:43'),
(806, 2, 'ZALAVADIYA', 1, '2021-02-08 16:57:43'),
(807, 2, 'ZANZRUKIYA', 1, '2021-02-08 16:57:43'),
(808, 2, 'ZATAKIYA', 1, '2021-02-08 16:57:43'),
(809, 2, 'ZULASANA', 1, '2021-02-08 16:57:43'),
(810, 2, 'ODHVIYA', 1, '2021-02-08 16:57:43'),
(811, 2, 'ODIYA', 1, '2021-02-08 16:57:43'),
(812, 2, 'OGANAJA', 1, '2021-02-08 16:57:44'),
(813, 2, 'RABADIYA', 1, '2021-02-08 16:57:44'),
(814, 2, 'RABARA', 1, '2021-02-08 16:57:44'),
(815, 2, 'RACHHDIYA', 1, '2021-02-08 16:57:44'),
(816, 2, 'RADHESIYA', 1, '2021-02-08 16:57:44'),
(817, 2, 'RADIYA', 1, '2021-02-08 16:57:44'),
(818, 2, 'RAIYANI', 1, '2021-02-08 16:57:44'),
(819, 2, 'RAJKOTIYA', 1, '2021-02-08 16:57:44'),
(820, 2, 'RAJODIYA', 1, '2021-02-08 16:57:44'),
(821, 2, 'RAJPARA', 1, '2021-02-08 16:57:44'),
(822, 2, 'RAKAL', 1, '2021-02-08 16:57:44'),
(823, 2, 'RAKJA', 1, '2021-02-08 16:57:44'),
(824, 2, 'RAMANI', 1, '2021-02-08 16:57:44'),
(825, 2, 'RAMOLIYA', 1, '2021-02-08 16:57:44'),
(826, 2, 'RANGANI', 1, '2021-02-08 16:57:44'),
(827, 2, 'RANGAPARIYA', 1, '2021-02-08 16:57:44'),
(828, 2, 'RANIPA', 1, '2021-02-08 16:57:44'),
(829, 2, 'RANKJA', 1, '2021-02-08 16:57:44'),
(830, 2, 'RANSARIYA', 1, '2021-02-08 16:57:44'),
(831, 2, 'RASADIYA', 1, '2021-02-08 16:57:44'),
(832, 2, 'RASAMIYA', 1, '2021-02-08 16:57:44'),
(833, 2, 'RATANPARA', 1, '2021-02-08 16:57:44'),
(834, 2, 'ROIYANA', 1, '2021-02-08 16:57:44'),
(835, 2, 'ROJIVADIYA', 1, '2021-02-08 16:57:44'),
(836, 2, 'ROKAD', 1, '2021-02-08 16:57:44'),
(837, 2, 'RUPALA', 1, '2021-02-08 16:57:44'),
(838, 2, 'KACHARIYA', 1, '2021-02-08 16:57:44'),
(839, 2, 'KACHROLA', 1, '2021-02-08 16:57:44'),
(840, 2, 'KADCHHI', 1, '2021-02-08 16:57:45'),
(841, 2, 'KADEVAR', 1, '2021-02-08 16:57:45'),
(842, 2, 'KADIVAR', 1, '2021-02-08 16:57:45'),
(843, 2, 'KADTHI', 1, '2021-02-08 16:57:45'),
(844, 2, 'KADVANI', 1, '2021-02-08 16:57:45'),
(845, 2, 'KAGTHARA', 1, '2021-02-08 16:57:45'),
(846, 2, 'KAILA', 1, '2021-02-08 16:57:45'),
(847, 2, 'KAKADIYA', 1, '2021-02-08 16:57:45'),
(848, 2, 'KAKANIYA', 1, '2021-02-08 16:57:45'),
(849, 2, 'KAKASANIYA', 1, '2021-02-08 16:57:45'),
(850, 2, 'KALARIYA', 1, '2021-02-08 16:57:45'),
(851, 2, 'KALAVADIYA', 1, '2021-02-08 16:57:45'),
(852, 2, 'KALOLA', 1, '2021-02-08 16:57:45'),
(853, 2, 'KAMANI', 1, '2021-02-08 16:57:45'),
(854, 2, 'KAMARIYA', 1, '2021-02-08 16:57:45'),
(855, 2, 'KANANI', 1, '2021-02-08 16:57:45'),
(856, 2, 'KANERIYA', 1, '2021-02-08 16:57:45'),
(857, 2, 'KANGRASIYA', 1, '2021-02-08 16:57:45'),
(858, 2, 'KANJIYA', 1, '2021-02-08 16:57:45'),
(859, 2, 'KANSAGARA', 1, '2021-02-08 16:57:45'),
(860, 2, 'KANTARIYA', 1, '2021-02-08 16:57:45'),
(861, 2, 'KANTESARIYA', 1, '2021-02-08 16:57:45'),
(862, 2, 'KARAVADIYA', 1, '2021-02-08 16:57:45'),
(863, 2, 'KARDANI', 1, '2021-02-08 16:57:45'),
(864, 2, 'KARETIYA', 1, '2021-02-08 16:57:45'),
(865, 2, 'KARSANIYA', 1, '2021-02-08 16:57:45'),
(866, 2, 'KASETIYA', 1, '2021-02-08 16:57:45'),
(867, 2, 'KASIPARA', 1, '2021-02-08 16:57:45'),
(868, 2, 'KASOTIYA', 1, '2021-02-08 16:57:45'),
(869, 2, 'KASUNDRA', 1, '2021-02-08 16:57:46'),
(870, 2, 'KATARIYA', 1, '2021-02-08 16:57:46'),
(871, 2, 'KATHROTIYA', 1, '2021-02-08 16:57:46'),
(872, 2, 'KAVADIYA', 1, '2021-02-08 16:57:46'),
(873, 2, 'KAVAR', 1, '2021-02-08 16:57:46'),
(874, 2, 'KAVAT', 1, '2021-02-08 16:57:46'),
(875, 2, 'KAVATHIYA', 1, '2021-02-08 16:57:46'),
(876, 2, 'KHACHAR', 1, '2021-02-08 16:57:46'),
(877, 2, 'KHADUVALA', 1, '2021-02-08 16:57:46'),
(878, 2, 'KHANPARA', 1, '2021-02-08 16:57:46'),
(879, 2, 'KHANT', 1, '2021-02-08 16:57:46'),
(880, 2, 'KHARED', 1, '2021-02-08 16:57:46'),
(881, 2, 'KHAVADIYA', 1, '2021-02-08 16:57:46'),
(882, 2, 'KHIRASARIYA', 1, '2021-02-08 16:57:46'),
(883, 2, 'KHOKHAL', 1, '2021-02-08 16:57:46'),
(884, 2, 'KIKANI', 1, '2021-02-08 16:57:46'),
(885, 2, 'KOARAVDIYA', 1, '2021-02-08 16:57:46'),
(886, 2, 'KORADIYA', 1, '2021-02-08 16:57:46'),
(887, 2, 'KORINGA', 1, '2021-02-08 16:57:46'),
(888, 2, 'KOTADIYA', 1, '2021-02-08 16:57:46'),
(889, 2, 'KOTHADIYA', 1, '2021-02-08 16:57:46'),
(890, 2, 'KOTHIYA', 1, '2021-02-08 16:57:46'),
(891, 2, 'KUDALIYA', 1, '2021-02-08 16:57:46'),
(892, 2, 'KUDANI', 1, '2021-02-08 16:57:46'),
(893, 2, 'KUDARI', 1, '2021-02-08 16:57:46'),
(894, 2, 'KUDARIYA', 1, '2021-02-08 16:57:46'),
(895, 2, 'KUKADIA', 1, '2021-02-08 16:57:46'),
(896, 2, 'KUNDALIYA', 1, '2021-02-08 16:57:46'),
(897, 2, 'KUNDARIYA', 1, '2021-02-08 16:57:46'),
(898, 2, 'KVADIYA', 1, '2021-02-08 16:57:46'),
(899, 2, 'MADVIYA', 1, '2021-02-08 16:57:46'),
(900, 2, 'MAKADIYA', 1, '2021-02-08 16:57:46'),
(901, 2, 'MAKASANA', 1, '2021-02-08 16:57:46'),
(902, 2, 'MAKATI', 1, '2021-02-08 16:57:46'),
(903, 2, 'MAKHANASA', 1, '2021-02-08 16:57:47'),
(904, 2, 'MAKHASANA', 1, '2021-02-08 16:57:47'),
(905, 2, 'MAKHESANA', 1, '2021-02-08 16:57:47'),
(906, 2, 'MAKHSANA', 1, '2021-02-08 16:57:47'),
(907, 2, 'MAKWANA', 1, '2021-02-08 16:57:47'),
(908, 2, 'MALASNA', 1, '2021-02-08 16:57:47'),
(909, 2, 'MALLI', 1, '2021-02-08 16:57:47'),
(910, 2, 'MALVANIYA', 1, '2021-02-08 16:57:47'),
(911, 2, 'MANAVADARIYA', 1, '2021-02-08 16:57:47'),
(912, 2, 'MANDAVIYA', 1, '2021-02-08 16:57:47'),
(913, 2, 'MANJARIYA', 1, '2021-02-08 16:57:47'),
(914, 2, 'MANUSURIYA', 1, '2021-02-08 16:57:47'),
(915, 2, 'MANVAR', 1, '2021-02-08 16:57:47'),
(916, 2, 'MARADIYA', 1, '2021-02-08 16:57:47'),
(917, 2, 'MARSONIYA', 1, '2021-02-08 16:57:47'),
(918, 2, 'MARVADIYA', 1, '2021-02-08 16:57:47'),
(919, 2, 'MARVANIYA', 1, '2021-02-08 16:57:47'),
(920, 2, 'MASAT', 1, '2021-02-08 16:57:47'),
(921, 2, 'MATALIYA', 1, '2021-02-08 16:57:47'),
(922, 2, 'MATARIYA', 1, '2021-02-08 16:57:47'),
(923, 2, 'MATHASODIYA', 1, '2021-02-08 16:57:47'),
(924, 2, 'MATHOLIYA', 1, '2021-02-08 16:57:47'),
(925, 2, 'MEGHPARA', 1, '2021-02-08 16:57:47'),
(926, 2, 'MENDAPARA', 1, '2021-02-08 16:57:47'),
(927, 2, 'MENPARA', 1, '2021-02-08 16:57:47'),
(928, 2, 'MERJA', 1, '2021-02-08 16:57:47'),
(929, 2, 'METALIYA', 1, '2021-02-08 16:57:47'),
(930, 2, 'METHANIYA', 1, '2021-02-08 16:57:47'),
(931, 2, 'MEVA', 1, '2021-02-08 16:57:47'),
(932, 2, 'MINIPARA', 1, '2021-02-08 16:57:48'),
(933, 2, 'MINJROLA', 1, '2021-02-08 16:57:48'),
(934, 2, 'MODIYA', 1, '2021-02-08 16:57:48'),
(935, 2, 'MOKANI', 1, '2021-02-08 16:57:48'),
(936, 2, 'MORDIYA', 1, '2021-02-08 16:57:48'),
(937, 2, 'MORI', 1, '2021-02-08 16:57:48'),
(938, 2, 'MORJARIYA', 1, '2021-02-08 16:57:48'),
(939, 2, 'MOTAKO', 1, '2021-02-08 16:57:48'),
(940, 2, 'MOTERIYA', 1, '2021-02-08 16:57:48'),
(941, 2, 'MOTKA', 1, '2021-02-08 16:57:48'),
(942, 2, 'MUNDADIYA', 1, '2021-02-08 16:57:48'),
(943, 2, 'MUNJAPARA', 1, '2021-02-08 16:57:48'),
(944, 2, 'GABARI', 1, '2021-02-08 16:57:48'),
(945, 2, 'GADARA', 1, '2021-02-08 16:57:48'),
(946, 2, 'GADHIA', 1, '2021-02-08 16:57:48'),
(947, 2, 'GALANI', 1, '2021-02-08 16:57:48'),
(948, 2, 'GAMBHA', 1, '2021-02-08 16:57:48'),
(949, 2, 'GAMBHAVA', 1, '2021-02-08 16:57:48'),
(950, 2, 'GAMI', 1, '2021-02-08 16:57:48'),
(951, 2, 'GANGADIYA', 1, '2021-02-08 16:57:48'),
(952, 2, 'GARALA', 1, '2021-02-08 16:57:48'),
(953, 2, 'GARDHARIYA', 1, '2021-02-08 16:57:48'),
(954, 2, 'GARMOR', 1, '2021-02-08 16:57:48'),
(955, 2, 'GATHIYA', 1, '2021-02-08 16:57:48'),
(956, 2, 'GHAGHRA', 1, '2021-02-08 16:57:48'),
(957, 2, 'GHATODIYA', 1, '2021-02-08 16:57:48'),
(958, 2, 'GHAVNA', 1, '2021-02-08 16:57:48'),
(959, 2, 'GHETIYA', 1, '2021-02-08 16:57:48'),
(960, 2, 'GHODASARA', 1, '2021-02-08 16:57:48'),
(961, 2, 'GHUJARIA', 1, '2021-02-08 16:57:48'),
(962, 2, 'GHUMALIYA', 1, '2021-02-08 16:57:48'),
(963, 2, 'GIRAGHARIYA', 1, '2021-02-08 16:57:49'),
(964, 2, 'GODHANI', 1, '2021-02-08 16:57:49'),
(965, 2, 'GODHASARA', 1, '2021-02-08 16:57:49'),
(966, 2, 'GODHATRA', 1, '2021-02-08 16:57:49'),
(967, 2, 'GODHAVIYA', 1, '2021-02-08 16:57:49'),
(968, 2, 'GODVANI', 1, '2021-02-08 16:57:49'),
(969, 2, 'GOJARIYA', 1, '2021-02-08 16:57:49'),
(970, 2, 'GOL', 1, '2021-02-08 16:57:49'),
(971, 2, 'GOLANI', 1, '2021-02-08 16:57:49'),
(972, 2, 'GONDHA', 1, '2021-02-08 16:57:49'),
(973, 2, 'GOPANI', 1, '2021-02-08 16:57:49'),
(974, 2, 'GOR', 1, '2021-02-08 16:57:49'),
(975, 2, 'GORIYA', 1, '2021-02-08 16:57:49'),
(976, 2, 'GOTHI', 1, '2021-02-08 16:57:49'),
(977, 2, 'GOVANI', 1, '2021-02-08 16:57:49'),
(978, 2, 'GUDLIYA', 1, '2021-02-08 16:57:49'),
(979, 2, 'GUNJARIYA', 1, '2021-02-08 16:57:49'),
(980, 2, 'HADVANI', 1, '2021-02-08 16:57:49'),
(981, 2, 'HAJIPARA', 1, '2021-02-08 16:57:49'),
(982, 2, 'HALPARA', 1, '2021-02-08 16:57:49'),
(983, 2, 'HALVADIYA', 1, '2021-02-08 16:57:49'),
(984, 2, 'HANSHALIYA', 1, '2021-02-08 16:57:49'),
(985, 2, 'HANSHALPARA', 1, '2021-02-08 16:57:49'),
(986, 2, 'HANSHLIYA', 1, '2021-02-08 16:57:49'),
(987, 2, 'HANSOTIYA', 1, '2021-02-08 16:57:49'),
(988, 2, 'HARANIYA', 1, '2021-02-08 16:57:49'),
(989, 2, 'HARKHEDI', 1, '2021-02-08 16:57:49'),
(990, 2, 'HATANIYA', 1, '2021-02-08 16:57:49'),
(991, 2, 'HEDAPARA', 1, '2021-02-08 16:57:49'),
(992, 2, 'HEDPARA', 1, '2021-02-08 16:57:49'),
(993, 2, 'HINGRAJIYA', 1, '2021-02-08 16:57:49'),
(994, 2, 'HINSHU', 1, '2021-02-08 16:57:49'),
(995, 2, 'HINSU', 1, '2021-02-08 16:57:49'),
(996, 2, 'HIRANI', 1, '2021-02-08 16:57:49'),
(997, 2, 'HIRPARA', 1, '2021-02-08 16:57:50'),
(998, 2, 'HOTHI', 1, '2021-02-08 16:57:50'),
(999, 2, 'HUDKA', 1, '2021-02-08 16:57:50'),
(1000, 2, 'HULANI', 1, '2021-02-08 16:57:50'),
(1001, 2, 'LABKAMNA', 1, '2021-02-08 16:57:50'),
(1002, 2, 'LADANI', 1, '2021-02-08 16:57:50'),
(1003, 2, 'LADOLA', 1, '2021-02-08 16:57:50'),
(1004, 2, 'LADPARA', 1, '2021-02-08 16:57:50'),
(1005, 2, 'LAGNECHA', 1, '2021-02-08 16:57:50'),
(1006, 2, 'LAKHANI', 1, '2021-02-08 16:57:50'),
(1007, 2, 'LAKHDHRIYA', 1, '2021-02-08 16:57:50'),
(1008, 2, 'LAKHTARIYA', 1, '2021-02-08 16:57:50'),
(1009, 2, 'LALAKIYA', 1, '2021-02-08 16:57:50'),
(1010, 2, 'LALANI', 1, '2021-02-08 16:57:50'),
(1011, 2, 'LAMBHIYA', 1, '2021-02-08 16:57:50'),
(1012, 2, 'LANDHNOJA', 1, '2021-02-08 16:57:50'),
(1013, 2, 'LANGNECHA', 1, '2021-02-08 16:57:50'),
(1014, 2, 'LIKHIYA', 1, '2021-02-08 16:57:50'),
(1015, 2, 'LIMBHANI', 1, '2021-02-08 16:57:50'),
(1016, 2, 'LODARIYA', 1, '2021-02-08 16:57:50'),
(1017, 2, 'LOHIYA', 1, '2021-02-08 16:57:50'),
(1018, 2, 'LORIYA', 1, '2021-02-08 16:57:50'),
(1019, 2, 'NADAPARA', 1, '2021-02-08 16:57:50'),
(1020, 2, 'NAGPARA', 1, '2021-02-08 16:57:50'),
(1021, 2, 'NAGRIYA', 1, '2021-02-08 16:57:50'),
(1022, 2, 'NANANIYA', 1, '2021-02-08 16:57:50'),
(1023, 2, 'NANDASANA', 1, '2021-02-08 16:57:50'),
(1024, 2, 'NAR', 1, '2021-02-08 16:57:51'),
(1025, 2, 'NARIYA', 1, '2021-02-08 16:57:51'),
(1026, 2, 'NARIYANI', 1, '2021-02-08 16:57:51'),
(1027, 2, 'NARODIYA', 1, '2021-02-08 16:57:51'),
(1028, 2, 'NAVAPARIYA', 1, '2021-02-08 16:57:51'),
(1029, 2, 'NAYAKPARA', 1, '2021-02-08 16:57:51'),
(1030, 2, 'NESHADIYA', 1, '2021-02-08 16:57:51'),
(1031, 2, 'NINDROLA', 1, '2021-02-08 16:57:51'),
(1032, 3, 'test', -1, '2021-02-08 19:30:07'),
(1033, 1, 'અન્ય', 1, '2021-02-12 11:18:42'),
(1034, 2, 'અન્ય', 1, '2021-02-12 11:18:42'),
(1035, 3, 'અન્ય', 1, '2021-02-12 11:18:51'),
(1036, 0, 'undefined', 1, '2021-02-25 07:12:27'),
(1037, 0, 'undefined', 1, '2021-02-25 07:13:30'),
(1038, 1, 'KHOKHARIYA', 1, '2021-03-11 17:14:53'),
(1039, 1, 'પટેલ', 1, '2021-03-12 17:17:52'),
(1040, 2, 'PATEL', 1, '2021-03-12 18:27:51'),
(1041, 3, 'કડવાપટેલ', 1, '2021-03-14 14:28:16'),
(1042, 1, 'Padhara', 1, '2021-03-15 02:10:54'),
(1043, 3, 'ASLALIYA', 1, '2021-03-15 17:09:10'),
(1044, 2, 'Patel', 1, '2021-03-22 08:20:40'),
(1045, 1, 'Harpal', 1, '2021-03-25 18:45:40'),
(1046, 1, 'Jodhani', 1, '2021-04-05 10:22:57'),
(1047, 3, 'Raval', 1, '2021-05-29 15:05:55'),
(1048, 1, 'ફાચરા', 1, '2021-06-13 07:08:08'),
(1049, 1, 'Vavadiya', 1, '2021-06-14 08:34:21'),
(1050, 3, 'Patel', 1, '2021-06-16 06:51:08'),
(1051, 2, 'Patel', 1, '2021-06-16 18:09:03'),
(1052, 1, 'KUMBHANI', 1, '2021-06-17 04:52:04'),
(1053, 3, 'Shah', 1, '2021-06-23 07:53:16');

-- --------------------------------------------------------

--
-- Table structure for table `taluko_master`
--

CREATE TABLE `taluko_master` (
  `id` int(11) NOT NULL,
  `dist_id` int(11) NOT NULL,
  `name` varchar(500) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `taluko_master`
--

INSERT INTO `taluko_master` (`id`, `dist_id`, `name`, `status`, `date`) VALUES
(1, 1, 'Babra', 1, '2020-12-23 15:31:21'),
(2, 2, 'Jasdan', 1, '2021-01-07 15:28:06'),
(3, 2, 'Gondal', 1, '2021-01-07 15:28:12'),
(4, 2, 'Viciya', 1, '2021-01-07 15:28:26'),
(5, 1, 'Lathi', 1, '2021-01-07 07:28:20'),
(6, 1, 'Amreli', 1, '2021-01-07 07:28:43'),
(7, 2, 'Ko.shangani', 1, '2021-01-07 07:29:12'),
(8, 2, 'Jetpur', 1, '2021-01-07 07:29:27'),
(9, 2, 'Dhoraji', 1, '2021-01-07 07:30:20'),
(10, 28, 'Viramgam', 1, '2021-01-07 08:05:52'),
(11, 28, 'Sanand', 1, '2021-01-07 08:06:12'),
(12, 28, 'Mandal', 1, '2021-01-07 08:06:35'),
(13, 28, 'Dholera', 1, '2021-01-07 08:07:00'),
(14, 28, 'Dholka', 1, '2021-01-07 08:07:34'),
(15, 28, 'Dhandhuka', 1, '2021-01-07 08:07:58'),
(16, 28, 'Detroj-Rampura', 1, '2021-01-07 08:08:21'),
(17, 28, 'Daskroi', 1, '2021-01-07 08:08:38'),
(18, 28, 'Bavla', 1, '2021-01-07 08:08:57'),
(19, 28, 'Ahemdabad City', 1, '2021-01-07 08:10:07'),
(20, 21, 'Amreli', 1, '2021-01-08 00:50:58'),
(21, 21, 'Babra	', 1, '2021-01-08 00:51:16'),
(22, 21, 'Bagasara', 1, '2021-01-08 00:51:39'),
(23, 21, 'Dhari', 1, '2021-01-08 00:52:05'),
(24, 21, 'Jafrabad	', 1, '2021-01-08 00:52:32'),
(25, 21, 'Khambha	 	', 1, '2021-01-08 00:52:59'),
(26, 21, 'Kunkavav ', 1, '2021-01-08 00:53:45'),
(27, 21, 'Vadia	 ', 1, '2021-01-08 00:54:10'),
(28, 21, 'Lathi', 1, '2021-01-08 00:54:37'),
(29, 21, 'Lilia	', 1, '2021-01-08 00:54:56'),
(30, 21, 'Rajula ', 1, '2021-01-08 00:55:14'),
(31, 21, 'Savar Kundla', 1, '2021-01-08 00:55:28'),
(32, 34, 'Anand	', 1, '2021-01-08 01:01:43'),
(33, 34, 'Anklav	', 1, '2021-01-08 01:02:00'),
(34, 34, 'Borsad	', 1, '2021-01-08 01:02:27'),
(35, 34, 'Khambhat', 1, '2021-01-08 01:02:56'),
(36, 34, 'Petlad	', 1, '2021-01-08 01:03:17'),
(37, 34, 'Sojitra	', 1, '2021-01-08 01:03:40'),
(38, 34, 'Tarapur	', 1, '2021-01-08 01:03:58'),
(39, 34, 'Umreth', 1, '2021-01-08 01:04:12'),
(40, 36, 'અન્ય', 1, '2021-01-08 01:20:23'),
(41, 25, 'Amirgadh', 1, '2021-01-08 02:18:24'),
(42, 25, 'Danta', 1, '2021-01-08 02:19:03'),
(43, 25, 'Bhabhar	', 1, '2021-01-08 02:19:30'),
(44, 25, 'Dantiwada', 1, '2021-01-08 02:19:54'),
(45, 25, 'Deesa', 1, '2021-01-08 02:20:21'),
(46, 25, 'Deodar', 1, '2021-01-08 02:20:47'),
(47, 25, 'Dhanera', 1, '2021-01-08 02:21:10'),
(48, 25, 'Vav', 1, '2021-01-08 02:21:26'),
(49, 25, 'Vadgam	', 1, '2021-01-08 02:21:43'),
(50, 25, 'Tharad	', 1, '2021-01-08 02:22:00'),
(51, 25, 'Palanpur', 1, '2021-01-08 02:22:18'),
(52, 25, 'Kankrej	', 1, '2021-01-08 02:22:40'),
(53, 8, 'Valia', 1, '2021-01-08 02:24:58'),
(54, 8, 'Vagra', 1, '2021-01-08 02:25:14'),
(55, 8, 'Jhagadia', 1, '2021-01-08 02:25:35'),
(56, 8, 'Hansot', 1, '2021-01-08 02:26:00'),
(57, 8, 'Jambusar	 ', 1, '2021-01-08 02:26:27'),
(58, 8, 'Bharuch', 1, '2021-01-08 02:30:02'),
(59, 8, 'Anklesvar', 1, '2021-01-08 02:30:27'),
(60, 8, 'Amod', 1, '2021-01-08 02:30:46'),
(61, 20, 'Vallabhipur', 1, '2021-01-08 02:32:42'),
(62, 20, 'Umrala		', 1, '2021-01-08 02:33:02'),
(63, 20, 'Talaja', 1, '2021-01-08 02:33:19'),
(64, 20, 'Sihor', 1, '2021-01-08 02:33:34'),
(65, 20, 'Palitana	', 1, '2021-01-08 02:34:03'),
(66, 20, 'Mahuva	', 1, '2021-01-08 02:34:22'),
(67, 20, 'Ghogha', 1, '2021-01-08 02:34:41'),
(68, 20, 'Gariadhar	', 1, '2021-01-08 02:34:57'),
(69, 20, 'Jeshar', 1, '2021-01-08 03:31:19'),
(70, 20, 'Bhavnagar', 1, '2021-01-08 02:35:39'),
(71, 37, 'Botad', 1, '2021-01-08 02:36:08'),
(72, 33, 'Limkheda', 1, '2021-01-08 02:37:12'),
(73, 33, 'Jhalod	', 1, '2021-01-08 02:37:32'),
(74, 33, 'Garbada  ', 1, '2021-01-08 02:37:46'),
(75, 33, 'Fatepura', 1, '2021-01-08 02:38:04'),
(76, 33, 'Dohad', 1, '2021-01-08 02:38:20'),
(77, 33, 'Dhanpur	', 1, '2021-01-08 02:38:40'),
(78, 33, 'Devgadbaria	', 1, '2021-01-08 02:38:51'),
(79, 15, 'Okhamandal', 1, '2021-01-08 02:40:32'),
(80, 15, 'Lalpur	', 1, '2021-01-08 02:40:48'),
(81, 15, 'Khambhalia	', 1, '2021-01-08 02:41:05'),
(82, 15, 'Kalyanpur	', 1, '2021-01-08 02:41:21'),
(83, 15, 'Kalavad	', 1, '2021-01-08 02:45:57'),
(84, 15, 'Jodiya	', 1, '2021-01-08 02:46:13'),
(85, 15, 'Jamnagar	', 1, '2021-01-08 02:46:37'),
(86, 15, 'Jamjodhpur	', 1, '2021-01-08 02:46:52'),
(87, 15, 'Dhrol', 1, '2021-01-08 02:47:06'),
(88, 15, 'Bhanvad', 1, '2021-01-08 02:47:21'),
(89, 14, 'Visavadar', 1, '2021-01-08 02:48:20'),
(90, 14, 'Vanthali		', 1, '2021-01-08 02:48:40'),
(91, 14, 'Una		', 1, '2021-01-08 02:49:00'),
(92, 14, 'Talala	', 1, '2021-01-08 02:49:31'),
(93, 14, 'Sutrapada		', 1, '2021-01-08 02:50:07'),
(94, 14, 'Patan-Veraval	', 1, '2021-01-08 02:50:51'),
(95, 14, 'Mendarda	', 1, '2021-01-08 02:51:15'),
(96, 14, 'Mangrol	', 1, '2021-01-08 02:51:36'),
(97, 14, 'Manavadar	', 1, '2021-01-08 02:51:58'),
(98, 14, 'Malia	', 1, '2021-01-08 02:52:25'),
(99, 14, 'Kodinar', 1, '2021-01-08 02:52:53'),
(100, 14, 'Keshod	', 1, '2021-01-08 02:53:16'),
(101, 14, 'Junagadh	', 1, '2021-01-08 02:53:37'),
(102, 14, 'Bhesan	', 1, '2021-01-08 02:53:52'),
(103, 27, 'Dehgam	', 1, '2021-01-08 02:54:37'),
(104, 27, 'Mansa', 1, '2021-01-08 02:54:46'),
(105, 27, 'Kalol', 1, '2021-01-08 02:55:00'),
(106, 27, 'Gandhinagar', 1, '2021-01-08 02:55:15'),
(107, 10, 'Rapar', 1, '2021-01-08 02:56:07'),
(108, 10, 'Nakhatrana		', 1, '2021-01-08 02:56:23'),
(109, 10, 'Mundra	', 1, '2021-01-08 02:56:38'),
(110, 10, 'Mandvi	', 1, '2021-01-08 02:56:54'),
(111, 10, 'Lakhpat	', 1, '2021-01-08 02:57:24'),
(112, 10, 'Gandhidham	', 1, '2021-01-08 02:57:43'),
(113, 10, 'Bhuj	', 1, '2021-01-08 02:57:56'),
(114, 10, 'Bhachau	', 1, '2021-01-08 02:58:09'),
(115, 10, 'Anjar', 1, '2021-01-08 02:59:33'),
(116, 10, 'Abdasa	', 1, '2021-01-08 02:59:47'),
(117, 31, 'Virpur', 1, '2021-01-08 03:00:51'),
(118, 31, 'Thasra	', 1, '2021-01-08 03:01:07'),
(119, 31, 'Nadiad		', 1, '2021-01-08 03:01:23'),
(120, 31, 'Mehmedabad		', 1, '2021-01-08 03:01:39'),
(121, 31, 'Matar	', 1, '2021-01-08 03:01:55'),
(122, 31, 'Mahudha		', 1, '2021-01-08 03:02:15'),
(123, 31, 'Kheda		', 1, '2021-01-08 03:02:30'),
(124, 31, 'Kathlal		', 1, '2021-01-08 03:02:51'),
(125, 31, 'Kapadvanj		', 1, '2021-01-08 03:03:09'),
(126, 31, 'Balasinor		', 1, '2021-01-08 03:03:22'),
(127, 24, 'Visnagar', 1, '2021-01-08 03:04:15'),
(128, 24, 'Vijapur	', 1, '2021-01-08 03:04:30'),
(129, 24, 'Vadnagar	  ', 1, '2021-01-08 03:04:48'),
(130, 24, 'Unjha	', 1, '2021-01-08 03:05:06'),
(131, 24, 'Satlasana	 ', 1, '2021-01-08 03:05:22'),
(132, 24, 'Mahesana	', 1, '2021-01-08 03:05:37'),
(133, 24, 'Kheralu	', 1, '2021-01-08 03:05:54'),
(134, 24, 'Kadi	', 1, '2021-01-08 03:06:12'),
(135, 24, 'Becharaji', 1, '2021-01-08 03:06:29'),
(136, 6, 'Tilakwada', 1, '2021-01-08 03:08:18'),
(137, 6, 'Sagbara	', 1, '2021-01-08 03:08:31'),
(138, 6, 'Nandod	', 1, '2021-01-08 03:08:42'),
(139, 6, 'Dediapada', 1, '2021-01-08 03:08:52'),
(140, 37, 'Gadhada', 1, '2021-01-08 03:27:57'),
(141, 37, 'Barvala', 1, '2021-01-08 03:28:09'),
(142, 37, 'Ranpur', 1, '2021-01-08 03:28:22'),
(143, 5, 'Navsari', 1, '2021-01-08 03:35:47'),
(144, 5, 'Jalalpore	', 1, '2021-01-08 03:35:59'),
(145, 5, 'Gandevi	', 1, '2021-01-08 03:36:09'),
(146, 5, 'Chikhli		', 1, '2021-01-08 03:36:22'),
(147, 5, 'Bansda		', 1, '2021-01-08 03:36:32'),
(148, 29, 'Shehera', 1, '2021-01-08 03:38:15'),
(149, 29, 'Santrampur	', 1, '2021-01-08 03:38:33'),
(150, 29, 'Morwa (Hadaf)		', 1, '2021-01-08 03:38:52'),
(151, 29, 'Lunawada	', 1, '2021-01-08 03:39:10'),
(152, 29, 'Khanpur		', 1, '2021-01-08 03:39:27'),
(153, 29, 'Kalol		', 1, '2021-01-08 03:39:43'),
(154, 29, 'Kadana		', 1, '2021-01-08 03:39:59'),
(155, 29, 'Kadana	', 1, '2021-01-08 03:40:15'),
(156, 29, 'Jambughoda		', 1, '2021-01-08 03:40:36'),
(157, 29, 'Halol		', 1, '2021-01-08 03:40:54'),
(158, 29, 'Halol	1', 1, '2021-01-08 03:42:20'),
(159, 29, 'Godhra		', 1, '2021-01-08 03:41:25'),
(160, 29, 'Ghoghamba		', 1, '2021-01-08 03:41:46'),
(161, 23, 'Sidhpur', 1, '2021-01-08 03:43:11'),
(162, 23, 'Santalpur		', 1, '2021-01-08 03:43:25'),
(163, 23, 'Sami		', 1, '2021-01-08 03:43:39'),
(164, 23, 'Radhanpur	', 1, '2021-01-08 03:43:51'),
(165, 23, 'Patan		', 1, '2021-01-08 03:44:07'),
(166, 23, 'Harij	', 1, '2021-01-08 03:44:20'),
(167, 23, 'Chanasma		', 1, '2021-01-08 03:44:44'),
(168, 12, 'Kutiyana		', 1, '2021-01-08 03:45:29'),
(169, 12, 'Porbandar', 1, '2021-01-08 03:45:43'),
(170, 12, 'Ranavav', 1, '2021-01-08 03:45:54'),
(171, 2, 'Wankaner 1', 1, '2021-01-08 04:02:42'),
(172, 2, 'Upleta		', 1, '2021-01-08 03:47:03'),
(173, 2, 'Tankara		1', 1, '2021-01-08 04:02:13'),
(174, 2, 'Paddhari', 1, '2021-01-08 03:47:51'),
(175, 2, 'Rajkot', 1, '2021-01-08 03:48:03'),
(176, 2, 'Morvi	1', 1, '2021-01-08 04:02:02'),
(177, 2, 'Maliya	1', 1, '2021-01-08 04:01:52'),
(178, 2, 'Lodhika		', 1, '2021-01-08 03:49:10'),
(179, 2, 'Jamkandorna	', 1, '2021-01-08 03:50:08'),
(180, 22, 'Vijaynagar', 1, '2021-01-08 03:51:25'),
(181, 22, 'Vadali		', 1, '2021-01-08 03:51:43'),
(182, 22, 'Prantij		', 1, '2021-01-08 03:52:01'),
(183, 22, 'Talod		', 1, '2021-01-08 03:52:19'),
(184, 22, 'Modasa		', 1, '2021-01-08 03:52:37'),
(185, 22, 'Meghraj	', 1, '2021-01-08 03:52:55'),
(186, 22, 'Malpur		', 1, '2021-01-08 03:53:17'),
(187, 22, 'Khedbrahma		', 1, '2021-01-08 03:53:32'),
(188, 22, 'Idar		', 1, '2021-01-08 03:53:48'),
(189, 22, 'Himatnagar	', 1, '2021-01-08 03:54:05'),
(190, 22, 'Dhansura			', 1, '2021-01-08 03:54:29'),
(191, 22, 'Bhiloda		', 1, '2021-01-08 03:54:51'),
(192, 22, 'Bayad	', 1, '2021-01-08 03:55:13'),
(193, 13, 'Maliya', 1, '2021-01-08 03:56:05'),
(194, 13, 'Morbi', 1, '2021-01-08 03:56:14'),
(195, 13, 'Tankara', 1, '2021-01-08 03:56:22'),
(196, 13, 'Wakaner', 1, '2021-01-08 03:56:35'),
(197, 13, 'Halvad', 1, '2021-01-08 03:56:47'),
(198, 9, 'Umarpada', 1, '2021-01-08 03:58:09'),
(199, 9, 'Palsana		', 1, '2021-01-08 03:58:23'),
(200, 9, 'Olpad		', 1, '2021-01-08 03:58:38'),
(201, 9, 'Mangrol		', 1, '2021-01-08 03:58:52'),
(202, 9, 'Mandvi		', 1, '2021-01-08 03:59:06'),
(203, 9, 'Mahuva		', 1, '2021-01-08 03:59:20'),
(204, 9, 'Kamrej		', 1, '2021-01-08 03:59:32'),
(205, 9, 'Chorasi		', 1, '2021-01-08 03:59:48'),
(206, 9, 'Bardoli	', 1, '2021-01-08 04:00:02'),
(207, 9, 'surat	', 1, '2021-01-08 04:00:12'),
(208, 11, 'Wadhwan', 1, '2021-01-08 04:03:34'),
(209, 11, 'Sayla	', 1, '2021-01-08 04:03:52'),
(210, 11, 'Muli		', 1, '2021-01-08 04:04:08'),
(211, 11, 'Limbdi		', 1, '2021-01-08 04:04:22'),
(212, 11, 'Lakhtar		', 1, '2021-01-08 04:04:38'),
(213, 11, 'Dhrangadhra		', 1, '2021-01-08 04:04:55'),
(214, 11, 'Dasada		', 1, '2021-01-08 04:05:13'),
(215, 11, 'Chuda	 ', 1, '2021-01-08 04:05:27'),
(216, 11, 'Chotila		', 1, '2021-01-08 04:05:49'),
(217, 11, 'surendranagar    ', 1, '2021-01-08 04:06:01'),
(218, 4, 'Vyara', 1, '2021-01-08 04:06:45'),
(219, 4, 'Valod		', 1, '2021-01-08 04:06:55'),
(220, 4, 'Uchchhal		', 1, '2021-01-08 04:07:06'),
(221, 4, 'Songadh		', 1, '2021-01-08 04:07:18'),
(222, 4, 'Nizar		', 1, '2021-01-08 04:07:34'),
(223, 35, 'Vaghodia', 1, '2021-01-08 04:08:22'),
(224, 35, 'Vadodara		', 1, '2021-01-08 04:08:36'),
(225, 35, 'Sinor	', 1, '2021-01-08 04:08:50'),
(226, 35, 'Savli	', 1, '2021-01-08 04:09:05'),
(227, 35, 'Sankheda	', 1, '2021-01-08 04:09:21'),
(228, 35, 'Padra		', 1, '2021-01-08 04:09:38'),
(229, 35, 'Nasvadi', 1, '2021-01-08 04:09:54'),
(230, 35, 'Kavant		', 1, '2021-01-08 04:10:12'),
(231, 35, 'Karjan		', 1, '2021-01-08 04:10:32'),
(232, 35, 'Jetpur Pavi		', 1, '2021-01-08 04:10:54'),
(233, 35, 'Dabhoi		', 1, '2021-01-08 04:11:10'),
(234, 35, 'Chhota Udaipur	', 1, '2021-01-08 04:11:23'),
(235, 32, 'Naswadi		', 1, '2021-01-08 04:13:03'),
(236, 32, 'Chota Udaipur', 1, '2021-01-08 04:13:35'),
(237, 32, 'Jetpur pavi', 1, '2021-01-08 04:13:54'),
(238, 32, 'Kawant', 1, '2021-01-08 04:14:09'),
(239, 32, 'Sankheda', 1, '2021-01-08 04:14:24'),
(240, 32, 'Bodeli', 1, '2021-01-08 04:14:34'),
(241, 3, 'Valsad', 1, '2021-01-08 04:15:58'),
(242, 3, 'Umbergaon		', 1, '2021-01-08 04:16:10'),
(243, 3, 'Pardi	', 1, '2021-01-08 04:16:21'),
(244, 3, 'Kaprada		', 1, '2021-01-08 04:16:31'),
(245, 3, 'Dharampur		', 1, '2021-01-08 04:16:40'),
(246, 30, 'Khanpur', 1, '2021-01-08 04:21:23'),
(247, 30, 'Santrampur ', 1, '2021-01-08 04:21:34'),
(248, 30, 'Virpur ', 1, '2021-01-08 04:21:52'),
(249, 30, 'Kadana ', 1, '2021-01-08 04:22:04'),
(250, 30, 'Lunavada ', 1, '2021-01-08 04:22:17'),
(251, 30, 'Balasinor ', 1, '2021-01-08 04:22:26'),
(252, 26, 'Modasha', 1, '2021-01-08 04:28:29'),
(253, 26, 'Malpur', 1, '2021-01-08 04:28:39'),
(254, 26, 'Dhasura', 1, '2021-01-08 04:28:53'),
(255, 26, 'Meghraj', 1, '2021-01-08 04:29:06'),
(256, 26, 'Bhiloda', 1, '2021-01-08 04:29:27'),
(257, 26, 'Bayad', 1, '2021-01-08 04:29:46'),
(258, 18, 'Dwarka', 1, '2021-01-08 04:31:24'),
(259, 18, 'Bhanvad', 1, '2021-01-08 04:31:31'),
(260, 18, 'Kalyanpur', 1, '2021-01-08 04:31:41'),
(261, 18, 'Jam Khambhaliya', 1, '2021-01-08 04:32:03'),
(262, 16, 'Somnath', 1, '2021-01-08 04:32:39'),
(263, 16, 'Veraval', 1, '2021-01-08 04:32:45'),
(264, 16, 'Delwada', 1, '2021-01-08 04:33:05'),
(265, 16, 'Veraval chowpati', 1, '2021-01-08 04:33:32'),
(266, 16, 'Salaam gir', 1, '2021-01-08 04:33:44'),
(267, 7, 'Subir', 1, '2021-01-08 04:36:01'),
(268, 7, 'Waghai', 1, '2021-01-08 04:36:15'),
(269, 7, 'Ahwa', 1, '2021-01-08 04:36:26'),
(270, 16, 'Talala gir', 1, '2021-01-16 11:42:25'),
(271, 39, 'Maharashtra', 1, '2021-03-11 11:51:09'),
(272, 38, 'Rajasthan', 1, '2021-03-11 11:51:23');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `uid` int(11) NOT NULL,
  `member_id` varchar(100) NOT NULL,
  `samaj_id` int(11) NOT NULL,
  `surname_id` int(11) NOT NULL,
  `middle_name` varchar(150) NOT NULL,
  `father_name` varchar(150) NOT NULL,
  `mobile` varchar(15) DEFAULT NULL,
  `mobile_1` varchar(15) NOT NULL,
  `email` varchar(150) NOT NULL,
  `password` varchar(32) CHARACTER SET latin7 COLLATE latin7_general_cs DEFAULT NULL,
  `ad_password` varchar(100) NOT NULL,
  `dob` date NOT NULL,
  `profile_pic` varchar(500) NOT NULL,
  `blood_group` varchar(10) NOT NULL,
  `business` int(11) NOT NULL,
  `business_address` longtext NOT NULL,
  `business_2` int(11) NOT NULL,
  `business_address_2` varchar(1000) NOT NULL,
  `business_3` int(11) NOT NULL,
  `business_address_3` varchar(1000) NOT NULL,
  `gov_job` varchar(1000) NOT NULL,
  `qualification` varchar(1000) NOT NULL,
  `karkidi_id` int(11) NOT NULL,
  `address` varchar(500) NOT NULL,
  `village_id` int(11) NOT NULL,
  `taluko_id` int(11) NOT NULL,
  `dist_id` int(11) NOT NULL,
  `hundi_amount` double NOT NULL,
  `fund_amount` double NOT NULL,
  `is_mobile` enum('yes','no') NOT NULL DEFAULT 'yes',
  `gender` int(1) NOT NULL DEFAULT '1',
  `is_Token` varchar(100) NOT NULL,
  `notificationToken` varchar(100) NOT NULL,
  `verify_by` int(11) NOT NULL,
  `app_access` enum('yes','no') NOT NULL DEFAULT 'yes',
  `status` int(1) DEFAULT '0',
  `type` enum('management','admin','user','city_admin') DEFAULT 'user',
  `date` datetime DEFAULT CURRENT_TIMESTAMP,
  `lastlogin` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`uid`, `member_id`, `samaj_id`, `surname_id`, `middle_name`, `father_name`, `mobile`, `mobile_1`, `email`, `password`, `ad_password`, `dob`, `profile_pic`, `blood_group`, `business`, `business_address`, `business_2`, `business_address_2`, `business_3`, `business_address_3`, `gov_job`, `qualification`, `karkidi_id`, `address`, `village_id`, `taluko_id`, `dist_id`, `hundi_amount`, `fund_amount`, `is_mobile`, `gender`, `is_Token`, `notificationToken`, `verify_by`, `app_access`, `status`, `type`, `date`, `lastlogin`) VALUES
(1, 'PSB1', 1, 447, 'Jayanit', 'Vithalbhai', '8128500518', '', '', '12345', '12345', '0000-00-00', '20437f03c7ac57f5bd35808df504217cdata.png', 'O+', 43, '', 0, '', 0, '', '', 'MCA', 0, 'Dharai', 0, 21, 21, 0, 0, 'yes', 1, '16245103039491825007', '', 0, 'yes', 2, 'management', '2021-02-08 17:02:09', '2021-06-29 03:53:32'),
(2, 'PSB2', 1, 317, 'Kamleshbhai', 'Mithabhai', '9974010036', '', 'kmhirpara@gmail.com', '12345', '12345', '1975-03-20', '152dacf0a3a1aaa43c719e6474223244data.png', 'B+', 11, '', 0, '', 0, '', '', 'B.com', 0, ' \'Shanti Nivash\'2 Hirpara Nagar, Chitliya road, Jasdan ', 56, 2, 2, 0, 5100, 'yes', 1, '16247227228053439221', '', 0, 'yes', 2, 'management', '2021-02-12 12:48:15', '2021-06-28 18:43:59'),
(8, 'PSB3', 1, 317, 'JAYDEEP', 'RAJESHBHAI', '9726704405', '', 'jaydeeprhirapara@gmail.com', '12345', '12345', '1998-02-24', 'bdb228f01736b625179630dd3d8dedebdata.png', 'O+', 11, 'S.P.S.Sankul, Opp.Bhumi Ginning, Atkot - Jasdan Road, Atkot.360040', 0, '', 0, '', '', '', 0, 'Hirapara Nagar - 4, Chitaliya Road, Jasdan.360050', 56, 2, 2, 0, 0, 'yes', 1, '16221128254017936046', '', 0, 'yes', 2, 'city_admin', '2021-03-06 08:43:32', '2021-05-27 06:53:45'),
(9, 'PSB4', 1, 317, 'Keval', 'Kamleshbhai', '7984339406', '', '', NULL, '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, 'Jasdan', 56, 2, 2, 0, 0, 'yes', 1, '', '', 0, 'yes', 2, 'user', '2021-03-06 08:45:52', ''),
(10, 'PSB5', 3, 0, 'પાટીદાર શૈક્ષણિક ભવન', '', '9725138799', '', '', '12345', '12345', '0000-00-00', 'e80e16de6d0dde74326b8ed7d2316835n1611314.png', '', 56, 'પાટીદાર શૈક્ષણિક ભવન -1\nઆટકોટ રોડ. જસદણ ', 0, '', 0, '', '', '', 0, 'પાણી નાં ટાંકા પાસે, આટકોટ રોડ,જસદણ.', 0, 2, 2, 0, 125705, 'yes', 1, '16227373412672030593', '', 0, 'yes', 2, 'admin', '2021-03-06 11:22:54', '2021-06-03 12:22:21'),
(11, 'PSB6', 1, 447, 'Ghanshyambhai ', 'Amrutbhai', '9909074544', '', '', '12345', '', '1990-09-12', '0bbadf969a3f91e45793aeb193bab92bdata.png', '', 34, 'Varniraj hotel \nRajkot  Bhavnagar haive \nNear gondal chokdi\nAtkot', 34, '', 11, 'Shree m d kahor arts commers  collage  \nBypass cirkal  jasdan', '', '', 0, 'Soliter  society  \r\nBlock no.143\r\nJasdan\r\n', 56, 2, 2, 0, 5100, 'yes', 1, '16150540464514392604', '', 0, 'yes', 2, 'user', '2021-03-06 13:06:05', '2021-03-06 13:07:26'),
(14, 'PSB7', 1, 286, 'Dinesh Bhai', 'B', '9913580980', '', '', '12345', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, 'H', 20, 2, 2, 0, 999600, 'yes', 1, '16154715944908553880', '', 0, 'yes', 2, 'user', '2021-03-11 09:04:40', '2021-03-11 09:06:34'),
(15, 'PSB8', 1, 329, 'R', 'R', '9429431781', '', '', '98320', '', '2021-03-03', '', '', 4, 'F', 0, '', 0, '', '', 'D', 0, 'F', 0, 55, 8, 0, 0, 'yes', 1, '16154717563985490583', '', 0, 'yes', 3, 'user', '2021-03-11 09:09:17', '2021-03-11 09:09:29'),
(16, 'PSB9', 1, 414, 'Jay', 'Pravinbhai', '9638638814', '', 'Jaychhayani369@gmail.com', '79816', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', 'B. Sc. It. ', 0, 'Chitaliya road jasdan\n', 0, 2, 2, 0, 0, 'yes', 1, '16154757467630334124', '', 0, 'yes', 2, 'user', '2021-03-11 10:15:47', '2021-03-11 10:16:01'),
(17, 'PSB10', 1, 40, 'Dipen', 'Vinodbhai', '9725429829', '', 'Dipenthumar1188@yahoo.com', '12345', '', '1987-06-08', '78d8f1fe1ae43f177675bc661b7efaebdata.png', 'B+', 33, 'Ramani general hospital', 34, 'Hari amrut soda soap &AMUL PARLOUR', 52, 'Black sloon ambli hotel near new bus station road jasdan', '', 'M.com', 0, 'Chitaliya kuva road', 0, 2, 2, 0, 0, 'yes', 1, '16155467115955794782', '', 0, 'yes', 2, 'user', '2021-03-11 10:16:01', '2021-03-12 05:58:31'),
(18, 'PSB11', 1, 317, 'Keval', 'Kamlesh bhai', '9978710035', '', 'hiraparakeval1427@gmail.com', '12345', '', '2000-09-22', '', '', 11, 'Atkot-jasdan highway\n\nAtkot', 0, '', 0, '', '', 'B.com', 2, 'Hirpara Nagar 2 chitaliya road jasdan', 0, 2, 2, 0, 0, 'yes', 1, '16154770696477549253', '', 0, 'yes', 2, 'user', '2021-03-11 10:25:14', '2021-03-11 10:37:49'),
(19, 'PSB12', 1, 317, 'Bipin', 'Jivrajbhai', '9824221402', '', '', '36193', '', '0000-00-00', '', 'B+', 14, '', 0, '', 0, '', '', '', 0, 'Gangabhuvan', 0, 2, 2, 0, 0, 'yes', 1, '16154764107029046706', '', 0, 'yes', 2, 'user', '2021-03-11 10:26:51', '2021-03-11 10:27:10'),
(20, 'PSB13', 1, 241, 'Dineshbhai', 'Mohanabhai', '9725437614', '', 'Sidhaparadinesh@gmil.com', '97002', '', '0000-00-00', '', 'O+', 30, 'Jangavvd', 56, 'Jagavad sarpanch ', 0, '', '', '5', 0, 'Khanpar rod ', 0, 2, 2, 0, 1100, 'yes', 1, '16154766307130402566', '', 0, 'yes', 2, 'user', '2021-03-11 10:30:30', '2021-03-11 10:30:57'),
(21, 'PSB14', 2, 895, 'dipakbhai', 'govindbhai', '8980217383', '', 'dipakkukadiya@gmail.com', '87374', '', '1993-04-19', '', 'O+', 30, 'jasdan', 0, '', 0, '', '', 'bsc it', 0, 'polet vistar', 0, 2, 2, 0, 0, 'yes', 1, '16154775015543701463', '', 0, 'yes', 2, 'user', '2021-03-11 10:45:01', '2021-03-11 10:45:16'),
(22, 'PSB15', 1, 419, 'લલીતભાઈ', 'ઉકાભાઈ', '9978917734', '', 'marakana.lalit@gmail.com ', '68318', '', '1982-02-05', '', 'B+', 51, 'ઈશ્વરીયા, જસદણ, રાજકોટ, ગુજરાત ૩૬૦૦૪૦.', 30, 'ઈશ્વરીયા, જસદણ, રાજકોટ, ગુજરાત ૩૬૦૦૪૦.', 38, '', '', 'ઈલેક્ટ્રીક એન્જીનીયર (ડિપ્લોમા) ,બીએ એસ.વાય.', 11, 'At ishwariya, Khodiyar krupa, near primary school,Tal jasdan, Dist Rajkot, via Atkot, Gujarat 360040.', 0, 2, 2, 0, 1100, 'yes', 1, '16154776566270108992', '', 0, 'yes', 2, 'user', '2021-03-11 10:47:36', '2021-03-11 10:48:14'),
(24, 'PSB17', 1, 108, 'Dharmesh', 'Maganbhai', '9723655731', '', 'Dharmeshpatel688@gmail.com', '12345', '', '0000-00-00', '417e79f73c15405ee84185b76175efeedata.png', 'B-', 33, '', 0, '', 0, '', '', 'B.com', 0, 'Anandnagar samat rod jasdan', 56, 2, 2, 0, 0, 'yes', 1, '16198860409677191416', '', 0, 'yes', 2, 'user', '2021-03-11 10:51:30', '2021-05-01 12:20:41'),
(25, 'PSB18', 1, 414, 'Jitendra', 'Vallabhbhai', '9925611251', '', '', '52068', '', '0000-00-00', '', '', 16, 'Jayram plot near sardar school', 0, '', 0, '', '', '10 pass', 0, 'Jay ram plot near sardar school sardar chowk', 0, 2, 2, 0, 0, 'yes', 1, '16154784578454238138', '', 0, 'yes', 2, 'user', '2021-03-11 11:00:58', '2021-03-11 11:01:17'),
(26, 'PSB19', 1, 357, 'Chandu bhai', 'Ransodbhai', '9714059393', '', '', '12345', '', '0000-00-00', '', 'B+', 18, '', 30, 'Jasdan vishya rod jasdan', 0, '', '', '12', 0, 'Chandu bhai bodar\nJivapar. Ta.jasdan .Rajkot', 0, 2, 2, 0, 0, 'yes', 1, '16247944827010199699', '', 0, 'yes', 2, 'user', '2021-03-11 11:01:55', '2021-06-27 07:48:02'),
(27, 'PSB20', 1, 317, 'Manish', 'Dhirubhai', '9574513100', '', 'manishhirapra3537@gmail.com', '51466', '', '0000-00-00', '', '', 5, 'Chitlia kuva road sardar ', 5, 'Chitlia kuva road sardar chok', 0, '', '', '12', 0, 'Atkot road assar petrol pump niear vadla vadi', 0, 2, 2, 0, 0, 'yes', 1, '16154785162070122469', '', 0, 'yes', 2, 'user', '2021-03-11 11:01:56', '2021-03-11 11:02:12'),
(28, 'PSB21', 1, 354, 'Prakashbhai', 'Natubhai', '9714998525', '', '', '12345', '', '1987-07-31', '', 'B+', 23, 'Jasdan in to all gujarat', 0, '', 0, '', '', 'B.com', 0, 'Chhitaliya Road Ranchod Nagar', 56, 2, 2, 0, 6300, 'yes', 1, '16235122699991693654', '', 0, 'yes', 2, 'user', '2021-03-11 11:02:27', '2021-06-12 11:37:49'),
(30, 'PSB23', 1, 414, 'પ્રવીણ', 'ઉકાભાઇ', '9879320912', '', '', '12345', '', '1979-07-16', '', 'B+', 30, 'જસદણ', 30, 'જસદણ', 0, '', '', '6', 0, 'ચિતલીયા રોડ લક્ષ્મણ નગર જસદણ', 0, 2, 2, 0, 0, 'yes', 1, '16162276204285915722', '', 0, 'yes', 2, 'user', '2021-03-11 11:15:31', '2021-03-20 04:07:00'),
(32, 'PSB25', 1, 317, 'Chandragupt ', 'Vallabh Bhia ', '9887929283', '', 'Gopihandicraft@yahoo ', '25379', '', '0000-00-00', '', '', 41, '', 41, '', 0, '', '', '9', 0, '46 panchwati colony Sanganer jaipur \r\nRajasthan 302029', 0, 272, 38, 0, 0, 'yes', 1, '16154797751442523450', '', 0, 'yes', 2, 'user', '2021-03-11 11:22:57', '2021-03-11 11:23:10'),
(34, 'PSB27', 1, 114, 'Dhirajlal', 'Savjibhai', '9898932132', '', 'Togadiya ', '17188', '', '1970-06-01', '', '', 0, '', 0, '', 0, '', '', '', 0, 'Ambika Nagar b/h court atkot road ', 0, 2, 2, 0, 0, 'yes', 1, '16154805997071930468', '', 0, 'yes', 2, 'user', '2021-03-11 11:36:39', '2021-03-11 11:36:59'),
(35, 'PSB28', 1, 40, 'R', 'R', '8141694838', '', '', '49278', '', '2000-08-21', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 0, 0, 0, 0, 0, 'yes', 1, '16154806639256046759', '', 0, 'yes', 2, 'user', '2021-03-11 11:37:44', '2021-03-11 11:38:04'),
(36, 'PSB29', 1, 35, 'Dharmesh', 'Vinubhai', '8469986186', '', '', '20528', '', '0000-00-00', '0c32aa149396f79df8dce380009e839adata.png', '', 30, '', 36, '', 0, 'Atkot', '', 'B.b.a', 0, 'Chora vali seri', 0, 2, 2, 0, 0, 'yes', 1, '16154808422546573859', '', 0, 'yes', 2, 'user', '2021-03-11 11:40:42', '2021-03-11 11:41:01'),
(37, 'PSB30', 1, 64, 'Ghanshyam', 'Bachubhai', '9408663400', '', '', '12345', '', '0000-00-00', '', '', 0, '', 0, '', 30, '', '', '', 0, 'Gam', 0, 2, 2, 0, 0, 'yes', 1, '16165824112872454316', '', 0, 'yes', 2, 'user', '2021-03-11 11:41:33', '2021-03-24 06:40:11'),
(38, 'PSB31', 1, 389, 'Pravin', 'Nagajibhai', '8320843603', '', 'Pravinantala666@gmail.com', '40847', '', '0000-00-00', '', 'A-', 34, '', 0, 'Aatkot', 34, '', '', '10', 0, 'Sholiter bagloj', 0, 3, 2, 0, 0, 'yes', 1, '16154820058279598381', '', 0, 'yes', 2, 'user', '2021-03-11 12:00:06', '2021-03-11 12:00:30'),
(39, 'PSB32', 1, 141, 'Nareshkumar', 'SURESHBHAI', '9714910858', '', 'nareshpatel.np111.in@gmail.com', '18415', '', '1995-11-23', '', 'AB+', 1, 'Mota varacha  Surat', 0, '', 0, '', '', 'B.com', 0, 'Gayatri nagar ', 0, 2, 2, 0, 0, 'yes', 1, '16154823854597351387', '', 0, 'yes', 2, 'user', '2021-03-11 12:06:26', '2021-03-11 12:06:40'),
(41, 'PSB34', 1, 1038, 'KRUNAL', 'VIPULBHAI', '9638998888', '', 'krunalkhokhariya@gmail.com', '55691', '', '1991-11-10', '', 'B+', 1, 'Atkot', 0, '', 0, '', '', 'Bcom', 8, 'ATKOT\n', 0, 2, 2, 0, 0, 'yes', 1, '16154828793074429853', '', 0, 'yes', 2, 'user', '2021-03-11 12:14:40', '2021-03-11 12:14:53'),
(42, 'PSB35', 1, 64, 'Ashish', 'Pravinbhai', '7878579600', '', 'aprupareliya9600@gmail.com', '15343', '', '1995-08-17', '', 'B+', 6, 'Shivam Engineering and plastic ', 0, '', 0, '', '', '', 7, 'Gundala Road jasmat Nagar nr Maruti steel Gondal 360311', 0, 3, 2, 0, 0, 'yes', 1, '16154829375636277838', '', 0, 'yes', 2, 'user', '2021-03-11 12:15:38', '2021-03-11 12:15:52'),
(43, 'PSB36', 1, 362, 'Bharatbhalala', 'Bhikhabhal', '9879751518', '', 'Bharat', '12345', '', '1973-03-24', '', 'B+', 42, 'Jasdan', 38, 'Jasdan', 28, 'Jasdan', '', '7', 11, 'Umiya nagar jasdan', 0, 2, 2, 0, 0, 'yes', 1, '16154840847473227985', '', 0, 'yes', 2, 'user', '2021-03-11 12:26:44', '2021-03-11 12:34:44'),
(44, 'PSB37', 1, 285, 'Narendrabhai', 'Hansharajbhai', '9033759614', '', 'bhainarendra123@gmail.com.', '83398', '', '1979-03-09', '', 'B+', 7, '', 0, '', 7, '', '', '10', 0, '87krishanapark pardi kande Sachin surat', 0, 179, 2, 0, 0, 'yes', 1, '16154836281432911189', '', 0, 'yes', 2, 'user', '2021-03-11 12:27:09', '2021-03-11 12:27:26'),
(46, 'PSB39', 1, 28, 'Rameshbhai', 'Girdharbhai', '9737013819', '', 'rkjasdan@gmail.com', '12345', '12345', '1978-07-27', 'b3040ae97162cf37cf03e17e621b0f71data.png', 'O+', 11, 'Radhakrishna Infotech\nGokul chamber\n2nd floor\nOpp new St depot\nJasdan  360050', 1, 'Radhakrishna puc\nAtkot road\nNr. By pass circle\nOpp. Dharti Honda\nJasdan , 360050', 37, 'Radhakrishna Auto advisor\nNr. By pass circle\nOpp. Dharti Honda\nJasdan , 360050', '', 'M.com , PGDCA, DISM', 0, 'Atkot road\nJalaram society\nJasdan 360050', 0, 2, 2, 0, 0, 'yes', 1, '16235037922997464996', '', 0, 'yes', 2, 'city_admin', '2021-03-11 12:50:55', '2021-06-12 09:16:32'),
(47, 'PSB40', 1, 414, 'BHARATBHAI', 'LADHABHAI', '9624172701', '', '', '35726', '', '1970-01-04', '92f0caf897fd4526dfc058cf80fe48c2data.png', 'AB+', 38, '', 0, '', 0, '', '', 'NON METRIC', 0, 'GOKHALANA ROAD RAMESHWAR  NAGAR', 56, 2, 2, 0, 0, 'yes', 1, '16154855606316485952', '', 0, 'yes', 2, 'user', '2021-03-11 12:59:21', '2021-03-11 12:59:37'),
(48, 'PSB41', 1, 28, 'Jagdish', 'Shambhubhai ', '9930904123', '', 'pj4123@gmail.com ', '23257', '', '1968-01-30', '00ea7e36f89eb6c96db046bc73f18dd9data.png', 'O+', 23, 'G002 laxmi krupa babai naka borivali west Mumbai ', 0, '', 0, '', '', 'S.S.C.', 0, 'Flat no 502 shree yogesh tower chsl near korakendra ground no 2borivali west Mumbai 400092', 0, 3, 2, 0, 0, 'yes', 1, '16154859241298509657', '', 0, 'yes', 2, 'user', '2021-03-11 13:05:25', '2021-03-11 13:05:40'),
(50, 'PSB43', 1, 157, 'Vivej', 'Ramnikbhai', '9106018622', '', '', '12345', '', '2021-03-11', '', '', 0, '', 45, 'Robin ', 0, '', '', '12', 0, 'Sapar veraval', 0, 7, 2, 0, 0, 'yes', 1, '16155662255594469610', '', 0, 'yes', 2, 'user', '2021-03-11 13:30:29', '2021-03-12 11:23:45'),
(52, 'PSB45', 1, 317, 'Suresh Kumar', 'Kangi bhai', '8200122412', '', 'sureshpatel26667@gmail.com', '12345', '', '1967-06-26', '', 'AB+', 1, 'Sardar cok jasdan', 1, 'Sardar cok jasdan', 1, 'Sardar cok jasdan', '', '10', 10, 'Sardar cok jasdan', 0, 2, 2, 0, 0, 'yes', 1, '16154896016199163439', '', 0, 'yes', 2, 'user', '2021-03-11 13:58:23', '2021-03-11 14:06:41'),
(53, 'PSB46', 1, 57, 'Jaydip', 'Bhupat bhai', '9723280760', '', '', '67807', '', '1990-12-12', '', 'B+', 4, 'Jasdan', 56, 'Jasdan ', 0, '', '', '10', 10, 'Virnagar', 0, 2, 2, 0, 0, 'yes', 1, '16154995838748439025', '', 0, 'yes', 2, 'user', '2021-03-11 16:53:04', '2021-03-11 16:53:25'),
(58, 'PSB51', 1, 19, 'Dilip', 'Vallabhbhai', '9924437175', '', 'dkakadiya10@gmail.com', '12345', '', '1990-12-26', '', '', 1, '', 43, '', 1, '', '', '', 8, 'Main bazar,Near Ramji mandir\n360050', 0, 2, 2, 0, 0, 'yes', 1, '16155592815218487646', '', 0, 'yes', 2, 'user', '2021-03-11 22:05:55', '2021-03-12 09:28:01'),
(59, 'PSB52', 1, 157, 'Damjibhai', 'Ghusabhai', '9913535742', '', '', '68998', '', '1967-03-05', '', 'B+', 27, 'Amreli pra.school', 30, 'Kerala ta.paddhari dist.rajkot', 0, '', 'Teacher', 'PTC', 4, 'Rajnagar society street no4\nKuvadava road Rajkot 360003', 0, 175, 2, 0, 0, 'yes', 1, '16155195283329009018', '', 0, 'yes', 2, 'user', '2021-03-11 22:25:29', '2021-03-11 22:25:44'),
(60, 'PSB53', 2, 711, 'Sanjaybhai', 'Vallabhbhai', '9824218085', '', 'Sanjayviroja0842@gmail.com', '12345', '', '1978-11-26', 'be713b72ed68d2a1d88acb73c60afa53data.png', 'B+', 33, 'Atkot', 33, '', 33, 'K.d.parvadiya hospital - atkot', '', 'Sybcom', 0, 'K.d.parvadiya multispeciality  hospital - atkot', 3, 2, 2, 0, 0, 'yes', 1, '16181556224451738949', '', 0, 'yes', 2, 'user', '2021-03-11 22:34:08', '2021-04-11 11:40:22'),
(61, 'PSB42', 0, 0, '', '', '9723318284', '', '', '15768', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 0, 0, 0, 0, 0, 'yes', 1, '16155234566659351561', '', 0, 'yes', -1, 'user', '2021-03-11 23:30:56', ''),
(62, 'PSB44', 0, 0, '', '', '7878747099', '', '', '59040', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 0, 0, 0, 0, 0, 'yes', 1, '16155234998321232153', '', 0, 'yes', -1, 'user', '2021-03-11 23:31:40', ''),
(63, 'PSB16', 1, 371, 'Vithalbhai', 'Nathabhai', '9978763363', '', 'Arjun', '10790', '', '0000-00-00', '', '', 4, 'Chitaliyarod', 0, '', 0, '', '', 'Ssc', 0, 'Arjunpark', 0, 2, 2, 0, 0, 'yes', 1, '16155275823394048716', '', 0, 'yes', 2, 'user', '2021-03-12 00:39:43', '2021-03-12 00:40:14'),
(64, 'PSB22', 1, 57, 'ashish', 'ramashbhai', '9898516555', '', 'ramaniashish87@gmail.com ', '12345', '', '2010-12-09', '', 'B+', 9, 'jasdan', 18, '', 1, '', '', '12', 4, 'uniyan bank pase', 0, 2, 2, 0, 0, 'yes', 1, '16155284515737268466', '', 0, 'yes', 2, 'user', '2021-03-12 00:52:33', '2021-03-12 00:54:11'),
(65, 'PSB24', 1, 414, 'Mehul', 'Jayantibhai', '9426406337', '', 'mpchhayani9@gmail.com', '12345', '', '0000-00-00', '', 'A+', 54, 'Rangoli pan center opp Krishna school', 0, '', 0, '', '', 'Hardware', 0, 'New Bus stop near Krishna school', 0, 2, 2, 0, 0, 'yes', 1, '16165668883801776210', '', 0, 'yes', 2, 'user', '2021-03-12 00:58:13', '2021-03-24 02:21:28'),
(66, 'PSB47', 1, 58, 'Sandip', 'Dineshbhai', '9601446450', '', 'sandiprank@gmail.com ', '17882', '', '1993-10-25', '', 'O+', 47, 'Textile industry ', 0, 'Jetpur-360370', 0, '', '', 'MBA(Marketing)', 7, 'Krishna Para vadia-365480', 0, 27, 21, 0, 0, 'yes', 1, '16155312668089696910', '', 0, 'yes', 2, 'user', '2021-03-12 01:41:06', '2021-03-12 01:41:18'),
(67, 'PSB48', 1, 54, 'Rasikbhai', 'VALLABHBHAI', '9879136221', '', 'rasiklalramotiya@gmail.com', '16533', '', '1969-09-22', '', '', 0, '', 0, 'Bildi', 27, 'Bildi', '', '', 2, 'Kailash bag society\nStreet no 9 \nB/H Ganda zad\nGondal', 0, 3, 2, 0, 0, 'yes', 1, '16155345806204667102', '', 0, 'yes', 2, 'user', '2021-03-12 02:36:21', '2021-03-12 02:36:48'),
(68, 'PSB49', 2, 662, 'Domadiya kenil', 'Raheshbhai', '9978624841', '', 'kenildomadiya58@gmail.com', '67865', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '1st year', 0, 'Surat', 0, 29, 21, 0, 0, 'yes', 1, '16155346179019330156', '', 0, 'yes', 2, 'user', '2021-03-12 02:36:57', '2021-03-12 02:37:07'),
(69, 'PSB50', 1, 161, 'MAHESH', 'SAVAJIBHAI', '9099114462', '', 'msshiroya@gmail.com', '44160', '', '1990-11-10', '07cec3c5630b51513c74ba004be0ba90data.png', 'B+', 27, 'Jasdan taluka panchayat', 0, '', 0, '', 'Extension officer agri', 'M.Sc Agriculture', 0, 'JUNU GAM, GADH PASE, SANATHALI, PIN/-364490\n', 0, 2, 2, 0, 0, 'yes', 1, '16155375061182891295', '', 0, 'yes', 2, 'user', '2021-03-12 03:25:06', '2021-03-12 03:25:25'),
(70, 'PSB26', 1, 317, 'Sanjay', 'Hasmukhbhai', '8905007143', '', 'patelsanjay2468@gmail.com', '31629', '', '1992-10-08', '', '', 1, '', 0, '', 0, '', '', 'B.Tech (mechanical)', 0, 'LATI PLOT , STREET NO 5', 0, 2, 2, 0, 0, 'yes', 1, '16155419779702072015', '', 0, 'yes', 2, 'user', '2021-03-12 04:39:37', '2021-03-12 04:39:47'),
(71, 'PSB33', 1, 57, 'CHANKIT', 'RAMESHBHAI', '9978544454', '', 'Chankitramani2213@gmail.Com', '30534', '', '1990-05-22', '', 'O+', 32, 'Shyam comlex First flor,shop no. 4,taluka seva sadan,same jasdan ', 38, '', 0, '', '', 'B. Com L.L.B', 0, 'Mohandada plot,360050 ', 0, 2, 2, 0, 0, 'yes', 1, '16155445678789987811', '', 0, 'yes', 2, 'user', '2021-03-12 05:22:48', '2021-03-12 05:23:06'),
(72, 'PSB38', 0, 0, '', '', '8320363052', '', '', '75649', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 0, 0, 0, 0, 0, 'yes', 1, '16155484354861274596', '', 0, 'yes', -1, 'user', '2021-03-12 06:27:16', ''),
(73, 'PSB54', 1, 138, 'Milan', 'Parshotambhai', '7229042222', '', 'Marutityreseng01@gmail.com ', '63552', '', '1983-04-23', '', 'B+', 23, 'Divam complex khodiyar col', 0, '', 0, '', '', 'B.A', 0, '2 shree nivas coloney s t Road ', 0, 85, 15, 0, 0, 'yes', 1, '16155522963632240108', '', 0, 'yes', 2, 'user', '2021-03-12 07:31:37', '2021-03-12 07:31:49'),
(74, 'PSB55', 1, 70, 'Bhaikhabhai', 'Nathabhai', '9824451310', '', 'Bnrokad@gmail.com', '12345', '', '1962-07-14', 'cff57fadd39ede1a830fe927cd7cdf14data.png', 'O+', 7, 'Sardar Patel Daymand Market, Gayatri Mandir Opp, Jasdan-360050.', 0, '', 0, '', '', '7', 0, 'Ganga Bhuvan, Sardar Patel Nagar Sheri Nambar-1, Atakot Rood, Jasdan-360050.', 56, 2, 2, 0, 0, 'yes', 1, '16155550555847902834', '', 0, 'yes', 2, 'user', '2021-03-12 08:16:36', '2021-03-12 08:17:35'),
(75, 'PSB56', 0, 0, '', '', '9979009363', '', '', '40677', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 0, 0, 0, 0, 0, 'yes', 1, '16155577554658057743', '', 0, 'yes', -1, 'user', '2021-03-12 09:02:35', ''),
(76, 'PSB57', 0, 0, '', '', '9925342458', '', '', '29505', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 0, 0, 0, 0, 0, 'yes', 1, '16155644093018773824', '', 0, 'yes', -1, 'user', '2021-03-12 10:53:29', ''),
(77, 'PSB58', 0, 0, '', '', '9925372458', '', '', '53586', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 0, 0, 0, 0, 0, 'yes', 1, '16155644813620833397', '', 0, 'yes', -1, 'user', '2021-03-12 10:54:42', ''),
(78, 'PSB59', 1, 205, 'PRAFULBHAI', 'GORDHANBHAI', '9409418426', '', 'prafulpaghadal11@gmail.com', '91434', '', '1976-07-08', '', 'A+', 5, 'Dhundhiya pipliya', 30, 'Dhundhiya pipliya', 11, 'Dhundhiya pipliya', '', 'S H C', 11, 'Dhundhiya pipliya', 0, 26, 21, 0, 0, 'yes', 1, '16155648183405237566', '', 0, 'yes', 2, 'user', '2021-03-12 11:00:18', '2021-03-12 11:00:40'),
(79, 'PSB60', 0, 0, '', '', '9979440239', '', '', '92577', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 0, 0, 0, 0, 0, 'yes', 1, '16155668305737058478', '', 0, 'yes', -1, 'user', '2021-03-12 11:33:51', ''),
(80, 'PSB61', 1, 175, 'v', 'm', '9328228704', '', '', NULL, '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, 'j', 51, 2, 2, 0, 5100, 'yes', 1, '', '', 0, 'yes', 2, 'user', '2021-03-12 11:56:36', ''),
(81, 'PSB62', 1, 110, 'a', 'a', '997200893', '', '', NULL, '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, 'a', 3, 2, 2, 0, 500, 'yes', 1, '', '', 0, 'yes', 2, 'user', '2021-03-12 12:02:11', ''),
(82, 'PSB63', 1, 126, 'n', 'v', '9904302237', '', '', NULL, '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, 'c', 2, 2, 2, 0, 1100, 'yes', 1, '', '', 0, 'yes', 2, 'user', '2021-03-12 12:04:44', ''),
(83, 'PSB64', 1, 215, 'Mehul', 'Laljibhai', '9725364900', '', 'Mehulpatel1575mp87@gmail.com', '12345', '', '0000-00-00', '99f3236a931a3ba4b8f0609104ec0e21data.png', '', 1, '', 56, '', 8, '', '', 'Civil engineering', 0, 'Chitliya rod jasdan', 56, 2, 2, 0, 1100, 'yes', 1, '16165072411589721729', '', 0, 'yes', 2, 'user', '2021-03-12 12:07:02', '2021-03-23 09:47:21'),
(84, 'PSB65', 1, 121, 'NIKHILBHAI', 'RAMESHBHAI', '9898348148', '', 'ernikhildesai@gmail.com', '12345', '', '1992-11-25', 'a6ecfa30355527baf0778c30e45d2888data.png', 'O+', 6, 'SHRI PITRU PUMPS\nMahadev Industries Area - 1\n60 feet main Road\nPADAVLA,RAJKOT-360024 ', 0, '', 0, '', '', 'Bachelor Of Engineering', 0, 'Plot Vistar,Bhandariya,\nJasdan,Rajkot-360026', 24, 2, 2, 0, 1100, 'yes', 1, '16162406793008554565', '', 0, 'yes', 2, 'user', '2021-03-12 12:10:23', '2021-03-20 07:44:39'),
(85, 'PSB66', 1, 286, 'jigneshbhai ', 'kalubhai', '9898733957', '', '', '12345', '', '1993-07-05', '3cc1c34e520b1383d632f04921c5bc2ddata.png', '', 14, '', 0, '', 0, '', '', 'F.y.b.com', 0, '', 20, 2, 2, 0, 501, 'yes', 1, '16198512426164243052', '', 0, 'yes', 2, 'user', '2021-03-12 12:13:20', '2021-05-01 02:40:42'),
(86, 'PSB67', 1, 234, 'j', 'd', '8000019261', '', '', NULL, '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 56, 2, 2, 0, 1111, 'yes', 1, '', '', 0, 'yes', 2, 'user', '2021-03-12 12:15:09', ''),
(87, 'PSB68', 1, 443, 'k', 'b', '9824425371', '', '', NULL, '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 56, 2, 2, 0, 1100, 'yes', 1, '', '', 0, 'yes', 2, 'user', '2021-03-12 12:16:51', ''),
(88, 'PSB69', 1, 1039, 'રાકેશ', 'અરવિંદભાઈ', '9699033451', '', 'rakesharvindbhai_patel@yahoo.co.in', '59503', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, 'Chandkheda ', 0, 19, 28, 0, 0, 'yes', 1, '16155694581522932996', '', 0, 'yes', 2, 'user', '2021-03-12 12:17:38', '2021-03-12 12:17:52'),
(89, 'PSB70', 1, 438, 'h', 'b', '9925467210', '', '', NULL, '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 56, 2, 2, 0, 1100, 'yes', 1, '', '', 0, 'yes', 2, 'user', '2021-03-12 12:18:35', ''),
(90, 'PSB71', 1, 222, 'bavanji Bhai', 'narshih Bhai', '9428265452', '', '', '12345', '', '0000-00-00', '', 'O-', 54, 'Opera pelsh c11\nFlet 503 laskana kholvad rod suart', 0, '', 0, '', '', '12', 0, 'Kylashnagar. ‌ ‌‌', 56, 2, 2, 0, 251, 'yes', 1, '16156969414495884461', '', 0, 'yes', 2, 'user', '2021-03-12 12:20:31', '2021-03-13 23:42:21'),
(91, 'PSB72', 1, 60, 'g', 'b', '9724428244', '', '', '12345', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 51, 2, 2, 0, 1100, 'yes', 1, '16197108756780798461', '', 0, 'yes', 2, 'user', '2021-03-12 12:22:56', '2021-04-29 11:41:15'),
(92, 'PSB73', 1, 354, 'v', 'a', '9998154530', '', '', NULL, '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 20, 2, 2, 0, 1100, 'yes', 1, '', '', 0, 'yes', 2, 'user', '2021-03-12 12:26:28', ''),
(93, 'PSB74', 1, 414, 'l', 'm', '9426442712', '', '', NULL, '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 56, 2, 2, 0, 5000, 'yes', 1, '', '', 0, 'yes', 2, 'user', '2021-03-12 12:28:18', ''),
(94, 'PSB75', 1, 57, 'arjan bhai', 'limba bhai', '9426961132', '', '', NULL, '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 4, 2, 2, 0, 11000, 'yes', 1, '', '', 0, 'yes', 2, 'user', '2021-03-12 12:31:00', ''),
(95, 'PSB76', 1, 141, 'r', 'v', '7096522029', '', '', NULL, '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 56, 2, 2, 0, 5100, 'yes', 1, '', '', 0, 'yes', 2, 'user', '2021-03-12 12:34:45', ''),
(96, 'PSB77', 1, 371, 'd', 'a', '9825982497', '', '', NULL, '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 2, 2, 2, 0, 1100, 'yes', 1, '', '', 0, 'yes', 2, 'user', '2021-03-12 12:36:18', ''),
(97, 'PSB78', 1, 357, 'kishor', 'gandu bhai', '9909201105', '', '', NULL, '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 11, 2, 2, 0, 1100, 'yes', 1, '', '', 0, 'yes', 2, 'user', '2021-03-12 12:40:42', ''),
(98, 'PSB79', 1, 157, 'r', 'b', '9913100717', '', '', NULL, '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 2, 2, 2, 0, 500, 'yes', 1, '', '', 0, 'yes', 2, 'user', '2021-03-12 12:44:20', ''),
(99, 'PSB80', 1, 1039, 'Kiritkumar', 'Govindbhai', '9428407611', '', 'kirit007bond@gmail.com', '12345', '', '1954-05-06', '4349740176149f12f1881d07e8e94ebadata.png', 'O+', 30, '', 1, 'N.A.', 57, '', '', 'B. A', 0, 'પ્લોટ નંબર:-૧૨૯/૧, સેક્ટર 6B, \nગાંધીનગર- ૩૮૨૦૦૬', 0, 106, 27, 0, 0, 'yes', 1, '16156567432565062539', '', 0, 'yes', 2, 'user', '2021-03-12 12:54:01', '2021-03-13 12:32:23'),
(100, 'PSB81', 1, 203, 'jagdish', 'chimanbhai', '9824342166', '', 'shyamauto1976@gmail.com', '12345', '', '1976-10-02', '', 'O+', 9, '6-urja complex nr D mart visat koba high way SABARMATI 380005', 16, 'as  above also all types of battery dealers\nautomotive/ups/solar/invester/e bike', 0, '', '', 'b com graduate ', 0, 'B/208 abhiyan flats TP-44 CHANDKHEDA', 0, 17, 28, 0, 0, 'yes', 1, '16155744268048191849', '', 0, 'yes', 2, 'user', '2021-03-12 13:27:05', '2021-03-12 13:40:26'),
(101, 'PSB82', 2, 1040, 'MAHENDRABHAI', 'PURASHOTTAMDAS', '9909012701', '', 'mppatel377752@gmail.com', '16055', '', '1961-05-01', '', 'A-', 23, 'VIJAY SHOPPING CENTER SEC 11 GNR', 30, 'VIJAY SHOPPING CENTER SEC 11 GNR', 0, '', '', 'Builders', 10, '672 G/1. SECTOR 6/B. GANDHINAGAR', 0, 106, 27, 0, 0, 'yes', 1, '16155736508543753894', '', 0, 'yes', 2, 'user', '2021-03-12 13:27:31', '2021-03-12 13:27:51'),
(102, 'PSB83', 1, 1039, 'Yogesh', 'Natvarbhai', '9879593240', '', 'yogesh3240@gmail.com', '18990', '', '0000-00-00', '', '', 37, 'Shree Ram Insurance Services \nAt-Adalaj', 23, 'Vassika developers', 0, '', '', '', 0, '31 RADHE HOME Bunglow AT-KUDASN\n', 0, 106, 27, 0, 0, 'yes', 1, '16155817628207068235', '', 0, 'yes', 2, 'user', '2021-03-12 15:42:43', '2021-03-12 15:42:54'),
(103, 'PSB84', 1, 355, 'Bhavika', 'Bharatbhai', '9725025681', '', '', '12345', '', '1990-03-23', '', '', 0, '', 0, 'Xyz', 1, '', '', '', 8, 'Vimalnagar mainroad rahkot', 0, 3, 2, 0, 0, 'yes', 0, '16155991918841916323', '', 0, 'yes', 2, 'user', '2021-03-12 20:22:44', '2021-03-12 20:33:11'),
(104, 'PSB85', 0, 0, '', '', '7801890962', '', '', '72745', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 0, 0, 0, 0, 0, 'yes', 1, '16156002582989800818', '', 0, 'yes', -1, 'user', '2021-03-12 20:50:59', ''),
(105, 'PSB86', 2, 1040, 'Pareshkumar', 'Kalidas', '9904125691', '', 'pparesh25691@gmail.com', '13284', '', '1980-06-20', '', 'B+', 23, 'Satvam parklane b/h.asshka hospital sargasan gandhinagar', 23, 'Satvam greens.near strowbery bungalows kudasan gandhinagar', 23, 'Radhe greens.mansa-itadara road mansa', '', 'Tyba', 10, '14, radhe homes.near shreenath homes.kudasan gandhinagar 382421', 0, 106, 27, 0, 0, 'yes', 1, '16156047037881841096', '', 0, 'yes', 2, 'user', '2021-03-12 22:05:03', '2021-03-12 22:05:16'),
(106, 'PSB87', 2, 950, 'ગુણવંતલાલ', 'લલ્લુભાઈ પટેલ', '9998996499', '', 'gunvant1504@gmail', '12345', '', '1958-04-15', '93b71087b4f789ee5794d9f3960c54dadata.png', 'B+', 30, 'Chandkheda', 57, 'Chandkheda', 27, 'અમદાવાદ મ્યુનિસિપલ કોર્પોરેશન\nલાયબ્રેરી ક્લાર્ક', '', 'B. A. Economic', 0, 'Chandkheda gam', 0, 17, 28, 0, 0, 'yes', 1, '16171140096148532524', '', 0, 'yes', 2, 'user', '2021-03-12 22:10:15', '2021-03-30 10:20:09'),
(107, 'PSB88', 1, 280, 'Dhaval Patel', 'LaxmanBhai', '7048704070', '', 'Dhavalpatel3069@gmail.com', '44718', '', '0000-00-00', '', 'B+', 17, 'Sant Kabir Rod', 17, '', 17, 'Sant Kabir Rod', '', '10.s.s.c', 0, 'Sant Kabir rod', 0, 175, 2, 0, 0, 'yes', 1, '16156081737277847116', '', 0, 'yes', 2, 'user', '2021-03-12 23:02:53', '2021-03-12 23:03:08'),
(108, 'PSB89', 1, 317, 'RAMESHBHAI', 'SHAMBHUBHAI', '9426719610', '', 'romeshtigerdj@gmail.com', '12345', '', '1974-01-06', '', 'O+', 18, 'M M YARNS PVT LTD\nSR NO 8 ,8,ATKOT - GONDAL HIGHWAY,KHARCHIYA(JAM)', 11, '', 0, '', '', 'NON METRIC', 0, 'BAJRANG NAGAR, MOCHIGOR PLOT \n360050', 0, 2, 2, 0, 0, 'yes', 1, '16159506237094302442', '', 0, 'yes', 2, 'user', '2021-03-12 23:16:41', '2021-03-16 23:10:23'),
(109, 'PSB90', 1, 1039, 'Pankajkumar', 'Chimanlal', '9904201285', '', 'Pcpatel75@yahoo.com ', '14104', '', '1975-03-28', '', '', 1, 'ચાંદખેડા ', 0, '', 0, '', '', '', 0, 'C103 dev poojan appartment motera sabarmati Ahmedabad 380005', 0, 106, 27, 0, 0, 'yes', 1, '16156093305150383762', '', 0, 'yes', 2, 'user', '2021-03-12 23:22:11', '2021-03-12 23:22:35'),
(110, 'PSB91', 2, 662, 'Rajendrakumar', 'Gandalal', '9824515473', '', 'www.rajpatel1231973@gmail.com', '13439', '', '1973-03-12', '', 'B+', 11, 'Sector-23, Gandhinagar', 0, '', 0, '', '', 'M.A.B.Ed', 2, 'Plot no-239 F-1 Xitij Appartment , Sector-28, Gandhinagar.', 0, 106, 27, 0, 0, 'yes', 1, '16156095516702342499', '', 0, 'yes', 2, 'user', '2021-03-12 23:25:52', '2021-03-12 23:26:13'),
(111, 'PSB92', 0, 0, '', '', '9924462666', '', '', '25402', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 0, 0, 0, 0, 0, 'yes', 1, '16156105295786395275', '', 0, 'yes', -1, 'user', '2021-03-12 23:42:10', ''),
(112, 'PSB93', 0, 0, '', '', '9427405570 ', '', '', '15157', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 0, 0, 0, 0, 0, 'yes', 1, '16156106601773744997', '', 0, 'yes', -1, 'user', '2021-03-12 23:44:20', ''),
(113, 'PSB94', 1, 2, 'A', 'R', '8128047277', '', '', NULL, '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 2, 2, 2, 0, 500, 'yes', 1, '', '', 0, 'yes', 2, 'user', '2021-03-12 23:46:54', ''),
(114, 'PSB95', 1, 93, 'JATIN', 'DIPAKBHAI', '8140060055', '', 'jatin.kikani.jk@gmail.com', '91925', '', '1998-03-25', '', 'O+', 14, 'zanzarda road near yamuna vadi \n\'THE BABY SHOP\' junagafh', 0, '', 0, '', '', 'BCA', 7, 'ranchod nagar shanti ni ketan soc junagadh 362001', 0, 101, 14, 0, 0, 'yes', 1, '16156112955710454512', '', 0, 'yes', 2, 'user', '2021-03-12 23:54:56', '2021-03-12 23:55:06'),
(115, 'PSB96', 1, 42, 'Kandarp', 'Bipinchandra', '7016811381', '', 'Thesiyakandarp9925@gmail.com', '66417', '', '1998-12-05', '', '', 14, 'THE BABY SHOP , zanzarda road , near yamuna vadi , junagah', 0, '', 0, '', '', 'Bsc(IT)', 7, 'Near railway station , joshipura , junagadh', 0, 101, 14, 0, 0, 'yes', 1, '16156114585406296957', '', 0, 'yes', 2, 'user', '2021-03-12 23:57:39', '2021-03-12 23:57:55'),
(116, 'PSB97', 0, 0, '', '', '9825946036', '', '', '42617', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 0, 0, 0, 0, 0, 'yes', 1, '16156121839218938290', '', 0, 'yes', -1, 'user', '2021-03-13 00:09:44', ''),
(117, 'PSB98', 1, 153, 'Rameshbhai', 'Harjibhai', '9427565557', '', '', NULL, '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 2, 2, 2, 0, 500, 'yes', 1, '', '', 0, 'yes', 2, 'user', '2021-03-13 00:11:08', ''),
(118, 'PSB99', 1, 60, 'BHARATKUMAR', 'DUDABHAI', '9898054636', '', 'bharat_15463@yahoo.com', '73806', '', '2021-03-13', '', 'O+', 0, 'Deputy Secretary, Revenue Department, Government of Gujarat, Block: 11/3, Sardar Bhavan, Sachivalaya, Gandhinagar. ', 0, '', 0, '', '', 'B. Com. ', 0, '1315/1, SECTOR : 2-B, GANDHINSGAR', 0, 0, 21, 0, 0, 'yes', 1, '16156122932270307937', '', 0, 'yes', 2, 'user', '2021-03-13 00:11:33', '2021-03-13 00:11:48'),
(119, 'PSB100', 1, 362, ' Ashwinbhai', '', '9825217618', '', '', '12345', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 0, 3, 2, 0, 11000, 'yes', 1, '16190870454271076151', '', 0, 'yes', 2, 'user', '2021-03-13 00:14:02', '2021-04-22 06:24:05'),
(120, 'PSB101', 1, 57, 'Gajendrabhai', 'P', '9426915325', '', '', '12239', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, 'J', 0, 2, 2, 0, 0, 'yes', 1, '16156125562375264976', '', 0, 'yes', 2, 'user', '2021-03-13 00:15:57', '2021-03-13 00:16:11'),
(121, 'PSB102', 1, 303, 'Dilipbhai', 'Nagjibhai', '7990797254', '', '', NULL, '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 0, 3, 2, 0, 11000, 'yes', 1, '', '', 0, 'yes', 2, 'user', '2021-03-13 00:17:22', ''),
(122, 'PSB103', 1, 363, 'Meghajibhai', 'Pragjibhai', '9824092372', '', 'Meghjibhaibhayani@gmail.com', '12345', '', '1970-11-02', '4ad34c5ad72df277c4711c153480205cdata.png', 'B+', 23, 'Jasdan Atkot road, opposite kalyani petrol pump', 0, '', 0, '', '', '12', 0, 'Chitaliya road, jasdan', 56, 2, 2, 0, 2100, 'yes', 1, '16197059098827035520', '', 0, 'yes', 2, 'user', '2021-03-13 00:20:48', '2021-04-29 10:18:29'),
(123, 'PSB104', 3, 0, 'Aashis', 'Ada', '9879685053', '', '', NULL, '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 12, 2, 2, 0, 500, 'yes', 1, '', '', 0, 'yes', 2, 'user', '2021-03-13 00:24:50', ''),
(124, 'PSB105', 0, 0, '', '', '9824130332', '', '', '10821', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 0, 0, 0, 0, 0, 'yes', 1, '16156140821571730216', '', 0, 'yes', -1, 'user', '2021-03-13 00:41:22', ''),
(125, 'PSB106', 2, 950, 'સંજયકુમાર', 'વિરચંદભાઈ', '9904153671', '', 'Sanjaypatel8419@gmail.com ', '81508', '', '1984-07-18', '7644c254dc58c60b074c9c39815f2897data.png', 'AB+', 32, 'ડીસ્ટ્રીક્ટ એન્ડ સેશન્સ કોર્ટ ગાંધીનગર સે-૧૧', 0, '', 0, '', '', 'એલએલબી', 0, '509,મુખી વાસ, ચાંદખેડા ગામ, અમદાવાદ ', 0, 19, 28, 0, 0, 'yes', 1, '16156159999732545618', '', 0, 'yes', 2, 'user', '2021-03-13 01:13:20', '2021-03-13 01:13:37'),
(126, 'PSB107', 2, 1040, 'Parth', 'Rameshbhai', '9978964446', '', 'R.jtravels143@gmail.com', '65290', '', '0000-00-00', '', 'AB+', 30, '11, m v houses, sahibag ', 0, '', 0, '', '', '', 0, '13.rajdeep society, opp d mart, vishat, chandkhda ', 0, 19, 28, 0, 0, 'yes', 1, '16156183878871479913', '', 0, 'yes', 2, 'user', '2021-03-13 01:53:08', '2021-03-13 01:53:23'),
(127, 'PSB108', 1, 1039, 'Piyushnikola', 'Jagdishbhai', '9687129460', '', 'NIKOLAPIYUSH@GMAIL.COM ', '72569', '', '1991-10-08', '', '', 1, '', 0, '', 0, '', '', 'M.com', 0, '124/8,street no-2,Mayur township, ranjit sagar road ', 0, 85, 15, 0, 0, 'yes', 1, '16156286799512679954', '', 0, 'yes', 2, 'user', '2021-03-13 04:44:39', '2021-03-13 04:44:52'),
(128, 'PSB109', 0, 0, '', '', '9904464970', '', '', '72605', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 0, 0, 0, 0, 0, 'yes', 1, '16156298743449838800', '', 0, 'yes', -1, 'user', '2021-03-13 05:04:34', ''),
(129, 'PSB110', 1, 191, 'Paresh', 'Ravjibhai', '9824295422', '', 'p.nariya@yahoo.com', '97999', '', '0000-00-00', '', 'B+', 9, 'Gidc phase ii plot no 656 village dared Rohit auto industries 361004', 30, '', 0, '', '', '12 sci', 0, 'Utsav residency flat no 402 shri nivas colony Street no -1 ranjit nagar main road near Jain Vijay farsan Mart Jamnagar 361005\n', 0, 80, 15, 0, 0, 'yes', 1, '16156338582427543377', '', 0, 'yes', 2, 'user', '2021-03-13 06:10:58', '2021-03-13 06:11:16'),
(130, 'PSB111', 1, 145, 'Prakash', 'Vallbhbhai', '9429048330', '', 'pprakash347@yahoo.com', '69395', '', '1985-12-08', '', '', 1, '', 0, '', 0, '', '', '8', 0, 'Morbi road shree Ram Prak Seri No-1(31) ', 0, 0, 2, 0, 0, 'yes', 1, '16156355457970791974', '', 0, 'yes', 2, 'user', '2021-03-13 06:39:06', '2021-03-13 06:39:20'),
(131, 'PSB112', 2, 840, 'Chandrkant', 'A', '9824228767', '', '', NULL, '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 56, 2, 2, 0, 1100, 'yes', 1, '', '', 0, 'yes', 2, 'user', '2021-03-13 07:26:39', ''),
(132, 'PSB113', 1, 80, 'HIREN', 'RAMJIBHAI', '9974718209', '', 'hrlimbani1994@gmail.com', '39518', '', '1994-09-30', '', 'O+', 5, 'At.sanosara', 0, '', 0, '', '', 'MSc in industrial chemistry', 0, 'At.sanosara', 0, 64, 20, 0, 0, 'yes', 1, '16156389155847860119', '', 0, 'yes', 2, 'user', '2021-03-13 07:35:16', '2021-03-13 07:35:29'),
(133, 'PSB114', 1, 205, 'VIPULBHAI', 'Chhaganbhai', '9687140302', '', 'Sppatel5253@gmail.com ', '24364', '', '1979-03-13', '', '', 6, '', 0, 'Shree hari metal, survy no.36, near ice factory,  patel chok, vavdi, gondal road, rajkot 4.', 0, '', '', 'Hsc', 4, '\"Krish\", jasrajnagar-3, garbi chowk,  near radhe hotal, mavdi  rajkot4', 0, 175, 2, 0, 0, 'yes', 1, '16156424274266374242', '', 0, 'yes', 2, 'user', '2021-03-13 08:33:47', '2021-03-13 08:34:02'),
(134, 'PSB115', 1, 317, 'Hardik', 'Ramesh bhai', '8758449044', '', 'hardikhirpara3821@gmail.com', '42226', '', '1991-07-13', '', 'O+', 6, 'Khodiyar industrial area mavdi', 0, '', 0, '', '', '10th', 0, 'Khodiyar industrial area', 0, 175, 2, 0, 0, 'yes', 1, '16156530096741145690', '', 0, 'yes', 2, 'user', '2021-03-13 11:30:09', '2021-03-13 11:30:21'),
(135, 'PSB116', 1, 323, 'Dhiren', 'Dasarthabhai', '9909009253', '', 'dhirenamin1171@gmail.com', '89264', '', '1973-06-07', '', '', 25, 'Kh-2, sector 4 c, gandhinagar', 0, '', 0, '', '', 'Hsc', 0, 'Plot no-480/1, sector-3 c, gandhinagar', 0, 106, 27, 0, 0, 'yes', 1, '16156581723146613697', '', 0, 'yes', 2, 'user', '2021-03-13 12:56:12', '2021-03-13 12:56:33'),
(136, 'PSB117', 1, 1039, 'દેવેન્દ્ર', 'લાલભાઈ', '9725805050', '', 'dpatel.anmol@gmail.com', '33583', '', '1970-06-01', '', 'O+', 22, 'સેટેલાઇટ, અમદાવાદ ', 15, '', 0, '', '', 'સિવિલ એન્જિનિયર ', 0, '8, લક્ષ્મીકૃપા સોસાયટી, અંકિની સ્કૂલ પાસે, ચાંદખેડા, અમદાવાદ ', 0, 19, 28, 0, 0, 'yes', 1, '16156760643367097385', '', 0, 'yes', 2, 'user', '2021-03-13 17:54:24', '2021-03-13 17:54:44'),
(137, 'PSB118', 0, 0, '', '', '9825071653', '', '', '27912', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 0, 0, 0, 0, 0, 'yes', 1, '16156875444971806125', '', 0, 'yes', -1, 'user', '2021-03-13 21:05:45', ''),
(138, 'PSB119', 1, 316, 'Yagnik', 'Manojbhai', '8380801380', '', 'yagnikhirani12@gmail.com', '32992', '', '2000-04-21', '', 'B+', 7, 'Jasdan', 7, '', 0, '', '', '12th complete', 0, 'Sahiyar city', 0, 2, 2, 0, 0, 'yes', 1, '16156882357142606382', '', 0, 'yes', 2, 'user', '2021-03-13 21:17:16', '2021-03-13 21:17:30'),
(139, 'PSB120', 1, 1039, 'Kaushikkumar', 'Natvarlal', '9909904941', '', 'knpatel1704@gmail.com', '65198', '', '1974-04-17', 'fcd43db5e8ba87267b0f880d9d9c3473data.png', 'B-', 32, '5 Hari Om complex vepari Jin kalol 382721', 1, '', 0, '', '', 'B.com LLB', 0, 'Mukhivas juna chora Kalol 382721', 0, 105, 27, 0, 0, 'yes', 1, '16156910989495062575', '', 0, 'yes', 2, 'user', '2021-03-13 22:04:58', '2021-03-13 22:05:17'),
(140, 'PSB121', 2, 1040, 'Aakash', 'Jayntibhai', '7984417262', '', 'akash14054@gmail.com', '34465', '', '1999-08-25', '', 'A+', 4, 'Kubadharol', 0, '', 0, '', '', 'B.sc B.ed.', 0, 'Kubadharol', 0, 181, 22, 0, 0, 'yes', 1, '16156977705049812783', '', 0, 'yes', 2, 'user', '2021-03-13 23:56:11', '2021-03-13 23:56:20'),
(141, 'PSB122', 0, 0, '', '', '9825858825', '', '', '49204', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 0, 0, 0, 0, 0, 'yes', 1, '16156999263108313299', '', 0, 'yes', -1, 'user', '2021-03-14 00:32:07', ''),
(142, 'PSB123', 0, 0, '', '', '7573003058', '', '', '50945', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 0, 0, 0, 0, 0, 'yes', 1, '16157024816930884334', '', 0, 'yes', -1, 'user', '2021-03-14 01:14:41', ''),
(143, 'PSB124', 1, 196, 'Rushil', 'Vinodbhai', '6355294447', '', 'rushilnathani11@gmail.com', '35208', '', '2000-06-15', '', '', 30, 'RATANPAR', 33, 'Bas stund Rajkot trikon bag', 0, '', '', '12pass', 0, 'RATANPAR Morbi road Rajkot', 0, 175, 2, 0, 0, 'yes', 1, '16157026723531860029', '', 0, 'yes', 2, 'user', '2021-03-14 01:17:52', '2021-03-14 01:18:13'),
(144, 'PSB125', 1, 1039, 'Dhruv', 'Rakeshbhai', '9265563082', '', 'rakeshp373@gmail.com ', '80008', '', '1995-08-07', '', 'A+', 20, '', 20, 'Money exchange and tours and travels', 23, '', '', 'B.Com', 9, 'Dharmaj ', 0, 36, 34, 0, 0, 'yes', 1, '16157028048735966458', '', 0, 'yes', 2, 'user', '2021-03-14 01:20:04', '2021-03-14 01:20:29'),
(145, 'PSB126', 2, 662, 'Raghav', 'Amrutlal', '9327048157', '', 'ragpatidar72@gmail.com', '87756', '', '2001-07-20', '', 'B+', 35, 'Bhuj', 0, '', 0, '', '', '12+com.', 0, 'Sanyra\n', 0, 108, 10, 0, 0, 'yes', 1, '16157028257265797261', '', 0, 'yes', 2, 'user', '2021-03-14 01:20:25', '2021-03-14 01:20:40'),
(146, 'PSB127', 1, 411, 'MAHESH', 'PRAVINBHAI', '8200190683', '', 'mahesh.chovatiya9@gmail.com', '12345', '', '1988-12-02', '', 'B+', 48, 'RAJKOT', 1, '', 0, '', '', 'LLB', 6, 'AT AMARGADH TA JI RAJKOT', 0, 175, 2, 0, 0, 'yes', 1, '16157030612235008751', '', 0, 'yes', 2, 'user', '2021-03-14 01:21:26', '2021-03-14 01:24:21'),
(147, 'PSB128', 1, 57, 'HITESH', 'JAYNTIBHAI', '9924530749', '', 'Hhiteshh1515@yahoo.com', '12345', '', '2021-03-04', '', 'O+', 11, 'Latiplot jasdan', 1, 'Lati plot jasdan', 11, 'Ganga bhavan jasdan', '', 'B,ed', 2, 'Geeta nagar jasdan', 0, 2, 2, 0, 0, 'yes', 1, '16171549939555124932', '', 0, 'yes', 2, 'user', '2021-03-14 01:37:10', '2021-03-30 21:43:13'),
(148, 'PSB129', 1, 45, 'MANSUKHBHAI', 'POPATBHAI', '9979498616', '', '', '12345', '', '1976-09-19', '7958497968dd228a30c471f5721b317ddata.png', '', 38, 'JASDAN', 0, '', 0, '', '', 'NONMETRIC', 0, 'NAVA JASAPAR', 7, 2, 2, 0, 0, 'yes', 1, '16157046699684845402', '', 0, 'yes', 2, 'user', '2021-03-14 01:50:54', '2021-03-14 01:51:09'),
(149, 'PSB130', 1, 145, 'પંકજ', 'ગોબરભાઈ', '9879685584', '', 'Pankajvasani84@gmail.com', '72886', '', '1982-09-19', '', 'B+', 24, 'Jasapar', 37, 'Jasapar', 0, '', '', 'B A ', 0, 'Juna jasapar main bajar ', 0, 2, 2, 0, 0, 'yes', 1, '16157053858521090771', '', 0, 'yes', 2, 'user', '2021-03-14 03:03:05', '2021-03-14 03:03:19'),
(150, 'PSB131', 0, 0, '', '', '7575035473', '', '', '94819', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 0, 0, 0, 0, 0, 'yes', 1, '16157065922880418562', '', 0, 'yes', -1, 'user', '2021-03-14 03:23:13', ''),
(151, 'PSB132', 1, 209, 'Sandip', 'Gobarbhai', '9725458925', '', 'Mantrapambhar11@gmail. Com', '15644', '', '1989-11-09', '', 'B+', 36, '', 0, 'Vavdi rajkot', 36, '', '', '12', 0, 'To આણંદપર  ta. કાલાવડ ', 0, 83, 15, 0, 0, 'yes', 1, '16157067966021142466', '', 0, 'yes', 2, 'user', '2021-03-14 03:26:37', '2021-03-14 03:26:48'),
(152, 'PSB133', 1, 74, 'PRADIP', 'GOVINDBHAI', '9825804279', '', 'pradipgpatel9001@gmail.com ', '82459', '', '1977-11-14', '', 'AB+', 18, 'GIRIRAJ COTSYN PVT LTD MANAVADAR ', 8, '', 0, '', '', '12', 0, '		GIRIRAJ  NEAR MAVJI JINA SOS. ', 0, 97, 14, 0, 0, 'yes', 1, '16157071641704985443', '', 0, 'yes', 2, 'user', '2021-03-14 03:32:45', '2021-03-14 03:33:02'),
(153, 'PSB134', 1, 14, 'Prakashbhai', 'Cahnabhai', '9824835673', '', 'Prakash', '12345', '', '0000-00-00', '', '', 17, 'Marketigyad', 0, '', 0, '', '', '9 std', 0, 'Marketigyad', 0, 175, 2, 0, 0, 'yes', 1, '16157904479568668657', '', 0, 'yes', 2, 'user', '2021-03-14 06:31:06', '2021-03-15 02:40:47'),
(154, 'PSB135', 1, 335, 'સુનિલભાઈ', 'રઘુભાઈ', '9428036652', '', '', '86767', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, 'ત્રંબા ત્રંબા ત્રંબ', 0, 175, 2, 0, 0, 'yes', 1, '16157212709294484924', '', 0, 'yes', 2, 'user', '2021-03-14 07:27:51', '2021-03-14 07:28:39'),
(155, 'PSB136', 1, 64, 'Alpesh', 'Babubhai', '9824905171', '', '', '85762', '', '1987-07-10', 'f1968514cb8cbb0d210f16a63cab5820data.png', 'A+', 9, 'Sardarchok', 38, '', 0, '', '', 'Ba', 0, 'Bajrang Nagar', 56, 2, 2, 0, 11000, 'yes', 1, '16157305834516692748', '', 0, 'yes', 2, 'user', '2021-03-14 10:03:04', '2021-03-14 10:03:18'),
(156, 'PSB137', 3, 1041, 'અમૃતલાલ', 'દ્વારકાદાસ', '9879788378', '', 'Bhagvatiply@gmail.com', '28609', '', '1970-06-01', '84b3bad1d24c418440be4a05238a91e3data.png', 'B-', 39, 'ભગવતી પ્લાયવુડ સેન્ટર', 0, '', 0, '', '', 'Bcom', 0, 'A.301 દેવસ્ય ધ લેન્ડમાર્ક નિકોલ', 0, 17, 28, 0, 0, 'yes', 1, '16157320693188048509', '', 0, 'yes', 2, 'user', '2021-03-14 10:27:50', '2021-03-14 10:28:16'),
(157, 'PSB138', 2, 869, 'Vijaykumar', 'Jagadishbhai', '9879136848', '', 'vijaykasundra45@gmail.com', '17582', '', '1983-06-01', '3f2fcf2ffeecefabb33e02012e63863bdata.png', 'O+', 0, 'To avaniya', 27, '', 27, '', '', 'Msc bed chemistry', 0, 'To --maliyahatina\nRanchhodnagar\nNear prajapati samaj\nPin--362245\nMo--9879136848', 0, 98, 14, 0, 0, 'yes', 1, '16157323069519816399', '', 0, 'yes', 2, 'user', '2021-03-14 10:31:47', '2021-03-14 10:32:00'),
(158, 'PSB139', 2, 468, 'JAY', 'SURESH BHAI', '7433898958', '', 'jayaghara3@gmail.com', '72523', '', '1996-03-15', '88c4e4b0137fe003836e407b10e64d52data.png', '', 47, 'SPYKON CERAMIC', 0, '', 0, '', '', 'B.E. ( Civil )', 0, 'Shree Hari Height,Dev Park, Morbi', 0, 194, 13, 0, 0, 'yes', 1, '16157330562144211354', '', 0, 'yes', 2, 'user', '2021-03-14 10:44:16', '2021-03-14 10:44:28'),
(159, 'PSB140', 2, 1040, 'VISHAL', 'GOVINDBHAI', '7485927797', '', 'Karnavatvishal2001@gmail.com', '78988', '', '2001-12-04', '', 'B+', 0, '', 1, 'Motipura ', 0, '', '', 'Bsc microbiology', 3, 'Motipura ', 0, 51, 25, 0, 0, 'yes', 1, '16157335405908450170', '', 0, 'yes', 2, 'user', '2021-03-14 10:52:20', '2021-03-14 10:52:35'),
(160, 'PSB141', 0, 0, '', '', '9998571199', '', '', '54864', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 0, 0, 0, 0, 0, 'yes', 1, '16157342408207693282', '', 0, 'yes', -1, 'user', '2021-03-14 11:04:01', ''),
(161, 'PSB142', 2, 662, 'Surekha', 'Prahladbhai', '7623816540', '', 'surekhap385@gmail.com', '65544', '', '1986-12-30', '', 'B+', 27, 'Teacher', 0, '', 0, '', '', 'M.A,B.ed', 0, 'Vasai', 0, 128, 24, 0, 0, 'yes', 0, '16157349418477499358', '', 0, 'yes', 2, 'user', '2021-03-14 11:15:42', '2021-03-14 11:15:55'),
(162, 'PSB143', 2, 1040, 'Savankumar', 'Kanaiyalal', '7405090193', '', 'skp.patel90@gmail.com ', '89929', '', '0000-00-00', '', '', 11, 'Sankalchand Patel University ', 0, '', 0, '', '', 'M.E. Transportation Engineering ', 0, 'Mehanna \n', 0, 132, 24, 0, 0, 'yes', 1, '16157359634445932204', '', 0, 'yes', 2, 'user', '2021-03-14 11:32:44', '2021-03-14 11:33:10'),
(163, 'PSB144', 1, 205, 'Keyur', 'Devshibhai', '9327566327', '', 'Keyurpagada@gmail.com ', '22681', '', '2003-07-26', '1c7885823da694d1fba7895bbef5484fdata.png', 'B+', 0, '', 0, '', 0, '', '', 'B.com ', 0, 'Motiveravl ', 0, 80, 15, 0, 0, 'yes', 1, '16157367454963911437', '', 0, 'yes', 2, 'user', '2021-03-14 11:45:45', '2021-03-14 11:46:00'),
(164, 'PSB145', 1, 383, 'Vipul Patel', 'Bagavan.BaHi', '9898953210', '', '', '53487', '', '0000-00-00', '', 'B+', 7, 'ગામ કાગદડીતાબગસરાજીઅમરેલી', 28, 'ગામ કાગદડીતાબગસરાજીઅમરેલી', 28, 'ગામ કાગદડીતાબગસરાજીઅમરેલી', '', '10', 10, 'ગામ કાગદડીતાબગસરાજીઅમરેલી', 0, 22, 21, 0, 0, 'yes', 1, '16157380628194871509', '', 0, 'yes', 2, 'user', '2021-03-14 12:07:43', '2021-03-14 12:08:56'),
(165, 'PSB146', 2, 1040, 'Parthiv', 'Maheshbhai', '9904989629', '', '', '35329', '', '0000-00-00', '', 'B+', 0, '', 0, '', 0, '', '', 'Becom', 2, 'Balol gamma joshi matha', 0, 132, 24, 0, 0, 'yes', 1, '16157380849431136872', '', 0, 'yes', 2, 'user', '2021-03-14 12:08:04', '2021-03-14 12:08:16'),
(166, 'PSB147', 1, 1039, 'રાજેશ', 'કિશનભાઈ', '8140845700', '', 'rajeshpatel190675@gmail.com ', '54317', '', '0000-00-00', '', 'A+', 0, '', 0, '', 0, '', '', 'ઈલેકટ્રીકલ એન્જીનીયરીંગ', 0, 'Shivranjani park, street no -2  near Ranchhod ashram, Kuvadva road, Rajkot.', 0, 175, 2, 0, 0, 'yes', 1, '16157396157942425094', '', 0, 'yes', 2, 'user', '2021-03-14 12:33:36', '2021-03-14 12:33:57'),
(167, 'PSB148', 2, 1040, 'ROHIT', 'CHANDUBHAI', '9998953682', '', 'rohitpatel6380@gmail.com', '97759', '', '1980-03-06', '', 'B+', 1, 'G.S.RT.C.\nRANIP BUS PORT.', 1, '', 0, '', '', 'B.A.', 0, 'G-403, home town-3, Near. Canara bank, new ranip, ahmedabad', 0, 19, 28, 0, 0, 'yes', 1, '16157398836124792453', '', 0, 'yes', 2, 'user', '2021-03-14 12:38:04', '2021-03-14 12:38:17'),
(168, 'PSB149', 0, 0, '', '', '8140845550', '', '', '89648', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 0, 0, 0, 0, 0, 'yes', 1, '16157401575942061357', '', 0, 'yes', -1, 'user', '2021-03-14 12:42:37', ''),
(169, 'PSB150', 1, 86, 'Prashant', 'Parsotambhai', '7600524630 ', '', 'somnath22891143@gmail.com ', '12345', '', '1991-08-22', '', 'AB+', 45, '', 0, 'Dhoraji ', 0, '', '', 'Graduation ', 4, 'Nr. Ram temple, jashapar ', 0, 179, 2, 0, 0, 'yes', 1, '16179978264017914420', '', 0, 'yes', 2, 'user', '2021-03-14 12:59:44', '2021-04-09 15:50:26'),
(170, 'PSB151', 1, 317, 'રમેશ હિરપરા', 'ગોરઘનભાઈહિરપરાજ', '8511030298', '', '', '21424', '', '2021-03-14', '6842d54f620fb61fb759f0bbe0008121data.png', 'O+', 0, 'જોબ', 52, 'જેતપુર', 21, '', '', '12', 0, 'જેતપુર', 0, 8, 2, 0, 0, 'yes', 1, '16157418883671077673', '', 0, 'yes', 2, 'user', '2021-03-14 13:11:29', '2021-03-14 13:11:51'),
(171, 'PSB152', 2, 662, 'JAGDISHKUMAR', 'RANCHHODBHAI', '9925015083', '', 'jagdishadvo@gmail.com', '49711', '', '1962-06-01', '', 'AB+', 32, '07, Sith floor  Sungress Arcade\nMotra, Ahmedabad 382424', 0, '', 0, '', '', 'B. Com LLb', 10, '4, SARTHAK BUNGLOWS, \nOPP. Govt. Engg. College \nNew CG Road Chandkheda. \nAhmedabad. 382424', 0, 19, 28, 0, 0, 'yes', 1, '16157423845183770696', '', 0, 'yes', 2, 'user', '2021-03-14 13:19:45', '2021-03-14 13:20:51');
INSERT INTO `user` (`uid`, `member_id`, `samaj_id`, `surname_id`, `middle_name`, `father_name`, `mobile`, `mobile_1`, `email`, `password`, `ad_password`, `dob`, `profile_pic`, `blood_group`, `business`, `business_address`, `business_2`, `business_address_2`, `business_3`, `business_address_3`, `gov_job`, `qualification`, `karkidi_id`, `address`, `village_id`, `taluko_id`, `dist_id`, `hundi_amount`, `fund_amount`, `is_mobile`, `gender`, `is_Token`, `notificationToken`, `verify_by`, `app_access`, `status`, `type`, `date`, `lastlogin`) VALUES
(172, 'PSB153', 2, 478, 'રસીકલાલ', 'ગણેશભાઇ', '9428248051', '', 'rasiklalgpatel@gmail.com', '12345', '', '1960-08-04', 'f483b5717a9e263d95e56981f369e08adata.png', 'A+', 30, 'સુલતાનપુર', 0, '', 0, '', '', 'BCOM', 0, 'જડેસ્વર સોસાયટી  . હળવદ રોડ.\nપીન 363310 - Dhrangadhara.', 0, 213, 11, 0, 0, 'yes', 1, '16160911734824965923', '', 0, 'yes', 2, 'user', '2021-03-14 13:23:01', '2021-03-18 14:12:53'),
(173, 'PSB154', 1, 414, 'PIYUSH', 'GOVINDBHAI', '9737862702', '', 'PIYUSHPATEL999999999@GMAIL.COM', '49048', '', '2021-03-14', '', 'B+', 22, 'CIVIL ', 22, 'Civil', 0, 'Civil', '', '12 ', 7, 'SHREE NATHAJI CWOK JASDAN', 0, 2, 2, 0, 0, 'yes', 1, '16157431623514738710', '', 0, 'yes', 2, 'user', '2021-03-14 13:32:43', '2021-03-14 13:32:54'),
(174, 'PSB155', 2, 662, 'Jashubhai', 'Ramanbhai', '9426534528', '', 'pateljashubhai28@gmail.com ', '13191', '', '1976-05-08', '430c05c469861795300ae067b9014045data.png', 'O+', 27, 'Shivpurakampa TA.&Dist.-gandhinagar ', 5, '', 1, 'Chiloda ', 'Principal ', 'P.T.C,M.A,B.ed,L.L.B.', 0, '2, Strawberry Bungalows \nKudasan,gandhinagar. 382421', 0, 106, 27, 0, 0, 'yes', 1, '16157432623750647700', '', 0, 'yes', 2, 'user', '2021-03-14 13:34:23', '2021-03-14 13:34:49'),
(175, 'PSB156', 1, 1039, 'Krunal', 'Vasantbhai', '9537743552', '', 'Patelkrunal13694@gmail.com', '75629', '', '1994-06-13', '', 'B+', 34, 'HARIJ', 0, '', 0, '', '', '12', 10, 'Patel Vas, Harij', 0, 166, 23, 0, 0, 'yes', 1, '16157434459332342178', '', 0, 'yes', 2, 'user', '2021-03-14 13:37:26', '2021-03-14 13:37:43'),
(176, 'PSB157', 2, 1040, 'KALPESHKUMAR', 'MANILAL', '8238889184', '', 'kmp29632@yahoo.com', '66636', '', '1972-01-15', '7c8bc0ca5199657efc6b03e09fc59cc9data.png', 'O+', 31, '03,Umiya house complex\nOpp Arogyanagar Mr. Busstand \nHimatnagar ', 0, '', 0, '', '', 'Bsc Physics Diploma Pharmacy ', 0, '05,Patidar Bungalow Gayatri Mandir \nRoad MAHAVIRNAGAR Himatnagar \n383001', 0, 189, 22, 0, 0, 'yes', 1, '16157449545681185753', '', 0, 'yes', 2, 'user', '2021-03-14 14:02:35', '2021-03-14 14:02:49'),
(177, 'PSB158', 0, 0, '', '', '9825568052', '', '', '74036', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 0, 0, 0, 0, 0, 'yes', 1, '16157451713570826548', '', 0, 'yes', -1, 'user', '2021-03-14 14:06:12', ''),
(178, 'PSB159', 3, 1041, 'Hiren', 'Indravadan', '7984184365', '', 'hiren0914@gmail.com ', '98168', '', '1985-10-10', '', 'B+', 27, 'Ahmedabad municipal corporation ', 0, '', 0, '', '', 'BA. B. ED', 0, 'At: Kherol', 0, 183, 22, 0, 0, 'yes', 1, '16157468745951249321', '', 0, 'yes', 2, 'user', '2021-03-14 14:34:35', '2021-03-14 14:34:50'),
(179, 'PSB160', 1, 127, 'KRUTIL', 'V.', '9978660074', '', 'krutildhameliya0074@gmail.com', '47966', '', '1995-06-17', '', 'A+', 31, 'Surat', 31, '', 0, 'Surat', '', '12', 3, 'Palitana', 0, 65, 20, 0, 0, 'yes', 1, '16157492054781733740', '', 0, 'yes', 2, 'user', '2021-03-14 15:13:25', '2021-03-14 15:13:37'),
(180, 'PSB161', 1, 367, 'KALPESH BHAI', 'BABUBHAI', '9824910297', '', 'Kalpeshbhikadiya111@gmail.com', '50355', '', '1982-12-26', 'b3697e936a35cb5c98734d1218d4aa00data.png', 'AB+', 7, 'Cristal bilding nirmal nagar ', 0, '', 0, '', '', '9', 0, 'Bhavngar chandrdarsan sosayti plot no 90', 0, 61, 20, 0, 0, 'yes', 1, '16157732401287081286', '', 0, 'yes', 2, 'user', '2021-03-14 21:54:01', '2021-03-14 21:54:19'),
(181, 'PSB162', 1, 1042, 'Hitesh', 'Chhaganbhai', '9725625011', '', 'padhara.hitesh@gmail.com', '57741', '', '1988-08-01', '', 'O+', 1, '', 0, '', 0, '', '', 'M.com', 0, 'Kasturbadham- \nTramba-360020', 0, 175, 2, 0, 0, 'yes', 1, '16157742395510537374', '', 0, 'yes', 2, 'user', '2021-03-14 22:10:39', '2021-03-14 22:10:54'),
(182, 'PSB163', 1, 354, 'Yogesh', 'Vitthalbhai', '7984248411', '', '', '81938', '', '1994-03-16', '', 'O+', 6, 'Rajkot', 0, '', 0, '', '', 'Collage', 0, 'Plot area Kanpar', 0, 2, 2, 0, 0, 'yes', 1, '16157818231575742468', '', 0, 'yes', 2, 'user', '2021-03-15 00:17:04', '2021-03-15 00:17:17'),
(183, 'PSB164', 1, 1039, 'SAMIR', 'BHARAT', '9624162367', '', 'samir15121978@gmail.com', '41893', '', '1978-12-15', '51e25d2d2b18c8db84cda5db9ef71f62data.png', 'B+', 19, 'VAPI', 45, 'Panoli', 30, 'BALISANA Patan Gujarat', '', '8th', 0, 'Balisana Patan Gujarat', 0, 165, 23, 0, 0, 'yes', 1, '16157822087591419913', '', 0, 'yes', 2, 'user', '2021-03-15 00:23:28', '2021-03-15 00:23:49'),
(184, 'PSB165', 1, 354, 'Bhikhabhai', '', '9558532905', '', '', NULL, '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 20, 2, 2, 0, 500, 'yes', 1, '', '', 0, 'yes', 2, 'user', '2021-03-15 00:53:57', ''),
(185, 'PSB166', 1, 234, 'Dhirubhai', 'Karshanbhai', '9998552518', '', '', NULL, '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 20, 2, 2, 0, 500, 'yes', 1, '', '', 0, 'yes', 2, 'user', '2021-03-15 00:56:12', ''),
(186, 'PSB167', 1, 222, 'Rajeshbhai', 'Jivrajbhai', '9904198643', '', '', NULL, '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 56, 2, 2, 0, 1111, 'yes', 1, '', '', 0, 'yes', 2, 'user', '2021-03-15 00:59:28', ''),
(187, 'PSB168', 1, 19, 'Chhaganbhai', 'Popatbhai', '9998943409', '', '', NULL, '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 56, 2, 2, 0, 251000, 'yes', 1, '', '', 0, 'yes', 2, 'user', '2021-03-15 01:06:25', ''),
(188, 'PSB169', 0, 0, '', '', '9879779089', '', '', '73601', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 0, 0, 0, 0, 0, 'yes', 1, '16157861432041086817', '', 0, 'yes', -1, 'user', '2021-03-15 01:29:04', ''),
(189, 'PSB170', 1, 108, 'Rajeshbhai', 'Chhaganbhai', '9725250848', '', '', NULL, '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 56, 2, 2, 0, 500, 'yes', 1, '', '', 0, 'yes', 2, 'user', '2021-03-15 01:45:06', ''),
(190, 'PSB171', 0, 0, '', '', '9850226005', '', '', '20151', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 0, 0, 0, 0, 0, 'yes', 1, '16158042748645925351', '', 0, 'yes', -1, 'user', '2021-03-15 06:31:15', ''),
(191, 'PSB172', 0, 0, '', '', '9840258165', '', '', '62988', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 0, 0, 0, 0, 0, 'yes', 1, '16158057824834422039', '', 0, 'yes', -1, 'user', '2021-03-15 06:56:22', ''),
(192, 'PSB173', 2, 844, 'Rakesh', 'Mithalal', '9909551422', '', '', '99246', '', '2021-03-15', '', 'O+', 15, 'Ambavada', 56, ' ઉમિયા ધામ અંબાવાડા ના ટ્રસ્ટી ', 30, 'અંબાવાડા', '', 'Ba', 10, 'Ambavada 383210', 0, 182, 22, 0, 0, 'yes', 1, '16158059156034877007', '', 0, 'yes', 2, 'user', '2021-03-15 06:58:35', '2021-03-15 06:59:01'),
(193, 'PSB174', 1, 384, 'Surnsns', 'Nsnwk', '9116911687', '', 'Vikuseervi0@gmail.com', '21448', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, 'Ahmadabad', 0, 19, 28, 0, 0, 'yes', 1, '16158059883552686606', '', 0, 'yes', 2, 'user', '2021-03-15 06:59:48', '2021-03-15 07:00:03'),
(194, 'PSB175', 1, 335, 'Jaymin', 'Jayantilal', '8140309943', '', '', '63245', '', '2000-01-09', '', '', 6, '', 0, '', 0, '', '', 'Bba', 0, '150 feet ring road bihind tapovan school street no 2', 0, 175, 2, 0, 0, 'yes', 1, '16158118618868666692', '', 0, 'yes', 2, 'user', '2021-03-15 08:37:42', '2021-03-15 08:39:04'),
(195, 'PSB176', 2, 788, 'Vipul', 'Rameshbhai', '9067676702', '', 'Carecreation.cc@gmail.com', '31202', '', '1991-06-11', '', 'B+', 4, '', 0, 'Care mobile', 0, '', '', 'Ty b com', 0, 'Atkot ramji  mandir pase', 0, 2, 2, 0, 0, 'yes', 1, '16158121315670697176', '', 0, 'yes', 2, 'user', '2021-03-15 08:42:11', '2021-03-15 08:42:22'),
(196, 'PSB177', 1, 141, 'Ravjibhai', 'Rajabhai', '9924324201', '', '', '12345', '', '1971-10-10', '', 'AB+', 30, 'Atkot', 0, '', 0, '', '', 'SY B COM', 4, 'Kailash Nagar Atkot', 0, 2, 2, 0, 0, 'yes', 1, '16158148237834081236', '', 0, 'yes', 2, 'user', '2021-03-15 09:17:26', '2021-03-15 09:27:03'),
(197, 'PSB178', 1, 183, 'NARENDRABHAI', 'Vashrambhai', '9925140318', '', 'Contact@AJITAGENCY', '12345', '', '1976-07-17', '', 'O-', 1, 'Atkot', 1, 'Atkot', 0, '', '', '12', 0, 'Atkot', 0, 2, 2, 0, 0, 'yes', 1, '16158273719547460672', '', 0, 'yes', 2, 'user', '2021-03-15 09:25:13', '2021-03-15 12:56:11'),
(198, 'PSB179', 1, 414, 'RAJAN', 'Vinubhai', '7567557931', '', 'rajanchhayani123@gmail.com', '16945', '', '2000-07-11', '', 'B+', 43, 'Chitaliya kuva road k.k. garbi chowk', 0, '', 0, '', '', 'I T T', 0, 'Chitaliya kuva road k.k. garbi chowk j', 0, 2, 2, 0, 0, 'yes', 1, '16158160424136991911', '', 0, 'yes', 2, 'user', '2021-03-15 09:47:22', '2021-03-15 09:47:33'),
(199, 'PSB180', 0, 0, '', '', '7572908833', '', '', '18264', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 0, 0, 0, 0, 0, 'yes', 1, '16158229699730411021', '', 0, 'yes', -1, 'user', '2021-03-15 11:42:49', ''),
(200, 'PSB181', 1, 149, 'Paresh', 'Hasmukhbhai', '9879245694', '', '', '11655', '', '0000-00-00', '', 'B+', 55, 'Gondal', 0, '', 0, '', '', '12', 0, 'Patidad', 0, 3, 2, 0, 0, 'yes', 1, '16158271434958067452', '', 0, 'yes', 2, 'user', '2021-03-15 12:52:24', '2021-03-15 12:52:47'),
(201, 'PSB182', 3, 1043, 'DINESH', 'DAYABHAI', '9825901577', '', 'viraj. agro@yahoo.com', '31614', '', '1990-08-31', '', '', 5, 'VIRAJ AGRO CENTER\nNEAR ST BUS STOP. ATKOT', 0, 'VIRAJ AGRO CENTER\nNEAR ST BUS STOP. ATKOT', 0, '', '', 'B. Com', 0, 'Ghanshyam narar', 0, 2, 2, 0, 0, 'yes', 1, '16158281129877571613', '', 0, 'yes', 2, 'user', '2021-03-15 13:08:33', '2021-03-15 13:09:10'),
(202, 'PSB183', 1, 103, 'Ashish', 'Jentibhai', '8780545369', '', 'ashishpatel3540.ap@gmail.com', '75051', '', '1993-12-05', '', 'B+', 1, '', 1, '', 0, '', '', '12', 0, 'Ramnagar Kankot nyari dem pase', 0, 175, 2, 0, 0, 'yes', 1, '16158290469219596959', '', 0, 'yes', 2, 'user', '2021-03-15 13:24:06', '2021-03-15 13:24:22'),
(203, 'PSB184', 1, 383, 'RIKIN', 'RAMESHBHAI', '7016832166', '', 'rikinmalaviya20@gmail.com', '69169', '', '1992-06-12', '', 'B+', 23, 'Gundala Road,Gundala Chokdi,Near Ram Krushna Marbel,Gondal ', 0, '', 0, '', '', 'Master Of Science Information Technology And computer Application (MSCIT&CA)', 0, 'Shiv Shakti Bhuvan \nB/H Bus Stand\nLaxman Nagar Society\nGondal 360311', 0, 3, 2, 0, 0, 'yes', 1, '16158297031407066073', '', 0, 'yes', 2, 'user', '2021-03-15 13:35:03', '2021-03-15 13:35:14'),
(204, 'PSB185', 0, 0, '', '', '9824542103', '', '', '57634', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 0, 0, 0, 0, 0, 'yes', 1, '16158309694905801758', '', 0, 'yes', -1, 'user', '2021-03-15 13:56:10', ''),
(205, 'PSB186', 0, 0, '', '', '9723690902', '', '', '36565', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 0, 0, 0, 0, 0, 'yes', 1, '16158315779794483043', '', 0, 'yes', -1, 'user', '2021-03-15 14:06:18', ''),
(206, 'PSB187', 1, 141, 'Yagnik', 'Hareshbhai', '7359813073', '', '', '26044', '', '0000-00-00', '', '', 0, 'Surat', 0, 'Surat', 0, 'Surat', '', '', 0, 'Surat\n', 0, 63, 20, 0, 0, 'yes', 1, '16158708354925021831', '', 0, 'yes', 2, 'user', '2021-03-16 01:00:36', '2021-03-16 01:01:00'),
(207, 'PSB188', 1, 354, 'MUKESH', 'VALLABHBHAI', '9601920555', '', 'drmboghara@gmail. Com', '75543', '', '1985-09-20', '', 'O-', 0, '', 33, 'સત્યમ હોસ્પિટલ આટકોટ \nબસ સ્ટેન્ડ ની સામે \n', 0, '', '', 'BHMS', 0, 'SATYAM HOSPITAL \nOppo-bus stand \nATKOT', 0, 2, 2, 0, 5100, 'yes', 1, '16158747649163722672', '', 0, 'yes', 2, 'user', '2021-03-16 02:06:05', '2021-03-16 02:06:27'),
(208, 'PSB189', 1, 127, 'Vijay', 'Babubhai', '9714446831', '', 'vijaydhameliya149@gmail.com', '20859', '', '1990-11-30', '', 'B+', 8, 'At.kharchya jam', 30, '', 0, '', '', 'M.A ptc', 7, 'At.kharchiya jam sardar chock', 0, 2, 2, 0, 0, 'yes', 1, '16158804323097164978', '', 0, 'yes', 2, 'user', '2021-03-16 03:40:32', '2021-03-16 03:40:54'),
(209, 'PSB190', 1, 236, 'Vijaybhai', 'Kurjibhai', '9429096613', '', 'kishanpatel1613@gmail.com', '67013', '', '1974-11-08', '', 'B+', 27, 'Shree vivekanand vinay mandir\n1,kailasnagar \nJasdan-360050\nDi.-Rajkot', 11, 'Vivekanand hostel\n1,kailashnagar \nJasdan-360050\nDi.-Rajkot', 0, '', 'School', '12', 2, 'PRAMUKH SADAN\nVIVEKANAND NAGAR \nKHANPAR ROAD\nJASDAN-360050\n  DI.- RAJKOT', 0, 2, 2, 0, 0, 'yes', 1, '16158933417706536115', '', 0, 'yes', 2, 'user', '2021-03-16 07:15:42', '2021-03-16 07:15:54'),
(210, 'PSB191', 0, 0, '', '', '9925009645', '', '', '58446', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 0, 0, 0, 0, 0, 'yes', 1, '16159793305235140473', '', 0, 'yes', -1, 'user', '2021-03-17 07:08:51', ''),
(211, 'PSB192', 0, 0, '', '', '9879051944', '', '', '82186', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 0, 0, 0, 0, 0, 'yes', 1, '16159970361200176822', '', 0, 'yes', -1, 'user', '2021-03-17 12:03:57', ''),
(212, 'PSB193', 1, 414, 'Bharatbhai', 'B.', '9825865214', '', '', NULL, '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 56, 2, 2, 0, 350, 'yes', 1, '', '', 0, 'yes', 2, 'user', '2021-03-18 04:48:59', ''),
(213, 'PSB194', 1, 36, 'Vipulbhai', 'Batukbhai', '9824217901', '', '', '12345', '', '0000-00-00', '5f620d7b9611c6b6ccac20fb2f3794a5data.png', '', 0, '', 0, '', 0, '', '', '', 0, '', 56, 2, 2, 0, 1000, 'yes', 1, '16243799022958223618', '', 0, 'yes', 2, 'user', '2021-03-18 04:53:23', '2021-06-22 12:38:22'),
(214, 'PSB195', 1, 97, 'Dineshbhai', 'Narmda bio.chem.pvt.ltd.', '9727477700', '', '', NULL, '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 56, 2, 2, 0, 251000, 'yes', 1, '', '', 0, 'yes', 2, 'user', '2021-03-18 04:58:56', ''),
(215, 'PSB196', 1, 168, 'CHHABILBHAI', 'LALJIBHAI', '9824565891', '', 'chhabilkorat@gmail.com ', '74663', '', '1976-01-01', '5c1ed3d099e0fba6c0bb35d46a297f38data.png', 'A+', 27, '1, Kailash Nagar,  SHRI VIVEKANAND VINAY MANDIR - JASDAN', 0, '', 0, '', 'Edu', '12 PASS', 0, '301, Prasthan Residency, 1, Kailash Nagar, Jasdan', 56, 2, 2, 0, 0, 'yes', 1, '16160653784702102804', '', 0, 'yes', 2, 'user', '2021-03-18 07:02:58', '2021-03-18 07:03:13'),
(216, 'PSB197', 1, 55, 'Himanshu', 'Ratily', '6354162356', '', 'himanshuratilalrakholiya@gmail.com', '85250', '', '2002-09-04', '', 'B+', 0, '', 22, '', 22, '', '', '', 0, 'Maadrada,vallbhanagar, Gujarat', 0, 95, 14, 0, 0, 'yes', 1, '16160670121231643681', '', 0, 'yes', 2, 'user', '2021-03-18 07:30:12', '2021-03-18 07:30:25'),
(217, 'PSB198', 0, 0, '', '', '9426185662', '', '', '31895', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 0, 0, 0, 0, 0, 'yes', 1, '16160814061853805507', '', 0, 'yes', -1, 'user', '2021-03-18 11:30:07', ''),
(218, 'PSB199', 1, 317, 'Nilesh', 'Amarshibhai', '9427963358', '', '', '34623', '', '0000-00-00', '44b5f029bf1eb781b655024ec259472cdata.png', 'B+', 38, '', 38, '', 38, '', '', 'Sybcom', 0, 'Bh /Panchayat office', 0, 2, 2, 0, 0, 'yes', 1, '16160895204676486789', '', 0, 'yes', 2, 'user', '2021-03-18 13:45:21', '2021-03-18 13:45:44'),
(219, 'PSB200', 2, 1040, 'Akshay', 'Dineshbhai', '8320492578', '', 'akshaypatel8545@gmail.com', '89463', '', '1998-07-17', '', 'B+', 22, 'Navrangpura\n', 0, '', 0, '', '', '', 10, 'Ratanpura Gam Vastral Road Vastral Ahmedabad\n', 0, 17, 28, 0, 0, 'yes', 1, '16160937565326004400', '', 0, 'yes', 2, 'user', '2021-03-18 14:55:57', '2021-03-18 14:56:07'),
(220, 'PSB201', 0, 0, '', '', '9925455590', '', '', '50972', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 0, 0, 0, 0, 0, 'yes', 1, '16161485922598123296', '', 0, 'yes', -1, 'user', '2021-03-19 06:09:52', ''),
(221, 'PSB202', 2, 915, 'AshokKumar', 'Kantilal', '8758070510', '', 'ashokmanvar74@gmail.com', '12345', '', '0000-00-00', '', 'O+', 27, 'Deputy director of agriculture (Ext)\nJunagadh', 0, '', 0, '', '', 'Bsc(agri)', 0, '603, Shreenathji avenue\nOpp.Javiya school\nZanzarda, Junagadh', 0, 101, 14, 0, 0, 'yes', 1, '16162568027661743068', '', 0, 'yes', 2, 'user', '2021-03-19 08:48:27', '2021-03-20 12:13:22'),
(222, 'PSB203', 2, 916, 'Kiranben', 'Rajeshbhai', '9725393895', '', '', '56759', '', '0000-00-00', '', 'A+', 30, 'Agtarai', 30, 'Agtarai', 52, 'Agtarai', '', 'T y bcom', 4, 'Agtrai mota taver ni bajuma', 0, 100, 14, 0, 0, 'yes', 0, '16161718368133112135', '', 0, 'yes', 2, 'user', '2021-03-19 12:37:17', '2021-03-19 12:37:38'),
(223, 'PSB204', 0, 0, '', '', '9898870246', '', '', '19945', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 0, 0, 0, 0, 0, 'yes', 1, '16162518483730424473', '', 0, 'yes', -1, 'user', '2021-03-20 10:50:49', ''),
(224, 'PSB205', 0, 0, '', '', '9726222535', '', '', '52862', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 0, 0, 0, 0, 0, 'yes', 1, '16162594379862422482', '', 0, 'yes', -1, 'user', '2021-03-20 12:57:18', ''),
(225, 'PSB206', 2, 1040, 'Kamal', 'Hareshbhai', '7436002670', '', 'Patelkamal333999@gmail.com ', '23112', '', '2000-10-28', 'd79b5afda4bc38c0342c526135c0846cdata.png', 'B+', 16, 'Badayad', 16, 'Badayad', 16, 'Badayad', '', 'iti', 0, 'Lakshmipura', 0, 257, 26, 0, 0, 'yes', 1, '16163286875077493583', '', 0, 'yes', 2, 'user', '2021-03-21 08:11:28', '2021-03-21 08:11:51'),
(226, 'PSB207', 1, 183, 'Nishit', 'Damjibhai', '9825436672', '', 'khuntn1@gmail.com', '15610', '', '1991-08-18', '', 'A+', 22, 'Vadodara', 0, '', 0, '', '', 'B.Tech', 0, '15/A Paras nagar society\nAnand mahal road\nAdajan\nSurat', 0, 207, 9, 0, 0, 'yes', 1, '16163361336394944792', '', 0, 'yes', 2, 'user', '2021-03-21 10:15:33', '2021-03-21 10:15:49'),
(227, 'PSB208', 0, 0, '', '', '9978969885', '', '', '24731', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 0, 0, 0, 0, 0, 'yes', 1, '16164008443305798338', '', 0, 'yes', -1, 'user', '2021-03-22 04:14:05', ''),
(228, 'PSB209', 2, 1044, 'Utpal', 'Manharbhai', '9664719575', '', 'patelutpal248@gmail.com', '49733', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, 'Rajpur', 0, 248, 30, 0, 0, 'yes', 1, '16164011988303180099', '', 0, 'yes', 2, 'user', '2021-03-22 04:19:59', '2021-03-22 04:20:40'),
(229, 'PSB210', 0, 0, '', '', '6355455885', '', '', '21153', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 0, 0, 0, 0, 0, 'yes', 1, '16164048804889303087', '', 0, 'yes', -1, 'user', '2021-03-22 05:21:20', ''),
(230, 'PSB211', 0, 0, '', '', '6355455882', '', '', '28170', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 0, 0, 0, 0, 0, 'yes', 1, '16164049199206416378', '', 0, 'yes', -1, 'user', '2021-03-22 05:22:00', ''),
(231, 'PSB212', 2, 799, 'Ankit', 'Hasmukh', '9724351454', '', 'patelankit143143@gmail.com', '92442', '', '0000-00-00', '', 'B+', 1, '', 0, '', 0, '', '', '', 0, 'Hari om park ghuntu', 0, 194, 13, 0, 0, 'yes', 1, '16164263555494003573', '', 0, 'yes', 2, 'user', '2021-03-22 11:19:15', '2021-03-22 11:19:30'),
(232, 'PSB213', 1, 1039, 'Mayurbhai', 'Keshavlal', '9714110909', '', '', '36399', '', '0000-00-00', '', 'O+', 30, 'Unava', 30, '', 0, '', '', 'Bsc', 0, 'Sector 22', 0, 106, 27, 0, 0, 'yes', 1, '16164985967011861364', '', 0, 'yes', 2, 'user', '2021-03-23 07:23:17', '2021-03-23 07:23:30'),
(233, 'PSB214', 0, 0, '', '', '9913764305', '', '', '76517', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 0, 0, 0, 0, 0, 'yes', 1, '16165095071355211587', '', 0, 'yes', -1, 'user', '2021-03-23 10:25:08', ''),
(234, 'PSB215', 1, 19, 'Naresh bhai', 'સવજીભાઈ', '6351772712', '', 'hevikkakadiya16@gmail.com', '12345', '', '0000-00-00', '', 'A+', 0, '', 0, '', 7, '', '', '', 4, 'Hbukvad', 0, 63, 20, 0, 0, 'yes', 1, '16165105667055999032', '', 0, 'yes', 2, 'user', '2021-03-23 10:34:38', '2021-03-23 10:42:46'),
(235, 'PSB216', 0, 0, '', '', '9924218826', '', '', '60872', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 0, 0, 0, 0, 0, 'yes', 1, '16165119766085820388', '', 0, 'yes', -1, 'user', '2021-03-23 11:06:17', ''),
(236, 'PSB217', 1, 121, 'L ', 'B', '9428156731', '', '', NULL, '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, 'ભંડારીયા', 24, 2, 2, 0, 11000, 'yes', 1, '', '', 0, 'yes', 2, 'user', '2021-03-23 12:35:06', ''),
(237, 'PSB218', 2, 1040, 'HARSH', 'Kanubhai', '9898220082', '', 'harshpatelg030@gmail.com', '37705', '', '1996-11-30', '', 'B+', 45, '', 30, '', 0, '', '', 'Agriculture', 0, 'At&Po Tajpur', 0, 182, 22, 0, 0, 'yes', 1, '16165209225934145203', '', 0, 'yes', 2, 'user', '2021-03-23 13:35:23', '2021-03-23 13:35:40'),
(238, 'PSB219', 1, 19, 'Ritesh', 'Vithalbhai', '9924578803', '', 'ritesh.patel155@gmail.com', '12345', '', '1989-06-20', '5480456402c23babf2b11536f46a6df2data.png', '', 54, 'Parth Handicraft  Rameshwar Nagar Gokhlna Road Jasdsn', 0, '', 0, '', '', '10', 0, 'Rameshwar Nagar Gokhlna Road Jasdsn', 0, 2, 2, 0, 0, 'yes', 1, '16165251233984712009', '', 0, 'yes', 2, 'user', '2021-03-23 14:26:37', '2021-03-23 14:45:23'),
(239, 'PSB220', 1, 141, 'Dilip', 'MaganBhai', '9624649018', '', '', '67819', '', '1990-10-03', '', '', 54, 'Gokhlana road rameshver nagar', 0, '', 0, '', '', 'Bcom', 7, 'Gokhlana road rameshvar nagar jasdan', 0, 2, 2, 0, 0, 'yes', 1, '16165385602158476829', '', 0, 'yes', 2, 'user', '2021-03-23 18:29:21', '2021-03-23 18:29:49'),
(240, 'PSB221', 1, 141, 'Vallabhbhai', 'Lakhabhai', '9924171040', '', 'Zeelvallbh', '75419', '', '2021-03-24', '', 'O+', 54, 'Latiplot sen10 grbisok', 54, 'Latiplot sen10 grbisok', 54, 'Latiplot sen10 grbisok', '', '', 0, 'Latiplot sen 10 grbi sok', 0, 2, 2, 0, 0, 'yes', 1, '16165588497219721717', '', 0, 'yes', 2, 'user', '2021-03-24 00:07:30', '2021-03-24 00:07:46'),
(241, 'PSB222', 1, 81, 'Jaydeep', 'Pravinbhai', '7567868528', '', 'Jaydiplimbasiya786@gmail.com', '34766', '', '0000-00-00', '0691a83104c9b85a7640be92870845c7data.png', '', 31, 'Dharati veterinary clinic chitaliya road k. K garbi chawk', 0, '', 0, '', '', '', 0, 'Chitaliya road k. K garbi chawk', 56, 2, 2, 0, 0, 'yes', 1, '16165673609287243126', '', 0, 'yes', 2, 'user', '2021-03-24 02:29:21', '2021-03-24 02:29:35'),
(242, 'PSB223', 0, 0, '', '', '9016826268', '', '', '96122', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 0, 0, 0, 0, 0, 'yes', 1, '16165740248752878213', '', 0, 'yes', -1, 'user', '2021-03-24 04:20:25', ''),
(243, 'PSB224', 1, 221, 'वीमल', 'रणछोड़भाई', '9727383839', '', '', '40921', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, 'भडारीया', 0, 178, 2, 0, 0, 'yes', 1, '16165902275458320751', '', 0, 'yes', 2, 'user', '2021-03-24 08:50:28', '2021-03-24 08:50:47'),
(244, 'PSB225', 1, 121, 'Bhavesh', 'Rameshbhai', '9773461376', '', 'bhavesh92.desai@gmail.com', '87645', '', '1992-10-30', '', 'O+', 6, 'I Khodal Power press work\nSed No.15 varun industry sery No.6 kothariya ring road\n', 1, 'Rajkot', 0, '', '', '10', 0, 'Junu gam\n', 0, 0, 22, 0, 0, 'yes', 1, '16165902906323573097', '', 0, 'yes', 2, 'user', '2021-03-24 08:51:30', '2021-03-24 08:51:44'),
(245, 'PSB226', 1, 121, 'Bharat', 'Jivanbhai', '9974054349', '', '', '12345', '', '0000-00-00', '6ec9ec7efdcf28456f5b6db07dab3173data.png', 'B+', 30, '', 0, 'To bhandariya ta jasdan dis.rajkot', 0, '', '', 'T.y bcom', 0, 'To bhandari ta. Jasda dis rajkor', 0, 2, 2, 0, 0, 'yes', 1, '16165928723675091542', '', 0, 'yes', 2, 'user', '2021-03-24 08:51:52', '2021-03-24 09:34:32'),
(246, 'PSB227', 0, 0, '', '', '9510066697', '', '', '30497', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 0, 0, 0, 0, 0, 'yes', 1, '16165906047241851401', '', 0, 'yes', -1, 'user', '2021-03-24 08:56:44', ''),
(247, 'PSB228', 1, 316, 'mitesh', 'jayntibhai', '9265458264', '', '2002mithirani@gmail.com', '51971', '', '2000-03-17', '', 'B+', 43, 'khothariya rajkot', 0, '', 0, '', '', 'ty', 8, 'kharachiya jam', 0, 2, 2, 0, 0, 'yes', 1, '16165987968469668909', '', 0, 'yes', 2, 'user', '2021-03-24 11:13:16', '2021-03-24 11:13:30'),
(248, 'PSB229', 1, 355, 'Ketan', 'Rameshbhai', '7600132558', '', 'ketanborad007@gmail.com', '12345', '', '0000-00-00', '', 'AB+', 13, '', 0, '', 0, '', '', '', 0, 'Bhandariya', 0, 2, 2, 0, 0, 'yes', 1, '16166003748018356269', '', 0, 'yes', 2, 'user', '2021-03-24 11:26:04', '2021-03-24 11:39:34'),
(249, 'PSB230', 2, 903, 'Divyang', 'Kishorbhai', '7990899199', '', 'divyang.makhansa777@gmail.com', '27941', '', '2002-01-19', '43004f1ba0cd07d31c310e241410831adata.png', 'B+', 0, '', 0, '', 0, '', '', 'Polytechnic in agriculture sub branch in navsari agricultural university', 0, 'Block no.A/101, sampati gold appartement, dipanjali -2 , giriraj park, timbavadi , junagadh- 362015', 0, 0, 14, 0, 0, 'yes', 1, '16166547307799172410', '', 0, 'yes', 2, 'user', '2021-03-25 02:45:31', '2021-03-25 02:45:50'),
(250, 'PSB231', 1, 141, 'Bhavesh', 'Pragjibhai', '9909567143', '', 'Bhavesh Vaghasiya ', '52120', '', '1979-05-20', '', 'A+', 30, 'Jasdan ', 54, 'Jay verai hendicraft \nAadamji Rod Ram para, Jasdan ', 0, '', '', '1.', 0, 'Admji road rampara Jasdan ', 0, 2, 2, 0, 0, 'yes', 1, '16166577946219902885', '', 0, 'yes', 2, 'user', '2021-03-25 03:36:34', '2021-03-25 03:36:45'),
(251, 'PSB232', 1, 303, 'Ashvin(Munnabhai)', 'Sbhubhai', '9427254017', '', '', '27321', '', '1982-09-07', '', 'O+', 54, 'Latiplot mein bjar jasdan', 19, '', 38, 'Jasdan', '', '10', 0, 'Khanpar rod vrudavan sosaity', 0, 0, 2, 0, 0, 'yes', 1, '16166709793685286275', '', 0, 'yes', 2, 'user', '2021-03-25 07:16:19', '2021-03-25 07:16:44'),
(252, 'PSB233', 1, 1045, 'Darpan', 'Rashikbhai', '8866479534', '', 'Darpanpatel9534@gmail.com', '51476', '', '1993-11-25', '', 'O+', 3, 'Patel bakery & cake shop \n\nSatyam Park Gate Sardar Chowk kothariya Rajkot', 15, 'Tirupati society near Brahmani hall kothariya Chokdi Rajkot', 1, 'Ram Vijay dairy farm gundavadi Hospital Rajkot', '', '12th commerce', 0, 'Ram park - 2 near Ramnath weighbridge National Highway Aji river Rajkot', 0, 175, 2, 0, 0, 'yes', 1, '16166979185774973493', '', 0, 'yes', 2, 'user', '2021-03-25 14:45:19', '2021-03-25 14:45:40'),
(253, 'PSB234', 1, 1039, 'Karshanbhai', 'Patel', '9898400674', '', '', NULL, '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, 'Kalol', 0, 105, 27, 0, 10000, 'yes', 1, '', '', 0, 'yes', 2, 'user', '2021-03-26 01:54:24', ''),
(254, 'PSB235', 0, 0, '', '', '9904199265', '', '', '92118', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 0, 0, 0, 0, 0, 'yes', 1, '16167404752902306290', '', 0, 'yes', -1, 'user', '2021-03-26 02:34:36', ''),
(255, 'PSB236', 1, 317, 'Gopalbhai', 'Popatbhai', '9428412914', '', 'Hirparagopalbhai12@gmail. Com', '12345', '', '0000-00-00', '', '', 30, '', 0, '', 0, '', '', '', 0, 'Nandana', 0, 86, 15, 0, 0, 'yes', 1, '16167410427208651642', '', 0, 'yes', 2, 'user', '2021-03-26 02:38:20', '2021-03-26 02:44:02'),
(256, 'PSB237', 2, 1040, 'Meenaben', 'Ramesh bhai', '9724140148', '', '', '97200', '', '1979-12-14', '', 'O-', 0, '', 0, '', 0, '', '', 'Ba', 0, 'Morbi Rajnager\nPanchasar road', 0, 194, 13, 0, 0, 'yes', 0, '16167607201526633434', '', 0, 'yes', 2, 'user', '2021-03-26 08:12:01', '2021-03-26 08:12:15'),
(257, 'PSB238', 0, 0, '', '', '9925001214', '', '', '83309', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 0, 0, 0, 0, 0, 'yes', 1, '16167862757158747808', '', 0, 'yes', -1, 'user', '2021-03-26 15:17:56', ''),
(258, 'PSB239', 2, 470, 'Miral', 'Rameshbhai', '9924409998', '', 'miral.agola@gmail.com', '19344', '', '0000-00-00', '', '', 1, 'Chachavadarada', 0, '', 0, '', '', 'B.com', 7, 'Swarg Apartment, Shyam Park, Avani Cross Road, Sanala Road, Morbi', 0, 194, 13, 0, 0, 'yes', 1, '16167864651980887492', '', 0, 'yes', 2, 'user', '2021-03-26 15:21:06', '2021-03-26 15:21:18'),
(259, 'PSB240', 1, 160, 'BHARAT', 'TARSIBHAI', '6351673417', '', 'bharatshingala82@gmail.com', '75165', '', '1983-04-05', 'e3bd86f3cdfd3a10dd89ca8e0b6d654bdata.png', 'A+', 6, 'Out door', 0, '', 0, '', '', 'B com', 0, 'Creative structure \n\nC/o dhara motors ,dhebar road ,aarti society near somnath hall rajkot', 0, 0, 2, 0, 0, 'yes', 1, '16168223319535862757', '', 0, 'yes', 2, 'user', '2021-03-27 01:18:51', '2021-03-27 01:19:20'),
(260, 'PSB241', 2, 976, 'Kishorbhai', 'Kanjibhai', '9723655155', '', '', '62825', '', '1966-06-19', '', 'O-', 45, 'Gadhdha road jasdan', 0, '', 0, '', '', '10', 7, 'Lati plot main road jasdan\nNear Kadva patel samaj ', 0, 2, 2, 0, 0, 'yes', 1, '16168525442031353715', '', 0, 'yes', 2, 'user', '2021-03-27 09:42:24', '2021-03-27 09:42:35'),
(261, 'PSB242', 1, 354, '‌નટુભાઈ', 'ગોરધનભાઈ', '9725460971', '', '', '12345', '', '0000-00-00', '', '', 23, 'જસદણ\n', 0, '', 0, '', '', '', 0, 'ચિતલિયા રોડ રણછોડ નગર\n', 0, 177, 2, 0, 0, 'yes', 1, '16247713059872938421', '', 0, 'yes', 2, 'user', '2021-03-27 11:50:04', '2021-06-27 01:21:45'),
(262, 'PSB243', 1, 57, 'Dilip', 'Khimjibhai', '9825133038', '', 'Krishnasankul6@gmail.com', '67664', '', '1976-07-27', '', 'O+', 11, 'Chitaliyrode', 11, 'Chitaliyarode', 56, 'Jasdan', '', 'Ll.b', 2, 'Chitaliyaroad', 0, 2, 2, 0, 0, 'yes', 1, '16169077642882118130', '', 0, 'yes', 2, 'user', '2021-03-28 01:02:45', '2021-03-28 01:02:57'),
(263, 'PSB244', 1, 414, 'Bharat Bhai', 'Ladha Bhai', '9426443296', '', '', NULL, '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, 'Jasdan', 56, 2, 2, 0, 0, 'yes', 1, '', '', 0, 'yes', 2, 'user', '2021-03-28 01:07:29', ''),
(264, 'PSB245', 1, 219, 'MAHESH', 'DHIRAJBHAI', '9537388981', '', 'maheshsatasiya86@gmail.com', '12345', '12345', '1985-01-11', '', 'A+', 27, 'Teacher', 0, '', 0, '', '', 'PTC B.A.', 2, 'Sajadiyali', 0, 179, 2, 0, 2100, 'yes', 1, '16235037518960914074', '', 0, 'yes', 2, 'city_admin', '2021-03-31 08:48:11', '2021-06-12 09:15:51'),
(265, 'PSB246', 0, 0, '', '', '9426259772', '', '', '68027', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 0, 0, 0, 0, 0, 'yes', 1, '16174444956639692796', '', 0, 'yes', -1, 'user', '2021-04-03 06:08:16', ''),
(266, 'PSB247', 0, 0, '', '', '9913361192', '', '', '80374', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 0, 0, 0, 0, 0, 'yes', 1, '16174581765151845987', '', 0, 'yes', -1, 'user', '2021-04-03 09:56:16', ''),
(267, 'PSB248', 0, 0, '', '', '7567102102', '', '', '75206', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 0, 0, 0, 0, 0, 'yes', 1, '16176445198436310698', '', 0, 'yes', -1, 'user', '2021-04-05 13:42:00', ''),
(268, 'PSB249', 0, 0, '', '', '9067901982', '', '', '11071', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 0, 0, 0, 0, 0, 'yes', 1, '16176455871615914949', '', 0, 'yes', -1, 'user', '2021-04-05 13:59:47', ''),
(269, 'PSB250', 0, 0, '', '', '9426035602', '', '', '12526', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 0, 0, 0, 0, 0, 'yes', 1, '16176476598163403122', '', 0, 'yes', -1, 'user', '2021-04-05 14:34:20', ''),
(270, 'PSB251', 0, 0, '', '', '8200759119', '', '', '13037', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 0, 0, 0, 0, 0, 'yes', 1, '16176477115550028454', '', 0, 'yes', -1, 'user', '2021-04-05 14:35:12', ''),
(271, 'PSB252', 0, 0, '', '', '8401602600', '', '', '99046', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 0, 0, 0, 0, 0, 'yes', 1, '16176510172622492536', '', 0, 'yes', -1, 'user', '2021-04-05 15:30:18', ''),
(272, 'PSB253', 0, 0, '', '', '7046429597', '', '', '88748', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 0, 0, 0, 0, 0, 'yes', 1, '16176649904317495251', '', 0, 'yes', -1, 'user', '2021-04-05 19:23:11', ''),
(273, 'PSB254', 0, 0, '', '', '9925121844', '', '', '29208', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 0, 0, 0, 0, 0, 'yes', 1, '16176746255738500954', '', 0, 'yes', -1, 'user', '2021-04-05 22:03:46', ''),
(274, 'PSB255', 0, 0, '', '', '9558986222', '', '', '53986', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 0, 0, 0, 0, 0, 'yes', 1, '16176769364437539933', '', 0, 'yes', -1, 'user', '2021-04-05 22:42:17', ''),
(275, 'PSB256', 0, 0, '', '', '9574614450', '', '', '37044', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 0, 0, 0, 0, 0, 'yes', 1, '16176799249597676727', '', 0, 'yes', -1, 'user', '2021-04-05 23:32:05', ''),
(276, 'PSB257', 0, 0, '', '', '9925153453', '', '', '77004', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 0, 0, 0, 0, 0, 'yes', 1, '16176823285341261091', '', 0, 'yes', -1, 'user', '2021-04-06 00:12:08', ''),
(277, 'PSB258', 0, 0, '', '', '9638540300', '', '', '92837', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 0, 0, 0, 0, 0, 'yes', 1, '16176837706532842723', '', 0, 'yes', -1, 'user', '2021-04-06 00:36:10', ''),
(278, 'PSB259', 0, 0, '', '', '8000010037', '', '', '82065', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 0, 0, 0, 0, 0, 'yes', 1, '16176839184121129597', '', 0, 'yes', -1, 'user', '2021-04-06 00:38:38', ''),
(279, 'PSB260', 0, 0, '', '', '9727893633', '', '', '59510', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 0, 0, 0, 0, 0, 'yes', 1, '16176866562482498841', '', 0, 'yes', -1, 'user', '2021-04-06 01:24:17', ''),
(280, 'PSB261', 0, 0, '', '', '9925803357', '', '', '20252', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 0, 0, 0, 0, 0, 'yes', 1, '16176938351233577338', '', 0, 'yes', -1, 'user', '2021-04-06 03:23:56', ''),
(281, 'PSB262', 0, 0, '', '', '6355120002', '', '', '14074', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 0, 0, 0, 0, 0, 'yes', 1, '16176940055338521018', '', 0, 'yes', -1, 'user', '2021-04-06 03:26:45', ''),
(282, 'PSB263', 0, 0, '', '', '8140285632', '', '', '75640', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 0, 0, 0, 0, 0, 'yes', 1, '16176942897440902524', '', 0, 'yes', -1, 'user', '2021-04-06 03:31:30', ''),
(283, 'PSB264', 0, 0, '', '', '7228808550', '', '', '64302', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 0, 0, 0, 0, 0, 'yes', 1, '16176990678025360325', '', 0, 'yes', -1, 'user', '2021-04-06 04:51:09', ''),
(284, 'PSB265', 0, 0, '', '', '8000003078', '', '', '27071', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 0, 0, 0, 0, 0, 'yes', 1, '16177150652063557306', '', 0, 'yes', -1, 'user', '2021-04-06 09:17:46', ''),
(285, 'PSB266', 0, 0, '', '', '9429500113', '', '', '43666', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 0, 0, 0, 0, 0, 'yes', 1, '16177257666124417204', '', 0, 'yes', -1, 'user', '2021-04-06 12:16:06', ''),
(286, 'PSB267', 0, 0, '', '', '9904136133', '', '', '72631', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 0, 0, 0, 0, 0, 'yes', 1, '16177298669996814080', '', 0, 'yes', -1, 'user', '2021-04-06 13:24:27', ''),
(287, 'PSB268', 0, 0, '', '', '9373611825', '', '', '92665', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 0, 0, 0, 0, 0, 'yes', 1, '16177905589704774010', '', 0, 'yes', -1, 'user', '2021-04-07 06:15:59', ''),
(288, 'PSB269', 0, 0, '', '', '9574614342', '', '', '50071', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 0, 0, 0, 0, 0, 'yes', 1, '16177923867888302279', '', 0, 'yes', -1, 'user', '2021-04-07 06:46:26', ''),
(289, 'PSB270', 0, 0, '', '', '9904253497', '', '', '82122', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 0, 0, 0, 0, 0, 'yes', 1, '16178048448579760309', '', 0, 'yes', -1, 'user', '2021-04-07 10:14:05', ''),
(290, 'PSB271', 0, 0, '', '', '9998881426', '', '', '86959', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 0, 0, 0, 0, 0, 'yes', 1, '16178488299463536495', '', 0, 'yes', -1, 'user', '2021-04-07 22:27:10', ''),
(291, 'PSB272', 0, 0, '', '', '9427021282', '', '', '18363', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 0, 0, 0, 0, 0, 'yes', 1, '16178647887580307554', '', 0, 'yes', -1, 'user', '2021-04-08 02:53:09', ''),
(292, 'PSB273', 0, 0, '', '', '9714237332', '', '', '24372', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 0, 0, 0, 0, 0, 'yes', 1, '16178864739497651040', '', 0, 'yes', -1, 'user', '2021-04-08 08:54:34', ''),
(293, 'PSB274', 0, 0, '', '', '8200154327', '', '', '12345', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 0, 0, 0, 0, 0, 'yes', 1, '16180396633183939796', '', 0, 'yes', -1, 'user', '2021-04-10 03:27:43', ''),
(294, 'PSB275', 0, 0, '', '', '9624667104', '', '', '12345', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 0, 0, 0, 0, 0, 'yes', 1, '16180402185489773850', '', 0, 'yes', -1, 'user', '2021-04-10 03:36:58', ''),
(295, 'PSB276', 0, 0, '', '', '9228181603', '', '', '12345', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 0, 0, 0, 0, 0, 'yes', 1, '16180711759543150402', '', 0, 'yes', -1, 'user', '2021-04-10 12:12:55', ''),
(296, 'PSB277', 2, 1003, 'Batukbhai', 'Jivan bhai', '7283944576', '', '', '12345', '', '1963-01-08', '', 'A+', 32, 'Bharaj  complex,no 2 near court l,limbdi', 0, '', 0, '', '', 'BALLB', 0, 'Bhalgamada gate,patel street,limbdi\n', 0, 211, 11, 0, 0, 'yes', 1, '16180712776902022181', '', 0, 'yes', 2, 'user', '2021-04-10 12:14:37', '2021-04-10 12:14:43'),
(297, 'PSB278', 2, 841, 'Hiteshbhai', 'Jentibhai', '9825170369', '', 'navrangstationers76@gmail.com', '12345', '', '1981-11-30', '', 'B+', 21, 'Samat road\nB/h new bus stand\nJasdan', 0, '', 0, '', '', '10', 0, 'Chitaliya road, shaktinagar\nJasdan', 0, 2, 2, 0, 0, 'yes', 1, '16181288731413847657', '', 0, 'yes', 2, 'user', '2021-04-11 04:14:33', '2021-04-11 04:14:44'),
(298, 'PSB279', 2, 976, 'Chiragbhai', 'Dhirubhai', '9427355891', '', '', '12345', '', '0000-00-00', '', 'B+', 53, '', 0, '', 0, '', '', 'B.com', 0, 'Jasdan', 0, 2, 2, 0, 0, 'yes', 1, '16230813427106157883', '', 0, 'yes', 2, 'user', '2021-04-11 05:43:38', '2021-06-07 11:55:42'),
(300, 'PSB281', 1, 166, 'Hiren Bhai', 'Valji Bhai', '9825835688', '', '', '12345', '', '2021-04-06', '', 'B+', 11, '', 0, '', 0, '', '', '12', 0, 'Jasdan', 56, 2, 2, 50000, 50000, 'yes', 1, '16182266726585985356', '', 0, 'yes', 2, 'user', '2021-04-12 07:21:58', '2021-04-12 07:24:32'),
(301, 'PSB282', 1, 60, 'Hiralben', 'Sureshbhai', '7874546115', '', '', NULL, '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, 'Shivrajpur', 50, 2, 2, 0, 0, 'yes', 1, '', '', 0, 'yes', 2, 'user', '2021-04-21 04:26:06', ''),
(302, 'PSB283', 1, 329, 'TEST', 'wewq', '8128500517', '', 'te@fs.com', '12345', '', '2021-04-29', '', 'A-', 13, 'abcd', 0, '', 0, '', '', 'Test', 7, 'Test', 0, 21, 21, 0, 0, 'yes', 1, '16196773608460406957', '', 0, 'yes', 3, 'user', '2021-04-29 02:22:40', '2021-04-29 02:22:46'),
(303, 'PSB284', 1, 38, 'BRIJESH', 'GOVIND BHAI', '9574985688', '', 'brijeshtimbadiya1991@gmail.com', '12345', '', '1991-11-09', '6a9bf7d95cbfb3b71595ce1ab0f08c8edata.png', 'O+', 1, 'New Market yard,jasdan.', 0, 'By pass road,apmc jasdan.', 0, '', '', '12 pass', 0, 'GAYTRI NAGAR,NEW RAVTI CHOWK, SHIVRAJPUR\n', 0, 2, 2, 0, 0, 'yes', 1, '16197062446706902702', '', 0, 'yes', 2, 'user', '2021-04-29 10:24:04', '2021-04-29 10:24:13'),
(304, 'PSB285', 1, 64, 'Pradip', 'Raghavbhai', '9099082482', '', 'Pradeep.rupareliya@gmail.com', '12345', '', '1990-09-30', '95befda92509784dc14c2ec900f5cc77data.png', '', 14, 'Atmiya Fashion - Adamaji road near rudra hanuman dada dairy jasdan', 0, '', 0, '', '', '12 pass', 0, 'Chitaliya road laxman nagar jasdan', 56, 2, 2, 0, 0, 'yes', 1, '16197063272266046749', '', 0, 'yes', 2, 'user', '2021-04-29 10:25:27', '2021-04-29 10:25:31'),
(305, 'PSB286', 1, 317, 'Jay', 'Nileshbhai ', '8000520520', '', 'Patelprince00770@gmail.com', '12345', '', '1996-03-15', 'f1ac1d485c0b1ec2e4a72d6caf27d56adata.png', 'B+', 23, 'Gadhdiya bay pass chokdi ', 0, '', 0, '', '', 'B. Com ', 0, 'Chitaliya kuva road khothi nala pase. Khodiyar mandap sarvish ni same', 0, 2, 2, 0, 0, 'yes', 1, '16197064087453129316', '', 0, 'yes', 2, 'user', '2021-04-29 10:26:48', '2021-04-29 10:26:55'),
(306, 'PSB287', 1, 81, 'Varun', 'Jayantibhai', '9979474332', '', 'varunp456@gmail.com', '12345', '', '1995-05-29', '', 'A+', 30, 'Jasdan', 5, 'Jasdan', 0, 'Jasdan', '', 'Graduate B.Com', 4, 'Jasdan', 0, 2, 2, 0, 0, 'yes', 1, '16197066534325934744', '', 0, 'yes', 2, 'user', '2021-04-29 10:30:53', '2021-04-29 10:30:58'),
(307, 'PSB288', 1, 141, 'NIRAJ', 'NITINBHAI', '9558375143', '', 'Patelnirajram32@gmail.com', '12345', '', '1997-02-03', '8182db4b99bab575ae8f278f0edfa60cdata.png', 'B-', 6, 'Lati plot main road Jasdan ', 0, '', 0, '', '', 'Civil engineer ', 0, 'Shree Ram Machinery Store, Lati plot main road Jasdan ', 0, 2, 2, 0, 0, 'yes', 1, '16215604319081312032', '', 0, 'yes', 2, 'user', '2021-04-29 10:35:13', '2021-05-20 21:27:11'),
(308, 'PSB289', 1, 363, 'Er. Gaurav', 'Meghjibhai', '7043572272', '', 'Gauravmbhayani@gmail.cim', '12345', '', '1999-02-16', '7b6f9cdcce12b96f856aff3658a18975data.png', 'B+', 22, 'BHUMI PLANNING & DESIGNING', 0, '', 0, '', '', 'B. E. CIVIL', 0, 'Shaktinagar , chitaliya road', 0, 2, 2, 0, 0, 'yes', 1, '16197073848517146071', '', 0, 'yes', 2, 'user', '2021-04-29 10:43:04', '2021-04-29 10:43:08'),
(309, 'PSB290', 1, 108, 'Ramesh', 'BATUKBHAI', '9638349192', '', 'jaybhavani1111.jb@gmail.com', '12345', '', '1986-06-18', '', 'B+', 54, '10 LATIPLOT JASDAN', 36, 'Paver ends tries rajkot', 0, '', '', 'Ty ', 3, '10 LATIPLOT JASDAN', 0, 2, 2, 0, 0, 'yes', 1, '16197084798331081653', '', 0, 'yes', 2, 'user', '2021-04-29 11:01:19', '2021-04-29 11:01:30'),
(310, 'PSB291', 1, 108, 'hasmukhbhai', 'valjibhai', '9924038373', '', '', '12345', '', '1969-06-15', '602251d25f9dd18fb9f41896c9c00d4bdata.png', 'O+', 0, '', 54, 'samat rod patidar komlex', 54, 'samat rod patida komplex', '', '6', 0, 'gakhalana rod karisna nagar', 0, 2, 2, 0, 0, 'yes', 1, '16229791928749296746', '', 0, 'yes', 2, 'user', '2021-04-29 11:04:04', '2021-06-06 07:33:12'),
(311, 'PSB292', 0, 0, '', '', '9978272227', '', '', '12345', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 0, 0, 0, 0, 0, 'yes', 1, '16197155444972655558', '', 0, 'yes', -1, 'user', '2021-04-29 12:59:04', ''),
(312, 'PSB293', 1, 214, 'Ashokbhai', 'Kantibhai', '9106998195', '', '', '12345', '', '1975-03-16', '', 'B-', 45, 'India iron works\nGitanagar jasdan\n', 0, '', 0, '', '', 'B com', 0, 'Gitanagar near New Bus stend', 0, 2, 2, 0, 0, 'yes', 1, '16197157878892662878', '', 0, 'yes', 2, 'user', '2021-04-29 13:03:07', '2021-04-29 13:03:21'),
(313, 'PSB294', 1, 317, 'Ashokbhai', 'Keshubhai', '9924961119', '', 'hirparaashvin1988@gmail.com', '12345', '', '1988-05-15', '', '', 45, 'Atkor rod baypass circle. BH. Dharti honda show room.. ', 0, '', 0, '', '', '12', 0, 'Atkot rod baypass circle vadlavadi. ', 0, 2, 2, 0, 0, 'yes', 1, '16197178669110286715', '', 0, 'yes', 2, 'user', '2021-04-29 13:37:46', '2021-04-29 13:38:06'),
(314, 'PSB295', 1, 40, 'Suresh', 'Vallabhbhai', '9558178222', '', '', '12345', '', '1985-10-18', '', '', 30, '', 0, '', 0, '', '', '10', 0, 'Bhandariya\n', 0, 2, 2, 0, 0, 'yes', 1, '16197639207583760496', '', 0, 'yes', 2, 'user', '2021-04-30 02:25:20', '2021-04-30 02:25:45'),
(315, 'PSB296', 1, 442, 'Pragnesh', 'Ravjibhai', '9428265007', '', '', '12345', '', '1974-01-13', '', '', 4, ' Narayan electricals Near old bus stop', 0, '', 0, '', '', '12', 0, 'Shiv shakti society atkot road', 0, 2, 2, 0, 0, 'yes', 1, '16197783261385024721', '', 0, 'yes', 2, 'user', '2021-04-30 06:25:26', '2021-04-30 06:25:37'),
(316, 'PSB297', 1, 329, 'PRAKASHSUTHAR', 'BIBNJARAM', '9549002960', '', 'prakashsuthar8528@gmail.com', '12345', '', '1990-01-01', '', 'A+', 1, 'Hi can', 1, 'You can you', 1, 'Hi can', '', 'I am not', 11, 'You can', 0, 20, 21, 0, 0, 'yes', 1, '16224254126929248324', '', 0, 'yes', 2, 'user', '2021-05-02 01:23:04', '2021-05-30 21:43:32'),
(317, 'PSB298', 2, 452, 'Patelanish', 'Hargovanbhai', '9824738111', '', 'anishpatelcolor@gmail.com', '12345', '', '1989-10-16', '', 'O+', 15, 'Jeera fectory', 30, 'Ganj bazar', 30, 'Jeera no business', '', 'Ssc', 9, 'New krishna paru unjha', 0, 130, 24, 0, 0, 'yes', 1, '16199418726101519765', '', 0, 'yes', 2, 'user', '2021-05-02 03:46:58', '2021-05-02 03:51:12'),
(318, 'PSB299', 1, 249, 'Sutariya Akash', 'Himmatbhai', '9974192965', '', 'akashsutariya83@gmail.com', '12345', '', '2002-01-23', '', 'B+', 6, 'Surat', 0, 'Surat', 0, 'Electric vehicle showroom', '', 'Automobile engineer', 7, 'Bhavnagar', 0, 70, 20, 0, 0, 'yes', 1, '16199765736484084933', '', 0, 'yes', 2, 'user', '2021-05-02 13:29:33', '2021-05-02 13:29:41'),
(319, 'PSB300', 1, 1039, 'Kenil', 'Manish', '9712962289', '', 'kenilsunita@gmail.com', '12345', '', '1996-11-27', '', 'B+', 0, '', 0, '', 43, '', '', 'Computer engineer', 0, 'Kenil manish kumar Patel 2 shreeji soc. Metpur Khambhat', 0, 35, 34, 0, 0, 'yes', 1, '16202176787832444439', '', 0, 'yes', 2, 'user', '2021-05-05 08:27:58', '2021-05-05 08:28:03'),
(320, 'PSB301', 2, 850, 'Alvinkalariya', 'rameshbhai', '9974427255', '', 'alvinpatel308@gmail.com', '12345', '', '1985-08-07', '', 'O+', 1, 'Om chsma ghair Raghuvir complex shop no 5 main bajar', 0, '', 0, '', '', 'Tyba', 0, 'Om chsma ghair main bazar Raghuvir complex shop no5', 0, 2, 2, 0, 0, 'yes', 1, '16203606682303808713', '', 0, 'yes', 2, 'user', '2021-05-07 00:11:08', '2021-05-07 00:11:12'),
(321, 'PSB302', 1, 1046, 'Bhavik', 'Mansukhbhai', '9712159557', '', 'jodhani_b_m@yahoo.com ', '12345', '', '1991-07-25', 'f7c37843c32add9b91eea269d6bf0abfdata.png', 'O+', 22, '\"Shivam Planning & Surveying\"\n103 Panchavati complex, Chitaliya road, Jasdan', 0, '', 0, '', '', 'D E Civil engineer', 0, 'Anand nagara, Samat road', 56, 2, 2, 0, 0, 'yes', 1, '16221937589598911872', '', 0, 'yes', 2, 'user', '2021-05-07 09:07:50', '2021-05-28 05:22:38'),
(322, 'PSB303', 1, 141, 'Ashvin', 'Lakhabhai', '9924466924', '', '', '12345', '', '1972-01-20', '', 'B+', 22, 'Shivam Planning and Surveying, Jasdan', 0, '', 0, '', '', 'Surveyor', 0, 'Jasdan', 56, 2, 2, 0, 0, 'yes', 1, '16203939563679689875', '', 0, 'yes', 2, 'user', '2021-05-07 09:25:56', '2021-05-07 09:25:59'),
(323, 'PSB303', 1, 222, 'Meet', 'Rameshbhai', '9727794246', '', 'Meetsakhiya001@gmail.com', '12345', '', '2002-09-05', '', 'A+', 4, 'Rajkot', 0, '', 0, '', '', 'B. Com', 0, 'Suki Sajadiyali', 0, 175, 2, 0, 0, 'yes', 1, '16208672938379558427', '', 0, 'yes', 2, 'user', '2021-05-12 20:54:53', '2021-05-12 20:54:57'),
(324, 'PSB304', 1, 35, 'Vipulbhai', 'Dhanjibhai', '9824234220 ', '', 'siya3715@gmail.com', '12345', '', '1982-09-20', '', 'B+', 48, 'Kangen water, surat ', 11, 'સુરત ', 0, '', '', 'M. Com B. Ed', 2, 'Bhagvanpara, Behind Gelmata Temple, Chitaliya Road, Jasdan', 0, 2, 2, 0, 0, 'yes', 1, '16210791171277960018', '', 0, 'yes', 2, 'user', '2021-05-15 07:45:17', '2021-05-15 07:45:21'),
(325, 'PSB305', 0, 0, '', '', '8849996043', '', '', '12345', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 0, 0, 0, 0, 0, 'yes', 1, '16211623562300831457', '', 0, 'yes', -1, 'user', '2021-05-16 06:52:36', ''),
(326, 'PSB306', 0, 0, '', '', '7778908927', '', '', '12345', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 0, 0, 0, 0, 0, 'yes', 1, '16211630584002166955', '', 0, 'yes', -1, 'user', '2021-05-16 07:04:18', ''),
(327, 'PSB307', 1, 126, 'Vinod Bhai', 'Pragji Bhai', '9979469124', '', 'vinoddhaduk113@gmail.com', '12345', '', '1978-12-05', '', '', 30, 'Kheti', 30, 'Kheti', 30, 'Thavi', '', '8', 0, 'Thavi', 0, 31, 21, 0, 0, 'yes', 1, '16213286918885703304', '', 0, 'yes', 2, 'user', '2021-05-18 05:04:51', '2021-05-18 05:06:34'),
(328, 'PSB308', 1, 309, 'SANJAYBHAI', 'RAVJIBHAI', '9724210001', '', 'sanjayharkhani78@gmail.com', '12345', '', '1978-09-16', '', 'AB+', 24, 'SHREE RDC BANK LTD. JASDAN', 0, '', 0, '', '', 'M.COM', 10, 'AT KALASAR TA JASDAN DIST RAJKOT', 0, 2, 2, 0, 11111, 'yes', 1, '16218450275283792045', '', 0, 'yes', 2, 'user', '2021-05-24 04:30:27', '2021-05-24 04:31:19'),
(329, 'PSB309', 2, 568, 'Tushar', 'Premjibhai', '9737510399', '', 'tusharbhuva11@gmail.com', '12345', '', '1992-10-01', '', 'A+', 1, '', 0, '', 0, '', '', 'B.com,m.com, LLb ', 0, 'To.Asalpur ', 0, 4, 2, 0, 0, 'yes', 1, '16220995787660755206', '', 0, 'yes', 2, 'user', '2021-05-27 03:12:58', '2021-05-27 03:13:02'),
(330, 'PSB310', 1, 27, 'satishbhai', 'BhuraBhai', '8200351666', '', '', NULL, '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, 'Jasdan', 56, 2, 2, 0, 100, 'yes', 1, '', '', 0, 'yes', 2, 'user', '2021-05-27 07:00:13', ''),
(331, 'PSB311', 1, 57, 'keshubhai', 'Jinabhai', '9974579095', '', '', NULL, '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, 'Lilapur', 46, 2, 2, 0, 1100, 'yes', 1, '', '', 0, 'yes', 2, 'user', '2021-05-27 07:03:43', ''),
(332, 'PSB312', 1, 126, 'vinubhai', '', '8200110051', '', '', NULL, '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, 'santhali', 2, 2, 2, 0, 5100, 'yes', 1, '', '', 0, 'yes', 2, 'user', '2021-05-27 07:06:35', ''),
(333, 'PSB313', 0, 60, 'Dr.Ghanshyambhai', '', '9998497378', '', '', NULL, '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, 'kamlapur', 20, 2, 2, 0, 25000, 'yes', 1, '', '', 0, 'yes', 2, 'user', '2021-05-27 07:09:37', ''),
(334, 'PSB314', 1, 357, 'Divyesh', 'Ambabhai', '9913744675', '', 'dvsbodar@gmail.com', '12345', '', '0000-00-00', 'ffb500e8cd57b7db6f7d9a694800ff25data.png', 'B+', 11, 'Sarasvati Vidya Mandir Chital', 0, '', 0, '', '', 'B.A.', 0, 'Highway, jivapar', 0, 2, 2, 0, 0, 'yes', 1, '16222157046137886493', '', 0, 'yes', 2, 'user', '2021-05-28 11:28:24', '2021-05-28 11:28:28'),
(335, 'PSB315', 1, 1039, 'સચીન', 'રમેશભાઈ', '9638612144', '', '', '12345', '', '1989-09-09', '', 'O+', 30, 'ગામ. વાળીનાથ તાલુકો. ઘોઘંબા  જિલ્લો.પંચમહાલ ', 30, '', 0, '', '', '', 10, 'ગામ. વાળીનાથ   તાલુકો.ઘોઘંબા   જિલ્લો.પંચમહાલ  પિન નંબર 389365', 0, 160, 29, 0, 0, 'yes', 1, '16222757652952632095', '', 0, 'yes', 2, 'user', '2021-05-29 04:04:24', '2021-05-29 04:09:25'),
(336, 'PSB316', 3, 1047, 'Sagar', 'Nashabhai', '9157179175', '', 'Likeeravalonline@gmail.com ', '12345', '', '0000-00-00', '', 'AB+', 1, 'Anjar ', 1, '', 0, '', '', 'Pct college ', 8, 'Anjar', 0, 115, 10, 0, 0, 'yes', 1, '16223007481273560829', '', 0, 'yes', 4, 'user', '2021-05-29 11:05:48', '2021-05-29 11:05:55'),
(337, 'PSB317', 1, 357, 'Pankaj', 'Babubhai', '9924092151', '', 'Shriharikrushna.exports@gmail.com ', '12345', '', '0000-00-00', '', '', 15, '', 0, '', 0, '', '', '', 0, 'Haveli Street,Khambha ', 0, 25, 21, 0, 0, 'yes', 1, '16225645102748753771', '', 0, 'yes', 2, 'user', '2021-05-31 11:53:23', '2021-06-01 12:21:50');
INSERT INTO `user` (`uid`, `member_id`, `samaj_id`, `surname_id`, `middle_name`, `father_name`, `mobile`, `mobile_1`, `email`, `password`, `ad_password`, `dob`, `profile_pic`, `blood_group`, `business`, `business_address`, `business_2`, `business_address_2`, `business_3`, `business_address_3`, `gov_job`, `qualification`, `karkidi_id`, `address`, `village_id`, `taluko_id`, `dist_id`, `hundi_amount`, `fund_amount`, `is_mobile`, `gender`, `is_Token`, `notificationToken`, `verify_by`, `app_access`, `status`, `type`, `date`, `lastlogin`) VALUES
(338, 'PSB318', 1, 357, 'Gaurav', 'Chandubhai', '9574972853', '', '', '12345', '', '2001-11-22', '', '', 6, '', 0, '', 0, '', '', '', 0, 'Jivapar, opp.BSNL tower', 0, 2, 2, 0, 0, 'yes', 1, '16224765698039941127', '', 0, 'yes', 2, 'user', '2021-05-31 11:56:09', '2021-05-31 11:56:20'),
(339, 'PSB319', 1, 357, 'Kishor', 'B,', '9265005172', '', '', '12345', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, 'Sarathana ', 0, 204, 9, 0, 0, 'yes', 1, '16224775795612292821', '', 0, 'yes', 2, 'user', '2021-05-31 12:12:59', '2021-05-31 12:13:05'),
(340, 'PSB320', 1, 357, 'MANSUKHBHAI', 'RANCHHODBHAI', '8200379795', '', '', '12345', '', '1958-12-04', '', 'O+', 30, 'Jivapar ', 30, 'Jivapar ', 0, '', '', '12', 0, 'To jivapar. Post - jasdan vapar. Disat-Rajkot.. Taluka - jasdan ', 0, 2, 2, 0, 0, 'yes', 1, '16224783593871506246', '', 0, 'yes', 2, 'user', '2021-05-31 12:16:57', '2021-05-31 12:25:59'),
(341, 'PSB321', 0, 0, '', '', '9227922727', '', '', '12345', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 0, 0, 0, 0, 0, 'yes', 1, '16224806655252388789', '', 0, 'yes', -1, 'user', '2021-05-31 13:04:25', ''),
(342, 'PSB322', 1, 178, 'જગદીશભાઈ', 'વલ્લભભાઈ', '9913155903', '', '', '12345', '', '0000-00-00', '', 'B+', 16, '', 30, '', 0, '', '', '12', 0, 'ખજુરી ', 0, 26, 21, 0, 0, 'yes', 1, '16224839008846460382', '', 0, 'yes', 2, 'user', '2021-05-31 13:58:20', '2021-05-31 13:58:30'),
(343, 'PSB323', 1, 136, 'Bharat', 'Harajibhai', '9879276808', '', '', '12345', '', '1997-02-20', '', 'O+', 7, 'Amadavad', 30, '', 0, '', '', '8', 0, 'Sanali', 0, 26, 21, 0, 0, 'yes', 1, '16224851925120777108', '', 0, 'yes', 2, 'user', '2021-05-31 14:15:29', '2021-05-31 14:19:52'),
(344, 'PSB324', 1, 357, 'Ketan', 'Babubhai', '9726959346', '', '', '12345', '', '2019-01-20', '', 'O+', 45, 'Jivapar\n', 30, '', 0, '', '', 'B.com ', 0, 'Jivapar ', 0, 2, 2, 0, 0, 'yes', 1, '16225067636751337415', '', 0, 'yes', 2, 'user', '2021-05-31 20:19:23', '2021-05-31 20:19:27'),
(345, 'PSB325', 1, 357, 'Nileshbhai', 'Parshotambhai', '9426777690', '', '', '12345', '', '1974-06-06', '', '', 14, 'Ambrodari job', 0, '', 0, '', '', '11', 0, 'Partappara nanaakdiya rod amreli', 0, 20, 21, 0, 0, 'yes', 1, '16225175536968041235', '', 0, 'yes', 2, 'user', '2021-05-31 23:19:13', '2021-05-31 23:19:17'),
(346, 'PSB326', 1, 77, 'Piyush', 'Dulabhi', '9327616675', '', 'piyushlathiya573@gmil.com', '12345', '', '2021-06-01', '', 'A+', 28, 'Hira office', 53, 'Sop', 3, '', '', 'Hsc', 2, 'Sentrosha aaet utran amroli', 0, 66, 20, 0, 0, 'yes', 1, '16225214743619870876', '', 0, 'yes', 2, 'user', '2021-06-01 00:24:34', '2021-06-01 00:25:03'),
(347, 'PSB327', 1, 184, 'Dineshbhai', 'Meghajibhai', '9913049201', '', '', NULL, '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, 'BOghravadar', 30, 2, 2, 0, 0, 'yes', 1, '', '', 0, 'yes', 2, 'user', '2021-06-01 01:26:01', ''),
(348, 'PSB328', 1, 103, 'Bhanjibhai', 'Lakshmanbhai', '9909868337', '', '', NULL, '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, 'Boghravadar', 30, 2, 2, 0, 0, 'yes', 1, '', '', 0, 'yes', 2, 'user', '2021-06-01 01:28:03', ''),
(349, 'PSB329', 1, 103, 'Gordhanbhai', 'Bhimjibhai', '9427335491', '', '', NULL, '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, 'Boghravadar', 30, 2, 2, 0, 0, 'yes', 1, '', '', 0, 'yes', 2, 'user', '2021-06-01 01:30:28', ''),
(350, 'PSB330', 1, 103, 'Bhanubhai', 'Ghohabhai', '7567713108', '', '', NULL, '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, 'Bogharavadar', 30, 2, 2, 0, 0, 'yes', 1, '', '', 0, 'yes', 2, 'user', '2021-06-01 01:33:16', ''),
(351, 'PSB331', 1, 229, 'Devabhai', 'Lakhmanbhai', '9725253959', '', '', NULL, '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, 'Bogharavadar', 30, 2, 2, 0, 0, 'yes', 1, '', '', 0, 'yes', 2, 'user', '2021-06-01 01:35:39', ''),
(352, 'PSB332', 1, 103, 'Babubhai', 'Vallabhabhai', '9909603738', '', '', NULL, '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, 'BoGharavadar', 30, 2, 2, 0, 0, 'yes', 1, '', '', 0, 'yes', 2, 'user', '2021-06-01 01:37:21', ''),
(353, 'PSB333', 1, 103, 'Raghubhai', 'Gordhanbhai', '9725631998', '', '', NULL, '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, 'Bogharavadar', 30, 2, 2, 0, 0, 'yes', 1, '', '', 0, 'yes', 2, 'user', '2021-06-01 01:40:15', ''),
(354, 'PSB334', 2, 659, 'BHAVIN', 'HARESHBHAI', '9725316784', '', 'padashalabhavin@gmail.com', '12345', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', 'M com ', 2, 'To . Talali \n\n', 0, 26, 21, 0, 0, 'yes', 1, '16225293952563988469', '', 0, 'yes', 2, 'user', '2021-06-01 02:36:35', '2021-06-01 02:36:40'),
(355, 'PSB335', 1, 355, 'Kaushal', 'Ashokbhai', '9313175004', '', '', '12345', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, 'Mahadev para', 0, 25, 21, 0, 0, 'yes', 1, '16225354478849944239', '', 0, 'yes', 2, 'user', '2021-06-01 04:17:27', '2021-06-01 04:17:31'),
(356, 'PSB336', 1, 357, 'Dr Mehul', 'Raghavbhai', '8758780909', '', 'mehulbodar89@gmail.com', '12345', '', '1989-09-29', '', 'B+', 46, 'Vijay Industries, GIDC Ankleshwar', 0, '', 0, '', '', 'BAMS', 7, '05, green home society, B/h La Pino\'s pizza, GIDC Ankleshwar-393002', 0, 2, 2, 0, 0, 'yes', 1, '16225367477316908391', '', 0, 'yes', 2, 'user', '2021-06-01 04:39:07', '2021-06-01 04:39:15'),
(357, 'PSB337', 1, 357, 'Bhupat', 'Bavachandabhai', '8866312213', '', 'bhupatbbodar1978@gmail.com', '12345', '', '1978-01-03', '', '', 21, '', 0, '', 0, '', '', '7', 0, '40 ghautampark puna gam', 0, 207, 9, 0, 0, 'yes', 1, '16225489665106554524', '', 0, 'yes', 2, 'user', '2021-06-01 08:02:46', '2021-06-01 08:02:52'),
(358, 'PSB338', 1, 128, 'બટુકભાઈ', 'હિરજીભાઈ', '9428699156', '', 'dhamibatukbhai@gmail.com', '12345', '', '0000-00-00', '', 'B+', 57, 'dhoraji', 57, 'dhoraji', 57, 'dhoraji', '', 'ba', 0, 'શ્રીકુંજ અપૂર્વ સ્કૂલ પાસે જમનાવડ રોડ\nધોરાજી મો.9428699156', 0, 9, 2, 0, 0, 'yes', 1, '16227097348879392497', '', 0, 'yes', 2, 'user', '2021-06-02 00:45:24', '2021-06-03 04:42:14'),
(359, 'PSB339', 2, 1044, 'Shiv', 'Mukeshbhai', '7990049651', '', 'shivp311@gmail.com', '12345', '', '2001-01-01', '', 'O-', 43, 'Anand', 0, '', 0, '', '', 'B.Tech (Computer Engineering)', 8, 'Anand ', 0, 32, 34, 0, 0, 'yes', 1, '16226223971577084148', '', 0, 'yes', 2, 'user', '2021-06-02 04:26:37', '2021-06-02 04:26:45'),
(360, 'PSB340', 1, 19, 'Yogesh', 'Babubhai', '6353796549', '', 'Yogeshkakdiya135@gmail.com', '12345', '', '2001-10-20', '808f02c3e7d3a5501281037a9b32d99bdata.png', '', 21, 'Kailash nagar jasdan', 0, '', 0, '', '', '', 0, 'Atkot road ,vadla vadi,jasdan', 0, 2, 2, 0, 0, 'yes', 1, '16227824154315775044', '', 0, 'yes', 2, 'user', '2021-06-03 06:34:34', '2021-06-04 00:53:35'),
(361, 'PSB341', 1, 1039, 'નરેશ ભાઈ', 'રવજીભાઈ', '9727341111', '', '', NULL, '', '2021-06-01', '', '', 6, '', 0, '', 0, '', '', '', 10, 'પટેલ બ્રાસ મેટોડા જીઆઇડીસી વિસ્તાર રાજકોટ', 0, 175, 2, 0, 600000, 'yes', 1, '', '', 0, 'yes', 2, 'user', '2021-06-03 07:35:03', ''),
(362, 'PSB342', 1, 258, 'Harkha', 'Parbat', '6353775914', '', 'Gamistationers@gmail.com', '12345', '', '0000-00-00', '', '', 19, 'Vijay speed service', 0, 'Vijay speed service', 0, '', '', '5', 0, 'Adajan', 0, 207, 9, 0, 0, 'yes', 1, '16227882229245491895', '', 0, 'yes', 2, 'user', '2021-06-04 02:30:22', '2021-06-04 02:30:25'),
(363, 'PSB343', 1, 426, 'નિલેશભાઈ', 'વિઠ્ઠલ ભાઈ', '9537748610', '', '', '12345', '', '1993-02-15', '', '', 28, '', 28, '', 28, '', '', '8', 0, '225.ગઢપુર ટાઉનશીપ કઠોદરા પાસોદરા રોડ, સુરત', 0, 140, 37, 0, 0, 'yes', 1, '16227952916766773907', '', 0, 'yes', 2, 'user', '2021-06-04 04:28:11', '2021-06-04 04:28:16'),
(364, 'PSB344', 2, 950, 'Girish Kumar', 'Arvindbhai', '7600012262', '', 'girishpatel.amtc@gmail.com', '12345', '', '1978-07-05', '', 'B+', 19, 'Arvindbhai Motilal transport co hed office PALANPUR.                              banch office Ankleshwar .jambusar.morbi', 1, 'Morbi  ceramic material trending', 0, '', '', '12', 0, 'Juna laxmipura Sheri no 25 palanpur near umiya mataji temple', 0, 51, 25, 0, 0, 'yes', 1, '16231399126842015559', '', 0, 'yes', 2, 'user', '2021-06-08 04:11:52', '2021-06-08 04:12:07'),
(365, 'PSB345', 3, 1047, 'Mayuri', 'Dipak', '7621851634', '', 'Mayuri Raval@gmail.com', '12345', '', '2021-06-08', '', 'A+', 1, 'Amreli', 1, 'Amreli', 1, 'Amreli', '', 'B.com', 3, 'Amreli', 0, 20, 21, 0, 0, 'yes', 0, '16232148787540367301', '', 0, 'yes', 2, 'user', '2021-06-08 05:27:56', '2021-06-09 01:01:18'),
(366, 'PSB346', 2, 662, 'PatelRameshchandra', 'Revabhai', '9426307022', '', '', '12345', '', '2021-03-18', '', 'B+', 11, 'Godhra ', 11, 'Godhra ', 0, '', '', 'M.sc Bed', 2, 'Dolariya ', 0, 250, 30, 0, 0, 'yes', 1, '16234860816336290309', '', 0, 'yes', 2, 'user', '2021-06-12 04:21:21', '2021-06-12 04:21:28'),
(367, 'PSB347', 1, 18, 'Kashyap', 'Kishorbhay', '8200365110', '', '', '12345', '', '2003-04-06', '', 'A+', 0, '', 0, '', 0, '', '', 'Kolej lasat yar', 0, 'Shobhavadala gir', 0, 89, 14, 0, 0, 'yes', 1, '16235157388266668959', '', 0, 'yes', 2, 'user', '2021-06-12 12:35:38', '2021-06-12 12:35:42'),
(368, 'PSB348', 1, 1048, 'શૈલેષભાઇ', 'ડાયાભાઈ', '9979282792', '', 'omsfachra@gmail.com', '12345', '', '1987-09-24', '', 'O+', 27, 'જુના પીપળીયા તા- જસદણ', 30, 'રાવકી તા - લોધિકા', 0, '', '  શિક્ષક', 'PTC/B.A.', 2, 'Ghanshyam nagar maitri school near', 0, 2, 2, 0, 0, 'yes', 1, '16235680789589556843', '', 0, 'yes', 2, 'user', '2021-06-13 03:07:58', '2021-06-13 03:08:08'),
(369, 'PSB349', 1, 153, 'Bhupat', 'Lakhamanbhai', '9725552590', '', 'Vekariya.bhupat1971@gmail.com ', '12345', '', '1971-03-04', '', 'B+', 27, 'At. Nani Lakhavad\nTauka- Jasdan District -  Rajkot ', 0, '', 0, '', 'Primary Teacher ', 'PTC', 2, 'At. Jasdan\nNew Block No. 1, Arjun Park Society, Chitaliya Road, Arjun jasdan', 0, 2, 2, 0, 0, 'yes', 1, '16235692257298878646', '', 0, 'yes', 2, 'user', '2021-06-13 03:27:05', '2021-06-13 03:27:11'),
(370, 'PSB350', 1, 1049, 'Haresh', 'M', '9925774320', '', '', '12345', '', '1989-02-03', '', 'B-', 0, 'Vepar', 0, 'Udhana ', 14, 'Udhan', '', '12', 10, 'Damnagar ', 0, 28, 21, 0, 0, 'yes', 1, '16236596571889154550', '', 0, 'yes', 2, 'user', '2021-06-14 04:34:17', '2021-06-14 04:34:21'),
(371, 'PSB351', 2, 662, 'AKSH', 'DHARMENDRAKUMAR', '7990619709', '', 'patelad700@gmail.com', '12345', '', '2002-10-23', '', 'O+', 0, '', 0, '', 0, '', '', 'Iti', 0, 'A/21, Rambagh society,sahkari jin road', 0, 189, 22, 0, 0, 'yes', 1, '16236693375391797071', '', 0, 'yes', 2, 'user', '2021-06-14 07:15:37', '2021-06-14 07:15:54'),
(372, 'PSB352', 1, 373, 'PRAKASH SUHTAR', 'BIBNJARAM', '9373611825', '', 'prakashsuthar8528@gmail.com', '12345', '', '1990-01-01', '', 'A+', 1, 'Rajasthan', 3, 'For the other day and', 2, 'Good morning I', '', 'Can you', 5, 'Dear sir', 0, 20, 21, 0, 0, 'yes', 1, '16238040163669027588', '', 0, 'yes', 2, 'user', '2021-06-15 20:40:16', '2021-06-15 20:40:19'),
(373, 'PSB353', 3, 1050, 'Jatin', 'Rajeshbhai', '7990519177', '', 'jatinpatel1788@gmail.com', '12345', '', '1988-02-17', '', 'O+', 1, '', 1, '', 0, '', '', 'Engineer', 9, 'Sidchakra society', 0, 39, 34, 0, 0, 'yes', 1, '16238262632938625135', '', 0, 'yes', 2, 'user', '2021-06-16 02:51:03', '2021-06-16 02:51:08'),
(374, 'PSB354', 2, 1051, 'Sunil', 'Rameshbhai', '96878036663', '', '9687803663@sunilg.com', '12345', '', '1993-09-01', '', 'O+', 0, '', 1, '', 0, '', '', 'B.com', 0, '10/3/A pujan bungalows meera mahdev bopal 380058', 0, 17, 28, 0, 0, 'yes', 1, '16238669371395260446', '', 0, 'yes', 2, 'user', '2021-06-16 14:08:57', '2021-06-16 14:09:03'),
(375, 'PSB355', 1, 1052, 'Yagnesh', 'Vinubhai', '8200565752', '', 'yagneshkumbhani92753@gmail.com', '12345', '', '1993-07-23', '', 'B+', 32, '429, Dreemland Sarthana Jakat, Near Nirmal Nagar,', 32, '429, Dreemland Sarthana Jakat, Near Nirmal Nagar,', 32, '429, Dreemland Sarthana Jakat, Near Nirmal Nagar,', '', 'LL.B., LL.M.', 0, '283,Kum Kum Bungalow, Sandhiyer Gam, Surat\n394130', 0, 69, 20, 0, 0, 'yes', 1, '16239055125423320713', '', 0, 'yes', 2, 'user', '2021-06-17 00:51:52', '2021-06-17 00:52:04'),
(376, 'PSB356', 3, 1050, 'ચૌધરી પ્રકાશ', 'હેમજી', '9978527449', '', '', '12345', '', '2021-06-17', '', '', 0, '', 0, '', 0, 'બીજનેશ', '', '', 0, 'થરાદ', 0, 50, 25, 0, 0, 'yes', 1, '16239378998135051515', '', 0, 'yes', 2, 'user', '2021-06-17 09:51:39', '2021-06-17 09:51:45'),
(377, 'PSB357', 2, 1040, 'Patelmehul', 'Vishnubhai', '9714257272', '', 'immehulpatel1958@gmail.com', '12345', '', '1994-08-19', '', 'A+', 6, 'At vijapur', 0, '', 0, '', '', 'Be automobile engineering', 0, 'At - Hirpura , laxmipura vas, pin code - 382870', 0, 128, 24, 0, 0, 'yes', 1, '16240977778359522122', '', 0, 'yes', 2, 'user', '2021-06-19 06:16:17', '2021-06-19 06:16:24'),
(378, 'PSB358', 1, 317, 'DIVYESH', 'KANTIBHAI', '9099089808', '', 'Divyesh.hirpara@gmail.com ', '12345', '', '1991-07-05', '', 'O+', 43, '', 37, '', 0, '', '', 'MCA', 8, 'Gondal', 0, 3, 2, 0, 0, 'yes', 1, '16241997659214869582', '', 0, 'yes', 2, 'user', '2021-06-20 10:36:05', '2021-06-20 10:36:10'),
(379, 'PSB359', 1, 150, 'Ashokbhai', 'Mohanbhai', '9428791244', '', 'amvirani@gmail.com ', '12345', '', '1969-01-16', '', 'O+', 11, 'Vajsurpara', 0, '', 0, '', '', 'PTC', 2, 'Gangabhuvan', 0, 2, 2, 0, 0, 'yes', 1, '16242801266608995472', '', 0, 'yes', 2, 'user', '2021-06-21 08:55:26', '2021-06-21 08:55:30'),
(380, 'PSB360', 2, 1051, 'Nishant', 'Sanjaybhai', '9104903822', '', 'patelnsp2712@gmail.com', '12345', '', '1997-12-27', '', 'O+', 1, 'Himtangar SABARDAIRY', 0, '', 0, '', '', 'Iti', 10, 'At& Post- kherol, ', 0, 183, 22, 0, 0, 'yes', 1, '16243534581193543881', '', 0, 'yes', 2, 'user', '2021-06-22 05:17:38', '2021-06-22 05:17:45'),
(381, 'PSB361', 1, 317, 'Amit', 'Vinubhai', '9033484298', '', '', NULL, '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, 'Jasdan', 56, 2, 2, 0, 500, 'yes', 1, '', '', 0, 'yes', 2, 'user', '2021-06-22 08:59:31', ''),
(382, 'PSB362', 1, 317, 'Jaydeep', 'Dhanji Bhai', '9737149546', '', '', NULL, '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, 'Jasdan', 56, 2, 2, 0, 500, 'yes', 1, '', '', 0, 'yes', 2, 'user', '2021-06-22 09:01:29', ''),
(383, 'PSB363', 1, 57, 'Babu Bhai', 'Dharmshi Bhai', '9426930612', '', '', '12345', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, 'Lilapur', 46, 2, 2, 0, 5100, 'yes', 1, '16243746352414809661', '', 0, 'yes', 2, 'user', '2021-06-22 09:03:49', '2021-06-22 11:10:35'),
(384, 'PSB364', 1, 70, 'Dhiru Bhai', 'Ravji Bhai', '9787590476', '', '', NULL, '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, 'Rajkot', 0, 175, 2, 0, 0, 'yes', 1, '', '', 0, 'yes', 2, 'user', '2021-06-22 09:05:38', ''),
(385, 'PSB365', 1, 57, 'Bharat Bhai', 'Jasmat Bhai', '9974403025', '', '', NULL, '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, 'Lilapur', 46, 2, 2, 0, 0, 'yes', 1, '', '', 0, 'yes', 2, 'user', '2021-06-22 09:07:28', ''),
(386, 'PSB366', 1, 317, 'Dinesh Bhai', 'Manji Bhai', '9104942100', '', '', NULL, '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, 'Jasdan', 56, 2, 2, 0, 5100, 'yes', 1, '', '', 0, 'yes', 2, 'user', '2021-06-22 09:09:54', ''),
(387, 'PSB367', 1, 64, 'Vinu Bhai', 'Keshu Bhai', '9998448316', '', '', NULL, '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, 'Jasdan', 56, 2, 2, 0, 2500, 'yes', 1, '', '', 0, 'yes', 2, 'user', '2021-06-22 09:12:10', ''),
(388, 'PSB368', 1, 193, 'Parbhat Bhai', 'Bagha Bhai', '9824440559', '', '', '12345', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, 'Jasdan', 56, 2, 2, 0, 0, 'yes', 1, '16247888351797382099', '', 0, 'yes', 2, 'user', '2021-06-22 09:13:46', '2021-06-27 06:13:55'),
(389, 'PSB369', 1, 57, 'Raghav Bhai', 'Lavji Bhai', '70464 56859', '', '', NULL, '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, 'Lilapur', 46, 2, 2, 0, 5000, 'yes', 1, '', '', 0, 'yes', 2, 'user', '2021-06-22 09:40:57', ''),
(390, 'PSB370', 3, 1053, 'Shailesh', 'Chinubhai', '9428760675', '', '', '12345', '', '1964-03-23', '', 'B+', 1, 'Navsari', 37, 'Navsari', 31, 'Navsari', '', 'Bsc', 3, 'Astamangal\n Navsari', 0, 143, 5, 0, 0, 'yes', 1, '16244347921713376068', '', 0, 'yes', 2, 'user', '2021-06-23 03:53:12', '2021-06-23 03:53:16'),
(391, 'PSB371', 1, 136, 'Ankit', 'Chandulal', '6352606620', '', 'ankitvasoya3@gmail.com', '12345', '', '0000-00-00', '', 'B+', 1, '', 0, '', 0, '', '', 'Mechanical engineering', 0, 'Chitaliya kuva road ,\nLaxman nagar 2\nJasdan', 0, 2, 2, 0, 0, 'yes', 1, '16245111065464472309', '', 0, 'yes', 2, 'user', '2021-06-24 01:05:06', '2021-06-24 01:05:12'),
(392, 'PSB372', 1, 103, 'Gautam', 'Damjibhai', '8980261168', '', 'dobriyagautam@gmail.com', '12345', '', '0000-00-00', '', 'O+', 0, '', 0, '', 0, '', '', 'B.sc', 0, 'Chitliyakuva road ,arjun park', 0, 2, 2, 0, 0, 'yes', 1, '16245112595894784544', '', 0, 'yes', 2, 'user', '2021-06-24 01:07:39', '2021-06-24 01:07:48'),
(393, 'PSB373', 1, 1052, 'Dhruvesh', 'Kantibhai', '9106838137', '', 'dhruveshkumbhanidk632@gmail.com', '12345', '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, 'Chetiliya kuva road , Arjunpark society ', 0, 2, 2, 0, 0, 'yes', 1, '16245112749423299611', '', 0, 'yes', 2, 'user', '2021-06-24 01:07:54', '2021-06-24 01:07:57'),
(394, 'PSB374', 1, 1039, 'Chirag Bhai', 'Bharat Bhai', '9558088210', '', '', NULL, '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, 'Hadala bhal', 0, 14, 28, 0, 20000, 'yes', 1, '', '', 0, 'yes', 2, 'user', '2021-06-24 06:12:05', ''),
(395, 'PSB375', 2, 956, 'Nikunj', 'Lalji bhai', '9998788037', '', ' Nikunj9998788037@gmail.com', '12345', '', '1987-01-26', '', 'AB+', 6, ' Shree Maruti industries                      Nidhi® product  non stick hard anodized cookware product                             National cotton industries ', 1, ' Shree Maruti industries                      Nidhi® product  non stick hard anodized cookware product                             National cotton industries ', 0, ' Shree Maruti industries                      Nidhi® product  non stick hard anodized cookware product                             National cotton industries ', '', '', 7, ' Toyota showroom ', 0, 175, 2, 0, 0, 'yes', 1, '16245329932488215123', '', 0, 'yes', 2, 'user', '2021-06-24 07:09:53', '2021-06-24 07:10:01'),
(396, 'PSB376', 1, 214, 'Kamlesh', 'Govind Bhai', '9998100071', '', '', NULL, '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, 'Jasdan', 56, 2, 2, 0, 11000, 'yes', 1, '', '', 0, 'yes', 2, 'user', '2021-06-26 01:09:13', ''),
(397, 'PSB377', 1, 57, 'Cankit', 'R', '7016515159', '', '', NULL, '', '0000-00-00', '', '', 0, '', 0, '', 0, '', '', '', 0, 'Kamlapur', 20, 2, 2, 0, 2500, 'yes', 1, '', '', 0, 'yes', 2, 'user', '2021-06-26 01:15:09', ''),
(398, 'PSB378', 1, 317, 'Mayur', 'Maganbhai', '9727224243', '', 'Mayur. Mp556@gmail.com', '12345', '', '1984-11-16', '41ea043384852af36132dfa34f06e54adata.png', '', 4, 'Gayatri sound Jasdan \nMain road. Moti Chowk. ', 0, '', 0, '', '', '12', 0, 'Sardar Patel street. Moti Chowk ', 56, 2, 2, 0, 0, 'yes', 1, '16247253732473952475', '', 0, 'yes', 2, 'user', '2021-06-26 12:36:13', '2021-06-26 12:36:23'),
(399, 'PSB379', 2, 1040, 'Avani', 'Navinbhai', '9228245101', '', 'Navinchadra19660@gmail. Com', '12345', '', '1992-12-06', '', 'A+', 0, '', 0, '', 0, '', '', 'M. Com ', 0, 'A-503 satva recedency,\nNr, S. B. I Bank,\nBorisna road,\nPachvati,\nKalol', 0, 105, 27, 0, 0, 'yes', 0, '16247292975287010986', '', 0, 'yes', 2, 'user', '2021-06-26 13:40:23', '2021-06-26 13:41:37'),
(400, 'PSB380', 1, 164, 'Mitul', 'Rameshbhai', '9712555284', '', 'mitulshekhaliya@gmail.com', '12345', '', '0000-00-00', '', 'B+', 1, '', 1, '', 1, '', '', '', 4, 'B', 0, 204, 9, 0, 0, 'yes', 1, '16247311967243440305', '', 0, 'yes', 2, 'user', '2021-06-26 14:13:16', '2021-06-26 14:13:20'),
(401, 'PSB381', 1, 1052, 'Mayurbhai', 'Pragjibhai', '9909151315', '', '', '12345', '', '1986-02-24', '', '', 1, 'Bhaktinagar shakmarket', 0, '', 0, '', '', '12', 0, 'Lakhapadar', 0, 23, 21, 0, 0, 'yes', 1, '16247429515496876822', '', 0, 'yes', 2, 'user', '2021-06-26 17:29:11', '2021-06-26 17:29:17'),
(402, 'PSB382', 1, 21, 'AJAY', 'Himantbhai', '9824843046', '', '', '12345', '', '1986-10-04', 'c6abe842b51ef6eff9eb1ce8d21ddfa4data.png', 'A+', 34, 'Moti chowck', 0, '', 34, 'Bay pas jasdan', '', '', 0, 'Sardar Patel Street 3.motichowck', 22, 2, 2, 0, 0, 'yes', 1, '16247626293911247681', '', 0, 'yes', 2, 'user', '2021-06-26 22:57:09', '2021-06-26 22:57:14'),
(403, 'PSB383', 1, 317, 'KALPESHBHAI', 'HASMUKHBHAI', '9426720963 ', '', 'Kalpeshhirpara@yahoo.co.in ', '12345', '', '1977-11-27', '', 'A+', 37, '', 0, '', 0, '', '', '12', 0, 'A-502, ARADHANA RESIDENCY\nNR GANGOTRI CURCLE NIKOL ', 0, 19, 28, 0, 0, 'yes', 1, '16247643423542918612', '', 0, 'yes', 2, 'user', '2021-06-26 23:25:42', '2021-06-26 23:25:46'),
(404, 'PSB384', 1, 347, 'kishan', 'Dineshbhai', '8264693132', '', '', '12345', '', '1997-06-07', '', 'B+', 29, '', 0, '', 0, '', '', 'b.com', 0, 'jasdan ', 0, 2, 2, 0, 0, 'yes', 1, '16247735222825865684', '', 0, 'yes', 2, 'user', '2021-06-27 01:58:42', '2021-06-27 01:58:46'),
(405, 'PSB385', 2, 875, 'vijay', 'joytiVIJAY', '9913834052', '', '', '12345', '', '0000-00-00', '', '', 29, '', 0, '', 0, '', '', '', 7, 'H', 0, 2, 2, 0, 0, 'yes', 1, '16247943098320628366', '', 0, 'yes', 2, 'user', '2021-06-27 07:45:09', '2021-06-27 07:45:11'),
(406, 'PSB386', 1, 157, 'Jitendra', 'Vaghjibhai', '9727701380', '', '', '12345', '', '1970-05-01', '', 'B+', 27, 'Jasdan', 0, '', 0, '', '', 'Mphs', 3, 'Samat road', 0, 2, 2, 0, 0, 'yes', 1, '16248101697732730509', '', 0, 'yes', 2, 'user', '2021-06-27 12:09:29', '2021-06-27 12:09:36'),
(407, 'PSB387', 2, 1040, '', '', '8347050200', '', '', '12345', '', '2021-06-01', '', '', 0, '', 0, '', 0, '', '', '', 0, '', 0, 0, 0, 0, 0, 'yes', 1, '16248178315074345136', '', 0, 'yes', 2, 'user', '2021-06-27 14:17:11', '2021-06-27 14:17:16'),
(408, 'PSB388', 2, 1044, 'Sani', 'Amrutbhai', '9824530525', '', 'sanipatel02@gmail.com', '12345', '', '1995-02-11', '', 'B+', 27, 'Kalol', 0, '', 0, '', 'Junior Assistant UGVCL', 'M.com', 2, '\nAt Sankapura\nTa Vijapur \nDist Mehsana', 0, 128, 24, 0, 0, 'yes', 1, '16248207259276266151', '', 0, 'yes', 2, 'user', '2021-06-27 15:01:09', '2021-06-27 15:05:25'),
(409, 'PSB389', 2, 717, 'Nirav', 'Manshukhbhai', '9106092192', '', 'Patelbhai9948@gmail.com', '12345', '', '1999-02-28', '', 'B+', 47, 'Deco gold ciramic morabi\n', 53, 'Morabi\n', 47, 'Ocean industry morabi', '', 'Engineering', 0, 'Sara\n', 0, 210, 11, 0, 0, 'yes', 1, '16248488329809548922', '', 0, 'yes', 2, 'user', '2021-06-27 22:53:52', '2021-06-27 22:53:56'),
(410, 'PSB390', 1, 5, 'Denish', 'JAYANTIBHAI', '8140078007', '', 'denishkamani820@gmail.com', '12345', '', '0000-00-00', '', 'B+', 0, '', 0, '', 0, '', '', '', 0, 'Rajkot', 0, 175, 2, 0, 0, 'yes', 1, '16248687644201107761', '', 0, 'yes', 2, 'user', '2021-06-28 04:26:04', '2021-06-28 04:26:08'),
(411, 'PSB391', 1, 1039, 'Ashokitaliya', 'Dayabhai', '9925454129', '', '', '12345', '', '0000-00-00', '', '', 0, 'Surat', 0, '', 0, '', '', '7', 0, 'Surat', 0, 28, 21, 0, 0, 'yes', 1, '16249030201242169371', '', 0, 'yes', 2, 'user', '2021-06-28 13:57:00', '2021-06-28 13:57:04');

-- --------------------------------------------------------

--
-- Table structure for table `user_master`
--

CREATE TABLE `user_master` (
  `id` int(11) NOT NULL,
  `member_id` varchar(50) NOT NULL,
  `p_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(200) NOT NULL,
  `mobile` varchar(15) NOT NULL,
  `mobile_2` varchar(15) NOT NULL,
  `password` varchar(20) NOT NULL,
  `web_password` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `gender` int(1) NOT NULL COMMENT '1=male 0 Female',
  `address` varchar(1000) NOT NULL,
  `city_id` int(11) NOT NULL,
  `profile_pic` varchar(1000) NOT NULL,
  `addhar_card_doc` varchar(1000) NOT NULL,
  `pan_doc` varchar(1000) NOT NULL,
  `type` enum('management','it','partner','account') NOT NULL,
  `lastlogin` date NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `join_date` date NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_master`
--

INSERT INTO `user_master` (`id`, `member_id`, `p_id`, `name`, `mobile`, `mobile_2`, `password`, `web_password`, `email`, `gender`, `address`, `city_id`, `profile_pic`, `addhar_card_doc`, `pan_doc`, `type`, `lastlogin`, `status`, `join_date`, `updated_at`, `created_at`) VALUES
(1, '', 0, 'Jaykar Infotech', '8128500518', '', '', '12345', 'jaykarinfotech@gmailcom', 0, 'Surat', 0, '', '', '', 'it', '2021-05-08', 1, '0000-00-00', '0000-00-00 00:00:00', '2021-05-04 02:07:11'),
(2, '1', 0, 'Jayanit Satani', '81285005123', '123', '', 'abcd@123', 'test@gmail.com', 1, 'Dharai', 2, 'dbebd570f46c206d67dc5f3a54bc0f6bCompany_logo.png', 'a54a8ed5e5266e84f7457c51fa92d482Company_logo.png', 'bc953f88b8d1eb51ad3417c1cfe4179aCompany_logo.png', 'partner', '0000-00-00', 1, '2021-05-07', '2021-05-07 12:31:42', '2021-05-07 11:44:04');

-- --------------------------------------------------------

--
-- Table structure for table `user_transcription`
--

CREATE TABLE `user_transcription` (
  `id` int(11) NOT NULL,
  `txt_id` varchar(100) NOT NULL,
  `bill_id` varchar(100) NOT NULL,
  `uid` int(11) NOT NULL,
  `amount` double NOT NULL,
  `notes` varchar(1000) NOT NULL,
  `fund_type` int(11) NOT NULL,
  `by_uid` int(11) NOT NULL,
  `by_brand` varchar(1000) NOT NULL,
  `by_model` varchar(1000) NOT NULL,
  `time` time NOT NULL,
  `type` enum('hundi','main') NOT NULL DEFAULT 'main',
  `status` int(1) NOT NULL DEFAULT '1',
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_transcription`
--

INSERT INTO `user_transcription` (`id`, `txt_id`, `bill_id`, `uid`, `amount`, `notes`, `fund_type`, `by_uid`, `by_brand`, `by_model`, `time`, `type`, `status`, `date`) VALUES
(1, '16189736711436', 'NKMJ1', 80, 5100, 'તા.૪/૨/૨૦૨૧ ખાતમુહૂર્ત પ.નં.-A ૦૦૧', 1, 8, 'OPPO', 'RMX1801', '00:00:00', 'main', 1, '2021-04-20 22:54:31'),
(2, '16189747152414', 'NKMJ2', 2, 5100, 'તા.૪/૨/૨૦૨૧ ખાતમુહ્રત પ.નં.-A ૦૦૨', 1, 8, 'OPPO', 'RMX1801', '00:00:00', 'main', 1, '2021-04-20 23:11:55'),
(3, '16189747713930', 'NKMJ3', 81, 500, 'તા.૪/૨/૨૦૨૧ ખાતમુહ્રત પ.નં.-A૦૦૩', 1, 8, 'OPPO', 'RMX1801', '00:00:00', 'main', 1, '2021-04-20 23:12:51'),
(4, '16189748116463', 'NKMJ4', 82, 1100, 'તા.૪/૨/૨૦૨૧ ખાતમુહ્રત પ.નં.-A૦૦૪', 1, 8, 'OPPO', 'RMX1801', '00:00:00', 'main', 1, '2021-04-20 23:13:31'),
(5, '16189748458056', 'NKMJ5', 83, 1100, 'તા.૪/૨/૨૦૨૧ ખાતમુહ્રત પ.નં.-A૦૦૫', 1, 8, 'OPPO', 'RMX1801', '00:00:00', 'main', 1, '2021-04-20 23:14:05'),
(6, '16189748791418', 'NKMJ6', 84, 1100, 'તા.૪/૨/૨૦૨૧ ખાતમુહ્રત પ.નં.-A૦૦૬', 1, 8, 'OPPO', 'RMX1801', '00:00:00', 'main', 1, '2021-04-20 23:14:39'),
(7, '16189749245795', 'NKMJ7', 85, 501, 'તા.૪/૨/૨૦૨૧ ખાતમુહ્રત પ.નં.-A૦૦૭', 1, 8, 'OPPO', 'RMX1801', '00:00:00', 'main', 1, '2021-04-20 23:15:24'),
(8, '16189749604274', 'NKMJ8', 86, 1111, 'તા.૪/૨/૨૦૨૧ ખાતમુહ્રત પ.નં.-A૦૦૮', 1, 8, 'OPPO', 'RMX1801', '00:00:00', 'main', 1, '2021-04-20 23:16:00'),
(9, '16189750054787', 'NKMJ9', 87, 1100, 'તા.૪/૨/૨૦૨૧ ખાતમુહ્રત પ.નં.-A૦૦૯', 1, 8, 'OPPO', 'RMX1801', '00:00:00', 'main', 1, '2021-04-20 23:16:45'),
(10, '16189750388906', 'NKMJ10', 89, 1100, 'તા.૪/૨/૨૦૨૧ ખાતમુહ્રત પ.નં.-A૦૧૦', 1, 8, 'OPPO', 'RMX1801', '00:00:00', 'main', 1, '2021-04-20 23:17:18'),
(11, '16189750901237', 'NKMJ11', 90, 251, 'તા.૪/૨/૨૦૨૧ ખાતમુહ્રત પ.નં.-A૦૧૧', 1, 8, 'OPPO', 'RMX1801', '00:00:00', 'main', 1, '2021-04-20 23:18:10'),
(12, '16189751266123', 'NKMJ12', 91, 1100, 'તા.૪/૨/૨૦૨૧ ખાતમુહ્રત પ.નં.-A૦૧૨', 1, 8, 'OPPO', 'RMX1801', '00:00:00', 'main', 1, '2021-04-20 23:18:46'),
(13, '16189751572085', 'NKMJ13', 22, 1100, 'તા.૪/૨/૨૦૨૧ ખાતમુહ્રત પ.નં.-A૦૧૩', 1, 8, 'OPPO', 'RMX1801', '00:00:00', 'main', 1, '2021-04-20 23:19:17'),
(14, '16189752126116', 'NKMJ14', 92, 1100, 'તા.૪/૨/૨૦૨૧ ખાતમુહ્રત પ.નં.-A૦૧૪', 1, 8, 'OPPO', 'RMX1801', '00:00:00', 'main', 1, '2021-04-20 23:20:12'),
(15, '16189753143571', 'NKMJ15', 93, 5000, 'તા.૪/૨/૨૦૨૧ ખાતમુહ્રત પ.નં.-A૦૧૫ (૧૧૧૧૧-૫૦૦૦=૬૧૧૧ બાકી)', 1, 8, 'OPPO', 'RMX1801', '00:00:00', 'main', 1, '2021-04-20 23:21:54'),
(16, '16189753494282', 'NKMJ16', 94, 11000, 'તા.૪/૨/૨૦૨૧ ખાતમુહ્રત પ.નં.-A૦૧૬', 1, 8, 'OPPO', 'RMX1801', '00:00:00', 'main', 1, '2021-04-20 23:22:29'),
(17, '16189753894656', 'NKMJ17', 95, 5100, 'તા.૪/૨/૨૦૨૧ ખાતમુહ્રત પ.નં.-A૦૧૭', 1, 8, 'OPPO', 'RMX1801', '00:00:00', 'main', 1, '2021-04-20 23:23:09'),
(18, '16189754252165', 'NKMJ18', 96, 1100, 'તા.૪/૨/૨૦૨૧ ખાતમુહ્રત પ.નં.-A૦૧૮', 1, 8, 'OPPO', 'RMX1801', '00:00:00', 'main', 1, '2021-04-20 23:23:45'),
(19, '16189754598122', 'NKMJ19', 97, 1100, 'તા.૪/૨/૨૦૨૧ ખાતમુહ્રત પ.નં.-A૦૧૯', 1, 8, 'OPPO', 'RMX1801', '00:00:00', 'main', 1, '2021-04-20 23:24:19'),
(20, '16189755215509', 'NKMJ20', 20, 1100, 'તા.૪/૨/૨૦૨૧ ખાતમુહ્રત પ.નં.-A૦૨૦', 1, 8, 'OPPO', 'RMX1801', '00:00:00', 'main', 1, '2021-04-20 23:25:21'),
(21, '16189755512291', 'NKMJ21', 98, 500, 'તા.૪/૨/૨૦૨૧ ખાતમુહ્રત પ.નં.-A૦૨૧', 1, 8, 'OPPO', 'RMX1801', '00:00:00', 'main', 1, '2021-04-20 23:25:51'),
(22, '16189755815805', 'NKMJ22', 113, 500, 'તા.૪/૨/૨૦૨૧ ખાતમુહ્રત પ.નં.-A૦૨૨', 1, 8, 'OPPO', 'RMX1801', '00:00:00', 'main', 1, '2021-04-20 23:26:21'),
(23, '16189756106887', 'NKMJ23', 117, 500, 'તા.૪/૨/૨૦૨૧ ખાતમુહ્રત પ.નં.-A૦૨૩', 1, 8, 'OPPO', 'RMX1801', '00:00:00', 'main', 1, '2021-04-20 23:26:50'),
(24, '16189756409205', 'NKMJ24', 119, 11000, 'તા.૪/૨/૨૦૨૧ ખાતમુહ્રત પ.નં.-A૦૨૪', 1, 8, 'OPPO', 'RMX1801', '00:00:00', 'main', 1, '2021-04-20 23:27:20'),
(25, '16189756705682', 'NKMJ25', 121, 11000, 'તા.૪/૨/૨૦૨૧ ખાતમુહ્રત પ.નં.-A૦૨૫', 1, 8, 'OPPO', 'RMX1801', '00:00:00', 'main', 1, '2021-04-20 23:27:50'),
(26, '16189757001493', 'NKMJ26', 122, 2100, 'તા.૪/૨/૨૦૨૧ ખાતમુહ્રત પ.નં.-A૦૨૬', 1, 8, 'OPPO', 'RMX1801', '00:00:00', 'main', 1, '2021-04-20 23:28:20'),
(27, '16189757303768', 'NKMJ27', 123, 500, 'તા.૪/૨/૨૦૨૧ ખાતમુહ્રત પ.નં.-A૦૨૭', 1, 8, 'OPPO', 'RMX1801', '00:00:00', 'main', 1, '2021-04-20 23:28:50'),
(28, '16189757597733', 'NKMJ28', 131, 1100, 'તા.૪/૨/૨૦૨૧ ખાતમુહ્રત પ.નં.-A૦૨૮', 1, 8, 'OPPO', 'RMX1801', '00:00:00', 'main', 1, '2021-04-20 23:29:19'),
(29, '16189757883356', 'NKMJ29', 184, 500, 'તા.૪/૨/૨૦૨૧ ખાતમુહ્રત પ.નં.-A૦૨૯', 1, 8, 'OPPO', 'RMX1801', '00:00:00', 'main', 1, '2021-04-20 23:29:48'),
(30, '16189758265372', 'NKMJ30', 185, 500, 'તા.૪/૨/૨૦૨૧ ખાતમુહ્રત પ.નં.-A૦૩૦', 1, 8, 'OPPO', 'RMX1801', '00:00:00', 'main', 1, '2021-04-20 23:30:26'),
(31, '16189758547842', 'NKMJ31', 189, 500, 'તા.૪/૨/૨૦૨૧ ખાતમુહ્રત પ.નં.-A૦૩૧', 1, 8, 'OPPO', 'RMX1801', '00:00:00', 'main', 1, '2021-04-20 23:30:54'),
(32, '16189758853630', 'NKMJ32', 186, 1111, 'તા.૪/૨/૨૦૨૧ ખાતમુહ્રત પ.નં.-A૦૩૨', 1, 8, 'OPPO', 'RMX1801', '00:00:00', 'main', 1, '2021-04-20 23:31:25'),
(33, '16189762056740', 'NKMJ33', 14, 200000, 'આંગડીયાથી ૧૧/૨/૨૦૨૧ પ.નં.A ૦૩૩', 1, 8, 'OPPO', 'RMX1801', '00:00:00', 'main', 1, '2021-04-20 23:36:45'),
(34, '16189762784763', 'NKMJ34', 14, 99700, 'આંગડીયાથી ૧૬/૨/૨૦૨૧ પ.નં.A૦૩૪', 1, 8, 'OPPO', 'RMX1801', '00:00:00', 'main', 1, '2021-04-20 23:37:58'),
(35, '16189763458827', 'NKMJ35', 187, 125500, '૧/૩/૨૦૨૧ પ.નં.A૦૩૫', 1, 8, 'OPPO', 'RMX1801', '00:00:00', 'main', 1, '2021-04-20 23:39:05'),
(36, '16189763987563', 'NKMJ36', 187, 125500, '૧/૩/૨૦૨૧ પ.નં.A ૦૩૬', 1, 8, 'OPPO', 'RMX1801', '00:00:00', 'main', 1, '2021-04-20 23:39:58'),
(37, '16189817571326', 'NKMJ37', 214, 151000, 'ચેક નં 693496', 10, 2, 'OPPO', 'CPH2001', '00:00:00', 'main', 1, '2021-04-21 01:09:17'),
(38, '16189818083534', 'NKMJ38', 253, 10000, '17/3/21 ટ્રાન્સફર', 10, 2, 'OPPO', 'CPH2001', '00:00:00', 'main', 1, '2021-04-21 01:10:08'),
(39, '16189823425515', 'NKMJ39', 155, 11000, 'તા.૧૪/૩/૨૦૨૧ પ.નં A ૦૩૮', 1, 8, 'OPPO', 'RMX1801', '00:00:00', 'main', 1, '2021-04-21 01:19:02'),
(40, '16189824512723', 'NKMJ40', 14, 99900, 'આંગડીયાથી ૯/૪/૨૦૨૧', 1, 8, 'OPPO', 'RMX1801', '00:00:00', 'main', 1, '2021-04-21 01:20:51'),
(41, '16189825343812', 'NKMJ41', 300, 50000, 'તા.૧૨/૪/૨૦૨૧ પ.નં.A૦૪૨', 1, 8, 'OPPO', 'RMX1801', '00:00:00', 'main', 1, '2021-04-21 01:22:14'),
(42, '16189826532696', 'NKMJ42', 14, 25000, 'તા.૧૬/૪/૨૦૨૧ પ.નં.A૦૪૩', 1, 8, 'OPPO', 'RMX1801', '00:00:00', 'main', 1, '2021-04-21 01:24:13'),
(43, '16189827211457', 'NKMJ43', 14, 25000, 'તા.૧૬/૪/૨૦૨૧ પ.નં.A ૦૪૪', 1, 8, 'OPPO', 'RMX1801', '00:00:00', 'main', 1, '2021-04-21 01:25:21'),
(44, '16189831692342', 'NKMJ44', 212, 350, '30 મિનિટ jcb ચલાવતા સેવા પેટે', 6, 8, 'OPPO', 'RMX1801', '00:00:00', 'main', 1, '2021-04-21 01:32:49'),
(45, '16189832375715', 'NKMJ45', 10, 200, 'સિમેન્ટની ખાલી થેલી વેચતા પ.નં.A ૧૦૨', 6, 8, 'OPPO', 'RMX1801', '00:00:00', 'main', 1, '2021-04-21 01:33:57'),
(46, '16189832926723', 'NKMJ46', 11, 5100, '51 પ્લેટ ભોજન સેવા પેટે પ.નં.A ૧૦૩', 6, 8, 'OPPO', 'RMX1801', '00:00:00', 'main', 1, '2021-04-21 01:34:52'),
(47, '16189834679722', 'NKMJ47', 28, 3000, '30 ફેરા ફરતી કરતા ટ્રેક્ટર ભાડું સેવા પ.નં.A ૧૦૪', 6, 8, 'OPPO', 'RMX1801', '00:00:00', 'main', 1, '2021-04-21 01:37:47'),
(48, '16189835492138', 'NKMJ48', 10, 200, 'સિમેન્ટની ખાલી થેલી વેચતા પ.નં.A ૧૦૫', 6, 8, 'OPPO', 'RMX1801', '00:00:00', 'main', 1, '2021-04-21 01:39:09'),
(49, '16189858248012', 'NKMJ49', 236, 11000, 'ચેક નં   034', 10, 2, 'OPPO', 'CPH2001', '00:00:00', 'main', 1, '2021-04-21 02:17:04'),
(50, '16191717099694', 'NKMJ50', 10, 2669, 'Interest credit', 10, 2, 'OPPO', 'CPH2001', '00:00:00', 'main', 1, '2021-04-23 05:55:09'),
(51, '16191718527720', 'NKMJ51', 10, 117000, 'આખર બેલેન્સ', 10, 2, 'OPPO', 'CPH2001', '00:00:00', 'main', 1, '2021-04-23 05:57:32'),
(52, '16192481048937', 'NKMJ52', 10, 326, 'આખર બેલેન્સ ડીફરનટ', 10, 2, 'OPPO', 'CPH2001', '00:00:00', 'main', 1, '2021-04-24 03:08:24'),
(53, '16201224304093', 'NKMJ53', 14, 150000, 'હ. વલ્લભભાઈ ખાખરીયા', 1, 2, 'OPPO', 'CPH2001', '00:00:00', 'main', 1, '2021-05-04 06:00:30'),
(54, '16206275209647', 'NKMJ54', 213, 1000, 'Vipul tilaka', 5, 2, 'OPPO', 'CPH2001', '00:00:00', 'main', -1, '2021-05-10 02:18:40'),
(55, '16206471917843', 'NKMJ55', 14, 50000, 'Kamlesh HDFC transfer', 1, 2, 'OPPO', 'CPH2001', '00:00:00', 'main', 1, '2021-05-10 07:46:31'),
(56, '16207438263747', 'NKMJ56', 10, 5000, 'પરચુરણ આવક\nપીઠીયા વેચતા હ, વલ્લભભાઈ', 6, 2, 'OPPO', 'CPH2001', '00:00:00', 'main', 1, '2021-05-11 10:37:06'),
(57, '16216870455622', 'NKMJ57', 14, 200000, 'V Patel Angadiya thi jama', 1, 2, 'OPPO', 'CPH2001', '00:00:00', 'main', 1, '2021-05-22 08:37:25'),
(58, '16219434524617', 'NKMJ58', 328, 11111, 'RDC jasdan cak no..', 1, 2, 'OPPO', 'CPH2001', '00:00:00', 'main', 1, '2021-05-25 07:50:52'),
(59, '16220135993768', 'NKMJ59', 214, 100000, 'Chek no ..094 SBI', 1, 2, 'OPPO', 'CPH2001', '00:00:00', 'main', 1, '2021-05-26 03:19:59'),
(60, '16221132564511', 'NKMJ60', 330, 100, '25/05/2021', 1, 8, 'OPPO', 'RMX1801', '00:00:00', 'main', 1, '2021-05-27 07:00:56'),
(61, '16221134541388', 'NKMJ61', 331, 1100, '25/05/2021', 1, 8, 'OPPO', 'RMX1801', '00:00:00', 'main', 1, '2021-05-27 07:04:14'),
(62, '16221136207079', 'NKMJ62', 332, 5100, '25/05/2021', 1, 8, 'OPPO', 'RMX1801', '00:00:00', 'main', 1, '2021-05-27 07:07:00'),
(63, '16221138399480', 'NKMJ63', 333, 25000, '26/05/2021', 1, 8, 'OPPO', 'RMX1801', '00:00:00', 'main', 1, '2021-05-27 07:10:39'),
(64, '16221140611682', 'NKMJ64', 207, 5100, '26/05/2021', 1, 8, 'OPPO', 'RMX1801', '00:00:00', 'main', 1, '2021-05-27 07:14:21'),
(65, '16222631319160', 'NKMJ65', 10, 310, 'સીમેન્ટ ની ખાલી થેલી વેચતા 210', 6, 2, 'OPPO', 'CPH2001', '00:00:00', 'main', 1, '2021-05-29 00:38:51'),
(66, '16227201939214', 'NKMJ66', 361, 600000, 'બાંધકામ લોન માટે જમા આવેલ છે.', 10, 2, 'OPPO', 'CPH2001', '00:00:00', 'main', 1, '2021-06-03 07:36:33'),
(67, '16233250208236', 'NKMJ67', 28, 3200, 'આટકોટ બાંકડા ભરવા\nમાટી ફેરવતા 12ફેરા\nજીવાપર ટેન્કર મુકવા જતા ભાડુ', 6, 2, 'OPPO', 'CPH2001', '00:00:00', 'main', 1, '2021-06-10 07:37:00'),
(68, '16235039066664', 'NKMJ68', 264, 2100, '', 1, 264, 'vivo', 'vivo 1916', '00:00:00', 'main', 1, '2021-06-12 09:18:26'),
(69, '16235660498840', 'NKMJ69', 28, 100, '', 1, 2, 'OPPO', 'CPH2001', '00:00:00', 'main', 1, '2021-06-13 02:34:09'),
(70, '16240805425120', 'NKMJ70', 14, 125000, 'Kadi pase thi\nKeval 200000-75000 lon jama', 1, 2, 'OPPO', 'CPH2001', '00:00:00', 'main', 1, '2021-06-19 01:29:02'),
(71, '16243678759609', 'NKMJ71', 383, 5100, '', 4, 264, 'vivo', 'vivo 1916', '00:00:00', 'main', 1, '2021-06-22 09:17:55'),
(72, '16243679206137', 'NKMJ72', 382, 500, '', 1, 264, 'vivo', 'vivo 1916', '00:00:00', 'main', 1, '2021-06-22 09:18:40'),
(73, '16243679489620', 'NKMJ73', 381, 500, '', 1, 264, 'vivo', 'vivo 1916', '00:00:00', 'main', 1, '2021-06-22 09:19:08'),
(74, '16243693502572', 'NKMJ74', 389, 5000, 'Rokd transfer', 10, 2, 'OPPO', 'CPH2001', '00:00:00', 'main', 1, '2021-06-22 09:42:30'),
(75, '16245289008617', 'NKMJ75', 14, 25000, 'Natu Bhai hast ', 1, 2, 'OPPO', 'CPH2001', '00:00:00', 'main', 1, '2021-06-24 06:01:40'),
(76, '16245296111938', 'NKMJ76', 394, 20000, 'Transfer\nReference.dinsha Bhai', 1, 2, 'OPPO', 'CPH2001', '00:00:00', 'main', 1, '2021-06-24 06:13:31'),
(77, '16246842313455', 'NKMJ77', 396, 11000, '10/6/21 online tranfar', 10, 2, 'OPPO', 'CPH2001', '00:00:00', 'main', 1, '2021-06-26 01:10:31'),
(78, '16246845869868', 'NKMJ78', 397, 2500, '11/6/21 online tranfar', 10, 2, 'OPPO', 'CPH2001', '00:00:00', 'main', 1, '2021-06-26 01:16:26'),
(79, '16247904745253', 'NKMJ79', 386, 5100, 'લગ્ન પ્રસંગે ભેટ', 4, 46, 'xiaomi', 'Redmi Note 4', '00:00:00', 'main', 1, '2021-06-27 06:41:14'),
(80, '16247917676854', 'NKMJ80', 387, 2500, 'લગ્ન પ્રસંગે ભેટ', 4, 46, 'xiaomi', 'Redmi Note 4', '00:00:00', 'main', 1, '2021-06-27 07:02:47');

-- --------------------------------------------------------

--
-- Table structure for table `video`
--

CREATE TABLE `video` (
  `id` int(11) NOT NULL,
  `name` varchar(1000) NOT NULL,
  `banner` varchar(1000) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `sender_type` enum('temple','dada','all') NOT NULL DEFAULT 'all',
  `uid` int(11) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `video`
--

INSERT INTO `video` (`id`, `name`, `banner`, `sender_id`, `sender_type`, `uid`, `status`, `date`) VALUES
(1, 'Hirpara Parivar Sneh Milan 2020 Surat', '', 2, 'all', 2, -1, '2021-01-18 08:25:57'),
(2, 'પાટીદાર શૈક્ષણિક ભવન-જસદણ બાંધકામ પ્રોગ્રેસ', '16e3d65e1374db45d590ff4fdd788af5IMG20210509104736.jpg', 2, 'all', 2, 1, '2021-06-04 01:07:45');

-- --------------------------------------------------------

--
-- Table structure for table `village_master`
--

CREATE TABLE `village_master` (
  `id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `name` varchar(1000) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `village_master`
--

INSERT INTO `village_master` (`id`, `city_id`, `name`, `status`, `date`) VALUES
(1, 2, 'juna pipliya', 1, '2021-02-13 11:10:21'),
(2, 2, 'Shanthli', 1, '2021-02-13 05:59:20'),
(3, 2, 'Atkot', 1, '2021-02-13 05:59:26'),
(4, 2, 'Virnagar', 1, '2021-02-13 05:59:34'),
(5, 2, 'Baldhuy', 1, '2021-02-13 05:59:47'),
(6, 2, 'Kharchiya ,jas', 1, '2021-02-13 06:00:09'),
(7, 2, 'Jashapar', 1, '2021-02-13 06:00:18'),
(8, 2, 'Pachavda', 1, '2021-02-13 06:00:28'),
(9, 2, 'Ishvariya', 1, '2021-02-13 06:00:39'),
(10, 2, 'Kanpar', 1, '2021-02-13 06:00:50'),
(11, 2, 'Jivapar', 1, '2021-02-13 06:00:59'),
(12, 2, 'partap pur', 1, '2021-02-13 11:07:06'),
(13, 2, 'Dolatpar', 1, '2021-02-13 06:01:43'),
(14, 2, 'Veraval- santh', 1, '2021-02-13 06:02:01'),
(15, 2, 'Jangvad', 1, '2021-02-13 06:02:31'),
(16, 2, 'Gundala-jam', 1, '2021-02-13 11:06:05'),
(17, 2, 'Jundala', 1, '2021-02-13 06:02:47'),
(18, 2, 'Bakhalvad', 1, '2021-02-13 06:02:57'),
(19, 2, 'Parevala', 1, '2021-02-13 06:03:04'),
(20, 2, 'Kamlapur', 1, '2021-02-13 06:03:13'),
(21, 2, 'Dahishra', 1, '2021-02-13 06:03:20'),
(22, 2, 'Bhadala', 1, '2021-02-13 06:03:27'),
(23, 2, 'Veraval-bhad', 1, '2021-02-13 06:03:46'),
(24, 2, 'Bhandariya', 1, '2021-02-13 06:04:05'),
(25, 2, 'Adhiya', 1, '2021-02-13 06:04:23'),
(26, 2, 'Ramliya', 1, '2021-02-13 06:04:31'),
(27, 2, 'Khadvavdi', 1, '2021-02-13 06:04:39'),
(28, 2, 'Bedala', -1, '2021-02-13 06:04:47'),
(29, 2, 'Ranigpar', 1, '2021-02-13 06:04:56'),
(30, 2, 'Bogharavadar', 1, '2021-02-13 06:05:38'),
(31, 2, 'Virpar -bhad', 1, '2021-02-13 06:06:54'),
(32, 2, 'Kaneshra', 1, '2021-02-13 06:07:03'),
(33, 2, 'Kundani', 1, '2021-02-13 06:07:10'),
(34, 2, 'Barvala', 1, '2021-02-13 06:07:19'),
(35, 2, 'Rajavadla -jam', 1, '2021-02-13 06:07:35'),
(36, 2, 'Rajavadla -jas', 1, '2021-02-13 06:07:48'),
(37, 2, 'Shantinagar', 1, '2021-02-13 06:07:56'),
(38, 2, 'Kothi', 1, '2021-02-13 06:08:03'),
(39, 2, 'Nani lakhavad', 1, '2021-02-13 06:08:12'),
(40, 2, 'Chitliya', 1, '2021-02-13 06:08:18'),
(41, 2, 'Kaduka', 1, '2021-02-13 06:08:35'),
(42, 2, 'Madava', 1, '2021-02-13 06:08:41'),
(43, 2, 'Devpara', 1, '2021-02-13 06:08:48'),
(44, 2, 'Khanda-hadmtiya', 1, '2021-02-13 06:09:09'),
(45, 2, 'Kalashar', 1, '2021-02-13 06:12:51'),
(46, 2, 'Lilapur', 1, '2021-02-13 06:09:25'),
(47, 2, 'Madhvipur', 1, '2021-02-13 06:09:33'),
(48, 2, 'Gadhdiya- jas', 1, '2021-02-13 06:09:48'),
(49, 2, 'Gadhdiya-jam', 1, '2021-02-13 06:09:58'),
(50, 2, 'Sivrajpur', 1, '2021-02-13 06:10:10'),
(51, 2, 'Gokhlana', 1, '2021-02-13 06:10:19'),
(52, 2, 'Vadod', 1, '2021-02-13 06:10:26'),
(53, 2, 'Navagam- vadod', 1, '2021-02-13 06:10:53'),
(54, 2, 'Ambardi', 1, '2021-02-13 06:11:39'),
(55, 2, 'Meghapar', 1, '2021-02-13 06:15:54'),
(56, 2, 'Jasdan', 1, '2021-02-13 06:23:52'),
(57, 2, 'dodiyala', 1, '2021-02-13 11:12:07'),
(58, 2, 'madhda', 1, '2021-02-13 11:13:00'),
(59, 2, 'ranparda', 1, '2021-02-13 11:13:21'),
(60, 2, 'polarpar', 1, '2021-02-13 11:13:43'),
(61, 2, 'godladhar', 1, '2021-02-13 11:14:05'),
(62, 2, 'ranjit gadh', 1, '2021-02-13 11:16:01'),
(63, 4, 'vichiya', 1, '2021-02-13 11:16:54'),
(64, 4, 'revaniya', 1, '2021-02-13 11:17:14'),
(65, 4, 'kaduka', 1, '2021-02-13 11:17:25'),
(66, 4, 'lalavadar', 1, '2021-02-13 11:17:38'),
(67, 4, 'higol gadh', 1, '2021-02-13 11:17:50'),
(68, 4, 'amrapur', 1, '2021-02-13 11:18:05'),
(69, 4, 'kotda- vichiya', 1, '2021-02-13 11:18:43');

-- --------------------------------------------------------

--
-- Table structure for table `vyakti_vishesh`
--

CREATE TABLE `vyakti_vishesh` (
  `id` int(11) NOT NULL,
  `name` varchar(500) NOT NULL,
  `mobile` varchar(15) NOT NULL,
  `village` varchar(500) NOT NULL,
  `address` varchar(1000) NOT NULL,
  `city_id` int(11) NOT NULL,
  `dist_id` int(11) NOT NULL,
  `dob` varchar(15) NOT NULL,
  `title` longtext NOT NULL,
  `notes` longtext NOT NULL,
  `image_1` varchar(1000) NOT NULL,
  `image_2` varchar(1000) NOT NULL,
  `image_3` varchar(1000) NOT NULL,
  `image_4` varchar(1000) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `vyakti_vishesh`
--

INSERT INTO `vyakti_vishesh` (`id`, `name`, `mobile`, `village`, `address`, `city_id`, `dist_id`, `dob`, `title`, `notes`, `image_1`, `image_2`, `image_3`, `image_4`, `status`, `date`) VALUES
(1, 'રૂડા ભાઈ નાગજીભાઇ હીરપરા (રૂડા બાપા)', '', 'જસદણ', '૧, કૈલાસ નગર, વિવેકાનંદ સ્કૂલ પાસે, જસદણ', 2, 2, '', 'વર્ષ ...જસદણ માર્કેટ યાર્ડ નાં સ્થાપક', 'Test', '', '76c5a9187dedc543eaa6a662ee7debb3art_1.png', '1155a95cad97218376f9a4fc9f545eb9colars_stud.png', '022d04925a7f3a92366e0f7d42b00a1cUntitled_5.png', 1, '2021-01-18 09:38:20');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_transcription`
--
ALTER TABLE `admin_transcription`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ayojan`
--
ALTER TABLE `ayojan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ayojan_img`
--
ALTER TABLE `ayojan_img`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `business_master`
--
ALTER TABLE `business_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `district_master`
--
ALTER TABLE `district_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ebook`
--
ALTER TABLE `ebook`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `eventlog`
--
ALTER TABLE `eventlog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fund_type`
--
ALTER TABLE `fund_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery_details`
--
ALTER TABLE `gallery_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gov_document`
--
ALTER TABLE `gov_document`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jiveensathi`
--
ALTER TABLE `jiveensathi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `job`
--
ALTER TABLE `job`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `karkidi_master`
--
ALTER TABLE `karkidi_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kharsh_transcription`
--
ALTER TABLE `kharsh_transcription`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `labharthi_master`
--
ALTER TABLE `labharthi_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `loan_transcription`
--
ALTER TABLE `loan_transcription`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mainAdmin_transcription`
--
ALTER TABLE `mainAdmin_transcription`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notice`
--
ALTER TABLE `notice`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `qualification_master`
--
ALTER TABLE `qualification_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_master`
--
ALTER TABLE `role_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `session`
--
ALTER TABLE `session`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `std_master`
--
ALTER TABLE `std_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_scholarship`
--
ALTER TABLE `student_scholarship`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_scholarship_info`
--
ALTER TABLE `student_scholarship_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `surname`
--
ALTER TABLE `surname`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `taluko_master`
--
ALTER TABLE `taluko_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`uid`);

--
-- Indexes for table `user_master`
--
ALTER TABLE `user_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_transcription`
--
ALTER TABLE `user_transcription`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `video`
--
ALTER TABLE `video`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `village_master`
--
ALTER TABLE `village_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vyakti_vishesh`
--
ALTER TABLE `vyakti_vishesh`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_transcription`
--
ALTER TABLE `admin_transcription`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `ayojan`
--
ALTER TABLE `ayojan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `ayojan_img`
--
ALTER TABLE `ayojan_img`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `business_master`
--
ALTER TABLE `business_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `district_master`
--
ALTER TABLE `district_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `ebook`
--
ALTER TABLE `ebook`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `eventlog`
--
ALTER TABLE `eventlog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT for table `feedback`
--
ALTER TABLE `feedback`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `fund_type`
--
ALTER TABLE `fund_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `gallery`
--
ALTER TABLE `gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `gallery_details`
--
ALTER TABLE `gallery_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;

--
-- AUTO_INCREMENT for table `gov_document`
--
ALTER TABLE `gov_document`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `jiveensathi`
--
ALTER TABLE `jiveensathi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `job`
--
ALTER TABLE `job`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `karkidi_master`
--
ALTER TABLE `karkidi_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `kharsh_transcription`
--
ALTER TABLE `kharsh_transcription`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=96;

--
-- AUTO_INCREMENT for table `labharthi_master`
--
ALTER TABLE `labharthi_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=83;

--
-- AUTO_INCREMENT for table `loan_transcription`
--
ALTER TABLE `loan_transcription`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `mainAdmin_transcription`
--
ALTER TABLE `mainAdmin_transcription`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `notice`
--
ALTER TABLE `notice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `qualification_master`
--
ALTER TABLE `qualification_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `role_master`
--
ALTER TABLE `role_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `session`
--
ALTER TABLE `session`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=233;

--
-- AUTO_INCREMENT for table `setting`
--
ALTER TABLE `setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `std_master`
--
ALTER TABLE `std_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `student_scholarship`
--
ALTER TABLE `student_scholarship`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `student_scholarship_info`
--
ALTER TABLE `student_scholarship_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `surname`
--
ALTER TABLE `surname`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1054;

--
-- AUTO_INCREMENT for table `taluko_master`
--
ALTER TABLE `taluko_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=273;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `uid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=412;

--
-- AUTO_INCREMENT for table `user_master`
--
ALTER TABLE `user_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user_transcription`
--
ALTER TABLE `user_transcription`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;

--
-- AUTO_INCREMENT for table `video`
--
ALTER TABLE `video`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `village_master`
--
ALTER TABLE `village_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;

--
-- AUTO_INCREMENT for table `vyakti_vishesh`
--
ALTER TABLE `vyakti_vishesh`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
