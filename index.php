<?php
include_once("scripts/db.php");
include_once("scripts/functions.php");
db_connect();

if (!empty($_SESSION['access']) && $_SESSION['access'] != "") {
	$id = $_SESSION['access']['uid'];
	$admin = getRow("SELECT u.uid,s.name surname,u.middle_name,u.father_name,u.mobile,u.address,u.type FROM user u
	LEFT JOIN surname s On s.id=u.surname_id WHERE u.status = '2' and uid=:id", array('id' => $id));
}
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<head>
	<meta charset="utf-8" />
	<link rel="shortcut icon" href="scripts/img/logo.png" />
	<title>સમસ્ત પાટીદાર સમાજ - ગુજરાત | Admin</title>
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />

	<!-- ================== BEGIN BASE CSS STYLE ================== -->
	<link href="//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
	<link href="<?php echo $adminURL; ?>scripts/plugins/jquery-ui/themes/base/minified/jquery-ui.min.css" rel="stylesheet" />
	<link href="<?php echo $adminURL; ?>scripts/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
	<link href="<?php echo $adminURL; ?>scripts/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
	<link href="<?php echo $adminURL; ?>scripts/plugins/gritter/css/jquery.gritter.css" rel="stylesheet" />
	<link href="<?php echo $adminURL; ?>scripts/css/animate.min.css" rel="stylesheet" />
	<link href="<?php echo $adminURL; ?>scripts/css/style.min.css?v=1.1" rel="stylesheet" />
	<link href="<?php echo $adminURL; ?>scripts/css/style-responsive.min.css" rel="stylesheet" />
	<link href="<?php echo $adminURL; ?>scripts/css/theme/blue.css" rel="stylesheet" id="theme" />
	<link href="<?php echo $adminURL; ?>scripts/plugins/bootstrap-datepicker/css/datepicker.css" rel="stylesheet" />
	<link href="<?php echo $adminURL; ?>scripts/plugins/bootstrap-datetimepicker/css/datetimepicker.css" rel="stylesheet" />
	<link href="<?php echo $adminURL; ?>scripts/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css" rel="stylesheet" />
	<link href="<?php echo $adminURL; ?>scripts/css/DT_bootstrap.css?v=1.2" rel="stylesheet" />
	<link href="<?php echo $adminURL; ?>scripts/css/bootstrap-dialog.min.css?v=1.2" rel="stylesheet" />
	<link href="<?php echo $adminURL; ?>scripts/css/custom.css?v=1.1" rel="stylesheet" />
	<link href="<?php echo $adminURL; ?>scripts/plugins/lightbox/css/lightbox.css" rel="stylesheet" />
	<!-- ================== END BASE CSS STYLE ================== -->

	<!-- ================== BEGIN BASE JS ================== -->
	<script src="<?php echo $adminURL; ?>scripts/plugins/pace/pace.min.js"></script>
	<!-- ================== END BASE JS ================== -->
	<script src="<?php echo $adminURL; ?>scripts/js/apps.min.js"></script>

</head>
<style>
	.loading {
		position: fixed;
		z-index: 999999;
		height: 2em;
		width: 2em;
		overflow: visible;
		margin: auto;
		top: 0;
		left: 0;
		bottom: 0;
		right: 0;
	}

	/* Transparent Overlay */
	.loading:before {
		content: '';
		display: block;
		position: fixed;
		top: 0;
		left: 0;
		width: 100%;
		height: 100%;
		background-color: rgba(0, 0, 0, 0.3);
	}

	/* :not(:required) hides these rules from IE9 and below */
	.loading:not(:required) {
		/* hide "loading..." text */
		font: 0/0 a;
		color: transparent;
		text-shadow: none;
		background-color: transparent;
		border: 0;
	}

	.loading:not(:required):after {
		content: '';
		display: block;
		font-size: 10px;
		width: 1em;
		height: 1em;
		margin-top: -0.5em;
		-webkit-animation: spinner 1500ms infinite linear;
		-moz-animation: spinner 1500ms infinite linear;
		-ms-animation: spinner 1500ms infinite linear;
		-o-animation: spinner 1500ms infinite linear;
		animation: spinner 1500ms infinite linear;
		border-radius: 0.5em;
		-webkit-box-shadow: rgba(0, 0, 0, 0.75) 1.5em 0 0 0, rgba(0, 0, 0, 0.75) 1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) 0 1.5em 0 0, rgba(0, 0, 0, 0.75) -1.1em 1.1em 0 0, rgba(0, 0, 0, 0.5) -1.5em 0 0 0, rgba(0, 0, 0, 0.5) -1.1em -1.1em 0 0, rgba(0, 0, 0, 0.75) 0 -1.5em 0 0, rgba(0, 0, 0, 0.75) 1.1em -1.1em 0 0;
		box-shadow: rgba(0, 0, 0, 0.75) 1.5em 0 0 0, rgba(0, 0, 0, 0.75) 1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) 0 1.5em 0 0, rgba(0, 0, 0, 0.75) -1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) -1.5em 0 0 0, rgba(0, 0, 0, 0.75) -1.1em -1.1em 0 0, rgba(0, 0, 0, 0.75) 0 -1.5em 0 0, rgba(0, 0, 0, 0.75) 1.1em -1.1em 0 0;
	}

	/* Animation */

	@-webkit-keyframes spinner {
		0% {
			-webkit-transform: rotate(0deg);
			-moz-transform: rotate(0deg);
			-ms-transform: rotate(0deg);
			-o-transform: rotate(0deg);
			transform: rotate(0deg);
		}

		100% {
			-webkit-transform: rotate(360deg);
			-moz-transform: rotate(360deg);
			-ms-transform: rotate(360deg);
			-o-transform: rotate(360deg);
			transform: rotate(360deg);
		}
	}

	@-moz-keyframes spinner {
		0% {
			-webkit-transform: rotate(0deg);
			-moz-transform: rotate(0deg);
			-ms-transform: rotate(0deg);
			-o-transform: rotate(0deg);
			transform: rotate(0deg);
		}

		100% {
			-webkit-transform: rotate(360deg);
			-moz-transform: rotate(360deg);
			-ms-transform: rotate(360deg);
			-o-transform: rotate(360deg);
			transform: rotate(360deg);
		}
	}

	@-o-keyframes spinner {
		0% {
			-webkit-transform: rotate(0deg);
			-moz-transform: rotate(0deg);
			-ms-transform: rotate(0deg);
			-o-transform: rotate(0deg);
			transform: rotate(0deg);
		}

		100% {
			-webkit-transform: rotate(360deg);
			-moz-transform: rotate(360deg);
			-ms-transform: rotate(360deg);
			-o-transform: rotate(360deg);
			transform: rotate(360deg);
		}
	}

	@keyframes spinner {
		0% {
			-webkit-transform: rotate(0deg);
			-moz-transform: rotate(0deg);
			-ms-transform: rotate(0deg);
			-o-transform: rotate(0deg);
			transform: rotate(0deg);
		}

		100% {
			-webkit-transform: rotate(360deg);
			-moz-transform: rotate(360deg);
			-ms-transform: rotate(360deg);
			-o-transform: rotate(360deg);
			transform: rotate(360deg);
		}
	}
</style>

<body>
	<div class="loading hide" id="upload-loader"></div>

	<?php if (!empty($_SESSION['access'])) { ?>
		<!-- begin #page-loader -->
		<div id="page-loader" class="fade in"><span class="spinner"></span></div>
		<!-- end #page-loader -->

		<!-- begin #page-container -->
		<div id="page-container" class="fade page-sidebar-fixed page-header-fixed">
			<!-- begin #header -->
			<div id="header" class="header navbar navbar-default navbar-fixed-top">
				<!-- begin container-fluid -->
				<div class="container-fluid">
					<!-- begin mobile sidebar expand / collapse button -->
					<div class="navbar-header">
						<a href="#index.php" class="ajax navbar-brand"><img src="scripts/img/logo.png" /></a>
						<button type="button" class="navbar-toggle" data-click="sidebar-toggled">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
					</div>
					<!-- end mobile sidebar expand / collapse button -->
					<!-- begin header navigation right -->
					<ul class="nav navbar-nav navbar-right">
						<li style="display:none;">
							<form class="navbar-form full-width">
								<div class="form-group">
									<input type="text" class="form-control" placeholder="Enter keyword" />
									<button type="submit" class="btn btn-search"><i class="fa fa-search"></i></button>
								</div>
							</form>
						</li>

						<li class="dropdown navbar-user">
							<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
								<img src="scripts/img/user-512.webp" class="gritter-image">
								<span class="hidden-xs"><?php echo $admin['middle_name'] . ' ' . $admin['surname']; ?></span> <b class="caret"></b>
							</a>
							<ul class="dropdown-menu animated fadeInLeft">
								<li class="arrow"></li>
								<li><a href="#profile.php">Edit Profile</a></li>
								<li><a href="scripts/php/logout.php">Log Out</a></li>
							</ul>
						</li>

					</ul>
					<!-- end header navigation right -->
				</div>
				<!-- end container-fluid -->
			</div>
			<!-- end #header -->

			<!-- begin #sidebar -->
			<div id="sidebar" class="sidebar">
				<!-- begin sidebar scrollbar -->
				<div data-scrollbar="true" data-height="100%">
					<!-- begin sidebar user -->
					<ul class="nav">
						<li class="nav-profile">
							<div class="image">
								<a href="javascript:;"><img src="scripts/img/user-512.webp" class=""></a>
							</div>
							<div class="info">
								<?php echo $admin['middle_name'] . ' ' . $admin['surname']; ?>
								<small>Admin</small>
							</div>
						</li>
					</ul>
					<ul class="nav">
						<li class="active" id="page_index">
							<a href="#index.php" class="ajax"><i class="fa fa-dashboard"></i> Dashboard</a>
						</li>
						<?php if (menuRights($id, 'member', 'view')) { ?>
							<li class="has-sub" id="page_user">
								<a href="javascript:;">
									<i class="fa fa-users"></i>
									<span>સભ્યો</span>
								</a>
								<ul class="sub-menu">
									<li class="" id="page_user2"><a href="#member.php?type=2" class="ajax">Active</a></li>
									<li class="" id="page_user1"><a href="#member.php?type=1" class="ajax">New Request</a></li>
									<li class="" id="page_user3"><a href="#member.php?type=3" class="ajax">Rejected</a></li>
									<li class="" id="page_user4"><a href="#member.php?type=4" class="ajax">Guest User</a></li>
									<li class="" id="page_user5"><a href="#member.php?type=0" class="ajax">Only Mobile</a></li>
								</ul>
							</li>
						<?php } ?>
						<?php if (menuRights($id, 'fund', 'view')) { ?>

							<li class="" id="page_allFund">
								<a href="#AllFund.php" class="ajax"><i class="fa fa-inr"></i>ફાળો</a>
							</li>
						<?php } ?>
						<?php if (menuRights($id, 'loan', 'view')) { ?>
							<li class="" id="page_loan">
								<a href="#loan.php" class="ajax"><i class="fa fa-inr"></i>લોન</a>
							</li>
						<?php } ?>
						<?php if (menuRights($id, 'kharsh', 'view')) { ?>
							<li class="" id="page_kharsh">
								<a href="#exp.php" class="ajax"><i class="fa fa-inr"></i>ખર્ચ</a>
							</li>
						<?php } ?>
						<?php if (menuRights($id, 'notice', 'view')) { ?>
							<li class="" id="page_notice">
								<a href="#notice.php" class="ajax"><i class="fa fa-bullhorn"></i>નોટીસ</a>
							</li>
						<?php } ?>
						<?php if (menuRights($id, 'sahai', 'view')) { ?>
							<li class="has-sub" id="page_job">
								<a href="javascript:;">
									<i class="fa fa-graduation-cap"></i>
									<span>નોકરી</span>
								</a>
								<ul class="sub-menu">
									<li class="" id="page_com"><a href="#job.php?type=company" class="ajax">કંપની</a></li>
									<li class="" id="page_emp"><a href="#job.php?type=employee" class="ajax">એમ્પ્લોઈ</a></li>
								</ul>
							</li>
						<?php } ?>
						<?php if (menuRights($id, 'job', 'view')) { ?>
							<li class="" id="page_sahay">
								<a href="#document.php?type=sahay" class="ajax"><i class="fa fa-file"></i>સહાય યોજના</a>
							</li>
						<?php } ?>
						<?php if (menuRights($id, 'document', 'view')) { ?>
							<li class="" id="page_document">
								<a href="#document.php?type=gov" class="ajax"><i class="fa fa-file"></i>જરૂરી ડોક્યુમેન્ટ</a>
							</li>
						<?php } ?>
						<?php if (menuRights($id, 'eBook', 'view')) { ?>
							<li class="" id="page_ebook">
								<a href="#ebook.php" class="ajax"><i class="fa fa-book"></i>ઈ-બુક </a>
							</li>
						<?php } ?>
						<?php if (menuRights($id, 'jaherat', 'view')) { ?>
							<li class="" id="page_jaherat">
								<a href="#jaherat.php" class="ajax"><i class="fa fa-user"></i>જાહેરાત</a>
							</li>
						<?php } ?>
						<?php if (menuRights($id, 'student', 'view')) { ?>
							<li class="" id="page_student">
								<a href="#student.php" class="ajax"><i class="fa fa-user"></i>Student Scholarship</a>
							</li>
						<?php } ?>
						<?php if (menuRights($id, 'labharthi', 'view')) { ?>
							<li class="" id="page_labharthi">
								<a href="#labharthi.php" class="ajax"><i class="fa fa-money"></i>લાભાર્થી</a>
							</li>
							<?php } ?>
						<?php if (menuRights($id, 'elders', 'view')) { ?>
							<li class="" id="page_elders">
								<a href="#elders.php" class="ajax"><i class="fa fa-users"></i>વડીલો</a>
							</li>
						<?php } ?>
						<?php if (menuRights($id, 'feedback', 'view')) { ?>
							<li class="" id="page_feedback">
								<a href="#feedback.php" class="ajax"><i class="fa fa-comments"></i>Feedback</a>
							</li>
						<?php } ?>
						<?php if (menuRights($id, 'gallery', 'view')) { ?>
							<li class="" id="page_gallery">
								<a href="#gallery.php" class="ajax"><i class="fa fa-picture-o"></i>Gallery</a>
							</li>
						<?php } ?>
						<?php if (menuRights($id, 'video', 'view')) { ?>
							<li class="" id="page_video">
								<a href="#video.php" class="ajax"><i class="fa fa-video-camera"></i>વિડિઓ</a>
							</li>
						<?php } ?>
						<?php if (menuRights($id, 'slider', 'view')) { ?>
							<li class="" id="page_slider">
								<a href="#slider.php" class="ajax"><i class="fa fa-sliders"></i>Slider</a>
							</li>
						<?php } ?>

						<?php if (menuRights($id, 'settings', 'view')) { ?>

						<li class="has-sub" id="page_setting">
							<a href="javascript:;">
								<i class="fa  fa-cog"></i>
								<span>Settings</span>
							</a>
							<ul class="sub-menu">
								<li class="" id="page_district"><a href="#district.php" class="ajax">District Master</a></li>
								<li class="" id="page_karkidi"><a href="#karkidi.php" class="ajax">કારકિર્દી</a></li>
								<li class="" id="page_surname"><a href="#surname.php?id=0" class="ajax">Surname Master</a></li>
								<li class="" id="page_business"><a href="#business.php" class="ajax">વ્યવસાય</a></li>
								<li class="" id="page_fund"><a href="#fund_type.php" class="ajax">Fund Type</a></li>
								<li class="" id="page_department"><a href="#department.php" class="ajax">Department</a></li>
								<li class="" id="page_setting"><a href="#setting.php" class="ajax">Settings</a></li>
							</ul>
						</li>	<?php } ?>
					
						<!-- <li class="" id="page_city">
							<a href="#city.php" class="ajax"><i class="fa fa-map-marker"></i>City Master</a>
						</li>
						<li class="hide" id="page_Salesaman_Tracking">
							<a href="#Salesaman Tracking.php" class="ajax"><i class="fa fa-map-marker"></i>Salesaman Tracking</a>
						</li>
						<li class="" id="page_Vehical_Managment">
							<a href="#vehical_managment.php" class="ajax"><i class="fa fa-truck"></i>Vehicle Master</a>
						</li>
						<li class="" id="page_bank">
							<a href="#bank_master.php" class="ajax"><i class="fa fa-university"></i>Bank Master</a>
						</li>
						<li class="" id="page_setting">
							<a href="#setting.php" class="ajax"><i class="fa fa-cog"></i>Settings</a>
						</li> -->
					</ul>
					<!-- end sidebar nav -->
				</div>
				<!-- end sidebar scrollbar -->
			</div>
			<div class="sidebar-bg"></div>
			<!-- end #sidebar -->

			<!-- begin #content -->
			<div id="content" class="content master-container">
			</div>
		<?php } else {
		echo "<div class='master-container'></div>";
	} ?>
		<div class="hidden">
			<a href="#" id="hiddenLink" class="ajax">[x]</a>
		</div>

		<!-- ================== BEGIN BASE JS ================== -->
		<script src="<?php echo $adminURL; ?>scripts/plugins/jquery/jquery-1.9.1.min.js"></script>
		<script src="<?php echo $adminURL; ?>scripts/plugins/jquery/jquery-migrate-1.1.0.min.js"></script>
		<script src="<?php echo $adminURL; ?>scripts/plugins/jquery-ui/ui/minified/jquery-ui.min.js"></script>
		<script src="<?php echo $adminURL; ?>scripts/plugins/bootstrap/js/bootstrap.min.js"></script>
		<script src="<?php echo $adminURL; ?>scripts/js/clipboard.min.js"></script>
		<script src="<?php echo $adminURL; ?>scripts/js/bootstrap-dialog.min.js"></script>
		<script src="<?php echo $adminURL; ?>scripts/js/jquery.cycle.all.min.js"></script>
		<!--[if lt IE 9]>
	<script src="assets/crossbrowserjs/html5shiv.js"></script>
	<script src="assets/crossbrowserjs/respond.min.js"></script>
	<script src="assets/crossbrowserjs/excanvas.min.js"></script>
<![endif]-->
		<script src="<?php echo $adminURL; ?>scripts/plugins/slimscroll/jquery.slimscroll.min.js"></script>
		<script src="<?php echo $adminURL; ?>scripts/plugins/jquery-cookie/jquery.cookie.js"></script>
		<script src="<?php echo $adminURL; ?>scripts/plugins/gritter/js/jquery.gritter.js"></script>
		<!-- ================== END BASE JS ================== -->

		<!-- 
<script src="<?php echo $adminURL; ?>scripts/plugins/DataTables/media/js/jquery.dataTables.js"></script>
<script src="<?php echo $adminURL; ?>scripts/plugins/DataTables/media/js/dataTables.bootstrap.min.js"></script>
<script src="<?php echo $adminURL; ?>scripts/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
 -->
		<!-- <script src="<?php echo $adminURL; ?>scripts/plugins/moment/moment.min.js"></script> -->
		<script src="<?php echo $adminURL; ?>scripts/plugins/bootstrap-daterangepicker/moment.min.js"></script>
		<script src="<?php echo $adminURL; ?>scripts/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js?v1"></script>
		<script src="<?php echo $adminURL; ?>scripts/plugins/bootstrap-daterangepicker/daterangepicker.js?v1"></script>
		<script src="<?php echo $adminURL; ?>scripts/plugins/lightbox/js/lightbox.min.js"></script>

		<script src="<?php echo $adminURL; ?>scripts/js/my.dataTables.js"></script>
		<script src="<?php echo $adminURL; ?>scripts/js/DT_bootstrap.js"></script>
		<script src="<?php echo $adminURL; ?>scripts/js/datatable.js"></script>

		<script type="text/javascript" src="<?php echo $adminURL; ?>scripts/js/custom.js"> </script>
		<script type="text/javascript">
			var siteURL = <?php echo json_encode($adminURL); ?>;

			<?php if (empty($_SESSION['access'])) { ?>
				isLoggedIn = false;
				user = null;
				App.init();
				login();
			<?php } else { ?>
				isLoggedIn = true;
				user = <?php echo json_encode($_SESSION['access']); ?>;
				App.init();
				try_route();
				$(document).ready(function() {
					$(".nav a").click(function() {
						$(".nav .active").removeClass("active");
						$(this).parent().addClass("active");
					});
				});
			<?php } ?>

			function setCookie(name, value, days) {
				var expires = "";
				if (days) {
					var date = new Date();
					date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
					expires = "; expires=" + date.toUTCString();
				}
				document.cookie = name + "=" + (value || "") + expires + "; path=/";
			}

			function getCookie(name) {
				var nameEQ = name + "=";
				var ca = document.cookie.split(';');
				for (var i = 0; i < ca.length; i++) {
					var c = ca[i];
					while (c.charAt(0) == ' ') c = c.substring(1, c.length);
					if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
				}
				return null;
			}

			function eraseCookie(name) {
				document.cookie = name + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
			}
		</script>
</body>

</html>
<!-- Test commit -->