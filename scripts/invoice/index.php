<?php
include '../db.php';
include '../functions.php';
db_connect();
if (!empty($_SESSION['access']) && $_SESSION['access'] != "") {
  // $id = $_SESSION['access']['id'];
  $cgst = getSetting("cgst");
  $sgst = getSetting("sgst");

  $total_tax = $cgst +  $sgst;
  $taxable = 0;
  $taxable_amt = 0;
  $cgst_amt = 0;
  $sgst_amt = 0;
  $tax_total = 0;

  $cat_id = 0;
  if (!empty($_GET['id'])) {
    $cat_id = $_GET['id'];
  }

  $query = $con->prepare("SELECT o.*,u.name shop_owner,u.shop_name,u.GSTIN,u.city_id,c.name city,d.vehical_id,v.vehical_number
    FROM  order_management o 
    LEFT JOIN user u ON u.uid=o.shop_id
    LEFT JOIN order_delivery d ON d.id=o.d_id
    LEFT JOIN vehical_master v ON v.id=d.vehical_id
    LEFT JOIN city_master c ON c.id=u.city_id
    WHERE o.status = '1' and o.id=:id
	");

  $query->bindParam(":id", $cat_id);

  $query->execute();
  if ($query->rowCount() > 0) {
    $order_list = $query->fetch(PDO::FETCH_ASSOC);

    $product_master = getRows("SELECT *
	from order_management_details
	 where status='1' and order_id=:order_id", array('order_id' => $order_list['id']));


    $taxable = ($order_list['total_amount'] * $total_tax) / 100;
    $cgst_amt = ($order_list['total_amount'] * $cgst) / 100;
    $sgst_amt = ($order_list['total_amount'] * $sgst) / 100;

    $taxable_amt = $order_list['total_amount'] - $taxable;
    $tax_total = $sgst_amt + $cgst_amt;
  }
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Damodar Oil Mill</title>
  <meta name="author" content="harnishdesign.net">

  <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900' type='text/css'>
  <link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css" />
  <link rel="stylesheet" type="text/css" href="vendor/font-awesome/css/all.min.css" />
  <link rel="stylesheet" type="text/css" href="css/stylesheet.css" />
</head>
<style>
  .header_no {
    width: 5%;
  }

  .header_desc {
    width: 50%;
  }

  .header_rate {
    width: 10%;
    text-align: left;
  }

  .header_qty {
    width: 20%;
  }

  .header_amt {
    width: 1015%;
  }

  .cat_font {
    font-size: 10px;
  }
</style>

<body>
  <div class="container-fluid invoice-container">
    <header>
      <div class="row align-items-center">
        <div class="col-sm-7 text-center text-sm-left mb-3 mb-sm-0"> 
        <h5 class="mb-0">Damodar Oil Mill</h5>  
          <address>
            Station Road,<br />
            Jasdan, Gujarat, India.<br />
            Contact: 02821-220555, 78029 94555<br />
            Email: damodaroilmill@gmail.com<br />
            GSTIN/UIN: 24AACFD5899A1ZK <br />
            Company PAN: AACFD5899A <br />
          </address>
        </div>
        <div class="col-sm-5 text-center text-sm-right">
          <h4 class="mb-0">Invoice</h4>
          <p class="mb-0">Invoice Number - <?php echo $order_list['bill_number']; ?></p>
        </div>
      </div>
      <hr>
    </header>

    <!-- Main Content -->
    <main>
      <div class="row">
        <div class="col-sm-6 mb-2"> <strong>Despatch No.:</strong> <?php echo $order_list['vehical_number']; ?><span></span> </div>
        <div class="col-sm-6 mb-2 text-sm-right"> <strong> Date:</strong> <?php echo  date('d/m/Y', strtotime($order_list['date'])); ?><span></span> </div>
      </div>
      <hr class="mt-0">
      <div class="row">
        <div class="col-sm-6">Party: <strong> <?php echo $order_list['shop_name']; ?></strong>
          <address>
            <!-- <?php echo $order_list['shop_owner']; ?><br /> -->
            <?php echo $order_list['city']; ?>, Gujarat, India.<br />
            GSTIN/UIN: <?php echo $order_list['GSTIN']; ?><br />
          </address>
        </div>
        <div class="col-sm-6"> <strong> Company Bank Details</strong>
          <address>
            Bank name: ICICI Bank Ltd <br />
            A/C: ICICI Bank Ltd <br />
            Branch & IFSC: Jasdan & ICIC0001708 <br />
           
          </address>
        </div>
      </div>
      <div class="card">
        <div class="card-header py-0">
          <table class="table mb-0">
            <thead>
              <tr style="">
                <td class="border-0 header_no text-left"><strong>No.</strong></td>
                <td class="border-0 header_desc"><strong>Description</strong></td>
                <td class="border-0 header_qty"><strong>
                    <center>QTY</center>
                <td class="border-0 header_rate"><strong>
                    <center>Rate</center>
                  </strong></td>
                </strong></td>
                <td class="border-0 header_amt"><strong>
                    <center>Amount</center>
                  </strong></td>
              </tr>
            </thead>
          </table>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table">
              <tbody>
                <?php
                $i = 0;
                foreach ($product_master as $row) {
                  $i = $i + 1;

                ?>
                  <tr>
                    <td class="header_no border-0" style="padding: 0px 5px !important;"><?php echo $i; ?></td>
                    <td class="header_desc border-0" style="padding: 0px 5px !important;"><?php echo $row['product_name'] . '<br><p class="cat_font">' . $row['cat_name'] . '</p>'; ?></td>
                   
                    <td class="header_qty border-0" style="padding: 0px !important;">
                      <center><?php echo $row['qty'] . '<br><p class="cat_font">(' . $row['total_kg'] . ' Kg)</p>'; ?></center>
                    </td>
                    <td class="header_rate border-0" style="padding: 0px!important;">
                      <center><?php echo $row['price']; ?></center>
                    </td>
                    <td class="header_amt border-0" style="padding: 0px !important;">
                      <center> <?php echo $row['total_amount']; ?></center>
                    </td>
                  </tr>
                <?php } ?>
                <tr>
                  <td colspan="2" class="bg-light-2 text-right"><strong>Total:</strong></td>
                  <td class="bg-light-2 header_qty">
                    <center> <?php echo $order_list['total_qty']. '<br><span class="cat_font">(' . $order_list['total_kg'] . ' Kg)</span>' ?></center>
                  </td>
                  <td class="bg-light-2 header_qty">
                  </td>
                  <td class="bg-light-2 header_amt text-right"> <?php echo number_format($order_list['total_amount'], 2); ?></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <br>
      <p class="text-1 text-muted">Amount in words: <strong><?php echo getIndianCurrency($order_list['total_amount']); ?></strong></p>
    </main>
    <!-- Footer -->
    <footer class="text-center">
      <!-- <hr> -->
      <!-- <p><strong>Koice Inc.</strong><br>
        4th Floor, Plot No.22, Above Public Park, 145 Murphy Canyon Rd,<br>
        Suite 100-18, San Diego CA 2028. </p> -->
      <div class="row">
        <div class="col-sm-8">
          <table class="table" style="border:1px solid rgba(0,0,0,.125)">
            <tbody>
              <tr>
                <td>Taxable Amount</td>
                <td>Central Tax</td>
                <td>State Tax</td>
                <td>Total Tax Amount</td>
              </tr>
              <tr>
                <td></td>
                <td><?php echo $cgst;?>%</td>
                <td><?php echo $sgst;?>%</td>
                <td></td>
              </tr>
              <tr>
                <td><?php echo number_format($taxable_amt, 2); ?></td>
                <td><?php echo number_format($cgst_amt, 2); ?></td>
                <td><?php echo number_format($sgst_amt, 2); ?></td>
                <td><?php echo number_format($tax_total, 2); ?></td>
              </tr>
            </tbody>
          </table>
        </div>
        <div class="col-sm-4">for <strong> DAMODAR OIL MILL</strong>
          <address>
            <br />
            <br />
            <br />
            <br />
            Authorised Signatory
          </address>
        </div>
      </div>
      <hr>
      <p class="text-1 text-muted text-left">Notes: SUBJECT TO JASDAN JURISDICTION</p>
      <p class="text-1">**** <strong> SHREE HARIKRUPA HI KEVALAM </strong>****</p>
      <div class="btn-group btn-group-sm d-print-none"> <a href="javascript:window.print()" class="btn btn-light border text-black-50 shadow-none"><i class="fa fa-print"></i> Print</a> </div>
    </footer>
  </div>
</body>

</html>