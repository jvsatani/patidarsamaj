<?php
include_once '../db.php';
include_once '../functions.php';
db_connect();
$where = "";
if (!empty($_REQUEST['f_id'])) {
	$where .= " and t.bank_type=:f_id ";
}
if (!empty($_REQUEST['uid'])) {
	$where .= " and t.uid=:uid ";
}

$query = $con->prepare("SELECT t.*,CONCAT(u.`middle_name`,' ',u.`father_name`,' ', IFNULL(LCASE(s.`name`),'')) full_name,u.member_id
FROM kharsh_transcription t
LEFT JOIN user u ON u.uid=t.by_uid
LEFT JOIN surname s ON s.id=u.surname_id
WHERE t.status >= '1' {$where}
			");
if (!empty($_REQUEST['f_id'])) {
	$query->bindValue(":f_id", "{$_REQUEST['f_id']}");
}
if (!empty($_REQUEST['uid'])) {
	$query->bindValue(":uid", "{$_REQUEST['uid']}");
}

$query->execute();
$group = array();
if ($query->rowCount() > 0) {
	$group = $query->fetchAll(PDO::FETCH_ASSOC);
}


$queryA = $con->prepare("SELECT sum(t.amount) amount
		FROM `kharsh_transcription` t 
			 WHERE t.status='1'   {$where}
				");
if (!empty($_REQUEST['f_id'])) {
	$queryA->bindValue(":f_id", "{$_REQUEST['f_id']}");
}
if (!empty($_REQUEST['uid'])) {
	$queryA->bindValue(":uid", "{$_REQUEST['uid']}");
}
$queryA->execute();
$isData = 0;
$list = array();
$original_data = array();
if ($queryA->rowCount() > 0) {
	$list = $queryA->fetch(PDO::FETCH_ASSOC);
	if (!empty($list['amount'])) {
		$list['amount'] = $list['amount'];
	} else {
		$list['amount'] = '0';
	}
}


$users = getRow("SELECT *
			FROM purchase_account 
			WHERE status = '1'  and id=:uid", array('uid' => $_REQUEST['uid']));;
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>સમસ્ત પાટીદાર સમાજ - ગુજરાત</title>
	<meta name="author" content="harnishdesign.net">

	<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900' type='text/css'>
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css" />
	<link rel="stylesheet" type="text/css" href="vendor/font-awesome/css/all.min.css" />
	<link rel="stylesheet" type="text/css" href="css/stylesheet.css" />
</head>
<style>
	.header_no {
		width: 8%;
		text-align: left;
	}

	.header_name {
		width: 30%;
		text-align: left;
	}

	.header_desc {
		width: 35%;
		text-align: left;
	}

	.header_rate {
		width: 10%;
		text-align: left;
	}

	.header_qty {
		width: 12%;
		text-align: left;
	}

	.header_amt {
		width: 1015%;
	}

	.cat_font {
		font-size: 10px;
		margin-bottom: 0rem;
	}

	.card-body {
		padding: 5px;
		padding-left: 0px;
		padding-right: 0px;
	}

	.tableDetails tr {
		border-bottom: 1px solid rgba(0, 0, 0, .1);
	}
</style>

<body>
	<div class="container-fluid invoice-container">
		<header>
			<div class="row align-items-center">
				<div class="col-sm-7 text-center text-sm-left mb-3 mb-sm-0">
				</div>
				<div class="col-sm-5 text-center text-sm-right">
					<p class="mb-0">પબ્લીક ટ્રસ્ટ રજી. નં. ઈ. ૨૦૯૨</p>
				</div>
			</div>
			<div class="row align-items-center">
				<div class="col-sm-12 text-center text-sm-center mb-3 mb-sm-0">
					<h4 class="mb-0"><strong>શ્રી લેઉવા પટેલ નૂતન કેળવણી મંડળ - જસદણ</strong></h4>

				</div>
			</div>
			<hr>
		</header>

		<!-- Main Content -->
		<main>
			<div class="row">
				<div class="col-sm-6 mb-2"> <strong> ખર્ચ </strong>

					<?php if (!empty($_REQUEST['uid'])) { ?><br>
					Payment To	<strong> <?php echo $users['business_name']?> </strong><br>
						 <?php echo $users['name']?> <br>
						 <?php echo $users['mobile']?> <br>
					<?php echo $users['address']?> <br>
					<?php } ?>
				</div>
				<div class="col-sm-6 mb-2 text-sm-right"> <strong> તારીખ:</strong> <?php echo date('d/m/Y') ?><span></span> </div>
			</div>
			<div class="row">
				<div class="col-sm-6 mb-2">

				</div>
			</div>

			<hr class="mt-0">

			<div class="card">
				<div class="card-header py-0">
					<table class="table mb-0">
						<thead>
							<tr style="">
								<td class="border-0 header_no text-left"><strong>નંબર</strong></td>

								<td class="border-0 header_desc"><strong>
										વિગત
									</strong></td>
								<td class="border-0 header_qty"><strong>
										મોડ
									</strong></td>
								<td class="border-0 header_qty"><strong>
										તારીખ
									</strong></td>
								<td class="border-0 header_qty"><strong>
										રૂપિયા
									</strong></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table tableDetails">
							<tbody>
								<?php
								$i = 0;
								foreach ($group as $row) {
									$i = $i + 1;
									$dateT = date('d/m/Y', strtotime($row['date']));
								?>
									<tr>
										<td class="header_no border-0" style="padding: 0px 5px !important;"><?php echo $row['bill_id']; ?></td>

										<td class="header_desc border-0" style="padding: 0px !important;">
											<sapn style="float:left"><?php echo $row['notes'] . '<p class="cat_font">' . $row['full_name'] . '</p>'; ?>
										</td>
										<td class="header_qty border-0" style="padding: 0px!important;">
											<?php echo ucfirst($row['bank_type']); ?>
										</td>
										<td class="header_qty border-0" style="padding: 0px!important;">
											<?php echo $dateT; ?>
										</td>
										<td class="header_qty border-0" style="padding: 0px !important;">
											<?php echo number_format($row['amount'], 2); ?>
										</td>
									</tr>
								<?php } ?>
								<tr>
									<td class="bg-light-2 header_qty"></td>
									<td colspan="3" class="bg-light-2 text-right"><strong>ટોટલ: </strong></td>
									<td class="bg-light-2 header_qty">
										<?php echo number_format($list['amount'], 2); ?>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<br>
			<!-- <p class="text-1 text-muted">Amount in words: <strong><?php echo getIndianCurrency($list['amount']); ?></strong></p> -->
		</main>
		<!-- Footer -->
		<footer class="text-center">
			<!-- <hr> -->
			<!-- <p><strong>Koice Inc.</strong><br>
        4th Floor, Plot No.22, Above Public Park, 145 Murphy Canyon Rd,<br>
        Suite 100-18, San Diego CA 2028. </p> -->
			<hr>
			<!-- <p class="text-1">**** <strong> Jay Sardar </strong>****</p> -->
			<div class="btn-group btn-group-sm d-print-none"> <a href="javascript:window.print()" class="btn btn-light border text-black-50 shadow-none"><i class="fa fa-print"></i> Print</a> </div>
		</footer>
	</div>
</body>

</html>
<script type="text/javascript">
	setTimeout(function() {
		window.print();
	}, 2000);
</script>