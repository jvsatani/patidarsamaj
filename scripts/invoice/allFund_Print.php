<?php
include_once '../db.php';
include_once '../functions.php';
db_connect();
$where = "";
if (!empty($_REQUEST['f_id'])) {
	$where .= " and t.fund_type=:f_id ";
}
if (!empty($_REQUEST['r_id'])) {
	$where .= " and t.by_uid=:r_id ";
}
if (!empty($_REQUEST['emp_id'])) {
	$where .= " and t.uid=:emp_id ";
}

if (!empty($_REQUEST['sdate']) && !empty($_REQUEST['edate'])) {
	$start_date = $_REQUEST['sdate'];
	$start_date = DateTime::createFromFormat("d/m/Y", $start_date);
	$sdate = $start_date->format('Y-m-d');

	$end_date = $_REQUEST['edate'];
	$end_date = DateTime::createFromFormat("d/m/Y", $end_date);
	$edate = $end_date->format('Y-m-d');

	$where .= " and ((date(t.date) BETWEEN '{$sdate}' AND '{$edate}') OR (date(t.date) >= '{$sdate}' AND date(t.date) <= '{$edate}'))";
}

$query = $con->prepare("SELECT t.*,CONCAT(u.`middle_name`,' ',u.`father_name`,' ', IFNULL(s.`name`,'')) full_name,u.member_id,CONCAT(uu.`middle_name`,' ',uu.`father_name`,' ', ss.`name`) R_full_name,f.name fund_name
FROM user_transcription t
LEFT JOIN user u ON u.uid=t.uid
LEFT JOIN user uu ON uu.uid=t.by_uid
LEFT JOIN fund_type f ON f.id=t.fund_type
LEFT JOIN surname s ON s.id=u.surname_id
LEFT JOIN surname ss ON ss.id=uu.surname_id
WHERE t.status >= '1' {$where}
			");
if (!empty($_REQUEST['r_id'])) {
	$query->bindValue(":r_id", "{$_REQUEST['r_id']}");
}
if (!empty($_REQUEST['f_id'])) {
	$query->bindValue(":f_id", "{$_REQUEST['f_id']}");
}
if (!empty($_REQUEST['emp_id'])) {
	$query->bindValue(":emp_id", "{$_REQUEST['emp_id']}");
}
$query->execute();
$group = array();
if ($query->rowCount() > 0) {
	$group = $query->fetchAll(PDO::FETCH_ASSOC);
}


$queryA = $con->prepare("SELECT sum(t.amount) amount
		FROM `user_transcription` t 
			 WHERE t.status='1'  {$where}
				");
if (!empty($_GET['f_id'])) {
	$queryA->bindValue(":f_id", "{$_GET['f_id']}");
}
if (!empty($_GET['r_id'])) {
	$queryA->bindValue(":r_id", "{$_GET['r_id']}");
}
if (!empty($_GET['emp_id'])) {
	$queryA->bindValue(":emp_id", "{$_GET['emp_id']}");
}
$queryA->execute();
$isData = 0;
$list = array();
$original_data = array();
if ($queryA->rowCount() > 0) {
	$list = $queryA->fetch(PDO::FETCH_ASSOC);
	if (!empty($list['amount'])) {
		$list['amount'] = $list['amount'];
	} else {
		$list['amount'] = '0';
	}
}

$fund_type = getRow("SELECT *
			FROM fund_type
			WHERE status = 1 and id=:id", array('id' => $_REQUEST['f_id']));

$users = getRow("SELECT *,CONCAT(u.`middle_name`,' ',u.`father_name`,' ', IFNULL(s.`name`,'')) full_name
			FROM user u
			LEFT JOIN surname s ON s.id=u.surname_id
			WHERE u.status = '2' and (u.type='management' or u.type='admin') and uid=:id", array('id' => $_REQUEST['r_id']));;

$empName = getRow("SELECT *,CONCAT(u.`middle_name`,' ',u.`father_name`,' ', IFNULL(s.`name`,'')) full_name
			FROM user u
			LEFT JOIN surname s ON s.id=u.surname_id
			WHERE u.status = '2' and uid=:id", array('id' => $_REQUEST['emp_id']));;


?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>સમસ્ત પાટીદાર સમાજ - ગુજરાત</title>
	<meta name="author" content="harnishdesign.net">

	<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900' type='text/css'>
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css" />
	<link rel="stylesheet" type="text/css" href="vendor/font-awesome/css/all.min.css" />
	<link rel="stylesheet" type="text/css" href="css/stylesheet.css" />
</head>
<style>
	.header_no {
		width: 8%;
		text-align: left;
	}

	.header_desc {
		width: 30%;
		text-align: left;
	}

	.header_rate {
		width: 10%;
		text-align: left;
	}

	.header_qty {
		width: 12%;
		text-align: left;
	}

	.header_amt {
		width: 1015%;
	}

	.cat_font {
		font-size: 10px;
		margin-bottom: 0rem;
	}

	.card-body {
		padding: 5px;
		padding-left: 0px;
		padding-right: 0px;
	}

	.tableDetails tr {
		border-bottom: 1px solid rgba(0, 0, 0, .1);
	}
</style>

<body>
	<div class="container-fluid invoice-container">
		<header>
			<div class="row align-items-center">
				<div class="col-sm-7 text-center text-sm-left mb-3 mb-sm-0">
				</div>
				<div class="col-sm-5 text-center text-sm-right">
					<p class="mb-0">પબ્લીક ટ્રસ્ટ રજી. નં. ઈ. ૨૦૯૨</p>
				</div>
			</div>
			<div class="row align-items-center">
				<div class="col-sm-12 text-center text-sm-center mb-3 mb-sm-0">
					<h4 class="mb-0"><strong>શ્રી લેઉવા પટેલ નૂતન કેળવણી મંડળ - જસદણ</strong></h4>

				</div>
			</div>
			<hr>
		</header>

		<!-- Main Content -->
		<main>
			<div class="row">
				<div class="col-sm-6 mb-2">
					<?php
					if (!empty($_REQUEST['sdate']) && !empty($_REQUEST['edate'])) {
						echo "<strong>Select Date:</strong> " . $_REQUEST['sdate'] . " - " . $_REQUEST['edate'] . "<span></span>";
					} else {
						echo "<strong>Select Date:</strong> All<span></span>";
					} ?>
				</div>
				<div class="col-sm-6 mb-2 text-sm-right"> <strong> તારીખ:</strong> <?php echo date('d/m/Y') ?><span></span> </div>
			</div>
			<div class="row">
				<div class="col-sm-6 mb-2">
					<?php
					if (!empty($_REQUEST['f_id'])) {
						echo "<strong>Fund Type:</strong> " . $fund_type['name'] . "<span></span>";
					} else {
						echo "<strong>Fund Type:</strong> All<span></span>";
					} ?>
				</div>
				<div class="col-sm-6 mb-2 text-sm-right">
					<?php
					if (!empty($_REQUEST['r_id'])) {
						echo "<strong>ફંડ ઉઘરાવનાર:</strong> " .  $users['full_name'] . "<span></span>";
					} else {
						echo "<strong>ફંડ ઉઘરાવનાર:</strong> All<span></span>";
					} ?>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12 mb-2 text-sm-left">
					<?php
					if (!empty($_REQUEST['emp_id'])) {
						echo "<strong>Member Name:</strong> " .  $empName['full_name'] . "<span></span>";
					} ?>
				</div>
			
			</div>
			<hr class="mt-0">

			<div class="card">
				<div class="card-header py-0">
					<table class="table mb-0">
						<thead>
							<tr style="">
								<td class="border-0 header_no text-left"><strong>નંબર</strong></td>
								<td class="border-0 header_desc"><strong>
										નામ
									</strong></td>
								<td class="border-0 header_desc"><strong>
										વિગત
									</strong></td>
								<td class="border-0 header_qty"><strong>
										તારીખ
									</strong></td>
								<td class="border-0 header_qty"><strong>
										રૂપિયા
									</strong></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table tableDetails">
							<tbody>
								<?php
								$i = 0;
								foreach ($group as $row) {
									$i = $i + 1;
									$dateT = date('d/m/Y', strtotime($row['date']));

								?>
									<tr>
										<td class="header_no border-0" style="padding: 0px 5px !important;"><?php echo $row['bill_id']; ?></td>
										<td class="header_desc border-0" style="padding: 0px !important;"><?php echo ucfirst($row['full_name']); ?></td>

										<td class="header_desc border-0" style="padding: 0px !important;">
											<sapn style="float:left"><?php echo $row['fund_name'] . '</sapn><p class="cat_font"> &nbsp;&nbsp;(' . $row['notes'] . ')</p>'; ?>
										</td>
										<td class="header_qty border-0" style="padding: 0px!important;">
											<center><?php echo $dateT; ?></center>
										</td>
										<td class="header_qty border-0" style="padding: 0px !important;">
											<center><?php echo number_format($row['amount'], 2); ?></center>
										</td>
									</tr>
								<?php } ?>
								<tr>
									<td class="bg-light-2 header_qty"></td>
									<td class="bg-light-2 header_qty"></td>
									<td colspan="2" class="bg-light-2 text-right"><strong>ટોટલ: </strong></td>
									<td class="bg-light-2 header_amt text-right">
										<center><?php echo number_format($list['amount'], 2); ?></center>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<br>
			<!-- <p class="text-1 text-muted">Amount in words: <strong><?php echo getIndianCurrency($list['amount']); ?></strong></p> -->
		</main>
		<!-- Footer -->
		<footer class="text-center">
			<!-- <hr> -->
			<!-- <p><strong>Koice Inc.</strong><br>
        4th Floor, Plot No.22, Above Public Park, 145 Murphy Canyon Rd,<br>
        Suite 100-18, San Diego CA 2028. </p> -->
			<hr>
			<!-- <p class="text-1">**** <strong> Jay Sardar </strong>****</p> -->
			<div class="btn-group btn-group-sm d-print-none"> <a href="javascript:window.print()" class="btn btn-light border text-black-50 shadow-none"><i class="fa fa-print"></i> Print</a> </div>
		</footer>
	</div>
</body>

</html>
<script type="text/javascript">
	setTimeout(function() {
		window.print();
	}, 2000);
</script>