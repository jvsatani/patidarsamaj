<?php
function uploadImage($file, $name = "")
{
	global $proj_dir;
	$upload_path = getSetting("upload_path");
	$image_width = getSetting("img_width");
	$image_height = getSetting("img_height");

	$time = md5(microtime());

	// validate file
	$whitelist = array(
		".jpg",
		".jpeg",
		".gif",
		".png",
		".bmp"
	);

	$val_img = true;

	if ($val_img == false) {
		$msg = "Invalid image format. Vaild formats are: .jpg, .jpeg, .gif, .png, .bmp.";
		return "";
	} else {
		$base_name_pic = str_replace(" ", "_", basename($file['name']));
		$base_name_pic = str_replace("#", "_", $base_name_pic);
		$file_name = $name . $time . $base_name_pic;

		$ROOT = $_SERVER['DOCUMENT_ROOT'] . "/" . $proj_dir . $upload_path . "/";

		if (!move_uploaded_file($file['tmp_name'], $ROOT . $file_name)) {
			$msg = "Error occured while uploading document.";
			echo $msg;
			return "";
		} else {
			// optimizing
			// get image size
			$path = "{$ROOT}{$file_name}";
			$arr = explode(".", $path);
			$extension = $arr[count($arr) - 1];

			$imgInfo = getimagesize("{$path}");
			if ($imgInfo[0] > $image_width) {
				exec("convert \"{$path}\" -resize " . $image_width . "x" . $image_height . " \"{$path}\"");
			}

			if ($extension == "png") {
				exec("optipng \"{$path}\" --out \"{$path}\"");
			} else {
				exec("jpegoptim \"{$path}\" --max=90 --strip-all --preserve --totals");
			}
			return $file_name;
			// $file_path_for_pic = "/" . $file_path_for_pic;
		}
	}
}

function uploadFile($file)
{
	global $proj_dir;
	$name = "";
	$upload_path = getSetting("upload_path");

	$time = md5(microtime());

	// validate file
	$whitelist = array(
		".jpg",
		".jpeg",
		".gif",
		".png",
		".bmp"
	);

	$val_img = true;

	if ($val_img == false) {
		$msg = "Invalid pdf format. Vaild formats are: .pdf.";
		return "";
	} else {
		$base_name_pic = str_replace(" ", "_", basename($file['name']));
		$base_name_pic = str_replace("#", "_", $base_name_pic);
		$file_name = $name . $time . $base_name_pic;

		$ROOT = $_SERVER['DOCUMENT_ROOT'] . "/" . $proj_dir . $upload_path . "/";

		if (!move_uploaded_file($file['tmp_name'], $ROOT . $file_name)) {
			$msg = "Error occured while uploading document.";
			echo $msg;
			return "";
		} else {
			// optimizing
			// get image size
			$path = "{$ROOT}{$file_name}";
			$arr = explode(".", $path);
			$extension = $arr[count($arr) - 1];

			return  $file_name;
			// $file_path_for_pic = "/" . $file_path_for_pic;
		}
	}
}
function uploadFileS($file)
{
	global $proj_dir;
	$name = "";
	$b_defualt = '';
	// if (!empty($fiilePath)) {
	// 	$b_defualt = $fiilePath;
	// }
	$upload_path = getSetting("upload_path");

	$time = md5(microtime());

	// validate file
	$whitelist = array(
		".pdf",
		".jpg",
		".jpeg",
		".gif",
		".png",
		".bmp"
	);

	$val_img = true;

	if ($val_img == false) {
		$msg = "Invalid pdf format. Vaild formats are: .pdf.";
		return "";
	} else {
		$base_name_pic = str_replace(" ", "_", basename($file['name']));
		$base_name_pic = str_replace("#", "_", $base_name_pic);
		$file_name = $name . $time . $base_name_pic;

		$ROOT = $_SERVER['DOCUMENT_ROOT'] . "/" . $proj_dir . $upload_path . "/";

		if (!move_uploaded_file($file['tmp_name'], $ROOT . $file_name)) {
			$msg = "Error occured while uploading document.";
			echo $msg;
			return "";
		} else {
			// optimizing
			// get image size
			$path = "{$ROOT}{$file_name}";
			$arr = explode(".", $path);
			$extension = $arr[count($arr) - 1];

			return $b_defualt . '/' . $file_name;
			// $file_path_for_pic = "/" . $file_path_for_pic;
		}
	}
}
function setSettingByKey($key, $val)
{
	global $con;
	$sql = $con->prepare("UPDATE setting SET
							`val`=:val
 							WHERE `key`=:key");

	$sql->bindParam(":val", $val);
	$sql->bindParam(":key", $key);
	$sql->execute();
}
function logout()
{
	$_SESSION = array();
	unset($_SESSION);
}
function getSetting($key)
{
	global $con;
	$query = $con->prepare("SELECT `val`
			FROM `setting`
			WHERE `key`=:key");
	$query->bindParam(":key", $key);
	$query->execute();
	if ($query->rowCount() > 0) {
		$row = $query->fetch(PDO::FETCH_ASSOC);
		return $row['val'];
	} else {
		return false;
	}
}
function login()
{
	db_connect();
	$query = $con->prepare("select  u.*, if(m.file_name is null or m.file_name='','user.gif',m.file_name) file_name
		from user u
		LEFT JOIN media m
		ON u.uid=m.rec_id and m.rec_type='user'
			WHERE u.mobile=:mobile and u.password=:password  and weblogin_enabled=1");
	$query->bindParam(":mobile", $mobile);
	$query->bindParam(":password", $password);
	$query->execute();
	if ($query->rowCount() == 1) {
		$admin = $query->fetch(PDO::FETCH_ASSOC);
		$_SESSION['admin'] = $admin;

		$query = $con->prepare("update user set last_seen=now()	where mobile=:mobile");
		$query->bindParam(":mobile", $mobile);

		$query->execute();

		if (isset($url) && $url != "") {
			header("Location: $url");
		} else {
			header("Location: index.php");
		}
	} else {
		$msg = "Wrong Mobile or Password.";
	}
}
function getFullImage($path)
{
	global $siteURL;
	$upload_path = getSetting("upload_path");
	if (!empty($path)) {
		return $siteURL . $upload_path . $path;
	} else {
		return "";
	}
}
function getRingImage($path)
{
	global $siteURL;
	$upload_path = getSetting("ring_path");
	if (!empty($path)) {
		return $siteURL . $upload_path . $path;
	} else {
		return "";
	}
}
function getSettingImage($path)
{
	global $siteURL;
	$upload_path = getSetting("setting_path");
	if (!empty($path)) {
		return $siteURL . $upload_path . $path;
	} else {
		return "";
	}
}
function getSketchImage($path)
{
	global $siteURL;
	$upload_path = getSetting("sketch_path");
	if (!empty($path)) {
		return $siteURL . $upload_path . $path;
	} else {
		return "";
	}
}
function getStoneImage($path)
{
	global $siteURL;
	$upload_path = getSetting("stons_path");
	if (!empty($path)) {
		return $siteURL . $upload_path . $path;
	} else {
		return "";
	}
}

function sendMail($mailData, &$error)
{
	global $siteURL, $SMTP_HOST, $SMTP_USER, $SMTP_PASSWORD, $SMTP_FROM, $SMTP_PORT, $SMTP_FROM_EMAIL;
	require_once 'mailer/PHPMailerAutoload.php';

	if (trim($mailData['email']) == "") {
		return false;
	}

	$html = file_get_contents($siteURL . 'email/common.php');
	$html = str_replace("{BODY}", $mailData['body'], $html);


	$mail = new PHPMailer();
	//$mail->SMTPDebug = 3; // Enable verbose debug output
	$mail->isSMTP(); // Set mailer to use SMTP
	$mail->Host = $SMTP_HOST; // 'mail.leadinfosoft.com'; // Specify main and backup SMTP servers
	$mail->SMTPAuth = true; // Enable SMTP authentication
	$mail->Username = $SMTP_USER; // 'info@leadinfosoft.com'; // SMTP username
	$mail->Password = $SMTP_PASSWORD; // SMTP password
	//$mail->SMTPSecure = 'tls'; // Enable TLS encryption, `ssl` also accepted
	$mail->Port = $SMTP_PORT; // TCP port to connect to

	$mail->From = $SMTP_FROM_EMAIL; //'noreply@leadinfosoft.com';
	$mail->FromName = $SMTP_FROM; // "LoadBoard";
	$mail->addAddress($mailData['email'], $mailData['name']); // Add a recipient
	$mail->isHTML(true); // Set email format to HTML

	$mail->Subject = $mailData['sub'];
	$mail->Body = $html;

	if (!$mail->send()) {
		// echo 'Message could not be sent.';
		$error = $mail->ErrorInfo;
		return false;
	} else {
		return true;
	}
}
function sendMailAll($mailData, &$error)
{
	global $siteURL, $SMTP_HOST, $SMTP_USER, $SMTP_PASSWORD, $SMTP_FROM, $SMTP_PORT, $SMTP_FROM_EMAIL;
	require_once 'mailer/PHPMailerAutoload.php';

	if (trim($mailData['email']) == "") {
		return false;
	}

	$html = file_get_contents($siteURL . 'email/common.php');
	$html = str_replace("{BODY}", $mailData['body'], $html);

	$mail = new PHPMailer();
	//$mail->SMTPDebug = 3; // Enable verbose debug output
	$mail->isSMTP(); // Set mailer to use SMTP
	$mail->Host = $SMTP_HOST; // 'mail.leadinfosoft.com'; // Specify main and backup SMTP servers
	$mail->SMTPAuth = true; // Enable SMTP authentication
	$mail->Username = $SMTP_USER; // 'info@leadinfosoft.com'; // SMTP username
	$mail->Password = $SMTP_PASSWORD; // SMTP password
	//$mail->SMTPSecure = 'tls'; // Enable TLS encryption, `ssl` also accepted
	$mail->Port = $SMTP_PORT; // TCP port to connect to

	$mail->From = $SMTP_FROM_EMAIL; //'noreply@leadinfosoft.com';
	$mail->FromName = $SMTP_FROM; // "LoadBoard";
	//$mail->addAddress ( $mailData ['email'], $mailData ['name'] ); // Add a recipient
	foreach ($mailData['alluser'] as $email) {
		$mail->addCC($email);
	}
	$mail->isHTML(true); // Set email format to HTML

	$mail->Subject = $mailData['sub'];
	$mail->Body = $html;

	if (!$mail->send()) {
		// echo 'Message could not be sent.';
		$error = $mail->ErrorInfo;
		return false;
	} else {
		return true;
	}
}


function sendCertificatePush($uid, $body)
{
	global $APP_MODE;
	include_once("push/GCM.php");
	$notification = array(
		"type" => "general",
		"body" => $body
	);

	$devs = getRows("select dev_type,dev_token from devices where uid=:uid and status=1 order by dev_type", array("uid" => $uid));
	$android = array();
	$ios = array();
	foreach ($devs as $d) {
		if ($d['dev_type'] == 0) {
			$android[] = $d['dev_token'];
		} else {
			$ios[] = $d['dev_token'];
		}
	}

	if (count($android) > 0) {


		$android_tokens = $android;

		$gcm = new GCM();
		$message_1 = array(
			"message" => $notification
		);

		$result = $gcm->send_notification($android_tokens, $message_1);

		$android_success = $result->success;
		$android_failure = $result->failure;

		$success_count = $android_success;
	}

	if (count($ios) > 0) {

		foreach ($ios as $dev_token) {
			$payload = array();
			$payload['aps'] = array(
				'alert' => $notification,
				'badge' => 1,
				'sound' => 'default'
			);

			$payload = json_encode($payload);
			$apnsPort = 2195;
			$apnsCert = 'push/dev.pem';

			if ($APP_MODE == MODE_PRODUCTION) {
				$apnsHost = 'gateway.push.apple.com';
			} else {
				$apnsHost = 'gateway.sandbox.push.apple.com';
			}

			$pass = 'lead';

			$streamContext = stream_context_create();

			stream_context_set_option($streamContext, 'ssl', 'local_cert', $apnsCert);
			stream_context_set_option($streamContext, 'ssl', 'passphrase', $pass);
			$apns = stream_socket_client('ssl://' . $apnsHost . ':' . $apnsPort, $error, $errorString, 2, STREAM_CLIENT_CONNECT, $streamContext);

			if (!$apns) {
				print "Push Error: Failed to connect $error $errorString \n";
				exit();
			} else {
				$apnsMessage = chr(0) . chr(0) . chr(32) . pack('H*', str_replace(' ', '', $dev_token)) . chr(0) . chr(strlen($payload)) . $payload;
				try {
					fwrite($apns, $apnsMessage);
					//echo "Sent!";
				} catch (Exception $e) {
					var_dump($e);
				}

				fclose($apns);
			}
		}
	}
}


function getIndianCurrency($number)
{
	$decimal = round($number - ($no = floor($number)), 2) * 100;
	$hundred = null;
	$digits_length = strlen($no);
	$i = 0;
	$str = array();
	$words = array(
		0 => '', 1 => 'એક', 2 => 'બે',
		3 => 'ત્રણ', 4 => 'ચાર', 5 => 'પાંચ', 6 => 'છ',
		7 => 'સાત', 8 => 'આઠ', 9 => 'નવ',
		10 => 'દસ', 11 => 'અગીયાર', 12 => 'બાર',
		13 => 'તેર', 14 => 'ચૌદ', 15 => 'પંદર',
		16 => 'સોળ', 17 => 'સતર', 18 => 'અઢાર',
		19 => 'ઓગણીસ', 20 => 'વિસ', 30 => 'ત્રીસ',
		40 => 'ચાલીસ', 50 => 'પચાસ', 60 => 'સાંઇઠ',
		70 => 'સિત્તેર', 80 => 'એંસી', 90 => 'નેંવુ'
	);
	$digits = array('', 'સો', 'હજાર', 'લાખ', 'કરોડ');
	while ($i < $digits_length) {
		$divider = ($i == 2) ? 10 : 100;
		$number = floor($no % $divider);
		$no = floor($no / $divider);
		$i += $divider == 10 ? 1 : 2;
		if ($number) {
			$plural = (($counter = count($str)) && $number > 9) ? 's' : null;
			$hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
			$str[] = ($number < 21) ? $words[$number] . ' ' . $digits[$counter] . $plural . ' ' . $hundred : $words[floor($number / 10) * 10] . ' ' . $words[$number % 10] . ' ' . $digits[$counter] . $plural . ' ' . $hundred;
		} else $str[] = null;
	}
	$Rupees = implode('', array_reverse($str));
	$paise = ($decimal > 0) ? "." . ($words[$decimal / 10] . " " . $words[$decimal % 10]) . ' Paise' : '';
	return ($Rupees ? $Rupees . 'Rupees ' : '') . $paise;
}

function notificationSend($notificationToken, $message)
{
	$content = array(
		"en" => $message
	);

	$headings = array(
		"en" => 'Damodar Oil Mill'
	);

	$fields = array(
		'app_id' => "21b84e85-d19f-4017-bc44-e9ed640d2e3a",
		// 'included_segments' => array('All'),
		'include_player_ids' => [$notificationToken],
		// 'data' => array("foo" => "bar"),
		//  'large_icon' => 'http://www.samayexpress.in/images/uploads/'.'a324c300a9556a9d6a3643400db4c896IMG-20200724-WA0045.jpg',
		// 'big_picture' => 'http://www.samayexpress.in/images/uploads/' . $newsImg,
		'contents' => $content,
		'headings' => $headings,
	);
	$fields = json_encode($fields);

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
	curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		'Content-Type: application/json; charset=utf-8',
		'Authorization: Basic ZTU3N2U4NmQtY2E2NS00YTM1LWIxODEtNWE1MzdjZjA5Y2Iz'
	));
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

	curl_setopt($ch, CURLOPT_HEADER, FALSE);
	curl_setopt($ch, CURLOPT_POST, TRUE);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

	$response = curl_exec($ch);
	curl_close($ch);
	// echo $response;
	return true;
}
function SendOTP($mobile, $OTP)
{
	try {
		$url = "smsbomb.online/sendsms.aspx";
		$fields = array(
			'mobile' => '9974010036',
			'pass' => 'DPSRF',
			'senderid' => 'KGDGTL',
			'to' => $mobile,
			'msg' => urlencode('One time OTP is ' . $OTP . ' for Patidar Samaj - Gujarat app. - Message send by Patidar Samaj.')
		);
		$fields_string = '';
		//url-ify the data for the POST
		foreach ($fields as $key => $value) {
			$fields_string .= $key . '=' . $value . '&';
		}
		rtrim($fields_string, '&');
		//open connection
		$ch = curl_init();
		//set the url, number of POST vars, POST data
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HEADER, true);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, count($fields));
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
		//execute post
		$result = curl_exec($ch);
		//close connection
		curl_close($ch);
		// return $result ;
	} catch (Exception $e) {
		// echo 'Message:' . $e->getMessage();
		return true;
	}
}

function SendFundSMS($mobile, $amount)
{
	try {
		$url = "smsbomb.online/sendsms.aspx";
		$fields = array(
			'mobile' => '9974010036',
			'pass' => 'DPSRF',
			'senderid' => 'SPENPL',
			'msgtype' => 'uc',
			'to' => $mobile,
			'msg' => urlencode('શ્રી લેઉવા પ. નુ. કે. મં. - જસદણ ને ' . $amount . ' ફાળો આપવા બદલ આપનો આભાર.')
			// 'msg' => urlencode('શ્રી લેઉવા પટેલ નુતન કેળવણી મંડળ - જસદણ ને ' . $amount . ' રૂપિયા ફાળો આપવા બદલ આપનો ખુબ ખુબ આભાર. https://bit.ly/3uV4dZA')
		);
		$fields_string = '';
		//url-ify the data for the POST
		foreach ($fields as $key => $value) {
			$fields_string .= $key . '=' . $value . '&';
		}
		rtrim($fields_string, '&');
		//open connection
		$ch = curl_init();
		//set the url, number of POST vars, POST data
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HEADER, true);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, count($fields));
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
		//execute post
		$result = curl_exec($ch);
		//close connection
		curl_close($ch);
		// return $result ;
	} catch (Exception $e) {
		// echo 'Message:' . $e->getMessage();
		return true;
	}
}
function conutMember($id, $type)
{
	db_connect();
	$role_master = getRow("SELECT COUNT(uid) as uid 
	FROM role_master 
	WHERE status = 1 and  city_id=:id and type=:attend", array('id' => $id, 'attend' => $type));
	return $role_master['uid'];
}

function menuRights($emp_id, $menu_key, $accessKey)
{
	$user = getRow("SELECT *
	FROM  user
	WHERE status = '2' and uid=:emp_id ", array('emp_id' => $emp_id));

	$rights_master = getRow("SELECT *
	FROM  rights_master
	WHERE status = '1' and uid=:emp_id and menu_key=:menu_key", array('emp_id' => $emp_id, 'menu_key' => $menu_key));
	if ($user && $user['type'] == 'management') {
		return true;
	}

	if ($user && $user['type'] == 'admin') {
		if ($rights_master['is_view'] == '1' && $accessKey == 'view') {
			return true;
		}
		if ($rights_master['is_add'] == '1' && $accessKey == 'add') {
			return true;
		}
		if ($rights_master['is_edit'] == '1' && $accessKey == 'edit') {
			return true;
		}
		if ($rights_master['is_delete'] == '1' && $accessKey == 'delete') {
			return true;
		}
		return false;
	} else {
		return false;
	}
	return false;
}

function employeeToRightAdd($uid)
{
	$menu_right = array(
		array('id' => '1', 'menu_name' => 'Member', 'menu_key' => 'member', 'status' => '1', 'isView' => '1', 'isAdd' => '0', 'isEdit' => '0', 'isDelete' => '0'),
		array('id' => '1', 'menu_name' => 'Fund', 'menu_key' => 'fund', 'status' => '1', 'isView' => '1', 'isAdd' => '0', 'isEdit' => '0', 'isDelete' => '0'),
		array('id' => '1', 'menu_name' => 'Loan', 'menu_key' => 'loan', 'status' => '1', 'isView' => '1', 'isAdd' => '0', 'isEdit' => '0', 'isDelete' => '0'),
		array('id' => '1', 'menu_name' => 'Kharsh', 'menu_key' => 'kharsh', 'status' => '1', 'isView' => '1', 'isAdd' => '0', 'isEdit' => '0', 'isDelete' => '0'),
		array('id' => '1', 'menu_name' => 'Notice', 'menu_key' => 'notice', 'status' => '1', 'isView' => '1', 'isAdd' => '0', 'isEdit' => '0', 'isDelete' => '0'),
		array('id' => '1', 'menu_name' => 'Sahay', 'menu_key' => 'sahai', 'status' => '1', 'isView' => '1', 'isAdd' => '0', 'isEdit' => '0', 'isDelete' => '0'),
		array('id' => '1', 'menu_name' => 'Job', 'menu_key' => 'job', 'status' => '1', 'isView' => '1', 'isAdd' => '0', 'isEdit' => '0', 'isDelete' => '0'),
		array('id' => '1', 'menu_name' => 'Document', 'menu_key' => 'document', 'status' => '1', 'isView' => '1', 'isAdd' => '0', 'isEdit' => '0', 'isDelete' => '0'),
		array('id' => '1', 'menu_name' => 'eBook', 'menu_key' => 'eBook', 'status' => '1', 'isView' => '1', 'isAdd' => '0', 'isEdit' => '0', 'isDelete' => '0'),
		array('id' => '1', 'menu_name' => 'Jaherat', 'menu_key' => 'jaherat', 'status' => '1', 'isView' => '1', 'isAdd' => '0', 'isEdit' => '0', 'isDelete' => '0'),
		array('id' => '1', 'menu_name' => 'Student', 'menu_key' => 'student', 'status' => '1', 'isView' => '1', 'isAdd' => '0', 'isEdit' => '0', 'isDelete' => '0'),
		array('id' => '1', 'menu_name' => 'Labharthi', 'menu_key' => 'labharthi', 'status' => '1', 'isView' => '1', 'isAdd' => '0', 'isEdit' => '0', 'isDelete' => '0'),
		array('id' => '1', 'menu_name' => 'Elders', 'menu_key' => 'elders', 'status' => '1', 'isView' => '1', 'isAdd' => '0', 'isEdit' => '0', 'isDelete' => '0'),
		array('id' => '1', 'menu_name' => 'Feedback', 'menu_key' => 'feedback', 'status' => '1', 'isView' => '1', 'isAdd' => '0', 'isEdit' => '0', 'isDelete' => '0'),
		array('id' => '1', 'menu_name' => 'Gallery', 'menu_key' => 'gallery', 'status' => '1', 'isView' => '1', 'isAdd' => '0', 'isEdit' => '0', 'isDelete' => '0'),
		array('id' => '1', 'menu_name' => 'Video', 'menu_key' => 'video', 'status' => '1', 'isView' => '1', 'isAdd' => '0', 'isEdit' => '0', 'isDelete' => '0'),
		array('id' => '1', 'menu_name' => 'Slider', 'menu_key' => 'slider', 'status' => '1', 'isView' => '1', 'isAdd' => '0', 'isEdit' => '0', 'isDelete' => '0'),
		array('id' => '1', 'menu_name' => 'Settings', 'menu_key' => 'settings', 'status' => '1', 'isView' => '0', 'isAdd' => '0', 'isEdit' => '0', 'isDelete' => '0'),
		array('id' => '1', 'menu_name' => 'Dashboard', 'menu_key' => 'dashboard', 'status' => '1', 'isView' => '1', 'isAdd' => '0', 'isEdit' => '0', 'isDelete' => '0'),
	);

	foreach ($menu_right as $row) {
		$data = array(
			"uid" => $uid,
			"status" => '1',
			"date" => phpNow()
		);
		$data['menu_name'] = $row['menu_name'];
		$data['menu_key'] = $row['menu_key'];
		$data['is_view'] = $row['isView'];
		$data['is_add'] = $row['isAdd'];
		$data['is_edit'] = $row['isEdit'];
		$data['is_delete'] = $row['isDelete'];
		$id = insertRow("rights_master", $data);
	}
}
