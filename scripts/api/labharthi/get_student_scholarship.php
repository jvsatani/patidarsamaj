<?php
include '../../db.php';
include '../../functions.php';
db_connect();
header("Content-Type: application/json");
header('Access-Control-Allow-Origin: *');


if (isset($_SERVER['HTTP_ORIGIN'])) {
	header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
	header('Access-Control-Allow-Credentials: true');
	header('Access-Control-Max-Age: 86400');    // cache for 1 day
}
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
		header("Access-Control-Allow-Methods: POST, POST, OPTIONS");

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
		header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

	exit(0);
}

// $query = $con->prepare("SELECT  * FROM `user` 
// 		WHERE  uid=:uid and is_Token=:is_Token");
// $query->bindParam(":is_Token", $request->is_Token);
// $query->bindParam(":uid", $request->uid);
// $query->execute();
// if ($query->rowCount() > 0) {
$limit = 100;
if (isset($_REQUEST['page'])) {
	$page  = $_REQUEST['page'];
} else {
	$page = 1;
};
$start_from = ($page - 1) * $limit;
$where = '';

$query = $con->prepare("SELECT s.*
		FROM student_scholarship s 
		where s.status = '1'
		 order by id desc 
		LIMIT $start_from, $limit
	");
$query->execute();

$List = array();
if ($query->rowCount() > 0) {
	$List = $query->fetchAll(PDO::FETCH_ASSOC);
	
	$completed_data = array();
	foreach ($List as $row) {
		$images_data = getRows("SELECT s.school_name,s.school_address,s.school_mobile,s.total_fees,s.scholarship_fees,s.donation_fees,sm.name,u.surname_id,u.middle_name,u.father_name,IFNULL(sn.`name`,'') surname_name
		FROM  student_scholarship_info s
		LEFT JOIN std_master sm ON sm.id = s.std_id
		LEFT JOIN user u ON u.uid = s.donation_id
		LEFT JOIN surname sn ON sn.id = u.surname_id
		WHERE s.status = '1' and s.student_id=:id", array('id' => $row['id']));
		$row['data'] = $images_data;
		array_push($completed_data, $row);
	}
	echo json_encode(array("data" => $completed_data, "count" => $limit, "errorCode" => '00'));
	exit;
} else {
	echo json_encode(array("data" => $List, "count" => $limit, "errorCode" => '00'));
	exit;
}
// } else {
// 	echo json_encode(array("errorMsg" => "Session expired", "errorCode" => '05'));
// 	exit;
// }
