<?php
include '../../db.php';
include '../../functions.php';
db_connect();
header("Content-Type: application/json");
header('Access-Control-Allow-Origin: *');

if (isset($_SERVER['HTTP_ORIGIN'])) {
	header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
	header('Access-Control-Allow-Credentials: true');
	header('Access-Control-Max-Age: 86400');    // cache for 1 day
}
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
		header("Access-Control-Allow-Methods: POST, POST, OPTIONS");

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
		header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

	exit(0);
}
if (!empty($_POST['by_uid'])) {

	$txt_id = time() . rand(1111, 9999);
	$data = array(
		"txt_id" => $txt_id,
		"department_id" => $_POST['department_id'],
		"uid" => $_POST['uid'],
		"fund_id" => $_POST['fund_id'],
		"amount" => $_POST['amount'],
		"notes" => $_POST['notes'],
		"by_uid" => $_POST['by_uid'],
		"by_brand" => $_POST['by_brand'],
		"by_model" => $_POST['by_model'],
		"bank_type" => $_POST['bank_type'],
		"type" => $_POST['type'],
		// "date" => phpNow()
	);
	if (!empty($_POST['date'])) {
		$data['date'] = $_POST['date'];
	}else{
		$data['date'] = phpNow();
	}
	$id = insertRow("kharsh_transcription", $data);

	echo json_encode(array("successMsg" => 'Added Successfull!', "errorCode" => '00'));
	exit;
} else {
	echo json_encode(array("errorMsg" => "Invalid Data.", "errorCode" => '01'));
	exit;
}
