<?php
include '../../db.php';
include '../../functions.php';
db_connect();
header("Content-Type: application/json");
header('Access-Control-Allow-Origin: *');


if (isset($_SERVER['HTTP_ORIGIN'])) {
	header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
	header('Access-Control-Allow-Credentials: true');
	header('Access-Control-Max-Age: 86400');    // cache for 1 day
}
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
		header("Access-Control-Allow-Methods: POST, POST, OPTIONS");

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
		header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

	exit(0);
}

// $query = $con->prepare("SELECT  * FROM `user` 
// 		WHERE  uid=:uid and is_Token=:is_Token");
// $query->bindParam(":is_Token", $request->is_Token);
// $query->bindParam(":uid", $request->uid);
// $query->execute();
// if ($query->rowCount() > 0) {
$limit = 100;
if (isset($_REQUEST['page'])) {
	$page  = $_REQUEST['page'];
} else {
	$page = 1;
};
$start_from = ($page - 1) * $limit;


$where = "";

if (!empty($_REQUEST['userId'])) {
	$where .= " and a.uid=:userId ";
}

$query = $con->prepare("SELECT a.*,CONCAT(u.`middle_name`,' ',u.`father_name`,' ', s.`name`) full_name,u.middle_name,u.father_name,u.surname_id,u.mobile,u.profile_pic,d.name department_name,p.business_name
 		FROM kharsh_transcription a
 		LEFT JOIN user u ON u.uid=a.by_uid
 		LEFT JOIN department_master d ON d.id=a.department_id
 		LEFT JOIN purchase_account p ON p.id=a.uid
 		LEFT JOIN surname s ON s.id=u.surname_id
 		WHERE a.status = 1 and a.type='cr' {$where}  order by a.id desc 
		LIMIT $start_from, $limit
	");

if (!empty($_REQUEST['userId'])) {
	$query->bindValue(":userId", "{$_REQUEST['userId']}");
}

$query->execute();

$List = array();
if ($query->rowCount() > 0) {
	$List = $query->fetchAll(PDO::FETCH_ASSOC);

	$original_data = array();

	$department_master = getRows("SELECT *
FROM department_master
WHERE status = 1 ");

	$purchase_master = getRows("SELECT *
FROM purchase_account
WHERE status = 1 ");

	$original_data['department_data'] = $department_master;
	$original_data['purchase_data'] = $purchase_master;
	// $original_data['Bank_Amount'] = $TotalAmount['amount'];
	// $original_data['Cash_Amount'] = $CreditAmount['amount'];
	// $original_data['Total_Amount'] = $TotalAmount['amount'] + $CreditAmount['amount'];
	$original_data['TransctionList'] = $List;
	echo json_encode(array("data" => $original_data, "count" => $limit, "errorCode" => '00'));
	exit;
} else {
	echo json_encode(array("data" => $List, "count" => $limit, "errorCode" => '00'));
	exit;
}
