<?php
include '../../db.php';
include '../../functions.php';
db_connect();
header("Content-Type: application/json");
header('Access-Control-Allow-Origin: *');


if (isset($_SERVER['HTTP_ORIGIN'])) {
	header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
	header('Access-Control-Allow-Credentials: true');
	header('Access-Control-Max-Age: 86400');    // cache for 1 day
}
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
		header("Access-Control-Allow-Methods: POST, POST, OPTIONS");

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
		header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

	exit(0);
}
// $district_master = getRow("SELECT *
// 				  FROM district_master 
// 				  WHERE status = 1 and id=:id", array('id' => $id));

$management_master = getRows("SELECT u.uid,CONCAT(u.`middle_name`,' ',u.`father_name`,' ', IFNULL(s.`name`,'')) full_name,u.mobile
				  FROM user u
				  LEFT JOIN surname s ON s.id=u.surname_id
				  WHERE u.status = '2' and u.type='management' and u.uid!='1' ");
$district_master = getRows("SELECT id,name
				  FROM district_master 
				  WHERE status = 1");
$completed_data = array();
foreach ($district_master as $row) {
	$dist_data = getRows("SELECT r.id,CONCAT(u.`middle_name`,' ',u.`father_name`,' ', IFNULL(s.`name`,'')) full_name,u.mobile
	FROM role_master r 
	LEFT JOIN user u ON u.uid=r.uid
		LEFT JOIN surname s ON s.id=u.surname_id
	WHERE r.status = '1' and r.type = 'dist' and r.city_id=:city_id", array('city_id' => $row['id']));
	$row['dist_data'] = $dist_data;


	$taluko_master = getRows("SELECT id,name
				  FROM taluko_master 
				  WHERE status = 1 and dist_id=:city_id", array('city_id' => $row['id']));
	$completedT_data = array();
	foreach ($taluko_master as $rowT) {
		$exam_master = getRows("SELECT r.id,CONCAT(u.`middle_name`,' ',u.`father_name`,' ', IFNULL(s.`name`,'')) full_name,u.mobile
		FROM role_master r 
		LEFT JOIN user u ON u.uid=r.uid
			LEFT JOIN surname s ON s.id=u.surname_id
		WHERE r.status = '1' and r.type = 'taluka' and r.city_id=:city_id", array('city_id' => $rowT['id']));
		$rowT['taluka_data'] = $exam_master;

		$completedV_data = array();
		$village_master = getRows("SELECT id,name
				  FROM village_master 
				  WHERE status = 1 and city_id=:city_id", array('city_id' => $row['id']));

		foreach ($village_master as $rowV) {
			$village_data = getRows("SELECT r.id,CONCAT(u.`middle_name`,' ',u.`father_name`,' ', IFNULL(s.`name`,'')) full_name,u.mobile
		FROM role_master r 
		LEFT JOIN user u ON u.uid=r.uid
			LEFT JOIN surname s ON s.id=u.surname_id
		WHERE r.status = '1' and r.type = 'village' and r.city_id=:city_id", array('city_id' => $rowV['id']));
			$rowV['village_data'] = $village_data;
			if (count($village_data) > 0) {
				array_push($completedV_data, $rowV);
			}
		}
		$rowT['village_master'] = $completedV_data;


		if (count($exam_master) > 0) {
			array_push($completedT_data, $rowT);
		}
	}
	$row['taluka_master'] = $completedT_data;

	if (count($dist_data) > 0) {
		array_push($completed_data, $row);
	}
}


echo json_encode(array("district_master" => $completed_data, "management_master" => $management_master, "errorCode" => '00'));
exit;
