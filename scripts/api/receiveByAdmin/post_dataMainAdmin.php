<?php
include '../../db.php';
include '../../functions.php';
db_connect();
header("Content-Type: application/json");
header('Access-Control-Allow-Origin: *');

if (isset($_SERVER['HTTP_ORIGIN'])) {
	header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
	header('Access-Control-Allow-Credentials: true');
	header('Access-Control-Max-Age: 86400');    // cache for 1 day
}
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
		header("Access-Control-Allow-Methods: POST, POST, OPTIONS");

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
		header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

	exit(0);
}
if (!empty($_POST['by_uid'])) {
	$u_transcription  = getRow("SELECT count(id) as total
	FROM mainAdmin_transcription ");
	$ut = $u_transcription['total'] + '1';
	$txt_id = time() . rand(1111, 9999);
	if($_POST['payment_type'] == 'bank'){
	$data = array(
		"txt_id" => $txt_id,
		"bill_id" => 'M' . '' . $ut,
		"uid" => $_POST['uid'],
		"amount" => $_POST['amount'],
		"notes" => $_POST['notes'],
		"bank_type" => 'cash',
		"type" => 'dr',
		"by_uid" => $_POST['by_uid'],
		"by_brand" => $_POST['by_brand'],
		"by_model" => $_POST['by_model'],
		"date" => phpNow()
	);
	$id = insertRow("mainAdmin_transcription", $data);
	$ut = $ut +'1';
	$dataA = array(
		"txt_id" => $txt_id,
		"bill_id" => 'M' . '' . $ut,
		"uid" => $_POST['uid'],
		"amount" => $_POST['amount'],
		"notes" => $_POST['notes'],
		"bank_type" => 'bank',
		"type" => 'cr',
		"by_uid" => $_POST['by_uid'],
		"by_brand" => $_POST['by_brand'],
		"by_model" => $_POST['by_model'],
		"date" => phpNow()
	);
	$Aid = insertRow("mainAdmin_transcription", $dataA);

	}else{
		$data = array(
			"txt_id" => $txt_id,
			"bill_id" => 'M' . '' . $ut,
			"uid" => $_POST['uid'],
			"amount" => $_POST['amount'],
			"notes" => $_POST['notes'],
			"bank_type" => 'bank',
			"type" => 'dr',
			"by_uid" => $_POST['by_uid'],
			"by_brand" => $_POST['by_brand'],
			"by_model" => $_POST['by_model'],
			"date" => phpNow()
		);
		$id = insertRow("mainAdmin_transcription", $data);
		$ut = $ut +'1';
		$dataA = array(
			"txt_id" => $txt_id,
			"bill_id" => 'M' . '' . $ut,
			"uid" => $_POST['uid'],
			"amount" => $_POST['amount'],
			"notes" => $_POST['notes'],
			"bank_type" => 'cash',
			"type" => 'cr',
			"by_uid" => $_POST['by_uid'],
			"by_brand" => $_POST['by_brand'],
			"by_model" => $_POST['by_model'],
			"date" => phpNow()
		);
		$Aid = insertRow("mainAdmin_transcription", $dataA);
	}
	echo json_encode(array("successMsg" => 'Added Successfull!', "errorCode" => '00'));
	exit;
} else {
	echo json_encode(array("errorMsg" => "Invalid Data.", "errorCode" => '01'));
	exit;
}
