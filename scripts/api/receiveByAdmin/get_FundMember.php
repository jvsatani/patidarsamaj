<?php
include '../../db.php';
include '../../functions.php';
db_connect();
header("Content-Type: application/json");
header('Access-Control-Allow-Origin: *');


if (isset($_SERVER['HTTP_ORIGIN'])) {
	header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
	header('Access-Control-Allow-Credentials: true');
	header('Access-Control-Max-Age: 86400');    // cache for 1 day
}
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
		header("Access-Control-Allow-Methods: POST, POST, OPTIONS");

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
		header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

	exit(0);
}

// $query = $con->prepare("SELECT  * FROM `user` 
// 		WHERE  uid=:uid and is_Token=:is_Token");
// $query->bindParam(":is_Token", $request->is_Token);
// $query->bindParam(":uid", $request->uid);
// $query->execute();
// if ($query->rowCount() > 0) {
$limit = 100;
if (isset($_REQUEST['page'])) {
	$page  = $_REQUEST['page'];
} else {
	$page = 1;
};
$start_from = ($page - 1) * $limit;

$UserType = getRow("SELECT type
FROM user 
WHERE status = '2' and  uid=:uid", array('uid' => $_REQUEST['adminUid']));
$where = '';

$where .= " (u.type='admin' OR u.type='city_admin' )";



$query = $con->prepare("SELECT u.uid,CONCAT(u.`middle_name`,' ',u.`father_name`,' ', IFNULL(s.`name`,'')) full_name,u.middle_name,u.father_name,u.surname_id,IFNULL(s.`name`,'') surname,u.mobile,u.member_id,u.is_mobile,u.profile_pic 
		FROM `user` u
		LEFT JOIN surname s ON s.id=u.surname_id
		WHERE  {$where} and u.status = '2'  order by u.uid desc 
		LIMIT $start_from, $limit
	");

// $query->bindParam(":userId", $_GET['userId']);

$query->execute();

$List = array();
if ($query->rowCount() > 0) {
	$List = $query->fetchAll(PDO::FETCH_ASSOC);

	$original_data = array();

	foreach ($List as $row) {
		// $TotalAmount = getRow("SELECT id,sum(amount) amount
		// FROM user_transcription 
		// WHERE status = 1 and  by_uid=:uid", array('uid' => $row['uid']));

		// $CreditAmount = getRow("SELECT id,sum(amount) amount
		// FROM admin_transcription 
		// WHERE status = 1 and type='admin' and uid=:uid", array('uid' => $row['uid']));

		// $TransctionList = getRows("SELECT a.*,CONCAT(u.`middle_name`,' ',u.`father_name`,' ', s.`name`) full_name,u.middle_name,u.father_name,u.surname_id,u.mobile,u.profile_pic
		// FROM admin_transcription a
		// LEFT JOIN user u ON u.uid=a.by_uid
		// LEFT JOIN surname s ON s.id=u.surname_id
		// WHERE a.status = 1 and a.type='admin' and  a.uid=:uid order by a.id  DESC", array('uid' => $row['uid']));

		// $row['TotalAmount'] = $TotalAmount['amount'];
		// $row['CreditAmount'] = $CreditAmount['amount'];
		// $row['TransctionList'] = $TransctionList;
		array_push($original_data, $row);
	}
	echo json_encode(array("data" => $original_data, "count" => $limit, "errorCode" => '00'));
	exit;
} else {
	echo json_encode(array("data" => $List, "count" => $limit, "errorCode" => '00'));
	exit;
}
// } else {
// 	echo json_encode(array("errorMsg" => "Session expired", "errorCode" => '05'));
// 	exit;
// }
