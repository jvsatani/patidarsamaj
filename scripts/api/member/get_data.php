<?php
include '../../db.php';
include '../../functions.php';
db_connect();
header("Content-Type: application/json");
header('Access-Control-Allow-Origin: *');


if (isset($_SERVER['HTTP_ORIGIN'])) {
	header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
	header('Access-Control-Allow-Credentials: true');
	header('Access-Control-Max-Age: 86400');    // cache for 1 day
}
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
		header("Access-Control-Allow-Methods: POST, POST, OPTIONS");

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
		header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

	exit(0);
}

// $query = $con->prepare("SELECT  * FROM `user` 
// 		WHERE  uid=:uid and is_Token=:is_Token");
// $query->bindParam(":is_Token", $request->is_Token);
// $query->bindParam(":uid", $request->uid);
// $query->execute();
// if ($query->rowCount() > 0) {
$limit = 100;
if (isset($_REQUEST['page'])) {
	$page  = $_REQUEST['page'];
} else {
	$page = 1;
};
$start_from = ($page - 1) * $limit;

$where = "";
if (!empty($_REQUEST['dada_id'])) {
	$where .= " and u.dada_id=:dada_id ";
}
if (!empty($_REQUEST['temple_id'])) {
	$where .= " and u.temple_id=:temple_id ";
}

if (!empty($_REQUEST['barot_id'])) {
	$where .= " and u.barot_id=:barot_id ";
}


$query = $con->prepare("SELECT u.uid,u.surname,u.middle_name,u.father_name,u.mobile,u.mobile_1,u.profile_pic,u.business,u.blood_group,u.taluko_id,u.address,u.dist_id,c.name city_name,d.name dist_name
		FROM `user` u
	LEFT JOIN taluko_master c ON c.id=u.taluko_id
	LEFT JOIN district_master d ON d.id=u.dist_id
		WHERE u.status = '2' and u.p_id='0' {$where}  order by u.uid desc 
		LIMIT $start_from, $limit
	");
if (!empty($_REQUEST['dada_id'])) {
	$query->bindValue(":dada_id", "{$_REQUEST['dada_id']}");
}
if (!empty($_REQUEST['temple_id'])) {
	$query->bindValue(":temple_id", "{$_REQUEST['temple_id']}");
}
if (!empty($_REQUEST['barot_id'])) {
	$query->bindValue(":barot_id", "{$_REQUEST['barot_id']}");
}
$query->execute();

$List = array();
if ($query->rowCount() > 0) {
	$List = $query->fetchAll(PDO::FETCH_ASSOC);
	echo json_encode(array("data" => $List, "count" => $limit, "errorCode" => '00'));
	exit;
} else {
	echo json_encode(array("data" => $List, "count" => $limit, "errorCode" => '00'));
	exit;
}
// } else {
// 	echo json_encode(array("errorMsg" => "Session expired", "errorCode" => '05'));
// 	exit;
// }
