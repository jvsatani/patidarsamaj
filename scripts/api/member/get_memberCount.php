<?php
include '../../db.php';
include '../../functions.php';
db_connect();
header("Content-Type: application/json");
header('Access-Control-Allow-Origin: *');


if (isset($_SERVER['HTTP_ORIGIN'])) {
	header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
	header('Access-Control-Allow-Credentials: true');
	header('Access-Control-Max-Age: 86400');    // cache for 1 day
}
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
		header("Access-Control-Allow-Methods: POST, POST, OPTIONS");

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
		header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

	exit(0);
}


if (!empty($_GET['is_Token'])) {
	$query = $con->prepare("SELECT id,name 
	FROM `district_master` 
		 WHERE status='1'
		 ORDER BY name asc
			");
	// $query->bindParam(":insti_id", $request->insti_id);
	$query->execute();

	$list = array();
	if ($query->rowCount() > 0) {
		$list = $query->fetchAll(PDO::FETCH_ASSOC);
		$completed_data = array();
		foreach ($list as $row) {
			$exam_master = getRow("SELECT count(uid) as us FROM user WHERE status = 2  and dist_id=:dist_id", array('dist_id' => $row['id']));
			$row['count'] = $exam_master['us'];
			if($row['count'] != '0' || $row['count'] != 0){
				array_push($completed_data, $row);
			}
			// array_push($completed_data, $row);
		}
		echo json_encode(array("data" => $completed_data, "errorCode" =>  '00'));
		exit;
	} else {
		echo json_encode(array("data" => $list, "errorCode" =>  '00'));
		exit;
	}
} else {
	echo json_encode(array("errorMsg" => "Invalid Action", "errorCode" => '01'));
	exit;
}
