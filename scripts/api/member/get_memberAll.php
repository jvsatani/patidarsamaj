<?php
include '../../db.php';
include '../../functions.php';
db_connect();
header("Content-Type: application/json");
header('Access-Control-Allow-Origin: *');


if (isset($_SERVER['HTTP_ORIGIN'])) {
	header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
	header('Access-Control-Allow-Credentials: true');
	header('Access-Control-Max-Age: 86400');    // cache for 1 day
}
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
		header("Access-Control-Allow-Methods: POST, POST, OPTIONS");

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
		header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

	exit(0);
}


if (!empty($_GET['is_Token'])) {
	$query = $con->prepare("SELECT u.uid,u.member_id,u.middle_name,CONCAT(u.`middle_name`, ' ', IFNULL(s.`name`,'')) short_name,CONCAT(u.`middle_name`,' ',u.`father_name`,' ', IFNULL(s.`name`,'')) full_name,s.name surname,u.mobile,u.profile_pic,u.address,t.name city_name,d.name dist_name,u.taluko_id,v.name village_name,u.village_id,u.fund_amount,u.hundi_amount
	FROM `user` u
	LEFT JOIN district_master d ON d.id=u.dist_id
	LEFT JOIN taluko_master t ON t.id=u.taluko_id
	LEFT JOIN village_master v ON v.id=u.village_id
	LEFT JOIN surname s ON s.id=u.surname_id
		 WHERE u.status='2' and u.dist_id=:dist_id
		 ORDER BY u.`middle_name` ASC
	");
	$query->bindParam(":dist_id", $_GET['dist_id']);
	$query->execute();

	$list = array();
	if ($query->rowCount() > 0) {
		$list = $query->fetchAll(PDO::FETCH_ASSOC);
		echo json_encode(array("data" => $list, "errorCode" =>  '00'));
		exit;
	} else {
		echo json_encode(array("data" => $list, "errorCode" =>  '00'));
		exit;
	}
} else {
	echo json_encode(array("errorMsg" => "Invalid Action", "errorCode" => '01'));
	exit;
}
