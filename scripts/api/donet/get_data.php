<?php
include '../../db.php';
include '../../functions.php';
db_connect();
header("Content-Type: application/json");
header('Access-Control-Allow-Origin: *');


if (isset($_SERVER['HTTP_ORIGIN'])) {
	header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
	header('Access-Control-Allow-Credentials: true');
	header('Access-Control-Max-Age: 86400');    // cache for 1 day
}
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
		header("Access-Control-Allow-Methods: POST, POST, OPTIONS");

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
		header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

	exit(0);
}

// $query = $con->prepare("SELECT  * FROM `user` 
// 		WHERE  uid=:uid and is_Token=:is_Token");
// $query->bindParam(":is_Token", $request->is_Token);
// $query->bindParam(":uid", $request->uid);
// $query->execute();
// if ($query->rowCount() > 0) {
$limit = 5000;
if (isset($_REQUEST['page'])) {
	$page  = $_REQUEST['page'];
} else {
	$page = 1;
};
$start_from = ($page - 1) * $limit;
$where = '';

$query = $con->prepare("SELECT u.uid,CONCAT(u.`middle_name`,' ',u.`father_name`,' ', IFNULL(s.`name`,'')) full_name,u.middle_name,u.father_name,u.surname_id,IFNULL(s.`name`,'') surname,u.mobile,u.member_id,u.is_mobile,u.profile_pic,u.hundi_amount,u.fund_amount
		FROM user u 
		LEFT JOIN surname s ON s.id=u.surname_id
		where (u.status = '2' OR  u.status = '4')
		 order by u.middle_name ASC 
		LIMIT $start_from, $limit
	");
$query->execute();

$List = array();
if ($query->rowCount() > 0) {
	$List = $query->fetchAll(PDO::FETCH_ASSOC);

	$completed_data = array();
	foreach ($List as $row) {
		if ($row['fund_amount'] > '0' || $row['fund_amount'] > 0) {
			array_push($completed_data, $row);
		}
	}

	echo json_encode(array("data" => $completed_data, "count" => $limit, "errorCode" => '00'));
	exit;
} else {
	echo json_encode(array("data" => $List, "count" => $limit, "errorCode" => '00'));
	exit;
}
// } else {
// 	echo json_encode(array("errorMsg" => "Session expired", "errorCode" => '05'));
// 	exit;
// }
