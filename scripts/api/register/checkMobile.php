<?php
include '../../db.php';
include '../../functions.php';
db_connect();
header("Content-Type: application/json");
header('Access-Control-Allow-Origin: *');


if (isset($_SERVER['HTTP_ORIGIN'])) {
	header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
	header('Access-Control-Allow-Credentials: true');
	header('Access-Control-Max-Age: 86400');    // cache for 1 day
}
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
		header("Access-Control-Allow-Methods: POST, POST, OPTIONS");

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
		header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

	exit(0);
}
if (!empty($_POST['mobile'])) {
	$mobile = $_POST['mobile'];
	$notificationToken = $_POST['notificationToken'];
	$is_Token = time() . rand(1111111111, 9999999999);
	// $OTP = rand(10000, 99999);
	$OTP = '12345';

	$query = $con->prepare("SELECT  * FROM `user` 
		WHERE  mobile=:mobile and status !='-1'");
	$query->bindParam(":mobile", $mobile);
	$query->execute();
	if ($query->rowCount() > 0) {
		echo json_encode(array("errorMsg" => "આ મોબાઇલ નંબર રજીસ્ટર થઇ ગયો છે.", "errorCode" => '01'));
		exit;
	} else {
		// SendOTP($mobile, $OTP);
		$member_id  = getRow("SELECT count(uid) as total
	FROM user ");
		$mi = $member_id['total'] + '1';
		$data = array(
			"member_id" => 'PSB' . '' . $mi,
			"mobile" => $mobile,
			"notificationToken" => $notificationToken,
			"is_Token" => $is_Token,
			"password" => $OTP,
			"type" => 'user',
			"date" => phpNow()
		);
		$id = insertRow("user", $data);
		$userData = getRow("SELECT uid,password,is_Token
				  FROM user 
				  WHERE status = 0 and uid=:id", array('id' => $id));
		echo json_encode(array("successMsg" => 'તમારો OTP  '.$OTP.' છે.', 'data' => $userData,"isOTP" => '0', "errorCode" => '00'));
		exit;
	}
} else {
	echo json_encode(array("errorMsg" => "કૃપા કરી ને મોબાઇલ નંબર દાખલ કરો.", "errorCode" => '01'));
	exit;
}
