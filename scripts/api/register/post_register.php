<?php
include '../../db.php';
include '../../functions.php';
db_connect();
header("Content-Type: application/json");
header('Access-Control-Allow-Origin: *');


if (isset($_SERVER['HTTP_ORIGIN'])) {
	header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
	header('Access-Control-Allow-Credentials: true');
	header('Access-Control-Max-Age: 86400');    // cache for 1 day
}
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
		header("Access-Control-Allow-Methods: POST, POST, OPTIONS");

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
		header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

	exit(0);
}

if (!empty($_POST['uid']) && !empty($_POST['is_Token'])) {
	$is_Token = $_POST['is_Token'];
	$uid = $_POST['uid'];
	$samaj_id = $_POST['samaj_id'];
	$surname_id = $_POST['surname_id'];
	$surname = $_POST['surname'];
	$middle_name = $_POST['name'];
	$father_name = $_POST['lastname'];
	$email = $_POST['email'];
	$dob = $_POST['dob'];
	$blood_group = $_POST['blood_group'];
	$business = $_POST['business'];
	$business_address = $_POST['business_address'];
	$qualification = $_POST['qualification'];
	$address = $_POST['address'];
	$taluko_id = $_POST['city_id'];
	$dist_id = $_POST['dist_id'];
	$gender = $_POST['gender'];
	$gov_job = $_POST['gov_job'];
	$karkidi_id = $_POST['karkidi_id'];

	$business_2 = '';
	if (!empty($_POST['business_2'])) {
		$business_2 = $_POST['business_2'];
	}
	$business_address_2 = '';
	if (!empty($_POST['business_address_2'])) {
		$business_address_2 = $_POST['business_address_2'];
	}
	$business_3 = '';
	if (!empty($_POST['business_3'])) {
		$business_3 = $_POST['business_3'];
	}
	$business_address_3 = '';
	if (!empty($_POST['business_address_3'])) {
		$business_address_3 = $_POST['business_address_3'];
	}

	$profile_pic = '';
	if (isset($_FILES['profile_pic'])) {
		$file = $_FILES['profile_pic'];
		$file_type = pathinfo($file["name"], PATHINFO_EXTENSION);
		$file_type = strtolower(trim($file_type));
		ini_set('memory_limit', '-1');
		set_time_limit(0);

		$profile_pic = uploadFile($file);
	}

	if (!empty($_POST['surname'])) {
		$surnameData = array(
			"samaj_id" => $samaj_id,
			"name" => $surname,
		);
		$surname_id = insertRow("surname", $surnameData);
	}

	$query = $con->prepare("SELECT  * FROM `user` 
		WHERE  is_Token=:is_Token and uid=:uid");
	$query->bindParam(":is_Token", $is_Token);
	$query->bindParam(":uid", $uid);
	$query->execute();
	if ($query->rowCount() > 0) {
		$mallList = $query->fetch(PDO::FETCH_ASSOC);
		$data = array(
			"samaj_id" => $samaj_id,
			"surname_id" => $surname_id,
			"middle_name" => $middle_name,
			"father_name" => $father_name,
			"email" => $email,
			"dob" => $dob,
			"profile_pic" => $profile_pic,
			"blood_group" => $blood_group,
			"business" => $business,
			"business_address" => $business_address,
			"business_2" => $business_2,
			"business_address_2" => $business_address_2,
			"business_3" => $business_3,
			"business_address_3" => $business_address_3,
			"qualification" => $qualification,
			"address" => $address,
			"taluko_id" => $taluko_id,
			"dist_id" => $dist_id,
			"karkidi_id" => $karkidi_id,
			"gov_job" => $gov_job,
			"gender" => $gender,
			"status" => '2',
			"lastlogin" => phpNow()
		);
		$id = updateRow("user", $data, array("uid" => $mallList['uid']));
		$queryA = $con->prepare("SELECT u.uid,u.member_id,u.middle_name,CONCAT(u.`middle_name`, ' ', IFNULL(s.`name`,'')) short_name,CONCAT(u.`middle_name`,' ',u.`father_name`,' ', IFNULL(s.`name`,'')) full_name,u.mobile,u.profile_pic,is_Token,u.status,u.type
			FROM `user` u  
			LEFT JOIN surname s ON s.id = u.surname_id
			WHERE  u.is_Token=:is_Token and u.uid=:uid");

		$queryA->bindParam(":is_Token", $is_Token);
		$queryA->bindParam(":uid", $uid);
		$queryA->execute();
		$userData = $queryA->fetch(PDO::FETCH_ASSOC);
		$slider_master = getRows("SELECT *
		FROM slider 
		WHERE status = '1'");
		$userData['slider_master'] = $slider_master;
		$message = 'New request for ' . $userData['full_name'] . ' Mo. ' . $userData['mobile'];
		$managementData = getRows("SELECT notificationToken
				  FROM user 
				  WHERE status = '2' and type= 'management'");

		foreach ($managementData as $row) {
			notificationSend($row['notificationToken'], $message);
		}
		echo json_encode(array("successMsg" => 'Register Successfull!', 'data' => $userData,  "errorCode" => '00'));
	} else {
		echo json_encode(array("errorMsg" => "Invalid mobile no and OTP", "errorCode" => '01'));
		exit;
	}
} else {

	$isData = 0;
	echo json_encode(array("errorMsg" => "Data Invalid", "errorCode" => '01'));
	exit;
}
