<?php
include '../../db.php';
include '../../functions.php';
db_connect();
header("Content-Type: application/json");
header('Access-Control-Allow-Origin: *');


if (isset($_SERVER['HTTP_ORIGIN'])) {
	header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
	header('Access-Control-Allow-Credentials: true');
	header('Access-Control-Max-Age: 86400');    // cache for 1 day
}
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
		header("Access-Control-Allow-Methods: POST, POST, OPTIONS");

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
		header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

	exit(0);
}

if (!empty($_POST['uid'])) {
	$uid = $_POST['uid'];


	$query = $con->prepare("SELECT  * FROM `user` 
		WHERE  uid=:uid");
	$query->bindParam(":uid", $uid);
	$query->execute();
	if ($query->rowCount() == 1) {
		$mallList = $query->fetch(PDO::FETCH_ASSOC);
			$data = array(
				"notificationToken" => '',
				"is_Token" => '',
			);
			$id = updateRow("user", $data, array("uid" => $uid));
		echo json_encode(array("successMsg" => 'Logout Successfull!', "errorCode" => '00'));
	} else {
		echo json_encode(array("errorMsg" => "Token expired", "errorCode" => '02'));
		exit;
	}
} else {
	echo json_encode(array("errorMsg" => "Invalid Request", "errorCode" => '02'));
	exit;
}
