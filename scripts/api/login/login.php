<?php
include '../../db.php';
include '../../functions.php';
db_connect();
header("Content-Type: application/json");
header('Access-Control-Allow-Origin: *');


if (isset($_SERVER['HTTP_ORIGIN'])) {
	header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
	header('Access-Control-Allow-Credentials: true');
	header('Access-Control-Max-Age: 86400');    // cache for 1 day
}
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
		header("Access-Control-Allow-Methods: POST, POST, OPTIONS");

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
		header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

	exit(0);
}
if (!empty($_POST['mobile'])) {
	$mobile = $_POST['mobile'];
	$notificationToken = $_POST['notificationToken'];
	$is_Token = time() . rand(1111111111, 9999999999);
	$OTP = rand(10000, 99999);
	// $OTP = '12345';

	$query = $con->prepare("SELECT  * FROM `user` 
		WHERE  mobile=:mobile and (status='4' OR status='1' OR status='2') ");
	$query->bindParam(":mobile", $mobile);
	$query->execute();
	if ($query->rowCount() > 0) {
		$mallList = $query->fetch(PDO::FETCH_ASSOC);
		// SendOTP($mobile, $OTP);
		$data = array(
			"password" => $OTP,
		);
		$id = updateRow("user", $data, array("uid" => $mallList['uid']));
		echo json_encode(array("successMsg" => 'OTP send successfully', "isOTP" => $OTP, "isOTPServer" => false, "errorCode" => '00'));
		// echo json_encode(array("successMsg" => 'તમને ૧ મિનિટ માં OTP મળી જાશે.', "errorCode" => '00'));
		exit;
	} else {
		echo json_encode(array("errorMsg" => "કૃપા કરી મોબાઇલ નંબર સાચો દાખલ કરો.", "errorCode" => '01'));
		exit;
	}
} else {
	echo json_encode(array("errorMsg" => "કૃપા કરી ને મોબાઇલ નંબર દાખલ કરો.", "errorCode" => '01'));
	exit;
}
