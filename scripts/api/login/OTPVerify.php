<?php
include '../../db.php';
include '../../functions.php';
db_connect();
header("Content-Type: application/json");
header('Access-Control-Allow-Origin: *');


if (isset($_SERVER['HTTP_ORIGIN'])) {
	header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
	header('Access-Control-Allow-Credentials: true');
	header('Access-Control-Max-Age: 86400');    // cache for 1 day
}
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
		header("Access-Control-Allow-Methods: POST, POST, OPTIONS");

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
		header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

	exit(0);
}
if (!empty($_POST['mobile'])) {
	$mobile = $_POST['mobile'];
	$otp = $_POST['OTP'];
	$notificationToken = $_POST['notificationToken'];
	$is_Token = time() . rand(1111111111, 9999999999);

	$query = $con->prepare("SELECT * FROM `user` 
		WHERE  mobile=:mobile and password=:otp and (status='0' OR status='1' OR status='2') ");
	$query->bindParam(":mobile", $mobile);
	$query->bindParam(":otp", $otp);
	$query->execute();
	if ($query->rowCount() > 0) {
		$mallList = $query->fetch(PDO::FETCH_ASSOC);
		$data = array(
			"notificationToken" => $notificationToken,
			"is_Token" => $is_Token,
			"password" => '12345',
			"lastlogin" => phpNow()
		);
		$id = updateRow("user", $data, array("uid" => $mallList['uid']));

		$rights_master = getRows("SELECT *
		FROM rights_master 
		WHERE status = '1' and uid=:uid",array('uid' => $mallList['uid']));

		$slider_master = getRows("SELECT *
			FROM slider 
			WHERE status = '1'");

		$userData = getRow("SELECT u.uid,u.member_id,u.middle_name,CONCAT(u.`middle_name`, ' ', IFNULL(s.`name`,'')) short_name,CONCAT(u.`middle_name`,' ',u.`father_name`,' ', IFNULL(s.`name`,'')) full_name,u.mobile,u.profile_pic,is_Token,u.status,u.type
				  FROM user u
				  LEFT JOIN surname s ON s.id = u.surname_id
				  WHERE (u.status = 1 OR u.status=2 OR u.status=4) and u.uid=:id", array('id' => $mallList['uid']));
		$userData['slider_master'] = $slider_master;
		$userData['rights_master'] = $rights_master;

		echo json_encode(array("successMsg" => 'Login Successfully', 'data' => $userData, "errorCode" => '00'));
		exit;
	} else {
		echo json_encode(array("errorMsg" => " આ OTP ખોટો છે.", "errorCode" => '01'));
		exit;
	}
} else {
	echo json_encode(array("errorMsg" => "કૃપા કરી ને મોબાઇલ નંબર દાખલ કરો.", "errorCode" => '01'));
	exit;
}
