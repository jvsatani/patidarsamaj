<?php
include '../../db.php';
include '../../functions.php';
db_connect();
header("Content-Type: application/json");
header('Access-Control-Allow-Origin: *');


if (isset($_SERVER['HTTP_ORIGIN'])) {
	header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
	header('Access-Control-Allow-Credentials: true');
	header('Access-Control-Max-Age: 86400');    // cache for 1 day
}
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
		header("Access-Control-Allow-Methods: POST, POST, OPTIONS");

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
		header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

	exit(0);
}


$query = $con->prepare("SELECT  * FROM `user` 
		WHERE  uid=:uid and is_Token=:is_Token");
$query->bindParam(":is_Token", $_REQUEST['is_Token']);
$query->bindParam(":uid", $_REQUEST['uid']);
$query->execute();
if ($query->rowCount() > 0) {
$isUserValid = '0';
$userData = getRow("SELECT *
				  FROM user 
				  WHERE (status='0' OR status='1' OR status='2'  OR status='4')  and uid=:id", array('id' => $_REQUEST['uid']));
if ($userData['status'] == '2') {
	$isUserValid = '1';
}


$district_master = getRows("SELECT *
				  FROM district_master 
				  WHERE status = 1  ORDER BY name ASC");


$adv_mobile = getSetting("adv_mobile");

$city_master = getRows("SELECT *
				  FROM taluko_master 
				  WHERE status = 1  ORDER BY name ASC");

$business_master = getRows("SELECT *
FROM business_master 
WHERE status = 1  ORDER BY name ASC");

$completed_data = array();
foreach ($business_master as $row) {
	$exam_master = getRow("SELECT count(uid) as us FROM user WHERE status = 2  and (business=:id OR business_2=:id OR business_3=:id)", array('id' => $row['id']));
	$row['count'] = $exam_master['us'];
	// if ($row['count'] != '0' || $row['count'] != 0) {
		array_push($completed_data, $row);
	// }
	// array_push($completed_data, $row);
}

$village_master = getRows("SELECT *
FROM village_master 
WHERE status = 1  ORDER BY name ASC");

$karkidi_master = getRows("SELECT *
FROM karkidi_master 
WHERE status = 1  ORDER BY name ASC");
$slider_master = getRows("SELECT *
				  FROM slider 
				  WHERE status = 1");

$fund_type = getRows("SELECT *
				  FROM fund_type 
				  WHERE status = 1  ORDER BY name ASC");
echo json_encode(array("adv_mobile" => $adv_mobile, "fund_type" => $fund_type, 'city_master' => $city_master, 'village_master' => $village_master, 'karkidi_master' => $karkidi_master, "district_master" => $district_master, "business_master" => $completed_data, "isUserValid" => $isUserValid,  "errorCode" => '00'));
exit;
} else {
	echo json_encode(array("errorMsg" => "Session expired", "errorCode" => '05'));
	exit;
}