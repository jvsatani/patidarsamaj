<?php
include '../../db.php';
include '../../functions.php';
db_connect();
header("Content-Type: application/json");
header('Access-Control-Allow-Origin: *');


if (isset($_SERVER['HTTP_ORIGIN'])) {
	header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
	header('Access-Control-Allow-Credentials: true');
	header('Access-Control-Max-Age: 86400');    // cache for 1 day
}
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
		header("Access-Control-Allow-Methods: POST, POST, OPTIONS");

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
		header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

	exit(0);
}
// $district_master = getRow("SELECT *
// 				  FROM district_master 
// 				  WHERE status = 1 and id=:id", array('id' => $id));

$business_master = getRows("SELECT *
				  FROM business_master 
				  WHERE status = 1  ORDER BY name ASC");

$surname_master = getRows("SELECT *
				  FROM surname 
				  WHERE status = 1  ORDER BY name ASC");

$district_master = getRows("SELECT *
				  FROM district_master 
				  WHERE status = 1  ORDER BY name ASC");

$city_master = getRows("SELECT *
				  FROM taluko_master 
				  WHERE status = 1  ORDER BY name ASC");

$village_master = getRows("SELECT *
				FROM village_master 
				WHERE status = 1  ORDER BY name ASC");

$karkidi_master = getRows("SELECT *
				FROM karkidi_master 
				WHERE status = 1  ORDER BY name ASC");


$slider_master = getRows("SELECT *
				  FROM slider 
				  WHERE status = '1'");

echo json_encode(array("village_master" => $village_master, "karkidi_master" => $karkidi_master, "business_master" => $business_master, "slider_master" => $slider_master, "surname_master" => $surname_master, "city_master" => $city_master, "district_master" => $district_master,  "errorCode" => '00'));
exit;
