<?php
include '../../db.php';
include '../../functions.php';
db_connect();
header("Content-Type: application/json");
header('Access-Control-Allow-Origin: *');


if (isset($_SERVER['HTTP_ORIGIN'])) {
	header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
	header('Access-Control-Allow-Credentials: true');
	header('Access-Control-Max-Age: 86400');    // cache for 1 day
}
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
		header("Access-Control-Allow-Methods: POST, POST, OPTIONS");

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
		header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

	exit(0);
}
$new_member = '0';
$total_member = '0';
$total_fund = '0';

$total_bank = '0';
$total_cash = '0';
$total_amount = '0';

$cash_kharsh = '0';
$bank_kharsh = '0';
$total_kharsh = '0';

$cash_loan = '0';
$bank_loan = '0';
$total_loan = '0';

$cash_on = '0';
$bank_on = '0';
$total_on = '0';

$userData = getRow("SELECT *
				  FROM user 
				  WHERE (status='0' OR status='1' OR status='2'  OR status='4')  and uid=:id", array('id' => $_REQUEST['uid']));
// if ($userData['type'] == 'management') {

$pending_member = getRow("SELECT COUNT(uid) as total_member
				  FROM user 
				  WHERE status = 1");
$new_member = 	$pending_member['total_member'];

$ka_member = getRow("SELECT COUNT(uid) as total_member
				  FROM user 
				  WHERE status = 2 and type != 'user'");

$kamiti_member = 	$ka_member['total_member'];

$accpet_member = getRow("SELECT COUNT(uid) as total_member
				  FROM user 
				  WHERE status = 2");
$total_member = 	$accpet_member['total_member'];

$main_amount = getRow("SELECT SUM(amount) as amount
				  FROM user_transcription 
				  WHERE status = 1 ");
$total_fund = number_format($main_amount['amount'], 2);


$cash_amount_credit = getRow("SELECT SUM(amount) as amount
				  FROM mainAdmin_transcription 
				  WHERE status = 1 and type='cr' and bank_type='cash'");

$cash_amount_debit = getRow("SELECT SUM(amount) as amount
				FROM mainAdmin_transcription 
				WHERE status = 1 and type='dr' and bank_type='cash'");
$cash_amount = $cash_amount_credit['amount'] - $cash_amount_debit['amount'];
$total_cash = number_format($cash_amount, 2);

$bank_amount_credit = getRow("SELECT SUM(amount) as amount
				  FROM mainAdmin_transcription 
				  WHERE status = 1 and type='cr' and bank_type='bank'");

$bank_amount_debit = getRow("SELECT SUM(amount) as amount
				FROM mainAdmin_transcription 
				WHERE status = 1  and type='dr' and bank_type='bank'");
$bank_amount = $bank_amount_credit['amount'] - $bank_amount_debit['amount'];

$total_bank = number_format($bank_amount, 2);


$total_amount = $cash_amount + $bank_amount;

$loan_ca_cr = getRow("SELECT SUM(amount) as amount
			FROM loan_transcription 
			WHERE status = 1 and type='cr' and bank_type='cash' ");
$loan_ca_dr = getRow("SELECT SUM(amount) as amount
			FROM loan_transcription 
			WHERE status = 1 and type='dr' and bank_type='cash' ");
$t_loan_cash = $loan_ca_dr['amount'] - $loan_ca_cr['amount'];
$cash_loan = number_format($t_loan_cash, 2);

$loan_cr = getRow("SELECT SUM(amount) as amount
				  FROM loan_transcription 
				  WHERE status = 1 and type='cr' and bank_type='bank' ");

$loan_dra = getRow("SELECT SUM(amount) as amount
				  FROM loan_transcription 
				  WHERE status = 1 and type='dr' and bank_type='bank' ");

$t_loan_bank = $loan_dra['amount'] - $loan_cr['amount'];
$bank_loan = number_format($t_loan_bank, 2);


$total_loan = $t_loan_bank + $t_loan_cash;



$cash_karshTotal = getRow("SELECT SUM(amount) as amount
				  FROM kharsh_transcription 
				  WHERE status = 1 and bank_type='cash'");
$cash_kharsh = number_format($cash_karshTotal['amount'], 2);

$bank_karshTotal = getRow("SELECT SUM(amount) as amount
				  FROM kharsh_transcription 
				  WHERE status = 1 and bank_type='bank'");
$bank_kharsh = number_format($bank_karshTotal['amount'], 2);

$total_kharsh = $cash_karshTotal['amount'] + $bank_karshTotal['amount'];

$cash_on = $cash_amount - ($cash_karshTotal['amount'] + $t_loan_cash);
$bank_on = $bank_amount - ($bank_karshTotal['amount'] + $t_loan_bank);
$total_on = $cash_on + $bank_on;


// }
if ($userData['type'] == 'city_admin' || $userData['type'] == 'admin') {
}
echo json_encode(array("new_member" => $new_member, "total_member" => $total_member, "total_fund" => $total_fund, "total_bank" => $total_bank, "total_cash" => $total_cash, "total_amount" => number_format($total_amount, 2), "cash_loan" => $cash_loan, "bank_loan" => $bank_loan, "total_loan" => number_format($total_loan, 2),  "cash_kharsh" => $cash_kharsh, "bank_kharsh" => $bank_kharsh, "total_kharsh" => number_format($total_kharsh, 2), "cash_on" => number_format($cash_on, 2), "bank_on" => number_format($bank_on, 2), "total_on" => number_format($total_on, 2), "kamiti_member" => $kamiti_member, "errorCode" => '00'));
exit;
