<?php
include '../../db.php';
include '../../functions.php';
db_connect();
header("Content-Type: application/json");
header('Access-Control-Allow-Origin: *');


if (isset($_SERVER['HTTP_ORIGIN'])) {
	header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
	header('Access-Control-Allow-Credentials: true');
	header('Access-Control-Max-Age: 86400');    // cache for 1 day
}
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
		header("Access-Control-Allow-Methods: POST, POST, OPTIONS");

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
		header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

	exit(0);
}
$bank_name = getSetting("bank_name");
$account_name = getSetting("account_name");
$account_no = getSetting("account_no");
$ifsc_code = getSetting("ifsc_code");
$branch_name = getSetting("branch_name");
$phonepay_mobile = getSetting("phonepay_mobile");
$phonepay_name = getSetting("phonepay_name");
$gpay_mobile = getSetting("gpay_mobile");
$gpay_name = getSetting("gpay_name");
$adv_mobile = getSetting("adv_mobile");

$data = array();
$data['bank_name'] = $bank_name;
$data['account_name'] = $account_name;
$data['account_no'] = $account_no;
$data['ifsc_code'] = $ifsc_code;
$data['branch_name'] = $branch_name;
$data['phonepay_mobile'] = $phonepay_mobile;
$data['phonepay_name'] = $phonepay_name;
$data['gpay_mobile'] = $gpay_mobile;
$data['gpay_name'] = $gpay_name;
$data['adv_mobile'] = $adv_mobile;

echo json_encode(array("data" => $data,  "errorCode" => '00'));
exit;
