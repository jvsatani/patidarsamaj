<?php
include '../../db.php';
include '../../functions.php';
db_connect();
header("Content-Type: application/json");
header('Access-Control-Allow-Origin: *');


if (isset($_SERVER['HTTP_ORIGIN'])) {
	header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
	header('Access-Control-Allow-Credentials: true');
	header('Access-Control-Max-Age: 86400');    // cache for 1 day
}
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
		header("Access-Control-Allow-Methods: POST, POST, OPTIONS");

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
		header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

	exit(0);
}
if(isset($_GET['mobile'])){
$query = $con->prepare("SELECT u.uid,u.member_id,CONCAT(u.`middle_name`,' ',u.`father_name`,' ', IFNULL(s.`name`,'')) full_name,u.middle_name,u.father_name,u.surname_id,IFNULL(s.`name`,'') surname,u.mobile
		FROM `user` u
		LEFT JOIN surname s ON s.id=u.surname_id
		WHERE  (u.status = '2' OR  u.status = '4') and u.mobile=:mobile
	");
$query->bindParam(":mobile", $_GET['mobile']);
$query->execute();

$List = array();
if ($query->rowCount() > 0) {
	$List = $query->fetch(PDO::FETCH_ASSOC);
	echo json_encode(array("data" => $List, "errorCode" => '00'));
	exit;
} else {
	echo json_encode(array("error" =>'User not found!', "errorCode" => '01'));
	exit;
}
}else{
	echo json_encode(array("error"=>"please Enter  correct data.","errorCode" => '01',));
	exit;
}