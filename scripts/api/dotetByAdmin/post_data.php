<?php
include '../../db.php';
include '../../functions.php';
db_connect();
header("Content-Type: application/json");
header('Access-Control-Allow-Origin: *');

if (isset($_SERVER['HTTP_ORIGIN'])) {
	header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
	header('Access-Control-Allow-Credentials: true');
	header('Access-Control-Max-Age: 86400');    // cache for 1 day
}
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
		header("Access-Control-Allow-Methods: POST, POST, OPTIONS");

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
		header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

	exit(0);
}
if (!empty($_POST['by_uid'])) {
	$u_transcription  = getRow("SELECT count(id) as total
	FROM user_transcription ");
	$ut = $u_transcription['total'] + '1';
	$txt_id = time() . rand(1111, 9999);
	$data = array(
		"txt_id" => $txt_id,
		"bill_id" => 'NKMJ' . '' . $ut,
		"uid" => $_POST['uid'],
		"amount" => $_POST['amount'],
		"notes" => $_POST['notes'],
		"by_uid" => $_POST['by_uid'],
		"by_brand" => $_POST['by_brand'],
		"by_model" => $_POST['by_model'],
		"type" => $_POST['type'],
	);

	if (!empty($_POST['fund_type'])) {
		$data['fund_type'] = $_POST['fund_type'];
	}
	if (!empty($_POST['date'])) {
		$data['date'] = $_POST['date'];
	}else{
		$data['date'] = phpNow();
	}
	$id = insertRow("user_transcription", $data);


	$users = getRow("SELECT *
 				  FROM user 
 				  WHERE status = 2 and uid=:uid", array('uid' => $_POST['uid']));
	SendFundSMS($users['mobile'], $_POST['amount']);

	if ($_POST['type'] == 'hundi') {
		$hundi_amount = $users['hundi_amount'] + $_POST['amount'];
		$paramts = array(
			"hundi_amount" => $hundi_amount,
		);
		$uid = updateRow("user", $paramts, array("uid" => $_POST['uid']));
	}
	if ($_POST['type'] == 'main') {
		$fund_amount = $users['fund_amount'] + $_POST['amount'];
		$paramts = array(
			"fund_amount" => $fund_amount,
		);
		$uid = updateRow("user", $paramts, array("uid" => $_POST['uid']));
	}


	$user_by = getRow("SELECT *
 				  FROM user 
 				  WHERE status = 2 and uid=:uid", array('uid' => $_POST['by_uid']));

	if ($user_by['type'] == 'management') {
		$uu_transcription  = getRow("SELECT count(id) as total
	FROM admin_transcription ");
		$uut = $uu_transcription['total'] + '1';
		$txt_idt = time() . rand(1111, 9999);
		$dataU = array(
			"txt_id" => $txt_idt,
			"bill_id" => 'R' . '' . $uut,
			"uid" => $_POST['uid'],
			"amount" => $_POST['amount'],
			"notes" => 'Direct payment for bill no.: ' . 'NKMJ' . '' . $ut,
			"by_uid" => $_POST['by_uid'],
			"by_brand" => $_POST['by_brand'],
			"by_model" => $_POST['by_model'],
			"bank_type" => $_POST['bank_type'],
			// "by_model" => 'direct',
			"date" => phpNow()
		);

		$id = insertRow("admin_transcription", $dataU);

		$A_transcription  = getRow("SELECT count(id) as total
	FROM mainAdmin_transcription ");
		$At = $A_transcription['total'] + '1';
		$txtid = time() . rand(1111, 9999);
		$dataA = array(
			"txt_id" => $txtid,
			"bill_id" => 'M' . '' . $At,
			"uid" => $_POST['uid'],
			"amount" => $_POST['amount'],
			"notes" => 'Direct payment for member id: ' . $users['member_id'],
			"by_uid" => $_POST['by_uid'],
			"by_brand" => $_POST['by_brand'],
			"by_model" => $_POST['by_model'],
			"bank_type" => $_POST['bank_type'],
			"date" => phpNow()
		);

		$idA = insertRow("mainAdmin_transcription", $dataA);
	}

	echo json_encode(array("successMsg" => 'Added Successfull!', "errorCode" => '00'));
	exit;
} else {
	echo json_encode(array("errorMsg" => "Invalid Data.", "errorCode" => '01'));
	exit;
}
