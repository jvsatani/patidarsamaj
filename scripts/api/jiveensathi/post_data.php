<?php
include '../../db.php';
include '../../functions.php';
db_connect();
header("Content-Type: application/json");
header('Access-Control-Allow-Origin: *');


if (isset($_SERVER['HTTP_ORIGIN'])) {
	header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
	header('Access-Control-Allow-Credentials: true');
	header('Access-Control-Max-Age: 86400');    // cache for 1 day
}
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
		header("Access-Control-Allow-Methods: POST, POST, OPTIONS");

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
		header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

	exit(0);
}
if (!empty($_POST['name'])) {

	$profile_pic = '';
	if (isset($_FILES['profile_pic'])) {
		$file = $_FILES['profile_pic'];
		$file_type = pathinfo($file["name"], PATHINFO_EXTENSION);
		$file_type = strtolower(trim($file_type));
		ini_set('memory_limit', '-1');
		set_time_limit(0);

		$profile_pic = uploadFile($file);
	}

	$data = array(
		"name" => $_POST['name'],
		"uid" => $_POST['uid'],
		"address" => $_POST['address'],
		"mobile" => $_POST['mobile'],
		"home_mobile" => $_POST['home_mobile'],
		"email" => $_POST['email'],
		"dob" => $_POST['dob'],
		"height" => $_POST['height'],
		"weight" => $_POST['weight'],
		"qualification" => $_POST['qualification'],
		"mulvatan" => $_POST['mulvatan'],
		"mosal" => $_POST['mosal'],
		"pasndi" => $_POST['pasndi'],
		"gender" => $_POST['gender'],
		"business" => $_POST['business'],
		"profile" => $profile_pic,
		"profile" => '0',
		"date" => phpNow()
	);
	$id = insertRow("jiveensathi", $data);
	echo json_encode(array("successMsg" => 'Added Successfull!', "errorCode" => '00'));
	exit;
} else {
	echo json_encode(array("errorMsg" => "કૃપા કરી ને મોબાઇલ નંબર દાખલ કરો.", "errorCode" => '01'));
	exit;
}
