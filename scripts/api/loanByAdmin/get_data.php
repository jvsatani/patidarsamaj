<?php
include '../../db.php';
include '../../functions.php';
db_connect();
header("Content-Type: application/json");
header('Access-Control-Allow-Origin: *');


if (isset($_SERVER['HTTP_ORIGIN'])) {
	header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
	header('Access-Control-Allow-Credentials: true');
	header('Access-Control-Max-Age: 86400');    // cache for 1 day
}
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
		header("Access-Control-Allow-Methods: POST, POST, OPTIONS");

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
		header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

	exit(0);
}

// $query = $con->prepare("SELECT  * FROM `user` 
// 		WHERE  uid=:uid and is_Token=:is_Token");
// $query->bindParam(":is_Token", $request->is_Token);
// $query->bindParam(":uid", $request->uid);
// $query->execute();
// if ($query->rowCount() > 0) {
$limit = 100;
if (isset($_REQUEST['page'])) {
	$page  = $_REQUEST['page'];
} else {
	$page = 1;
};
$start_from = ($page - 1) * $limit;


if (!empty($_REQUEST['userId'])) {
	$where .= "ut.status = '1' and ut.uid=:userId ";
}
if (!empty($_REQUEST['adminUid'])) {
	$where = "ut.status = '1' and ut.by_uid=:adminUid";
}


$query = $con->prepare("SELECT ut.*,CONCAT(u.`middle_name`,' ',u.`father_name`,' ', IFNULL(s.`name`,'')) full_name,u.middle_name,u.father_name,u.surname_id,IFNULL(s.`name`,'') surname,u.mobile,bu.mobile by_mobile,u.member_id,u.is_mobile,u.profile_pic,bu.profile_pic by_profile_pic,bu.middle_name by_middle_name,bu.father_name by_father_name,bu.surname_id by_surname_id,IFNULL(ss.`name`,'') by_surname
		FROM `user_transcription` ut
		LEFT JOIN user u ON u.uid=ut.uid
		LEFT JOIN user bu ON bu.uid=ut.by_uid
		LEFT JOIN surname s ON s.id=u.surname_id
		LEFT JOIN surname ss ON ss.id=bu.surname_id
		WHERE {$where}  order by ut.id desc 
		LIMIT $start_from, $limit
	");
if (!empty($_REQUEST['userId'])) {
	$query->bindValue(":userId", "{$_REQUEST['userId']}");
}
if (!empty($_REQUEST['adminUid'])) {
	$query->bindValue(":adminUid", "{$_REQUEST['adminUid']}");
}

$query->execute();

$List = array();
if ($query->rowCount() > 0) {
	$List = $query->fetchAll(PDO::FETCH_ASSOC);
	echo json_encode(array("data" => $List, "count" => $limit, "errorCode" => '00'));
	exit;
} else {
	echo json_encode(array("data" => $List, "count" => $limit, "errorCode" => '00'));
	exit;
}
// } else {
// 	echo json_encode(array("errorMsg" => "Session expired", "errorCode" => '05'));
// 	exit;
// }
