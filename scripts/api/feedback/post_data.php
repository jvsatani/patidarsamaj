<?php
include '../../db.php';
include '../../functions.php';
db_connect();
header("Content-Type: application/json");
header('Access-Control-Allow-Origin: *');


if (isset($_SERVER['HTTP_ORIGIN'])) {
	header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
	header('Access-Control-Allow-Credentials: true');
	header('Access-Control-Max-Age: 86400');    // cache for 1 day
}
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
		header("Access-Control-Allow-Methods: POST, POST, OPTIONS");

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
		header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

	exit(0);
}
if (!empty($_POST['uid'])) {

	$data = array(
		"uid" => $_POST['uid'],
		"title" => $_POST['title'],
		"notes" => $_POST['notes'],
		"date" => phpNow()
	);
	$id = insertRow("feedback", $data);
	echo json_encode(array("successMsg" => 'Added Successfull!', "errorCode" => '00'));
	exit;
} else {
	echo json_encode(array("errorMsg" => "કૃપા કરી ને લોગીન કરો.", "errorCode" => '01'));
	exit;
}
