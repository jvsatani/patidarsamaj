<?php
include '../../db.php';
include '../../functions.php';
db_connect();
header("Content-Type: application/json");
header('Access-Control-Allow-Origin: *');


if (isset($_SERVER['HTTP_ORIGIN'])) {
	header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
	header('Access-Control-Allow-Credentials: true');
	header('Access-Control-Max-Age: 86400');    // cache for 1 day
}
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
		header("Access-Control-Allow-Methods: POST, POST, OPTIONS");

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
		header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

	exit(0);
}

// $query = $con->prepare("SELECT  * FROM `user` 
// 		WHERE  uid=:uid and is_Token=:is_Token");
// $query->bindParam(":is_Token", $request->is_Token);
// $query->bindParam(":uid", $request->uid);
// $query->execute();
// if ($query->rowCount() > 0) {
$limit = 1500;
if (isset($_REQUEST['page'])) {
	$page  = $_REQUEST['page'];
} else {
	$page = 1;
};
$start_from = ($page - 1) * $limit;

$where = "";
if (!empty($_REQUEST['business'])) {
	$where .= " and (u.business=:business OR u.business_2=:business OR u.business_3=:business) ";
}
if (!empty($_REQUEST['blood_group'])) {
	$where .= " and u.blood_group=:blood_group ";
}

if (!empty($_REQUEST['city_id'])) {
	$where .= " and u.taluko_id=:city_id ";
}


$query = $con->prepare("SELECT u.*,u.middle_name,CONCAT(u.`middle_name`, ' ', IFNULL(s.`name`,'')) short_name,CONCAT(u.`middle_name`,' ',u.`father_name`,' ',IFNULL(s.`name`,'')) full_name,d.name dist_name,t.name city_name,v.name village_name,bm.name business_name,b2.name business_name_2,b3.name business_name_3,k.name karkidi_name,u.qualification,u.hundi_amount,u.fund_amount,s.name surname
FROM `user` u
LEFT JOIN surname s ON s.id=u.surname_id
LEFT JOIN district_master d ON d.id=u.dist_id
LEFT JOIN taluko_master t ON t.id=u.taluko_id
LEFT JOIN village_master v ON v.id=u.village_id
LEFT JOIN karkidi_master k ON k.id=u.karkidi_id
LEFT JOIN business_master bm ON bm.id=u.business
LEFT JOIN business_master b2 ON b2.id=u.business_2
LEFT JOIN business_master b3 ON b3.id=u.business_3
		WHERE u.status = '2' {$where}  order by u.middle_name ASC 
		LIMIT $start_from, $limit
	");
if (!empty($_REQUEST['business'])) {
	$query->bindValue(":business", "{$_REQUEST['business']}");
}
if (!empty($_REQUEST['blood_group'])) {
	$query->bindValue(":blood_group", "{$_REQUEST['blood_group']}");
}
if (!empty($_REQUEST['city_id'])) {
	$query->bindValue(":city_id", "{$_REQUEST['city_id']}");
}
$query->execute();

$List = array();
if ($query->rowCount() > 0) {
	$List = $query->fetchAll(PDO::FETCH_ASSOC);
	echo json_encode(array("data" => $List, "count" => $limit, "errorCode" => '00'));
	exit;
} else {
	echo json_encode(array("data" => $List, "count" => $limit, "errorCode" => '00'));
	exit;
}
// } else {
// 	echo json_encode(array("errorMsg" => "Session expired", "errorCode" => '05'));
// 	exit;
// }
