<?php
include '../../db.php';
include '../../functions.php';
db_connect();
header("Content-Type: application/json");
header('Access-Control-Allow-Origin: *');


if (isset($_SERVER['HTTP_ORIGIN'])) {
	header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
	header('Access-Control-Allow-Credentials: true');
	header('Access-Control-Max-Age: 86400');    // cache for 1 day
}
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
		header("Access-Control-Allow-Methods: POST, POST, OPTIONS");

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
		header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

	exit(0);
}

if (!empty($_GET['is_Token'])) {
	$query = $con->prepare("
		SELECT t.*,tm.name city_name,d.name dist_name
		FROM vyakti_vishesh t
		LEFT JOIN district_master d ON d.id=t.dist_id
		LEFT JOIN taluko_master tm ON tm.id=t.city_id
		WHERE t.status = 1  ORDER BY t.name ASC
			");
	// $query->bindParam(":insti_id", $request->insti_id);
	$query->execute();

	$list = array();
	if ($query->rowCount() > 0) {
		$list = $query->fetchAll(PDO::FETCH_ASSOC);
		echo json_encode(array("data" => $list, "errorCode" =>  '00'));
		exit;
	} else {
		echo json_encode(array("data" => $list, "errorCode" =>  '00'));
		exit;
	}
} else {
	echo json_encode(array("errorMsg" => "Invalid Action", "errorCode" => '01'));
	exit;
}
