<?php
include '../../db.php';
include '../../functions.php';
db_connect();
header("Content-Type: application/json");
header('Access-Control-Allow-Origin: *');


if (isset($_SERVER['HTTP_ORIGIN'])) {
	header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
	header('Access-Control-Allow-Credentials: true');
	header('Access-Control-Max-Age: 86400');    // cache for 1 day
}
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
		header("Access-Control-Allow-Methods: POST, POST, OPTIONS");

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
		header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

	exit(0);
}
$fileInput = file_get_contents("php://input");
$request = json_decode($fileInput);

	$query = $con->prepare("SELECT *,
	case is_display
				when '0' then 'true'
				when '1' then 'false'
			end is_display
		FROM `city_master` 
			 WHERE status='1'  order by `name` ASC
				");

	$query->execute();
	$list = array();
	if ($query->rowCount() > 0) {
		$list = $query->fetchAll(PDO::FETCH_ASSOC);
		echo json_encode(array("data" => $list,  "errorCode" => '00'));
		exit;
	} else {
		echo json_encode(array("data" => $list,  "errorCode" => '00'));
		exit;
	}

