<?php
include '../db.php';
include '../functions.php';
db_connect();

header("Content-Type: application/json");
header('Access-Control-Allow-Origin: *');
// $news_Master = getRow("SELECT id,title,image
// 		FROM news 
// 		WHERE status = 1 and is_notification='0'");

// if (!empty($news_Master['title'])) {
	$user_Master = getRows("SELECT notificationToken
		FROM user 
		WHERE status = 1");
	// $newsTitle = 'પ્રોડક્ટ નો ભાવ અપડેટ થઇ ગયો છે.';
	// $newsImg = '';
	// // if (!empty($news_Master['image'])) {
	// // 	$newsImg = $news_Master['image'];
	// // }
	// $dataA = array(
	// 	"is_notification" => '1',
	// );


	// // $id = updateRow("news", $dataA, array("id" => $news_Master['id']));

	foreach ($user_Master as $row) {

		if (!empty($row['notificationToken'])) {
			$content = array(
				"en" => 'પ્રોડક્ટ નો ભાવ અપડેટ થઇ ગયો છે.'
			);

			$headings = array(
				"en" => 'Damodar Oil Mill'
			);

			$fields = array(
				'app_id' => "21b84e85-d19f-4017-bc44-e9ed640d2e3a",
				// 'included_segments' => array('All'),
				'include_player_ids' => [$row['notificationToken']],
				// 'data' => array("foo" => "bar"),
				//  'large_icon' => 'http://www.samayexpress.in/images/uploads/'.'a324c300a9556a9d6a3643400db4c896IMG-20200724-WA0045.jpg',
				// 'big_picture' => 'http://www.samayexpress.in/images/uploads/' . $newsImg,
				'contents' => $content,
				'headings' => $headings,
			);
// print_r($fields);
			$fields = json_encode($fields);

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				'Content-Type: application/json; charset=utf-8',
				'Authorization: Basic ZTU3N2U4NmQtY2E2NS00YTM1LWIxODEtNWE1MzdjZjA5Y2Iz'
			));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

			curl_setopt($ch, CURLOPT_HEADER, FALSE);
			curl_setopt($ch, CURLOPT_POST, TRUE);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

			$response = curl_exec($ch);
			curl_close($ch);
			echo $response;
		}
	}
// }
?>