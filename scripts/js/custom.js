var isLoggedIn=false;
var user=null;

function getHTML(page,cb){
	if(!page || page==''){
		return;
	}
	
	
	if(page != 'login.html' && !isLoggedIn){
		console.log(new Error(page).stack || new Error(page));
		return;
	}
	
	Pace.restart();
	$.ajax({
		url: "pages/"+page,
		success: function(data){
			$(".master-container").html(data);
		},
		error: function(data){
			console.error("AJAX Failed");
		}
	});
}
function getData(page){
	Pace.restart();
	$.ajax({
		url: "scripts/php/"+page,
		success: function(data){
			$(".master-container").html(data);
		},
		error: function(data){
			console.error("AJAX Failed");
		}
	});
}
function login(){
	Pace.restart();
	$.ajax({
		url: "pages/login.html",
		success: function(data){
			$(".master-container").html(data);
			App.init();
		},
		error: function(data){
			console.error("AJAX Failed");
		}
	});
}
function numberWithCommas(x) {
	var parts = x.toString().split(".");
	parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	return parts.join(".");
}
function notify(data){
	if(data.success){
		$.gritter.add({title:data.success});
	}else{
		$.gritter.add({title:data.error, class_name:"gritter-light"});
	}
}
function escapeHtml(unsafe) {
	if(unsafe)
		return unsafe
			 .replace(/&/g, "&amp;")
			 .replace(/</g, "&lt;")
			 .replace(/>/g, "&gt;")
			 .replace(/"/g, "&quot;")
			 .replace(/'/g, "&#039;");
	else return "";
 }
$.getParameter = function(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null){
       return null;
    }
    else{
       return results[1] || 0;
    }
}

function try_route(){
	var url=window.location.href;
	if(url.indexOf("#")!=-1){
		var arr=url.split("#");
		getHTML(arr[1]);
	}else{
		if(url==siteURL && isLoggedIn){
			getHTML("index.html");
		}else{
			getHTML(url);
		}
	}
}
function redirect(href){
	$("#hiddenLink").prop("href",href);
	$("#hiddenLink").trigger("click");
}
$(document).ready(function(){
	$(document).on("click","a.ajax",function(e){
		if($(this).hasClass('confirm-deploy')){
			if(!confirm('Are you sure you wish to run the script?')){
				return false;
			}
		}
		e.preventDefault();
		//get page name
		var href=$(this).prop("href");
		var url=href;
		var arr=href.split("#");
		if(arr[1]){
			getHTML(arr[1]);
		}else{
			getHTML(arr[0]);
		}
		if(url!=window.location){
			
			if($('#page-container').hasClass('page-sidebar-toggled')){
				$('#sidebar, .sidebar-bg').animate(
					{
						marginLeft: '-'+$('.sidebar-bg').width()+'px'
					},
					{
						easing: 'swing',
						duration: 200,
						complete: function(){
							$('#sidebar, .sidebar-bg').css({marginLeft: 'auto'});
							$('[data-click=sidebar-toggled]').trigger('click');
						}
					}
				);
			}
			
			window.history.pushState({path:url},'',url);
		}
	});
	$(window).bind('popstate', function() {
		if(location.hash!=""){
			var arr=location.hash.split("#");
			getHTML(arr[1]);
		}else{
			getHTML("index.html");
		}
	});
	
	$(document).on('click', '[data-popup-ajax]', function(e){
		e.preventDefault();
		var href=$(this).data('popup-ajax');
		var title=$(this).data('popup-title');
		var type=$(this).data('popup-type');
		showAjaxPopup({
			href: href,
			title: title,
			type: type
		});
	});
	
});

$.fn.iframeAutoResize = function(){
  var lastHeight = 0, curHeight = 0, $frame = $(this);
  setInterval(function(){
    curHeight = $frame.contents().find('body').height();
    if ( curHeight != lastHeight ) {
      $frame.css('height', (lastHeight = curHeight) + 'px' );
    }
  },500);
};

function showAjaxPopup(options){
	
	options = options || {};
	options.title = options.title || '';
	options.type = options.type || 'type-default';
	
	var popup_id = ('popup-ajax-instance-'+Math.random()).replace('.','');
	var $container = $('<div id="'+popup_id+'" style="min-height:200px;"></div>');
	
	BootstrapDialog.show({
		title: options.title,
		type: options.type,
		message: $container,
		onshow: function(dialogRef){
			
			dialogRef.getParameter = function(name) {
				name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
				var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
					results = regex.exec(options.href);
				return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
			}
			
			dialogRef.showLoading = function(){
				$container.html('<p class="text-center" style="margin-top:100px;"><i class="fa fa-spinner fa-spin"></i> Loading...</p>');
			}
			dialogRef.showLoading();
			$container.on('click', '[data-popup-action="close"]', function(){
				dialogRef.close();
			});
		},
		onshown: function(dialogRef){
			$.ajax({
				url: 'pages/'+options.href,
				dataType: 'HTML',
				cache: false,
				success: function(response){
					window.currentPopup=dialogRef;
					$container.html(response);
				}
			});
		},
		buttons: []
	});
	
	
}


$(function(){
	var clipboard = new Clipboard('[data-clipboard-text]');
	clipboard.on('success', function(e) {
		//console.info('Action:', e.action);
		//console.info('Text:', e.text);
		//console.info('Trigger:', e.trigger);
		notify({success: e.text+" copied to clipboard"});
		e.clearSelection();
	});

	clipboard.on('error', function(e) {
		//console.error('Action:', e.action);
		//console.error('Trigger:', e.trigger);
		notify({error: e.text+" not copied to clipboard"});
	});
});





