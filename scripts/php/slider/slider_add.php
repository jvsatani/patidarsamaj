<?php
include_once '../../db.php';
header('Content-Type: application/json');
db_connect();

if(isset($_POST['action']) && $_POST['action']=="add"){
	$data = array(
		"image"=>$_POST['image'],
		"date"=>phpNow()
	);
	$id = insertRow("slider",$data);
	echo json_encode(array("success"=>"Add successfully!"));
}
else{
	echo json_encode(array("error"=>"Add not successfully!"));
}

?>