<?php
include_once '../../db.php';
header('Content-Type: application/json');
db_connect();
if(isset($_POST['action']) && $_POST['action']=="edit"){
	$data = array(
		"city_id"=>$_POST['city_id'],
		"name"=>$_POST['name'],
		"date"=>phpNow()
	);

	$id = updateRow("village_master",$data,array("id"=>$_POST['id']));
	echo json_encode(array("success"=>"Edit successfully!"));
}
else{
	echo json_encode(array("error"=>"Edit not successfully!"));
}

?>