<?php
include_once '../../db.php';
header('Content-Type: application/json');
db_connect();

if(isset($_POST['action']) && $_POST['action']=="add"){
	$data = array(
		"dist_id"=>$_POST['dist_id'],
		"name"=>$_POST['name'],
		"date"=>phpNow()
	);
	$id = insertRow("taluko_master",$data);
	echo json_encode(array("success"=>"Add successfully!"));
}
else{
	echo json_encode(array("error"=>"Add not successfully!"));
}

?>