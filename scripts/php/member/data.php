<?php
include_once("../../db.php");
include_once("../../functions.php");
db_connect();
$userINFO = getRow("SELECT *
FROM user 
WHERE uid=:id", array('id' => $_REQUEST['uid']));

$where = "";
if (!empty($_REQUEST['search'])) {
	$where .= " and (s.name like :search OR u.middle_name like :search OR u.father_name like :search OR u.mobile like :search) ";
}
if (!empty($_REQUEST['type'])) {
	// $where .= " and u.status=:type ";
}
$columns = explode(",", $_POST['sColumns']);
$sortCol = $_POST['iSortCol_0'];
$sortOrder = $_POST['sSortDir_0'];

$records = array();

$query = "select count(*) total,s.name surname
		FROM user u
		LEFT JOIN surname s ON s.id=u.surname_id
		where u.status=:type {$where} ";

$query = $con->prepare($query);
if (!empty($_REQUEST['search'])) {
	$query->bindValue(":search", "%{$_REQUEST['search']}%");
}
// if (!empty($_REQUEST['type'])) {
$query->bindValue(":type", "{$_REQUEST['type']}");
// }
$query->execute();
$row = $query->fetch(PDO::FETCH_ASSOC);

$iTotalRecords = $row['total'];
$iDisplayLength = intval($_REQUEST['iDisplayLength']);
$iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
$iDisplayStart = intval($_REQUEST['iDisplayStart']);
$sEcho = intval($_REQUEST['sEcho']);

$records["aaData"] = array();
$query = "SELECT u.*,d.name dist_name,t.name city_name,v.name village_name,s.name surname
		FROM  user u
		LEFT JOIN district_master d ON d.id=u.dist_id
		LEFT JOIN taluko_master t ON t.id=u.taluko_id
		LEFT JOIN village_master v ON v.id=u.village_id
		LEFT JOIN surname s ON s.id=u.surname_id
		WHERE u.status=:type {$where}
		";

// $query .= " order by {$columns[$sortCol]} {$sortOrder}";
$query .= " limit $iDisplayStart, $iDisplayLength";

$query = $con->prepare($query);
if (!empty($_REQUEST['search'])) {
	$query->bindValue(":search", "%{$_REQUEST['search']}%");
}
// if (!empty($_REQUEST['type'])) {
$query->bindValue(":type", "{$_REQUEST['type']}");
// }

$query->execute();

if ($query->rowCount() > 0) {

	$idx = 0;
	while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
		$idx++;
		$id =  base64_encode($row['uid']);
		$row['profile_pic'] = getFullImage($row['profile_pic']);
		if ($row['dob'] != '0000-00-00') {
			$row['dob'] = date('d/m/Y', strtotime($row['dob']));
		} else {
			$row['dob'] = '';
		}
		if ($row['lastlogin'] != '0000-00-00') {
			$lastlogin = date('d/m/Y', strtotime($row['lastlogin']));
		} else {
			$lastlogin = '';
		}

		$image = "<a href='{$row['profile_pic']}' data-lightbox='gallery-group-1'><img src='{$row['profile_pic']}' class='img-thumbnail' alt='' style='max-height: 35px;'></a>";
		$name = $image . '  &nbsp;&nbsp;&nbsp;' . $row['middle_name'] . ' ' . $row['father_name'] . ' ' . $row['surname'];
		$address = $row['address'] . '</br>  <span> ગામ: ' . $row['village_name']  . '</br>  <span> તાલુકો: ' . $row['city_name'] . '</span>' . '</br> જિલ્લો: ' . $row['dist_name'];
		$edit = '';
		$delete = '';
		$aprove = '';
		$reject = '';
		$guest = '';
		$subcat = "";
		if ($userINFO['type'] == 'management') {
			if ($row['type'] == 'admin') {
				$subcat = "<a href='#rights_master.php?id={$id}' class='btn btn-sm btn-primary' i='{$id}' title='Rights Master'><i class='fa fa-key'></i></a>";
			}
		}

		if ($_REQUEST['type'] == '1') {
			if (menuRights($_REQUEST['uid'], 'member', 'add')) {

			$aprove = "<a href='javascript:;' role='button'  class='btn btn-sm btn-green aprove' i='{$row['uid']}' data-i=\"" . htmlspecialchars(json_encode($row)) . "\"  title='Aprove'> <i class='fa fa-check tableFontIcon'></i> </a> ";
			$reject = "<a href='javascript:;' role='button'  class='btn btn-sm btn-danger reject ' i='{$row['uid']}' data-i=\"" . htmlspecialchars(json_encode($row)) . "\" title='Reject'> <i class='fa fa-times tableFontIcon'></i></a>";
			$guest = "<a href='javascript:;' role='button'  class='btn btn-sm btn-indigo guest ' i='{$row['uid']}' data-i=\"" . htmlspecialchars(json_encode($row)) . "\" title='Reject'> <i class='fa fa-user tableFontIcon'></i></a>";
			}
		} else if ($_REQUEST['type'] == '3' || $_REQUEST['type'] == '4') {
			$aprove = "<a href='javascript:;' role='button'  class='btn btn-sm btn-green aprove' i='{$row['uid']}' data-i=\"" . htmlspecialchars(json_encode($row)) . "\"  title='Aprove'> <i class='fa fa-check tableFontIcon'></i> </a> ";
			if (menuRights($_REQUEST['uid'], 'member', 'delete')) {
				$delete = "<a href='javascript:;' role='button' data-toggle='modal' data-target='#modal-del' class='btn btn-sm btn-danger delete ' i='{$row['uid']}' data-i=\"" . htmlspecialchars(json_encode($row)) . "\" title='Delete'> <i class='fa fa-trash tableFontIcon'></i> </a>";
			}
		} else {
			if (menuRights($_REQUEST['uid'], 'member', 'edit')) {
				$edit = "<a href='javascript:;' role='button' data-toggle='modal' data-target='#modal-edit' class='btn btn-sm btn-green edit' i='{$row['uid']}' data-i=\"" . htmlspecialchars(json_encode($row)) . "\"  title='Edit'> <i class='fa fa-pencil-square-o tableFontIcon'></i> </a> ";
			}
			if (menuRights($_REQUEST['uid'], 'member', 'delete')) {
				$delete = "<a href='javascript:;' role='button' data-toggle='modal' data-target='#modal-del' class='btn btn-sm btn-danger delete ' i='{$row['uid']}' data-i=\"" . htmlspecialchars(json_encode($row)) . "\" title='Delete'> <i class='fa fa-trash tableFontIcon'></i></a>";
			}
		}
		$view = "<a href='#user_detail.php?id={$id}' class='btn btn-sm btn-primary' title='View'> <i class='fa fa-eye tableFontIcon'></i> </a> ";
		$isDisplay= '';
			if (menuRights($_REQUEST['uid'], 'member', 'add')) {
				$isDisplay = "<i class='fa fa-lock fa-3x classOff' style='color: #F44336;' data-i='{$id}'></i>";
		if ($row['is_mobile'] == 'yes') {
			$isDisplay = "<i class='fa fa-unlock fa-3x classOn'  style='color: #4CAF50;' data-i='{$id}'></i>";
		}
	}
		$records["aaData"][] = array(
			$row['uid'],
			"{$idx}",
			$name,
			"{$address}",
			$row['mobile'],
			$isDisplay,
			$lastlogin,
			"{$aprove} {$reject} {$guest} {$view} {$edit} {$delete} {$subcat}",
		);
	}
}

$records["sEcho"] = $sEcho;
$records["iTotalRecords"] = $iTotalRecords;
$records["iTotalDisplayRecords"] = $iTotalRecords;
//$records ["total"] = $total;
echo json_encode($records);
