<?php
include_once '../../db.php';
include_once '../../functions.php';
header('Content-Type: application/json');
db_connect();

if (isset($_REQUEST['id'])) {
	$queryG = $con->prepare("SELECT *
	FROM `user` 
		 WHERE status='2' and uid=:id
			");
	$queryG->bindParam(":id", $_REQUEST['id']);
	$queryG->execute();
	if ($queryG->rowCount() != 0) {
		$data = array(
			"type" => $_REQUEST['status'],
		);
		$id = updateRow("user", $data, array("uid" => $_REQUEST['id']));

		if ($_REQUEST['status'] == 'admin') {
			employeeToRightAdd($_REQUEST['id']);
		} else {
			$query = $con->prepare("DELETE FROM `rights_master` WHERE uid=:id");
			$query->bindParam(":id", $_REQUEST['id']);
			$query->execute();
		}
		echo json_encode(array("success" => "Status updated!"));
		exit;
	} else {
		echo json_encode(array("error" => "Invalid Access!"));
		exit;
	}
} else {
	echo json_encode(array("error" => "Invalid Access!"));
}
