<?php
include_once '../../db.php';
header('Content-Type: application/json');
db_connect();

if (isset($_POST['action']) && $_POST['action'] == "add") {
	if ($_POST['middle_name'] == '') {
		echo json_encode(array("error" => "Please enter name"));
		exit;
	}
	$query = $con->prepare("SELECT  * FROM `user` 
		WHERE  mobile=:mobile  and status !='-1' ");
	$query->bindParam(":mobile", $_POST['mobile']);
	$query->execute();
	if ($query->rowCount() > 0) {
		echo json_encode(array("error" => "આ મોબાઇલ નંબર રજીસ્ટર થઇ ગયો છે.", "errorCode" => '01'));
		exit;
	} else {
		$dob = '';
		if (!empty($_POST['dob'])) {
			$start_date = $_POST['dob'];
			$start_date = DateTime::createFromFormat("d/m/Y", $start_date);
			$dob = $start_date->format('Y-m-d');
		}
		$status = '2';
		if ($_POST['status'] == '4') {
			$status = '4';
		}
		$surname_id = $_POST['surname_id'];
		if (!empty($_POST['surname'])) {
			$surnameData = array(
				"samaj_id" => $_POST['samaj_id'],
				"surname" => $_POST['surname'],
			);
			$surname_id = insertRow("surname", $surnameData);
		}
		$member_id  = getRow("SELECT count(uid) as total
		FROM user ");
			$mi = $member_id['total'] + '1';
			$data = array(
			"member_id" => 'PSB' . '' . $mi,
			"samaj_id" => $_POST['samaj_id'],
			"surname_id" => $surname_id,
			"middle_name" => $_POST['middle_name'],
			"father_name" => $_POST['father_name'],
			"email" => $_POST['email'],
			"dob" => $dob,
			"blood_group" => $_POST['blood_group'],
			"mobile" => $_POST['mobile'],
			"karkidi_id" => $_POST['karkidi_id'],
			"address" => $_POST['address'],
			"village_id" => $_POST['village_id'],
			"taluko_id" => $_POST['taluko_id'],
			"dist_id" => $_POST['dist_id'],
			"business" => $_POST['business'],
			"qualification" => $_POST['qualification'],
			"profile_pic" => $_POST['image'],
			"gender" => $_POST['gender'],
			"status" => $status,
			"type" => 'user',
			"date" => phpNow()
		);
		$id = insertRow("user", $data);
	}
	echo json_encode(array("success" => "Add successfully!"));
} else {
	echo json_encode(array("error" => "Add not successfully!"));
}
