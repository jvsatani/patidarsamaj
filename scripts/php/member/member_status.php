<?php
include_once '../../db.php';
header('Content-Type: application/json');
db_connect();

if (isset($_REQUEST['id'])) {
	$queryG = $con->prepare("SELECT *
	FROM `user` 
		 WHERE status='1' and uid=:id
			");
	$queryG->bindParam(":id", $_REQUEST['id']);
	$queryG->execute();
	if ($queryG->rowCount() != 0) {
		$data = array(
			"status" => $_REQUEST['action'],
		);
		$id = updateRow("user", $data, array("uid" => $_REQUEST['id']));
		echo json_encode(array("success" => "Status updated!"));
	} else {
		echo json_encode(array("error" => "Invalid Access!"));
		exit;
	}
} else {
	echo json_encode(array("error" => "Edit not successfully!"));
}
