<?php
include_once '../../db.php';
header('Content-Type: application/json');
db_connect();
if(isset($_POST['action']) && $_POST['action']=="edit"){
	$id =$_POST['id'];
	if ($_POST['middle_name'] == '') {
		echo json_encode(array("error" => "Please enter name"));
		exit;
	}
	$dob = '';
		if (!empty($_POST['dob'])) {
			$start_date = $_POST['dob'];
			$start_date = DateTime::createFromFormat("d/m/Y", $start_date);
			$dob = $start_date->format('Y-m-d');
		}
	// $query = $con->prepare("SELECT  * FROM `user` 
	// 	WHERE  mobile=:mobile  and status !='-1' and uid!={$id} ");
	// $query->bindParam(":mobile", $_POST['mobile']);
	// $query->execute();
	// if ($query->rowCount() > 0) {
	// 	echo json_encode(array("error" => "આ મોબાઇલ નંબર રજીસ્ટર થઇ ગયો છે.", "errorCode" => '01'));
	// 	exit;
	// } else {
		$surname_id = $_POST['surname_id'];
		if (!empty($_POST['surname'])) {
			$surnameData = array(
				"samaj_id" => $_POST['samaj_id'],
				"surname" => $_POST['surname'],
			);
			$surname_id = insertRow("surname", $surnameData);
		}

		$data = array(
			"samaj_id" => $_POST['samaj_id'],
			"surname_id" => $surname_id,
			"middle_name" => $_POST['middle_name'],
			"father_name" => $_POST['father_name'],
			"email" => $_POST['email'],
			"dob" => $dob,
			"blood_group" => $_POST['blood_group'],
			"mobile" => $_POST['mobile'],
			"karkidi_id" => $_POST['karkidi_id'],
			"address" => $_POST['address'],
			"village_id" => $_POST['village_id'],
			"taluko_id" => $_POST['taluko_id'],
			"dist_id" => $_POST['dist_id'],
			"business" => $_POST['business'],
			"qualification" => $_POST['qualification'],
			"gender" => $_POST['gender'],
		);
		if (!empty($_POST['image'])) {
			$data['profile_pic'] = $_POST['image'];
		}
		$id = updateRow("user",$data,array("uid"=>$_POST['id']));
	// }
	
	echo json_encode(array("success"=>"Edit successfully!"));
}
else{
	echo json_encode(array("error"=>"Edit not successfully!"));
}
