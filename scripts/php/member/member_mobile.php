<?php
include_once '../../db.php';
header('Content-Type: application/json');
db_connect();

if (isset($_REQUEST['uid'])) {
	$queryG = $con->prepare("SELECT *
	FROM `user` 
		 WHERE  uid=:uid
			");
	$queryG->bindParam(":uid", $_REQUEST['uid']);
	$queryG->execute();
	if ($queryG->rowCount() != 0) {
		$data = array(
			"is_mobile" => $_REQUEST['is_mobile'],
		);
		$id = updateRow("user", $data, array("uid" => $_REQUEST['uid']));
		echo json_encode(array("success" => "Status updated!"));
	} else {
		echo json_encode(array("error" => "Invalid Access!"));
		exit;
	}
} else {
	echo json_encode(array("error" => "Edit not successfully!"));
}
