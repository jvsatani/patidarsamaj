<?php
include_once '../../db.php';
header('Content-Type: application/json');
db_connect();

if (isset($_POST['action']) && $_POST['action'] == "add") {
	if ($_POST['middle_name'] == '') {
		echo json_encode(array("error" => "Please enter name"));
		exit;
	}
	$query = $con->prepare("SELECT  * FROM `user` 
		WHERE  mobile=:mobile  and status !='-1' ");
	$query->bindParam(":mobile", $_POST['mobile']);
	$query->execute();
	if ($query->rowCount() > 0) {
		echo json_encode(array("error" => "આ મોબાઇલ નંબર રજીસ્ટર થઇ ગયો છે.", "errorCode" => '01'));
		exit;
	} else {
		$dob = '';
		if(!empty($_POST['dob'])){
			$start_date = $_POST['dob'];
			$start_date = DateTime::createFromFormat("d/m/Y", $start_date);
			$dob = $start_date->format('Y-m-d');
		}
		$data = array(
			"p_id" => $_POST['p_id'],
			"surname" => $_POST['surname'],
			"middle_name" => $_POST['middle_name'],
			"father_name" => $_POST['father_name'],
			"email" => $_POST['email'],
			"dob" => $dob,
			"blood_group" => $_POST['blood_group'],
			"mobile" => $_POST['mobile'],
			"mobile_1" => $_POST['mobile_1'],
			"address" => $_POST['address'],
			"taluko_id" => $_POST['taluko_id'],
			"dist_id" => $_POST['dist_id'],
			"barot_id" => $_POST['barot_id'],
			"business" => $_POST['business'],
			"qualification" => $_POST['qualification'],
			"temple_id" => $_POST['temple_id'],
			"dada_id" => $_POST['dada_id'],
			"gov_job" => $_POST['gov_job'],
			"profile_pic" => $_POST['image'],
			"gender" => $_POST['gender'],
			"relationship" => $_POST['relationship'],
			"status" => '2',
			"type" => 'user',
			"date" => phpNow()
		);
		$id = insertRow("user", $data);
	}
	echo json_encode(array("success" => "Add successfully!"));
} else {
	echo json_encode(array("error" => "Add not successfully!"));
}
