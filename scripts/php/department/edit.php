<?php
include_once '../../db.php';
header('Content-Type: application/json');
db_connect();
if(isset($_POST['action']) && $_POST['action']=="edit"){

	if ($_POST['name'] == '') {
		echo json_encode(array("error" => "Please enter name"));
		exit;
	}
	$data = array(
		"name"=>$_POST['name'],
		"date"=>phpNow()
	);

	$id = updateRow("department_master",$data,array("id"=>$_POST['id']));
	echo json_encode(array("success"=>"Edit successfully!"));
}
else{
	echo json_encode(array("error"=>"Edit not successfully!"));
}

?>