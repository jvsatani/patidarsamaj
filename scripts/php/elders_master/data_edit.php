<?php
include_once '../../db.php';
header('Content-Type: application/json');
db_connect();
if(isset($_POST['action']) && $_POST['action']=="edit"){
	if ($_POST['name'] == '') {
		echo json_encode(array("error" => "Please enter fullname "));
		exit;
	}
	
	if ($_POST['mobile'] == '') {
		echo json_encode(array("error" => "Please enter mobile"));
		exit;
	}

	if ($_POST['address'] == '') {
		echo json_encode(array("error" => "Please enter address"));
		exit;
	}
	
	$data = array(
		"name" => $_POST['name'],
		"mobile" => $_POST['mobile'],
		"address" => $_POST['address'],
		"date" => phpNow()
	);
	
	$id = updateRow("kamiti_member",$data,array("id"=>$_POST['id']));
	echo json_encode(array("success"=>"Edit successfully!"));
}
else{
	echo json_encode(array("error"=>"Edit not successfully!"));
}
