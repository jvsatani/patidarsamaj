<?php
include_once '../../db.php';
header('Content-Type: application/json');
db_connect();

if (isset($_POST['action']) && $_POST['action'] == "add") {
	if ($_POST['name'] == '') {
		echo json_encode(array("error" => "Please enter full name"));
		exit;
	}
	if ($_POST['mobile'] == '') {
		echo json_encode(array("error" => "Please enter mobile"));
		exit;
	}

	if ($_POST['address'] == '') {
		echo json_encode(array("error" => "Please enter address"));
		exit;
	}

	$data = array(
		"name" => $_POST['name'],
		"mobile" => $_POST['mobile'],
			"address" => $_POST['address'],
		"status" => '1',
		"date" => phpNow()
	);
	$id = insertRow("kamiti_member", $data);
	echo json_encode(array("success" => "Add successfully!"));
} else {
	echo json_encode(array("error" => "Add not successfully!"));
}
