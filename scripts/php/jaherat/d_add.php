<?php
include_once '../../db.php';
header('Content-Type: application/json');
db_connect();

if (isset($_POST['action']) && $_POST['action'] == "add") {
	if ($_POST['image'] == '') {
		echo json_encode(array("error" => "Please select image"));
		exit;
	}
	$data = array(
		"ayojan_id" => $_POST['ayojan_id'],
		"image" => $_POST['image'],
	);
	$id = insertRow("ayojan_img", $data);
	echo json_encode(array("success" => "Add successfully!"));
} else {
	echo json_encode(array("error" => "Add not successfully!"));
}
