<?php
include_once '../../db.php';
header('Content-Type: application/json');
db_connect();
if(isset($_POST['action']) && $_POST['action']=="edit"){
	if ($_POST['title'] == '') {
		echo json_encode(array("error" => "Please enter title"));
		exit;
	}
	$data = array(
		"title" => $_POST['title'],
		"short_notes" => $_POST['short_notes'],
		"notes" => $_POST['notes'],
		"date" => phpNow()
	);
	
	$id = updateRow("ayojan",$data,array("id"=>$_POST['id']));
	echo json_encode(array("success"=>"Edit successfully!"));
}
else{
	echo json_encode(array("error"=>"Edit not successfully!"));
}
