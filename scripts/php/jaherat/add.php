<?php
include_once '../../db.php';
header('Content-Type: application/json');
db_connect();

if (isset($_POST['action']) && $_POST['action'] == "add") {
	if ($_POST['title'] == '') {
		echo json_encode(array("error" => "Please enter title"));
		exit;
	}
	$data = array(
		"title" => $_POST['title'],
		"short_notes" => $_POST['short_notes'],
		"notes" => $_POST['notes'],
		"date" => phpNow()
	);
	$id = insertRow("ayojan", $data);
	echo json_encode(array("success" => "Add successfully!"));
} else {
	echo json_encode(array("error" => "Add not successfully!"));
}
