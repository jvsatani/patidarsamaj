<?php
include_once("../../db.php");
include_once("../../functions.php");
db_connect();

$where = "";
if (!empty($_REQUEST['search'])) {
	$where .= " and (name like :search) ";
}
if (!empty($_REQUEST['id'])) {
	$where .= " and r.city_id=:city_id ";
}
$columns = explode(",", $_POST['sColumns']);
$sortCol = $_POST['iSortCol_0'];
$sortOrder = $_POST['sSortDir_0'];

$records = array();

$query = "select count(*) total
		FROM role_master r
		where status='1'   {$where} ";

$query = $con->prepare($query);
if (!empty($_REQUEST['search'])) {
	$query->bindValue(":search", "%{$_REQUEST['search']}%");
}
if (!empty($_REQUEST['id'])) {
	$query->bindValue(":city_id", "{$_REQUEST['id']}");
}
$query->execute();
$row = $query->fetch(PDO::FETCH_ASSOC);

$iTotalRecords = $row['total'];
$iDisplayLength = intval($_REQUEST['iDisplayLength']);
$iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
$iDisplayStart = intval($_REQUEST['iDisplayStart']);
$sEcho = intval($_REQUEST['sEcho']);

$records["aaData"] = array();
$query = "SELECT r.*,CONCAT(u.`middle_name`,' ',u.`father_name`,' ', IFNULL(s.`name`,'')) full_name,u.mobile
		FROM  role_master r
		LEFT JOIN user u ON u.uid=r.uid
		LEFT JOIN surname s ON s.id=u.surname_id
		WHERE r.status = '1' and r.type='dist' {$where}
		";

$query .= " order by {$columns[$sortCol]} {$sortOrder}";
$query .= " limit $iDisplayStart, $iDisplayLength";

$query = $con->prepare($query);
if (!empty($_REQUEST['search'])) {
	$query->bindValue(":search", "%{$_REQUEST['search']}%");
}
if (!empty($_REQUEST['id'])) {
	$query->bindValue(":city_id", "{$_REQUEST['id']}");
}
$query->execute();

if ($query->rowCount() > 0) {

	$idx = 0;
	while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
		$idx++;
		$id =$row['id'];
		
		$delete = "<a href='javascript:;' role='button' data-toggle='modal' data-target='#modal-del-member' class='btn btn-sm btn-danger delete ' i='{$row['id']}' data-i=\"" . htmlspecialchars(json_encode($row)) . "\"> Delete </a>";
	
		$records["aaData"][] = array(
			$row['id'],
			"{$idx}",
			$row['full_name'].'<br>'.$row['mobile'],
			"{$delete}",
		);
	}
}

$records["sEcho"] = $sEcho;
$records["iTotalRecords"] = $iTotalRecords;
$records["iTotalDisplayRecords"] = $iTotalRecords;
//$records ["total"] = $total;
echo json_encode($records);
