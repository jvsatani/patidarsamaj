<?php
include_once '../../db.php';
header('Content-Type: application/json');
db_connect();

if (isset($_POST['action']) && $_POST['action'] == "add") {
	if ($_POST['uid'] == '') {
		echo json_encode(array("error" => "Please enter mobile and find user"));
		exit;
	}

	$query = $con->prepare("SELECT  * FROM `role_master` 
	WHERE  uid=:uid  and status ='1' ");
	$query->bindParam(":uid", $_POST['uid']);
	$query->execute();
	if ($query->rowCount() > 0) {
		echo json_encode(array("error" => "This user already use!.", "errorCode" => '01'));
		exit;
	} else {

		$data = array(
			"uid" => $_POST['uid'],
			"assign_id" => $_POST['assign_id'],
			"city_id" => $_POST['city_id'],
			"type" => $_POST['type'],
			"date" => phpNow()
		);
		$id = insertRow("role_master", $data);
		$dataA = array();
		if ($_POST['type'] == 'dist') {
			$dataA['type'] = 'city_admin';
		} else {
			$dataA['type'] = 'admin';
		}
		if ($_POST['type'] == 'managment') {
			$dataA['type'] = 'management';
		}
		$dataA['ad_password'] = '12345';

		$id = updateRow("user", $dataA, array("uid" => $_POST['uid']));
		echo json_encode(array("success" => "Add successfully!"));
	}
} else {
	echo json_encode(array("error" => "Add not successfully!"));
}
