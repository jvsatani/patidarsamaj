<?php
include_once '../../db.php';
header('Content-Type: application/json');
db_connect();
if (!empty($_POST['id'])) {
	$id = $_POST['id'];
	$query = $con->prepare("SELECT *
		FROM `role_master` 
		WHERE  status = '1'  and id=:id
	");
	$query->bindParam(":id", $id);
	$query->execute();

	$List = array();
	if ($query->rowCount() > 0) {
		$List = $query->fetch(PDO::FETCH_ASSOC);

		$queryA = $con->prepare("update role_master set status = '-1'
							where id=:id");
		$queryA->bindParam(":id", $id);
		$queryA->execute();
		$data = array(
			"type" => 'user',
			"ad_password" => '',
		);
		$idA = updateRow("user", $data, array("uid" => $List['uid']));
		echo json_encode(array("success" => "Deleted successfully!"));
		exit;
	} else {
		echo json_encode(array("error" => "Invalid Action!"));
		exit;
	}
}
