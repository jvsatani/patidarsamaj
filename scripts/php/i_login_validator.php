<?php
include_once 'db.php';
if(!isset($_SESSION['user'])||$_SESSION['user']==""){
	header("Location: {$adminURL}login.php?url=".$_SERVER['REQUEST_URI']);
	exit;
}else{
	db_connect();
	$check=getRow("select * from session where session_id=:session_id and status='active' and session_type='back'",array("session_id"=>session_id()));
	if($check==false){
		header("Location: {$adminURL}logout.php");
		exit;
	}else{
		updateRow("session",array("last_seen"=>phpNow()),array("session_type"=>'back',"session_id"=>session_id()));
	}
}
?>
