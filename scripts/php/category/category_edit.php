<?php
include_once '../../db.php';
header('Content-Type: application/json');
db_connect();

if(isset($_POST['action']) && $_POST['action']=="edit"){
	$data = array(
		"name"=>$_POST['cat_name'],
		"hsn"=>$_POST['hsn'],
		"date"=>phpNow()
	);
	if (!empty($_POST['image'])) {
		$data['image'] = $_POST['image'];
	}
	$id = updateRow("category",$data,array("id"=>$_POST['id']));
	echo json_encode(array("success"=>"Edit successfully!"));
}
else{
	echo json_encode(array("error"=>"Edit not successfully!"));
}

?>