<?php
include_once '../../db.php';
header('Content-Type: application/json');
db_connect();

if(isset($_POST['action']) && $_POST['action']=="add"){
	$data = array(
		"name"=>$_POST['cat_name'],
		"image"=>$_POST['image'],
		"hsn"=>$_POST['hsn'],
		"date"=>phpNow()
	);
	$id = insertRow("category",$data);
	echo json_encode(array("success"=>"Add successfully!"));
}
else{
	echo json_encode(array("error"=>"Add not successfully!"));
}

?>