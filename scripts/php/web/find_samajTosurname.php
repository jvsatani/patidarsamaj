<?php
include '../../db.php';
include '../../functions.php';
db_connect();
header("Content-Type: application/json");

if (isset($_GET['id'])) {
	$query = $con->prepare("SELECT *
		FROM `surname` 
			 WHERE status='1' and samaj_id=:id order by name asc
				");

	$query->bindParam(":id", $_GET['id']);
	$query->execute();
	$isData = 0;
	$mallList = array();
	if ($query->rowCount() > 0) {
		$mallList = $query->fetchAll(PDO::FETCH_ASSOC);
		$isData = 1;
		echo json_encode(array("success" => $mallList, "isData" => $isData));
		exit;
	} else {
		echo json_encode(array("success" => $mallList, "isData" => $isData));
		exit;
	}
} else {
	echo json_encode(array("error" => "please Enter  correct data."));
	exit;
}
