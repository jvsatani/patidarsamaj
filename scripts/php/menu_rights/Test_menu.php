<?php
include_once '../../db.php';
header('Content-Type: application/json');
db_connect();

if (isset($_REQUEST['json'])) {
	$requestData = $_REQUEST['json'];
	$obj = json_decode($requestData);

	foreach ($obj->data as $r) {
		$data = array(
			"menu_name" => $r->menu_name,
			"menu_key" =>$r->menu_key,
			"role_id" => '9',
			"status" => '1',
			"date" => phpNow()
		);
		$id = insertRow("menu_right", $data);
	}

	echo json_encode(array("success" => "Add successfully!"));
} else {
	echo json_encode(array("error" => "Add not successfully!"));
}

// var json = {};

// var d = [
// {"id":"1","menu_name":"Institute","menu_key":"institute","role_id":"2","status":"1","date":"2020-12-11 18:44:45"},
// {"id":"2","menu_name":"Employee","menu_key":"employee","role_id":"2","status":"1","date":"2020-12-11 18:44:45"},
// {"id":"3","menu_name":"Use full link","menu_key":"use_full_link","role_id":"2","status":"1","date":"2020-12-11 18:47:21"},
// {"id":"4","menu_name":"Brochure","menu_key":"brochure","role_id":"2","status":"1","date":"2020-12-11 18:47:21"},
// {"id":"5","menu_name":"Thoughts","menu_key":"thoughts","role_id":"2","status":"1","date":"2020-12-11 18:47:41"},
// {"id":"6","menu_name":"Gallery","menu_key":"gallery","role_id":"2","status":"1","date":"2020-12-11 18:47:41"},
// {"id":"7","menu_name":"Menu Icon","menu_key":"menu_icon","role_id":"2","status":"1","date":"2020-12-11 18:48:19"},
// {"id":"8","menu_name":"eBooks","menu_key":"ebooks","role_id":"2","status":"1","date":"2020-12-11 18:48:19"},
// {"id":"9","menu_name":"Live TV","menu_key":"live_TV","role_id":"2","status":"1","date":"2020-12-11 18:49:02"},
// {"id":"10","menu_name":"Home work","menu_key":"home_work","role_id":"2","status":"1","date":"2020-12-11 18:49:02"},
// {"id":"11","menu_name":"Attendance","menu_key":"attendance","role_id":"2","status":"1","date":"2020-12-11 18:49:27"},
// {"id":"12","menu_name":"Student leave","menu_key":"student_leave","role_id":"2","status":"1","date":"2020-12-11 18:49:27"},
// {"id":"13","menu_name":"Employee notice","menu_key":"employee_notice","role_id":"2","status":"1","date":"2020-12-11 18:52:34"},
// {"id":"14","menu_name":"Student notice","menu_key":"student_notice","role_id":"2","status":"1","date":"2020-12-11 18:52:34"},
// {"id":"15","menu_name":"Exam","menu_key":"exam","role_id":"2","status":"1","date":"2020-12-11 18:53:13"},
// {"id":"16","menu_name":"Live Class","menu_key":"live_class","role_id":"2","status":"1","date":"2020-12-11 18:53:13"},
// {"id":"17","menu_name":"Employee","menu_key":"employee","role_id":"2","status":"1","date":"2020-12-11 18:53:28"},
// {"id":"18","menu_name":"Student","menu_key":"student","role_id":"2","status":"1","date":"2020-12-11 18:53:28"},
// {"id":"19","menu_name":"Fees master","menu_key":"fees_master","role_id":"2","status":"1","date":"2020-12-11 18:57:20"},
// {"id":"20","menu_name":"Material master","menu_key":"material_master","role_id":"2","status":"1","date":"2020-12-11 18:57:20"},
// {"id":"21","menu_name":"Chapter master","menu_key":"chapter_master","role_id":"2","status":"1","date":"2020-12-11 18:57:54"},
// {"id":"22","menu_name":"Time table","menu_key":"time_table","role_id":"2","status":"1","date":"2020-12-11 18:57:54"},
// {"id":"23","menu_name":"Teaching master","menu_key":"teaching_master","role_id":"2","status":"1","date":"2020-12-11 18:58:28"},
// {"id":"24","menu_name":"Calender","menu_key":"calender","role_id":"2","status":"1","date":"2020-12-11 18:58:28"},
// {"id":"25","menu_name":"Gallery","menu_key":"gallery","role_id":"2","status":"1","date":"2020-12-11 18:58:48"},
// {"id":"26","menu_name":"Standard","menu_key":"standard","role_id":"2","status":"1","date":"2020-12-11 18:58:48"},
// {"id":"27","menu_name":"eReference","menu_key":"ereference","role_id":"2","status":"1","date":"2020-12-11 18:59:00"}
// ];

// json.data = d;
// var j = JSON.stringify(json);

//  $.post("http://localhost/student-management-web-portal/scripts/php/menu_rights/Test_menu.php",{'json': j}, function(data) {
// console.log(JSON.stringify(data))
//              });