<?php
include_once '../../db.php';
header('Content-Type: application/json');
db_connect();
if (isset($_REQUEST['json'])) {
	$requestData = $_REQUEST['json'];
	$obj = json_decode($requestData);
	foreach ($obj->data as $r) {
		$data = array();
		
		$data['is_view'] =  $r->is_view;
		$data['is_add'] =  $r->is_add;
		$data['is_edit'] = $r->is_edit;
		$data['is_delete'] =  $r->is_delete;
		$id = updateRow("rights_master", $data, array("id" => $r->id));
	
	}
	echo json_encode(array("success" => "Status updated!"));
	exit;
} else {
	echo json_encode(array("error" => "Invalid Access!"));
	exit;
}
