<?php
include_once("../../db.php");
include_once("../../functions.php");
db_connect();

$where = "";
$emp_id = '';
if (!empty($_REQUEST['emp_id'])) {
	$emp_id = base64_decode($_REQUEST['emp_id']);
}
$empMaster = getRow("SELECT *
FROM emp_master 
WHERE status = 1 and  id=:id", array('id' => $emp_id));

$columns = explode(",", $_POST['sColumns']);
$sortCol = $_POST['iSortCol_0'];
$sortOrder = $_POST['sSortDir_0'];

$records = array();

$query = "select count(*) total
		FROM menu_right m
		left Join rights_master r on r.menu_id=m.id and r.emp_id=:emp_id
		where m.status='1'  and m.role_id=:role_id";

$query = $con->prepare($query);
if (!empty($_REQUEST['search'])) {
	$query->bindValue(":search", "%{$_REQUEST['search']}%");
}

$query->bindValue(":role_id", "{$empMaster['role_id']}");
$query->bindValue(":emp_id", "{$empMaster['id']}");
$query->execute();
$row = $query->fetch(PDO::FETCH_ASSOC);

$iTotalRecords = $row['total'];
$iDisplayLength = intval($_REQUEST['iDisplayLength']);
$iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
$iDisplayStart = intval($_REQUEST['iDisplayStart']);
$sEcho = intval($_REQUEST['sEcho']);

$records["aaData"] = array();
$query = "SELECT m.id,m.menu_name,m.menu_key,r.id rights_id,r.is_view,r.is_add,r.is_edit,r.is_delete,m.role_id
		FROM  menu_right m 
		left Join rights_master r on r.menu_id=m.id and r.emp_id=:emp_id
		WHERE m.status = '1' and m.role_id=:role_id
		";

$query .= " order by {$columns[$sortCol]} {$sortOrder}";
$query .= " limit $iDisplayStart, $iDisplayLength";

$query = $con->prepare($query);
$query->bindValue(":role_id", "{$empMaster['role_id']}");
$query->bindValue(":emp_id", "{$empMaster['id']}");
if (!empty($_REQUEST['search'])) {
	$query->bindValue(":search", "%{$_REQUEST['search']}%");
}
$query->execute();

if ($query->rowCount() > 0) {

	$idx = 0;
	while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
		$idx++;
		$id = $row['rights_id'];
		$rid = $row['role_id'];
		$mid = $row['id'];
		$isView = "<i class='fa fa-toggle-off fa-3x isView' data-v='1' style='color: #af4c4c;' data-i='{$id}' data-r='{$rid}' data-m='{$mid}' ></i>";
		if ($row['is_view'] && $row['is_view'] == '1') {
			$isView = "<i class='fa fa-toggle-on fa-3x isView' data-v='0' style='color: #4caf50;' data-i='{$id}' data-r='{$rid}' data-m='{$mid}'></i>";
		}
		$isAdd = "<i class='fa fa-toggle-off fa-3x isAdd' data-v='1' style='color: #af4c4c;' data-i='{$id}' data-r='{$rid}' data-m='{$mid}'></i>";
		if ($row['is_add'] && $row['is_add'] == '1') {
			$isAdd = "<i class='fa fa-toggle-on fa-3x isAdd' data-v='0' style='color: #4caf50;' data-i='{$id}' data-r='{$rid}' data-m='{$mid}'></i>";
		}
		$isEdit = "<i class='fa fa-toggle-off fa-3x isEdit' data-v='1' style='color: #af4c4c;' data-i='{$id}' data-r='{$rid}' data-m='{$mid}'></i>";
		if ($row['is_edit'] && $row['is_edit'] == '1') {
			$isEdit = "<i class='fa fa-toggle-on fa-3x isEdit' data-v='0' style='color: #4caf50;' data-i='{$id}' data-r='{$rid}' data-m='{$mid}'></i>";
		}
		$isDelete = "<i class='fa fa-toggle-off fa-3x isDelete' data-v='1' style='color: #af4c4c;' data-i='{$id}' data-r='{$rid}' data-m='{$mid}'></i>";
		if ($row['is_delete'] && $row['is_delete'] == '1') {
			$isDelete = "<i class='fa fa-toggle-on fa-3x isDelete' data-v='0' style='color: #4caf50;' data-i='{$id}' data-r='{$rid}' data-m='{$mid}'></i>";
		}

		$records["aaData"][] = array(
			$row['id'],
			"{$idx}",
			$row['menu_name'],
			"{$isView}",
			"{$isAdd}",
			"{$isEdit}",
			"{$isDelete}",
		
		);
	}
}

$records["sEcho"] = $sEcho;
$records["iTotalRecords"] = $iTotalRecords;
$records["iTotalDisplayRecords"] = $iTotalRecords;
//$records ["total"] = $total;
echo json_encode($records);
