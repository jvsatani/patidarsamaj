<?php
include_once("../../db.php");
include_once("../../functions.php");
db_connect();

$where = "";
if (!empty($_REQUEST['search'])) {
	$where .= " and (t.full_name like :search) ";
}

if (!empty($_REQUEST['f_id'])) {
	$where .= " and t.fund_type=:f_id ";
}
if (!empty($_REQUEST['r_id'])) {
	$where .= " and t.by_uid=:r_id ";
}
if (!empty($_REQUEST['emp_id'])) {
	$where .= " and t.uid=:emp_id ";
}

if (!empty($_REQUEST['sdate']) && !empty($_REQUEST['edate'])) {
	$start_date = $_REQUEST['sdate'];
	$start_date = DateTime::createFromFormat("d/m/Y", $start_date);
	$sdate = $start_date->format('Y-m-d');

	$end_date = $_REQUEST['edate'];
	$end_date = DateTime::createFromFormat("d/m/Y", $end_date);
	$edate = $end_date->format('Y-m-d');

	$where .= " and ((date(t.date) BETWEEN '{$sdate}' AND '{$edate}') OR (date(t.date) >= '{$sdate}' AND date(t.date) <= '{$edate}'))";
}

$columns = explode(",", $_POST['sColumns']);
$sortCol = $_POST['iSortCol_0'];
$sortOrder = $_POST['sSortDir_0'];

$records = array();

$query = "select count(*) total
		FROM user_transcription t
		LEFT JOIN fund_type f ON f.id=t.fund_type
		LEFT JOIN user u ON u.uid=t.uid
		where t.status >= '1'   {$where} ";

$query = $con->prepare($query);
if (!empty($_REQUEST['search'])) {
	$query->bindValue(":search", "%{$_REQUEST['search']}%");
}

if (!empty($_REQUEST['f_id'])) {
	$query->bindValue(":f_id", "{$_REQUEST['f_id']}");
}
if (!empty($_REQUEST['r_id'])) {
	$query->bindValue(":r_id", "{$_REQUEST['r_id']}");
}
if (!empty($_REQUEST['emp_id'])) {
	$query->bindValue(":emp_id", "{$_REQUEST['emp_id']}");
}
// print_r($query);
$query->execute();
$row = $query->fetch(PDO::FETCH_ASSOC);

$iTotalRecords = $row['total'];
$iDisplayLength = intval($_REQUEST['iDisplayLength']);
$iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
$iDisplayStart = intval($_REQUEST['iDisplayStart']);
$sEcho = intval($_REQUEST['sEcho']);

$records["aaData"] = array();
$query = "SELECT t.*,CONCAT(u.`middle_name`,' ',u.`father_name`,' ', IFNULL(s.`name`,'')) full_name,u.member_id,CONCAT(uu.`middle_name`,' ',uu.`father_name`,' ', ss.`name`) R_full_name,f.name fund_name
		FROM user_transcription t
		LEFT JOIN user u ON u.uid=t.uid
		LEFT JOIN user uu ON uu.uid=t.by_uid
		LEFT JOIN fund_type f ON f.id=t.fund_type
		LEFT JOIN surname s ON s.id=u.surname_id
		LEFT JOIN surname ss ON ss.id=uu.surname_id
		WHERE t.status >= '1' {$where}
		";

$query .= " order by {$columns[$sortCol]} {$sortOrder}";
$query .= " limit $iDisplayStart, $iDisplayLength";

$query = $con->prepare($query);
if (!empty($_REQUEST['search'])) {
	$query->bindValue(":search", "%{$_REQUEST['search']}%");
}
if (!empty($_REQUEST['r_id'])) {
	$query->bindValue(":r_id", "{$_REQUEST['r_id']}");
}
if (!empty($_REQUEST['f_id'])) {
	$query->bindValue(":f_id", "{$_REQUEST['f_id']}");
}
if (!empty($_REQUEST['emp_id'])) {
	$query->bindValue(":emp_id", "{$_REQUEST['emp_id']}");
}
$query->execute();

if ($query->rowCount() > 0) {

	$idx = 0;
	while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
		$idx++;
		$name = "<b>" . $row['bill_id'] . "</b><br/> " . ucfirst($row['full_name']) . "&nbsp;&nbsp; <b> ("  . $row['member_id']. ")<b>";
		$dateT = date('d/m/Y', strtotime($row['date']));
		$status = "<a href='scripts/invoice/bill.php?id={$row['id']}'  target='_blank' class='btn btn-sm btn-primary'> Print </a>";
		
	
		$records["aaData"][] = array(
			$row['id'],
			"{$idx}",
			"{$name}",
			number_format($row['amount'], 2),
			$row['fund_name'],
			$row['notes'],
			$row['R_full_name'],
			 $dateT,
			"",
		);
	}
}

$records["sEcho"] = $sEcho;
$records["iTotalRecords"] = $iTotalRecords;
$records["iTotalDisplayRecords"] = $iTotalRecords;
//$records ["total"] = $total;
echo json_encode($records);
