<?php
include_once '../../db.php';
header('Content-Type: application/json');
db_connect();
if (!empty($_POST['id'])) {
	$id = $_POST['id'];
	$action = $_POST['action'];
	$query = $con->prepare("update order_customer set status = {$action}
							where id=:id");
	$query->bindParam(":id", $id);
	$query->execute();
	$msg = "";
	if($_POST['action'] == '2'){
		$msg = "Order accept successfully!";
	}else{
		$msg = "Order reject successfully!";
	}
	echo json_encode(array("success" => $msg ));
}
