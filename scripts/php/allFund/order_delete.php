<?php
include_once '../../db.php';
header('Content-Type: application/json');
db_connect();
if (!empty($_POST['id'])) {
	$id = $_POST['id'];
	$action = $_POST['action'];

	$order_management = getRow("SELECT *
				  FROM order_management 
				  WHERE status = 1 and id=:id", array('id' => $_POST['id']));

	$order_delivery = getRow("SELECT *
				  FROM order_delivery 
				  WHERE status = 1 and id=:id", array('id' => $order_management['d_id']));

	$data = array(
		"total_qty" =>$order_delivery['total_qty']- $order_management['total_qty'],
		"total_kg" => $order_delivery['total_kg']- $order_management['total_kg'],
	);
	$pid = updateRow("order_delivery", $data, array("id" => $order_management['d_id']));

	$data = array(
		"status" => '2',
		);
	$pid = updateRow("order_customer", $data, array("id" => $order_management['order_id']));

	$query = $con->prepare("update order_management set status = '0'
							where id=:id");
	$query->bindParam(":id", $id);
	$query->execute();
			$msg = "Order reject successfully!";
	echo json_encode(array("success" => $msg));
}
