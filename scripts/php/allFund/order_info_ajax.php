<?php
include_once("../../db.php");
include_once("../../functions.php");
db_connect();

$where = "";
if (!empty($_REQUEST['search'])) {
	$where .= " and (o.product_name like :search) ";
}
if (!empty($_REQUEST['order_id'])) {
	$where .= " and o.order_id=:order_id ";
}
$columns = explode(",", $_POST['sColumns']);
$sortCol = $_POST['iSortCol_0'];
$sortOrder = $_POST['sSortDir_0'];

$records = array();

$query = "select count(*) total
		FROM order_management_details o
		where o.status >= '1'   {$where} ";

$query = $con->prepare($query);
if (!empty($_REQUEST['search'])) {
	$query->bindValue(":search", "%{$_REQUEST['search']}%");
}
if (!empty($_REQUEST['order_id'])) {
	$query->bindValue(":order_id", "{$_REQUEST['order_id']}");
}
$query->execute();
$row = $query->fetch(PDO::FETCH_ASSOC);

$iTotalRecords = $row['total'];
$iDisplayLength = intval($_REQUEST['iDisplayLength']);
$iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
$iDisplayStart = intval($_REQUEST['iDisplayStart']);
$sEcho = intval($_REQUEST['sEcho']);

$records["aaData"] = array();
$query = "SELECT o.id,o.product_name,o.cat_name, o.qty,o.price,o.priority,o.total_amount,o.total_kg
FROM order_management_details o 
WHERE o.status = 1  {$where}
		";

$query .= " order by {$columns[$sortCol]} {$sortOrder}";
$query .= " limit $iDisplayStart, $iDisplayLength";

$query = $con->prepare($query);
if (!empty($_REQUEST['search'])) {
	$query->bindValue(":search", "%{$_REQUEST['search']}%");
}
if (!empty($_REQUEST['order_id'])) {
	$query->bindValue(":order_id", "{$_REQUEST['order_id']}");
}
$query->execute();

if ($query->rowCount() > 0) {

	$idx = 0;
	while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
		$idx++;
		$edit = "<a href='javascript:;' role='button' data-toggle='modal' data-target='#modal-edit' class='btn btn-sm btn-success edit' i='{$row['id']}' data-i=\"" . htmlspecialchars(json_encode($row)) . "\"> Edit </a> ";
		$delete = "<a href='javascript:;' role='button' data-toggle='modal' data-target='#modal-del' class='btn btn-sm btn-danger delete ' i='{$row['id']}' data-i=\"" . htmlspecialchars(json_encode($row)) . "\"> Delete </a>";
		// $name = "<b>" . $row['shop_name'] . "</b>" . " - " . $row['city'] . "<br/>" . $row['shop_owner'];
		$status = "";
		$priority = "";
		if ($row['priority'] == '1') {
			$priority = "<i class='fa fa-star' style='color: #ffd900;font-size: 20px;'></i>";
		}
		$records["aaData"][] = array(
			$row['id'],
			"{$idx}",
			"{$priority} " . $row['product_name'] .'<br>&nbsp;&nbsp;&nbsp;'. $row['cat_name'],
			number_format($row['price'], 2),
			$row['qty'],
			number_format($row['total_amount'], 2),
			$row['total_kg']." Kg",
			// $row['notes'],
			"{$edit} {$delete}",
		);
	}
}

$records["sEcho"] = $sEcho;
$records["iTotalRecords"] = $iTotalRecords;
$records["iTotalDisplayRecords"] = $iTotalRecords;
//$records ["total"] = $total;
echo json_encode($records);
