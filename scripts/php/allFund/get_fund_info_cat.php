<?php
include '../../db.php';
include '../../functions.php';
db_connect();


$where = "";

if (!empty($_GET['f_id'])) {
	$where .= " and fund_type=:f_id ";
}
if (!empty($_GET['r_id'])) {
	$where .= " and by_uid=:r_id ";
}
if (!empty($_GET['emp_id'])) {
	$where .= " and uid=:emp_id ";
}
if (!empty($_GET['sdate']) && !empty($_GET['edate'])) {
	$start_date = $_GET['sdate'];
	$start_date = DateTime::createFromFormat("d/m/Y", $start_date);
	$sdate = $start_date->format('Y-m-d');

	$end_date = $_GET['edate'];
	$end_date = DateTime::createFromFormat("d/m/Y", $end_date);
	$edate = $end_date->format('Y-m-d');

	$where .= " and ((date(date) BETWEEN '{$sdate}' AND '{$edate}') OR (date(date) >= '{$sdate}' AND date(date) <= '{$edate}'))";
}
$query = $con->prepare("SELECT sum(amount) amount
		FROM `user_transcription` 
			 WHERE status='1'  {$where}
				");
if (!empty($_GET['f_id'])) {
	$query->bindValue(":f_id", "{$_GET['f_id']}");
}
if (!empty($_GET['r_id'])) {
	$query->bindValue(":r_id", "{$_GET['r_id']}");
}
if (!empty($_GET['emp_id'])) {
	$query->bindValue(":emp_id", "{$_GET['emp_id']}");
}
$query->execute();
$isData = 0;
$list = array();
$original_data = array();
if ($query->rowCount() > 0) {
	$list = $query->fetch(PDO::FETCH_ASSOC);
	if (!empty($list['amount'])) {
		$list['amount'] = number_format($list['amount'], 2);
	} else {
		$list['amount'] = '0';
	}
	echo json_encode(array("success" => $list));
	exit;
} else {
	echo json_encode(array("success" => $list));
	exit;
}
