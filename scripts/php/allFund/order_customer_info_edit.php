<?php
include_once '../../db.php';
header('Content-Type: application/json');
db_connect();

if (isset($_POST['action']) && $_POST['action'] == "edit") {
	if ($_REQUEST['qty'] == '0' || $_REQUEST['qty'] == '') {
		echo json_encode(array("error" => "Please enter qty"));
		exit;
	}
	// if ($_REQUEST['note'] == '') {
	// 	echo json_encode(array("error" => "Please enter note"));
	// 	exit;
	// }

	$product = getRow("SELECT *
				  FROM product 
				  WHERE status = 1 and id=:id", array('id' => $_POST['product_id']));

	$totalKG = 0;
	$total_amt = 0;
	if ($product['weight_type'] == 'Kg') {
		$kg = $product['weight'] * $product['qty'];
		$totalKG = $kg * $_POST['qty'];
	} else {
		$ltr = $product['weight'] * $product['qty'];
		$kg = $ltr * '0.910';
		$totalKG = $kg * $_POST['qty'];
	}
	$amt = $product['price'] * $product['qty'];
	$total_amt = $amt * $_POST['qty'];


	$data = array(
		"qty" => $_POST['qty'],
		// "notes" => $_POST['note'],
		"total_kg" => $totalKG,
		"total_amount" => $total_amt,
	);
	$pid = updateRow("order_customer_details", $data, array("id" => $_POST['id']));


	$total_qty = 0;
	$total_amount = 0;
	$total_kg = 0;
	$products = getRows("SELECT *
				  FROM order_customer_details 
				  WHERE status = 1 and order_id=:order_id", array('order_id' => $_POST['order_id']));
	foreach ($products as $row) {
		$total_qty = $row['qty'] + $total_qty;
		$total_kg = $row['total_kg'] + $total_kg;
		$total_amount = $row['total_amount'] + $total_amount;
	}
	$data = array(
		"total_qty" => $total_qty,
		"total_kg" => $total_kg,
		"total_amount" => $total_amount,
	);
	$id = updateRow("order_customer", $data, array("id" => $_POST['order_id']));
	echo json_encode(array("success" => "Edit successfully!"));
} else {
	echo json_encode(array("error" => "Edit not successfully!"));
}
