<?php
include_once '../../db.php';
header('Content-Type: application/json');
db_connect();
if(isset($_POST['action']) && $_POST['action']=="edit"){
	$data = array(
		"name" => $_POST['name'],
		"sender_id" => $_POST['sender_id'],
		"sender_type" => $_POST['sender_type'],
		"uid" => $_POST['uid'],
		"date" => phpNow()
	);
	if (!empty($_POST['image'])) {
		$data['banner'] = $_POST['image'];
	}
	$id = updateRow("video",$data,array("id"=>$_POST['id']));
	echo json_encode(array("success"=>"Edit successfully!"));
}
else{
	echo json_encode(array("error"=>"Edit not successfully!"));
}
