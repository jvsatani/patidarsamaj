<?php
include_once '../../db.php';
header('Content-Type: application/json');
db_connect();

if (isset($_POST['action']) && $_POST['action'] == "add") {
	if ($_POST['title'] == '') {
		echo json_encode(array("error" => "Please enter name"));
		exit;
	}
	$data = array(
		"name" => $_POST['name'],
		"title" => $_POST['title'],
		"date" => phpNow()
	);
	if (!empty($_POST['document'])) {
		$data['document'] = $_POST['document'];
	}
	if (!empty($_POST['banner'])) {
		$data['banner'] = $_POST['banner'];
	}
	
	$id = insertRow("ebook", $data);
	echo json_encode(array("success" => "Add successfully!"));
} else {
	echo json_encode(array("error" => "Add not successfully!"));
}
