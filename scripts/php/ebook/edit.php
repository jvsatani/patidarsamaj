<?php
include_once '../../db.php';
header('Content-Type: application/json');
db_connect();
if(isset($_POST['action']) && $_POST['action']=="edit"){
	$data = array(
		"title" => $_POST['title'],
		"name" => $_POST['name'],
		"date" => phpNow()
	);
	if (!empty($_POST['document'])) {
		$data['document'] = $_POST['document'];
	}
	if (!empty($_POST['banner'])) {
		$data['banner'] = $_POST['banner'];
	}
	$id = updateRow("ebook",$data,array("id"=>$_POST['id']));
	echo json_encode(array("success"=>"Edit successfully!"));
}
else{
	echo json_encode(array("error"=>"Edit not successfully!"));
}

?>