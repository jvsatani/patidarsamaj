<?php
include_once("../db.php");
header("Content-Type: application/json");


if(isset($_POST['action']) && $_POST['action']=="login"){
	db_connect();
	//$url=$_POST['url'];
	$admin_username=$_POST['username'];
	$admin_password=$_POST['password'];

	$query=$con->prepare("
		SELECT u.uid,s.name surname,u.middle_name,u.father_name,u.mobile,u.address FROM user u
		LEFT JOIN surname s On s.id=u.surname_id
		WHERE u.mobile=:admin_username and u.ad_password=:admin_password and (u.type='management' OR u.type='admin') and u.status='2'");
	$query->bindParam(":admin_username", $admin_username);
	$query->bindParam(":admin_password", $admin_password);
	$query->execute();

	
	if($query->rowCount()==1){
		$row=$query->fetch(PDO::FETCH_ASSOC);

		// if($row['type'] == 'user'){
			// echo json_encode(array("error"=>"Access Denied."));
			// return;
		// }
		
		$_SESSION['logged'] = TRUE;
        $_SESSION['access'] = $row;
		
		$query=$con->prepare("update user set lastlogin=now()
			where mobile=:admin_username");
		$query->bindParam(":admin_username", $admin_username);
		$query->execute();
		
        $data=array();
    		$data['session_id']=session_id();
    		$data['uid']=$row['uid'];
    		$data['user_name']=$admin_username;
    		$data['user_pwd']=$admin_password;
    		$data['ip']=$_SERVER['REMOTE_ADDR'];
    		$data['user_agent']=$_SERVER['HTTP_USER_AGENT'];
    		$data['status']="active";
    		$data['logout_at']=NULL;
    		$data['last_seen']=phpNow();
    		$data['stamp']=phpNow();
    		$data['session_type']="back";
    		insertRow("session",$data,array("session_id"=>session_id()));
        
		echo json_encode(array("success"=>"Logged in..","route"=>"home", "user"=>$_SESSION['access']));
	}else{
		
		$dt=array();
        $dt['user']=$admin_username;
        $dt['password']=$admin_password;
        $ip=$_SERVER['REMOTE_ADDR'];
        $user_agent=$_SERVER['HTTP_USER_AGENT'];
        logEvent("LOGIN_ATTEMPT","Login Failed",json_encode($dt),0,$ip,$user_agent);
		echo json_encode(array("error"=>"Failed login. Please try again."));
	}
}

db_close();
