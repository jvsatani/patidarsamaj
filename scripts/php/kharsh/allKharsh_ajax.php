<?php
include_once("../../db.php");
include_once("../../functions.php");
db_connect();

$where = "";
if (!empty($_REQUEST['search'])) {
	$where .= " and (k.full_name like :search) ";
}

// if (!empty($_REQUEST['f_id'])) {
// 	$where .= " and t.fund_type=:f_id ";
// }
if (!empty($_REQUEST['f_id'])) {
	$where .= " and k.bank_type=:f_id ";
}
if (!empty($_REQUEST['uid'])) {
	$where .= " and k.uid=:uid ";
}

if (!empty($_REQUEST['sdate']) && !empty($_REQUEST['edate'])) {
	$start_date = $_REQUEST['sdate'];
	$start_date = DateTime::createFromFormat("d/m/Y", $start_date);
	$sdate = $start_date->format('Y-m-d');

	$end_date = $_REQUEST['edate'];
	$end_date = DateTime::createFromFormat("d/m/Y", $end_date);
	$edate = $end_date->format('Y-m-d');

	$where .= " and ((date(k.date) BETWEEN '{$sdate}' AND '{$edate}') OR (date(k.date) >= '{$sdate}' AND date(k.date) <= '{$edate}'))";
}

$columns = explode(",", $_POST['sColumns']);
$sortCol = $_POST['iSortCol_0'];
$sortOrder = $_POST['sSortDir_0'];

$records = array();

$query = "select count(*) total
		FROM kharsh_transcription k
		LEFT JOIN user u ON u.uid=k.uid
		where k.status >= '1'   {$where} ";

// LEFT JOIN fund_type f ON f.id=t.fund_type
$query = $con->prepare($query);
if (!empty($_REQUEST['search'])) {
	$query->bindValue(":search", "%{$_REQUEST['search']}%");
}

if (!empty($_REQUEST['f_id'])) {
	$query->bindValue(":f_id", "{$_REQUEST['f_id']}");
}
if (!empty($_REQUEST['uid'])) {
	$query->bindValue(":uid", "{$_REQUEST['uid']}");
}
// if (!empty($_REQUEST['r_id'])) {
// 	$query->bindValue(":r_id", "{$_REQUEST['r_id']}");
// }
// print_r($query);
$query->execute();
$row = $query->fetch(PDO::FETCH_ASSOC);

$iTotalRecords = $row['total'];
$iDisplayLength = intval($_REQUEST['iDisplayLength']);
$iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
$iDisplayStart = intval($_REQUEST['iDisplayStart']);
$sEcho = intval($_REQUEST['sEcho']);

$records["aaData"] = array();
$query = "SELECT k.*,CONCAT(u.`middle_name`,' ',u.`father_name`,' ', IFNULL(s.`name`,'')) full_name,u.member_id,p.name paid_name,p.business_name,p.mobile paid_mobile,p.address paid_address
		FROM kharsh_transcription k
		LEFT JOIN user u ON u.uid=k.by_uid
		LEFT JOIN purchase_account p ON p.id=k.uid
		LEFT JOIN surname s ON s.id=u.surname_id
		WHERE k.status >= '1' {$where}
		";
// -- LEFT JOIN fund_type f ON f.id=t.fund_type

$query .= " order by {$columns[$sortCol]} {$sortOrder}";
$query .= " limit $iDisplayStart, $iDisplayLength";

$query = $con->prepare($query);
if (!empty($_REQUEST['search'])) {
	$query->bindValue(":search", "%{$_REQUEST['search']}%");
}
if (!empty($_REQUEST['f_id'])) {
	$query->bindValue(":f_id", "{$_REQUEST['f_id']}");
}
if (!empty($_REQUEST['uid'])) {
	$query->bindValue(":uid", "{$_REQUEST['uid']}");
}
// if (!empty($_REQUEST['f_id'])) {
// 	$query->bindValue(":f_id", "{$_REQUEST['f_id']}");
// }

$query->execute();

if ($query->rowCount() > 0) {

	$idx = 0;
	while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
		$idx++;
		$dateT = date('d/m/Y', strtotime($row['date']));
		$amt = number_format($row['amount'], 2);
		$edit = "<a href='javascript:;' role='button' data-toggle='modal' data-target='#modal-edit' class='btn btn-sm btn-green edit' i='{$row['id']}' data-i=\"" . htmlspecialchars(json_encode($row)) . "\"  title='Edit'> <i class='fa fa-pencil-square-o tableFontIcon'></i> </a> ";
		$delete = "<a href='javascript:;' role='button' data-toggle='modal' data-target='#modal-del' class='btn btn-sm btn-danger delete ' i='{$row['id']}' data-i=\"" . htmlspecialchars(json_encode($row)) . "\" title='Delete'> <i class='fa fa-trash tableFontIcon'></i></a>";
		
		$records["aaData"][] = array(
			$row['id'],
			"{$idx}",
			$row['bill_id'],
			$row['business_name'].'</br> '.$row['paid_name'].'</br> Mo. '.$row['paid_mobile'].'</br> Address: '.$row['paid_address'],
			$row['notes'],
			ucfirst($row['full_name']),
			ucfirst($row['bank_type']),
			$amt,
			$dateT,
			$edit,
		);
	}
}

$records["sEcho"] = $sEcho;
$records["iTotalRecords"] = $iTotalRecords;
$records["iTotalDisplayRecords"] = $iTotalRecords;
//$records ["total"] = $total;
echo json_encode($records);
