<?php
include '../../db.php';
include '../../functions.php';
db_connect();

$where = "";


if (!empty($_GET['f_id'])) {
	$where .= " and t.bank_type=:f_id ";
}
if (!empty($_GET['r_id'])) {
	$where .= " and t.by_uid=:r_id ";
}
if (!empty($_GET['emp_id'])) {
	$where .= " and t.uid=:emp_id ";
}
if (!empty($_GET['sdate']) && !empty($_GET['edate'])) {
	$start_date = $_GET['sdate'];
	$start_date = DateTime::createFromFormat("d/m/Y", $start_date);
	$sdate = $start_date->format('Y-m-d');

	$end_date = $_GET['edate'];
	$end_date = DateTime::createFromFormat("d/m/Y", $end_date);
	$edate = $end_date->format('Y-m-d');

	$where .= "and ((date(t.date) BETWEEN '{$sdate}' AND '{$edate}') OR (date(t.date) >= '{$sdate}' AND date(t.date) <= '{$edate}'))";
}
$query = $con->prepare("SELECT *
		FROM `fund_type` 
			 WHERE status='1'  
				");

// $query->bindParam(":d_id", $_GET['d_id']);
$query->execute();
$isData = 0;
$list = array();
$original_data = array();
if ($query->rowCount() > 0) {
	$list = $query->fetchAll(PDO::FETCH_ASSOC);
	$isData = 1;


	foreach ($list as $row) {
		$dat = null;
		// $order_master = getRow("SELECT sum(od.qty) qty,od.product_name,o.salesman_id
		// 		FROM order_management_details od 
		// 		LEFT JOIN order_management o ON o.id=od.order_id
		// 		WHERE od.status = 1 and od.d_id=:d_id and od.pro_id=:pro_id {$where}", array('d_id' => $_GET['d_id'], 'pro_id' => $row['id']));
		$queryA = $con->prepare("SELECT sum(t.amount) amount
			 		FROM kharsh_transcription t 
					WHERE t.status = 1 and  t.fund_id=:fund_type {$where} ");

		$queryA->bindParam(":fund_type", $row['id']);
		// $queryA->bindParam(":d_id", $_GET['d_id']);
		if (!empty($_GET['f_id'])) {
			$queryA->bindValue(":f_id", "{$_GET['f_id']}");
		}
		if (!empty($_GET['r_id'])) {
			$queryA->bindValue(":r_id", "{$_GET['r_id']}");
		}
		if (!empty($_GET['emp_id'])) {
			$queryA->bindValue(":emp_id", "{$_GET['emp_id']}");
		}
		// if (!empty($_GET['s_id'])) {
		// 	$queryA->bindValue(":s_id", "{$_GET['s_id']}");
		// }
		$queryA->execute();
		if ($queryA->rowCount() > 0) {
			$order_master = $queryA->fetch(PDO::FETCH_ASSOC);

			$dat['id'] = $row['id'];
			// $dat['product_info'] = $order_master;
			if (!empty($order_master['amount'])) {
				$dat['amount'] = number_format($order_master['amount'], 2);
				array_push($original_data, $dat);
			} else {
				$dat['amount'] = '0';
				array_push($original_data, $dat);
			}
		}
	}
	echo json_encode(array("success" => $original_data));
	exit;
} else {
	echo json_encode(array("success" => $original_data));
	exit;
}
