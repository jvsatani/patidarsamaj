<?php
include_once '../../db.php';
header('Content-Type: application/json');
db_connect();

if (isset($_POST['action']) && $_POST['action'] == "add") {
	if ($_POST['title'] == '') {
		echo json_encode(array("error" => "Please enter name"));
		exit;
	}
	$data = array(
		"title" => $_POST['title'],
		"qualification" => $_POST['qualification'],
		"description" => $_POST['description'],
		"location" => $_POST['location'],
		"mobile" => $_POST['mobile'],
		"type" => $_POST['type'],
		"date" => phpNow()
	);
	if (!empty($_POST['document'])) {
		$data['document'] = $_POST['document'];
	}
	if (!empty($_POST['company_name'])) {
		$data['company_name'] = $_POST['company_name'];
	}
	if (!empty($_POST['vacancies'])) {
		$data['vacancies'] = $_POST['vacancies'];
	}
	if (!empty($_POST['website'])) {
		$data['website'] = $_POST['website'];
	}
	if (!empty($_POST['salary'])) {
		$data['salary'] = $_POST['salary'];
	}
	if (!empty($_POST['name'])) {
		$data['name'] = $_POST['name'];
	}
	$id = insertRow("job", $data);
	echo json_encode(array("success" => "Add successfully!"));
} else {
	echo json_encode(array("error" => "Add not successfully!"));
}
