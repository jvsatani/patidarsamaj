<?php
include_once("../../db.php");
include_once("../../functions.php");
db_connect();

$where = "";
$type = "";
if (!empty($_REQUEST['search'])) {
	$where .= " and (name like :search) ";
}
if (!empty($_REQUEST['type'])) {
	$where .= " and type=:type ";
}
$columns = explode(",", $_POST['sColumns']);
$sortCol = $_POST['iSortCol_0'];
$sortOrder = $_POST['sSortDir_0'];

$records = array();

$query = "select count(*) total
		FROM job 
		where status='1' {$where} ";

$query = $con->prepare($query);
if (!empty($_REQUEST['search'])) {
	$query->bindValue(":search", "%{$_REQUEST['search']}%");
}
if (!empty($_REQUEST['type'])) {
	$query->bindValue(":type", "{$_REQUEST['type']}");
}
$query->execute();
$row = $query->fetch(PDO::FETCH_ASSOC);

$iTotalRecords = $row['total'];
$iDisplayLength = intval($_REQUEST['iDisplayLength']);
$iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
$iDisplayStart = intval($_REQUEST['iDisplayStart']);
$sEcho = intval($_REQUEST['sEcho']);

$records["aaData"] = array();
$query = "SELECT *
		FROM  job 
		WHERE status = '1'  {$where}
		";

$query .= " order by {$columns[$sortCol]} {$sortOrder}";
$query .= " limit $iDisplayStart, $iDisplayLength";

$query = $con->prepare($query);
if (!empty($_REQUEST['search'])) {
	$query->bindValue(":search", "%{$_REQUEST['search']}%");
}
if (!empty($_REQUEST['type'])) {
	$query->bindValue(":type", "{$_REQUEST['type']}");
}

$query->execute();

if ($query->rowCount() > 0) {

	$idx = 0;
	while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
		$idx++;
		$id = $row['id'];
		$t = '';
		if ($_GET['type'] == 'company') {
			$t = "</br></br> કંપની નામ: " . $row['company_name'];
		} else {
			$t = "</br></br> ઉમેદવાર નામ: " . $row['name'];
		}
		$row['document'] = getFullImage($row['document']);
		$name = $row['title'] . $t . "</br></br> મોબાઈલ: " . $row['mobile'];
		$q = '';
		if ($_GET['type'] == 'company') {
			$q = "</br></br> જગ્યા: " . $row['vacancies'] . "</br></br> વેબસાઈટ: " . $row['website'] . "</br></br> પગાર ધોરણ: " . $row['salary'];
		}
		$que = $row['qualification'] . $q;
		$file = '';
		if (!empty($row['document'])) {
			$img = $row['document'];
			$file = "<a href='{$img}' target='_blank' > <img id='i1' style='height:50px' src='scripts/img/file.png'></a> ";
		}
		$edit = "";
		if (menuRights($_REQUEST['uid'], 'job', 'edit')) {
			$edit = "<a href='javascript:;' role='button' data-toggle='modal' data-target='#modal-edit' class='btn btn-sm btn-green edit' i='{$row['id']}' data-i=\"" . htmlspecialchars(json_encode($row)) . "\"> Edit </a> ";
		}
		$delete = "";
		if (menuRights($_REQUEST['uid'], 'job', 'delete')) {
			$delete = "<a href='javascript:;' role='button' data-toggle='modal' data-target='#modal-del' class='btn btn-sm btn-danger delete ' i='{$row['id']}' data-i=\"" . htmlspecialchars(json_encode($row)) . "\"> Delete </a>";
		}

		$records["aaData"][] = array(
			$row['id'],
			"{$idx}",
			"{$name}",
			$row['description'],
			"{$que}",
			"{$file}",
			"{$edit} {$delete}",
		);
	}
}

$records["sEcho"] = $sEcho;
$records["iTotalRecords"] = $iTotalRecords;
$records["iTotalDisplayRecords"] = $iTotalRecords;
//$records ["total"] = $total;
echo json_encode($records);
