<?php
include_once '../../db.php';
header('Content-Type: application/json');
db_connect();
if (isset($_POST['action']) && $_POST['action'] == "edit") {
	$data = array(
		"title" => $_POST['title'],
		"qualification" => $_POST['qualification'],
		"description" => $_POST['description'],
		"mobile" => $_POST['mobile'],
		"location" => $_POST['location'],
		"date" => phpNow()
	);
	if (!empty($_POST['document'])) {
		$data['document'] = $_POST['document'];
	}
	if (!empty($_POST['company_name'])) {
		$data['company_name'] = $_POST['company_name'];
	}
	if (!empty($_POST['vacancies'])) {
		$data['vacancies'] = $_POST['vacancies'];
	}
	if (!empty($_POST['website'])) {
		$data['website'] = $_POST['website'];
	}
	if (!empty($_POST['salary'])) {
		$data['salary'] = $_POST['salary'];
	}
	if (!empty($_POST['name'])) {
		$data['name'] = $_POST['name'];
	}
	$id = updateRow("job", $data, array("id" => $_POST['id']));
	echo json_encode(array("success" => "Edit successfully!"));
} else {
	echo json_encode(array("error" => "Edit not successfully!"));
}
