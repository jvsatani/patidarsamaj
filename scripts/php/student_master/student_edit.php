<?php
include_once '../../db.php';
header('Content-Type: application/json');
db_connect();
if (isset($_POST['action']) && $_POST['action'] == "edit") {
	if ($_POST['student_name'] == '') {
		echo json_encode(array("error" => "Please enter student name"));
		exit;
	}
	if ($_POST['parents_name'] == '') {
		echo json_encode(array("error" => "Please enter parents name"));
		exit;
	}

	if ($_POST['mobile'] == '') {
		echo json_encode(array("error" => "Please enter mobile"));
		exit;
	}

	if ($_POST['address'] == '') {
		echo json_encode(array("error" => "Please enter address"));
		exit;
	}
	$join_date = '';
	if (!empty($_POST['join_date'])) {
		$start_date = $_POST['join_date'];
		$start_date = DateTime::createFromFormat("d/m/Y", $start_date);
		$join_date = $start_date->format('Y-m-d');
	}
	$data = array(
		"student_name" => $_POST['student_name'],
		"parents_name" => $_POST['parents_name'],
		"mobile" => $_POST['mobile'],
		"parents_mobile" => $_POST['parents_mobile'],
		"reference" => $_POST['reference'],
		"address" => $_POST['address'],
		"city" => $_POST['city'],
		"notes" => $_POST['notes'],
		"join_date" => $join_date,
		"date" => phpNow()
	);

	$id = updateRow("student_scholarship", $data, array("id" => $_POST['id']));
	echo json_encode(array("success" => "Edit successfully!"));
} else {
	echo json_encode(array("error" => "Edit not successfully!"));
}
