<?php
include_once("../../db.php");
include_once("../../functions.php");
db_connect();

$where = "";
if (!empty($_REQUEST['search'])) {
	$where .= " and (student_name like :search OR parents_name like :search OR mobile like :search) ";
}

$columns = explode(",", $_POST['sColumns']);
$sortCol = $_POST['iSortCol_0'];
$sortOrder = $_POST['sSortDir_0'];

$records = array();

$query = "select count(*) total
		FROM student_scholarship s 
		where s.status ='1'   {$where} ";

$query = $con->prepare($query);
if (!empty($_REQUEST['search'])) {
	$query->bindValue(":search", "%{$_REQUEST['search']}%");
}
// print_r($query);
$query->execute();
$row = $query->fetch(PDO::FETCH_ASSOC);

$iTotalRecords = $row['total'];
$iDisplayLength = intval($_REQUEST['iDisplayLength']);
$iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
$iDisplayStart = intval($_REQUEST['iDisplayStart']);
$sEcho = intval($_REQUEST['sEcho']);

$records["aaData"] = array();
$query = "SELECT s.*
		FROM  student_scholarship s
		WHERE s.status = '1' {$where}
		";

$query .= " order by {$columns[$sortCol]} {$sortOrder}";
$query .= " limit $iDisplayStart, $iDisplayLength";

$query = $con->prepare($query);
if (!empty($_REQUEST['search'])) {
	$query->bindValue(":search", "%{$_REQUEST['search']}%");
}
$query->execute();

if ($query->rowCount() > 0) {

	$idx = 0;
	while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
		$idx++;
		$id = $row['id'];
		if ($row['join_date'] != '0000-00-00') {
			$row['join_date'] = date('d/m/Y', strtotime($row['join_date']));
		} else {
			$row['join_date'] = '';
		}
		$view = "<a href='javascript:;' role='button'  class='btn btn-sm btn-indigo guest ' i='{$row['id']}' data-i=\"" . htmlspecialchars(json_encode($row)) . "\" title='Reject'> <i class='fa fa-eye tableFontIcon'></i></a>";
		$edit = "";
		$delete = "";
		if (menuRights($_REQUEST['uid'], 'student', 'edit')) {
			$edit = "<a href='javascript:;' role='button' data-toggle='modal' data-target='#modal-edit' class='btn btn-sm btn-green edit' i='{$row['id']}' data-i=\"" . htmlspecialchars(json_encode($row)) . "\"> Edit </a> ";
		}
		if (menuRights($_REQUEST['uid'], 'student', 'delete')) {
			$delete = "<a href='javascript:;' role='button' data-toggle='modal' data-target='#modal-del' class='btn btn-sm btn-danger delete ' i='{$row['id']}' data-i=\"" . htmlspecialchars(json_encode($row)) . "\"> Delete </a>";
		}
		$records["aaData"][] = array(
			$row['id'],
			"{$idx}",
			$row['student_name'] . '</br>Parents: ' . $row['parents_name'],
			$row['mobile'] . '</br> Mobile 2: ' . $row['parents_mobile'],
			$row['address'] . '</br>' . $row['city'],
			$row['reference'],
			$row['notes'],
			$row['join_date'],
			"{$view} {$edit} {$delete}",
		);
	}
}

$records["sEcho"] = $sEcho;
$records["iTotalRecords"] = $iTotalRecords;
$records["iTotalDisplayRecords"] = $iTotalRecords;
//$records ["total"] = $total;
echo json_encode($records);
