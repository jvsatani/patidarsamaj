<?php
include_once '../../db.php';
header('Content-Type: application/json');
db_connect();
if(isset($_POST['action']) && $_POST['action']=="edit"){

	$data = array(
		"student_id" => $_POST['student_id'],
		"school_name" => $_POST['school_name'],
		"school_address" => $_POST['school_address'],
		"school_mobile" => $_POST['school_mobile'],
		"std_id" => $_POST['std_id'],
		"donation_id" => $_POST['donation_id'],
		"donation_fees" => $_POST['donation_fees'],
		"total_fees" => $_POST['total_fees'],
		"scholarship_fees" => $_POST['scholarship_fees'],
		"parents_fees" => $_POST['parents_fees'],
		"notes" => $_POST['notes'],
		"status" => '1',
		"date" => phpNow()
	);
	
	$id = updateRow("student_scholarship_info",$data,array("id"=>$_POST['id']));
	echo json_encode(array("success"=>"Edit successfully!"));
}
else{
	echo json_encode(array("error"=>"Edit not successfully!"));
}
