<?php
include_once '../../db.php';
header('Content-Type: application/json');
db_connect();

if (isset($_POST['action']) && $_POST['action'] == "add") {
	if ($_POST['name'] == '') {
		echo json_encode(array("error" => "Please enter name"));
		exit;
	}
	$data = array(
		"name" => $_POST['name'],
		"banner" => $_POST['image'],
		"sender_id" => $_POST['sender_id'],
		"sender_type" => $_POST['sender_type'],
		"uid" => $_POST['uid'],
		"date" => phpNow()
	);
	$id = insertRow("gallery", $data);
	echo json_encode(array("success" => "Add successfully!"));
} else {
	echo json_encode(array("error" => "Add not successfully!"));
}
