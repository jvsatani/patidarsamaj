<?php
include_once("../../db.php");
include_once("../../functions.php");
db_connect();

$where = "";
$type = "";
if (!empty($_REQUEST['search'])) {
	$where .= " and (name like :search) ";
}
if (!empty($_REQUEST['id'])) {
	$where .= " and g_id=:g_id ";
}
$columns = explode(",", $_POST['sColumns']);
$sortCol = $_POST['iSortCol_0'];
$sortOrder = $_POST['sSortDir_0'];

$records = array();

$query = "select count(*) total
		FROM gallery_details 
		where status='1' and type='image' {$where} ";

$query = $con->prepare($query);
if (!empty($_REQUEST['search'])) {
	$query->bindValue(":search", "%{$_REQUEST['search']}%");
}
if (!empty($_REQUEST['id'])) {
	$query->bindValue(":g_id", "{$_REQUEST['id']}");
}
$query->execute();
$row = $query->fetch(PDO::FETCH_ASSOC);

$iTotalRecords = $row['total'];
$iDisplayLength = intval($_REQUEST['iDisplayLength']);
$iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
$iDisplayStart = intval($_REQUEST['iDisplayStart']);
$sEcho = intval($_REQUEST['sEcho']);

$records["aaData"] = array();
$query = "SELECT *
		FROM  gallery_details 
		WHERE  status = '1' and type='image' {$where}
		";

$query .= " order by {$columns[$sortCol]} {$sortOrder}";
$query .= " limit $iDisplayStart, $iDisplayLength";

$query = $con->prepare($query);
if (!empty($_REQUEST['search'])) {
	$query->bindValue(":search", "%{$_REQUEST['search']}%");
}
if (!empty($_REQUEST['id'])) {
	$query->bindValue(":g_id", "{$_REQUEST['id']}");
}
$query->execute();

if ($query->rowCount() > 0) {

	$idx = 0;
	while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
		$idx++;
		$id = $row['id'];
		$row['image'] = getFullImage($row['image']);;

		$edit = "";
		if (menuRights($_REQUEST['uid'], 'gallery', 'edit')) {
			$edit = "<a href='javascript:;' role='button' data-toggle='modal' data-target='#modal-edit' class='btn btn-sm btn-green edit' i='{$row['id']}' data-i=\"" . htmlspecialchars(json_encode($row)) . "\"> Edit </a> ";
		}
		$delete = "";
		if (menuRights($_REQUEST['uid'], 'gallery', 'delete')) {
			$delete = "<a href='javascript:;' role='button' data-toggle='modal' data-target='#modal-del' class='btn btn-sm btn-danger delete ' i='{$row['id']}' data-i=\"" . htmlspecialchars(json_encode($row)) . "\"> Delete </a>";
		}
		$image = "<a href='{$row['image']}' data-lightbox='gallery-group-1'><img src='{$row['image']}' class='img-thumbnail' alt='' style='max-height: 35px;'></a>";
		$records["aaData"][] = array(
			$row['id'],
			"{$idx}",
				"{$image}",
			"{$delete}",
		);
	}
}

$records["sEcho"] = $sEcho;
$records["iTotalRecords"] = $iTotalRecords;
$records["iTotalDisplayRecords"] = $iTotalRecords;
//$records ["total"] = $total;
echo json_encode($records);
