<?php
include '../db.php';
include '../functions.php';
db_connect();
header("Content-Type: application/json");
if (empty ( $_FILES['file'] )) {
	echo json_encode ( array (
			"error" => "Please upload photo" 
	) );
	exit ();
}

$file=$_FILES['file'];
$file_type = pathinfo($file["name"],PATHINFO_EXTENSION);
$file_type=strtolower(trim($file_type));
if($file_type!="jpg" && $file_type!="jpeg" && $file_type!="png" && $file_type!="gif"){
	echo json_encode(array("error"=>"Invalid file type."));
	exit;
}else{
	ini_set ( 'memory_limit', '-1' );
	set_time_limit(0);
	$name=uploadImage($file);
	echo json_encode(array("success"=>" Photo uploaded ","name"=>$name,"url"=>getFullImage($name)));
}

?>
