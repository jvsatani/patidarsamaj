<?php
include_once '../../db.php';
header('Content-Type: application/json');
db_connect();
if(isset($_POST['action']) && $_POST['action']=="edit"){
	if ($_POST['name'] == '') {
		echo json_encode(array("error" => "Please enter fullname "));
		exit;
	}
	
	if ($_POST['mobile'] == '') {
		echo json_encode(array("error" => "Please enter mobile"));
		exit;
	}

	if ($_POST['address'] == '') {
		echo json_encode(array("error" => "Please enter address"));
		exit;
	}
	$join_date = '';
	if (!empty($_POST['join_date'])) {
		$start_date = $_POST['join_date'];
		$start_date = DateTime::createFromFormat("d/m/Y", $start_date);
		$join_date = $start_date->format('Y-m-d');
	}
	$data = array(
		"name" => $_POST['name'],
		"mobile" => $_POST['mobile'],
		"village" => $_POST['village'],
		"taluko" => $_POST['taluko'],
		"dist" => $_POST['dist'],
		"approv_amt" => $_POST['approv_amt'],
		"address" => $_POST['address'],
		"yojna_name" => $_POST['yojna_name'],
		"amount" => $_POST['amount'],
		"join_date" => $join_date,
		"date" => phpNow()
	);
	
	$id = updateRow("labharthi_master",$data,array("id"=>$_POST['id']));
	echo json_encode(array("success"=>"Edit successfully!"));
}
else{
	echo json_encode(array("error"=>"Edit not successfully!"));
}
