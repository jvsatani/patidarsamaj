<?php
include '../db.php';
include '../functions.php';
db_connect();
header("Content-Type: application/json");
if (empty ( $_FILES['file'] )) {
	echo json_encode ( array (
			"error" => "Please upload Document" 
	) );
	exit ();
}

$file=$_FILES['file'];
$file_type = pathinfo($file["name"],PATHINFO_EXTENSION);
$file_type=strtolower(trim($file_type));
	ini_set ( 'memory_limit', '-1' );
	set_time_limit(0);

	$name=uploadFileS($file);
	echo json_encode(array("success"=>"Document uploaded ","name"=>$name,"url"=>getFullImage($name)));

?>
