<?php
session_start();
$_SESSION['access']="";
include_once '../db.php';
db_connect();
updateRow("session",array("logout_at"=>phpNow(),"status"=>"inactive"),array("session_id"=>session_id()));

$_SESSION['access']="";
session_destroy();

header("Location: {$adminURL}");
?>
