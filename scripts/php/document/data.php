<?php
include_once("../../db.php");
include_once("../../functions.php");
db_connect();

$where = "";
$type = "";
if (!empty($_REQUEST['search'])) {
	$where .= " and (name like :search) ";
}
if (!empty($_REQUEST['type'])) {
	$where .= " and type=:type ";
}
$columns = explode(",", $_POST['sColumns']);
$sortCol = $_POST['iSortCol_0'];
$sortOrder = $_POST['sSortDir_0'];

$records = array();

$query = "select count(*) total
		FROM gov_document 
		where status='1' {$where} ";

$query = $con->prepare($query);
if (!empty($_REQUEST['search'])) {
	$query->bindValue(":search", "%{$_REQUEST['search']}%");
}
if (!empty($_REQUEST['type'])) {
	$query->bindValue(":type", "{$_REQUEST['type']}");
}
$query->execute();
$row = $query->fetch(PDO::FETCH_ASSOC);

$iTotalRecords = $row['total'];
$iDisplayLength = intval($_REQUEST['iDisplayLength']);
$iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
$iDisplayStart = intval($_REQUEST['iDisplayStart']);
$sEcho = intval($_REQUEST['sEcho']);

$records["aaData"] = array();
$query = "SELECT *
		FROM  gov_document 
		WHERE status = '1'  {$where}
		";

$query .= " order by {$columns[$sortCol]} {$sortOrder}";
$query .= " limit $iDisplayStart, $iDisplayLength";

$query = $con->prepare($query);
if (!empty($_REQUEST['search'])) {
	$query->bindValue(":search", "%{$_REQUEST['search']}%");
}
if (!empty($_REQUEST['type'])) {
	$query->bindValue(":type", "{$_REQUEST['type']}");
}

$query->execute();

if ($query->rowCount() > 0) {

	$idx = 0;
	while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
		$idx++;
		$id = $row['id'];
		$row['document'] = getFullImage($row['document']);
		$linkk = $row['link'];
		$link = "</br></br> લિંક: <a href='{$linkk}' target='_blank' >" . $linkk . "</a>";
		$file = '';
		if (!empty($row['document'])) {
			$img = $row['document'];
			$file = "<a href='{$img}' target='_blank' > <img id='i1' style='height:50px' src='scripts/img/file.png'></a> ";
		}
		$edit = "";
		$delete = "";
		if (menuRights($_REQUEST['uid'], 'sahai', 'edit')) {
			$edit = "<a href='javascript:;' role='button' data-toggle='modal' data-target='#modal-edit' class='btn btn-sm btn-green edit' i='{$row['id']}' data-i=\"" . htmlspecialchars(json_encode($row)) . "\"> Edit </a> ";
		}
		if (menuRights($_REQUEST['uid'], 'sahai', 'delete')) {
			$delete = "<a href='javascript:;' role='button' data-toggle='modal' data-target='#modal-del' class='btn btn-sm btn-danger delete ' i='{$row['id']}' data-i=\"" . htmlspecialchars(json_encode($row)) . "\"> Delete </a>";
		}
		$records["aaData"][] = array(
			$row['id'],
			"{$idx}",
			$row['title'],
			$row['notes'],
			$row['office'],
			"{$file} {$link}",
			"{$edit} {$delete}",
		);
	}
}

$records["sEcho"] = $sEcho;
$records["iTotalRecords"] = $iTotalRecords;
$records["iTotalDisplayRecords"] = $iTotalRecords;
//$records ["total"] = $total;
echo json_encode($records);
