<?php
include_once '../../db.php';
header('Content-Type: application/json');
db_connect();

if (isset($_POST['action']) && $_POST['action'] == "add") {
	if ($_POST['title'] == '') {
		echo json_encode(array("error" => "Please enter name"));
		exit;
	}
	$data = array(
		"title" => $_POST['title'],
		"notes" => $_POST['notes'],
		"link" => $_POST['link'],
		"type" => $_POST['type'],
		"date" => phpNow()
	);
	if (!empty($_POST['document'])) {
		$data['document'] = $_POST['document'];
	}
	if (!empty($_POST['office'])) {
		$data['office'] = $_POST['office'];
	}
	
	$id = insertRow("gov_document", $data);
	echo json_encode(array("success" => "Add successfully!"));
} else {
	echo json_encode(array("error" => "Add not successfully!"));
}
