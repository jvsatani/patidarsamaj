<?php
include_once '../../db.php';
header('Content-Type: application/json');
db_connect();
if(isset($_POST['action']) && $_POST['action']=="edit"){
	$data = array(
		"title" => $_POST['title'],
		"notes" => $_POST['notes'],
		"link" => $_POST['link'],
		"type" => $_POST['type'],
		"date" => phpNow()
	);
	if (!empty($_POST['document'])) {
		$data['document'] = $_POST['document'];
	}
	if (!empty($_POST['office'])) {
		$data['office'] = $_POST['office'];
	}
	
	$id = updateRow("gov_document",$data,array("id"=>$_POST['id']));
	echo json_encode(array("success"=>"Edit successfully!"));
}
else{
	echo json_encode(array("error"=>"Edit not successfully!"));
}
