<?php
include_once '../../db.php';
header('Content-Type: application/json');
db_connect();
if(isset($_POST['action']) && $_POST['action']=="edit"){
	$data = array(
		"name" => $_POST['name'],
		"mobile" => $_POST['mobile'],
		"address" => $_POST['address'],
		"city_id" => $_POST['city_id'],
		"dist_id" => $_POST['dist_id'],
		"village" => $_POST['village'],
		"title" => $_POST['title'],
		"notes" => $_POST['notes'],
		"dob" => $_POST['dob'],
		"date" => phpNow()
	);
	if (!empty($_POST['image_1'])) {
		$data['image_1'] = $_POST['image_1'];
	}
	if (!empty($_POST['image_2'])) {
		$data['image_2'] = $_POST['image_2'];
	}
	if (!empty($_POST['image_3'])) {
		$data['image_3'] = $_POST['image_3'];
	}
	if (!empty($_POST['image_4'])) {
		$data['image_4'] = $_POST['image_4'];
	}
	$id = updateRow("vyakti_vishesh",$data,array("id"=>$_POST['id']));
	echo json_encode(array("success"=>"Edit successfully!"));
}
else{
	echo json_encode(array("error"=>"Edit not successfully!"));
}

?>