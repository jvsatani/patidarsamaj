<?php
include_once("../../db.php");
include_once("../../functions.php");
db_connect();

$where = "";
$type = "";
if (!empty($_REQUEST['search'])) {
	$where .= " and (name like :search) ";
}

$columns = explode(",", $_POST['sColumns']);
$sortCol = $_POST['iSortCol_0'];
$sortOrder = $_POST['sSortDir_0'];

$records = array();

$query = "select count(*) total
		FROM vyakti_vishesh 
		where status='1' {$where} ";

$query = $con->prepare($query);
if (!empty($_REQUEST['search'])) {
	$query->bindValue(":search", "%{$_REQUEST['search']}%");
}
$query->bindValue(":type", "{$type}");

$query->execute();
$row = $query->fetch(PDO::FETCH_ASSOC);

$iTotalRecords = $row['total'];
$iDisplayLength = intval($_REQUEST['iDisplayLength']);
$iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
$iDisplayStart = intval($_REQUEST['iDisplayStart']);
$sEcho = intval($_REQUEST['sEcho']);

$records["aaData"] = array();
$query = "SELECT t.*,d.name dist_name,tm.name city_name
		FROM  vyakti_vishesh t
		LEFT JOIN district_master d ON d.id=t.dist_id
		LEFT JOIN taluko_master tm ON tm.id=t.city_id
		WHERE t.status = '1' {$where}
		";

$query .= " order by {$columns[$sortCol]} {$sortOrder}";
$query .= " limit $iDisplayStart, $iDisplayLength";

$query = $con->prepare($query);
if (!empty($_REQUEST['search'])) {
	$query->bindValue(":search", "%{$_REQUEST['search']}%");
}

$query->execute();

if ($query->rowCount() > 0) {

	$idx = 0;
	while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
		$idx++;
		$id = $row['id'];
		$row['image_1'] = getFullImage($row['image_1']);;
		$row['image_2'] = getFullImage($row['image_2']);;
		$row['image_3'] = getFullImage($row['image_3']);;
		$row['image_4'] = getFullImage($row['image_4']);;

		$edit = "<a href='javascript:;' role='button' data-toggle='modal' data-target='#modal-edit' class='btn btn-sm btn-green edit' i='{$row['id']}' data-i=\"" . htmlspecialchars(json_encode($row)) . "\"> Edit </a> ";
		$delete = "<a href='javascript:;' role='button' data-toggle='modal' data-target='#modal-del' class='btn btn-sm btn-danger delete ' i='{$row['id']}' data-i=\"" . htmlspecialchars(json_encode($row)) . "\"> Delete </a>";
			$name =$row['name'] . '</br> <span> મોબાઈલ નંબર: ' . $row['mobile'] . '</span>';
		$address = $row['address'] . '</br> <span> ગામ: ' . $row['village'] . '</br> <span> તાલુકો: ' . $row['city_name'] . '</span>' . '</br> જિલ્લો: ' . $row['dist_name'];
		$records["aaData"][] = array(
			$row['id'],
			"{$idx}",
			$name,
			"{$address}",
			$row['title'],
			"{$edit} {$delete}",
		);
	}
}

$records["sEcho"] = $sEcho;
$records["iTotalRecords"] = $iTotalRecords;
$records["iTotalDisplayRecords"] = $iTotalRecords;
//$records ["total"] = $total;
echo json_encode($records);
