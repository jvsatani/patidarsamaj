<?php
include_once '../../db.php';
include_once '../../functions.php';
header('Content-Type: application/json');
db_connect();

if(isset($_POST['action']) && $_POST['action']=="add"){

	setSettingByKey("bank_name",  $_POST['bank_name']);
	setSettingByKey("account_name",  $_POST['account_name']);
	setSettingByKey("account_no",  $_POST['account_no']);
	setSettingByKey("ifsc_code",  $_POST['ifsc_code']);
	setSettingByKey("branch_name",  $_POST['branch_name']);
	setSettingByKey("phonepay_mobile",  $_POST['phonepay_mobile']);
	setSettingByKey("phonepay_name",  $_POST['phonepay_name']);
	setSettingByKey("gpay_mobile",  $_POST['gpay_mobile']);
	setSettingByKey("gpay_name",  $_POST['gpay_name']);
	setSettingByKey("adv_mobile",  $_POST['adv_mobile']);
	setSettingByKey("app_version",  $_POST['app_version']);

	echo json_encode(array("success"=>"Add successfully!"));
}
else{
	echo json_encode(array("error"=>"Add not successfully!"));
}
