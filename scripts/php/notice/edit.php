<?php
include_once '../../db.php';
header('Content-Type: application/json');
db_connect();
if(isset($_POST['action']) && $_POST['action']=="edit"){
	$data = array(
		"title" => $_POST['title'],
		"notes" => $_POST['notes'],
		"sender_id" => $_POST['sender_id'],
		"sender_type" => $_POST['sender_type'],
		"uid" => $_POST['uid'],
		"date" => phpNow()
	);
	if (!empty($_POST['image'])) {
		$data['image'] = $_POST['image'];
	}
	$id = updateRow("notice",$data,array("id"=>$_POST['id']));
	echo json_encode(array("success"=>"Edit successfully!"));
}
else{
	echo json_encode(array("error"=>"Edit not successfully!"));
}
