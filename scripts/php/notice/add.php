<?php
include_once '../../db.php';
header('Content-Type: application/json');
db_connect();

if (isset($_POST['action']) && $_POST['action'] == "add") {
	if ($_POST['title'] == '') {
		echo json_encode(array("error" => "Please enter name"));
		exit;
	}
	$data = array(
		"title" => $_POST['title'],
		"notes" => $_POST['notes'],
		"image" => $_POST['image'],
		"sender_id" => $_POST['sender_id'],
		"sender_type" => $_POST['sender_type'],
		"uid" => $_POST['uid'],
		"date" => phpNow()
	);
	$id = insertRow("notice", $data);
	echo json_encode(array("success" => "Add successfully!"));
} else {
	echo json_encode(array("error" => "Add not successfully!"));
}
