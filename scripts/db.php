<?php 

$MODE="local";
if($_SERVER['HTTP_HOST']=="localhost" || $_SERVER['HTTP_HOST']=="dev-services.ln.local" || $_SERVER['HTTP_HOST']=="192.168.0.100"){
	$MODE="local";
	if($_SERVER['HTTP_HOST']=="dev-services.ln.local"){
		date_default_timezone_set("Asia/Manila");
	}else{
		date_default_timezone_set("Asia/Kolkata");
	}
	error_reporting(E_ALL);
	ini_set('display_errors', 1);
}else{

	$MODE="server";
	date_default_timezone_set('America/New_York');
	//error_reporting(0);
	//ini_set('display_errors', 0);
	error_reporting(E_ALL & ~E_NOTICE);
	ini_set('display_errors', 1);
		
}

if($MODE=="local" && $_SERVER['HTTP_HOST']!="dev-services.ln.local"){
	$proj_dir="patidarsamaj/";	
}else{
	$proj_dir="admin/";
}

$siteURL=siteURL()."$proj_dir";
$adminURL=siteURL()."$proj_dir";


define("APP_NAME","સમસ્ત હિરપરા પરીવાર | Admin");
define("LOCK_TIMEOUT","1800"); //seconds
define("LOGIN_TIMEOUT","5600"); //seconds

//
$_SERVER['REMOTE_ADDR'] = !empty($_SERVER['HTTP_X_FORWARDED_FOR'])?$_SERVER['HTTP_X_FORWARDED_FOR']:$_SERVER['REMOTE_ADDR'];
$_SERVER['REMOTE_ADDR'] = explode(',', $_SERVER['REMOTE_ADDR']);
$_SERVER['REMOTE_ADDR'] = trim($_SERVER['REMOTE_ADDR'][0]);


$con;
function db_connect(){
	global $con,$MODE;
	if($MODE=="local"){
		$dbhost = "localhost";
		$dbuser = "root";
		$dbpassword = "";
		$database = "patidarsamaj_db";
		if($_SERVER['HTTP_HOST']=="dev-services.ln.local"){
			$dbpassword = "";
		}
		error_reporting(E_ALL);
		ini_set('display_errors', 1);	
	}else{
		// $dbhost = "localhost";
		// $dbuser = "speni6bg_spen_master";
		// $dbpassword = "k&DHiyG_I+O(";
		// $database = "speni6bg_patidarsamaj_db";
		// if($_SERVER['HTTP_HOST']=="dev-services.ln.local"){
		// 	$dbpassword = "";
		// }
		$dbhost = "localhost";
		$dbuser = "root";
		$dbpassword = "";
		$database = "patidarsamaj_db";
		if($_SERVER['HTTP_HOST']=="dev-services.ln.local"){
			$dbpassword = "";
		}
		error_reporting(E_ALL);
		ini_set('display_errors', 1);	
	}
	$con = new PDO ( "mysql:host=$dbhost;dbname=$database", "$dbuser", "$dbpassword", array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8")) or die ( 'error' );
	$con->setAttribute ( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
	
}

function db_close(){
	global $con;
	$con=null;
}

$upload_path="scripts/docs/";
if(empty($_SESSION)){
	session_start();
}
function siteURL()
{
	$protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
	$domainName = $_SERVER['HTTP_HOST'].'/';
	return $protocol.$domainName;
}

function phpNow(){
	return date("Y-m-d H:i:s");
}
function phpNowCutOff(){
	return date("Y-m-d ".cut_off_time());
}
function cut_off_time(){
	return '20:00:00'; //8PM cut-off time
}

function getIPDetails($ip){
	$resp=@file_get_contents("http://ipinfo.io/$ip/json");
	if(empty($resp)){
		return array();
	}
	$resp=json_decode($resp);
	return $resp;
}

$last_isMobile = null;
function isMobile(){
	global $last_isMobile;
	if(!is_null($last_isMobile)){
		return $last_isMobile;
	}
	$useragent = $_SERVER['HTTP_USER_AGENT'];
	$last_isMobile = preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4));
	return $last_isMobile?'y':'n';
}

function logSession($page){
	global $con;
	//update session access time
	$query=$con->prepare("update sessions set last=:now where session_id=:session_id");
	$query->bindValue(":now",phpNow());
	$query->bindParam(":session_id",$page);
	$query->execute();
	
	$query=$con->prepare("INSERT INTO `sessions_log`(`id`, `access_id`, `pg`, `stamp`) 
	VALUES (NULL,:access_id, :pg, :stamp)");
	$query->bindParam(":access_id",$_SESSION['id']);
	$query->bindParam(":pg",$page);
	$query->bindValue(":stamp",phpNow());
	$query->execute();
}


function getRows($sql,$params=false){
	global $con;
	$query=$con->prepare($sql);
	if($params){
		foreach($params as $k=>$v){
			$query->bindValue(":$k",$v."");
		}
	}
	$query->execute();
	if($query->rowCount()==0){
		return array();
	}else{
		return $query->fetchAll(PDO::FETCH_ASSOC);
	}
}
function getRow($sql,$params=false){
	global $con;
	$query=$con->prepare($sql);
	if($params){
		foreach($params as $k=>$v){
			$query->bindValue(":$k",$v."");
		}
	}
	$query->execute();
	if($query->rowCount()==0){
		return false;
	}else{
		return $query->fetch(PDO::FETCH_ASSOC);
	}
}
function insertRow($table,$data){
	global $con;
	$columns=array_keys($data);
	$cols=implode(",",$columns);
	$params=":".implode(",:",$columns);
	$sql="insert into $table ($cols) values($params) ";
	$query=$con->prepare($sql);
	foreach($data as $k=>$v){
		$query->bindParam(":$k",$data[$k]);
	}
	try{
		$query->execute();
	}catch(Exception $e){
		trigger_error($e->getMessage(), E_USER_WARNING);
		return null;
	}
	return $con->lastInsertId();
}
function updateRow($table,$data,$where=false){
	global $con;
	$sql="update $table set ";
	$first=true;
	foreach($data as $k=>$v){
		if($first){
			$sql.=" $k=:$k ";
			$first=false;
		}else{
			$sql.=", $k=:$k ";
		}
	}
	if($where){
		$sql.=" where 1 ";
		foreach($where as $w=>$wv){
			$sql.=" and $w=:$w ";
		}
	}
	$query=$con->prepare($sql);
	foreach($data as $k=>$v){
		$query->bindParam(":$k",$data[$k]);
	}
	if($where){
		foreach($where as $k=>$v){
			$query->bindParam(":$k",$where[$k]);
		}
	}
	$query->execute();
	return $query->rowCount();
}

function extractNumbers($numbers,$minmax1){
	$arr=explode("-",$numbers);
	$temp_arr=explode("|",$minmax1);
	
	$part1=array();
	$part2=array();
	for($i=0;$i<$temp_arr[1];$i++){
		$part1[]=$arr[$i];
	}
	for($j=$i;$j<count($arr);$j++){
		$part2[]=$arr[$j];
	}
	return array($part1,$part2);
}

function new_ch($url, $cookie_file=null, $headers=array()){
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	@curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
	curl_setopt($ch, CURLOPT_ENCODING, '');
	curl_setopt($ch, CURLOPT_HEADER, 0);

	if(!empty($cookie_file)){
		curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_file);
		curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_file);
	}

	curl_setopt($ch, CURLOPT_HTTPHEADER, array_merge(array(
		'Accept:text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
		'User-Agent:Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.146 Safari/537.36',
	),$headers));
	
	return $ch;
}

class GetMyHelpers{
	
	static function displayDate($date, $format=null, $now=null, $show_time=false){
		if(empty($now)){
			$now = strtotime(date('Y-m-d',time()));
		}
		
		$dt = strtotime($date, $now);
		$dt = strtotime(date('Y-m-d', $dt), $now);
		
		$time = '';
		if($show_time){
			$time = ' '.date('h:i A', strtotime($date, $now));
		}
		
		if($dt == $now){
			$output = 'Today'.$time;
		}else if($dt == $now+86400){
			$output = 'Tomorrow'.$time;
		}else if(date('Y', $dt) != date('Y', $now)){
			$output = strtoupper(date('M j Y', $dt)).$time;
		}else if($dt<$now){
			$output = strtoupper(date('M j', $dt)).$time;
		}else if($dt >= $now+(86400*7)){
			$output = date('M j', $dt).$time;
		}else{
			$output = date('l', $dt).$time;
		}
		return '<span class="text-nowrap">'.$output.'</span>';
	}
	
	static function displayDateTime($date){
		if(empty($now)){
			$now = strtotime(date('Y-m-d',time()));
		}
		
		$dt = strtotime($date, $now);
		$dt = strtotime(date('Y-m-d', $dt), $now);
		
		$time = date(', h:i A', strtotime($date));
		
		if($dt == $now){
			$output = 'Today'.$time;
		}else if($dt == $now+86400){
			$output = 'Tomorrow'.$time;
		}else if($dt == $now-86400){
			$output = 'Yesterday'.$time;
		}else if(date('Y', $dt) != date('Y', $now)){
			$output = strtoupper(date('M j Y', $dt)).$time;
		}else if($dt >= $now+(86400*7)){
			$output = strtoupper(date('D M j', $dt)).$time;
		}else if($dt >= $now-(86400*7)){
			$output = strtoupper(date('D M j', $dt)).$time;
		}else if($dt<$now){
			$output = strtoupper(date('M j', $dt)).$time;
		}else{
			$output = date('l', $dt).$time;
		}
		
		return '<span class="text-nowrap">'.$output.'</span>';
	}
}


function logEvent($type,$title,$body,$uid=0,$ip,$user_agent){
	global $con;
	
	$query=$con->prepare("INSERT INTO `eventlog`(`id`, `event_datetime`, `uid`, `event_type`, `event_title`, `log_data`,ip,user_agent) 
	VALUES (NULL,:now,:uid, :event_type, :event_title, :log_data,:ip,:user_agent)");
	$query->bindValue(":now", phpNow());
	$query->bindParam(":uid",$uid);
	$query->bindParam(":event_type",$type);
	$query->bindParam(":event_title",$title);
	$query->bindParam(":log_data",$body);
	$query->bindParam(":uid",$uid);
	$query->bindParam(":ip",$ip);
	$query->bindParam(":user_agent",$user_agent);
	$query->execute();
}
