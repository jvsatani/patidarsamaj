<?php
include_once("../scripts/db.php");
include_once("../scripts/functions.php");
db_connect();
if (!empty($_SESSION['access']) && $_SESSION['access'] != "") {
    $id = $_SESSION['access']['uid'];
    $admin = getRow("SELECT u.*,CONCAT(u.`middle_name`,' ',u.`father_name`,' ', IFNULL(s.`name`,'')) full_name
 FROM user u
 LEFT JOIN surname s ON s.id=u.surname_id
 WHERE u.status = 2 and u.uid=:uid", array('uid' => $id));
}
// echo $admin['uid'];
?>
<div id="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="#index.php">Home</a></li>
        <li class="active">Profile </li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Profile </h1>
    <!-- end page-header -->
    <!-- begin profile-container -->
    <div class="profile-container">
        <!-- begin profile-section -->
        <div class="profile-section">

            <div class="col-md-7">
                <!-- begin profile-info -->

                <h4><?php echo $admin['full_name']; ?> <br><small>Admin</small></h4>


                <form method="post" class="form-horizontal" id="frmSave">
                    <input type="hidden" name="uid" value="<?php echo $id; ?>">
                    <!-- <input id="a1" type="hidden" name="profile_pic" value="<?php echo $admin['profile_pic']; ?>"> -->


                    <div class="form-group">
                        <label class="col-md-3 control-label">Password</label>
                        <div class="col-md-5">
                            <input type="text" class="form-control" name="password" placeholder="Password" value="<?php echo $admin['ad_password']; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-2">
                            <button type="submit" name="save" class="btn btn-info btn-sm" style="float: right;">Save</button>
                        </div>
                    </div>

                </form>
            </div>
            <!-- end profile-right -->
        </div>
        <!-- end profile-section -->

    </div>
    <!-- end profile-container -->
</div>
<script type="text/javascript">
    $(document).ready(function() {

        $("#frmSave").submit(function(e) {
            e.preventDefault();
            console.log("submit");
            $.post(siteURL + "scripts/php/user_save.php", $(this).serialize(), function(data) {
                notify(data);
                location.reload();
            });
        });

        $("#change_profile").click(function() {
            $("[name='file']").trigger('click');
        });


    });
</script>