<?php
include_once("../scripts/db.php");
db_connect();
$pending_member = getRow("SELECT COUNT(uid) as total_member
				  FROM user 
				  WHERE status = 1");
$accpet_member = getRow("SELECT COUNT(uid) as total_member
				  FROM user 
				  WHERE status = 2");
$hundi_amount = getRow("SELECT SUM(amount) as amount
				  FROM user_transcription 
				  WHERE status = 1 and type='hundi'");
$main_amount = getRow("SELECT SUM(amount) as amount
				  FROM user_transcription 
				  WHERE status = 1 and type='main'");
?>
<!-- begin breadcrumb -->
<ol class="breadcrumb pull-right">
	<li><a href="javascript:;">Home</a></li>
	<li class="active">Dashboard</li>
</ol>
<!-- end breadcrumb -->
<!-- begin page-header -->
<h1 class="page-header">Dashboard <small></small></h1>
<!-- end page-header -->
<!-- begin row -->
<div class="row">
	<!-- begin col-3 -->
	<div class="col-md-3 col-sm-6">
		<div class="widget widget-stats bg-green">
			<div class="stats-icon stats-icon-lg"><i class="fa fa-users"></i></div>
			<div class="stats-title">નવા સભ્યો</div>
			<div class="stats-number top_accounts"><?php echo $pending_member['total_member'] ?></div>
			<div class="stats-progress progress">
				<div class="progress-bar" style="width: 100%;"></div>
			</div>
			<div class="stats-desc"><span class="top_accounts_today"></span> <span class="pull-right top_accounts_30"><?php echo $pending_member['total_member'] ?></span></div>
		</div>
	</div>
	<!-- end col-3 -->
	<!-- begin col-3 -->
	<div class="col-md-3 col-sm-6">
		<div class="widget widget-stats bg-blue">
			<div class="stats-icon stats-icon-lg"><i class="fa fa-users"></i></div>
			<div class="stats-title">ટોટલ સભ્યો</div>
			<div class="stats-number top_revenue"><?php echo $accpet_member['total_member'] ?></div>
			<div class="stats-progress progress">
				<div class="progress-bar" style="width: 100%;"></div>
			</div>
			<div class="stats-desc"><span class="top_revenue_today"> </span> <span class="pull-right top_revenue_30"><?php echo $accpet_member['total_member'] ?></span></div>
		</div>
	</div>
	<!-- end col-3 -->
	<!-- begin col-3 -->
	<div class="col-md-3 col-sm-6">
		<div class="widget widget-stats bg-purple">
			<div class="stats-icon stats-icon-lg"><i class="fa fa-inr fa-fw"></i></div>
			<div class="stats-title">હુંડી</div>
			<div class="stats-number top_tickets"><?php echo $hundi_amount['amount'] ?>.00</div>
			<div class="stats-progress progress">
				<div class="progress-bar" style="width: 100%;"></div>
			</div>
			<div class="stats-desc"><span class="top_tickets_today"></span> <span class="pull-right top_tickets_30"><?php echo $hundi_amount['amount'] ?></span></div>
		</div>
	</div>
	<!-- end col-3 -->
	<!-- begin col-3 -->
	<div class="col-md-3 col-sm-6">
		<div class="widget widget-stats bg-black">
			<div class="stats-icon stats-icon-lg"><i class="fa fa-inr fa-fw"></i></div>
			<div class="stats-title">ફાળો</div>
			<div class="stats-number top_winnings"> <?php echo $main_amount['amount'] ?>.00</div>
			<div class="stats-progress progress">
				<div class="progress-bar" style="width: 100%;"></div>
			</div>
			<div class="stats-desc"><span class="top_winnings_today"></span> <span class="pull-right top_winnings_30"><?php echo $main_amount['amount'] ?></span></div>
		</div>
	</div>
	<!-- end col-3 -->
</div>
<!-- end row -->
</div>
<!-- end #content -->

<!-- begin scroll to top btn -->
<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
<!-- end scroll to top btn -->
</div>
<!-- end page container -->

<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<script src="scripts/plugins/morris/raphael.min.js"></script>
<script src="scripts/plugins/morris/morris.js"></script>
<script src="scripts/plugins/jquery-jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="scripts/plugins/jquery-jvectormap/jquery-jvectormap-world-merc-en.js"></script>
<script src="scripts/plugins/bootstrap-calendar/js/bootstrap_calendar.min.js"></script>
<script src="scripts/js/dashboard-v2.min.js"></script>

<!-- ================== END PAGE LEVEL JS ================== -->
<script type="text/javascript">
	$(document).ready(function() {
		$(".nav").find('li').removeClass("active");
		$("#page_index").addClass("active");
		$(".has-sub").removeClass("expand");
		$(".sub-menu").attr("style", "display:none;");
	});
</script>