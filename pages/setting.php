<?php
include_once("../scripts/db.php");
include_once("../scripts/functions.php");
db_connect();
if (!empty($_SESSION['access']) && $_SESSION['access'] != "") {
	$id = $_SESSION['access']['uid'];
	if (!menuRights($id, 'setting', 'view')) {
		echo "<script type='text/javascript'> document.location = 'index.php'; </script>";
	}
}
$bank_name = getSetting("bank_name");
$account_name = getSetting("account_name");
$account_no = getSetting("account_no");
$ifsc_code = getSetting("ifsc_code");
$branch_name = getSetting("branch_name");
$phonepay_mobile = getSetting("phonepay_mobile");
$phonepay_name = getSetting("phonepay_name");
$gpay_mobile = getSetting("gpay_mobile");
$gpay_name = getSetting("gpay_name");
$adv_mobile = getSetting("adv_mobile");
$app_version = getSetting("app_version");
?>
<div id="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="#index.php">Home</a></li>
        <li class="active">Setting </li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Setting </h1>
    <!-- end page-header -->
    <!-- begin profile-container -->
    <div class="profile-container">
        <!-- begin profile-section -->
        <div class="profile-section">

            <div class="col-md-7">
                <!-- begin profile-info -->
                <form method="post" class="form-horizontal" id="frmSave">
                    <!-- <div class="form-group">
                        <label class="col-md-3 control-label">Extra Charge</label>
                        <div class="col-md-9">
                            <input type="number" class="form-control" placeholder="Extra Charge" name='extra_charge' value="<?php echo $extra_charge; ?>">
                        </div>
                    </div> -->
                    <input type="hidden" class="form-control" placeholder="" name='action' value="add">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Whats app no</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" placeholder="Whats app no" name='adv_mobile' value="<?php echo $adv_mobile; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Bank Name </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" placeholder="Bank Name" name='bank_name' value="<?php echo $bank_name; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Account Name </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" placeholder="Account Name" name='account_name' value="<?php echo $account_name; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Account No. </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" placeholder="Account No" name='account_no' value="<?php echo $account_no; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">IFSC Code</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" placeholder="IFSC" name='ifsc_code' value="<?php echo $ifsc_code; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Branch </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" placeholder="Branch" name='branch_name' value="<?php echo $branch_name; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Phone Pay Mobile no </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" placeholder="Phone Pay Mobile no " name='phonepay_mobile' value="<?php echo $phonepay_mobile; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Phone pay Name </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" placeholder="Phone pay Name" name='phonepay_name' value="<?php echo $phonepay_name; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Google pay Mobile no </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" placeholder="Google pay Mobile no" name='gpay_mobile' value="<?php echo $gpay_mobile; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Google pay Name</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" placeholder="Google pay Name" name='gpay_name' value="<?php echo $gpay_name; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">App Version</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" placeholder="App Version" name='app_version' value="<?php echo $app_version; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-info btn-sm" style="float: right;" name="submit">Save</button>
                        </div>
                    </div>

                </form>
            </div>
            <!-- end profile-right -->
        </div>
        <!-- end profile-section -->

    </div>
    <!-- end profile-container -->
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $("#page_user").addClass("active");
        $("#frmSave").submit(function(e) {
            e.preventDefault();
            $('#frmSave .btn-sm').attr("disabled", true);
            $.post(siteURL + "scripts/php/setting/setting_add.php", $(this).serialize(), function(data) {
                notify(data);
                $('#frmSave .btn-sm').attr("disabled", false);
                if (data.success) {

                } else {

                }
            });
        });
    });
</script>