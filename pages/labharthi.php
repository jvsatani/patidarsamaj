<?php
include_once("../scripts/db.php");
include_once("../scripts/functions.php");
db_connect();
if (!empty($_SESSION['access']) && $_SESSION['access'] != "") {
	$id = $_SESSION['access']['uid'];
	if (!menuRights($id, 'labharthi', 'view')) {
		echo "<script type='text/javascript'> document.location = 'index.php'; </script>";
	}
}
$pageName = 'લાભાર્થી';

$standard_master = getRows("SELECT *
					FROM std_master 
					WHERE status = '1'");

?>
<div id="content">
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li>
			<a href="javascript:;">Home</a>
		</li>
		<li class="active"><?php echo $pageName ?></li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header"><?php echo $pageName ?></h1>
	<!-- end page-header -->
	<!-- begin row -->
	<div class="row">
		<!-- begin col-12 -->
		<div class="col-md-12">
			<!-- begin panel -->
			<div class="panel panel-primary" data-sortable-id="table-basic-7">
				<div class="panel-heading">
					<div class="panel-heading-btn">
						<?php if (menuRights($id, 'labharthi', 'add')) { ?>
							<a href='javascript:;' role='button' data-toggle='modal' data-target='#modal-add' class="btn btn-xs btn-success add">Add</a>
						<?php } ?>
					</div>

					<h4 class="panel-title"><?php echo $pageName ?></h4>
				</div>
				<div class="panel-body">
					<div class="filter">
						<div class="row">
							<div class="form-group col-md-12">
								<label class="col-md-1 control-label" style="margin:6px">Search : </label>
								<div class="col-md-7">
									<input type="text" name="search" class="form-filter form-control" placeholder="Search Any..." />
								</div>
							</div>
							<button class="hidden btn btn-xs btn-default filter-submit margin-bottom"><i class="fa fa-search"></i> Search</button>
						</div>
					</div>
					<div class="table-container">
						<table class="table table-striped table-bordered table-hover ajax-table rowclick">
							<thead>
								<tr role="row" class="heading">
									<th class="no-sort" width="5%"><input type="checkbox" class="toggle_all" /></th>
									<th>#</th>
									<th>Name</th>
									<th>Yojana name</th>
									<th>Amount</th>
									<th>Join Date</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<!-- end panel -->
		</div>
		<!-- end col-12 -->
	</div>
	<!-- end row -->
</div>

<!--- Add Modal -->
<div class="modal fade" id="modal-add">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title">Add</h4>
			</div>
			<form method="post" class="form-horizontal" id="frmAdd">
				<input type="hidden" name="action" value="add">
				<input type="hidden" name="agent_id" value="<?php echo $id; ?>">
				<div class="modal-body">

					<div class="form-group">
						<label class="col-md-3  col-sm-12  control-label">Name </label>
						<div class="col-md-9  col-sm-12 ">
							<input type="text" name="name" class="form-control" placeholder="full name">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3  col-sm-12  control-label">Mobile </label>
						<div class="col-md-9  col-sm-12 ">
							<input type="text" name="mobile" class="form-control" placeholder="Mobile">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3  col-sm-12  control-label">Village </label>
						<div class="col-md-9  col-sm-12 ">
							<input type="text" name="village" class="form-control" placeholder="Village">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3  col-sm-12  control-label">Taluko </label>
						<div class="col-md-9  col-sm-12 ">
							<input type="text" name="taluko" class="form-control" placeholder="Taluko">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3  col-sm-12  control-label">District </label>
						<div class="col-md-9  col-sm-12 ">
							<input type="text" name="dist" class="form-control" placeholder="District">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3  col-sm-12  control-label">Address</label>
						<div class="col-md-9  col-sm-12 ">
							<textarea name="address" class="form-control" placeholder="Address"></textarea>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3  col-sm-12  control-label">Yojana name </label>
						<div class="col-md-9  col-sm-12 ">
							<input type="text" name="yojna_name" class="form-control" placeholder="Yojana name">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3  col-sm-12  control-label">Amount </label>
						<div class="col-md-9  col-sm-12 ">
							<input type="text" name="amount" class="form-control" placeholder="Amount">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3  col-sm-12  control-label">Join Date</label>
						<div class="col-md-9  col-sm-12 ">
							<input type="text" name="join_date" id="date" class="form-control datepicker-autoClose" placeholder="Join Date">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3  col-sm-12  control-label">Approved amount </label>
						<div class="col-md-9  col-sm-12 ">
							<input type="text" name="approv_amt" class="form-control" placeholder="Approved amount">
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<a href="javascript:;" class="btn btn-sm btn-white" data-dismiss="modal">Close</a>
					<input type="submit" class="btn btn-sm btn-success" value="Save">
				</div>
			</form>
		</div>
	</div>
</div>

<!--- Edit Modal -->
<div class="modal fade" id="modal-edit">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title">Edit</h4>
			</div>
			<form method="post" class="form-horizontal" id="frmEdit">
				<input type="hidden" name="action" value="edit">
				<input type="hidden" name="id" value="0">
				<div class="modal-body">
					<div class="form-group">
						<label class="col-md-3  col-sm-12  control-label">Name </label>
						<div class="col-md-9  col-sm-12 ">
							<input type="text" name="name" class="form-control" placeholder="full name">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3  col-sm-12  control-label">Mobile </label>
						<div class="col-md-9  col-sm-12 ">
							<input type="text" name="mobile" class="form-control" placeholder="Mobile">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3  col-sm-12  control-label">Village </label>
						<div class="col-md-9  col-sm-12 ">
							<input type="text" name="village" class="form-control" placeholder="Village">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3  col-sm-12  control-label">Taluko </label>
						<div class="col-md-9  col-sm-12 ">
							<input type="text" name="taluko" class="form-control" placeholder="Taluko">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3  col-sm-12  control-label">District </label>
						<div class="col-md-9  col-sm-12 ">
							<input type="text" name="dist" class="form-control" placeholder="District">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3  col-sm-12  control-label">Address</label>
						<div class="col-md-9  col-sm-12 ">
							<textarea name="address" class="form-control" placeholder="Address"></textarea>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3  col-sm-12  control-label">Yojana name </label>
						<div class="col-md-9  col-sm-12 ">
							<input type="text" name="yojna_name" class="form-control" placeholder="Yojana name">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3  col-sm-12  control-label">Amount </label>
						<div class="col-md-9  col-sm-12 ">
							<input type="text" name="amount" class="form-control" placeholder="Amount">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3  col-sm-12  control-label">Join Date</label>
						<div class="col-md-9  col-sm-12 ">
							<input type="text" name="join_date" id="date" class="form-control datepicker-autoClose" placeholder="Join Date">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3  col-sm-12  control-label">Approved amount </label>
						<div class="col-md-9  col-sm-12 ">
							<input type="text" name="approv_amt" class="form-control" placeholder="Approved amount">
						</div>
					</div>

				</div>
				<div class="modal-footer">
					<a href="javascript:;" class="btn btn-sm btn-white" data-dismiss="modal">Close</a>
					<input type="submit" class="btn btn-sm btn-success" value="Save">
				</div>
			</form>
		</div>
	</div>
</div>

<!--- Delete Modal -->
<div class="modal fade" id="modal-del">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title">Delete Confirmation</h4>
			</div>
			<form method="post" id="frmDel">
				<div class="modal-body">
					<div class="alert alert-danger m-b-0">
						<input type="hidden" name="id" value="0">
						<h5><i class="fa fa-info-circle"></i>Do you want to delete record ?</h5>

					</div>
				</div>
				<div class="modal-footer">

					<input type='submit' class="btn btn-sm btn-danger" value='Delete' name='delete'>
					<a href="javascript:;" class="btn btn-sm btn-white" data-dismiss="modal">Close</a>
				</div>
			</form>
		</div>
	</div>
</div>

<script type="text/javascript">
	var grid;
	$(document).ready(function() {
		grid = new Datatable();
		$(".nav").find('li').removeClass("active");
		$("#page_labharthi").addClass("active");

		$(".has-sub").removeClass("expand");
		// $(".sub-menu").attr("style", "display:none;");
		$('.datepicker-autoClose').datepicker({
			// format: 'DD/MM/YYYY',
			todayHighlight: true,
			autoclose: true
		});


		grid.init({
			src: $(".ajax-table"),
			onSuccess: function(grid) {
				// execute some code after table records loaded
				$(".filter").appendTo(".table-toolbar");
				$("[name=search]:eq(0)").focus();
			},
			onError: function(grid) {
				// execute some code on network or other general error  
			},
			dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options 
				/* 
					By default the ajax datatable's layout is horizontally scrollable and this can cause an issue of dropdown menu is used in the table rows which.
					Use below "sDom" value for the datatable layout if you want to have a dropdown menu for each row in the datatable. But this disables the horizontal scroll. 
				*/
				//"sDom" : "<'row'<'col-md-8 col-sm-12'pl><'col-md-4 col-sm-12 text-right'i<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pl><'col-md-4 col-sm-12 text-right'i>r>>", 
				"sDom": "<'table-toolbar'>t<'row'<'col-md-8 col-sm-12'pl><'col-md-4 col-sm-12 text-right'i>r>>",
				"aLengthMenu": [
					[1000, 2000, 3000, 4000, 5000],
					[1000, 2000, 3000, 4000, 5000] // change per page values here
				],
				"oLanguage": { // language settings
					"sProcessing": '<fa class="fa fa-spin fa-spinner"></fa> Loading...',
				},
				"iDisplayLength": 1000, // default record count per page
				"bServerSide": true, // server side processing
				"sAjaxSource": "scripts/php/labharthi_master/student_ajax.php?uid=<?php echo $id; ?>", // ajax source
				"aaSorting": [
					[1, "desc"]
				], // set first column as a default sort by asc
				"aoColumns": [{
						"sName": "select",
						"bVisible": false
					},
					{
						"sName": "id",
						"bSortable": true,
						"sWidth": "2%"
					},

					{
						"sName": "name",
						"sWidth": "10%"
					},
					{
						"sName": "mobile",
						"sWidth": "10%"
					},
					{
						"sName": "address",
						"sWidth": "10%"
					},
					{
						"sName": "date",
						"sWidth": "10%"
					},
					{
						"sName": "status",
						"sWidth": "5%"
					},

				],
				"fnFooterCallback": function(nRow, aaData, iStart, iEnd, aiDisplay) {
					//var nCells = nRow.getElementsByTagName('th');
					//nCells[1].innerHTML=ajaxTotal.total_qty;
				},
				"fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
					$(nRow).data("row", aData[0]);
					$(nRow).find(".delete").click(function(e) {
						e.stopPropagation();
						var id = aData[0];
						$("input[name='id']").val(id);
						$("#modal-del").modal("show");
					});
					$(nRow).click(function() {
						var r = $(this).data("row");
						//console.log(r);
						// redirect("#student_details.php?id=" + r);
					});


					$(nRow).find(".edit").click(function(e) {
						e.stopPropagation();
						var row = $(this).data('i');
						$('#frmEdit')[0].reset();
						var id = aData[0];
						var name = aData[2];
						$("#modal-edit input[name='id']").val(id);
						// $("#modal-edit input[name='surname']").val(row.surname);
						$("#modal-edit input[name='name']").val(row.name);
						$("#modal-edit input[name='yojna_name']").val(row.yojna_name);
						$("#modal-edit input[name='mobile']").val(row.mobile);
						$("#modal-edit input[name='village']").val(row.village);
						$("#modal-edit input[name='taluko']").val(row.taluko);
						$("#modal-edit input[name='dist']").val(row.dist);
						$("#modal-edit input[name='amount']").val(row.amount);
						$("#modal-edit input[name='approv_amt']").val(row.approv_amt);
						$("#modal-edit input[name='join_date']").val(row.join_date);
						$("#modal-edit textarea[name='address']").val(row.address);

						$("#modal-edit").modal("show");
					});


				}
			}
		});

		$('.add').click(function() {
			$("[name=name]").val("");
			$('#frmAdd')[0].reset();
		});

		$(".form-filter").change(function() {
			$(".filter-submit").trigger("click");
		});

		var search_timeout = null;
		$("[name=search]").keyup(function() {
			var self = this;
			if (search_timeout) {
				clearTimeout(search_timeout);
				search_timeout = null;
			}
			search_timeout = setTimeout(function() {
				$(".filter-submit").trigger("click");
			}, 500);
		});
		$("input[type=file]").change(function() {
			var formData = new FormData();
			formData.append('section', 'general');
			formData.append('action', 'previewImg');
			formData.append('file', $(this)[0].files[0]);
			var field = $(this).data('field');
			var img = $(this).data('img');
			//$('#loading').html('<img src="../images/loader.gif"> loading...');
			$.ajax({
				url: siteURL + "scripts/php/upload_photo.php",
				type: 'POST',
				dataType: 'json',
				processData: false,
				contentType: false,
				data: formData,
				success: function(data) {
					$('#loading').html(data);
					console.log(data);
					if (data.success) {
						$(field).val(data.name);
						$(img).prop("src", data.url);
					} else {
						console.log(data.error);
					}
				},
				error: function(r) {
					console.log("Error occured");
				},
			});
		});





		$(".school").change(function(e) {
			let id = $(this).data('i');
			var value = $(".select_school" + id + " option:selected").val();
			if (value == '1') {
				$(".open_school" + id).removeClass('hide')
			} else {
				$(".open_school" + id).addClass('hide')
			}
		});
		/* Add Category */
		$("#frmAdd").submit(function(e) {
			e.preventDefault();
			$('#frmAdd .btn-success').attr("disabled", true);
			$.post(siteURL + "scripts/php/labharthi_master/student_add.php", $(this).serialize(), function(data) {
				notify(data);
				$('#frmAdd .btn-success').attr("disabled", false);
				if (data.success) {
					$("#modal-add").modal("hide");
					$(".filter-submit").trigger("click");
					$('#frmAdd')[0].reset();
				} else {

				}
			});
		});

		/* Edit Category */
		$("#frmEdit").submit(function(e) {
			e.preventDefault();
			$.post(siteURL + "scripts/php/labharthi_master/student_edit.php", $(this).serialize(), function(data) {
				notify(data);
				if (data.success) {
					$("#modal-edit").modal("hide");
					$(".filter-submit").trigger("click");
					$('#frmEdit')[0].reset();
				} else {

				}
			});
		});

		/* Delete Category */
		$("#frmDel").submit(function(e) {
			e.preventDefault();
			$.post(siteURL + "scripts/php/labharthi_master/student_del.php", $(this).serialize(), function(data) {
				notify(data);
				if (data.success) {
					$("#modal-del").modal("hide");
					$(".filter-submit").trigger("click");
				} else {

				}
			});
		});
	});
</script>