<?php
include_once("../scripts/db.php");
include_once("../scripts/functions.php");
db_connect();
if (!empty($_SESSION['access']) && $_SESSION['access'] != "") {
	$id = $_SESSION['access']['uid'];
	if (!menuRights($id, 'loan','view')) {
		echo "<script type='text/javascript'> document.location = 'index.php'; </script>";
	}
}
$f_id = '';
if (!empty($_GET['f_id'])) {
	$f_id = $_GET['f_id'];
}
$l_id = '';
if (!empty($_GET['l_id'])) {
	$l_id = $_GET['l_id'];
}

$where = "";
if (!empty($_REQUEST['f_id'])) {
	$where .= " and bank_type='{$f_id}' ";
}
if (!empty($_REQUEST['l_id'])) {
	$where .= " and uid='{$l_id}' ";
}

// $DebitAmt = getRow("SELECT SUM(amount) amount
// 			FROM loan_transcription 
// 			WHERE status = '1' and type='dr' and uid=:id and bank_type=:f_id", array('id' => $l_id, 'f_id' => $f_id));

// $CreditAmt = getRow("SELECT SUM(amount) amount
// 			FROM loan_transcription 
// 			WHERE status = '1' and type='cr' and uid=:id and bank_type=:f_id", array('id' => $l_id, 'f_id' => $f_id));
// } else {
	$DebitAmt = getRow("SELECT SUM(amount) amount
			FROM loan_transcription 
			WHERE status = '1' and type='dr' {$where} ");

	$CreditAmt = getRow("SELECT SUM(amount) amount
			FROM loan_transcription 
			WHERE status = '1' and type='cr' {$where}  ");
// }



$users = getRows("SELECT l.uid,CONCAT(u.`middle_name`,' ',u.`father_name`,' ', IFNULL(s.`name`,'')) full_name
			FROM loan_transcription l
			LEFT JOIN user u ON u.uid=l.uid
			LEFT JOIN surname s ON s.id=u.surname_id
			WHERE l.status = '1'  GROUP BY l.uid ORDER BY u.`middle_name` ASC");


$PendingAmount = $DebitAmt['amount'] - $CreditAmt['amount'];

?>
<div id="content">
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li>
			<a href="javascript:;">Home</a>
		</li>
		<li class="active">લોન</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header">લોન </h1>
	<!-- end page-header -->
	<!-- begin row -->
	<div class="row">
		<!-- begin col-12 -->
		<div class="col-md-12">
			<!-- begin panel -->
			<div class="panel panel-inverse" data-sortable-id="table-basic-7">
				<div class="panel-heading">
					<h4 class="panel-title">લોન</h4>
				</div>
				<div class="panel-body">
					<br>
					<div class="row">
						<div class="form-group col-md-12 ">
							<label class="col-md-2 control-label">સભ્યો :</label>
							<div class="col-md-3">
								<select class="form-control selectUser" style="margin-top: -8px;" data-i='1' name="l_id">
									<option value=''>All સભ્યો</option>
									<?php foreach ($users as $row) {
										if (!empty($row['full_name'])) { ?>
											<option value="<?php echo $row['uid']; ?>" <?php if ($l_id == $row['uid']) {
																							echo "Selected";
																						} ?>><?php echo $row['full_name']; ?></option>
									<?php }
									} ?>
								</select>
							</div>
							<label class="col-md-1 control-label">Select Type:</label>
							<div class="col-md-2">
								<select class="form-control selectFund" style="margin-top: -8px;" data-i='1' name="f_id">
									<option value=''>All</option>
									<option value='cash' <?php if ($f_id == 'cash') {
																echo "Selected";
															} ?>>Cash</option>
									<option value='bank' <?php if ($f_id ==  'bank') {
																echo "Selected";
															} ?>>Bank</option>
								</select>
							</div>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<!-- <div class="btn-group pull-right">
				<form action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="post">
					<button type="submit" id="export_data" name='export_data' value="Export to excel" class="btn btn-info">Export to excel</button>
				</form>
			</div> -->
			<div class="panel panel-inverse" data-sortable-id="table-basic-7">
				<div class="panel-body">
					<div class="row">
						<div class="row">
							<div class="col-md-11">
								<h1 class="page-header" style="margin: 0px 10px 5px;">Summary</h1>
							</div>
							<div class="col-md-1">
								<!-- <a target="_blank" href="scripts/invoice/FundSummary_Print.php?f_id=<?php echo $f_id; ?>&r_id=<?php echo $r_id; ?>&sdate=<?php echo $sdate; ?>&edate=<?php echo $edate; ?>" class="btn btn-sm btn-green">Print</a> -->
							</div>
						</div>

						<div class="col-md-3  total_amt">
							<div class="total_amt_name">
								<span>Total Amount</span>
							</div>
							<div class="Totalamt_Box box"><span style="font-size: 15px;font-weight: bold;"><?php echo number_format($DebitAmt['amount'], 2); ?></span><br></div>
						</div>
						<div class="col-md-3  ">
							<div class="categoryName">
								<span>Credit Amount</span>
							</div>
							<div class="productBox  box"><span style="font-size: 15px;font-weight: bold;"><?php echo number_format($CreditAmt['amount'], 2); ?></span><br></div>
						</div>
						<div class="col-md-3 ">
							<div class="categoryName">
								<span>Pending Amount</span>
							</div>
							<div class="productBox  box"><span style="font-size: 15px;font-weight: bold;"><?php echo number_format($PendingAmount, 2); ?></span><br></div>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			<div class="panel panel-inverse" data-sortable-id="table-basic-8">
				<div class="panel-body">
					<div class="filter">
						<button class="hidden btn btn-xs btn-default filter-submit margin-bottom"><i class="fa fa-search"></i> Search</button>
					</div>
					<div class="table-container">
						<div class="row">
							<div class="col-md-11">
							</div>
							<div class="col-md-1">
								<a target="_blank" href="scripts/invoice/allLoan_Print.php?l_id=<?php echo $l_id; ?>" class="btn btn-sm btn-success">Print</a>
							</div>
						</div>
						<table class="table table-striped table-bordered table-hover ajax-table rowclick">
							<thead>
								<tr role="row" class="heading">
									<th class="no-sort" width="5%"><input type="checkbox" class="toggle_all" /></th>
									<th>#</th>
									<th>Name</th>
									<th>Note </th>
									<th>Loan Amount</th>
									<th>Credit Amount</th>
									<th>Date</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>

					</br>
					</br>
					</br>
				</div>
			</div>

			<!-- end panel -->
		</div>
		<!-- end col-12 -->
	</div>
	<!-- end row -->
</div>


<script type="text/javascript">
	var grid;
	$(document).ready(function() {
		grid = new Datatable();
		var l_id = '<?php echo $l_id; ?>';
		var f_id = '<?php echo $f_id; ?>';
		$("#page_loan").addClass("active");


		$('#default-daterange').daterangepicker({});

		grid.init({
			src: $(".ajax-table"),
			onSuccess: function(grid) {
				// execute some code after table records loaded
				$(".filter").appendTo(".table-toolbar");
				$("[name=search]:eq(0)").focus();
			},
			onError: function(grid) {
				// execute some code on network or other general error  
			},
			dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options 
				/* 
				    By default the ajax datatable's layout is horizontally scrollable and this can cause an issue of dropdown menu is used in the table rows which.
				    Use below "sDom" value for the datatable layout if you want to have a dropdown menu for each row in the datatable. But this disables the horizontal scroll. 
				*/
				//"sDom" : "<'row'<'col-md-8 col-sm-12'pl><'col-md-4 col-sm-12 text-right'i<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pl><'col-md-4 col-sm-12 text-right'i>r>>", 
				"sDom": "<'table-toolbar'>t<'row'<'col-md-8 col-sm-12'pl><'col-md-4 col-sm-12 text-right'i>r>>",
				"aLengthMenu": [
					[400, 500, 1000, 1500, 2000],
					[400, 500, 1000, 1500, 2000] // change per page values here
				],
				"oLanguage": { // language settings
					"sProcessing": '<fa class="fa fa-spin fa-spinner"></fa> Loading...',
				},
				"iDisplayLength": 400, // default record count per page
				"bServerSide": true, // server side processing
				"sAjaxSource": "scripts/php/loan/allLoan_ajax.php?l_id=<?php echo $l_id; ?>&f_id=<?php echo $f_id; ?>", // ajax source
				"aaSorting": [
					[1, "asc"]
				], // set first column as a default sort by asc
				"aoColumns": [{
						"sName": "select",
						"bVisible": false
					},
					{
						"sName": "id",
						"bSortable": true,
						"sWidth": "2%"
					},
					{
						"sName": "name",
						"sWidth": "10%"
					},
					{
						"sName": "amount",
						"sWidth": "10%"
					},
					{
						"sName": "notes",
						"sWidth": "5%"
					},
					{
						"sName": "name",
						"sWidth": "5%"
					},

					{
						"sName": "date",
						"sWidth": "5%"
					},


				],
				"fnFooterCallback": function(nRow, aaData, iStart, iEnd, aiDisplay) {
					//var nCells = nRow.getElementsByTagName('th');
					//nCells[1].innerHTML=ajaxTotal.total_qty;
				},
				"fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
					$(nRow).data("row", aData[0]);
					$(nRow).find(".delete").click(function(e) {
						e.stopPropagation();
						var id = $(this).attr('i');
						$("input[name='id']").val(id);
						$("#modal-del").modal("show");
					});
					$(nRow).click(function() {
						var r = $(this).data("row");
						//console.log(r);
						// redirect("#order_info.php?id=" + r);
					});
				}
			}
		});


		$(".form-filter").change(function() {
			$(".filter-submit").trigger("click");
		});

		var search_timeout = null;
		$("[name=search]").keyup(function() {
			var self = this;
			if (search_timeout) {
				clearTimeout(search_timeout);
				search_timeout = null;
			}
			search_timeout = setTimeout(function() {
				$(".filter-submit").trigger("click");
			}, 500);
		});


		/* Add Category */

		$(".selectUser").change(function(e) {
			var value = $(".selectUser  option:selected").val();
			window.location = "#loan.php?l_id=" + value + "&f_id=" + f_id;
			location.reload();
		});
		$(".selectFund").change(function(e) {
			var value = $(".selectFund  option:selected").val();
			window.location = "#loan.php?l_id=" + l_id + "&f_id=" + value;
			location.reload();
		});
	});
</script>