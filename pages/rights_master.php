<?php
include_once("../scripts/db.php");
db_connect();
if (!empty($_SESSION['access']) && $_SESSION['access'] != "") {
	$uid = $_SESSION['access']['uid'];
	$empMaster = getRow("SELECT *
FROM user 
WHERE status = 2 and  uid=:id", array('id' =>  base64_decode($_GET['id'])));
	$eid = $_REQUEST['id'];
}
$menuRights = getRows("SELECT m.id,m.menu_name,m.menu_key,m.is_view,m.is_add,m.is_edit,m.is_delete
FROM  rights_master m 
WHERE m.status = '1' and m.uid=:uid", array('uid' => base64_decode($_GET['id'])));

?>
<div id="content">
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li>
			<a href="#index.php">Home</a>
		</li>
		<li class="active">Rights Master</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header">Rights Master </h1>
	<!-- end page-header -->
	<!-- begin row -->
	<div class="row">
		<!-- begin col-12 -->
		<div class="col-md-12">
			<!-- begin panel -->
			<div class="panel panel-inverse" data-sortable-id="table-basic-7">
				<div class="panel-heading">
					<div class="panel-heading-btn">
						<!-- <a href='#employee_add.php' class="btn btn-xs btn-primary add">Add Employee</a> -->
					</div>

					<h4 class="panel-title">Rights Master</h4>
				</div>
				<div class="panel-body">


					<div class="table-container">
						<table class="table table-striped table-bordered table-hover ajax-table rowclick">
							<thead>
								<tr role="row" class="heading">
									<th>#</th>
									<th>Menu Name</th>
									<th>View</th>
									<th>Add</th>
									<th>Edit</th>
									<th>Delete</th>
								</tr>
							</thead>
							<tbody>
								<?php $i = 0;
								foreach ($menuRights as $row) {
									$i = $i + 1; ?>
									<tr role="row" class="heading">
										<td><?php echo $i; ?></td>
										<td><?php echo $row['menu_name']; ?></td>
										<td class="with-checkbox">
											<div class="checkbox checkbox-css">
												<input type="checkbox" class="rightsCheckBox checkBoxView" data-i="<?php echo $row['id'] ?>" value="" <?php if ($row['is_view'] == 1) {
																																							echo 'checked';
																																						} ?> />
												<label for="table_checkbox_2">&nbsp;</label>
											</div>
										</td>
										<td class="with-checkbox">
											<div class="checkbox checkbox-css">
												<input type="checkbox" class="rightsCheckBox checkBoxAdd" data-i="<?php echo $row['id'] ?>" value="" <?php if ($row['is_add'] == '1') {
																																							echo 'checked';
																																						} ?> />
												<label for="table_checkbox_2">&nbsp;</label>
											</div>
										</td>
										<td class="with-checkbox">
											<div class="checkbox checkbox-css">
												<input type="checkbox" class="rightsCheckBox checkBoxEdit" data-i="<?php echo $row['id'] ?>" value="" <?php if ($row['is_edit'] == '1') {
																																							echo 'checked';
																																						} ?> />
												<label for="table_checkbox_2">&nbsp;</label>
											</div>
										</td>
										<td class="with-checkbox">
											<div class="checkbox checkbox-css">
												<input type="checkbox" class="rightsCheckBox checkBoxDelete" data-i="<?php echo $row['id'] ?>" value="" <?php if ($row['is_delete'] == '1') {
																																							echo 'checked';
																																						} ?> />
												<label for="table_checkbox_2">&nbsp;</label>
											</div>
										</td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
						<?php if (count($menuRights) != '0') { ?>
							<div class="row">
								<div class="col-md-1 col-md-offset-10">
									<br>
									<button type="button" class="btn btn-success btnSave" style="margin-right: 15px;">Save</button>
								</div>
							</div>
						<?php } ?>
					</div>
				</div>
			</div>
			<!-- end panel -->
		</div>
		<!-- end col-12 -->
	</div>
	<!-- end row -->
</div>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/underscore@1.11.0/underscore-min.js"> </script>


<script type="text/javascript">
	var grid;
	$(document).ready(function() {
		grid = new Datatable();
		$(".nav").find('li').removeClass("active");
		$("#page_employee").addClass("active");
		$(".has-sub").removeClass("expand");
		$(".sub-menu").attr("style", "display:none;");
		var select_institute = getCookie('SelectedInstitute');
		var emp_id = '<?php echo $eid; ?>';


		var data = '<?php echo json_encode($menuRights) ?>';
		data = JSON.parse(data);
console.log('***',data)
		$(".checkBoxView").click(function(e) {
			e.stopPropagation();
			var removeData = [];
			var id = $(this).data('i');
			var findData = _.find(data, function(d) {
				return d.id == id;
			})
			removeData = _.reject(data, function(d) {
				return d.id == id;
			});

			if (findData.is_view == '1') {
				findData.is_view = '0';
			} else {
				findData.is_view = '1';
			}

			removeData.push(findData);
			data = removeData;
		});
		$(".checkBoxAdd").click(function(e) {
			e.stopPropagation();
			var removeData = [];
			var id = $(this).data('i');
			var findData = _.find(data, function(d) {
				return d.id == id;
			})
			removeData = _.reject(data, function(d) {
				return d.id == id;
			});
			if (findData.is_add == '1') {
				findData.is_add = '0';
			} else {
				findData.is_add = '1';
			}

			removeData.push(findData);
			data = removeData;
		});
		$(".checkBoxEdit").click(function(e) {
			e.stopPropagation();
			var removeData = [];
			var id = $(this).data('i');
			var findData = _.find(data, function(d) {
				return d.id == id;
			})
			removeData = _.reject(data, function(d) {
				return d.id == id;
			});
			if (findData.is_edit == '1') {
				findData.is_edit = '0';
			} else {
				findData.is_edit = '1';
			}

			removeData.push(findData);
			data = removeData;
		});
		$(".checkBoxDelete").click(function(e) {
			e.stopPropagation();
			var removeData = [];
			var id = $(this).data('i');
			var findData = _.find(data, function(d) {
				return d.id == id;
			})
			removeData = _.reject(data, function(d) {
				return d.id == id;
			});
			if (findData.is_delete == '1') {
				findData.is_delete = '0';
			} else {
				findData.is_delete = '1';
			}
	
			removeData.push(findData);
			data = removeData;
		});
		$(".btnSave").click(function(e) {
			$('.btnSave').attr("disabled", true);


			if (data && data.length > 0) {
				var json = {};
				json = {
					'emp_id': '<?php echo base64_decode($_GET['id']) ?>',
					'data': data,
				}
				var j = JSON.stringify(json);
				$.post(siteURL + "scripts/php/menu_rights/menu_status.php", {
					'json': j
				}, function(dataA) {
					notify(dataA);
					$('.btnSave').attr("disabled", false);
					if (dataA.success) {
						data = [];
						window.history.go(-1);
					}
				});
			} else {
				notify({
					error: "Please add permission"
				});
				$('.btnSave').attr("disabled", false);
				return false;
			}
		});

		// 		"fnFooterCallback": function(nRow, aaData, iStart, iEnd, aiDisplay) {
		// 			//var nCells = nRow.getElementsByTagName('th');
		// 			//nCells[1].innerHTML=ajaxTotal.total_qty;
		// 		},

		// 		"fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
		// 			$(nRow).data("row", aData[0]);
		// 			$(nRow).find(".delete").click(function(e) {
		// 				e.stopPropagation();
		// 				var id = aData[0];
		// 				$("input[name='id']").val(id);
		// 				$("#modal-del").modal("show");
		// 			});
		// 			$(nRow).find(".isView").click(function(e) {
		// 				// $('.loading ').removeClass('hide')
		// 				e.stopPropagation();
		// 				var id = $(this).data('i');
		// 				var mid = $(this).data('m');
		// 				var rid = $(this).data('r');
		// 				var value = $(this).data('v');
		// 				$.post(siteURL + "scripts/php/menu_rights/menu_status.php?id=" + id + '&type=view&value=' + value + '&mid=' + mid + '&rid=' + rid + '&emp_id=' + <?php echo base64_decode($_GET['id']); ?>, function(data) {
		// 					notify(data);
		// 					// $('.loading ').addClass('hide')
		// 					// if (data.success) {
		// 					// 	// $(".filter-submit").trigger("click");
		// 					// } else {
		// 					// }
		// 				});
		// 			});
		// 			$(nRow).find(".isAdd").click(function(e) {
		// 				e.stopPropagation();
		// 				// $('.loading ').removeClass('hide')
		// 				var id = $(this).data('i');
		// 				var mid = $(this).data('m');
		// 				var rid = $(this).data('r');
		// 				var value = $(this).data('v');
		// 				$.post(siteURL + "scripts/php/menu_rights/menu_status.php?id=" + id + '&type=add&value=' + value + '&mid=' + mid + '&rid=' + rid + '&emp_id=' + <?php echo base64_decode($_GET['id']); ?>, function(data) {
		// 					notify(data);
		// 					// $('.loading ').addClass('hide')
		// 					// if (data.success) {
		// 					// 	$(".filter-submit").trigger("click");
		// 					// } else {
		// 					// }
		// 				});
		// 			});
		// 			$(nRow).find(".isEdit").click(function(e) {
		// 				e.stopPropagation();
		// 				// $('.loading ').removeClass('hide')
		// 				var id = $(this).data('i');
		// 				var mid = $(this).data('m');
		// 				var rid = $(this).data('r');
		// 				var value = $(this).data('v');
		// 				$.post(siteURL + "scripts/php/menu_rights/menu_status.php?id=" + id + '&type=edit&value=' + value + '&mid=' + mid + '&rid=' + rid + '&emp_id=' + <?php echo base64_decode($_GET['id']); ?>, function(data) {
		// 					notify(data);
		// 					$('.loading ').addClass('hide')
		// 					// if (data.success) {
		// 					// 	$(".filter-submit").trigger("click");
		// 					// } else {
		// 					// }
		// 				});
		// 			});
		// 			$(nRow).find(".isDelete").click(function(e) {
		// 				e.stopPropagation();
		// 				// $('.loading ').removeClass('hide')
		// 				var id = $(this).data('i');
		// 				var mid = $(this).data('m');
		// 				var rid = $(this).data('r');
		// 				var value = $(this).data('v');
		// 				$.post(siteURL + "scripts/php/menu_rights/menu_status.php?id=" + id + '&type=delete&value=' + value + '&mid=' + mid + '&rid=' + rid + '&emp_id=' + <?php echo base64_decode($_GET['id']); ?>, function(data) {
		// 					notify(data);
		// 					$('.loading ').addClass('hide')
		// 					// if (data.success) {
		// 					// 	$(".filter-submit").trigger("click");
		// 					// } else {
		// 					// }
		// 				});
		// 			});
		// 			$(nRow).find(".edit").click(function(e) {
		// 				e.stopPropagation();
		// 				var data = $(this).data('i');
		// 				var id = aData[0];
		// 				var logo = aData[7];

		// 				$("input[name='id']").val(id);
		// 				$(".medium option[value='" + data.type + "']").attr("selected", "selected");
		// 				$("#modal-edit").modal("show");
		// 			});
		// 		}
		// 	}
		// });

		$('.add').click(function() {
			$("input[name='id']").val('');
			$("#i1").attr('src', '');
		});

		$(".form-filter").change(function() {
			$(".filter-submit").trigger("click");
		});

		var search_timeout = null;
		$("[name=search]").keyup(function() {
			var self = this;
			if (search_timeout) {
				clearTimeout(search_timeout);
				search_timeout = null;
			}
			search_timeout = setTimeout(function() {
				$(".filter-submit").trigger("click");
			}, 500);
		});

		$("input[type=file]").change(function() {
			$("#upload-loader").removeClass('hide');
			var formData = new FormData();
			formData.append('filePath', 'Profile');

			formData.append('section', 'general');
			formData.append('action', 'previewImg');
			formData.append('file', $(this)[0].files[0]);
			var field = $(this).data('field');
			var img = $(this).data('img');
			//$('#loading').html('<img src="../images/loader.gif"> loading...');
			$.ajax({
				url: siteURL + "scripts/php/upload_photo.php",
				type: 'POST',
				dataType: 'json',
				processData: false,
				contentType: false,
				data: formData,
				success: function(data) {
					$('#loading').html(data);
					$("#upload-loader").addClass('hide');
					console.log(data);
					if (data.success) {
						$(field).val(data.name);
						$(img).prop("src", data.url);
					} else {
						console.log(data.error);
					}
				},
				error: function(r) {
					$("#upload-loader").addClass('hide');
					console.log("Error occured");
				},
			});
		});

		/* Add Institute */
		$("#frmAdd").submit(function(e) {
			e.preventDefault();
			$.post(siteURL + "scripts/php/institute/institute_add.php", $(this).serialize(), function(data) {
				notify(data);
				if (data.success) {
					$("#modal-add").modal("hide");
					$(".filter-submit").trigger("click");
				} else {

				}
			});
		});

		/* Edit Category */
		$("#frmEdit").submit(function(e) {
			e.preventDefault();
			$.post(siteURL + "scripts/php/institute/institute_edit.php", $(this).serialize(), function(data) {
				notify(data);
				if (data.success) {
					$("#modal-edit").modal("hide");
					$(".filter-submit").trigger("click");
					$("input[name='id']").val('');
					$("input[name='name']").val('');
					$("textarea[name='address']").val('');
					$("input[name='website']").val('');
					$("input[name='phone_no']").val('');
					$("input[name='estab_year']").val('');
					$("input[name='pin_code']").val('');
					$("input[name='principal_name']").val('');
					$("input[name='principal_contact']").val('');
					$("input[name='facebook']").val('');
					$("input[name='tweeter']").val('');
					$("input[name='instagram']").val('');
					$("input[name='email']").val('');
					$("#i2").attr('src', '');
				} else {

				}
			});
		});

		/* Delete Category */
		$("#frmDel").submit(function(e) {
			e.preventDefault();
			$.post(siteURL + "scripts/php/institute/institute_delete.php", $(this).serialize(), function(data) {
				notify(data);
				if (data.success) {
					$("#modal-del").modal("hide");
					$(".filter-submit").trigger("click");
				} else {

				}
			});
		});

	});
</script>