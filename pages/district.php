	<div id="content">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li>
				<a href="javascript:;">Home</a>
			</li>
			<li class="active">District Master</li>
		</ol>
		<!-- end breadcrumb -->
		<!-- begin page-header -->
		<h1 class="page-header">District Master </h1>
		<!-- end page-header -->
		<div class="row hide">
			<!-- begin col-12 -->
			<div class="col-md-12">
				<!-- begin panel -->
				<div class="panel panel-inverse" data-sortable-id="table-basic-7">
					<div class="panel-heading">
						<div class="panel-heading-btn">
							<a href='javascript:;' role='button' data-toggle='modal' data-target='#modal-add-member' class="btn btn-xs btn-success add">Add member</a>
						</div>

						<h4 class="panel-title">Management કમિટી</h4>
					</div>
					<div class="panel-body">
						<div class="filter1">
							<div class="row">
								<div class="form-group col-md-12 hide">
									<label class="col-md-1 control-label" style="margin:6px">Search : </label>
									<div class="col-md-7">
										<input type="text" name="search" class="form-filter form-control" placeholder="Search Any..." />
									</div>
								</div>
								<button class="hidden btn btn-xs btn-default filter-submit margin-bottom"><i class="fa fa-search"></i> Search</button>
							</div>
						</div>
						<div class="table-container">
							<table class="table table-striped table-bordered table-hover role_grid_ajax-table rowclick">
								<thead>
									<tr role="row" class="heading">
										<th class="no-sort" width="5%"><input type="checkbox" class="toggle_all" /></th>
										<th>#</th>
										<th>Name</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<!-- end panel -->
			</div>
			<!-- end col-12 -->
		</div>
		<!-- begin row -->
		<div class="row">
			<!-- begin col-12 -->
			<div class="col-md-12">
				<!-- begin panel -->
				<div class="panel panel-primary" data-sortable-id="table-basic-7">
					<div class="panel-heading">
						<div class="panel-heading-btn">
							<a href='javascript:;' role='button' data-toggle='modal' data-target='#modal-add' class="btn btn-xs btn-success add">Add District</a>
						</div>

						<h4 class="panel-title">District Master</h4>
					</div>
					<div class="panel-body">
						<div class="filter">
							<div class="row">
								<div class="form-group col-md-12">
									<label class="col-md-1 control-label" style="margin:6px">Search : </label>
									<div class="col-md-7">
										<input type="text" name="search" class="form-filter form-control" placeholder="Search Any..." />
									</div>
								</div>
								<button class="hidden btn btn-xs btn-default filter-submit margin-bottom"><i class="fa fa-search"></i> Search</button>
							</div>
						</div>
						<div class="table-container">
							<table class="table table-striped table-bordered table-hover ajax-table rowclick">
								<thead>
									<tr role="row" class="heading">
										<th class="no-sort" width="5%"><input type="checkbox" class="toggle_all" /></th>
										<th>#</th>
										<th>Name</th>
										<th>કમિટી</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<!-- end panel -->
			</div>
			<!-- end col-12 -->
		</div>
		<!-- end row -->
	</div>

	<!--- Add Modal -->
	<div class="modal fade" id="modal-add">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title">Add District</h4>
				</div>
				<form method="post" class="form-horizontal" id="frmAdd">
					<input type="hidden" name="action" value="add">
					<div class="modal-body">
						<div class="form-group">
							<label class="col-md-2 control-label">District</label>
							<div class="col-md-9">
								<input type="text" name="name" class="form-control" placeholder="Enter district name">
							</div>
						</div>

					</div>
					<div class="modal-footer">
						<a href="javascript:;" class="btn btn-sm btn-white" data-dismiss="modal">Close</a>
						<input type="submit" class="btn btn-sm btn-success" value="Save">
					</div>
				</form>
			</div>
		</div>
	</div>

	<!--- Edit Modal -->
	<div class="modal fade" id="modal-edit">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title">Edit District</h4>
				</div>
				<form method="post" class="form-horizontal" id="frmEdit">
					<input type="hidden" name="action" value="edit">
					<input type="hidden" name="id" value="0">
					<div class="modal-body">
						<div class="form-group">
							<label class="col-md-2 control-label">District</label>
							<div class="col-md-9">
								<input type="text" name="name" class="form-control" placeholder="Enter district name">
							</div>
						</div>

					</div>
					<div class="modal-footer">
						<a href="javascript:;" class="btn btn-sm btn-white" data-dismiss="modal">Close</a>
						<input type="submit" class="btn btn-sm btn-success" value="Save">
					</div>
				</form>
			</div>
		</div>
	</div>

	<!--- Delete Modal -->
	<div class="modal fade" id="modal-del">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title">Delete Confirmation</h4>
				</div>
				<form method="post" id="frmDel">
					<div class="modal-body">
						<div class="alert alert-danger m-b-0">
							<input type="hidden" name="id" value="0">
							<h5><i class="fa fa-info-circle"></i>Do you want to delete record ?</h5>

						</div>
					</div>
					<div class="modal-footer">

						<input type='submit' class="btn btn-sm btn-danger" value='Delete' name='delete'>
						<a href="javascript:;" class="btn btn-sm btn-white" data-dismiss="modal">Close</a>
					</div>
				</form>
			</div>
		</div>
	</div>


	<!--- Delete Modal -->
	<div class="modal fade" id="modal-del-member">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title">Delete Confirmation</h4>
				</div>
				<form method="post" id="frmDelMember">
					<div class="modal-body">
						<div class="alert alert-danger m-b-0">
							<input type="hidden" name="id" value="0">
							<h5><i class="fa fa-info-circle"></i>Do you want to delete record ?</h5>

						</div>
					</div>
					<div class="modal-footer">

						<input type='submit' class="btn btn-sm btn-danger" value='Delete' name='delete'>
						<a href="javascript:;" class="btn btn-sm btn-white" data-dismiss="modal">Close</a>
					</div>
				</form>
			</div>
		</div>
	</div>


	<div class="modal fade" id="modal-add-member">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title">Add member</h4>
				</div>
				<form method="post" class="form-horizontal" id="frmAddMember">
					<input type="hidden" name="action" value="add">
					<input type="hidden" name="type" value="managment">
					<input type="hidden" name="city_id" value="0">
					<input type="hidden" name="assign_id" value="<?php echo $uid; ?>">
					<div class="modal-body">
						<div class="form-group">
							<label class="col-md-3 control-label">Search member</label>
							<div class="col-md-6">
								<input type="text" name="mobile" class="form-control mobile" placeholder="Enter mobile no.">
							</div>
							<div class="col-md-2">
								<input type="button" class="btn btn-sm btn-indigo btn-find" value="Search">
							</div>
						</div>
						<div class="form-group UserDetails" style="margin-left: 30px;">

						</div>

					</div>
					<div class="modal-footer">
						<a href="javascript:;" class="btn btn-sm btn-white" data-dismiss="modal">Close</a>
						<input type="submit" class="btn btn-sm btn-success" value="Save">
					</div>
				</form>
			</div>
		</div>
	</div>


	<script type="text/javascript">
		var grid;
		var role_grid;
		$(document).ready(function() {
			grid = new Datatable();
			role_grid = new Datatable();
			$(".nav").find('li').removeClass("active");
			$("#page_district").addClass("active");
			$(".has-sub").removeClass("expand");
			$(".sub-menu").attr("style", "display:none;");
			role_grid.init({
				src: $(".role_grid_ajax-table"),
				onSuccess: function(role_grid) {
					// execute some code after table records loaded
					$(".filter1").appendTo(".table-toolbar1");
					$("[name=search]:eq(0)").focus();
				},
				onError: function(role_grid) {
					// execute some code on network or other general error  
				},
				dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options 
					/* 
						By default the ajax datatable's layout is horizontally scrollable and this can cause an issue of dropdown menu is used in the table rows which.
						Use below "sDom" value for the datatable layout if you want to have a dropdown menu for each row in the datatable. But this disables the horizontal scroll. 
					*/
					//"sDom" : "<'row'<'col-md-8 col-sm-12'pl><'col-md-4 col-sm-12 text-right'i<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pl><'col-md-4 col-sm-12 text-right'i>r>>", 
					"sDom": "<'table-toolbar1'>t<'row'<'col-md-8 col-sm-12'pl><'col-md-4 col-sm-12 text-right'i>r>>",
					"aLengthMenu": [
						[15, 25, 30, 40, 50],
						[15, 25, 30, 40, 50] // change per page values here
					],
					"oLanguage": { // language settings
						"sProcessing": '<fa class="fa fa-spin fa-spinner"></fa> Loading...',
					},
					"iDisplayLength": 15, // default record count per page
					"bServerSide": true, // server side processing
					"sAjaxSource": "scripts/php/role_master/management_data.php?id=0", // ajax source
					"aaSorting": [
						[1, "desc"]
					], // set first column as a default sort by asc
					"aoColumns": [{
							"sName": "select",
							"bVisible": false
						},
						{
							"sName": "id",
							"bSortable": true,
							"sWidth": "2%"
						},
						{
							"sName": "name",
							"bSortable": true,
							"sWidth": "30%"
						},
						{
							"sName": "status",
							"sWidth": "8%"
						},
					],
					"fnFooterCallback": function(nRow, aaData, iStart, iEnd, aiDisplay) {
						//var nCells = nRow.getElementsByTagName('th');
						//nCells[1].innerHTML=ajaxTotal.total_qty;
					},
					"fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
						$(nRow).data("row", aData[0]);
						$(nRow).find(".delete").click(function(e) {
							e.stopPropagation();
							var id = aData[0];
							$("input[name='id']").val(id);
							$("#modal-del-member").modal("show");
						});
						$(nRow).click(function() {
							var r = $(this).data("row");
							//console.log(r);
							// redirect("#village.php?id=" + r);
						});
						$(nRow).find(".edit").click(function(e) {
							e.stopPropagation();
							var row = $(this).data('i');
							var id = aData[0];
							var name = aData[2];
							$("input[name='id']").val(id);
							$("input[name='name']").val(name);

							$("#modal-edit").modal("show");
						});

					}
				}
			});
			grid.init({
				src: $(".ajax-table"),
				onSuccess: function(grid) {
					// execute some code after table records loaded
					$(".filter").appendTo(".table-toolbar");
					$("[name=search]:eq(0)").focus();
				},
				onError: function(grid) {
					// execute some code on network or other general error  
				},
				dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options 
					/* 
						By default the ajax datatable's layout is horizontally scrollable and this can cause an issue of dropdown menu is used in the table rows which.
						Use below "sDom" value for the datatable layout if you want to have a dropdown menu for each row in the datatable. But this disables the horizontal scroll. 
					*/
					//"sDom" : "<'row'<'col-md-8 col-sm-12'pl><'col-md-4 col-sm-12 text-right'i<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pl><'col-md-4 col-sm-12 text-right'i>r>>", 
					"sDom": "<'table-toolbar'>t<'row'<'col-md-8 col-sm-12'pl><'col-md-4 col-sm-12 text-right'i>r>>",
					"aLengthMenu": [
						[1000, 2000, 3000, 4000, 5000],
						[1000, 2000, 3000, 4000, 5000] // change per page values here
					],
					"oLanguage": { // language settings
						"sProcessing": '<fa class="fa fa-spin fa-spinner"></fa> Loading...',
					},
					"iDisplayLength": 1000, // default record count per page
					"bServerSide": true, // server side processing
					"sAjaxSource": "scripts/php/city/dist_ajax.php", // ajax source
					"aaSorting": [
						[2, "asc"]
					], // set first column as a default sort by asc
					"aoColumns": [{
							"sName": "select",
							"bVisible": false
						},
						{
							"sName": "id",
							"bSortable": true,
							"sWidth": "2%"
						},
						{
							"sName": "name",
							"bSortable": true,
							"sWidth": "30%"
						},
						{
							"sName": "member",
							"sWidth": "10%",
							"bVisible": false
						},
						{
							"sName": "status",
							"sWidth": "8%"
						},
					],
					"fnFooterCallback": function(nRow, aaData, iStart, iEnd, aiDisplay) {
						//var nCells = nRow.getElementsByTagName('th');
						//nCells[1].innerHTML=ajaxTotal.total_qty;
					},
					"fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
						$(nRow).data("row", aData[0]);
						$(nRow).find(".delete").click(function(e) {
							e.stopPropagation();
							var id = aData[0];
							$("input[name='id']").val(id);
							$("#modal-del").modal("show");
						});
						$(nRow).click(function() {
							var r = $(this).data("row");
							//console.log(r);
							redirect("#city.php?id=" + r);
						});
						$(nRow).find(".edit").click(function(e) {
							e.stopPropagation();
							var row = $(this).data('i');
							var id = aData[0];
							var name = aData[2];
							$("input[name='id']").val(id);
							$("input[name='name']").val(name);

							$("#modal-edit").modal("show");
						});


					}
				}
			});

			$('.add').click(function() {
				$("[name=name]").val("");
				$('#frmAdd')[0].reset();
			});

			$(".form-filter").change(function() {
				$(".filter-submit").trigger("click");
			});

			var search_timeout = null;
			$("[name=search]").keyup(function() {
				var self = this;
				if (search_timeout) {
					clearTimeout(search_timeout);
					search_timeout = null;
				}
				search_timeout = setTimeout(function() {
					$(".filter-submit").trigger("click");
				}, 500);
			});

			/* Add Category */
			$("#frmAdd").submit(function(e) {
				e.preventDefault();
				$('#frmAdd .btn-success').attr("disabled", true);
				$.post(siteURL + "scripts/php/city/dist_add.php", $(this).serialize(), function(data) {
					notify(data);
					$('#frmAdd .btn-success').attr("disabled", false);
					if (data.success) {
						$("#modal-add").modal("hide");
						$(".filter-submit").trigger("click");
						$('#frmAdd')[0].reset();
					} else {

					}
				});
			});

			/* Edit Category */
			$("#frmEdit").submit(function(e) {
				e.preventDefault();
				$.post(siteURL + "scripts/php/city/dist_edit.php", $(this).serialize(), function(data) {
					notify(data);
					if (data.success) {
						$("#modal-edit").modal("hide");
						$(".filter-submit").trigger("click");
						$('#frmEdit')[0].reset();
					} else {

					}
				});
			});

			/* Delete Category */
			$("#frmDel").submit(function(e) {
				e.preventDefault();
				$.post(siteURL + "scripts/php/city/dist_del.php", $(this).serialize(), function(data) {
					notify(data);
					if (data.success) {
						$("#modal-del").modal("hide");
						$(".filter-submit").trigger("click");
					} else {

					}
				});
			});
			$(".btn-find").click(function(e) {
				var mobile = $('.mobile').val();
				$(".UserDetails").empty("");
				if (!mobile) {
					notify({
						error: "Please enter mobile no"
					});
				}
				$.post(siteURL + "scripts/php/role_master/searchByMobile.php?mobile=" + mobile, function(data) {
					var st = data.data;
					if (st) {
						$(".UserDetails").append("<h5 class='modal-title'>Name: <span style='color:#F96332;'>" + st.full_name + "</span></h5>" +
							"<h5 class='modal-title' style='margin-top: 5px;'>Mobile: <span style='color:#F96332;'>" + st.mobile + "</span></h5>" +
							"<input type='hidden' name='uid' value='" + st.uid + "'>");
					}
				});
			});

			$("#frmAddMember").submit(function(e) {
				e.preventDefault();
				$('#frmAddMember .btn-success').attr("disabled", true);
				$.post(siteURL + "scripts/php/role_master/dist_add.php", $(this).serialize(), function(data) {
					notify(data);
					$('#frmAddMember .btn-success').attr("disabled", false);
					if (data.success) {
						$("#modal-add-member").modal("hide");
						$(".filter-submit").trigger("click");
						$('#frmAddMember')[0].reset();
					} else {

					}
				});
			});
			$("#frmDelMember").submit(function(e) {
				e.preventDefault();
				$.post(siteURL + "scripts/php/role_master/del.php", $(this).serialize(), function(data) {
					notify(data);
					if (data.success) {
						$("#modal-del-member").modal("hide");
						$(".filter-submit").trigger("click");
					} else {

					}
				});
			});
		});
	</script>