<div id="content" >
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Home</a></li>
				<li class="active">Transactions</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Transactions </h1>
			<!-- end page-header -->
			
			<!-- begin row -->
			<div class="row">
			    
			    <!-- begin col-12 -->
			    <div class="col-md-12 ui-sortable">
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand" data-original-title="" title=""><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload" data-original-title="" title=""><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">Transactions</h4>
                        </div>
                        <div class="panel-body">
                            <div id="data-table_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer"><div class="dataTables_length" id="data-table_length"><label>Show <select name="data-table_length" aria-controls="data-table" class="form-control input-sm"><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> entries</label></div><div class="dt-buttons btn-group"><a class="btn btn-default buttons-copy buttons-flash btn-sm" tabindex="0" aria-controls="data-table"><span>Copy</span><div style="position: absolute; left: 0px; top: 0px; width: 49px; height: 30px; z-index: 99;"><embed id="ZeroClipboard_TableToolsMovie_1" src="//cdn.datatables.net/buttons/1.0.0/swf/flashExport.swf" loop="false" menu="false" quality="best" bgcolor="#ffffff" width="49" height="30" name="ZeroClipboard_TableToolsMovie_1" align="middle" allowscriptaccess="always" allowfullscreen="false" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" flashvars="id=1&amp;width=49&amp;height=30" wmode="transparent"></div></a><a class="btn btn-default buttons-csv buttons-html5 btn-sm" tabindex="0" aria-controls="data-table"><span>CSV</span></a><a class="btn btn-default buttons-excel buttons-flash btn-sm" tabindex="0" aria-controls="data-table"><span>Excel</span><div style="position: absolute; left: 0px; top: 0px; width: 50px; height: 30px; z-index: 99;"><embed id="ZeroClipboard_TableToolsMovie_2" src="//cdn.datatables.net/buttons/1.0.0/swf/flashExport.swf" loop="false" menu="false" quality="best" bgcolor="#ffffff" width="50" height="30" name="ZeroClipboard_TableToolsMovie_2" align="middle" allowscriptaccess="always" allowfullscreen="false" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" flashvars="id=2&amp;width=50&amp;height=30" wmode="transparent"></div></a><a class="btn btn-default buttons-pdf buttons-flash btn-sm" tabindex="0" aria-controls="data-table"><span>PDF</span><div style="position: absolute; left: 0px; top: 0px; width: 43px; height: 30px; z-index: 99;"><embed id="ZeroClipboard_TableToolsMovie_3" src="//cdn.datatables.net/buttons/1.0.0/swf/flashExport.swf" loop="false" menu="false" quality="best" bgcolor="#ffffff" width="43" height="30" name="ZeroClipboard_TableToolsMovie_3" align="middle" allowscriptaccess="always" allowfullscreen="false" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" flashvars="id=3&amp;width=43&amp;height=30" wmode="transparent"></div></a><a class="btn btn-default buttons-print btn-sm" tabindex="0" aria-controls="data-table"><span>Print</span></a></div><div id="data-table_filter" class="dataTables_filter"><label>Search:<input type="search" class="form-control input-sm" placeholder="" aria-controls="data-table"></label></div><div style="position: absolute; height: 1px; width: 0px; overflow: hidden;"><input type="text" tabindex="0"></div><table id="data-table" class="table table-striped table-bordered dataTable no-footer dtr-inline collapsed" role="grid" aria-describedby="data-table_info" style="position: relative; width: 100%;">
                                <thead>
                                    <tr role="row"><th width="100px" nowrap="" class="sorting_asc" tabindex="0" aria-controls="data-table" rowspan="1" colspan="1" data-column-index="0" style="width: 84px;" aria-sort="ascending" aria-label="ID: activate to sort column descending">ID</th><th width="200px" nowrap="" class="sorting" tabindex="0" aria-controls="data-table" rowspan="1" colspan="1" data-column-index="1" style="width: 185px;" aria-label="First name: activate to sort column ascending">First name</th><th width="200px" nowrap="" class="sorting" tabindex="0" aria-controls="data-table" rowspan="1" colspan="1" data-column-index="2" style="width: 186px;" aria-label="Last name: activate to sort column ascending">Last name</th></tr>
                                </thead>
                                <tbody>   
                                <tr class="gradeA odd" role="row">
                                        <td class="sorting_1">Gecko</td>
                                        <td>Firefox 1.0</td>
                                        <td>Win 98+ / OSX.2+</td>
                                        
                                        
                                    </tr><tr class="gradeA even" role="row">
                                        <td class="sorting_1">Gecko</td>
                                        <td>Firefox 1.5</td>
                                        <td>Win 98+ / OSX.2+</td>
                                        
                                        
                                    </tr><tr class="gradeA odd" role="row">
                                        <td class="sorting_1">Gecko</td>
                                        <td>Firefox 2.0</td>
                                        <td>Win 98+ / OSX.2+</td>
                                        
                                        
                                    </tr><tr class="gradeA even" role="row">
                                        <td class="sorting_1">Gecko</td>
                                        <td>Firefox 3.0</td>
                                        <td>Win 2k+ / OSX.3+</td>
                                        
                                        
                                    </tr><tr class="gradeA odd" role="row">
                                        <td class="sorting_1">Gecko</td>
                                        <td>Camino 1.0</td>
                                        <td>OSX.2+</td>
                                        
                                        
                                    </tr><tr class="gradeA even" role="row">
                                        <td class="sorting_1">Gecko</td>
                                        <td>Camino 1.5</td>
                                        <td>OSX.3+</td>
                                        
                                        
                                    </tr><tr class="gradeA odd" role="row">
                                        <td class="sorting_1">Gecko</td>
                                        <td>Netscape 7.2</td>
                                        <td>Win 95+ / Mac OS 8.6-9.2</td>
                                        
                                        
                                    </tr><tr class="gradeA even" role="row">
                                        <td class="sorting_1">Gecko</td>
                                        <td>Netscape Browser 8</td>
                                        <td>Win 98SE+</td>
                                        
                                        
                                    </tr><tr class="gradeA odd" role="row">
                                        <td class="sorting_1">Gecko</td>
                                        <td>Netscape Navigator 9</td>
                                        <td>Win 98+ / OSX.2+</td>
                                        
                                        
                                    </tr><tr class="gradeA even" role="row">
                                        <td class="sorting_1">Gecko</td>
                                        <td>Mozilla 1.0</td>
                                        <td>Win 95+ / OSX.1+</td>
                                        
                                        
                                    </tr></tbody>
                            </table><div class="dataTables_info" id="data-table_info" role="status" aria-live="polite">Showing 1 to 10 of 57 entries</div><div class="dataTables_paginate paging_simple_numbers" id="data-table_paginate"><ul class="pagination"><li class="paginate_button previous disabled" id="data-table_previous"><a href="#" aria-controls="data-table" data-dt-idx="0" tabindex="0">Previous</a></li><li class="paginate_button active"><a href="#" aria-controls="data-table" data-dt-idx="1" tabindex="0">1</a></li><li class="paginate_button "><a href="#" aria-controls="data-table" data-dt-idx="2" tabindex="0">2</a></li><li class="paginate_button "><a href="#" aria-controls="data-table" data-dt-idx="3" tabindex="0">3</a></li><li class="paginate_button "><a href="#" aria-controls="data-table" data-dt-idx="4" tabindex="0">4</a></li><li class="paginate_button "><a href="#" aria-controls="data-table" data-dt-idx="5" tabindex="0">5</a></li><li class="paginate_button "><a href="#" aria-controls="data-table" data-dt-idx="6" tabindex="0">6</a></li><li class="paginate_button next" id="data-table_next"><a href="#" aria-controls="data-table" data-dt-idx="7" tabindex="0">Next</a></li></ul></div></div>
                        </div>
                    </div>
			    </div>
		
			    <!-- end col-10 -->
			</div>
			<!-- end row -->
		</div>