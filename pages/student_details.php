<?php
include_once("../scripts/db.php");
include_once("../scripts/functions.php");
db_connect();
if (!empty($_SESSION['access']) && $_SESSION['access'] != "") {
	$uid = $_SESSION['access']['uid'];
}
$id = $_REQUEST['id'];
$params = array('id' => $id);
$student_master = getRow("SELECT s.*
				  FROM student_scholarship s 
				  WHERE s.status = '1' and s.id=:id", $params);
$user_master = getRows("SELECT u.uid,u.middle_name,u.father_name,IFNULL(s.`name`,'') surname_name
					FROM user u 
					LEFT JOIN surname s ON s.id=u.surname_id
					WHERE u.status = '2'");

$standard_master = getRows("SELECT *
					FROM std_master 
					WHERE status = '1'");



?>
<div id="content" class="" style="padding: -20px -25px;">

	<div class="profile">
		<div class="profile-header">

			<div class="profile-header-cover"></div>


			<div class="profile-header-content">


				<div class="profile-header-info">
					<h4 class="mt-0 mb-1"><?php echo ucfirst($student_master['student_name']) ?></h4>
					<p class="mb-2"><?php echo ucfirst($student_master['parents_name']) ?></p>
					<!-- <a href="#" class="btn btn-xs btn-yellow">Edit Profile</a> -->
				</div>

			</div>


			<ul class="profile-header-tab nav nav-tabs">
				<li class="nav-item"><a href="#profile-info" class="nav-link active profile-info" data-toggle="tab">INFO</a></li>
			</ul>

		</div>
	</div>


	<div class="profile-content">

		<div class="tab-content p-0" style="background-color:#dee2e6;">

			<div class="tab-pane fade active show" id="profile-info" style="background-color:#fff;">

				<div class="row" style="margin-top: 15px;padding:15px">
					<div class="col-md-12">
						<div class="panel panel-primary" data-sortable-id="form-stuff-4">

							<div class="panel-heading">
								<div class="panel-heading-btn">
									<?php if (menuRights($uid, 'job', 'add')) { ?>
										<a href='javascript:;' role='button' data-toggle='modal' data-target='#modal-add' class="btn btn-xs btn-success add">Add school</a>
									<?php } ?>
								</div>
								<h4 class="panel-title">Scholarship info</h4>

							</div>
							<div class="panel-body">
								<div class="filter">
									<div class="row">
										<div class="form-group col-md-12 hide">
											<label class="col-md-1 control-label" style="margin:6px">Search : </label>
											<div class="col-md-7">
												<input type="text" name="search" class="form-filter form-control" placeholder="Search Any..." />
											</div>
										</div>
										<button class="hidden btn btn-xs btn-default filter-submit margin-bottom"><i class="fa fa-search"></i> Search</button>
									</div>
								</div>
								<div class="table-container">
									<table class="table table-striped table-bordered table-hover ajax-table rowclick">
										<thead>
											<tr role="row" class="heading">
												<th class="no-sort" width="5%"><input type="checkbox" class="toggle_all" /></th>
												<th>#</th>
												<th>School</th>
												<th>Standard</th>
												<th>Donation by</th>
												<th>Total Fees</th>
												<th>Scholarship Amount</th>
												<th>Parents Amount</th>
												<th>Donation Amount</th>
												<th>Note</th>
												<th>Date</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>



		</div>

	</div>

</div>

<!--- Add Modal -->
<div class="modal fade" id="modal-add">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title">Add</h4>
			</div>
			<form method="post" class="form-horizontal" id="frmAdd">
				<input type="hidden" name="action" value="add">
				<input type="hidden" name="student_id" value="<?php echo $id; ?>">
				<div class="modal-body">

					<div class="form-group">
						<label class="col-md-3  col-sm-12  control-label">School name </label>
						<div class="col-md-9  col-sm-12 ">
							<input type="text" name="school_name" class="form-control" placeholder="School name">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3  col-sm-12  control-label">School address </label>
						<div class="col-md-9  col-sm-12 ">
							<input type="text" name="school_address" class="form-control" placeholder="School address">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3  col-sm-12  control-label">School mobile </label>
						<div class="col-md-9  col-sm-12 ">
							<input type="text" name="school_mobile" class="form-control" placeholder="School mobile">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-3  col-sm-12  control-label">Standard</label>
						<div class="col-md-9  col-sm-12 ">
							<select class="form-control" name="std_id">
								<option value=''>Select Standard</option>
								<?php foreach ($standard_master as $row) { ?>
									<option value="<?php echo $row['id']; ?>"><?php echo ucfirst($row['name']); ?></option>
								<?php } ?>
							</select>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-3  col-sm-12  control-label">Donation by</label>
						<div class="col-md-9  col-sm-12 ">
							<select class="form-control" name="donation_id">
								<option value=''>Select Donation</option>
								<?php foreach ($user_master as $row) { ?>
									<option value="<?php echo $row['uid']; ?>"><?php echo $row['surname_name']  . ' ' . $row['middle_name'] . ' ' . $row['father_name']; ?></option>
								<?php } ?>
							</select>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-3  col-sm-12  control-label">School fees</label>
						<div class="col-md-9  col-sm-12 ">
							<input type="text" name="total_fees" class="form-control total_fees" placeholder=" School fees">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3  col-sm-12  control-label">School Scholarship Amount</label>
						<div class="col-md-9  col-sm-12 ">
							<input type="text" name="scholarship_fees" class="form-control scholarship_fees" placeholder="School Scholarship Amount">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3  col-sm-12  control-label">Parents Amount</label>
						<div class="col-md-9  col-sm-12 ">
							<input type="text" name="parents_fees" class="form-control parents_fees" placeholder="Parents Amount">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3  col-sm-12  control-label">Donation Amount</label>
						<div class="col-md-9  col-sm-12 ">
							<input type="text" name="donation_fees" class="form-control donation_fees" placeholder="Donation Amount">
						</div>
					</div>
					<!-- <div class="form-group">
						<label class="col-md-3  col-sm-12  control-label">Total Amount</label>
						<div class="col-md-9  col-sm-12 ">
							<input type="text" name="total_fees" class="form-control total_fees" placeholder="Total Amount">
						</div>
					</div> -->
					<div class="form-group">
						<label class="col-md-3  col-sm-12  control-label">Note</label>
						<div class="col-md-9  col-sm-12 ">
							<textarea name="notes" class="form-control" placeholder="Note"></textarea>
						</div>
					</div>

				</div>
				<div class="modal-footer">
					<a href="javascript:;" class="btn btn-sm btn-white" data-dismiss="modal">Close</a>
					<input type="submit" class="btn btn-sm btn-success" value="Save">
				</div>
			</form>
		</div>
	</div>
</div>

<!--- Edit Modal -->
<div class="modal fade" id="modal-edit">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title">Edit</h4>
			</div>
			<form method="post" class="form-horizontal" id="frmEdit">
				<input type="hidden" name="action" value="edit">
				<input type="hidden" name="id" value="0">
				<input type="hidden" name="student_id" value="<?php echo $id ?>">
				<div class="modal-body">
					<div class="form-group">
						<label class="col-md-3  col-sm-12  control-label">School name </label>
						<div class="col-md-9  col-sm-12 ">
							<input type="text" name="school_name" class="form-control" placeholder="School name">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3  col-sm-12  control-label">School address </label>
						<div class="col-md-9  col-sm-12 ">
							<input type="text" name="school_address" class="form-control" placeholder="School address">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3  col-sm-12  control-label">School mobile </label>
						<div class="col-md-9  col-sm-12 ">
							<input type="text" name="school_mobile" class="form-control" placeholder="School mobile">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-3  col-sm-12  control-label">Standard</label>
						<div class="col-md-9  col-sm-12 ">
							<select class="form-control std_id" name="std_id">
								<option value=''>Select Standard</option>
								<?php foreach ($standard_master as $row) { ?>
									<option value="<?php echo $row['id']; ?>"><?php echo ucfirst($row['name']); ?></option>
								<?php } ?>
							</select>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-3  col-sm-12  control-label">Donation by</label>
						<div class="col-md-9  col-sm-12 ">
							<select class="form-control donation_id" name="donation_id">
								<option value=''>Select Donation</option>
								<?php foreach ($user_master as $row) { ?>
									<option value="<?php echo $row['uid']; ?>"><?php echo $row['surname_name']  . ' ' . $row['middle_name'] . ' ' . $row['father_name']; ?></option>
								<?php } ?>
							</select>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-3  col-sm-12  control-label">School fees</label>
						<div class="col-md-9  col-sm-12 ">
							<input type="text" name="total_fees" class="form-control total_fees1" placeholder=" School fees">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3  col-sm-12  control-label">School Scholarship Amount</label>
						<div class="col-md-9  col-sm-12 ">
							<input type="text" name="scholarship_fees" class="form-control scholarship_fees1" placeholder="School Scholarship Amount">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3  col-sm-12  control-label">Parents Amount</label>
						<div class="col-md-9  col-sm-12 ">
							<input type="text" name="parents_fees" class="form-control parents_fees1" placeholder="Parents Amount">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3  col-sm-12  control-label">Donation Amount</label>
						<div class="col-md-9  col-sm-12 ">
							<input type="text" name="donation_fees" class="form-control donation_fees1" placeholder="Donation Amount">
						</div>
					</div>
					<!-- <div class="form-group">
						<label class="col-md-3  col-sm-12  control-label">Total Amount</label>
						<div class="col-md-9  col-sm-12 ">
							<input type="text" name="total_fees" class="form-control total_fees" placeholder="Total Amount">
						</div>
					</div> -->
					<div class="form-group">
						<label class="col-md-3  col-sm-12  control-label">Note</label>
						<div class="col-md-9  col-sm-12 ">
							<textarea name="notes" class="form-control" placeholder="Note"></textarea>
						</div>
					</div>

				</div>
				<div class="modal-footer">
					<a href="javascript:;" class="btn btn-sm btn-white" data-dismiss="modal">Close</a>
					<input type="submit" class="btn btn-sm btn-success" value="Save">
				</div>
			</form>
		</div>
	</div>
</div>

<!--- Delete Modal -->
<div class="modal fade" id="modal-del">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title">Delete Confirmation</h4>
			</div>
			<form method="post" id="frmDel">
				<div class="modal-body">
					<div class="alert alert-danger m-b-0">
						<input type="hidden" name="id" value="0">
						<h5><i class="fa fa-info-circle"></i>Do you want to delete record ?</h5>

					</div>
				</div>
				<div class="modal-footer">

					<input type='submit' class="btn btn-sm btn-danger" value='Delete' name='delete'>
					<a href="javascript:;" class="btn btn-sm btn-white" data-dismiss="modal">Close</a>
				</div>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
	var grid;
	$(document).ready(function() {
		grid = new Datatable();
		// $('#content').addClass('pending0')
		$(".nav").find('li').removeClass("active");
		$("#page_student1").addClass("active");
		$("#page_user").addClass("active");
		$(".has-sub").removeClass("expand");
		// $(".sub-menu").attr("style", "display:none;");
		$("#page_user").addClass("active expand");
		$(".profile-info").trigger("click");
		grid.init({
			src: $(".ajax-table"),
			onSuccess: function(grid) {
				// execute some code after table records loaded
				$(".filter").appendTo(".table-toolbar");
				$("[name=search]:eq(0)").focus();
			},
			onError: function(grid) {
				// execute some code on network or other general error  
			},
			dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options 
				/* 
					By default the ajax datatable's layout is horizontally scrollable and this can cause an issue of dropdown menu is used in the table rows which.
					Use below "sDom" value for the datatable layout if you want to have a dropdown menu for each row in the datatable. But this disables the horizontal scroll. 
				*/
				//"sDom" : "<'row'<'col-md-8 col-sm-12'pl><'col-md-4 col-sm-12 text-right'i<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pl><'col-md-4 col-sm-12 text-right'i>r>>", 
				"sDom": "<'table-toolbar'>t<'row'<'col-md-8 col-sm-12'pl><'col-md-4 col-sm-12 text-right'i>r>>",
				"aLengthMenu": [
					[200, 500, 1000, 1500, 2000],
					[200, 500, 1000, 1500, 2000] // change per page values here
				],
				"oLanguage": { // language settings
					"sProcessing": '<fa class="fa fa-spin fa-spinner"></fa> Loading...',
				},
				"iDisplayLength": 200, // default record count per page
				"bServerSide": true, // server side processing
				"sAjaxSource": "scripts/php/student_master/info_ajax.php?uid=<?php echo $id; ?>", // ajax source
				"aaSorting": [
					[1, "asc"]
				], // set first column as a default sort by asc
				"aoColumns": [{
						"sName": "select",
						"bVisible": false
					},
					{
						"sName": "id",
						"bSortable": true,
						"sWidth": "2%"
					},
					{
						"sName": "name",
						"sWidth": "15%"
					},
					{
						"sName": "name",
						"sWidth": "8%"
					},
					{
						"sName": "name",
						"sWidth": "12%"
					},
					{
						"sName": "name",
						"sWidth": "10%"
					},
					{
						"sName": "image",
						"sWidth": "10%"
					},
					{
						"sName": "image",
						"sWidth": "10%"
					},
					{
						"sName": "image",
						"sWidth": "10%"
					},
					{
						"sName": "image",
						"sWidth": "20%"
					},
					{
						"sName": "status",
						"sWidth": "8%"
					},
					{
						"sName": "status",
						"sWidth": "8%"
					},
				],
				"fnFooterCallback": function(nRow, aaData, iStart, iEnd, aiDisplay) {
					//var nCells = nRow.getElementsByTagName('th');
					//nCells[1].innerHTML=ajaxTotal.total_qty;
				},
				"fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
					$(nRow).data("row", aData[0]);
					$(nRow).find(".delete").click(function(e) {
						e.stopPropagation();
						var id = aData[0];
						$("input[name='id']").val(id);
						$("#modal-del").modal("show");
					});
					$(nRow).click(function() {
						var r = $(this).data("row");
						// redirect("#gallery_details.php?id=" + r);
					});
					$(nRow).find(".edit").click(function(e) {
						e.stopPropagation();
						var row = $(this).data('i');
						var id = aData[0];
						$("#frmEdit input[name='school_name']").val(row.school_name);
						$("#frmEdit input[name='school_address']").val(row.school_address);
						$("#frmEdit input[name='school_mobile']").val(row.school_mobile);
						$("#frmEdit input[name='total_fees']").val(row.total_fees);
						$("#frmEdit input[name='scholarship_fees']").val(row.scholarship_fees);
						$("#frmEdit input[name='donation_fees']").val(row.donation_fees);
						$("#frmEdit input[name='parents_fees']").val(row.parents_fees);
						$("#frmEdit input[name='id']").val(row.id);
						$(".std_id  option[value='" + row.std_id + "']").attr("selected", "selected");
						$(".donation_id  option[value='" + row.donation_id + "']").attr("selected", "selected");
						$("#modal-edit textarea[name='notes']").val(row.notes);


						$("#modal-edit").modal("show");
					});
				}
			}
		});

		$('.add').click(function() {
			$("[name=cat_name]").val("");
			$('#frmAdd')[0].reset();
			$("#i1").attr("src", '');
		});
		$('.edit').click(function() {
			var row = $(this).data('i');
			$("#frmEdit input[name='school_name']").val(row.school_name);
			$("#frmEdit input[name='school_address']").val(row.school_address);
			$("#frmEdit input[name='school_mobile']").val(row.school_mobile);
			$("#frmEdit input[name='total_fees']").val(row.total_fees);
			$("#frmEdit input[name='scholarship_fees']").val(row.scholarship_fees);
			$("#frmEdit input[name='donation_fees']").val(row.donation_fees);
			$("#frmEdit input[name='id']").val(row.id);
			$(".std_id  option[value='" + row.std_id + "']").attr("selected", "selected");
			$(".student_id  option[value='" + row.student_id + "']").attr("selected", "selected");
			// $("#modal-add-bank").modal("show");
		});

		$(".form-filter").change(function() {
			$(".filter-submit").trigger("click");
		});

		var search_timeout = null;
		$("[name=search]").keyup(function() {
			var self = this;
			if (search_timeout) {
				clearTimeout(search_timeout);
				search_timeout = null;
			}
			search_timeout = setTimeout(function() {
				$(".filter-submit").trigger("click");
			}, 500);
		});
		$("input[type=file]").change(function() {
			var formData = new FormData();
			formData.append('section', 'general');
			formData.append('action', 'previewImg');
			formData.append('file', $(this)[0].files[0]);
			var field = $(this).data('field');
			var img = $(this).data('img');
			//$('#loading').html('<img src="../images/loader.gif"> loading...');
			$.ajax({
				url: siteURL + "scripts/php/upload_photo.php",
				type: 'POST',
				dataType: 'json',
				processData: false,
				contentType: false,
				data: formData,
				success: function(data) {
					$('#loading').html(data);
					console.log(data);
					if (data.success) {
						$(field).val(data.name);
						$(img).prop("src", data.url);
					} else {
						console.log(data.error);
					}
				},
				error: function(r) {
					console.log("Error occured");
				},
			});
		});
		/* Add Category */
		$("#frmAdd").submit(function(e) {
			e.preventDefault();
			$('#frmAdd .btn-success').attr("disabled", true);
			$.post(siteURL + "scripts/php/student_master/info_add.php", $(this).serialize(), function(data) {
				notify(data);
				$('#frmAdd .btn-success').attr("disabled", false);
				if (data.success) {
					$("#modal-add").modal("hide");
					$(".filter-submit").trigger("click");
					$('#frmAdd')[0].reset();
				} else {

				}
			});
		});

		/* Edit Category */
		$("#frmEdit").submit(function(e) {
			e.preventDefault();
			$.post(siteURL + "scripts/php/student_master/info_edit.php", $(this).serialize(), function(data) {
				notify(data);
				if (data.success) {
					$("#modal-edit").modal("hide");
					$(".filter-submit").trigger("click");
					$('#frmEdit')[0].reset();
				} else {

				}
			});
		});

		/* Delete Category */
		$("#frmDel").submit(function(e) {
			e.preventDefault();
			$.post(siteURL + "scripts/php/student_master/info_del.php", $(this).serialize(), function(data) {
				notify(data);
				if (data.success) {
					$("#modal-del").modal("hide");
					$(".filter-submit").trigger("click");
				} else {

				}
			});
		});

		$('.scholarship_fees').on('blur', function() {
			var total_fees = $('.total_fees').val();
			var scholarship_fees = $('.scholarship_fees').val();
			var TF = total_fees - scholarship_fees;
			$('.donation_fees').val(TF);
			//parents_fees
		});
		$('.parents_fees').on('blur', function() {
			var total_fees = $('.total_fees').val();
			var parents_fees = $('.parents_fees').val();
			var scholarship_fees = $('.scholarship_fees').val();
			var TF = total_fees - scholarship_fees;
			TF = TF - parents_fees;
			$('.donation_fees').val(TF);
		});

		$('.scholarship_fees1').on('blur', function() {
			var total_fees = $('.total_fees1').val();
			var scholarship_fees = $('.scholarship_fees1').val();
			var TF = total_fees - scholarship_fees;
			$('.donation_fees1').val(TF);
			//parents_fees
		});
		$('.parents_fees1').on('blur', function() {
			var total_fees = $('.total_fees1').val();
			var parents_fees = $('.parents_fees1').val();
			var scholarship_fees = $('.scholarship_fees1').val();
			var TF = total_fees - scholarship_fees;
			TF = TF - parents_fees;
			$('.donation_fees1').val(TF);
		});
	});
</script>