<?php
include_once("../scripts/db.php");
include_once("../scripts/functions.php");
db_connect();
if (!empty($_SESSION['access']) && $_SESSION['access'] != "") {
	$userId = $_SESSION['access']['uid'];
	// if (!menuRights($id,'view')) {
	// 	echo "<script type='text/javascript'> document.location = 'index.php'; </script>";
	// }
	$userINFO = getRow("SELECT *
FROM user 
WHERE uid=:id", array('id' => $userId));
}



$uid = $_REQUEST['id'];
$params = array('uid' => base64_decode($uid));
db_connect();
$user = getRow("SELECT u.*,d.name dist_name,tm.name city_name,bm.name business_name,v.name village_name,k.name karkidi_name,s.name surname
	FROM  user u
	LEFT JOIN district_master d ON d.id=u.dist_id
	LEFT JOIN taluko_master tm ON tm.id=u.taluko_id
	LEFT JOIN village_master v ON v.id=u.village_id
	LEFT JOIN surname s ON s.id=u.surname_id
	LEFT JOIN business_master bm ON bm.id=u.business
	LEFT JOIN karkidi_master k ON k.id=u.karkidi_id
	WHERE  uid=:uid and u.status !='-1'", $params);
?>
<div id="content">
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li>
			<a href="javascript:;">Home</a>
		</li>
		<li class="active">User Details</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header"><?php echo $user['middle_name'] . " " . $user['surname']; ?> </h1>
	<!-- end page-header -->
	<!-- begin row -->
	<div class="row">
		<!-- begin col-12 -->
		<div class="col-md-12 ui-sortable">
			<!-- begin panel -->


			<div class="panel panel-inverse panel-with-tabs" data-sortable-id="ui-unlimited-tabs-1" data-init="true">
				<div class="panel-heading p-0">
					<div class="panel-heading-btn m-r-10 m-t-10">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-expand"><i class="fa fa-expand"></i></a>
					</div>
					<!-- begin nav-tabs -->
					<div class="tab-overflow overflow-right">
						<ul class="nav nav-tabs nav-tabs-inverse">
							<li class="prev-button"><a href="javascript:;" data-click="prev-tab" class="text-success"><i class="fa fa-arrow-left"></i></a></li>
							<li class="active"><a href="#nav-tab-1" data-toggle="tab"><?php echo " &nbsp;&nbsp;&nbsp;" . $user['middle_name'] . "  &nbsp;" . $user['surname']; ?></a></li>
							<!-- <li class=""><a href="#nav-tab-2" data-toggle="tab">પરિવાર</a></li> -->
						</ul>
					</div>
				</div>
				<style>
					.ext_padding {
						padding: 8px;
					}
				</style>
				<div class="tab-content">
					<div class="tab-pane fade  active in" id="nav-tab-1">
						<h3 class="m-t-10"><?php echo " &nbsp;&nbsp;&nbsp;" . $user['middle_name'] . "&nbsp;" . $user['surname']; ?> Details</h3>

						<form class="form-horizontal form-bordered">
							<div class="form-group row">
								<label class="col-lg-12 col-form-label">
									<a href='<?php echo getFullImage($user['profile_pic']) ?>' data-lightbox='gallery-group-1'><img src="<?php echo getFullImage($user['profile_pic']) ?>" style="max-height: 40px;" class='img-thumbnail' alt=""></a>
									<?php echo " &nbsp;&nbsp;&nbsp;" . $user['middle_name'] . " &nbsp;" . $user['father_name'] . "  &nbsp;" . $user['surname']; ?>
								</label>

							</div>
							<div class="form-group row">
								<div class="col-lg-4">
									<label class="col-form-label">મોબાઈલ : <?php echo $user['mobile']; ?></label>
								</div>
								<div class="col-lg-4">
									<label class="col-form-label">ઇ-મેઇલ : <?php echo $user['email']; ?></label>
								</div>
								<div class="col-lg-4">
									<label class="col-form-label">કારકિર્દી : <?php echo $user['karkidi_name']; ?></label>
								</div>
							</div>
							<div class="form-group row">
								<div class="col-lg-4">
									<label class="col-form-label">સરનામું : <?php echo $user['address']; ?>, ગામ: <?php echo $user['village_name']; ?>, તાલુકો: <?php echo $user['city_name']; ?>, જિલ્લો : <?php echo $user['dist_name']; ?></label>
								</div>
							</div>
							<div class="form-group row">
								<div class="col-lg-4">
									<label class="col-form-label">બ્લડ ગ્રુપ : <?php echo $user['blood_group']; ?></label>
								</div>
								<div class="col-lg-4">
									<label class="col-form-label">અભ્યાસ : <?php echo $user['qualification']; ?></label>
								</div>
								<div class="col-lg-4">
									<label class="col-form-label">વ્યવસાય : <?php echo $user['business_name']; ?></label>
									<label class="col-form-label">વ્યવસાય સરનામું : <?php echo $user['business_address']; ?></label>
								</div>
							</div>


							<div class="form-group row">
								<div class="col-lg-4">
									<label class="col-form-label">જન્મ તારીખ : <?php if ($user['dob'] != '0000-00-00') {
																					echo date('d/m/Y', strtotime($user['dob']));
																				} ?></label>
								</div>
								<div class="col-lg-4 panddingRow-15">
									<label class="col-form-label"><?php if ($user['gender'] == '0') {
																		echo "સ્ત્રી";
																	} else {
																		echo "પુરુષ";
																	} ?></label>
								</div>
								<div class="col-lg-4">
									<?php
									if ($userINFO['type'] == 'management') {
										if ($user['type'] == 'admin') {
									?>
											<input type="button" class="btn btn-sm btn-success removeAdmin" value="Remove Admin">
										<?php  } else { ?>
											<input type="button" class="btn btn-sm btn-success createAdmin" value="Create Admin">
									<?php }
									} ?>
									<!-- <label class="col-form-label">કારકિર્દી : <?php echo $user['karkidi_name']; ?></label> -->
								</div>
							</div>
						</form>
						<div class="clearfix"></div>
					</div>

				</div>
			</div>
		</div>
		<!-- end panel -->
	</div>
	<!-- end col-12 -->
</div>
<!-- end row -->
</div>



<script type="text/javascript">
	var grid;
	$(document).ready(function() {
		grid = new Datatable();
		$('.datepicker-autoClose').datepicker({
			// format: 'DD/MM/YYYY',
			todayHighlight: true,
			autoclose: true
		});
		$("#sidebar .nav").find('li').removeClass("active");
		$("#page_user").addClass("active");

		$("#page_user").addClass("active expand");


		$(".form-filter").change(function() {
			$(".filter-submit").trigger("click");
		});

		var search_timeout = null;
		$("[name=search]").keyup(function() {
			var self = this;
			if (search_timeout) {
				clearTimeout(search_timeout);
				search_timeout = null;
			}
			search_timeout = setTimeout(function() {
				$(".filter-submit").trigger("click");
			}, 500);
		});

		$("input[type=file]").change(function() {
			var formData = new FormData();
			formData.append('section', 'general');
			formData.append('action', 'previewImg');
			formData.append('file', $(this)[0].files[0]);
			var field = $(this).data('field');
			var img = $(this).data('img');
			//$('#loading').html('<img src="../images/loader.gif"> loading...');
			$.ajax({
				url: siteURL + "scripts/php/upload_photo.php",
				type: 'POST',
				dataType: 'json',
				processData: false,
				contentType: false,
				data: formData,
				success: function(data) {
					$('#loading').html(data);
					console.log(data);
					if (data.success) {
						$(field).val(data.name);
						$(img).prop("src", data.url);
					} else {
						console.log(data.error);
					}
				},
				error: function(r) {
					console.log("Error occured");
				},
			});
		});
		$(".selectDist").change(function(e) {
			let id = $(this).data('i');
			var value = $(".select_dist" + id + " option:selected").val();
			$(".select_city" + id).empty();
			$(".select_city" + id).append("<option value=''>સિલેક્ટ તાલુકો</option>");
			$.post(siteURL + "scripts/php/web/find_DistToCity.php?id=" + value, function(data) {
				if (data.success) {
					var jsonData1 = data.success;
					if (jsonData1.length > 0) {
						var appenddata1 = "";
						for (var i = 0; i < jsonData1.length; i++) {
							appenddata1 += "<option value = '" + jsonData1[i].id + "'>" + jsonData1[i].name + " </option>";
						}
						$(".select_city" + id).append(appenddata1);
					}
				}
			});
		});

		/* Add Category */
		$("#frmAdd").submit(function(e) {
			e.preventDefault();
			$('#frmAdd .btn-success').attr("disabled", true);
			$.post(siteURL + "scripts/php/member/d_add.php", $(this).serialize(), function(data) {
				notify(data);
				$('#frmAdd .btn-success').attr("disabled", false);
				if (data.success) {
					$("#modal-add").modal("hide");
					$(".filter-submit").trigger("click");
					$('#frmAdd')[0].reset();
					location.reload();
				} else {

				}
			});
		});

		/* Edit Category */
		$("#frmEdit").submit(function(e) {
			e.preventDefault();
			$.post(siteURL + "scripts/php/member/d_edit.php", $(this).serialize(), function(data) {
				notify(data);
				if (data.success) {
					$("#modal-edit").modal("hide");
					$(".filter-submit").trigger("click");
					$('#frmEdit')[0].reset();
					location.reload();
				} else {

				}
			});
		});

		/* Delete Category */
		$("#frmDel").submit(function(e) {
			e.preventDefault();
			$.post(siteURL + "scripts/php/member/d_del.php", $(this).serialize(), function(data) {
				notify(data);
				if (data.success) {
					$("#modal-del").modal("hide");
					$(".filter-submit").trigger("click");
					location.reload();
				} else {

				}
			});
		});
		$(".removeAdmin").click(function(e) {
			e.preventDefault();
			$.post(siteURL + "scripts/php/member/member_action.php?id=<?php echo $user['uid'] ?>&status=user", function(data) {
				notify(data);
				if (data.success) {
					location.reload();
				} else {

				}
			});
		});
		$(".createAdmin").click(function(e) {
			e.preventDefault();
			$.post(siteURL + "scripts/php/member/member_action.php?id=<?php echo $user['uid'] ?>&status=admin", function(data) {
				notify(data);
				if (data.success) {
					location.reload();
				} else {

				}
			});
		});
		$(".selectBus").change(function(e) {
			let id = $(this).data('i');
			var value = $(".select_bus" + id + "  option:selected").val();
			$(".gov_job" + id).removeClass('hide')
			$("input[name='gov_job']").val('');

			if (value == '27' || value == 27) {
				$(".gov_job" + id).removeClass('hide')
			} else {
				$(".gov_job" + id).addClass('hide')
			}
		});


		$(".edit").click(function(e) {
			e.stopPropagation();
			var row = $(this).data('i');
			$('#frmEdit')[0].reset();

			$("#modal-edit input[name='id']").val(row.uid);
			$("#modal-edit input[name='surname']").val(row.surname);
			$("#modal-edit input[name='middle_name']").val(row.middle_name);
			$("#modal-edit input[name='father_name']").val(row.father_name);
			$("#modal-edit input[name='gov_job']").val(row.gov_job);
			$("#modal-edit input[name='dob']").val(row.dob);
			$("#modal-edit input[name='mobile']").val(row.mobile);
			$("#modal-edit input[name='mobile_1']").val(row.mobile_1);
			$("#modal-edit input[name='email']").val(row.email);
			$("#modal-edit textarea[name='address']").val(row.address);
			$("#modal-edit input[name=gender]").removeAttr('checked');
			$("#modal-edit input[name=gender][value=" + row.gender + "]").prop('checked', true);

			$("#i2").attr("src", row.profile_pic);
			$(".select_dist2  option[value='" + row.dist_id + "']").attr("selected", "selected");
			$(".blood_group  option[value='" + row.blood_group + "']").attr("selected", "selected");
			$(".select_bus2  option[value='" + row.business + "']").attr("selected", "selected");
			$(".select_barot2  option[value='" + row.barot_id + "']").attr("selected", "selected");
			$(".select_qui2  option[value='" + row.qualification + "']").attr("selected", "selected");
			$(".select_temp2  option[value='" + row.temple_id + "']").attr("selected", "selected");
			$(".select_dada2  option[value='" + row.dada_id + "']").attr("selected", "selected");
			$(".relationship  option[value='" + row.relationship + "']").attr("selected", "selected");
			$('.select_dist2').trigger('change');
			setTimeout(function() {
				$(".select_city2  option[value='" + row.taluko_id + "']").attr("selected", "selected");
			}, 1000);
			$("#modal-edit").modal("show");
		});
		$(".delete").click(function(e) {
			e.stopPropagation();
			var row = $(this).data('i');
			$("input[name='id']").val(row.uid);
			$("#modal-del").modal("show");
		});
	});
</script>