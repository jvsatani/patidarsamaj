<?php
include_once("../scripts/db.php");
include_once("../scripts/functions.php");
db_connect();

$f_id = '';
if (!empty($_GET['f_id'])) {
	$f_id = $_GET['f_id'];
}
$uid = '';
if (!empty($_GET['uid'])) {
	$uid = $_GET['uid'];
}
$fund_id = '';
if (!empty($_GET['fund_id'])) {
	$fund_id = $_GET['fund_id'];
}

$date = '';
$sdate = '';
if (!empty($_GET['sdate'])) {
	$sdate = $_GET['sdate'];
}
$edate = '';
if (!empty($_GET['edate'])) {
	$edate = $_GET['edate'];
	$date = $sdate . ' - ' . $edate;
}

$purchase_account = getRow("SELECT *
					FROM purchase_account 
					WHERE status = '1'",array('id' => $_GET['id']));

?>
<div id="content">
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li>
			<a href="javascript:;">Home</a>
		</li>
		<li class="active">Summary</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header"><?php echo $purchase_account['name'];?> </h1>
	<!-- end page-header -->
	<!-- begin row -->
	<div class="row">
		<!-- begin col-12 -->
		<div class="col-md-12">
			<!-- begin panel -->
			<div class="panel panel-inverse" data-sortable-id="table-basic-7">
				<div class="panel-heading">
					<div class="panel-heading-btn">
						</div>
					<h4 class="panel-title"><?php echo $purchase_account['name'];?></h4>
				</div>
				<div class="panel-body">
					<br>
					<br>
					<div class="row">
						<div class="form-group col-md-12 ">
							<label class="col-md-1 control-label">Select Date:</label>
							<div class="col-md-2">
								<input type="text" name="default-daterange" class="form-control" value="<?php echo $date; ?>" placeholder="Select the date range" id="default-daterange" style="    margin-top: -7px;">
							</div>
							<label class="col-md-1 control-label">Select Type:</label>
							<div class="col-md-2">
								<select class="form-control selectFund" style="margin-top: -8px;" data-i='1' name="f_id">
									<option value=''>All</option>
									<option value='cash' <?php if ($f_id == 'cash') {
																echo "Selected";
															} ?>>Cash</option>
									<option value='bank' <?php if ($f_id ==  'bank') {
																echo "Selected";
															} ?>>Bank</option>
								</select>
							</div>
						
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
	
			<div class="panel panel-inverse" data-sortable-id="table-basic-8">
				<div class="panel-body">
					<div class="filter">
						<button class="hidden btn btn-xs btn-default filter-submit margin-bottom"><i class="fa fa-search"></i> Search</button>
					</div>
					<div class="table-container">
						<div class="row">
							<div class="col-md-11">
							</div>
							<div class="col-md-1">
								<a target="_blank" href="scripts/invoice/allKharsh_Print.php?f_id=<?php echo $f_id; ?>&uid=<?php echo $uid; ?>&sdate=<?php echo $sdate; ?>&edate=<?php echo $edate; ?>" class="btn btn-sm btn-success">Print</a>
							</div>
						</div>
						<table class="table table-striped table-bordered table-hover ajax-table rowclick">
							<thead>
								<tr role="row" class="heading">
									<th class="no-sort" width="5%"><input type="checkbox" class="toggle_all" /></th>
									<th>#</th>
									<th>Receipt no</th>
									<th>Paid to</th>
									<th>Note </th>
									<th>By</th>
									<th>Type</th>
									<th>Amount</th>
									<th>Date</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>

					</br>
					</br>
					</br>
				</div>
			</div>

			<!-- end panel -->
		</div>
		<!-- end col-12 -->
	</div>
	<!-- end row -->
</div>


<script type="text/javascript">
	var grid;
	$(document).ready(function() {
		grid = new Datatable();
		var f_id = '<?php echo $f_id; ?>';
		var uid = '<?php echo $uid; ?>';
		var sdate = '<?php echo $sdate; ?>';
		var edate = '<?php echo $edate; ?>';
		$("#page_kharsh").addClass("active");
		$('#default-daterange').daterangepicker({});
		grid.init({
			src: $(".ajax-table"),
			onSuccess: function(grid) {
				// execute some code after table records loaded
				$(".filter").appendTo(".table-toolbar");
				$("[name=search]:eq(0)").focus();
			},
			onError: function(grid) {
				// execute some code on network or other general error  
			},
			dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options 
				/* 
				    By default the ajax datatable's layout is horizontally scrollable and this can cause an issue of dropdown menu is used in the table rows which.
				    Use below "sDom" value for the datatable layout if you want to have a dropdown menu for each row in the datatable. But this disables the horizontal scroll. 
				*/
				//"sDom" : "<'row'<'col-md-8 col-sm-12'pl><'col-md-4 col-sm-12 text-right'i<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pl><'col-md-4 col-sm-12 text-right'i>r>>", 
				"sDom": "<'table-toolbar'>t<'row'<'col-md-8 col-sm-12'pl><'col-md-4 col-sm-12 text-right'i>r>>",
				"aLengthMenu": [
					[400, 500, 1000, 1500, 2000],
					[400, 500, 1000, 1500, 2000] // change per page values here
				],
				"oLanguage": { // language settings
					"sProcessing": '<fa class="fa fa-spin fa-spinner"></fa> Loading...',
				},
				"iDisplayLength": 400, // default record count per page
				"bServerSide": true, // server side processing
				"sAjaxSource": "scripts/php/kharsh/allKharsh_ajax.php?f_id=<?php echo $f_id; ?>&uid=<?php echo $uid; ?>&sdate=<?php echo $sdate; ?>&edate=<?php echo $edate; ?>", // ajax source
				"aaSorting": [
					[1, "asc"]
				], // set first column as a default sort by asc
				"aoColumns": [{
						"sName": "select",
						"bVisible": false
					},
					{
						"sName": "id",
						"bSortable": true,
						"sWidth": "2%"
					},
					{
						"sName": "no",
						"sWidth": "7%"
					},
					{
						"sName": "paid",
						"sWidth": "15%"
					},
					{
						"sName": "notes",
						"sWidth": "20%"
					},

					{
						"sName": "fund",
						"sWidth": "15%"
					},
					{
						"sName": "notes",
						"sWidth": "8%"
					},
					{
						"sName": "name",
						"sWidth": "8%"
					},
					{
						"sName": "date",
						"sWidth": "8%"
					},
					{
						"sName": "date",
						"bVisible": false,
						"sWidth": "8%"
					},
				],
				"fnFooterCallback": function(nRow, aaData, iStart, iEnd, aiDisplay) {
					//var nCells = nRow.getElementsByTagName('th');
					//nCells[1].innerHTML=ajaxTotal.total_qty;
				},
				"fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
					$(nRow).data("row", aData[0]);
					$(nRow).find(".delete").click(function(e) {
						e.stopPropagation();
						var id = $(this).attr('i');
						$("input[name='id']").val(id);
						$("#modal-del").modal("show");
					});
					$(nRow).click(function() {
						var r = $(this).data("row");
						//console.log(r);
						// redirect("#order_info.php?id=" + r);
					});
					$(nRow).find(".edit").click(function(e) {
						e.stopPropagation();
						var row = $(this).data('i');
						$('#frmEdit')[0].reset();
						var id = aData[0];
						var name = aData[2];
						$("#modal-edit input[name='id']").val(id);
						// $("#modal-edit input[name='surname']").val(row.surname);
						// $("#modal-edit input[name='name']").val(row.name);
						$(".selectedUID  option[value='" + row.uid + "']").attr("selected", "selected");

						$("#modal-edit textarea[name='notes']").val(row.notes);

						$("#modal-edit").modal("show");
					});

				}
			}
		});


		$(".form-filter").change(function() {
			$(".filter-submit").trigger("click");
		});

		var search_timeout = null;
		$("[name=search]").keyup(function() {
			var self = this;
			if (search_timeout) {
				clearTimeout(search_timeout);
				search_timeout = null;
			}
			search_timeout = setTimeout(function() {
				$(".filter-submit").trigger("click");
			}, 500);
		});

		$("#frmEdit").submit(function(e) {
			e.preventDefault();
			$.post(siteURL + "scripts/php/kharsh/kharsh_edit.php", $(this).serialize(), function(data) {
				notify(data);
				if (data.success) {
					$("#modal-edit").modal("hide");
					$(".filter-submit").trigger("click");
					$('#frmEdit')[0].reset();
				} else {

				}
			});
		});
		/* Add Category */

		$(".selectFund").change(function(e) {
			var value = $(".selectFund  option:selected").val();
			window.location = "#kharsh.php?f_id=" + value + "&uid=" + uid + "&sdate=" + sdate + "&edate=" + edate;
			location.reload();
		});
		$(".select_uid").change(function(e) {
			var value = $(".select_uid  option:selected").val();
			window.location = "#kharsh.php?f_id=" + f_id + "&uid=" + value + "&sdate=" + sdate + "&edate=" + edate;
			location.reload();
		});
		$(".applyBtn").click(function(e) {
			setTimeout(function() {
				var date = $('#default-daterange').val();
				var test = date.split(' - ')
				window.location = "#kharsh.php?f_id=" + f_id + "&uid=" + uid + "&sdate=" + test[0] + "&edate=" + test[1];
				location.reload();
			}, 500);
		});
		getOrderShortInfo();
		function getOrderShortInfo() {
			debugger
			$.post(siteURL + "scripts/php/kharsh/get_fund_info.php?f_id=" + f_id + "&sdate=" + sdate + "&edate=" + edate+"&uid="+uid, function(data) {
				var data = JSON.parse(data)
				if (data.success) {
					var st = data.success;
					if (st.length > 0) {
						for (var i = 0; i < st.length; i++) {
							$(".cat_" + st[i].id).append("<div class='productBox box'>" +
								"<span style='font-size: 15px;font-weight: bold;'>" + st[i].amount + "</span><br>" +
								"</div>");

						}
					}
				}
			});
		}
	});
</script>