<?php
include_once("../scripts/db.php");
db_connect();
$pageName = 'વ્યક્તિ  વિશેષ';
$district_master = getRows("SELECT *
				  FROM district_master 
				  WHERE status = 1");
?>
<div id="content">
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li>
			<a href="javascript:;">Home</a>
		</li>
		<li class="active"><?php echo $pageName ?></li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header"><?php echo $pageName ?></h1>
	<!-- end page-header -->
	<!-- begin row -->
	<div class="row">
		<!-- begin col-12 -->
		<div class="col-md-12">
			<!-- begin panel -->
			<div class="panel panel-primary" data-sortable-id="table-basic-7">
				<div class="panel-heading">
					<div class="panel-heading-btn">
						<a href='javascript:;' role='button' data-toggle='modal' data-target='#modal-add' class="btn btn-xs btn-success add">Add</a>
					</div>

					<h4 class="panel-title"><?php echo $pageName ?></h4>
				</div>
				<div class="panel-body">
					<div class="filter">
						<div class="row">
							<div class="form-group col-md-12 hide">
								<label class="col-md-1 control-label" style="margin:6px">Search : </label>
								<div class="col-md-7">
									<input type="text" name="search" class="form-filter form-control" placeholder="Search Any..." />
								</div>
							</div>
							<button class="hidden btn btn-xs btn-default filter-submit margin-bottom"><i class="fa fa-search"></i> Search</button>
						</div>
					</div>
					<div class="table-container">
						<table class="table table-striped table-bordered table-hover ajax-table rowclick">
							<thead>
								<tr role="row" class="heading">
									<th class="no-sort" width="5%"><input type="checkbox" class="toggle_all" /></th>
									<th>#</th>
									<th>નામ</th>
									<th>સરનામું</th>
									<th>પરિચય</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<!-- end panel -->
		</div>
		<!-- end col-12 -->
	</div>
	<!-- end row -->
</div>

<!--- Add Modal -->
<div class="modal fade" id="modal-add">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title">Add</h4>
			</div>
			<form method="post" class="form-horizontal" id="frmAdd">
				<input type="hidden" name="action" value="add">
				<input type="hidden" name="type" value="<?php echo $_GET['type']; ?>">
				<div class="modal-body">
					<div class="form-group">
						<label class="col-md-3 control-label">નામ </label>
						<div class="col-md-9">
							<input type="text" name="name" class="form-control" placeholder="નામ ">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">મોબાઈલ</label>
						<div class="col-md-9">
							<input type="text" name="mobile" class="form-control" placeholder="મોબાઈલ">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">સરનામું</label>
						<div class="col-md-9">
							<textarea name="address" class="form-control" placeholder="સરનામું"></textarea>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">ગામ નું નામ</label>
						<div class="col-md-9">
							<input type="text" name="village" class="form-control" placeholder="ગામ નું નામ">
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-md-3 control-label">જિલ્લો સિલેક્ટ કરો</label>
						<div class="col-md-9">
							<select class="form-control selectDist select_dist1" data-i='1' name="dist_id">
								<option value=''>સિલેક્ટ જિલ્લો</option>
								<?php foreach ($district_master as $row) { ?>
									<option value="<?php echo $row['id']; ?>"><?php echo ucfirst($row['name']); ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">તાલુકો સિલેક્ટ કરો</label>
						<div class="col-md-9">
							<select class="form-control selectCity select_city1" data-i='1' name="city_id">
								<option value=''>સિલેક્ટ તાલુકો</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">જન્મ તારીખ</label>
						<div class="col-md-9">
							<input type="text" name="dob" class="form-control" placeholder="જન્મ તારીખ">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">પરિચય</label>
						<div class="col-md-9">
						<textarea name="title" class="form-control" placeholder="પરિચય" style="height: 100px;"></textarea>
							</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">વિશેષ પરિચય</label>
						<div class="col-md-9">
							<textarea name="notes" class="form-control" placeholder="વિશેષ પરિચય" style="height: 100px;"></textarea>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">પ્રોફાઈલ ફોટો</label>
						<div class="col-md-9">
							<img id="i1" src="" style="max-height: 100px;" />
							<input data-field="#a1" data-img="#i1" type="file" name="file" class="form-control" />
							<input id="a1" type="hidden" name="image_1" value="" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">ફોટો અપલોડ કરો 1</label>
						<div class="col-md-9">
							<img id="i2" src="" style="max-height: 100px;" />
							<input data-field="#a2" data-img="#i2" type="file" name="file" class="form-control" />
							<input id="a2" type="hidden" name="image_2" value="" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">ફોટો અપલોડ કરો 2</label>
						<div class="col-md-9">
							<img id="i3" src="" style="max-height: 100px;" />
							<input data-field="#a3" data-img="#i3" type="file" name="file" class="form-control" />
							<input id="a3" type="hidden" name="image_3" value="" />
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-3 control-label">ફોટો અપલોડ કરો 3</label>
						<div class="col-md-9">
							<img id="i4" src="" style="max-height: 100px;" />
							<input data-field="#a4" data-img="#i4" type="file" name="file" class="form-control" />
							<input id="a4" type="hidden" name="image_4" value="" />
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<a href="javascript:;" class="btn btn-sm btn-white" data-dismiss="modal">Close</a>
					<input type="submit" class="btn btn-sm btn-success" value="Save">
				</div>
			</form>
		</div>
	</div>
</div>

<!--- Edit Modal -->
<div class="modal fade" id="modal-edit">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title">Edit</h4>
			</div>
			<form method="post" class="form-horizontal" id="frmEdit">
				<input type="hidden" name="action" value="edit">
				<input type="hidden" name="type" value="<?php echo $_GET['type']; ?>">
				<input type="hidden" name="id" value="0">
				<div class="modal-body">
				<div class="form-group">
						<label class="col-md-3 control-label">નામ </label>
						<div class="col-md-9">
							<input type="text" name="name" class="form-control" placeholder="નામ ">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">મોબાઈલ</label>
						<div class="col-md-9">
							<input type="text" name="mobile" class="form-control" placeholder="મોબાઈલ">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">સરનામું</label>
						<div class="col-md-9">
							<textarea name="address" class="form-control" placeholder="સરનામું"></textarea>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">ગામ નું નામ</label>
						<div class="col-md-9">
							<input type="text" name="village" class="form-control" placeholder="ગામ નું નામ">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">જિલ્લો સિલેક્ટ કરો</label>
						<div class="col-md-9">
							<select class="form-control selectDist select_dist2" data-i='2' name="dist_id">
								<option value=''>સિલેક્ટ જિલ્લો</option>
								<?php foreach ($district_master as $row) { ?>
									<option value="<?php echo $row['id']; ?>"><?php echo ucfirst($row['name']); ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">તાલુકો સિલેક્ટ કરો</label>
						<div class="col-md-9">
							<select class="form-control selectCity select_city2" data-i='2' name="city_id">
								<option value=''>સિલેક્ટ તાલુકો</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">જન્મ તારીખ</label>
						<div class="col-md-9">
							<input type="text" name="dob" class="form-control" placeholder="જન્મ તારીખ">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">પરિચય</label>
						<div class="col-md-9">
						<textarea name="title" class="form-control" placeholder="પરિચય" style="height: 100px;"></textarea>
							</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">વિશેષ પરિચય</label>
						<div class="col-md-9">
							<textarea name="notes" class="form-control" placeholder="વિશેષ પરિચય" style="height: 100px;"></textarea>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">પ્રોફાઈલ ફોટો</label>
						<div class="col-md-9">
							<img id="i5" src="" style="max-height: 100px;" />
							<input data-field="#a5" data-img="#i5" type="file" name="file" class="form-control" />
							<input id="a5" type="hidden" name="image_1" value="" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">ફોટો અપલોડ કરો 1</label>
						<div class="col-md-9">
							<img id="i6" src="" style="max-height: 100px;" />
							<input data-field="#a6" data-img="#i6" type="file" name="file" class="form-control" />
							<input id="a6" type="hidden" name="image_2" value="" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">ફોટો અપલોડ કરો 2</label>
						<div class="col-md-9">
							<img id="i7" src="" style="max-height: 100px;" />
							<input data-field="#a7" data-img="#i7" type="file" name="file" class="form-control" />
							<input id="a7" type="hidden" name="image_3" value="" />
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-3 control-label">ફોટો અપલોડ કરો 3</label>
						<div class="col-md-9">
							<img id="i8" src="" style="max-height: 100px;" />
							<input data-field="#a8" data-img="#i8" type="file" name="file" class="form-control" />
							<input id="a8" type="hidden" name="image_4" value="" />
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<a href="javascript:;" class="btn btn-sm btn-white" data-dismiss="modal">Close</a>
					<input type="submit" class="btn btn-sm btn-success" value="Save">
				</div>
			</form>
		</div>
	</div>
</div>

<!--- Delete Modal -->
<div class="modal fade" id="modal-del">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title">Delete Confirmation</h4>
			</div>
			<form method="post" id="frmDel">
				<div class="modal-body">
					<div class="alert alert-danger m-b-0">
						<input type="hidden" name="id" value="0">
						<h5><i class="fa fa-info-circle"></i>Do you want to delete record ?</h5>

					</div>
				</div>
				<div class="modal-footer">

					<input type='submit' class="btn btn-sm btn-danger" value='Delete' name='delete'>
					<a href="javascript:;" class="btn btn-sm btn-white" data-dismiss="modal">Close</a>
				</div>
			</form>
		</div>
	</div>
</div>

<script type="text/javascript">
	var grid;
	$(document).ready(function() {
		grid = new Datatable();
		$(".nav").find('li').removeClass("active");
		$("#page_vaykti").addClass("active");
		$(".has-sub").removeClass("expand");
		$(".sub-menu").attr("style", "display:none;");
		grid.init({
			src: $(".ajax-table"),
			onSuccess: function(grid) {
				// execute some code after table records loaded
				$(".filter").appendTo(".table-toolbar");
				$("[name=search]:eq(0)").focus();
			},
			onError: function(grid) {
				// execute some code on network or other general error  
			},
			dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options 
				/* 
					By default the ajax datatable's layout is horizontally scrollable and this can cause an issue of dropdown menu is used in the table rows which.
					Use below "sDom" value for the datatable layout if you want to have a dropdown menu for each row in the datatable. But this disables the horizontal scroll. 
				*/
				//"sDom" : "<'row'<'col-md-8 col-sm-12'pl><'col-md-4 col-sm-12 text-right'i<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pl><'col-md-4 col-sm-12 text-right'i>r>>", 
				"sDom": "<'table-toolbar'>t<'row'<'col-md-8 col-sm-12'pl><'col-md-4 col-sm-12 text-right'i>r>>",
				"aLengthMenu": [
					[1000, 2000, 3000, 4000, 5000],
					[1000, 2000, 3000, 4000, 5000] // change per page values here
				],
				"oLanguage": { // language settings
					"sProcessing": '<fa class="fa fa-spin fa-spinner"></fa> Loading...',
				},
				"iDisplayLength": 1000, // default record count per page
				"bServerSide": true, // server side processing
				"sAjaxSource": "scripts/php/vaykti/data.php", // ajax source
				"aaSorting": [
					[1, "desc"]
				], // set first column as a default sort by asc
				"aoColumns": [{
						"sName": "select",
						"bVisible": false
					},
					{
						"sName": "id",
						"bSortable": true,
						"sWidth": "2%"
					},
					{
						"sName": "name",
						"sWidth": "8%"
					},
					{
						"sName": "mobile",
						"sWidth": "10%"
					},
					{
						"sName": "address",
						"sWidth": "5%"
					},
				
					{
						"sName": "status",
						"sWidth": "5%"
					},
				],
				"fnFooterCallback": function(nRow, aaData, iStart, iEnd, aiDisplay) {
					//var nCells = nRow.getElementsByTagName('th');
					//nCells[1].innerHTML=ajaxTotal.total_qty;
				},
				"fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
					$(nRow).data("row", aData[0]);
					$(nRow).find(".delete").click(function(e) {
						e.stopPropagation();
						var id = aData[0];
						$("input[name='id']").val(id);
						$("#modal-del").modal("show");
					});
					$(nRow).click(function() {
						var r = $(this).data("row");
						//console.log(r);
						// redirect("#product.php?id=" + r);
					});
					$(nRow).find(".edit").click(function(e) {
						e.stopPropagation();
						var row = $(this).data('i');
						var id = aData[0];
						var name = aData[2];
						$("#modal-edit input[name='id']").val(id);
						$("#modal-edit input[name='name']").val(row.name);
						$("#modal-edit input[name='mobile']").val(row.mobile);
						$("#modal-edit input[name='village']").val(row.village);
						$("#modal-edit input[name='dob']").val(row.dob);
						$("#modal-edit textarea[name='address']").val(row.address);
						$("#modal-edit textarea[name='notes']").val(row.notes);
						$("#modal-edit textarea[name='title']").val(row.title);
						$("#i5").attr("src", row.image_1);
						$("#i6").attr("src", row.image_2);
						$("#i7").attr("src", row.image_3);
						$("#i8").attr("src", row.image_4);
						$(".select_dist2  option[value='" + row.dist_id + "']").attr("selected", "selected");
						$('.select_dist2').trigger('change');
						setTimeout(function() {
							$(".select_city2  option[value='" + row.city_id + "']").attr("selected", "selected");
						}, 1000);
						$("#modal-edit").modal("show");
					});


				}
			}
		});

		$('.add').click(function() {
			$("[name=name]").val("");
			$('#frmAdd')[0].reset();
		});

		$(".form-filter").change(function() {
			$(".filter-submit").trigger("click");
		});

		var search_timeout = null;
		$("[name=search]").keyup(function() {
			var self = this;
			if (search_timeout) {
				clearTimeout(search_timeout);
				search_timeout = null;
			}
			search_timeout = setTimeout(function() {
				$(".filter-submit").trigger("click");
			}, 500);
		});
		$("input[type=file]").change(function() {
			var formData = new FormData();
			formData.append('section', 'general');
			formData.append('action', 'previewImg');
			formData.append('file', $(this)[0].files[0]);
			var field = $(this).data('field');
			var img = $(this).data('img');
			//$('#loading').html('<img src="../images/loader.gif"> loading...');
			$.ajax({
				url: siteURL + "scripts/php/upload_photo.php",
				type: 'POST',
				dataType: 'json',
				processData: false,
				contentType: false,
				data: formData,
				success: function(data) {
					$('#loading').html(data);
					console.log(data);
					if (data.success) {
						$(field).val(data.name);
						$(img).prop("src", data.url);
					} else {
						console.log(data.error);
					}
				},
				error: function(r) {
					console.log("Error occured");
				},
			});
		});

		$(".selectDist").change(function(e) {
			let id = $(this).data('i');
			var value = $(".select_dist" + id + " option:selected").val();
			$(".select_city" + id).empty();
			$(".select_city" + id).append("<option value=''>સિલેક્ટ તાલુકો</option>");
			$.post(siteURL + "scripts/php/web/find_DistToCity.php?id=" + value, function(data) {
				if (data.success) {
					var jsonData1 = data.success;
					if (jsonData1.length > 0) {
						var appenddata1 = "";
						for (var i = 0; i < jsonData1.length; i++) {
							appenddata1 += "<option value = '" + jsonData1[i].id + "'>" + jsonData1[i].name + " </option>";
						}
						$(".select_city" + id).append(appenddata1);
					}
				}
			});
		});

		/* Add Category */
		$("#frmAdd").submit(function(e) {
			e.preventDefault();
			$('#frmAdd .btn-success').attr("disabled", true);
			$.post(siteURL + "scripts/php/vaykti/add.php", $(this).serialize(), function(data) {
				notify(data);
				$('#frmAdd .btn-success').attr("disabled", false);
				if (data.success) {
					$("#modal-add").modal("hide");
					$(".filter-submit").trigger("click");
					$('#frmAdd')[0].reset();
				} else {

				}
			});
		});

		/* Edit Category */
		$("#frmEdit").submit(function(e) {
			e.preventDefault();
			$.post(siteURL + "scripts/php/vaykti/edit.php", $(this).serialize(), function(data) {
				notify(data);
				if (data.success) {
					$("#modal-edit").modal("hide");
					$(".filter-submit").trigger("click");
					$('#frmEdit')[0].reset();
				} else {

				}
			});
		});

		/* Delete Category */
		$("#frmDel").submit(function(e) {
			e.preventDefault();
			$.post(siteURL + "scripts/php/vaykti/del.php", $(this).serialize(), function(data) {
				notify(data);
				if (data.success) {
					$("#modal-del").modal("hide");
					$(".filter-submit").trigger("click");
				} else {

				}
			});
		});

	});
</script>