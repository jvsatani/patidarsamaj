<?php
include_once("../scripts/db.php");
include_once("../scripts/functions.php");
db_connect();

$f_id = '';
if (!empty($_GET['f_id'])) {
	$f_id = $_GET['f_id'];
}
$r_id = '';
if (!empty($_GET['r_id'])) {
	$r_id = $_GET['r_id'];
}
$emp_id = '';
if (!empty($_GET['emp_id'])) {
	$emp_id = $_GET['emp_id'];
}
$date = '';
$sdate = '';
if (!empty($_GET['sdate'])) {
	$sdate = $_GET['sdate'];
}
$edate = '';
if (!empty($_GET['edate'])) {
	$edate = $_GET['edate'];
	$date = $sdate . ' - ' . $edate;
}

$fund_type = getRows("SELECT *
			FROM fund_type
			WHERE status = 1 ");

$users = getRows("SELECT *,CONCAT(u.`middle_name`,' ',u.`father_name`,' ', IFNULL(s.`name`,'')) full_name
			FROM user u
			LEFT JOIN surname s ON s.id=u.surname_id
			WHERE u.status = '2' and (u.type='management' or u.type='admin' or u.type='city_admin')  ORDER BY u.`middle_name` ASC");

$All_Member = getRows("SELECT *,CONCAT(u.`middle_name`,' ',u.`father_name`,' ', IFNULL(s.`name`,'')) full_name
			FROM user u
			LEFT JOIN surname s ON s.id=u.surname_id
			WHERE u.status = '2' ORDER BY u.`middle_name` ASC");

?>
<div id="content">
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li>
			<a href="javascript:;">Home</a>
		</li>
		<li class="active">ફાળો</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header">ફાળો </h1>
	<!-- end page-header -->
	<!-- begin row -->
	<div class="row">
		<!-- begin col-12 -->
		<div class="col-md-12">
			<!-- begin panel -->
			<div class="panel panel-inverse" data-sortable-id="table-basic-7">
				<div class="panel-heading">
					<h4 class="panel-title">ફાળો</h4>
				</div>
				<div class="panel-body">
					<br>
					<div class="row">
						<div class="form-group col-md-12 ">
							<label class="col-md-1 control-label">Select Date:</label>
							<div class="col-md-2">
								<input type="text" name="default-daterange" class="form-control" value="<?php echo $date; ?>" placeholder="Select the date range" id="default-daterange" style="    margin-top: -7px;">
							</div>
							<label class="col-md-1 control-label">Select Fund Type:</label>
							<div class="col-md-2">
								<select class="form-control selectFund" style="margin-top: -8px;" data-i='1' name="f_id">
									<option value=''>All Fund Type</option>
									<?php foreach ($fund_type as $row) { ?>
										<option value="<?php echo $row['id']; ?>" <?php if ($f_id == $row['id']) {
																						echo "Selected";
																					} ?>><?php echo $row['name']; ?></option>
									<?php } ?>
								</select>
							</div>

							<label class="col-md-1 control-label">ફંડ ઉઘરાવનાર:</label>
							<div class="col-md-2">
								<select class="form-control selectUser" style="margin-top: -8px;" data-i='1' name="r_id">
									<option value=''>All ફંડ ઉઘરાવનાર</option>
									<?php foreach ($users as $row) { ?>
										<option value="<?php echo $row['uid']; ?>" <?php if ($r_id == $row['uid']) {
																						echo "Selected";
																					} ?>><?php echo $row['full_name']; ?></option>
									<?php } ?>
								</select>
							</div>

							<label class="col-md-1 control-label">All Member:</label>
							<div class="col-md-2">
								<select class="form-control selectEMP" style="margin-top: -8px;" name="emp_id">
									<option value=''>All Member</option>
									<?php foreach ($All_Member as $row) { ?>
										<option value="<?php echo $row['uid']; ?>" <?php if ($emp_id == $row['uid']) {
																						echo "Selected";
																					} ?>><?php echo $row['full_name']; ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<!-- <div class="btn-group pull-right">
				<form action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="post">
					<button type="submit" id="export_data" name='export_data' value="Export to excel" class="btn btn-info">Export to excel</button>
				</form>
			</div> -->
			<div class="panel panel-inverse" data-sortable-id="table-basic-7">
				<div class="panel-body">
					<div class="row">
						<div class="row">
							<div class="col-md-11">
								<h1 class="page-header" style="margin: 0px 10px 5px;">Summary</h1>
							</div>
							<div class="col-md-1">
								<a target="_blank" href="scripts/invoice/FundSummary_Print.php?f_id=<?php echo $f_id; ?>&r_id=<?php echo $r_id; ?>&emp_id=<?php echo $emp_id; ?>&sdate=<?php echo $sdate; ?>&edate=<?php echo $edate; ?>" class="btn btn-sm btn-green">Print</a>
							</div>
						</div>
						<?php foreach ($fund_type as $row) {
						?>
							<div class="col-md-3  cat_<?php echo $row['id'] ?>">
								<div class="categoryName">
									<span><?php echo $row['name'] ?> </span>
								</div>
							</div>
						<?php } ?>
						<div class="col-md-3  total_amt">
							<div class="total_amt_name">
								<span>Total Amount</span>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			<div class="panel panel-inverse" data-sortable-id="table-basic-8">
				<div class="panel-body">
					<div class="filter">
						<button class="hidden btn btn-xs btn-default filter-submit margin-bottom"><i class="fa fa-search"></i> Search</button>
					</div>
					<div class="table-container">
						<div class="row">
							<div class="col-md-11">
							</div>
							<div class="col-md-1">
								<a target="_blank" href="scripts/invoice/allFund_Print.php?f_id=<?php echo $f_id; ?>&r_id=<?php echo $r_id; ?>&emp_id=<?php echo $emp_id; ?>&sdate=<?php echo $sdate; ?>&edate=<?php echo $edate; ?>" class="btn btn-sm btn-success">Print</a>
							</div>
						</div>
						<table class="table table-striped table-bordered table-hover ajax-table rowclick">
							<thead>
								<tr role="row" class="heading">
									<th class="no-sort" width="5%"><input type="checkbox" class="toggle_all" /></th>
									<th>#</th>
									<th>Name</th>
									<th>Amount</th>
									<th>Type</th>
									<th>Note </th>
									<th>Receive by</th>
									<th>Date</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>

					</br>
					</br>
					</br>
				</div>
			</div>

			<!-- end panel -->
		</div>
		<!-- end col-12 -->
	</div>
	<!-- end row -->
</div>


<script type="text/javascript">
	var grid;
	$(document).ready(function() {
		grid = new Datatable();
		var f_id = '<?php echo $f_id; ?>';
		var r_id = '<?php echo $r_id; ?>';
		var emp_id = '<?php echo $emp_id; ?>';
		var sdate = '<?php echo $sdate; ?>';
		var edate = '<?php echo $edate; ?>';
		$("#page_allFund").addClass("active");


		$('#default-daterange').daterangepicker({});

		grid.init({
			src: $(".ajax-table"),
			onSuccess: function(grid) {
				// execute some code after table records loaded
				$(".filter").appendTo(".table-toolbar");
				$("[name=search]:eq(0)").focus();
			},
			onError: function(grid) {
				// execute some code on network or other general error  
			},
			dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options 
				/* 
				    By default the ajax datatable's layout is horizontally scrollable and this can cause an issue of dropdown menu is used in the table rows which.
				    Use below "sDom" value for the datatable layout if you want to have a dropdown menu for each row in the datatable. But this disables the horizontal scroll. 
				*/
				//"sDom" : "<'row'<'col-md-8 col-sm-12'pl><'col-md-4 col-sm-12 text-right'i<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pl><'col-md-4 col-sm-12 text-right'i>r>>", 
				"sDom": "<'table-toolbar'>t<'row'<'col-md-8 col-sm-12'pl><'col-md-4 col-sm-12 text-right'i>r>>",
				"aLengthMenu": [
					[400, 500, 1000, 1500, 2000],
					[400, 500, 1000, 1500, 2000] // change per page values here
				],
				"oLanguage": { // language settings
					"sProcessing": '<fa class="fa fa-spin fa-spinner"></fa> Loading...',
				},
				"iDisplayLength": 400, // default record count per page
				"bServerSide": true, // server side processing
				"sAjaxSource": "scripts/php/allFund/allFund_ajax.php?f_id=<?php echo $f_id; ?>&r_id=<?php echo $r_id; ?>&emp_id=<?php echo $emp_id; ?>&sdate=<?php echo $sdate; ?>&edate=<?php echo $edate; ?>", // ajax source
				"aaSorting": [
					[1, "asc"]
				], // set first column as a default sort by asc
				"aoColumns": [{
						"sName": "select",
						"bVisible": false
					},
					{
						"sName": "id",
						"bSortable": true,
						"sWidth": "2%"
					},
					{
						"sName": "name",
						"sWidth": "10%"
					},
					{
						"sName": "amount",
						"sWidth": "5%"
					},

					{
						"sName": "fund",
						"sWidth": "5%"
					},
					{
						"sName": "notes",
						"sWidth": "5%"
					},
					{
						"sName": "name",
						"sWidth": "5%"
					},

					{
						"sName": "date",
						"sWidth": "9%"
					},

					{
						"sName": "status",
						"sWidth": "5%"
					},
				],
				"fnFooterCallback": function(nRow, aaData, iStart, iEnd, aiDisplay) {
					//var nCells = nRow.getElementsByTagName('th');
					//nCells[1].innerHTML=ajaxTotal.total_qty;
				},
				"fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
					$(nRow).data("row", aData[0]);
					$(nRow).find(".delete").click(function(e) {
						e.stopPropagation();
						var id = $(this).attr('i');
						$("input[name='id']").val(id);
						$("#modal-del").modal("show");
					});
					$(nRow).click(function() {
						var r = $(this).data("row");
						//console.log(r);
						// redirect("#order_info.php?id=" + r);
					});
				}
			}
		});


		$(".form-filter").change(function() {
			$(".filter-submit").trigger("click");
		});

		var search_timeout = null;
		$("[name=search]").keyup(function() {
			var self = this;
			if (search_timeout) {
				clearTimeout(search_timeout);
				search_timeout = null;
			}
			search_timeout = setTimeout(function() {
				$(".filter-submit").trigger("click");
			}, 500);
		});


		/* Add Category */

		$(".selectFund").change(function(e) {
			var value = $(".selectFund  option:selected").val();
			window.location = "#AllFund.php?f_id=" + value + "&r_id=" + r_id + "&sdate=" + sdate + "&edate=" + edate + "&emp_id=" + emp_id;
			location.reload();
		});
		$(".selectUser").change(function(e) {
			var value = $(".selectUser  option:selected").val();
			window.location = "#AllFund.php?f_id=" + f_id + "&r_id=" + value + "&sdate=" + sdate + "&edate=" + edate + "&emp_id=" + emp_id;
			location.reload();
		});
		$(".selectEMP").change(function(e) {
			var value = $(".selectEMP  option:selected").val();
			window.location = "#AllFund.php?f_id=" + f_id + "&r_id=" + r_id + "&sdate=" + sdate + "&edate=" + edate + "&emp_id=" + value;
			location.reload();
		});
		// $(".selectSalesman").change(function(e) {
		// 	var value = $(".selectSalesman  option:selected").val();
		// 	window.location = "#order.php?f_id=" + f_id + "&s_id=" + value + "&c_id=" + c_id + "&sdate=" + sdate + "&edate=" + edate;
		// 	location.reload();
		// });
		$(".applyBtn").click(function(e) {
			setTimeout(function() {
				var date = $('#default-daterange').val();
				var test = date.split(' - ')
				window.location = "#AllFund.php?f_id=" + f_id + "&r_id=" + r_id + "&sdate=" + test[0] + "&edate=" + test[1] + "&emp_id=" + emp_id;
				location.reload();
			}, 500);
		});
		getOrderShortInfo();
		getOrderShortInfoTotal();

		function getOrderShortInfo() {
			$.post(siteURL + "scripts/php/allFund/get_fund_info.php?f_id=<?php echo $f_id; ?>&r_id=<?php echo $r_id; ?>&sdate=<?php echo $sdate; ?>&edate=<?php echo $edate; ?>&emp_id=<?php echo $emp_id; ?>", function(data) {
				var data = JSON.parse(data)
				if (data.success) {
					var st = data.success;
					if (st.length > 0) {
						for (var i = 0; i < st.length; i++) {
							$(".cat_" + st[i].id).append("<div class='productBox box'>" +
								"<span style='font-size: 15px;font-weight: bold;'>" + st[i].amount + "</span><br>" +
								"</div>");

						}
					}
				}
			});
		}

		function getOrderShortInfoTotal() {
			$.post(siteURL + "scripts/php/allFund/get_fund_info_cat.php?f_id=<?php echo $f_id; ?>&r_id=<?php echo $r_id; ?>&sdate=<?php echo $sdate; ?>&edate=<?php echo $edate; ?>&emp_id=<?php echo $emp_id; ?>", function(data) {
				var data = JSON.parse(data)
				if (data.success) {
					var st = data.success;
					$(".total_amt").append("<div class='Totalamt_Box box'>" +
						"<span style='font-size: 15px;font-weight: bold;'>" + st.amount + "</span><br>" +
						"</div>");
				} else {
					$(".total_amt").append("<div class='Totalamt_Box box'>" +
						"<span style='font-size: 15px;font-weight: bold;'>0</span><br>" +
						"</div>");
				}
			});
		}
	});
</script>